<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

# app/controllers/AuthController.php

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = factory(User::class)->make();

        $response = $this->actingAs($user)->post('/api/login');
        $response->assertStatus(200);
        $this->assertAuthenticatedAs($user);
    }
}
