// backup 25-5-2022 22_22PM
// importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js');
// importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js');
//
// // importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
// // importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');
//
// // Initialize Firebase
// var config = {
//     //zuul
//     apiKey: "AIzaSyBMj7XglMuT4qXj3me5p9dG_Meqe6Fq49o",
//     authDomain: "zuul-master.firebaseapp.com",
//     databaseURL: "https://zuul-master.firebaseio.com",
//     projectId: "zuul-master",
//     storageBucket: "zuul-master.appspot.com",
//     messagingSenderId: "1013551636023"
//
//     // test firebase project credentials
//     // apiKey: "AIzaSyDREe7JeyrB_8tv_a2wxlyWnLmhtiGuXeg",
//     // authDomain: "test-a8d5a.firebaseapp.com",
//     // projectId: "test-a8d5a",
//     // storageBucket: "test-a8d5a.appspot.com",
//     // messagingSenderId: "361496221420",
//     // appId: "1:361496221420:web:72cb3dfa9e25afcc7537c9",
//     // measurementId: "G-WMTVRHQP9E"
// };
//
// firebase.initializeApp(config);
//
// /*
// Retrieve an instance of Firebase Messaging so that it can handle background messages.
// */
// const messaging = firebase.messaging();
// messaging.setBackgroundMessageHandler(function(payload) {
//     console.log(
//         "[firebase-messaging-sw.js] Received background message ",
//         payload,
//     );
//     // Customize notification here
//     const notificationTitle = "Background Message Title";
//     const notificationOptions = {
//         body: "Background Message body.",
//         icon: "/itwonders-web-logo.png",
//     };
//
//     return self.registration.showNotification(
//         notificationTitle,
//         notificationOptions,
//     );
// });
// backup 25-5-2022 22_22PM

importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js');
// importScripts('https://www.gstatic.com/firebasejs/4.6.2/init.js');


// Initialize Firebase
var config = {
    //zuul
    // apiKey: "AIzaSyBMj7XglMuT4qXj3me5p9dG_Meqe6Fq49o",
    // authDomain: "zuul-master.firebaseapp.com",
    // databaseURL: "https://zuul-master.firebaseio.com",
    // projectId: "zuul-master",
    // storageBucket: "zuul-master.appspot.com",
    // messagingSenderId: "1013551636023"

    //zuul 26-5-2022
    apiKey: "AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA",
    authDomain: "zuul-master.firebaseapp.com",
    databaseURL: "https://zuul-master.firebaseio.com",
    projectId: "zuul-master",
    storageBucket: "zuul-master.appspot.com",
    messagingSenderId: "1013551636023",
    appId: "1:1013551636023:web:55e1b9e9286170e003e9b2",
    measurementId: "G-9YZ7WDH9C4"

    //test api
    // apiKey: "AIzaSyDREe7JeyrB_8tv_a2wxlyWnLmhtiGuXeg",
    // authDomain: "test-a8d5a.firebaseapp.com",
    // projectId: "test-a8d5a",
    // storageBucket: "test-a8d5a.appspot.com",
    // messagingSenderId: "361496221420",
    // appId: "1:361496221420:web:72cb3dfa9e25afcc7537c9",
};

firebase.initializeApp(config);
const messaging = firebase.messaging();



