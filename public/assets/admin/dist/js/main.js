$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    },
    success: function(response, textStatus, jqXHR) {
        hideLoader();
        if (response.hasOwnProperty("message") && response.message.length > 0) {
            if (response.hasOwnProperty("status")) {
                if (response.status) {
                    toastr.success(response.message);
                    if (
                        response.hasOwnProperty("closeModel") &&
                        response.closeModel.length > 0
                    ) {
                        $("#" + response.closeModel).modal("hide");
                    }
                } else {
                    notifyError(response.message);
                }
            } else {
                notifyError(response.message);
            }
        }
        if (response.hasOwnProperty("redirect")) {
            window.location.href = response.redirect;
        }
    },
    error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 422) {
            notifyError(Object.values(jqXHR.responseJSON.errors)[0]);
            $.each(jqXHR.responseJSON.errors, function(key, item) {
                // notifyError(item[0]);
            });
        } else {
            notifyError(errorThrown);
        }
    }
});

$(document).on("submit", ".ajax-form", function(e) {
    e.preventDefault();

    $(this)
        .find('button[type="submit"]')
        .append(' <i class="fa fa-spinner fa-spin"></i>');
    $(this)
        .find('button[type="submit"]')
        .prop("disabled", true);

    $(this).ajaxSubmit({
        error: function(jqXHR, textStatus, errorThrown, form) {
            $(form)
                .find(":input.is-invalid")
                .removeClass("is-invalid");
            $(form)
                .find(".error")
                .remove();

            if (jqXHR.status == 422) {
                //notifyError(Object.values(jqXHR.responseJSON.errors)[0]);
                $.each(jqXHR.responseJSON.errors, function(key, item) {
                    $(form)
                        .find(':input[name="' + key + '"]')
                        .addClass("is-invalid");

                    var field = $(form).find(':input[name="' + key + '"]');
                    $(
                        "<span style='color:red;padding-top:2px' class='error'>" +
                        item +
                        "</span>"
                    ).insertAfter(field);
                });

                if ($(form).find(":input.is-invalid:first").length > 0) {
                    $(form)
                        .find(":input.is-invalid:first")
                        .focus()
                        .scroll();

                    var new_position =
                        $(form)
                            .find(":input.is-invalid:first")
                            .offset().top - 200;

                    $("html, body").animate({ scrollTop: new_position }, 500);
                }
            } else {
                notifyError(errorThrown);
            }
        },
        complete: function(jqXHR, textStatus, form) {
            form.find('button[type="submit"]').prop("disabled", false);
            form.find('button[type="submit"] i.fa-spin').remove();

            if (textStatus == "success") {
                form.find(":input.is-invalid").removeClass("is-invalid");
            }
        }
    });
});

//Mask input phone number
$(document).ready(function(e) {
    $("[data-mask]").inputmask();
    // $(".mask-phone").mask("999-999-9999");
    $("[data-bootstrap-switch]").each(function() {
        $(this).bootstrapSwitch("state", $(this).prop("checked"));
    });
});

function notifyError($msg) {
    /*notif({
        msg: $msg,
        type: "error",
        position: "center",
        color: "white"
    });*/
    toastr.error($msg);
}

function notifySuccess($msg) {
    toastr.success($msg);
}

function isNumberKey(evt) {
    let charCode = evt.which ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return !1;
    return !0;
}

function baseUrl(route) {
    let url = $('meta[name="base-url"]').attr("content");
    return url + "/" + route;
}

function showLoader() {
    $(".loading").show();
}

function hideLoader() {
    $(".loading").hide();
}

function baseUrl(route) {
    let url = $('meta[name="base-url"]').attr("content");
    return url + "/" + route;
}

function zuulDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = null
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        destroy: true,
        searching: false,
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}


function zuulDataTableSortable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: url,
        columns: columns,
        // columns: columns.map((col, index) => ({
        //     ...col,
        //     sort: "string" // Set column type to "string" for sorting
        // })),
        // columns: columns.map((col, index) => ({
        //     ...col,
        //     sort: index === 0 || /\d+/.test(col.data) ? "natural" : "string"
        // })),
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[DefaultOrderColumn, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]

    });

}

function zuulDataTableSortableGuestLogs(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    // Create the loader element
    const loader = document.createElement("div");
    loader.className = "loader";
    loader.id = "csvLoader";

    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: url,
        // columns: columns,
        columns: columns.map((col, index) => ({
            ...col,
            sort: "string" // Set column type to "string" for sorting
        })),
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[DefaultOrderColumn, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        dom: 'Blfrtip', // Add the "B" to enable buttons
        buttons: [
            {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i>&nbsp;&nbsp; Export Report',
                className: 'btn btn-primary', // Add your desired CSS classes for the button
                titleAttr: 'Export Report', // Tooltip text for the button
                init: function (api, node, config) {
                    // Remove any additional button styling
                    $(node).removeClass('btn-secondary btn-outline-secondary dt-button buttons-csv');
                    // Add a custom class for the button style
                    $(node).addClass('btn-custom-style');
                },
                action: function (e, dt, node, config) {
                    // Show the loader before starting the download process
                    $(node).append(loader);

                    // Fetch all data from the server
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            const allData = response.data;

                            let selectedColumns = getColumnWithHeaders(this.url).column;

                            let customHeaders = getColumnWithHeaders(this.url).headers;


                            getDataInColumns(this.url,selectedColumns,customHeaders,allData);

                            // Hide the loader after the download is complete
                            $(loader).remove();
                        },
                        error: function (error) {
                            console.error('Error fetching data:', error);

                            // Hide the loader if there's an error
                            $(loader).remove();
                        }
                    });


                }
            }
        ]
    });
}


function zuulImportFileDataTableSortable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    var table = $(selector).DataTable({
        destroy: true,
        retrieve: true,
        pageLength: 100,
        fixedHeader: true,
        ajax: ajax,
        columns: [
            { data: "first_name" },
            { data: "last_name" },
            { data: "email" },
            { data: "phone_number" },
            { data: "house_name" },
            { data: "head_of_family" },
            { data: "special_instructions" },
            { data: "temporary_password" },
            { data: "status" },
            { data: "message" },
            {
                data: function(data) {
                    return data.links;
                }
            }
        ],
        initComplete: function() {
            var api = this.api();

            // For each column
            api.columns()
                .eq(0)
                .each(function(colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $(".filters th").eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html(
                        '<input type="text" placeholder="' + title + '" />'
                    );

                    // On every keypress in this input
                    $(
                        "input",
                        $(".filters th").eq(
                            $(api.column(colIdx).header()).index()
                        )
                    )
                        .off("keyup change")
                        .on("keyup change", function(e) {
                            e.stopPropagation();

                            // Get the search value
                            $(this).attr("title", $(this).val());
                            var regexr = "({search})"; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api.column(colIdx)
                                .search(
                                    this.value !== ""
                                        ? regexr.replace(
                                            "{search}",
                                            "(((" + this.value + ")))"
                                        )
                                        : "",
                                    this.value !== "",
                                    this.value === ""
                                )
                                .draw();

                            $(this)
                                .focus()[0]
                                .setSelectionRange(
                                    cursorPosition,
                                    cursorPosition
                                );
                        });
                });
        }
    });
}

function TLDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = null
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[0, "desc"]],
        columnDefs: [
            { orderable: false, targets: disableColumnSearch },
            {
                targets: [0],
                visible: false,
                searchable: false
            }
        ],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}

function TLUnMatchedDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = null
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[1, "desc"]],
        columnDefs: [
            { orderable: false, targets: disableColumnSearch },
            {
                targets: [],
                visible: false,
                searchable: false
            }
        ],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}

function authLogDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = null
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        destroy: true,
        searching: true,
        order: [[0, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}

function importFileDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: url,
        // "columns":columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[DefaultOrderColumn, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
}

//send reset password link
$(document).on("click", ".send-reset-password-link", function() {
    let id = $(this).data("id");
    showLoader();
    $.ajax({
        url: baseUrl("admin/user/send-reset-link"),
        type: "POST",
        data: {
            id: id
        }
    });
});

function formDataForDataTable(selector) {
    let result = [];

    $.each($(selector).serializeArray(), function() {
        result[this.name] = this.value;
    });

    return result;
}

function PrintImage(source) {
    Pagelink = "about:blank";
    var pwa = window.open(Pagelink, "_new");
    pwa.document.open();
    pwa.document.write(ImageToPrint(source));
    pwa.document.close();
}
function ImageToPrint(source) {
    return (
        "<html><head><script>function step1(){\n" +
        "setTimeout('step2()', 10);}\n" +
        "function step2(){window.print();window.close()}\n" +
        "</scri" +
        "pt></head><body onload='step1()'>\n" +
        "<img src='" +
        source +
        "' width='100%'/></body></html>"
    );
}

//Mark:- This function will extract the dynamic headers of each table
function getColumnWithHeaders(url){
    let columns;
    let headers;

    console.log(url,'Request url');

    if(url.includes('guest-logs-data')) {//Mark:- For Guest Log Data
        columns = ['id', 'sender', 'recipient', 'community_name', 'event_name', 'formatted_pass_start_date', 'formatted_pass_date', 'scan_date', 'scan_by', 'vehicle_info', 'log_text'];
        headers = {
            'id': 'Id',
            'sender': 'Sender',
            'recipient': 'Recipient',
            'community_name': 'Community Name',
            'event_name': 'Event',
            'formatted_pass_start_date': 'Start',
            'formatted_pass_date': 'Expires',
            'scan_date': 'Scan Date',
            'scan_by': 'Scan By',
            'vehicle_info': 'License',
            'log_text': 'Status'
        };
    }else if(url.includes('houses-data')) {//Mark:- For Houses Data
        columns = ['id', 'house_name', 'house_detail', 'lot_number', 'community_name', 'total_house_members', 'complete_profile', 'phone'];
        headers = {
            'id': 'Id',
            'house_name': 'Resident Name',
            'house_detail': 'Address',
            'lot_number': 'Lot Number',
            'community_name': 'Community Name',
            'total_house_members': 'Household Member(s)',
            'complete_profile': 'Completed Profile',
            'phone': 'Primary Contact',
        };
    }else if(url.includes('rfids-data')) {//Mark:- For Rfid Data
        columns = ['id', 'rfid_tag_num', 'resident_name', 'resident_street_address', 'resident_phone', 'community_name', 'license', 'resident_email','additional_drivers','vehicle_info','status'];
        headers = {
            'id': 'Id',
            'rfid_tag_num': 'Rfid Tag Number',
            'resident_name': 'Resident Name',
            'resident_street_address': 'Address',
            'resident_phone': 'Phone',
            'community_name': 'Community',
            'license': 'License Plate',
            'resident_email': 'Email',
            'additional_drivers':'Additional Drivers',
            'vehicle_info':'Vehicle Info',
            'status':'Status',
        };
    }else if(url.includes('residents-data')) {//Mark:- For Resident Data
        columns = ['id', 'full_name', 'formatted_phone_by_dial_code', 'email', 'house_detail', 'is_suspended', 'role_id', 'community_id','house_name','special_instruction','created_at','is_completed_profile'];
        headers = {
            'id': 'Id',
            'full_name': 'Resident Name',
            'formatted_phone_by_dial_code': 'Address',
            'email': 'Lot Number',
            'house_detail': 'Community Name',
            'is_suspended': 'Household Member(s)',
            'role_id': 'Completed Profile',
            'community_id': 'Primary Contact',
            'house_name': 'Primary Contact',
            'special_instruction':'',
            'created_at':'',
            'is_completed_profile':'',
        };
    }else if(url.includes('rfid-logs-data')) {//Mark:- For Rfid Tracking Data
        columns = ['id', 'rfid', 'resident_name', 'scanner', 'scan_at', 'vehicle_info', 'log_text','formatted_status'];
        headers = {
            'id': 'Id',
            'rfid': 'Rfid Number',
            'resident_name': 'Resident Name',
            'scanner': 'Scanner',
            'scan_at': 'Scan At',
            'vehicle_info': 'Vehicle Info',
            'log_text': 'Log text',
            'formatted_status':'Status'
        };
    }else if(url.includes('guest-band-data')) {//Mark:- For Guest Banned List Data
        columns = ['id', 'communities', 'full_name', 'formatted_phone', 'license_plate', 'created_at'];
        headers = {
            'id': 'Id',
            'communities': 'Communities',
            'community_name': 'Name',
            'formatted_phone': 'Phone',
            'license_plate': 'License Plate',
            'created_at': 'Created at',
        };
    }else if(url.includes('passes-data')) {//Mark:- For Passes Data
        columns = ['id', 'created_by_name', 'community_name', 'pass_start_date', 'pass_date', 'description','created_at'];
        headers = {
            'id': 'Id',
            'created_by_name': 'Sender',
            'community_name': 'Community',
            'pass_start_date': 'Start',
            'pass_date': 'Expires',
            'description': 'Description',
            'created_at':'Created at',
        };
    }else if(url.includes('guards-data')) {//Mark:- For Guard Data
        columns = ['id', 'full_name', 'phone_number', 'email', 'role_name', 'created_at'];
        headers = {
            'id': 'Id',
            'full_name': 'Name',
            'phone_number': 'Phone',
            'email': 'Email',
            'role_name': 'Role',
            'created_at':'Created at',
        };
    }else if(url.includes('vehicles-data')) {//Mark:- For Vehicle Data
        columns = ['id', 'license_plate', 'make', 'model', 'year', 'color','user_id','ticket_count'];
        headers = {
            'id': 'Id',
            'license_plate': 'License Plate',
            'make': 'Make',
            'model': 'Model',
            'year': 'Year',
            'color':'Color',
            'user_id':'User Id',
            'ticket_count':'Tickets',
        };
    }else if(url.includes('community-admins-data')) {//Mark:- For Community Admin Data
        columns = ['id', 'full_name', 'formatted_phone', 'email', 'role_name', 'community_name','created_at'];
        headers = {
            'id': 'Id',
            'full_name': 'Name',
            'formatted_phone': 'Phone',
            'email': 'Email',
            'role_name': 'Role',
            'community_name':'Community',
            'created_at':'Created',
        };
    }

    return {'column':columns,'headers':headers};
}

//Mark:- This function will map the data in columns
function getDataInColumns(url,selectedColumns,customHeaders,allData) {

    console.log(selectedColumns);

    let csvContent = selectedColumns.map(col => customHeaders[col]).join(",") + "\n";
    var csvFileName = '';

    allData.forEach(row => {
        const rowData = selectedColumns.map(col => {

            console.log(url.includes('guest-logs-data'),'>>>>>>>>>>>>>>>>>>>');

            if(url.includes('guest-logs-data')) {//Mark:- For Guest Log Data

                //Mark:- override csv file name
                csvFileName = 'GuestLog-Report.csv';

                if (col === 'sender') {
                    return row["sender_name"] + "<br/>" + row["sender_phone"] + "<br/>" + row["sender_full_address"];
                } else if (col === 'recipient') {
                    return row["guard_display_name"] + "<br/>" + row["receiver_name"] + "<br/>" + row["receiver_phone"] + "<br/>" + row["receiver_full_address"];
                } else {
                    let value = row[col];
                    if (typeof value === 'string') {
                        // Remove newline characters (\n) from the value
                        value = value.replace(/\n/g, '');
                        // Wrap the value in double quotes if it contains a comma
                        if (value.includes(',')) {
                            value = `"${value}"`;
                        }
                    }
                    return value;
                }
            }else if(url.includes('houses-data')) {//Mark:- For Houses Data


                //Mark:- override csv file name
                csvFileName = 'HousesData-Report.csv';

                if (col === 'total_house_members') {

                    if (row["total_house_members"] == null) {
                        row["total_house_members"] = '0';
                    }

                }

                if (col === 'complete_profile') {
                    //     return row["sender_name"] + "<br/>" + row["sender_phone"] + "<br/>" + row["sender_full_address"];
                    // } else if (col === 'recipient') {
                    //     return row["guard_display_name"] + "<br/>" + row["receiver_name"] + "<br/>" + row["receiver_phone"] + "<br/>" + row["receiver_full_address"];
                    // } else {

                    const response = row["complete_profile"];

                    // Extract the text between the <span> tags
                    const regex = /<span[^>]*>(.*?)<\/span>/;
                    const match = regex.exec(response);

                    // Check if a match was found and extract the text
                    let result;
                    if (match && match.length > 1) {
                        result = match[1];
                    } else {
                        result = 'No match found';
                    }


                    row["complete_profile"] = result;

                    console.log(result);  // Output: No
                }
                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('rfids-data')) {//Mark:- For Rfid Data


                //Mark:- override csv file name
                csvFileName = 'RfidsData-Report.csv';

                if (col === 'status') {

                    if (row["status"] == 1) {
                        row["status"] ='Active';
                    } else {
                        row["status"] ='Not Active';
                    }

                }


                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('residents-data')) {//Mark:- For Resident Data


                //Mark:- override csv file name
                csvFileName = 'ResidentsData-Report.csv';


                if (col === 'is_completed_profile') {
                    //     return row["sender_name"] + "<br/>" + row["sender_phone"] + "<br/>" + row["sender_full_address"];
                    // } else if (col === 'recipient') {
                    //     return row["guard_display_name"] + "<br/>" + row["receiver_name"] + "<br/>" + row["receiver_phone"] + "<br/>" + row["receiver_full_address"];
                    // } else {

                    const response = row["is_completed_profile"];

                    // Extract the text between the <span> tags
                    const regex = /<span[^>]*>(.*?)<\/span>/;
                    const match = regex.exec(response);

                    // Check if a match was found and extract the text
                    let result;
                    if (match && match.length > 1) {
                        result = match[1];
                    } else {
                        result = 'No match found';
                    }


                    row["is_completed_profile"] = result;

                    console.log(result);  // Output: No
                }
                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('rfid-logs-data')) {//Mark:- For Rfid Tracking Data


                //Mark:- override csv file name
                csvFileName = 'RfidTracking-Report.csv';

                if (col === 'formatted_status') {
                    //     return row["sender_name"] + "<br/>" + row["sender_phone"] + "<br/>" + row["sender_full_address"];
                    // } else if (col === 'recipient') {
                    //     return row["guard_display_name"] + "<br/>" + row["receiver_name"] + "<br/>" + row["receiver_phone"] + "<br/>" + row["receiver_full_address"];
                    // } else {

                    const response = row["formatted_status"];

                    // Extract the text between the <span> tags
                    const regex = /<div[^>]*>(.*?)<\/div>/;
                    const match = regex.exec(response);

                    // Check if a match was found and extract the text
                    let result;
                    if (match && match.length > 1) {
                        result = match[1];
                    } else {
                        result = 'No match found';
                    }


                    row["formatted_status"] = result;

                    console.log(result);  // Output: No
                }
                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('guest-band-data')) {//Mark:- For Guest Band List Data


                //Mark:- override csv file name
                csvFileName = 'BandList-Report.csv';

                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('passes-data')) {//Mark:- For Guest Band List Data


                //Mark:- override csv file name
                csvFileName = 'Passes-Report.csv';

                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('guards-data')) {//Mark:- For Guards List Data


                //Mark:- override csv file name
                csvFileName = 'Guards-Report.csv';

                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('vehicles-data')) {//Mark:- For Vehicles Data


                //Mark:- override csv file name
                csvFileName = 'Vehicle-Report.csv';

                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }else if(url.includes('community-admins-data')) {//Mark:- For Community Admins Data


                //Mark:- override csv file name
                csvFileName = 'CommunityAdmins-Report.csv';

                let value = row[col];
                if (typeof value === 'string') {
                    // Remove newline characters (\n) from the value
                    value = value.replace(/\n/g, '');
                    // Wrap the value in double quotes if it contains a comma
                    if (value.includes(',')) {
                        value = `"${value}"`;
                    }
                }
                return value;
                // }
            }


        });

        csvContent += rowData.join(",") + "\n";
    });


    // Trigger the download
    const blob = new Blob([csvContent], { type: 'text/csv' });
    url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = csvFileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);


}

$(document).ready(function() {
    // open image in div
    $(document).on("click", ".print-img", function(event) {
        let img = $(this);
        img.attr("src");
        $(".enlargeImgDiv").remove();
        // var name = $(this).closest('td').find('.sorting_1').text();
        //onerror="this.src = 'https://zuul.dev/public/images/licance/licence.png'"
        var name = $(this).closest("td");
        let html = `
                    <div class="enlargeImgDiv parentDiv">
                                <button class="btn-print-img">Print</button>
                                <button class="btn-close-en-large">Close</button>
                                <img src="${img.attr(
            "src"
        )}" style="width: 100%;" class="user-image" alt="licence Image not fount" >
                    </div>
               `;
        $(this)
            .closest("td")
            .append(html);
        console.log(name);
        img.append(`<div><h1>Hello</h1></div>`);
    });
    //close image div
    $(document).on("click", ".btn-close-en-large", function(e) {
        $(".enlargeImgDiv").remove();
    });
    //print image
    $(document).on("click", ".btn-print-img", function(e) {
        let src = $(this)
            .parent()
            .children(".user-image")
            .attr("src");
        // var src = $(this).parent().parent().data('src');
        console.log(src);
        if (src) {
            PrintImage(src);
        }
    });
});




