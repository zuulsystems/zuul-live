$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    },
    success: function(response, textStatus, jqXHR) {
        hideLoader();
        if (response.hasOwnProperty("message") && response.message.length > 0) {
            if (response.hasOwnProperty("status")) {
                if (response.status) {
                    toastr.success(response.message);
                    if (
                        response.hasOwnProperty("closeModel") &&
                        response.closeModel.length > 0
                    ) {
                        $("#" + response.closeModel).modal("hide");
                    }
                } else {
                    notifyError(response.message);
                }
            } else {
                notifyError(response.message);
            }
        }
        if (response.hasOwnProperty("redirect")) {
            window.location.href = response.redirect;
        }
    },
    error: function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 422) {
            notifyError(Object.values(jqXHR.responseJSON.errors)[0]);
            $.each(jqXHR.responseJSON.errors, function(key, item) {
                // notifyError(item[0]);
            });
        } else {
            notifyError(errorThrown);
        }
    }
});

$(document).on("submit", ".ajax-form", function(e) {
    e.preventDefault();

    $(this)
        .find('button[type="submit"]')
        .append(' <i class="fa fa-spinner fa-spin"></i>');
    $(this)
        .find('button[type="submit"]')
        .prop("disabled", true);

    $(this).ajaxSubmit({
        error: function(jqXHR, textStatus, errorThrown, form) {
            $(form)
                .find(":input.is-invalid")
                .removeClass("is-invalid");
            $(form)
                .find(".error")
                .remove();

            if (jqXHR.status == 422) {
                //notifyError(Object.values(jqXHR.responseJSON.errors)[0]);
                $.each(jqXHR.responseJSON.errors, function(key, item) {
                    $(form)
                        .find(':input[name="' + key + '"]')
                        .addClass("is-invalid");

                    var field = $(form).find(':input[name="' + key + '"]');
                    $(
                        "<span style='color:red;padding-top:2px' class='error'>" +
                            item +
                            "</span>"
                    ).insertAfter(field);
                });

                if ($(form).find(":input.is-invalid:first").length > 0) {
                    $(form)
                        .find(":input.is-invalid:first")
                        .focus()
                        .scroll();

                    var new_position =
                        $(form)
                            .find(":input.is-invalid:first")
                            .offset().top - 200;

                    $("html, body").animate({ scrollTop: new_position }, 500);
                }
            } else {
                notifyError(errorThrown);
            }
        },
        complete: function(jqXHR, textStatus, form) {
            form.find('button[type="submit"]').prop("disabled", false);
            form.find('button[type="submit"] i.fa-spin').remove();

            if (textStatus == "success") {
                form.find(":input.is-invalid").removeClass("is-invalid");
            }
        }
    });
});

//Mask input phone number
$(document).ready(function(e) {
    $("[data-mask]").inputmask();
    // $(".mask-phone").mask("999-999-9999");
    $("[data-bootstrap-switch]").each(function() {
        $(this).bootstrapSwitch("state", $(this).prop("checked"));
    });
});

function notifyError($msg) {
    /*notif({
        msg: $msg,
        type: "error",
        position: "center",
        color: "white"
    });*/
    toastr.error($msg);
}

function notifySuccess($msg) {
    toastr.success($msg);
}

function isNumberKey(evt) {
    let charCode = evt.which ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return !1;
    return !0;
}

function baseUrl(route) {
    let url = $('meta[name="base-url"]').attr("content");
    return url + "/" + route;
}

function showLoader() {
    $(".loading").show();
}
function hideLoader() {
    $(".loading").hide();
}
function baseUrl(route) {
    let url = $('meta[name="base-url"]').attr("content");
    return url + "/" + route;
}

function zuulDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = []
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        destroy: true,
        searching: false,
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}
function zuulDataTableSortable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: url,
        columns: columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[DefaultOrderColumn, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
}

function zuulImportFileDataTableSortable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    var table = $(selector).DataTable({
        destroy: true,
        retrieve: true,
        pageLength: 100,
        fixedHeader: true,
        ajax: ajax,
        columns: [
            { data: "first_name" },
            { data: "last_name" },
            { data: "email" },
            { data: "phone_number" },
            { data: "house_name" },
            { data: "head_of_family" },
            { data: "special_instructions" },
            { data: "temporary_password" },
            { data: "status" },
            { data: "message" },
            {
                data: function(data) {
                    return data.links;
                }
            }
        ],
        initComplete: function() {
            var api = this.api();

            // For each column
            api.columns()
                .eq(0)
                .each(function(colIdx) {
                    // Set the header cell to contain the input element
                    var cell = $(".filters th").eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    $(cell).html(
                        '<input type="text" placeholder="' + title + '" />'
                    );

                    // On every keypress in this input
                    $(
                        "input",
                        $(".filters th").eq(
                            $(api.column(colIdx).header()).index()
                        )
                    )
                        .off("keyup change")
                        .on("keyup change", function(e) {
                            e.stopPropagation();

                            // Get the search value
                            $(this).attr("title", $(this).val());
                            var regexr = "({search})"; //$(this).parents('th').find('select').val();

                            var cursorPosition = this.selectionStart;
                            // Search the column for that value
                            api.column(colIdx)
                                .search(
                                    this.value != ""
                                        ? regexr.replace(
                                              "{search}",
                                              "(((" + this.value + ")))"
                                          )
                                        : "",
                                    this.value != "",
                                    this.value == ""
                                )
                                .draw();

                            $(this)
                                .focus()[0]
                                .setSelectionRange(
                                    cursorPosition,
                                    cursorPosition
                                );
                        });
                });
        }
    });
}

function TLDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = []
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[0, "desc"]],
        columnDefs: [
            { orderable: false, targets: disableColumnSearch },
            {
                targets: [0],
                visible: false,
                searchable: false
            }
        ],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}

function TLUnMatchedDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = []
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[1, "desc"]],
        columnDefs: [
            { orderable: false, targets: disableColumnSearch },
            {
                targets: [],
                visible: false,
                searchable: false
            }
        ],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}
function authLogDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    type = "GET",
    data = []
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: { url, type, data },
        columns: columns,
        destroy: true,
        searching: true,
        order: [[0, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [15, 25, 50, -1],
            [15, 25, 50, "All"]
        ]
    });
}

function importFileDataTable(
    selector,
    url,
    columns,
    disableColumnSearch = [],
    DefaultOrderColumn = 0
) {
    $(selector).DataTable({
        processing: true,
        scrollX: true,
        serverSide: true,
        ajax: url,
        // "columns":columns,
        pageLength: 50,
        destroy: true,
        searching: false,
        order: [[DefaultOrderColumn, "desc"]],
        columnDefs: [{ orderable: false, targets: disableColumnSearch }],
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ]
    });
}

//send reset password link
$(document).on("click", ".send-reset-password-link", function() {
    let id = $(this).data("id");
    showLoader();
    $.ajax({
        url: baseUrl("admin/user/send-reset-link"),
        type: "POST",
        data: {
            id: id
        }
    });
});

function formDataForDataTable(selector) {
    let result = [];
    $.each($(selector).serializeArray(), function() {
        result[this.name] = this.value;
    });
    return result;
}
function PrintImage(source) {
    Pagelink = "about:blank";
    var pwa = window.open(Pagelink, "_new");
    pwa.document.open();
    pwa.document.write(ImageToPrint(source));
    pwa.document.close();
}
function ImageToPrint(source) {
    return (
        "<html><head><script>function step1(){\n" +
        "setTimeout('step2()', 10);}\n" +
        "function step2(){window.print();window.close()}\n" +
        "</scri" +
        "pt></head><body onload='step1()'>\n" +
        "<img src='" +
        source +
        "' width='100%'/></body></html>"
    );
}
$(document).ready(function() {
    // open image in div
    $(document).on("click", ".print-img", function(event) {
        let img = $(this);
        img.attr("src");
        $(".enlargeImgDiv").remove();
        // var name = $(this).closest('td').find('.sorting_1').text();
        //onerror="this.src = 'https://zuul.dev/public/images/licance/licence.png'"
        var name = $(this).closest("td");
        let html = `
                    <div class="enlargeImgDiv parentDiv">
                                <button class="btn-print-img">Print</button>
                                <button class="btn-close-en-large">Close</button>
                                <img src="${img.attr(
                                    "src"
                                )}" style="width: 100%;" class="user-image" alt="licence Image not fount" >
                    </div>
               `;
        $(this)
            .closest("td")
            .append(html);
        console.log(name);
        img.append(`<div><h1>Hello</h1></div>`);
    });
    //close image div
    $(document).on("click", ".btn-close-en-large", function(e) {
        $(".enlargeImgDiv").remove();
    });
    //print image
    $(document).on("click", ".btn-print-img", function(e) {
        let src = $(this)
            .parent()
            .children(".user-image")
            .attr("src");
        // var src = $(this).parent().parent().data('src');
        console.log(src);
        if (src) {
            PrintImage(src);
        }
    });
});
