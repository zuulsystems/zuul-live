function getColumnWithHeaders(url){
    let columns;
    let headers;

    // Mark:- Url Condition will go here
    columns = ['id', 'sender', 'recipient', 'community_name', 'event_name', 'formatted_pass_start_date', 'formatted_pass_date', 'scan_date', 'scan_by', 'vehicle_info', 'log_text'];
    headers = {
        'id': 'Id',
        'sender': 'Sender',
        'recipient': 'Recipient',
        'community_name': 'Community Name',
        'event_name': 'Event',
        'formatted_pass_start_date': 'Start',
        'formatted_pass_date': 'Expires',
        'scan_date': 'Scan Date',
        'scan_by': 'Scan By',
        'vehicle_info': 'License',
        'log_text': 'Status'
    };

    return {'column':columns,'headers':headers};
}
