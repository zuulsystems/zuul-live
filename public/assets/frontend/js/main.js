
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (response, textStatus, jqXHR) {
        hideLoader();
        if (response.hasOwnProperty('message') && response.message.length > 0) {
            if (response.hasOwnProperty('status')) {
                if (response.status) {
                    toastr.success(response.message);
                    if (response.hasOwnProperty('closeModel') && response.closeModel.length > 0) {
                        $('#'+ response.closeModel).modal('hide');
                    }
                } else {
                    notifyError(response.message);

                }
            } else {
                notifyError(response.message);


            }
        }
        if (response.hasOwnProperty('redirect')) {
            window.location.href = response.redirect;
        }
    },
    error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 422) {
            notifyError(Object.values(jqXHR.responseJSON.errors)[0]);
            $.each(jqXHR.responseJSON.errors, function (key, item) {

                // notifyError(item[0]);

            });
        } else {

            notifyError(errorThrown);

        }
    },
});

$(document).on('submit', '.ajax-form', function (e) {
    e.preventDefault();

    $(this).find('button[type="submit"]').append(' <i class="fa fa-spinner fa-spin"></i>');
    $(this).find('button[type="submit"]').prop('disabled', true);

    $(this).ajaxSubmit({
        error: function (jqXHR, textStatus, errorThrown, form) {
            $(form).find(':input.is-invalid').removeClass('is-invalid');
            $(form).find('.error-field').remove();

            if (jqXHR.status == 422) {
                //notifyError(Object.values(jqXHR.responseJSON.errors)[0]);
                $.each(jqXHR.responseJSON.errors, function (key, item) {

                    $(form).find(':input[name="' + key + '"]').addClass('is-invalid');

                    var field = $(form).find(':input[name="' + key + '"]');
                    $("<span style='color:red;padding-top:2px' class='error-field'>" + item + "</span>").insertAfter(field);

                });

                if ($(form).find(':input.is-invalid:first').length > 0) {
                    $(form).find(':input.is-invalid:first').focus().scroll();

                    var new_position = $(form).find(':input.is-invalid:first').offset().top - 200;

                    $('html, body').animate({scrollTop: new_position}, 500);
                }
            } else {
                notifyError(errorThrown);

            }
        },
        complete: function (jqXHR, textStatus, form) {
            form.find('button[type="submit"]').prop('disabled', false);
            form.find('button[type="submit"] i.fa-spin').remove();

            if (textStatus == 'success') {
                form.find(':input.is-invalid').removeClass('is-invalid');
            }
        }
    });
});

//Mask input phone number
$(document).ready(function (e) {
    $('[data-mask]').inputmask();
});

function notifyError($msg) {
    toastr.error($msg);
}

function notifySuccess($msg) {
    toastr.success($msg);
}


function isNumberKey(evt) {
    let charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return !1;
    return !0
}


function baseUrl(route) {
    let url = $('meta[name="base-url"]').attr('content');
    return url + '/' + route;
}

function showLoader() {
    $('.loading').show();
}

function hideLoader() {
    $('.loading').hide();
}
function baseUrl(route) {
    let url = $('meta[name="base-url"]').attr('content');
    return url + '/' + route;
}

function zuulDataTable(selector,url,columns,disableColumnSearch = []) {

    $(selector).DataTable({
        "processing": true,
        "scrollX": true,
        "serverSide": true,
        "ajax":url,
        "columns":columns,
        "destroy": true,
        "searching": true,
        "columnDefs": [
            { "orderable": false, "targets": disableColumnSearch }
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
}

//update date time format
function formatAMPM(dt) {
    let month = (dt.getMonth() > 8) ? (dt.getMonth() + 1) : ('0' + (dt.getMonth() + 1));
    let date = (dt.getDate() > 9) ? dt.getDate() : ('0' + dt.getDate());
    let year = dt.getFullYear();
    let hours = dt.getHours();
    let minutes = dt.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    let strTime = month + '/' + date + '/' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//validate date
function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}


//get pass detail
//function getPassDetail(passUserId) {
function getPassDetail(passId,userId) {
    showLoader();
    $.ajax({
        url: baseUrl('passes/passes-detail'),
        data: {
            //passUserId:passUserId
            passId:passId,
            userId:userId
        },
        type: "POST",
        dataType: "json",
        success: function (result) {
            hideLoader();
            let passDetail = result.data;
            let html = '';
            if (passDetail  != null && passDetail != "") {
                let pass_detail_model = $('#pass-detail-modal');
                pass_detail_model.find('#pass-event-name').text(passDetail.event_name);
                pass_detail_model.find('#to-date').text(passDetail.to_date);
                pass_detail_model.find('#to-date-time').text(passDetail.to_date_time);
                pass_detail_model.find('#for-date').text(passDetail.for_date);
                pass_detail_model.find('#for-date-time').text(passDetail.for_date_time);
                pass_detail_model.find('#pass-licence-image').attr('src',passDetail.license_image);
                pass_detail_model.find('#sender-name').text(passDetail.sender_name);
                pass_detail_model.find('#sender-community-name').text(passDetail.sender_community_name);
                // var visitor_type = parseInt(passDetail['visitor_type']) === 2 ? 'Vendor / Delivery' : 'Family / Friend';
                let visitor_type = (passDetail.visitor_type == 'vendor_delivery' )? 'Vendor / Delivery' : 'Family / Friend';
                pass_detail_model.find('#visitor-type').text(visitor_type);
                pass_detail_model.find('#sender-phone-number').text(passDetail.sender_formatted_phone_number);
                pass_detail_model.find('#pass-description').text(passDetail.description);
                let qr_code_img = 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=' + passDetail.qr_code;
                pass_detail_model.find('#qr_code_image').attr('src',qr_code_img);
                pass_detail_model.find('#vehicle-license-plate').text(passDetail.license_plate);
                pass_detail_model.find('#vehicle-make').text(passDetail.make);
                pass_detail_model.find('#vehicle-model').text(passDetail.model);
                pass_detail_model.find('.get_directions').on('click', function (e) {
                    e.preventDefault();
                    let link = 'http://maps.google.com/maps?daddr=' + passDetail.description + '';
                    window.open(encodeURI(link), '_blank');
                });

                $('#pass-detail-modal').modal('show');
            } else {
                notifyError('No Record Found !');
            }
        }
    });
}


// ++++++++++++++++++ Pass Event & Functions ++++++++++++++++++

//get pass type
$('#pass_type').change(function () {
    let pass_type = $(this).val();
    let validityOptions  = '';
    console.log(pass_type);
    //check if pass type => self hide contact list
    //set validity options by pass type
    if(pass_type === 'self'){
        validityOptions = `
                <option value="3">3 Hours</option>
                <option value="6">6 Hours</option>
                <option value="12">12 Hours</option>
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                `;
        $('#contacts_div').hide();
        $('#end_date_div').show();

    } else if(pass_type === 'recurring'){
        validityOptions = `
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                <option value="168">1 Week</option>
                <option value="336">2 Weeks</option>
                <option value="720">1 Month</option>
                <option value="876000">No Limit</option>
                `;
        $('#end_date_div').hide();
    } else {
        validityOptions = `
                <option value="3">3 Hours</option>
                <option value="6">6 Hours</option>
                <option value="12">12 Hours</option>
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                `;
        $('#contacts_div').show();
        $('#end_date_div').show();
    }
    $('#pass_validity').html(validityOptions);
});

//update end data when update pass date form data picker
$(document).on('change', '#pass_date', function(){
    if($(this).val().length > 0){
        changeEndDate();
    }
    else{
        $('#end_date').val('');
    }
});

//update end date from pass validity
$(document).on('change', '#pass_validity', function(){
    changeEndDate();
});

//calculate end date from pass date & pass validity
function changeEndDate(){
    $pass_date = $('#pass_date').val();
    $pass_validity = $('#pass_validity :selected').val();
    var dt = new Date($pass_date);
    if(isValidDate(dt)){
        dt.setHours(parseInt(dt.getHours()) + parseInt($pass_validity));
        var formattedDate = formatAMPM(dt);
        $('#end_date').val(formattedDate);
    }
}

//save address when map model close
$('.btn-save-map-address').on('click', function (e) {
    //getMarkerLocation();
    $('#lat').val(lat);
    $('#lng').val(lang);
    $('#description').val(formattedAddress);
    $('#map-modal').modal('hide');
});


// ++++++++++++++++++ End Pass +++++++++++++++++++
