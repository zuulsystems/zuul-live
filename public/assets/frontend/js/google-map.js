var map, infoWindow, marker, geocoder, latlng;
var myLatLng, myAddress, paddress, searchQuery = '', items = [], newAddress = false;
let lat,lang,formattedAddress;
function getGeoAddress(coords){
    var self = this;
    var geocoder = new google.maps.Geocoder;
    var latlng = coords;
    geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
            console.log(results[0].geometry.location.lat());
            console.log(results[0].geometry.location.lng());
            console.log(results[0].geometry.location.lng());
            if (results[0]) {
                paddress = results[0].formatted_address;
                lat = results[0].geometry.location.lat();
                lang = results[0].geometry.location.lng();
                formattedAddress = results[0].formatted_address;
                console.log(paddress);
            } else {
                console.log('No results found');
            }
        } else {
            console.log('Geocoder failed due to: ' + status);
        }
    });

}

function initMap(localatlong) {
    // alert(localatlong);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: localatlong
    });

    mmarker = new google.maps.Marker({
        position: localatlong,
        map: map,
        draggable:true,
        title: 'Hello World!'
    });




    // directionsDisplay.setMap(map);

    google.maps.event.addListener(mmarker,'dragend',function(event) {
        var lt = event.latLng.lat();
        var lg = event.latLng.lng();
        var coords = {lat: lt, lng: lg};

        getGeoAddress(coords)

    });

    var searchInput = document.getElementById('searchbar-input');
    // console.log("Search input", searchInput);
    var searchBox = new google.maps.places.SearchBox(searchInput);
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);




    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length === 0) {
            return;
        }

        // Clear out the old markers.
        mmarker.setMap(null);
        // markers.forEach(function(marker) {
        //   marker.setMap(null);
        // });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            // var icon = {
            //   url: place.icon,
            //   size: new google.maps.Size(71, 71),
            //   origin: new google.maps.Point(0, 0),
            //   anchor: new google.maps.Point(17, 34),
            //   scaledSize: new google.maps.Size(25, 25)
            // };



            // Create a marker for each place.
            mmarker = new google.maps.Marker({
                position: place.geometry.location,
                map: map,
                draggable:true,
                title: 'Hello World!'
            });

            // markers.push(new google.maps.Marker({
            //   map: self.map,
            //   icon: icon,
            //   title: place.name,
            //   position: place.geometry.location
            // }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });



}

initMap(new google.maps.LatLng(0, 0));
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function toggleBounce() {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}

function getFormattedAddress(latlng) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
            $('.pass_address').text(results[0]['formatted_address']);
        }
    });
}

function setAddress(address){
    getCoordsForGeoAddress(address).then( function(coords) {
        initMap(coords);
    }, function( err ) {
        getCoordsViaHTML5Navigator().then(function(coords){
            initMap(coords);
        }, function(err){
            $('#MapModal').modal('hide');
        })
    })
}
// getCoordsViaHTML5Navigator();
var input = document.getElementById('searchbar-input');
var autocomplete = new google.maps.places.Autocomplete(input);


autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();
    var place_address = place.formatted_address;
    console.log(place.geometry.location.lat);
    var length= place.address_components.length;
    // $("#city").val(place.vicinity);
    // $("#address").val(place_address);
    // $("#lat").val(place.geometry.location.lat);
    // console.log(place.geometry.location.lat());
    // $("#lng").val(place.geometry.location.lng);
    map.setCenter(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
    mmarker.setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
    lat = place.geometry.location.lng();
    lang = place.geometry.location.lng();
    formattedAddress = place.formatted_address;
    console.log(place.formatted_address);
    console.log(lat);
    console.log(lang);
});


function getCoordsForGeoAddress(address, _default = true  ){

    var self = this;
    return new Promise( function(resolve, reject) {
        var self = this;
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {


                    var loc = results[0].geometry.location
                    console.log(loc.lat(), loc.lng())
                    var lat = loc.lat();
                    var lng = loc.lng();
                    resolve({ lat: lat, lng: lng })

                } else {


                    //this.presentToast('No results found');
                    if(_default === false){
                        reject();
                    }else{
                        getCoordsViaHTML5Navigator().then( function(coords) {
                            resolve(coords)
                        }, function(err) {
                            reject();
                        })
                    }


                }
            } else {
                console.log("Geocode status: " + status);
                //self.presentFailureToast("ERROR")
                reject()
            }
        });
    })

}

function getCoordsViaHTML5Navigator(){

    return new Promise( function(resolve){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                resolve(pos)

            }, function() {
                resolve({lat: 51.5074, lng: 0.1278 });
            });
        } else {
            // Browser doesn't support Geolocation
            resolve({lat: 51.5074, lng: 0.1278 });
        }
    })


}

function getMarkerLocation(){


    var lt = this.mmarker.position.lat();
    var lg = this.mmarker.position.lng();
    var coords = {lat: lt, lng: lg};


    var self = this;
    var geocoder = new google.maps.Geocoder;
    var latlng = coords;
    geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
            console.log(results);
            if (results[0]) {

                paddress = results[0].formatted_address;

                if(newAddress === true){
                    var _addressComponets = results[0].address_components;
                    console.log(_addressComponets);
                    var countryObject = _addressComponets.filter( function(x){ x.types.includes("country")})[0];
                    var country = ( countryObject ) ? countryObject['long_name'] : "";
                    var stateObject = _addressComponets.filter( function(x){ x.types.includes("administrative_area_level_1")})[0];
                    var state = ( stateObject ) ? stateObject['long_name'] : "";
                    var cityObject = _addressComponets.filter( function(x){ x.types.includes("administrative_area_level_2")})[0];
                    var city = ( cityObject ) ? cityObject['long_name'] : "";
                    var streetObject = _addressComponets.filter( function(x){ x.types.includes("route") })[0];
                    var street = ( streetObject ) ? streetObject['long_name'] : "";

                    console.log(country);


                    var threepartAddress = {
                        "country" : country,
                        "state" : state,
                        "city"  : city,
                        "street" : street
                    };
                    var coords2 = {lat: lt, lng: lg, address: paddress, parts: threepartAddress};
                    closeModal(coords2);
                }else{

                    var coords3 = {lat: lt, lng: lg, address: paddress};
                    closeModal(coords3);

                }



            } else {
                self.utilityProvider.presentToast('No results found');
            }
        } else {
            self.utilityProvider.presentToast('Geocoder failed due to: ' + status);
        }
    });


}

function closeModal(coords){
    /*var newpassmodal = $('#PassesModal');
    if ($("#EditPassesModal").data('bs.modal') && $("#EditPassesModal").data('bs.modal').isShown){
        newpassmodal = $('#EditPassesModal');
    }*/
    console.log(coords);
    $('#lat').val(coords['lat']);
    $('#lng').val(coords['lng']);
    $('#description').val(coords['address']);
}
