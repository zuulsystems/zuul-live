function changePhoneDigitsFormat() {
    var dialCode = $('.dial_code').val();

    $.ajax({
        url:"{{ route('get-phone-format-by-dial-code') }}",
        type:"POST",
        data:{dial_code:dialCode},
        dataType:"JSON",
        success:function (result) {
            if(result.countryPhoneFormat != null)
            {
                $('#phone_number').inputmask({ mask: result.countryPhoneFormat.format});
            }
            else
            {
                $('#phone_number').inputmask({ mask: '999-999-9999'});
            }
        }
    });

}

function changePhoneDigitsFormat1(url, isResidentPage = false) {
    var dialCode = $('#dial_code').val();

    $.ajax({
        url:url,
        type:"POST",
        data:{dial_code:dialCode},
        dataType:"JSON",
        success:function (result) {
            if(result.countryPhoneFormat != null)
            {
                $('#phone_number').inputmask({ mask: result.countryPhoneFormat.format});

                if(isResidentPage)
                {
                    $('.additional_phone_number1').inputmask({ mask: result.countryPhoneFormat.format});
                    $('.additional_phone_number2').inputmask({ mask: result.countryPhoneFormat.format});
                    $('.additional_phone_number3').inputmask({ mask: result.countryPhoneFormat.format});
                }

            }
            else
            {
                $('#phone_number').inputmask({ mask: '999-999-9999'});

                if(isResidentPage)
                {
                    $('.additional_phone_number1').inputmask({ mask: '999-999-9999'});
                    $('.additional_phone_number2').inputmask({ mask: '999-999-9999'});
                    $('.additional_phone_number3').inputmask({ mask: '999-999-9999'});
                }
            }
        }
    });

}

function changePhoneDigitsFormatofAdditionalPhone($dialCode, $url, aditionalPhoneNumberId) {
    var dialCode = $dialCode;

    $.ajax({
        url:$url,
        type:"POST",
        data:{dial_code:dialCode},
        dataType:"JSON",
        success:function (result) {
            let additionalPhoneNumberId = "#"+aditionalPhoneNumberId;
            // alert(additionalPhoneNumberId);
            if(result.countryPhoneFormat != null)
            {
                $(additionalPhoneNumberId).inputmask({ mask: result.countryPhoneFormat.format});
            }
            else
            {
                $(additionalPhoneNumberId).inputmask({ mask: '999-999-9999'});
            }
        }
    });

}

