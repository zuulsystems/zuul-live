@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Groups / Add</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('contact-groups.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Groups
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('contact-groups.store')}}"
                          class="ajax-form" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="group_name">Group Name</label>
                                        <input type="text" name="group_name" id="group_name" placeholder="Group Name"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea type="text" name="description" id="description"
                                                  placeholder="Description"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Add </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        //show or hide contact duration dropdown
        $('#is_temporary').change(function () {
            if ($(this).prop("checked") === true) {
                console.log("Checkbox is checked.");
                $("#temporary-contact-div").show();
            } else if ($(this).prop("checked") === false) {
                $("#temporary-contact-div").hide();
            }
        });
    </script>
@endpush
