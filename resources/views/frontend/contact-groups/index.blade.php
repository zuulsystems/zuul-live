@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Manage Groups</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{route('contact-groups.create')}}" class="add_advertiser">
                            <i class="fas fa-plus-circle"></i> Add Group
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div id="accordion" class="result"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('contact-groups-data')}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let contact_groups = result.data;
                    let html = '';
                    if (contact_groups.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(contact_groups, function (ket, row) {
                            html += `
                                <div class="card">
                                <div class="card-header" id="headingOne">
                                    <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne_${row.id}" aria-expanded="true" aria-controls="collapseOne_${row.id}">
                                                ${row.group_name} (${row.user_contact.length ?? ''})
                                            </button>
                                        </h5>
                                    </div>
                                    <div class="col-md-6">
                                         <div class="dropdown text-right">
                                              <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                                                aria-expanded="true">
                                            <i class="fa fa-sliders" aria-hidden="true"></i></a>
                                                 <div class="dropdown-menu">
                                            <a class="dropdown-item " href="${baseUrl('contact-groups/' + row.id + '/edit')}" ><i class="fa fa-pen" aria-hidden="true"></i> Edit</a>
                                            <form action="${baseUrl('contact-groups/' + row.id)}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="_method" value="DELETE">
                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                </div>

                                <div id="collapseOne_${row.id}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">`;
                            if (row.user_contact.length > 0) {
                                $.each(row.user_contact, function (ket, contact) {
                                    html += `
                                                    <div class="col-md-4 pt-3">
                                            <div class="card p-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3>${contact.contact_name ?? ''}</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                   ${contact.formatted_phone_number}</b>
                                                </div>
                                                <div class="col-md-2">
 <form action="${baseUrl('group-contact-delete')}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="contactGroupId" value="${row.id}">
                                                 <input type="hidden" name="contactId" value="${contact.id}">

                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </form>
                                                </div>
                                            </div>
                                        </div>
                                          </div>
                                                `;
                                });
                            } else {
                                html += `
                                                    <div class="col-md-4 pt-3">
                                                         <div class="card p-3">
                                                            <h5>No Contacts !</h5>
                                                         </div>
                                                    </div>
                                                `;
                            }

                            html += `</div>
                                    </div>
                                </div>
                            </div>
                        `;
                        });
                        if (reset === true) {
                            $('.result').empty().append(html)
                        } else {
                            $('.result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
