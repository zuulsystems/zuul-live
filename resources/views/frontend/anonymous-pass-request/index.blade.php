<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zuul Systems Admin | Register</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body class="hold-transition login-page">
{{--<div class="login-box" style="width: 460px">--}}
<div class="login-box">
    <div class="login-logo">
        <a href="{{route('login')}}"><b>Zuul Systems</b></a>
    </div>

    <!-- /.login-logo -->
    <div class="card">

        <div class="card-body login-card-body">
            <h4 class="login-box-msg">Request A Pass</h4>

            @if(session()->has('success'))
                <div class="alert alert-success">{{session()->get('success')}}</div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">{{session()->get('error')}}</div>
            @endif
            <form action="{{route('anonymous-request-a-pass-submit')}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="full_name" value="{{old('full_name')}}"
                           placeholder="Your Full Name">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    @error('full_name')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" data-inputmask='"mask": "(999)-999-9999"' data-mask
                           name="phone_number" placeholder="Your Phone Number" value="{{old('phone_number')}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-phone"></span>
                        </div>
                    </div>
                    @error('phone_number')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" data-inputmask='"mask": "(999)-999-9999"' data-mask
                           name="resident_phone_number" placeholder="Resident Phone Number"
                           value="{{old('resident_phone_number')}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-phone"></span>
                        </div>
                    </div>
                    @error('resident_phone_number')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="comment" placeholder="Provide Additional Details"
                           value="{{old('comment')}}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-comment"></span>
                        </div>
                    </div>
                    @error('comment')
                    <span class="invalid-feedback d-block" role="alert">
                          <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group text-center" style="margin:10px;">
                    <button type="submit" class="btn btn-primary btn-block">Request Now</button>
                </div>

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p>Not A Member? <a href="{{route('register')}}">Create Account</a></p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </form>

        </div>

        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $(document).ready(function (e) {
        $('[data-mask]').inputmask();
    });
</script>
</body>
</html>
