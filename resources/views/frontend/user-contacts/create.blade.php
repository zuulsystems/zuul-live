@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Contacts / Add</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('contacts.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Contacts
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('contacts.store')}}"
                          class="ajax-form" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="is_temporary">Temporary Contact</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-right">
                                        <input type="checkbox" name="is_temporary" id="is_temporary" value="1"
                                               data-toggle="toggle" data-offstyle="light">
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="temporary-contact-div" style="display: none">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="temporary_duration">Contact Duration</label>
                                        <select name="temporary_duration" id="temporary_duration" class="form-control">
                                            <option value="6" selected="">6 Hours</option>
                                            <option value="12">12 Hours</option>
                                            <option value="24">24 Hours</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="contact_name">Member Name</label>
                                        <input type="text" name="contact_name" id="contact_name"
                                               placeholder="Member Name"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="email" placeholder="Email"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dial_code">Country code</label>
                                        <select name="dial_code" id="dial_code" class="form-control">
                                            @foreach($countries as $country)
                                                <option value="+{{$country->dial_code}}">{{$country->country_name}}
                                                    +{{$country->dial_code}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number</label>
                                        <input type="text" name="phone_number" class="form-control" id="phone_number"
                                               placeholder="Phone Number"/>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Add </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        //show or hide contact duration dropdown
        $('#is_temporary').change(function () {
            if ($(this).prop("checked") === true) {
                console.log("Checkbox is checked.");
                $("#temporary-contact-div").show();
            } else if ($(this).prop("checked") === false) {
                $("#temporary-contact-div").hide();
            }
        });

        $(document).ready(function () {

            var $formattedPhone = '999-999-9999';

            $('#phone_number').inputmask({mask: $formattedPhone});

            $('#dial_code').change(function () {

                $.ajax({
                    url: "{{ route("get-phone-format-by-dial-code") }}",
                    type: "POST",
                    data: {dial_code: $(this).val()},
                    dataType: "JSON",
                    success: function (result) {
                        if (result.countryPhoneFormat != null) {
                            $('#phone_number').inputmask({mask: result.countryPhoneFormat.format});
                        } else {
                            $('#phone_number').inputmask({mask: '999-999-9999'});
                        }
                    }
                });

            });
        });

    </script>
@endpush
