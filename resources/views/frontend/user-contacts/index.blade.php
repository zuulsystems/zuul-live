@extends('frontend.layout.master')
@section('content')
    <style>
        .pac-container {
            z-index: 999999;
        }
    </style>
    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / Manage Contacts</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{route('contacts.create')}}">
                            <i class="fas fa-plus-circle"></i> Add Contact
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row" id="result">
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Add Contact in Group Model--->
    <div class="modal fade" id="add-group-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Group</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('add-contact-in-group')}}"
                      class="ajax-form" id="guard-permission-form">
                    <input type="hidden" name="contact_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="group_id">Assign Group </label>
                                    <select name="group_id" id="group_id" class="form-control">
                                        <option value="">Select Group</option>
                                        @foreach($contactGroups as $contactGroup)
                                            <option
                                                value="{{$contactGroup->id}}">{{$contactGroup->group_name}}  </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Sent Request A Pass Model--->
    <div class="modal fade" id="send-request-pass-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Request A Pass</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('send-pass-request-from-contact')}}"
                      class="ajax-form" enctype="multipart/form-data">
                    <input type="hidden" name="user_contact_id">
                    <div class="modal-body">

                        <div class="row" id="contacts_div">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="contact_name">Contact</label>
                                    <input type="text" readonly name="contact_name" id="contact_name"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Comments (optional)</label>
                                    <p class="text-gray">Enter some details or reason for pass request as the recipient
                                        may not know who you are</p>
                                    <textarea name="description" class="form-control pass_address"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <!----> <span>Add </span>
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Pass Model--->
    @include('frontend.passes.models.send-pass-model')

    <!-- Map Model--->
    @include('frontend.passes.models.pass-location-map-model')

@endsection
@push('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA&libraries=places"></script>
    <script src="{{asset('assets/frontend/js/google-map.js')}}"></script>
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

            //get more records on button click
            $(document).on('click', '.btn-add-group', function () {
                let contact_id = $(this).data('id');
                let addGroupModal = $('#add-group-modal');
                addGroupModal.find($('input[name="contact_id"]')).val(contact_id);
                addGroupModal.modal('show');
            });

            //send request a pass model
            $(document).on('click', '.send-request-pass', function () {
                let contact_id = $(this).data('id');
                let contact_name = $(this).data('name');
                let sendRequestPassModal = $('#send-request-pass-modal');
                sendRequestPassModal.find($('input[name="user_contact_id"]')).val("");
                sendRequestPassModal.find($('input[name="contact_name"]')).val("");
                sendRequestPassModal.find($('input[name="user_contact_id"]')).val(contact_id);
                sendRequestPassModal.find($('input[name="contact_name"]')).val(contact_name);
                sendRequestPassModal.modal('show');
            });

            //send pass a pass model
            $(document).on('click', '.send-pass', function () {
                let contact_id = $(this).data('id');
                let contact_name = $(this).data('name');
                let sendPassModal = $('#send-pass-modal');
                sendPassModal.find($('#selected_contacts')).val(contact_id);
                sendPassModal.find($('#selected_contacts')).html(
                    `<option value="${contact_id}" selected>${contact_name}</option>`
                );
                sendPassModal.modal('show');
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('contacts-data')}}?type={{$type ?? null}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let contacts = result.data;
                    let html = '';
                    if (contacts.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(contacts, function (ket, row) {
                            html += `
                          <div class="col-md-4 pt-3">
                            <div class="card p-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>${row.contact_name ?? ''}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                   ${row.formatted_phone_number}
                                </div>
                                <div class="col-md-2">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                                           aria-expanded="true">
                                            <i class="fa fa-sliders" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item " href="${baseUrl('contacts/' + row.id + '/edit')}" ><i class="fa fa-pen" aria-hidden="true"></i> Edit</a>
                                            <form action="${baseUrl('contacts/' + row.id)}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="_method" value="DELETE">
                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                            <a class="dropdown-item btn-add-group" data-id="${row.id}" href="javascript:void(0)" ><i class="fa fa-users" aria-hidden="true"></i> Add To Group</a>
                                            <a class="dropdown-item send-request-pass" data-id="${row.id}" data-name="${row.contact_name ?? ''}" href="javascript:void(0)" ><i class="fa fa-ticket-alt" aria-hidden="true"></i> Request A Pass</a>
                                            <a class="dropdown-item send-pass" data-id="${row.id}" data-name="${row.contact_name ?? ''}" href="javascript:void(0)" ><i class="fa fa-ticket-alt" aria-hidden="true"></i> Send A Pass</a>
`;

                            let type = "{{ $type ?? '' }}";

                            html += `<form action="${baseUrl('add-or-remove-favourite-contact')}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="id" value="${row.id}">
                                                 <input type="hidden" name="is_favourite" value="${row.is_favourite}">
                                                 <button type="submit" class="dropdown-item">
                                                 <input type="hidden" name="type" value="${type}">`;
                            if (row.is_favourite == '1') {
                                html += `<i class="fa fa-user-minus" aria-hidden="true"></i> Remove from Favorites`;
                            } else {
                                html += `<i class="fa fa-user-plus" aria-hidden="true"></i> Add to Favorites`;
                            }
                            html += `</button></form>`;
                            html += `</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
