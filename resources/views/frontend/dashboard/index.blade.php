@extends('frontend.layout.master')
@section('content')
    <!-- Passes CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/passes.css')}}">

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>

        <div class="w-100 mb-3 passes-header-bar">
            <h5 class="text-white font-weight-normal mb-0">
                <i class="fa fa-ticket-alt"></i>
                Passes
            </h5>
        </div>
        <!-- 3 Cards Row -->
        <div class="row">


        </div>
        <div class="row" id="result">
        </div>
        <!-- Pass Detail Model--->
        @include('frontend.passes.models.pass-detail-model')
    </div>
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore(1, 'active');
            loadMore(1, 'scanned');
            loadMore(1, 'sent');
            //pass actions / pass detail
            $(document).on('click', '.pass-action', function () {
                let passId = $(this).data('pass-id');
                getPassDetail(passId);
            });

        });

        //load more data by page number
        function loadMore(page = 1, type) {
            showLoader();
            $.ajax({
                url: "{{route('get-passes-by-type')}}",
                data: {
                    type: type
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let archive_passes = result.data;
                    let html = '';
                    let qr_code_image_url = "{{asset("assets/frontend/img/qr.jpg")}}";
                    let default_user_image_url = "{{asset('assets/frontend/img/default-user.png')}}";
                    let active_passes_link = "{{route('passes.active-passes.index')}}";
                    let archived_passes_link = "{{route('passes.archive-passes.index')}}";
                    let sent_passes_link = "{{route('passes.sent-passes.index')}}";
                    if (archive_passes.length > 0) {
                        $('#btn-load-more').data('page', page);
                        //received passes html
                        if (type == 'active') {
                            //populate passes data
                            $.each(archive_passes, function (ket, row) {
                                html = `
                                 <div class="col-md-6 col-lg-4 mb-4">
                <div class="card active-pass-container shadow h-100">
                    <div class="card-header d-flex justify-content-between">
                        <div>
                            <i class="fa fa-check-circle"></i> Active Passes
                        </div>
                        <div class="view-all">
                            <a href="${active_passes_link}">View All</a>
                        </div>
                    </div>
                    <div class="card-body">
                         <div class="pass pass-active pass-inside-div pass-action" data-id="${row.id}" data-pass-id="${row.pass_id}" style="width:100%;cursor: pointer; ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${row.sender_profile_image}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Sender: ${row.sender_name}</h5>
                                                <h6>Community Name<span>${row.sender_community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                `;
                            });
                        }
                        //sent passes
                        else if (type == 'sent') {
                            //populate passes data
                            $.each(archive_passes, function (ket, row) {
                                html = `
                                         <div class="col-md-6 col-lg-4 mb-4">
                        <div class="card sent-pass-container shadow h-100">
                            <div class="card-header d-flex justify-content-between">
                                <div>
                                    <i class="fa fa-check-circle"></i> Sent Passes
                                </div>
                                <div class="view-all">
                                    <a href="${sent_passes_link}">View All</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="pass pass-sent pass-inside-div" style="width:100%;cursor: pointer; ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${default_user_image_url}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Receiver: ${row.username}</h5>
                                                <h6>Community Name<span>${row.community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                                `;
                            });
                        }
                        //archived passes
                        else if (type == 'scanned') {
                            //populate passes data
                            $.each(archive_passes, function (ket, row) {
                                html = `
                                         <div class="col-md-6 col-lg-4 mb-4">
                        <div class="card scanned-pass-container shadow h-100">
                            <div class="card-header d-flex justify-content-between">
                                <div>
                                    <i class="fa fa-check-circle"></i> Scanned Passes
                                </div>
                                <div class="view-all">
                                    <a href="${archived_passes_link}">View All</a>
                                </div>
                            </div>
                            <div class="card-body">
                                 <div class="pass pass-scanned pass-inside-div pass-action" data-id="${row.id}" data-pass-id="${row.pass_id}" style="width:100%;cursor: pointer; ">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                            <div class="pass-lft">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <img src="${row.sender_profile_image}"
                                                             alt="pass-img" class="media-object" style="width:60px">
                                                    </div>
                                                    <div class="pass-body">
                                                        <h4 class="media-heading">${row.event_name}</h4>
                                                        <h5>Sender: ${row.sender_name}</h5>
                                                        <h6>Community Name<span>${row.sender_community_name}</span></h6>

                                                    </div>
                                                </div>
                                                <p>${row.description}</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="pass-ryt">
                                                <h3>${row.pass_start_date}</h3>
                                                <h4>${row.pass_validity}</h4>
                                                <div class="qr"><img src="${qr_code_image_url}"
                                                                     class="img-responsive" alt="QR Code"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                `;

                            });
                        }
                        $('#result').append(html);

                    } else {
                    }
                }
            });
        }

    </script>
@endpush
