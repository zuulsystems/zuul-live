@extends('frontend.layout.master')
@section('content')
    <link rel="stylesheet" href="{{asset('assets/frontend/intl-tel-Input/css/intlTelInput.css')}}">
    <style>
    </style>
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Account Settings</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('account-setting.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Manage Account / Settings
                </a>
            </div>
            <div class="card-body">
                @if(session()->has('success'))
                    <div class="alert alert-success">{{session()->get('success')}}</div>
                @endif
                <div class="card-body">
                    <form role="form" method="post" action="{{route('account-setting.update',[auth()->user()->id])}}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            <img src="{{ auth()->user()->ProfileImageUrl }}" class="img-responsive"
                                 style="max-width: 100px">
                            <div class="form-group">
                                <label for="profile_image">Profile Image </label>
                                <input type="file" name="profile_image" id="profile_image" class="form-control">
                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone">Phone </label>

                                        @php
                                            $dataMaskValue = '';
                                            if(auth()->user()->dial_code == "")
                                            {
                                            $dataMaskValue = '"mask": "(999) 999-9999"';
                                            }
                                            elseif(auth()->user()->dial_code == "1")
                                            {
                                            $dataMaskValue = '"mask": "(999) 999-9999"';
                                            }
                                            elseif(auth()->user()->dial_code == "504")
                                            {
                                            $dataMaskValue = '"mask": "9999-9999"';
                                            }
                                        @endphp

                                        <input type="text" id="phone1"
                                               value="{{ auth()->user()->phone_number }}" class="form-control" disabled>
                                        <input type="hidden" name="dial_code" id="dial_code"
                                               value="{{auth()->user()->dial_code}}">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="firstName">First Name </label>
                                <input type="text" name="first_name" id="firstName"
                                       value="{{ auth()->user()->first_name }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="lastName">Last Name </label>
                                <input type="text" name="last_name" id="lastName"
                                       value="{{ auth()->user()->last_name }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="email">Email </label>
                                <input type="email" name="email" id="email"
                                       value="{{ auth()->user()->email }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="street_address">Street Address </label>
                                <textarea class="form-control" name="street_address"
                                          id="street_address">{{ auth()->user()->street_address }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Apartment </label>
                                <input type="text" name="apartment" id="apartment"
                                       placeholder="Apartment" class="form-control"
                                       value="{{ auth()->user()->apartment }}">
                            </div>
                            <div class="form-group">
                                <label for="city">City </label>
                                <input type="text" name="city" id="city"
                                       placeholder="City" class="form-control" value="{{ auth()->user()->city }}">
                            </div>
                            <div class="form-group">
                                <label for="state">State </label>
                                <input type="text" name="state" id="state"
                                       placeholder="State" class="form-control" value="{{ auth()->user()->state }}">
                            </div>
                            <div class="form-group">
                                <label for="zip_code">Zip code </label>
                                <input type="text" name="zip_code" id="zip_code"
                                       placeholder="Zip code" class="form-control"
                                       value="{{ auth()->user()->zip_code }}">
                            </div>
                            {{--end fields--}}
                            <div class="form-group">
                                <label for="licence_type">License Type</label>
                                <select class="form-control" name="license_type">
                                    <option
                                        value="driving_license" {{ auth()->user()->license_type == "driving_license" ? 'selected':'' }}>
                                        Driver's License
                                    </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="license_image">License Image</label>
                                @if(empty(auth()->user()->license_image) && !auth()->user()->hasPermission('is_license_locked'))
                                    <input type="file" name="license_image" id="license_image">
                                @endif
                                <input type="hidden" name="old_license_image" value="{{auth()->user()->license_image}}">
                            </div>
                            <img src="{{ auth()->user()->LicenseImageUrl }}" class="img-responsive"
                                 style="max-width: 300px">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/frontend/intl-tel-Input/js/intlTelInput.js')}}"></script>
@endsection
@push('scripts')
    <script>
        let phone1 = document.querySelector("#phone1");

        var $phoneFormat = "{{ $phoneFormat }}";

        $('#phone1').inputmask({mask: $phoneFormat})

        let dial_code = $('#dial_code').val();
        $(document).ready(function () {
            //initialize intlTelInput
            let iti = window.intlTelInput(phone1, {
                separateDialCode: true,
                utilsScript: "{{asset('assets/frontend/intl-tel-Input/js/utils.js')}}",
            });
            //set dynamic number
            iti.setNumber(phone1.value);

            //add onchange for selected country code
            phone1.addEventListener("countrychange", function () {
                let data = iti.getSelectedCountryData();
                let dialCode = data.dialCode;
                if (dialCode != undefined) {
                    dial_code = dialCode;//set in dialCode for step 2
                    $('#dial_code').val("+" + dialCode);
                }
            });
        });

    </script>
@endpush

