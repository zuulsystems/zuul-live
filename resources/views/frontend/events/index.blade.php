@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Events</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{route('events.create')}}" class="add_advertiser">
                            <i class="fas fa-plus-circle"></i> Add Event
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row" id="result">
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('events-data')}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let events = result.data.data;
                    let html = '';
                    if (events.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(events, function (ket, row) {
                            html += `
                          <div class="col-md-4 pt-3">
                            <div class="card p-3">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3>${row.name}</h3>
                                </div>`;

                            if ({{ auth()->user()->id }} != row.created_by) {
                                html += `</div></div></div>`;
                            } else {
                                html += `  <div class="col-md-2">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                                           aria-expanded="true">
                                            <i class="fa fa-sliders" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item " href="${baseUrl('events/' + row.id + '/edit')}" ><i class="fa fa-pen" aria-hidden="true"></i> Edit</a>
                                            <form action="${baseUrl('events/' + row.id)}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="_method" value="DELETE">
                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>`;
                            }
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
