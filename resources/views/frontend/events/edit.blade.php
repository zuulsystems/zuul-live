@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Edit Event</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('events.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Manage Event
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{ route('events.update', [$event->id]) }}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Event Name </label>
                                <input type="text" name="name" id="name"
                                       class="form-control" value="{{$event->name}}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save Changes </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

