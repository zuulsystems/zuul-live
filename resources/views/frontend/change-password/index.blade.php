@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Change Password</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('change-password.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Manage Password
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{ route('change-password.update',[auth()->user()->id]) }}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="password">Current Password </label>
                                <input type="text" name="password" id="password"
                                       placeholder="Current Password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="newPassword">New Password </label>
                                <input type="text" name="newPassword" id="newPassword"
                                       placeholder="New Password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="reEnterPassword">Re-enter Password </label>
                                <input type="text" name="reEnterPassword" id="reEnterPassword"
                                       placeholder="Re-enter Password" class="form-control">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Submit </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

