<html>
<head>
    <style>    body {
            margin: 0;
            padding: 0;
            height: 100vh;
            font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: #eee;
        }

        .card {
            background-color: white;
            padding: 10px;
            border-radius: 20px;
            width: 1000px;
            height: 2200px;
        }

        .card img {
            width: 100%;
            border-radius: 15px;
        }

        .card-content {
            text-align: center;
            padding: 0 16px;
        }

        .card-content {
            color: darkblue;
        }

        .card-content p {
            color: #626262;
        }

        h3, h4 {
            font-size: -webkit-xxx-large;
        }
    </style>
</head>
<body>
<div class="card">
    <br>
    <h3 style="color:forestgreen;font-size: 40px !important;transform: translate(80px, 100px);">AFTER ONE USE, PASS NO
        LONGER VALID</h3>

    <img src="http://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{$pass_data[0]['qr_code']}}" width="300" alt="">
    <div class="card-content">

        <h4><?php echo $pass_data[0]->pass->createdBy->community != '' ? $pass_data[0]->pass->createdBy->community->name : '' ?> </h4>
        <h3>
            Resident: <?php echo $pass_data[0]->pass->createdBy->first_name . " " . $pass_data[0]->pass->createdBy->last_name ?></h3>
        <?php echo $pass_data[0]->user->vehicle != '' ? "<h3>Vehicle:" . $pass_data[0]->user->vehicle->license_plate . "</h3>" : '' ?>
        <h3>Guest: <?php echo $pass_data[0]->user->first_name . " " . $pass_data[0]->user->last_name?></h3>
        <h3>Expiration: <?php echo date('Y-m-d h:i a', strtotime($pass_data[0]->pass->pass_date));?></h3><br>
        <h3 style="color:red;font-size: 40px !important;">PLEASE OBEY ALL POSTED ROAD SIGNS AND SPEED LIMITS</h3>

    </div>
</div>
</body>
</html>
