@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / My Passes / Pass Request </p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{route('passes.active-passes.create')}}">
                            <i class="fas fa-plus-circle"></i> Create Pass
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row" id="result">
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            // loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

            //get more records on button click
            $(document).on('click', '.btn-add-group', function () {
                let contact_id = $(this).data('id');
                $('input[name="contact_id"]').val(contact_id);
                $('#add-group-modal').modal('show');
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('contacts-data')}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let contacts = result.data;
                    let html = '';
                    if (contacts.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(contacts, function (ket, row) {
                            html += `
                          <div class="col-md-4 pt-3">
                            <div class="card p-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>${row.contact_name ?? ''}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                   ${row.formatted_phone_number}
                                </div>
                                <div class="col-md-2">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                                           aria-expanded="true">
                                            <i class="fa fa-sliders" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item " href="${baseUrl('contacts/' + row.id + '/edit')}" ><i class="fa fa-pen" aria-hidden="true"></i> Edit</a>
                                            <form action="${baseUrl('contacts/' + row.id)}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="_method" value="DELETE">
                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                            <a class="dropdown-item btn-add-group" data-id="${row.id}" href="javascript:void(0)" ><i class="fa fa-users" aria-hidden="true"></i> Add To Group</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
