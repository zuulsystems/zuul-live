<html>
<head>
    <style>    body {
            margin: 0;
            padding: 0;
            height: 100vh;
            font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: #eee;
        }

        .card {
            background-color: white;
            padding: 10px;
            border-radius: 20px;
            width: 1000px;
            height: 2200px;
        }

        .card img {
            width: 100%;
            border-radius: 15px;
        }

        .card-content {
            text-align: center;
            padding: 0 16px;
        }

        .card-content {
            color: darkblue;
        }

        .card-content p {
            color: #626262;
        }

        h3, h4 {
            font-size: -webkit-xxx-large;
        }
    </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body>

<div class="card " id="qr-card">

</div>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

<script type="text/javascript">
    $(window).on('load', function () {
        let phone = prompt("Please enter your cell phone number");

        // if(phone != null && phone.length == 10){
        if (phone != null) {
            $.ajax({
                url: "../../../qr_code/group/data/" + phone,
                type: "GET",
                success: function (data) {
                    console.log(data);
                    if (data == 0) {
                        alert("NO DATA")
                    } else {

                        var e = $(`<br>
                <h3 style="color:forestgreen;font-size: 40px !important;transform: translate(80px, 100px);">AFTER ONE USE, PASS NO LONGER VALID</h3>

                <img src="http://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=` + data['qr_code'] + `" width="300" alt="">
                    <div class="card-content">

                        <h4>` + data['community_name'] + `</h4>
                        <h3>Resident: ` + data['resident_name'] + `</h3>
                        <h3>Guest: ` + data['guest_name'] + `</h3>
                        <h3>Expiration:` + data['expiry'] + `</h3><br>
                        <h3 style="color:red;font-size: 40px !important;">PLEASE OBEY ALL POSTED ROAD SIGNS AND SPEED LIMITS</h3>

                    </div>`);
                        $('#qr-card').append(e);
                    }
                }
            });


        } else {
            alert('Please enter valid phone!')
        }

    });
</script>
</html>
