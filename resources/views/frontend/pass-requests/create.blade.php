@extends('frontend.layout.master')
@section('content')
    <style>
        .pac-container {
            z-index: 1111;
        }
    </style>
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Pass Request / Add</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('passes.active-passes.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Passes
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('passes.pass-requests.store')}}"
                          class="ajax-form" enctype="multipart/form-data">
                        <div class="card-body">

                            <div class="row" id="contacts_div">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="requested_user_id">Contact</label>
                                        <select name="requested_user_id" id="requested_user_id" class="form-control">
                                            @forelse($userContacts as $userContact)
                                                <option
                                                    value="{{$userContact->contact->user->id}}">{{$userContact->contact_name}}</option>
                                            @empty
                                                <option value="">No Contacts</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Comments (optional)</label>
                                        <p class="text-gray">Enter some details or reason for pass request as the
                                            recipient may not know who you are</p>
                                        <textarea name="description" id="description"
                                                  class="form-control pass_address"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Add </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <!-- Add Contact in Group Model--->
    <div class="modal fade" id="map-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Location</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="searchbar-input">Search Address </label>
                                <input type="text" name="searchbar-input" class="form-control" id="searchbar-input"
                                       placeholder="Search Address"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="map" style="width:100%; height:25vh;"></div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-save-map-address">Save</button>
                    </div>
                    {{-- </form>--}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        @endsection
        @push('scripts')

            <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA&libraries=places"></script>
            <script src="{{asset('assets/frontend/js/google-map.js')}}"></script>
            <script>
                //get pass type
                $('#pass_type').change(function () {
                    let pass_type = $(this).val();
                    let validityOptions = '';
                    console.log(pass_type);
                    //check if pass type => self hide contact list
                    //set validity options by pass type
                    if (pass_type === 'self') {
                        validityOptions = `
                <option value="3">3 Hours</option>
                <option value="6">6 Hours</option>
                <option value="12">12 Hours</option>
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                `;
                        $('#contacts_div').hide();
                        $('#end_date_div').show();

                    } else if (pass_type === 'recurring') {
                        validityOptions = `
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                <option value="168">1 Week</option>
                <option value="336">2 Weeks</option>
                <option value="720">1 Month</option>
                <option value="876000">No Limit</option>
                `;
                        $('#end_date_div').hide();
                    } else {
                        validityOptions = `
                <option value="3">3 Hours</option>
                <option value="6">6 Hours</option>
                <option value="12">12 Hours</option>
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                `;
                        $('#contacts_div').show();
                        $('#end_date_div').show();
                    }
                    $('#pass_validity').html(validityOptions);
                });

                //update end data when update pass date form data picker
                $(document).on('change', '#pass_date', function () {
                    if ($(this).val().length > 0) {
                        changeEndDate();
                    } else {
                        $('#end_date').val('');
                    }
                });

                //update end date from pass validity
                $(document).on('change', '#pass_validity', function () {
                    changeEndDate();
                });

                //calculate end date from pass date & pass validity
                function changeEndDate() {
                    $pass_date = $('#pass_date').val();
                    $pass_validity = $('#pass_validity :selected').val();
                    var dt = new Date($pass_date);
                    if (isValidDate(dt)) {
                        dt.setHours(parseInt(dt.getHours()) + parseInt($pass_validity));
                        var formattedDate = formatAMPM(dt);
                        $('#end_date').val(formattedDate);
                    }
                }

                //save address when map model close
                $('.btn-save-map-address').on('click', function (e) {
                    //getMarkerLocation();
                    $('#lat').val(lat);
                    $('#lng').val(lang);
                    $('#description').val(formattedAddress);
                    $('#map-modal').modal('hide');
                })

            </script>
    @endpush
