@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Parental Control </p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="#" class="add_advertiser">
                    <i class="fas fa-list"></i> Parental Control
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('parental-control.update',[$member_id])}}"
                          enctype="multipart/form-data" class="ajax-form">
                        @method('PUT')
                        <div class="card-body">
                            <input type="hidden" name="parental_control_id"
                                   value="{{ $parentalControlValues->id ?? '' }}">
                            <input type="hidden" name="member_id" value="{{ $member_id ?? '' }}">
                            @foreach($options as $option)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            @php
                                                $label = str_replace('_',' ',$option);
                                                $label = ucfirst($label);
                                            @endphp
                                            <label for="{{ $option }}">{{ $label }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group text-left">
                                            <input type="checkbox" name="{{ $option }}" id="{{ $option }}"
                                                   {{ $parentalControlValues->$option == 1 ? 'checked' : '' }} data-toggle="toggle"
                                                   data-offstyle="left">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>
    </script>
@endpush
