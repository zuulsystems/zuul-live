@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Pre-Approved Guests / Edit</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('pre-approved.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Pre-Approved Guest
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('pre-approved.update',$userContact->id)}}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            <div class="row" id="temporary-contact-div" style="display: none">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="temporary_duration">Contact Duration</label>
                                        <select name="temporary_duration" id="temporary_duration" class="form-control">
                                            <option value="6" selected="">6 Hours</option>
                                            <option value="12">12 Hours</option>
                                            <option value="24">24 Hours</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact_name">Contact Name</label>
                                        <input type="text" name="contact_name" id="contact_name"
                                               placeholder="Contact Name"
                                               class="form-control" value="{{ $userContact->contact_name }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Shared</label>
                                        <select class="form-control" name="is_dnc_shared">
                                            <option
                                                value="0" {{ $userContact->is_dnc_shared == "0" ? "selected" : "" }}>No
                                            </option>
                                            <option
                                                value="1" {{ $userContact->is_dnc_shared == "1" ? "selected" : "" }}>Yes
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dial_code">Country code</label>
                                        <select name="dial_code" class="form-control dial_code"
                                                onchange="changePhoneDigitsFormat()">
                                            @foreach($countries as $country)
                                                <option
                                                    value="{{$country->dial_code}}" {{ $country->dial_code == $userContact->contact->dial_code ? 'selected' : ''  }}>{{$country->country_name}}
                                                    +{{$country->dial_code}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number</label>

                                        <input type="text" name="phone_number" class="form-control"
                                               placeholder="Phone Number"
                                               value="{{ $userContact->contact->phone_number }}" id="phone_number"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>


        $(document).ready(function () {
            var $phoneFormat = "{{ $phoneFormat }}";
            $('#phone_number').inputmask({mask: $phoneFormat})

            $('.dial_code').change(function () {

                $.ajax({
                    url: "{{ route("get-phone-format-by-dial-code") }}",
                    type: "POST",
                    data: {dial_code: $(this).val()},
                    dataType: "JSON",
                    success: function (result) {
                        if (result.countryPhoneFormat != null) {
                            $('#phone_number').inputmask({mask: result.countryPhoneFormat.format});
                        } else {
                            $('#phone_number').inputmask({mask: '999-999-9999'});
                        }
                    }
                });

            });

        });
    </script>
@endpush
