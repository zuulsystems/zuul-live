@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / Pre-Approved Guests</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{route('pre-approved.create')}}" class="add_advertiser">
                            <i class="fas fa-plus-circle"></i> Add Pre-Approved Guest
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row" id="result">
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('pre-approved-data')}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let preApproved = result.data;
                    console.log(preApproved);
                    let html = '';
                    if (preApproved.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(preApproved, function (ket, row) {
                            html += `
                          <div class="col-md-4 pt-3">
                            <div class="card p-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>${row.contact_name}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    ${row.formatted_phone_number}
                                </div>
                                <div class="col-md-2">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                                           aria-expanded="true">
                                            <i class="fa fa-sliders" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu">
                                            <!--<a class="dropdown-item " href="${baseUrl('pre-approved/' + row.id + '/edit')}" ><i class="fa fa-pen" aria-hidden="true"></i> Edit</a>-->
                                            <form action="${baseUrl('pre-approved/' + row.id)}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="_method" value="DELETE">
                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
