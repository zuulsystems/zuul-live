@extends('frontend.layout.master')
@section('content')
    <style>
        .unread-notification {
            background-color: rgba(255, 255, 224, 1);
        }
    </style>
    <div class="container-fluid">

        <p class="mb-4">Home / Manage Notifications</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <button class="btn btn-outline-primary mark-as-all-read">Mark All As Read</button>
            </div>
            <div class="card-body">
                <div class="row" id="result"></div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="reject-pass-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="pass-event-name">Reject Pass </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form role="form" method="post" action="{{route('passes.reject-pass-request')}}" class="ajax-form">
                    <input type="hidden" id="pass_request_id" name="pass_request_id">
                    <input type="hidden" id="notification_id" name="id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12"><h5>Are Your Sure ?</h5></div>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger">Yes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Pass Detail Model--->
    @include('frontend.passes.models.pass-detail-model')
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });
            //Mark as All Read notification
            $(document).on('click', '.mark-as-all-read', function () {
                let notificationIds = "";
                if ($('.mark-as-read').length > 1) {
                    notificationIds = $.map($('.mark-as-read'), function (x) {
                        return $(x).data('id')
                    });
                }
                $.ajax({
                    url: "{{route('mark-all-read-notification')}}",
                    data: {
                        notificationIds: notificationIds,
                        notificationType: "{{request('type')}}"
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (result) {
                        if (result.status) {
                            window.location.reload();
                        } else {
                            notifyError("Something went wrong.Please try again.");
                        }
                    }
                });
            });
            //mark as read notification on click
            $(document).on('click', '.mark-as-read', function () {
                let notificationId = $(this).data('id');
                let notification = $(this);
                $.ajax({
                    url: "{{route('mark-read-notification')}}",
                    data: {
                        notificationId: notificationId,
                        notificationType: "{{request('type')}}"
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (result) {
                        if (result.status) {
                            notification.removeClass('unread-notification');
                        } else {
                            notifyError(result.message);
                        }
                    }
                });
            });


        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();

            let route = "{{ route('notifications-data') }}?type={{$type ?? null}}";

            $.ajax({
                url: route,
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let notifications = result.data;
                    let html = '';
                    if (notifications.length > 0) {
                        $('#btn-load-more').data('page', page);
                        let unread_class = "";
                        $.each(notifications, function (ket, row) {
                            unread_class = ""
                            if (row.is_read === '0') {
                                unread_class = 'unread-notification';
                            }
                            html += `
                          <div class="col-md-4 pb-4" >
                            <div class="card h-100 notification-card flex-row align-content-between flex-wrap ${unread_class}  mark-as-read" data-id="${row.id}">
                                <div class="d-block">
                                    <h4>${row.heading ?? ""}</h4>
                                    <small>${row.subheading ?? ""}</small>
                                    <p class="mb-0">${row.text ?? ""}</p>
                                </div>
                                <div class="d-block mt-3">`;
                            if (row.category == 'send_request_for_pass') {
                                html += `<div class="btn-group" >
                                            <button class="btn btn-danger btn-reject-pass" data-id="${row.id}" data-request-id="${row.pass_request_id}">Reject</button>
                                            <button class="btn btn-success btn-accept-pass" data-id="${row.id}" data-request-id="${row.pass_request_id}" >Accept</button>
                                         </div>`;
                            } else if (row.category === 'send_pass') {
                                html += `<div class="btn-group "  >
                                            <button class="btn btn-danger btn-pass-detail" data-id="${row.pass_id}" >View Pass</button>
                                        </div>`;
                            }
                            html += `</div>
                                 </div>
                              </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }

        //show pass detail
        $(document).on('click', '.btn-pass-detail', function () {
            //let passUserId  =  $(this).data('id');
            let passId = $(this).data('id');
            getPassDetail(passId);
        });

        //reject a pass request
        $(document).on('click', '.btn-reject-pass', function () {
            let notificationId = $(this).data('id');
            let passRequestId = $(this).data('request-id');
            $('#reject-pass-modal #pass_request_id').val(passRequestId);
            $('#reject-pass-modal #notification_id').val(notificationId);
            $('#reject-pass-modal').modal('show');
        });
        //accept a pass request
        $(document).on('click', '.btn-accept-pass', function () {
            let passRequestId = $(this).data('request-id');
            showLoader();
            $.ajax({
                url: "{{route('passes.accept-pass-request')}}",
                data: {
                    pass_request_id: passRequestId,
                },
                type: "POST",
                dataType: "json"
            });
        });

    </script>
@endpush
