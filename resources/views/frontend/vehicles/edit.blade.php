@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Edit Vehicle</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('vehicles.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Manage Vehicle
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{ route('vehicles.update', [$vehicle->id]) }}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="make">Make </label>
                                <input type="text" name="make" id="make"
                                       placeholder="Make" class="form-control" value="{{$vehicle->make}}">
                            </div>
                            <div class="form-group">
                                <label for="model">Model </label>
                                <input type="text" name="model" id="model"
                                       placeholder="Model" class="form-control" value="{{$vehicle->model}}">
                            </div>
                            <div class="form-group">
                                <label for="year">Year </label>
                                <input type="number" min="0" name="year" id="year"
                                       placeholder="Year" class="form-control" value="{{$vehicle->year}}">
                            </div>
                            <div class="form-group">
                                <label for="color">Color </label>
                                <input type="text" name="color" id="color"
                                       placeholder="Color" class="form-control" value="{{$vehicle->color}}">
                            </div>
                            <div class="form-group">
                                <label for="license_plate">License Plate </label>
                                <input type="text" name="license_plate" id="license_plate"
                                       placeholder="License Plate" class="form-control"
                                       value="{{$vehicle->license_plate}}">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save Changes </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

