@extends('frontend.layout.master')
@push('css')
    <style>
        .main-card.mb-3.card {
            padding: 15px 5px;
        }

        .house-hold-name {
            font-size: 18px;
            font-weight: 700;
            color: #072f60;
        }

        .house-hold-phone {
            font-size: 16px;
        }

        .house-hold-permission {
            margin-top: 30px;
            color: #072f60;
            font-size: 14px;
            margin-bottom: 10px;
        }

        .house-hold-list ul {
            list-style: none;
            padding-left: 0;
        }

        .house-hold-list ul li {
            font-size: 14px;
        }

        .house-hold-list ul li i {
            margin-right: 5px;
        }

        .main-card.mb-3.card h4 {
            font-size: 16px;
            font-weight: 400;
            margin-bottom: 10px;
        }

        .house-hold-status ul {
            list-style: none;
            padding-left: 0;
        }

        .house-hold-status ul li {
            font-size: 14px;
        }

        .house-hold-status ul li span.color-green {
            width: 12px;
            height: 12px;
            background-color: green;
            display: inline-block;
            margin-right: 5px;
        }

        .house-hold-status ul li span.color-red {
            width: 12px;
            height: 12px;
            background-color: red;
            display: inline-block;
            margin-right: 5px;
        }

        .house-hold-status ul li span.color-grey {
            width: 12px;
            height: 12px;
            background-color: grey;
            display: inline-block;
            margin-right: 5px;
        }

        .house-hold-avater {
            border-radius: 200px;
            overflow: hidden;
            width: 100px;
            height: 100px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
@endpush
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / My Household Members</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">

                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">

                <div class="row" id="result">

                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Edit Member Model--->
    <div class="modal fade" id="edit-member-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Family Member</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action=""
                      class="ajax-form" id="guard-permission-form">
                    <input type="hidden" name="user_id">
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" name="first_name" id="first_name" placeholder="First Name"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="middle_name">Middle Name</label>
                                    <input type="text" name="middle_name" id="middle_name" placeholder="Middle Name"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" name="last_name" id="last_name" placeholder="Last Name"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="first_name">Email</label>
                                    <input type="text" name="email" id="email" placeholder="Email"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="relationship">Relationship</label>
                                    <select name="relationship" id="relationship" class="form-control">
                                        <option value="husband">Husband</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country_code">Country code</label>
                                    <select name="country_code" id="country_code" class="form-control">
                                        <option value="1">+1</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="can_manage_family">Can Manage Household</label>
                                    <input type="checkbox" name="can_manage_family" id="can_manage_family" value="1"
                                           data-toggle="toggle" data-offstyle="light">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="can_send_passes">Can Send Passes</label>
                                    <input type="checkbox" name="can_send_passes" id="can_send_passes" value="1"
                                           data-toggle="toggle" data-offstyle="light">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="allow_parental_control">Allow Parental Control </label>
                                    <input type="checkbox" name="allow_parental_control" id="allow_parental_control"
                                           value="1"
                                           data-toggle="toggle" data-offstyle="light">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('household-members-data')}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let house_hold_members = result.data;
                    let permission = result.permission;
                    let html = '';
                    let options = '';
                    if (house_hold_members.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(house_hold_members, function (key, row) {
                            options = '';
                            options = `
                                      <a class="dropdown-item " href="${baseUrl('passes/active-passes?userId=' + row.id + '')}" ><i class="fa fa-ticket-alt" aria-hidden="true"></i> Active Passes</a>
                                      <a class="dropdown-item " href="${baseUrl('passes/archive-passes?userId=' + row.id + '')}" ><i class="fa fa-ticket-alt" aria-hidden="true"></i> Archived Passes</a>
                                    `;
                            //check user has right for send passes
                            if (permission.can_send_passes === true) {
                                options += `<a class="dropdown-item " href="${baseUrl('passes/sent-passes?userId=' + row.id + '')}" ><i class="fa fa-ticket-alt" aria-hidden="true"></i> Sent Passes</a>`;
                            }
                            //check user has right for parental control
                            if (permission.allow_parental_control === true) {
                                options += `<a class="dropdown-item "  href="${baseUrl('parental-control/' + row.id + '/edit')}" ><i class="fa fa-ticket-alt" aria-hidden="true"></i> Parental Control</a>`;
                            }


                            html += `
                                 <div class="col-md-6">
                                    <div class="main-card mb-3 card">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="house-hold-member-box">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="house-hold-avater"><img class="img" src="${(row.profile_image_url) ? row.profile_image_url : '{{ asset("assets/frontend/img/undraw_profile.svg")}}'}" alt="" style="    width: 100%;"></div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="house-hold-name">${row.full_name}</div>
                                                        <div class="house-hold-phone">${row.formatted_phone_number}</div>
                                                        <div class="house-hold-permission">Permissions</div>
                                                        <div class="house-hold-list">
                                                            <ul>
                                                                <li><i class="fas ${(row.can_manage_family) ? 'fa-check' : 'fa-times-circle'}"></i> Manage Household</li>
                                                                <li><i class="fas ${(row.can_send_passes) ? 'fa-check' : 'fa-times-circle'}"></i> Send Passes</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <h4>Passes</h4>
                                            <div class="house-hold-status">
                                                <ul>
                                                    <li><span class="color-green"></span> Active (${row.active_passes_count})</li>
                                                    <li><span class="color-red"></span> Archive (${row.archived_passes_count})</li>
                                                    <li><span class="color-grey"></span> Sent (${row.sent_passes_count})</li>
                                                </ul>
                                            </div>
                                            <div class="dropdown">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" style="color:#858796"
                                                   aria-expanded="true">
                                                    <i class="fa fa-sliders" aria-hidden="true"></i>Options</a>
                                                <div class="dropdown-menu">
                                                   ${options}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
