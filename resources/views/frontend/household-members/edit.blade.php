@extends('frontend.layout.master')
@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Edit Family Member</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('household-members.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> HouseHold Members
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('household-members.update', [$user->id])}}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" name="first_name" id="first_name" placeholder="First Name"
                                               class="form-control" value="{{$user->first_name}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="middle_name">Middle Name</label>
                                        <input type="text" name="middle_name" id="middle_name" placeholder="Middle Name"
                                               class="form-control" value="{{$user->middle_name}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" name="last_name" id="last_name" placeholder="Last Name"
                                               class="form-control" value="{{$user->last_name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="first_name">Email</label>
                                        <input type="text" name="email" id="email" placeholder="Email"
                                               class="form-control" value="{{$user->email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="relationship">Relationship</label>
                                        <select name="relationship" id="relationship" class="form-control">
                                            <option value="husband">Husband</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dial_code">Country code</label>
                                        <select name="dial_code" id="dial_code" class="form-control">
                                            @foreach($countries as $country)
                                                <option
                                                    value="{{$country->dial_code}}" {{($country->dial_code == $user->dial_code)?"selected":""}}>{{$country->country_name}}
                                                    +{{$country->dial_code}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number</label>
                                        <input type="text" name="phone_number" class="form-control"
                                               data-inputmask='"mask": "999-999-9999"' value="{{$user->phone_number}}"
                                               data-mask placeholder="Phone Number"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="can_manage_family">Can Manage Household</label>
                                        <input type="checkbox" name="can_manage_family"
                                               {{($user->hasPermission('can_manage_family')?"checked":"")}} id="can_manage_family"
                                               value="1" data-toggle="toggle" data-offstyle="light">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="can_send_passes">Can Send Passes</label>
                                        <input type="checkbox" name="can_send_passes"
                                               {{($user->hasPermission('can_send_passes')?"checked":"")}} id="can_send_passes"
                                               value="1"
                                               data-toggle="toggle" data-offstyle="light">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="allow_parental_control">Allow Parental Control </label>
                                        <input type="checkbox" name="allow_parental_control"
                                               {{($user->hasPermission('allow_parental_control')?"checked":"")}} id="allow_parental_control"
                                               value="1" data-toggle="toggle" data-offstyle="light">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save Changes </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

