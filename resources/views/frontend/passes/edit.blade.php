@extends('frontend.layout.master')
@section('content')
    <style>
        .pac-container {
            z-index: 1111;
        }
    </style>
    <div class="container-fluid">

        <!-- Page Heading -->
        <p class="mb-4">Home / Pass / Edit</p>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{route('passes.active-passes.index')}}" class="add_advertiser">
                    <i class="fas fa-list"></i> Passes
                </a>
            </div>
            <div class="card-body">
                <div class="card-body">
                    <form role="form" method="post" action="{{route('passes.passes.update',$pass->id)}}"
                          class="ajax-form" enctype="multipart/form-data">
                        @method('PUT')
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pass_date">Pass Date </label>
                                        <input type="datetime-local" name="pass_date" id="pass_date"
                                               placeholder="Pass Date"
                                               class="form-control"
                                               value="{{date('Y-m-d\TH:i:s',$pass->convertedPassStartDate())}}"
                                               min="{{date('Y-m-d')}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pass_type">Pass Type</label>
                                        <select name="pass_type" class="form-control" id="pass_type">

                                            <option value="one" {{$pass->pass_type == 'one'?'selected':''}} >One Time
                                            </option>
                                            <option value="recurring" {{$pass->pass_type == 'recurring'?'selected':''}}>
                                                Recurring
                                            </option>
                                            <option value="self" {{$pass->pass_type == 'self'?'selected':''}}>Self
                                            </option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="visitor_type">Visitor Type</label>
                                        <select name="visitor_type" class="form-control" id="visitor_type">
                                            <option
                                                value="friends_family" {{$pass->visitor_type == 'friends_family'?'selected':''}}>
                                                Family / Friend
                                            </option>
                                            <option
                                                value="vendor_delivery" {{$pass->visitor_type == 'vendor_delivery'?'selected':''}}>
                                                Vendor / Delivery
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="pass_validity">Pass Validity</label>
                                        <select name="pass_validity" id="pass_validity" class="form-control">
                                            @if($pass->pass_type == 'one' || $pass->pass_type == 'self')
                                                <option value="3" {{$pass->pass_validity == '3'?'selected':''}}>3
                                                    Hours
                                                </option>
                                                <option value="6" {{$pass->pass_validity == '6'?'selected':''}}>6
                                                    Hours
                                                </option>
                                                <option value="12" {{$pass->pass_validity == '12'?'selected':''}}>12
                                                    Hours
                                                </option>
                                                <option value="24" {{$pass->pass_validity == '24'?'selected':''}} >24
                                                    Hours
                                                </option>
                                                <option value="48" {{$pass->pass_validity == '48'?'selected':''}}>48
                                                    Hours
                                                </option>
                                            @elseif('recurring')
                                                <option value="24" {{$pass->pass_validity == '24'?'selected':''}}>24
                                                    Hours
                                                </option>
                                                <option value="48" {{$pass->pass_validity == '48'?'selected':''}}>48
                                                    Hours
                                                </option>
                                                <option value="168" {{$pass->pass_validity == '168'?'selected':''}}>1
                                                    Week
                                                </option>
                                                <option value="336" {{$pass->pass_validity == '336'?'selected':''}}>2
                                                    Weeks
                                                </option>
                                                <option value="720" {{$pass->pass_validity == '720'?'selected':''}}>1
                                                    Month
                                                </option>
                                                <option
                                                    value="876000" {{$pass->pass_validity == '876000'?'selected':''}}>No
                                                    Limit
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4" id="end_date_div">
                                    <div class="form-group">
                                        <label for="pass_date">End Date</label>
                                        <input type="text" name="end_date" id="end_date" placeholder="End Date"
                                               class="form-control" disabled
                                               value="{{date('Y/m/d h:i A',strtotime($pass->pass_date))}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="dial_code">Pass Event</label>
                                        <select name="event_id" id="event_id" class="form-control">
                                            @foreach($events as $event)
                                                <option
                                                    value="{{$event->id}}" {{$pass->event_id == $event->id ?'selected':''}}>{{$event->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                                data-target="#map-modal">Change
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {{--description--}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="description" readonly id="description"
                                                  class="form-control pass_address">{{$pass->description}}</textarea>
                                        <input type="hidden" name="lat" id="lat" value="{{$pass->lat}}">
                                        <input type="hidden" name="lng" id="lng" value="{{$pass->lng}}">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <!----> <span>Save Changes </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <!-- Add Contact in Group Model--->
    <div class="modal fade" id="map-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Location</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="searchbar-input">Search Address </label>
                                <input type="text" name="searchbar-input" class="form-control" id="searchbar-input"
                                       placeholder="Search Address"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="map" style="width:100%; height:25vh;"></div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-save-map-address">Save</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        @endsection
        @push('scripts')

            <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA&libraries=places"></script>
            <script src="{{asset('assets/frontend/js/google-map.js')}}"></script>
            <script>
                //get pass type
                $('#pass_type').change(function () {
                    let pass_type = $(this).val();
                    let validityOptions = '';
                    console.log(pass_type);
                    //check if pass type => self hide contact list
                    //set validity options by pass type
                    if (pass_type === 'self') {
                        validityOptions = `
                <option value="3">3 Hours</option>
                <option value="6">6 Hours</option>
                <option value="12">12 Hours</option>
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                `;
                        $('#contacts_div').hide();
                        $('#end_date_div').show();

                    } else if (pass_type === 'recurring') {
                        validityOptions = `
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                <option value="168">1 Week</option>
                <option value="336">2 Weeks</option>
                <option value="720">1 Month</option>
                <option value="876000">No Limit</option>
                `;
                        $('#end_date_div').hide();
                    } else {
                        validityOptions = `
                <option value="3">3 Hours</option>
                <option value="6">6 Hours</option>
                <option value="12">12 Hours</option>
                <option value="24" selected="selected">24 Hours</option>
                <option value="48">48 Hours</option>
                `;
                        $('#contacts_div').show();
                        $('#end_date_div').show();
                    }
                    $('#pass_validity').html(validityOptions);
                });

                //update end data when update pass date form data picker
                $(document).on('change', '#pass_date', function () {
                    if ($(this).val().length > 0) {
                        changeEndDate();
                    } else {
                        $('#end_date').val('');
                    }
                });

                //update end date from pass validity
                $(document).on('change', '#pass_validity', function () {
                    changeEndDate();
                });

                //calculate end date from pass date & pass validity
                function changeEndDate() {
                    $pass_date = $('#pass_date').val();
                    $pass_validity = $('#pass_validity :selected').val();
                    var dt = new Date($pass_date);
                    if (isValidDate(dt)) {
                        dt.setHours(parseInt(dt.getHours()) + parseInt($pass_validity));
                        var formattedDate = formatAMPM(dt);
                        $('#end_date').val(formattedDate);
                    }
                }

                //save address when map model close
                $('.btn-save-map-address').on('click', function (e) {
                    //getMarkerLocation();
                    $('#lat').val(lat);
                    $('#lng').val(lang);
                    $('#description').val(formattedAddress);
                    $('#map-modal').modal('hide');
                })

            </script>
    @endpush
