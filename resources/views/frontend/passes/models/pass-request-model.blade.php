<div class="modal fade" id="pass-request-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Request a Pass</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" method="post" action="{{route('add-contact-in-group')}}"
                  class="ajax-form">
                <input type="hidden" name="contact_id">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-gray">Select Who To Request Pass From Request Pass From</p>
                            <div class="form-group">
                                <a href="{{route('passes.pass-requests.create')}}" class="btn btn-primary">Existing
                                    Contact</a><br/>
                                <label for="group_id" class="pt-2">Select from your existing contacts </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="{{route('contacts.create')}}" class="btn btn-primary">New Contact</a><br/>
                                <label for="group_id" class="pt-2">Add a new contact by entering out the phone
                                    number</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
