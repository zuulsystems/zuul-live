<style>

    .highlight {
        background: #6a87bc;
        color: #fff;
        padding: 10px 10px;
        margin: 8px -15px;
        width: calc(100% + 30px);
    }

    .to-date-1 h4 {
        font-size: 14px;
    }

    .t-d span {
        font-size: 12px;
    }

    .modal-lg-custom {
        max-width: 600px;
    }

    .sender-1 h4 {
        font-size: 16px;
        font-weight: bolder;
    }

    .sender-1 p {
        font-size: 14px;
        margin-bottom: 7px;

    }

    .m-cen {
        margin: 0 auto;
    }

</style>
<div class="modal fade" id="pass-detail-modal">
    <div class="modal-dialog modal-lg-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="pass-event-name">Event </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" method="post" action="#"
                  class="ajax-form" id="">
                <div id="printpassdetails" class="modal-body">
                    <div class="row highlight">
                        <div class="col-sm-4 to_date to-date-1">
                            <h4>Starts</h4>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="d-flex flex-column t-d">

                                        <span class="dt1 " id="to-date"></span>
                                        <span class="tm1" id="to-date-time"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 for_date to-date-1">
                            <h4>Expires</h4>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="d-flex flex-column t-d">

                                        <span class="dt1" id="for-date">{{--Feb 2 2121--}}</span>
                                        <span class="tm1" id="for-date-time">{{--12:45 PM--}}</span>
                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 licence_image">
                            <img src="https://zuulsystem-upgrade.dev/assets/frontend/img/zxc.png"
                                 id="pass-licence-image" style="max-width: 100%">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 sender sender-1">
                            <h4>Sender Name</h4>
                            <p class="name" id="sender-name"></p>
                        </div>
                        <div class="col-sm-4 sender sender-1">
                            <h4>Community Name</h4>
                            <p class="community" id="sender-community-name"></p>
                            <p class="visitor_type yellow-background" style="background: yellow !important"
                               id="visitor-type"></p>
                        </div>
                        <div class="col-sm-4 sender sender-1">
                            <button class="btn add_to_contact" style="display: none;">
                                Add To Contact
                            </button>
                            <p class="add_to_contact_message"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 sender sender-1">
                            <h4>Sender Phone #</h4>
                            <p class="phone" id="sender-phone-number"></p>
                        </div>
                        <div class="col-sm-4 sender sender-1">
                            <h4>Directions</h4>
                            <p class="description" id="pass-description"></p>
                        </div>
                        <div class="col-sm-4 sender sender-1">
                            <button class="btn m-cen get_directions btn-danger d-flex">
                                Get Directions
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 vehicle sender-1">
                            <h4>Vehicle Detail</h4>
                            <p class="licence_plate" id="vehicle-license-plate"></p>
                            <p class="make" id="vehicle-make"></p>
                            <p class="model" id="vehicle-model"></p>
                        </div>
                        <div class="col-sm-4">
                            <p>Please use the QR code below for scanning and verification</p>
                        </div>
                        <div class="col-sm-4 qrcode">
                            <img src="https://chart.googleapis.com/chart?chs=200x200&amp;cht=qr&amp;chl=248313201517"
                                 id="qr_code_image" style="max-width: 100%">
                        </div>
                    </div>


                    <input type="hidden" name="_token" value="uYDHpxUVjmJUrlcez18KhOvphr7owTUUZ3XD8qmH">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger m-cen col-8" data-dismiss="modal">Close</button>

                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
