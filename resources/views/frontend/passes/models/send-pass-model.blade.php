<div class="modal fade" id="send-pass-modal" style="overflow-y: scroll">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Send A Pass</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" method="post" action="{{route('passes.passes.store')}}"
                  class="ajax-form" enctype="multipart/form-data">
                <input type="hidden" name="redirect" value="{{route('contacts.index')}}">

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pass_date">Pass Date</label>
                                <input type="datetime-local" name="pass_date" id="pass_date" placeholder="Pass Date"
                                       class="form-control" min="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="pass_type">Pass Type</label>
                                <select name="pass_type" class="form-control" id="pass_type">
                                    <option value="one" selected>One Time</option>
                                    <option value="recurring">Recurring</option>
                                    <option value="self">Self</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="visitor_type">Visitor Type</label>
                                <select name="visitor_type" class="form-control" id="visitor_type">
                                    <option value="friends_family">Family / Friend</option>
                                    <option value="vendor_delivery">Vendor / Delivery</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="pass_validity">Pass Validity</label>
                                <select name="pass_validity" id="pass_validity" class="form-control">
                                    <option value="3">3 Hours</option>
                                    <option value="6">6 Hours</option>
                                    <option value="12">12 Hours</option>
                                    <option value="24" selected="selected">24 Hours</option>
                                    <option value="48">48 Hours</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" id="end_date_div">
                            <div class="form-group">
                                <label for="pass_date">End Date</label>
                                <input type="text" name="end_date" id="end_date" placeholder="End Date"
                                       class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="dial_code">Pass Event</label>
                                <select name="event_id" id="event_id" class="form-control">
                                    @foreach($events as $event)
                                        <option value="{{$event->id}}">{{$event->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary" type="button" data-toggle="modal"
                                        data-target="#map-modal">Change
                                </button>
                            </div>
                        </div>
                    </div>
                    {{--description--}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="description" readonly id="description"
                                          class="form-control pass_address"></textarea>
                                <input type="hidden" name="lat" id="lat" value="">
                                <input type="hidden" name="lng" id="lng" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row" id="contacts_div">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="selected_contacts">Contact</label>
                                <select name="selected_contacts[]" id="selected_contacts" class="form-control" multiple>

                                </select>
                                <input type="hidden" name="selected_contact_message">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        <!----> <span>Add </span>
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
