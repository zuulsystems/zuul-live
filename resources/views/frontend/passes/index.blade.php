@extends('frontend.layout.master')
@section('content')

    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / My Passes / Active Passes</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{route('passes.active-passes.create')}}">
                            <i class="fas fa-plus-circle"></i> Create Pass
                        </a>
                        <a href="javascript:void(0)" data-target="#pass-request-modal" data-toggle="modal">
                            <i class="fas fa-plus-circle"></i> Request A Pass
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <div class="row" id="result">
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Add Contact in Group Model--->
    <div class="modal fade" id="pass-request-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Request a Pass</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('add-contact-in-group')}}"
                      class="ajax-form" id="guard-permission-form">
                    <input type="hidden" name="contact_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-gray">Select Who To Request Pass From Request Pass From</p>
                                <div class="form-group">
                                    <a href="{{route('passes.pass-requests.create')}}" class="btn btn-primary">Existing
                                        Contact</a><br/>
                                    <label for="group_id" class="pt-2">Select from your existing contacts </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a href="{{route('contacts.create')}}" class="btn btn-primary">New Contact</a><br/>
                                    <label for="group_id" class="pt-2">Add a new contact by entering out the phone
                                        number</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script>
        let page = 1;
        $(document).ready(function () {
            // get data on page load
            // loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

            //get more records on button click
            $(document).on('click', '.btn-add-group', function () {
                let contact_id = $(this).data('id');
                $('input[name="contact_id"]').val(contact_id);
                $('#add-group-modal').modal('show');
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            $.ajax({
                url: "{{route('contacts-data')}}",
                data: {
                    page: page,
                    search: search,
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let contacts = result.data;
                    let html = '';
                    if (contacts.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(contacts, function (ket, row) {
                            html += `
                          <div class="col-md-4 pt-3">
                            <div class="card p-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>${row.contact_name ?? ''}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                   ${row.formatted_phone_number}
                                </div>
                                <div class="col-md-2">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"
                                           aria-expanded="true">
                                            <i class="fa fa-sliders" aria-hidden="true"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item " href="${baseUrl('contacts/' + row.id + '/edit')}" ><i class="fa fa-pen" aria-hidden="true"></i> Edit</a>
                                            <form action="${baseUrl('contacts/' + row.id)}" class="ajax-form" method="POST">
                                                 <input type="hidden" name="_method" value="DELETE">
                                                 <button type="submit" class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                            </form>
                                            <a class="dropdown-item btn-add-group" data-id="${row.id}" href="javascript:void(0)" ><i class="fa fa-users" aria-hidden="true"></i> Add To Group</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }
    </script>
@endpush
