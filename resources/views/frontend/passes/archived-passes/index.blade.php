@extends('frontend.layout.master')
@section('content')
    <!-- Passes CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/passes.css')}}">

    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / My Passes / Archived Passes</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-success btn-filter" data-sorting-value="received">Received</button>
                        <button class="btn btn-dark btn-filter" data-sorting-value="sent">Sent</button>
                        <button class="btn btn-danger btn-filter" data-sorting-value="scanned">Scanned</button>
                    </div>
                    <div class="col-md-2">
                        <select name="sortBy" id="sortBy" class="form-control">
                            <option value="">Sort By</option>
                            <option value="event_name">Event Name</option>
                            <option value="sender_last_name">Sender's Last Name</option>
                            <option value="date">Date</option>
                            <option value="community">Community</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search by Name, Event or Community"
                                   id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true,false)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter_list" style="">

                        </div>
                    </div>
                </div>


                <div class="row" id="result">
                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Pass Detail Model--->
    @include('frontend.passes.models.pass-detail-model')

@endsection
@push('scripts')
    <script>
        let page = 1;
        let filter_type = '';
        let filter_search = '';
        let sortType = 'received';
        let userId = '{{$userId}}';
        // let resetOptions = true;
        $(document).ready(function () {
            // get data on page load
            loadMore();

            //sort by button
            $(document).on('click', '.btn-filter', function () {
                sortType = $(this).data('sorting-value');
                filter_type = '';
                filter_search = '';
                loadMore(1, true);
            });

            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page, false, false);
            });

            //get filter values from
            $(document).on('change', '#sortBy', function () {
                let sortBy = $(this).val();
                filter_search = '';
                if (sortBy == 'event_name' || sortBy == 'sender_last_name' || sortBy == 'community') {
                    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
                    let alpha_html = '';
                    $.each(alphabet, function (letter) {
                        alpha_html += `<div class="filter_list_inner filter-data" data-value="${alphabet[letter]}">${alphabet[letter]}</div>`;
                    });
                    $('.filter_list').html(alpha_html);
                } else if (sortBy === 'date') {
                    showLoader();
                    let dateFilterUrl = "{{route('passes.active-passes-dates',['expired' => '1'])}}"; // default
                    if (sortType == 'received') {
                        //dateFilterUrl = "{{route('passes.active-passes-dates',['expired' => '1'])}}";
                        dateFilterUrl = baseUrl('passes/active-passes-dates?expired=1&userId=' + userId + '');
                    } else if (sortType == 'sent') {
                        //dateFilterUrl = "{{route('passes.sent-passes-dates',['expired' => '1','userId' => $userId])}}";
                        dateFilterUrl = baseUrl('passes/sent-passes-dates?expired=1&userId=' + userId + '');

                    } else if (sortType == 'scanned') {
                        //dateFilterUrl = "{{route('passes.scanned-passes-dates',['userId' => $userId])}}";
                        dateFilterUrl = baseUrl('passes/scanned-passes-dates?userId=' + userId + '');

                    }
                    $.get(dateFilterUrl, function (result) {
                        hideLoader();
                        if (result.data.length > 0) {
                            let pass_date_html = '';
                            $.each(result.data, function (key, row) {
                                pass_date_html += `<div class="filter_list_inner filter-data" data-value="${row.date}">
                                                        <span class="filter_list_date date">${row.pass_start_day}</span>
                                                        <span class="filter_list_date month">${row.pass_start_month}</span>
                                                   </div>`;
                            });
                            $('.filter_list').html(pass_date_html);
                        }
                    });
                } else {
                    $('.filter_list').html('');
                }
            });

            //Get filter value and filter records
            $(document).on('click', '.filter-data', function () {
                filter_search = $(this).data('value');
                loadMore(1, true, false);
            });

            //pass actions / pass detail
            $(document).on('click', '.pass-action', function () {
                let passId = $(this).data('pass-id');
                getPassDetail(passId, userId);
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false, resetOptions = true) {
            showLoader();
            let search = $('#search').val();
            // let filter_type = $('#sortBy option:selected').val();
            filter_type = $('#sortBy option:selected').val();

            let url = "{{route('passes.active-passes-data',['expired' => '1'])}}"; // default
            if (sortType == 'received') {
                url = "{{route('passes.active-passes-data',['expired' => '1'])}}";
            } else if (sortType == 'sent') {
                url = "{{route('passes.sent-passes-data',['expired' => '1'])}}";
            } else if (sortType == 'scanned') {
                url = "{{route('passes.scanned-passes-data')}}";
            }

            $.ajax({
                url: url,
                data: {
                    page: page,
                    search: search,
                    filter_type: filter_type,
                    filter_search: filter_search,
                    userId: userId
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let archive_passes = result.data;
                    let html = '';
                    let filterSortByOptions = '';
                    let qr_code_image_url = "{{asset("assets/frontend/img/qr.jpg")}}";
                    let default_user_image_url = "{{asset('assets/frontend/img/default-user.png')}}";
                    if (archive_passes.length > 0) {
                        $('#btn-load-more').data('page', page);
                        //received passes html
                        if (sortType == 'received') {
                            //filter sort options
                            filterSortByOptions = `
                                <option value="">Sort By</option>
                                <option value="event_name">Event Name</option>
                                <option value="sender_last_name">Sender's Last Name</option>
                                <option value="date">Date</option>
                                <option value="community">Community</option>
                            `;
                            //populate passes data
                            $.each(archive_passes, function (ket, row) {
                                html += `
                                <div class="col-md-4">
                        <div class="pass pass-active pass-inside-div pass-action" data-id="${row.id}" data-pass-id="${row.pass_id}" style="width:100%;cursor: pointer; ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${row.sender_profile_image}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Sender: ${row.sender_name}</h5>
                                                <h6>Community Name<span>${row.sender_community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                        `;
                            });
                        }
                        //sent passes
                        else if (sortType == 'sent') {
                            //filter sort options
                            filterSortByOptions = `
                                <option value="">Sort By</option>
                                <option value="event_name">Event Name</option>
                                <option value="date">Date</option>
                            `;
                            //populate passes data
                            $.each(archive_passes, function (ket, row) {
                                html += `
                                <div class="col-md-4">
                        <div class="pass pass-sent pass-inside-div" style="width:100%;cursor: pointer; ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${default_user_image_url}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Receiver: ${row.username}</h5>
                                                <h6>Community Name<span>${row.community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
                    </div>
                        `;
                            });
                        }
                        //archived passes
                        else if (sortType == 'scanned') {
                            //filter sort options
                            filterSortByOptions = `
                                <option value="">Sort By</option>
                                <option value="event_name">Event Name</option>
                                <option value="sender_last_name">Sender's Last Name</option>
                                <option value="date">Date</option>
                                <option value="community">Community</option>
                            `;
                            //populate passes data
                            $.each(archive_passes, function (ket, row) {
                                html += `
                                <div class="col-md-4">
                        <div class="pass pass-scanned pass-inside-div pass-action" data-id="${row.id}" data-pass-id="${row.pass_id}" style="width:100%;cursor: pointer; ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${row.sender_profile_image}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Sender: ${row.sender_name}</h5>
                                                <h6>Community Name<span>${row.sender_community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                        `;
                            });
                        }
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                        //filter sort by options
                        if (resetOptions) {
                            $('#sortBy').html(filterSortByOptions);//added sort by options
                            $('.filter_list').html(''); // clear filter list
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }


    </script>
@endpush
