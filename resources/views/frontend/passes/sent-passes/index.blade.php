@extends('frontend.layout.master')
@section('content')
    <!-- Passes CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/passes.css')}}">

    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / My Passes / Sent Passes</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{route('passes.passes.create')}}">
                            <i class="fas fa-plus-circle"></i> Create a Pass
                        </a>
                        <a href="javascript:void(0)" data-target="#pass-request-modal" data-toggle="modal">
                            <i class="fas fa-plus-circle"></i> Request a Pass
                        </a>
                    </div>

                    <div class="col-md-4">
                        <select name="sortBy" id="sortBy" class="form-control">
                            <option value="">Sort By</option>
                            <option value="event_name">Event Name</option>
                            <option value="date">Date</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search " id="search" >
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter_list" style="">

                        </div>
                    </div>
                </div>


                <div class="row" id="result">


                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="retract-pass-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="pass-event-name">Retract Pass </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form role="form" method="post" action="{{route('passes.retract-pass-request')}}" class="ajax-form">
                        <input type="hidden" id="pass_id" name="pass_id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12"><h5>Are Your Sure ?</h5></div>
                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger" >Yes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@include('frontend.passes.models.pass-request-model')

@endsection
@push('scripts')
    <script>
        let page = 1;
        let filter_search  = '';
        let userId  = '{{$userId}}';
        console.log(userId);
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

            //get filter values from
            $(document).on('change', '#sortBy', function () {
                let sortBy = $(this).val();
                filter_search = '';
                if(sortBy == 'event_name' || sortBy == 'sender_last_name' || sortBy == 'community'){
                    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
                    let alpha_html = '';
                    $.each(alphabet, function(letter) {
                        alpha_html += `<div class="filter_list_inner filter-data" data-value="${alphabet[letter]}">${alphabet[letter]}</div>`;
                    });
                    $('.filter_list').html(alpha_html);
                }
                else if(sortBy === 'date'){
                    showLoader();
                    $.get("{{route('passes.sent-passes-dates')."?userId=".$userId}}", function(result){
                        hideLoader();
                        if(result.data.length > 0){
                            let pass_date_html = '';
                            $.each(result.data,function (key,row) {
                                pass_date_html += `<div class="filter_list_inner filter-data" data-value="${row.date}">
                                                        <span class="filter_list_date date">${row.pass_start_day}</span>
                                                        <span class="filter_list_date month">${row.pass_start_month}</span>
                                                   </div>`;
                            });
                            $('.filter_list').html(pass_date_html);
                        }
                    });
                } else {
                    $('.filter_list').html('');
                }
            });

            //Get filter value and  filter records
            $(document).on('click','.filter-data',function () {
                filter_search = $(this).data('value');
                loadMore(1,true);
            });

            //reject a pass request
            $(document).on('click','.btn-retract-pass',function () {
                let passId  =  $(this).data('id');
                $('#retract-pass-modal #pass_id').val(passId);
                $('#retract-pass-modal').modal('show');
            });

        });

        //load more data by page number
        function loadMore(page = 1, reset = false)  {
            showLoader();
            let search = $('#search').val();
            let filter_type = $('#sortBy option:selected').val();
            $.ajax({
                url: "{{route('passes.sent-passes-data')}}",
                data: {
                    page: page,
                    search: search,
                    filter_type:filter_type,
                    filter_search:filter_search,
                    userId:userId
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let active_passes = result.data;
                    let html = '';
                    let qr_code_image_url = "{{asset("assets/frontend/img/qr.jpg")}}";
                    let default_user_image_url = "{{asset('assets/frontend/img/default-user.png')}}";
                    if (active_passes.length > 0) {
                        $('#btn-load-more').data('page', page);
                        //<h5>Sender: ${row.sender_name}</h5>
                        $.each(active_passes, function (ket, row) {
                            html += `
                                <div class="col-md-4">
                        <div class="pass pass-sent pass-inside-div" style="width:100%;cursor: pointer;position: relative ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${default_user_image_url}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Receiver: ${row.username}</h5>
                                                <h6>Community Name<span>${row.community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>`;

                            if(userId == ""){
                                html += ` <a href="#" class="btn-retract-pass" data-id="${row.id}" ><i class="fa fa-trash delPass"></i></a>
                                 <a href="${baseUrl('passes/passes/'+row.id+'/edit')}"><i class="fa fa-edit editPass"></i></a>`;
                            }


                            html += `<div>
                            </div>
                        </div>
                    </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }


    </script>
@endpush
