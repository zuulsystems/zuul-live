@extends('frontend.layout.master')
@section('content')
    <!-- Passes CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/passes.css')}}">
    <div class="container-fluid">

        <!-- Page Heading -->

        <p class="mb-4">Home / My Passes / Active Passes</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-4">
                        @cannot('guest-outsiders-daily')
                            @if(auth()->user()->is_suspended == '0')
                                <a href="{{route('passes.passes.create')}}">
                                    <i class="fas fa-plus-circle"></i> Create a Pass
                                </a>
                            @endif
                        @endcannot
                        <a href="javascript:void(0)" data-target="#pass-request-modal" data-toggle="modal">
                            <i class="fas fa-plus-circle"></i> Request a Pass
                        </a>
                    </div>
                    <div class="col-md-4">
                        <select name="sortBy" id="sortBy" class="form-control">
                            <option value="">Sort By</option>
                            <option value="event_name">Event Name</option>
                            <option value="sender_last_name">Sender's Last Name</option>
                            <option value="date">Date</option>
                            <option value="community">Community</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search by Name, Event or Community"
                                   id="search">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="loadMore(1,true)">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-secondary" type="button" onclick="window.location.reload();">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter_list" style="">
                        </div>
                    </div>
                </div>

                <div class="row" id="result">

                </div>
                <div class="row">
                    <div class="col-md-12 text-center pt-3">
                        <button class="btn btn-primary" id="btn-load-more" data-page="1">Load More</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Assign Vehicle to Received Pass Model--->
    <div class="modal fade" id="assign-vehicle-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Vehicle List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('passes.set-user-pass-vehicle')}}"
                      class="ajax-form">
                    <input type="hidden" name="pass_user_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" id="vehicle_list">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Pass Detail Model--->
    @include('frontend.passes.models.pass-detail-model')

    <!-- Pass Request Model--->
    @include('frontend.passes.models.pass-request-model')

@endsection
@push('scripts')
    <script>
        let page = 1;
        let filter_search = '';
        let userId = "{{$userId}}";
        $(document).ready(function () {
            // get data on page load
            loadMore();
            //get more records on button click
            $(document).on('click', '#btn-load-more', function () {
                page = $("#btn-load-more").data('page');
                page++;
                loadMore(page);
            });

            //get filter values from
            $(document).on('change', '#sortBy', function () {
                let sortBy = $(this).val();
                filter_search = '';
                if (sortBy == 'event_name' || sortBy == 'sender_last_name' || sortBy == 'community') {
                    let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
                    let alpha_html = '';
                    $.each(alphabet, function (letter) {
                        alpha_html += `<div class="filter_list_inner filter-data" data-value="${alphabet[letter]}">${alphabet[letter]}</div>`;
                    });
                    $('.filter_list').html(alpha_html);
                } else if (sortBy === 'date') {
                    showLoader();
                    $.get("{{route('passes.active-passes-dates')."?userId=".$userId}}", function (result) {
                        hideLoader();
                        if (result.data.length > 0) {
                            let pass_date_html = '';
                            $.each(result.data, function (key, row) {
                                pass_date_html += `<div class="filter_list_inner filter-data" data-value="${row.date}">
                                                        <span class="filter_list_date date">${row.pass_start_day}</span>
                                                        <span class="filter_list_date month">${row.pass_start_month}</span>
                                                   </div>`;
                            });
                            $('.filter_list').html(pass_date_html);
                        }
                    });
                } else {
                    $('.filter_list').html('');
                }
            });

            //Get filter value and  filter records
            $(document).on('click', '.filter-data', function () {
                filter_search = $(this).data('value');
                loadMore(1, true);
            });


            //pass actions
            $(document).on('click', '.pass-action', function () {
                let passId = $(this).data('pass-id');
                let passUserId = $(this).data('id');
                let passUserVehicleId = $(this).data('vehicle-id');

                //Open vehicle list modal for assign vehicle to received pass
                if (userId == "" && passUserVehicleId == null || passUserVehicleId == "") {
                    getUserVehicles(passUserId);
                } else {
                    getPassDetail(passId, userId);
                    console.log("Pass Detail");
                }
            });
        });

        //load more data by page number
        function loadMore(page = 1, reset = false) {
            showLoader();
            let search = $('#search').val();
            let filter_type = $('#sortBy option:selected').val();
            $.ajax({
                url: "{{route('passes.active-passes-data')}}",
                data: {
                    page: page,
                    search: search,
                    filter_type: filter_type,
                    filter_search: filter_search,
                    userId: "{{$userId ?? ""}}"
                },
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let active_passes = result.data;
                    let html = '';
                    let qr_code_image_url = "{{asset("assets/frontend/img/qr.jpg")}}";
                    if (active_passes.length > 0) {
                        $('#btn-load-more').data('page', page);
                        $.each(active_passes, function (ket, row) {
                            html += `
                                <div class="col-md-4">
                        <div class="pass pass-active pass-inside-div pass-action" data-id="${row.id}" data-pass-id="${row.pass_id}" data-vehicle-id="${row.vehicle_id}" style="width:100%;cursor: pointer; ">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding-right:0px !important;">
                                    <div class="pass-lft">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="${row.sender_profile_image}"
                                                     alt="pass-img" class="media-object" style="width:60px">
                                            </div>
                                            <div class="pass-body">
                                                <h4 class="media-heading">${row.event_name}</h4>
                                                <h5>Sender: ${row.sender_name}</h5>
                                                <h6>Community Name<span>${row.sender_community_name}</span></h6>

                                            </div>
                                        </div>
                                        <p>${row.description}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="pass-ryt">
                                        <h3>${row.pass_start_date}</h3>
                                        <h4>${row.pass_validity}</h4>
                                        <div class="qr"><img src="${qr_code_image_url}"
                                                             class="img-responsive" alt="QR Code"></div>
                                    </div>
                                </div>
                            </div>
                            <div>`;
                            if (userId == "") {
                                html += `${
                                    ((row.vehicle_id != null && row.vehicle_id != "") && (row.license_plate != "")) ?
                                        "<h6 style='color:green;' class='text-center'>Vehicle: " + row.make + " -  " + row.license_plate + "</h6>" :
                                        " <h6 style='color:red;' class='text-center'>No vehicle selected for this pass</h6>"
                                }`;

                            }
                            html += `</div>
                        </div>
                    </div>
                        `;
                        });
                        if (reset === true) {
                            $('#result').empty().append(html)
                        } else {
                            $('#result').append(html);
                        }
                    } else {
                        notifyError('No Records Found !');
                    }
                }
            });
        }

        //get User Vehicles
        function getUserVehicles(passUserId) {
            showLoader();

            $.ajax({
                url: "{{route('vehicles-data')}}",
                data: {},
                type: "GET",
                dataType: "json",
                success: function (result) {
                    hideLoader();
                    let vehicles = result.data.data;
                    let html = '';
                    $('input[name="pass_user_id"]').val(passUserId);
                    if (vehicles.length > 0) {
                        $.each(vehicles, function (ket, row) {
                            html += `
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="vehicle_id" value="${row.id}"> ${row.license_plate}
                                        </label>
                                     </div>
                                 `;
                        });
                        $('#vehicle_list').empty().append(html);
                        $('#assign-vehicle-modal').modal('show');
                    } else {
                        notifyError('No Vehicles Found !');
                    }
                }
            });
        }


    </script>
@endpush
