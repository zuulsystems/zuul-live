<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{url('/')}}"/>
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Zuul Systems</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/frontend/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/frontend/css/sb-admin-2.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="{{ asset('assets/frontend/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/toastr/toastr.min.css')}}">
    <!-- Toggle Switch  -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap-toggle.min.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/custom.css')}}">
    <style>
        /*loading css*/
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: visible;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.3);
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 1500ms infinite linear;
            -moz-animation: spinner 1500ms infinite linear;
            -ms-animation: spinner 1500ms infinite linear;
            -o-animation: spinner 1500ms infinite linear;
            animation: spinner 1500ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */
        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        /*Datatable CSS*/
        .dataTables_wrapper {
            overflow-x: auto;
        }

        .table-responsive,
        .dataTables_scrollBody {
            overflow: visible !important;
        }

        .table-responsive-disabled .dataTables_scrollBody {
            overflow: hidden !important;
        }

        .table-responsive .dataTables_scrollHead {
            overflow: unset !important;
        }

        .table {
            width: 100% !important;
        }


        .align-items-center-span {
            float: right;
        }
    </style>
    @stack('css')

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">
    <!-- Loader -->
    <div class="loading" style="display: none"></div>
    <!-- End Loader-->
    <!-- Sidebar -->
    @include('frontend.layout.sidebar')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>


                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                             aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                           placeholder="Search for..." aria-label="Search"
                                           aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>


                    <!-- push notifications, email notifications and vacation mode settings -->
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-cog fa-fw"></i>
                        </a>
                        <!-- Dropdown - Alerts -->
                        <div id="vacation_mode"
                             class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="alertsDropdown">
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="w-100 d-flex justify-content-between align-items-center">
                                    <span class="font-weight-bold">Enable Push Notifications</span>
                                    <input type="checkbox" name="push_notification"
                                           {{ auth()->user()->push_notification == 1 ? 'checked' : '' }} value="{{ auth()->user()->push_notification }}"
                                           data-toggle="toggle" data-offstyle="light"
                                           onchange="setSettingsFlag('push_notification', this)">
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="w-100 d-flex justify-content-between align-items-center">
                                    <span class="font-weight-bold">Enable Mail Notifications</span>
                                    <input type="checkbox" name="email_notification"
                                           {{ auth()->user()->email_notification == 1 ? 'checked' : '' }} value="{{ auth()->user()->email_notification }}"
                                           data-toggle="toggle" data-offstyle="light"
                                           onchange="setSettingsFlag('email_notification', this)">
                                </div>
                            </a>
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <div class="w-100 d-flex justify-content-between align-items-center">
                                    <span class="font-weight-bold">Enable Vacation Mode</span>
                                    <input type="checkbox" name="vacation_mode"
                                           {{ auth()->user()->vacation_mode == 1 ? 'checked' : '' }} value="{{ auth()->user()->vacation_mode }}"
                                           data-toggle="toggle" data-offstyle="light"
                                           onchange="setSettingsFlag('vacation_mode', this)">
                                </div>
                            </a>
                        </div>
                    </li>


                    <!-- Nav Item - Alerts -->
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bell fa-fw"></i>
                            <!-- Counter - Alerts -->
                        </a>
                        <!-- Dropdown - Alerts -->
                        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="alertsDropdown">
                            <a class="dropdown-item align-items-center" href="{{ route('notifications.index') }}">
                                <span class="font-weight-bold">ZUUL NOTIFICATIONS</span>
                                <span
                                    class="align-items-center-span">{{ notificationsCount('zuul', auth()->id()) }}</span>
                            </a>
                            <a class="dropdown-item align-items-center"
                               href="{{ route('notifications.index',['type'=>'pc']) }}">
                                <span class="font-weight-bold">PARENTAL NOTIFICATIONS</span>
                                <span
                                    class="align-items-center-span">{{ notificationsCount('parental_control', auth()->id()) }}</span>
                            </a>
                        </div>
                    </li>


                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span
                                class="mr-2 d-none d-lg-inline text-gray-600 small">{{auth()->user()->fullName}}</span>
                            <img class="img-profile rounded-circle"
                                 src="{{auth()->user()->profileImageUrl}}">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">

                            <a class="dropdown-item" href="{{ route('account-setting.index') }}">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Account Settings
                            </a>

                            <div class="dropdown-divider"></div>

                            <form action="{{route('logout')}}" method="post" id="logout-form" name="logout-form">
                                @csrf
                                <a href="#" class="dropdown-item" onclick="return $('#logout-form').submit()">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </form>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            @yield('content')


            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        @include('frontend.layout.footer')


    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets/frontend/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('assets/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('assets/frontend/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('assets/frontend/js/sb-admin-2.min.js')}}"></script>

<!-- Toastr -->
<script src="{{ asset('assets/admin/plugins/toastr/toastr.min.js')}}"></script>

<!-- Ajax submit library -->
<script src="{{asset('assets/frontend/js/jquery.form.min.js')}}"></script>

<!-- Mask Input -->
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<!-- Bootstrap Switch -->
<script src="{{ asset('assets/frontend/js/bootstrap-toggle.min.js')}}"></script>
<!-- Datatable -->
<script src="{{ asset('assets/frontend/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/frontend/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>

<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase-messaging.js"></script>

<script>
    $(document).ready(function () {


        $(document).on('click', '#vacation_mode', function () {
            e.stopPropagation();
        });
        getBrowserToken();
    });


    function setSettingsFlag($key, $switch) {
        let value = $($switch).val();
        let data = {};
        data[$key] = value == 1 ? 0 : 1;
        $.ajax({
            url: "{{ route('update-user-notification-settings') }}",
            data: data,
            type: "POST",
            success: function (data) {
                if (data.status === false) {
                    notifyError('Something went wrong');
                }
            },
        });
    }

    // Firebase configuration ------ Start --{---

    //Must keep firebase configuration file(firebase-messaging-sw.js) on domain root like, https://domain.com/firebase-messaging-sw.js
    var config = {
        apiKey: "AIzaSyBMj7XglMuT4qXj3me5p9dG_Meqe6Fq49o",
        authDomain: "zuul-master.firebaseapp.com",
        databaseURL: "https://zuul-master.firebaseio.com",
        projectId: "zuul-master",
        storageBucket: "zuul-master.appspot.com",
        messagingSenderId: "1013551636023"
    };

    let messaging;
    let globalToken;

    // Initialize Firebase
    firebase.initializeApp(config);
    messaging = firebase.messaging();

    function getBrowserToken() {
        console.log("in messeging");
        messaging.requestPermission().then(function () {
            globalToken = messaging.getToken();
            console.log('request for permission, with return globalToken: ', globalToken);
            return globalToken;
        }).then(function (token) {
            globalToken = token;
            storeBrowserToken(token);
        })
            .catch(function (err) {
                console.log('Permission denied', err);
            });

    }


    function storeBrowserToken(web_token) {
        $.ajax({
            url: '{{ route("store-browser-token")}}',
            data: {
                web_token: web_token
            },
            method: 'POST',
            dataType: 'json',
            success: function (data) {
            }
        });
    }

    // Firebase configuration ------ End --}---


</script>
@stack('scripts')
</body>

</html>
