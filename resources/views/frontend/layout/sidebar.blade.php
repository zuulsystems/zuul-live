<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
        <div class="sidebar-brand-text mx-3">Zuul Systems{{-- <sup>2</sup>--}}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePasses"
           aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-ticket-alt"></i>
            <span>My Passes</span>
        </a>
        <div id="collapsePasses" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">My Passes:</h6>
                <a class="collapse-item" href="{{route('passes.active-passes.index')}}">Active Passes</a>
                <a class="collapse-item" href="{{route('passes.archive-passes.index')}}">Archived Passes</a>
                @cannot('guest-outsiders-daily')
                    <a class="collapse-item" href="{{route('passes.sent-passes.index')}}">Sent Passes</a>
                @endcannot
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseContacts"
           aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-book"></i>
            <span>My Contacts</span>
        </a>
        <div id="collapseContacts" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">My Contacts:</h6>
                <a class="collapse-item" href="{{route('contacts.index')}}">Contacts</a>
                <a class="collapse-item" href="{{route('contact-groups.index')}}">Groups</a>
                <a class="collapse-item" href="{{route('contacts.index',['type'=>'favourites'])}}">Favorites</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('pre-approved.index')}}">
            <i class="fas fa-fw fa-check"></i>
            <span>Pre-Approved Guests</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{route('vehicles.index')}}">
            <i class="fas fa-fw fa-car"></i>
            <span>Manage Vehicles</span></a>
    </li>

    @cannot('guest-outsiders-daily')
        <li class="nav-item">
            <a class="nav-link" href="{{route('household-members.index')}}">
                <i class="fas fa-fw fa-users"></i>
                <span>My Household Members</span>
            </a>
        </li>
    @endcannot
    <li class="nav-item">
        <a class="nav-link" href="{{ route('account-setting.index') }}">
            <i class="fas fa-fw fa-cog"></i>
            <span>Account / Settings</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('change-password.index') }}">
            <i class="fas fa-fw fa-lock"></i>
            <span>Change Password</span></a>
    </li>

    @cannot('guest-outsiders-daily')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('events.index') }}">
                <i class="fas fa-fw fa-calendar"></i>
                <span>Events</span></a>
        </li>
    @endcannot

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    <!-- Sidebar Message -->

</ul>
