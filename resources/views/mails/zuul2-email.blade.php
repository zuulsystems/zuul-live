To all ZUUL members:
<br/>
<br/>

THANK YOU.Thank you for your patience, your feedback, and your trust. We are proud to announce that our latest version is available for download and update on the app stores.  We took great measures to enhance the user experience, speed, and overall functionality of ZUUL.
<br/>
<br/>

To work with our upgraded platform, please update or download ZUUL version 5.5
<br/>
IOS: <a href="https://apps.apple.com/us/app/zuul-systems/id1438385504">https://apps.apple.com/us/app/zuul-systems/id1438385504</a><br/>
Android: <a href="https://play.google.com/store/apps/details?id=com.reavertechnologies.zuulmaster&hl=en_US&gl=US">https://play.google.com/store/apps/details?id=com.reavertechnologies.zuulmaster&hl=en_US&gl=US</a>
<br/>
<br/>
Sincerely,<br/>
Adam Lucks<br/>
Founder / CEO
