<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zuul Systems Admin | Reset Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <style>
        body {
            background: url("images/bg-log.jpg") no-repeat !important;
            background-position-x: center !important;
        }

        .error {
            border: 2px solid red;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img class="img-fluid" src="{{ url('images/logo.png') }}">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Reset your password</p>
            @if(session()->has('success'))
                <div class="alert alert-success">{{session()->get('success')}}</div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">{{session()->get('error')}}</div>
            @endif
            <div class="alert alert-danger" style="display: none" id="msg">Please fill in all fields.</div>
            <form action="{{url('reset-password')}}" method="post" id="reset-password-form">
                @csrf
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password"
                           required>
                    <input type="hidden" class="form-control" name="code" value={{$_GET['code']}}>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="confirm_password" id="confirm_password"
                           placeholder="Confirm Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    {{-- password match message --}}
                    <span class="invalid-feedback d-block" style id="password-error" role="alert">
                        <strong>&nbsp;</strong>
                    </span>
                </div>
                <div class="row">
                    <div class="col-6">
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                        <button type="submit" id="submit-button" class="btn btn-primary btn-block">Reset Password
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#confirm_password').on('keyup', function () {
            var password = $('#password').val();
            var confirmPassword = $(this).val();
            var submitButton = $('#submit-button');

            if (password === confirmPassword) {
                // Passwords match
                $('#password-error').text("").show()
                submitButton.prop('disabled', false);
                // Enable submit button or perform any other action
            } else {
                // Passwords do not match
                $('#password-error').text('Password do not match').show();
                submitButton.prop('disabled', true);
                // Disable submit button or perform any other action
            }
        });

        $('#password').on('keyup', function () {
            var password = $('#confirm_password').val();
            var confirmPassword = $(this).val();
            var submitButton = $('#submit-button');

            if (password === confirmPassword) {
                // Passwords match
                $('#password-error').text("").show()
                submitButton.prop('disabled', false);
                // Enable submit button or perform any other action
            } else {
                // Passwords do not match
                $('#password-error').text('Password do not match').show();
                submitButton.prop('disabled', true);
                // Disable submit button or perform any other action
            }
        });
    });

    $('#reset-password-form').submit(function (event) {
        var password = $('#password').val();
        var confirmPassword = $('#confirm_password').val();

        if (password === '' || confirmPassword === '') {
            event.preventDefault(); // Prevent form submission
            document.getElementById("msg").style.display = 'block';
        }
    });
</script>
</body>
</html>
