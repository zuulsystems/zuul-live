<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zuul Systems Admin | Register</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{route('login')}}"><b>Zuul Systems</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign Up with Phone Number</p>

            <form action="{{route('register')}}" method="post">
                @csrf
                <div class="input-group mb-3"
                     id="phone-div" {{ old('userType') == "admin" ? "style=display:none" : '' }}>
                    <input type="text" class="form-control" name="name" placeholder="Name">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                    @error('name')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="mb-3" id="phone-div" {{ old('userType') == "admin" ? "style=display:none" : '' }}>
                    <label>Dial Code</label>
                    <select class="form-control" id="dial_code" name="dial_code">
                        @php
                            $countries = activeCountries();
                        @endphp
                        @foreach($countries as $country)
                            <option value="{{ $country->dial_code }}">+{{ $country->dial_code }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group mb-3"
                     id="phone-div" {{ old('userType') == "admin" ? "style=display:none" : '' }}>

                    <input type="text" class="form-control" name="phone_number" id="phone_number"
                           placeholder="Phone Number">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-phone"></span>
                        </div>
                    </div>
                    @error('phone_number')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                    <span class="invalid-feedback d-block" role="alert">
                          <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password_confirmation"
                           placeholder="Confirm Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('confirm_password')
                    <span class="invalid-feedback d-block" role="alert">
                          <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group text-center" style="margin:10px;">
                    <p id="passwordHelpBlock" class="form-text text-muted">
                        Your password must be more than 9 characters long, should contain at-least 1 Uppercase, 1
                        Lowercase, 1 Numeric and 1 special character.
                    </p>
                </div>
                <div class="row">
                    <div class="col-8">
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Save</button>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <br>
                        <center>
                            <p>Click here to <a href="{{route('login')}}">Login</a></p>
                        </center>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </form>

        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $(document).ready(function (e) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('[data-mask]').inputmask();

        $('#phone_number').inputmask({mask: "999-999-9999"});

        $('#dial_code').change(function () {
            var $dialCode = $(this).val();
            $.ajax({
                url: "{{ route("get-phone-format-by-dial-code") }}",
                type: "POST",
                data: {dial_code: $dialCode},
                success: function (result) {
                    if (result.countryPhoneFormat != null) {
                        let $phoneFormat = result.countryPhoneFormat.format;
                        $('#phone_number').inputmask({mask: $phoneFormat});
                    } else {
                        $('#phone_number').inputmask({mask: "999-999-9999"});
                    }
                }
            });

        });
    });
</script>
</body>
</html>
