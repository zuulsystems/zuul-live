<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zuul Systems Admin | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <style>
        body {
            background: url("images/bg-log.jpg") no-repeat !important;
            background-position-x: center !important;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img class="img-fluid" src="{{ url('images/logo.png') }}">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            @if(session()->has('success'))
                <div class="alert alert-success">{{session()->get('success')}}</div>
            @endif
            @if(session()->has('deactivated'))
                <div class="alert alert-danger">{{session()->get('deactivated')}}</div>
            @endif
            <form action="{{route('login')}}" method="post">
                @csrf
                <label>LOGIN AS {{old('userType')}}</label>
                <div class="input-group mb-3">
                    <select class="form-control" name="userType" id="userType">
                        <option value="guest_resident" {{ old('userType') == "guest_resident" ? 'selected' : '' }} >
                            Guest / Resident
                        </option>
                        <option value="admin" {{ old('userType') == "admin" ? 'selected' : '' }}>Admin</option>
                        <option value="guard" {{ old('userType') == "guard" ? 'selected' : '' }}>Guard</option>
                        <option value="kiosk" {{ old('userType') == "kiosk" ? 'selected' : '' }}>Kiosk</option>
                    </select>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                @if(old('userType') != "Admin")
                @elseif(old('userType') != "kiosk")
                @endif
                <div class="input-group mb-3"
                     id="email-div" {{ old('userType') == "admin" || old('userType') == "kiosk" ? '' : "style=display:none" }}>
                    <input type="email" class="form-control" name="email" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="mb-3"
                     id="dial_code" {{ old('userType') == "admin" || old('userType') == "kiosk" ? "style=display:none" : '' }}>
                    <label>Dial Code</label>
                    <select class="form-control" id="dial_code_select" name="dial_code_select">
                        @php
                            $countries = activeCountries();
                        @endphp
                        @foreach($countries as $country)
                            <option value="{{ $country->dial_code }}">+{{ $country->dial_code }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group mb-3"
                     id="phone-div" {{ old('userType') == "admin" || old('userType') == "kiosk" ? "style=display:none" : '' }}>

                    <input type="text" class="form-control" name="phone_number" placeholder="Phone Number"
                           id="phone_number">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-phone"></span>
                        </div>
                    </div>
                    @error('phone_number')
                    <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                    <span class="invalid-feedback d-block" role="alert">
                          <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-8">
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <center>
                            <a href="{{route('password.request')}}">I forgot my password</a>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <p>Not A Member? <a href="{{route('register')}}">Create Account</a></p>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </form>

        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $(document).ready(function (e) {


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#phone_number').inputmask({mask: "999-999-9999"})

        $('#userType').change(function () {
            let userType = $(this).val();
            if (userType === 'admin') {
                $('#email-div').show();
                $('input[name="phone_number"]').val("");
                $('#dial_code').hide();
                $('#phone-div').hide();
            } else if (userType === "kiosk") {
                $('#email-div').show();
                $('input[name="phone_number"]').val("");
                $('#dial_code').hide();
                $('#phone-div').hide();
            } else {
                $('#email-div').hide();
                $('input[name="email"]').val("");
                $('#dial_code').show();
                $('#phone-div').show();
            }
        });

        $('#dial_code_select').change(function () {

            var $dialCodeSelect = $(this).val();
            $.ajax({
                url: "{{ route("get-phone-format-by-dial-code") }}",
                type: "POST",
                data: {dial_code: $dialCodeSelect},
                success: function (result) {
                    if (result.countryPhoneFormat != null) {
                        let $phoneFormat = result.countryPhoneFormat.format;
                        $('#phone_number').inputmask({mask: $phoneFormat});
                    } else {
                        $('#phone_number').inputmask({mask: "999-999-9999"});
                    }
                }
            });

        });


    });
</script>
</body>
</html>
