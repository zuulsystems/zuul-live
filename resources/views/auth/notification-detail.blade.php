<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zuul Systems Admin | Register</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style type="text/css">
        .dropdown-item {
            display: block;
            padding: 12px;
            color: #fff !important;
            transition: all 0.3s ease;
            line-height: 25px;
            font-size: 15px;
            text-align: left;
        }

        .dropdown-item img {
            padding-right: 10px;
        }

        .dropdown-item:hover {
            opacity: 0.8 !important;
        }

        .btn-ios {
            width: 100%;
            background: #0a1318;
            margin: 0 auto;
            text-transform: uppercase;
            color: #fff;
            font-weight: 400;
            font-size: 12px;
            border-radius: 5px;
            position: relative;
            transition: all 0.2s ease-in;
            float: left;
            margin-bottom: 5px;
        }

        .btn-andr {
            width: 100%;
            background: #a6c839;
            margin: 0 auto;
            text-transform: uppercase;
            color: #fff;
            font-weight: 400;
            font-size: 12px;
            border-radius: 5px;
            position: relative;
            transition: all 0.2s ease-in;
            float: left;
            margin-bottom: 5px;
        }

        .btn-andr i, .btn-ios i {
            right: 5%;
            top: 20%;
            position: absolute;
        }


    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{route('login')}}"><b>Zuul Systems</b></a>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.reavertechnologies.zuulmaster"
               class="btn-andr dropdown-item">
                <img src="https://zuulsystems.com/wp-content/themes/zuul/images/iocn-z.png" class="pull-left"
                     alt="icon">Download For <br><span>Android</span><i class="fa fa-android fa-4x"
                                                                        aria-hidden="true"></i>
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <a target="_blank" href="https://apps.apple.com/us/app/zuul-systems/id1438385504"
               class="btn-ios dropdown-item">
                <img src="https://zuulsystems.com/wp-content/themes/zuul/images/iocn-z2.png" class="pull-left"
                     alt="icon">Download For <br><span>iOS</span><i class="fa fa-apple fa-4x" aria-hidden="true"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        @if(!$showNotificationDetail)
            <div class="card-body login-card-body">
                <p class="login-box-msg">ENTER YOUR CELL PHONE NUMBER BELOW</p>

                @if(session()->has('error'))
                    <div class="alert alert-danger">{{session()->get('error')}}</div>
                @endif
                <form action="{{route('notification-submit')}}" method="get">
                    @csrf

                    <div class="input-group mb-3"
                         id="phone-div" {{ old('userType') == "admin" ? "style=display:none" : '' }}>
                        <input type="hidden" name="notificationId" value="{{$notification->id ?? ""}}">

                        <input type="text" class="form-control" data-inputmask='"mask": "(999)-999-9999"' data-mask
                               name="phone_number" placeholder="">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-phone"></span>
                            </div>
                        </div>
                        @error('phone_number')
                        <span class="invalid-feedback d-block" role="alert">
                             <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>


                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <br>
                            <center>
                                <button type="submit" class="btn btn-primary btn-block">Check pass details</button>
                            </center>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </form>

            </div>
        @elseif($showNotificationDetail)
            <div class="card-body login-card-body">
                <div class="box-log" style="clear:both;">
                    <div class="container">
                        <h2><strong>Pass Details</strong></h2>
                        <img src="{{ $passUser['license_image'] ?? ""}}" width="100" height="100"/>
                        <br/><br/>
                        <h4><strong>{{ $passUser['event_name'] ?? ""}}</strong></h4>
                        Start <br/>
                        <strong>{{ $passUser['to_date'] . ' ' . $passUser['to_date_time'] }}</strong><br/>
                        Expires <br/>
                        <strong>{{ $passUser['for_date'] . ' ' . $passUser['for_date_time']}}</strong><br/>

                        <br/>
                        Sender Info <br/>
                        Sender Name <br/>
                        <strong>{{ $passUser['sender_name'] ?? "" }}</strong><br/>
                        Sender Phone # <br/>
                        <strong>{{ $passUser['sender_formatted_phone_number'] ?? ""}}</strong><br/>

                        <br/>
                        Community Name <br/>
                        <strong>{{ $passUser['sender_community_name']?? "" }}</strong><br/>

                        Address <br/>
                        <strong>{{ $passUser['description'] ?? "" }}</strong><br/>

                        <br/><br/><br/>
                        Please use the QR code to the right for scanning / verification <br/>
                        <img
                            src="http://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{ $passUser['qr_code']?? ""}}"
                            width="100" height="100"/>

                        <br/><br/>
                        <iframe
                            width="100%"
                            height="auto"
                            frameborder="0"
                            scrolling="no"
                            marginheight="0"
                            marginwidth="0"
                            src="https://maps.google.com/maps?q={{$passUser['lat']?? ""}},{{$passUser['lng']?? ""}}&hl=es;z=14&output=embed"
                        >
                        </iframe>
                    </div>
                </div>
            </div>
        @endif
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>

<script>
    $(document).ready(function (e) {
        $('[data-mask]').inputmask();
    });
</script>
</body>
</html>
