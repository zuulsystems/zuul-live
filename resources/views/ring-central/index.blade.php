<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ring Central</title>
</head>
<body>
<h1>Hello</h1>
<table>
    <tr>
        <th>Id</th>
        <th>play audio</th>
        <th>url</th>
    </tr>

    @forelse($result['records'] as $row)
        <tr>
            <td>{{$row['id']}}</td>
            <td></td>
            <td>
                @if(isset($row['message']) && !empty($row['message']))
                    <audio
                        src="{{$row['message']['uri'].'/content/'.$row['message']['id'].'?access_token='.$token}}"
                        controls>
                    </audio>
                    {{$row['message']['uri'].'/content/'.$row['message']['id'].'?access_token='.$token}}
                @endif
            </td>
            <td></td>
        </tr>
    @empty
    @endforelse
</table>
</body>
</html>
