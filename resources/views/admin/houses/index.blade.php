@extends('admin.layout.master')
@section('content')
    @include('admin.layout.datatable-css')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Houses List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Houses List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="house_name">Residential Surname</label>
                                                <input type="text" name="house_name" id="house_name"
                                                       placeholder="Residential Surname" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_address">Address</label>
                                                <input type="text" name="full_address" id="full_address"
                                                       placeholder="Address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="lot_number">Lot Number</label>
                                                <input type="text" name="lot_number" id="lot_number"
                                                       placeholder="Lot Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community Name</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="phone">Primary Contact Information</label>
                                                <input type="text" name="phone" id="phone" placeholder="Phone"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="complete_profile">Completed Profile</label>
                                                <select class="form-control" name="complete_profile">
                                                    <option value="">All</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                                <h3 class="card-title">
                                    <a href="{{ route('admin.residential.houses.create') }}" class="add_advertiser">
                                        <i class="fas fa-plus-circle"></i> Add House
                                    </a>
                                    <a href="javascript:void(0)" class="" data-toggle="modal"
                                       data-target="#house-import-modal">
                                        <i class="fas fa-file-import"></i> import House
                                    </a>
                                    <a href="{{ route('admin.residential.house-imported-files') }}" class="">
                                        <i class="fas fa-file"></i> Imported Files
                                    </a>
                                </h3>
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="housesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Resident Surname</th>
                                    <th>Address</th>
                                    <th>Lot Number</th>
                                    <th>Community Name</th>
                                    <th>Household Member(s)</th>
                                    <th>Completed Profile</th>
                                    <th>Primary Contact Information</th>
                                    @cannot('is-guard-admin')
                                        <th>Action(s)</th>
                                    @endcannot
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @php
        $showClass = '';
      $housesImportModel = '';
      if ($errors->any())
      {
      $showClass = 'show';
      $housesImportModel = 'display: block';
      }
    @endphp
        <!-- House Model--->
    <div class="modal fade" id="house-import-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import CSV</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            id="closeHousesImportModalBtn">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{ route('admin.residential.import-houses') }}"
                      enctype="multipart/form-data" class="ajax-form">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="house_csv">CSV file to import</label>
                                    <input type="file" name="house_csv" id="house_csv" class="col-md-12" accept=".csv"/>
                                </div>
                            </div>
                        </div>
                        <span class="text-danger" id="field"></span>

                        @if(auth()->user()->role_id == 1)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach ($communities as $community)
                                                <option value="{{ $community->id }}">{{ $community->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('community_id'))
                                            <span style="color:red;padding-top:2px"
                                                  class="error a">Select a community.</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="fileUploadInformationTable">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <a href="{{ asset('assets/csv-sample/house.csv') }}" class="btn btn-default">Download Sample
                            File</a>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@php
    if(session()->has('housesImportedSuccessfully'))
    {
@endphp
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-success" aria-live="polite" style="display: block; opacity: 1;">
        <div class="toast-message">Houses imported successfully.</div>
    </div>
</div>
<script>
    setTimeout(function () {
        document.getElementsByClassName('toast-success')[0].style.display = "none";
    }, 1000);
</script>
@php
    }
@endphp
@push('scripts')
    <script src="{{asset('assets/admin/dist/js/main17-6-2022.js')}}"></script>
    <script>
        let columns = [{
            data: 'id',
        },
            {
                data: 'house_name',
            },
            {
                data: 'house_detail',
            },
            {
                data: 'lot_number',
                name: 'lot_number_ten_digits'
            },
            {
                data: 'community_name',
                name: "community.name"
            },
            {
                data: "formatted_residents_count",
                name: "formatted_residents_count",
                sortable: false
            },
            {
                data: 'complete_profile',
                name: 'completed_profile'
            },
            {
                data: 'phone',
            },
        ];

        var isGuard = "{{ Gate::allows('is-guard-admin') }}";

        if (isGuard == false) {
            var links = {
                data: function (data) {
                    return data.links;
                }
            };
            columns.push(links);
        }


        zuulDataTableSortableGuestLogs('#housesTable',
            "{{ route('admin.residential.houses-data') }}?communityId={{ $communityId ?? null }}&houseId={{ $houseId ?? null }}", columns, [8]);

        $('.search-btn').click(function () {
            var ajax = "{{ route('admin.residential.houses-data') }}?communityId={{ $communityId ?? null }}&houseId={{ $houseId ?? null }}&" +
                $("#search-filter").serialize();
            zuulDataTableSortableGuestLogs('#housesTable', ajax, columns, [8]);
        })

        $('#closeHousesImportModalBtn').click(function () {
            $("#house-import-modal").css("display", "none");
            $('.a').css('display', 'none');
        });

        $('#notificationUl').css('margin-top', '57px');

    </script>
@endpush
