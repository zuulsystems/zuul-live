@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Houses List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Houses List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="house_name">Residential Surname</label>
                                                <input type="text" name="house_name" id="house_name"
                                                       placeholder="Residential Surname" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_address">Address</label>
                                                <input type="text" name="full_address" id="full_address"
                                                       placeholder="Address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="lot_number">Lot Number</label>
                                                <input type="text" name="lot_number" id="lot_number"
                                                       placeholder="Lot Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community Name</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="phone">Primary Contact Information</label>
                                                <input type="text" name="phone" id="phone" placeholder="Phone"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.houses.create')}}" class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Add House
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="housesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Resident Surename</th>
                                    <th>Address</th>
                                    <th>Lot Number</th>
                                    <th>Community Name</th>
                                    <th>Household Member(s)</th>
                                    <th>Primary Contact Information</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'house_name',
            },
            {
                data: 'full_address',
            },
            {
                data: 'lot_number',
            },
            {
                data: 'community_name',
            },
            {
                data: 'residents_count',
            },
            {
                data: 'phone',
            },
            {
                data: function (data) {
                    return data.edit_link;
                }
            },
        ];
        zuulDataTable('#housesTable', "{{route('admin.residential.houses-data')}}", columns, [7]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.residential.houses-data')}}?" + $("#search-filter").serialize();
            zuulDataTable('#housesTable', ajax, columns, [7]);
        })
    </script>
@endpush
