@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit House Csv</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit House Csv</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{ route('admin.residential.houses.index') }}" class="add_advertiser">
                                    <i class="fas fa-list"></i> House List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{ route('admin.residential.house-csvs-table-record-update', [$houseCsv->id]) }}"
                                  class="ajax-form"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Residential Sur Name </label>
                                        <input type="text" name="house_name" id="house_name"
                                               placeholder="Residential Sur Name" class="form-control"
                                               value="{{ $houseCsv->house_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="house_detail">Address </label>
                                        <textarea class="form-control" name="address"
                                                  id="address">{{ $houseCsv->address }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Lot Number </label>
                                        <input type="text" name="lot_number" id="lot_number" placeholder="Lot Number"
                                               class="form-control" value="{{ $houseCsv->lot_number }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Contact Number </label>
                                        <input type="text" class="form-control" name="phone" id="phone"
                                               value="{{ $houseCsv->primary_contact }}"/>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save Changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
