@extends('admin.layout.master')
@push('css')
    <link href="{{asset('assets/admin/plugins/clock/timedropper.css')}}" rel="stylesheet">
@endpush
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>RFID Schedules | {{$rfid->rfid_tag_num}}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">RFID Schedules</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="schedule_name">Schedule Name</label>
                                                <input type="text" name="schedule_name" id="schedule_name"
                                                       placeholder="Schedule Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="start_date">Start Date</label>
                                                <input type="date" name="start_date" id="start_date"
                                                       placeholder="Start Date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="start_date">End Date</label>
                                                <input type="date" name="end_date" id="end_date"
                                                       placeholder="End Date" class="form-control">
                                            </div>
                                        </div>


                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.rfid.rfids-schedules.create')."?".Request::getQueryString()}}"
                                   class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Add RFID Schedules
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="rfidScheduleTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Start - End</th>
                                    <th>Days</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <!-- Time Model--->
    <div class="modal fade" id="time-model">
        <div class="modal-dialog modal-lg" style="max-width: 900px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Schedule Entry | <span id="schedule_name_label"></span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" id="schedule_form" method="post" action="javascript:void(0)"
                      class="ajax-form">
                    <input type="hidden" name="rfid_date_limit_id" id="rfid_date_limit_id" value=""/>
                    <div class="modal-body">
                        <div class="px-3">

                            <div class="row">
                                <input type="hidden" id="clock_start_time" value=""/>
                                <input type="hidden" id="clock_end_time" value=""/>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> Checked days of week with time means they are enabled, unchecked will
                                            keep record of data but that day will be disabled on schedule </label>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Monday </label>
                                            <div class="input-group">
                                <span class="input-group-addon  beautiful">
                                    <input type="checkbox" name="monday_status" id="monday_status" checked>
                                </span>
                                                <input type="text" value="00:00" id="monday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="monday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label> Tuesday </label>
                                            <div class="input-group">
                                <span class="input-group-addon beautiful">
                                    <input type="checkbox" id="tuesday_status" checked>
                                </span>
                                                <input type="text" value="00:00" id="tuesday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="tuesday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Wednesday </label>
                                            <div class="input-group">
                            <span class="input-group-addon beautiful">
                                <input type="checkbox" id="wednesday_status" checked>
                            </span>
                                                <input type="text" value="00:00" id="wednesday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="wednesday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label> Thursday </label>
                                            <div class="input-group">
                            <span class="input-group-addon beautiful">
                                <input type="checkbox" id="thursday_status" checked>
                            </span>
                                                <input type="text" value="00:00" id="thursday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="thursday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Friday </label>
                                            <div class="input-group">
                            <span class="input-group-addon beautiful">
                                <input type="checkbox" id="friday_status" checked>
                            </span>
                                                <input type="text" value="00:00" id="friday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="friday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label> Saturday </label>
                                            <div class="input-group">
                            <span class="input-group-addon beautiful">
                                <input type="checkbox" id="saturday_status" checked>
                            </span>
                                                <input type="text" value="00:00" id="saturday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="saturday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Sunday </label>
                                            <div class="input-group">
                            <span class="input-group-addon beautiful">
                                <input type="checkbox" id="sunday_status" checked>
                            </span>
                                                <input type="text" value="00:00" id="sunday_start"
                                                       class="form-control onlytimepicker">
                                                <input type="text" value="00:00" id="sunday_end"
                                                       class="form-control onlytimepicker">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>WARNING</label>
                                            <button type="button" onclick="deleteTimeLogsOfSchedule()"
                                                    class="btn btn-danger action-btn">Delete All Weekly Schedule
                                            </button>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-6">
                                    <div id="onewheel" class="timerangewheel" style="float: right"></div>
                                    <div class="start"></div>
                                    <div class="end"></div>
                                    <div class="duration"></div>
                                    <br>
                                    <label><input id="knob_alldays" type="checkbox" name="check" checked> <span
                                            class="label-text">All days</span></label>
                                    <label><input id="knob_monday" type="checkbox" name="check" checked> <span
                                            class="label-text">Monday</span></label>
                                    <label><input id="knob_tuesday" type="checkbox" name="check" checked> <span
                                            class="label-text">Tuesday</span></label>
                                    <label><input id="knob_wednesday" type="checkbox" name="check" checked> <span
                                            class="label-text">Wednesday</span></label>
                                    <label><input id="knob_thursday" type="checkbox" name="check" checked> <span
                                            class="label-text">Thursday</span></label>
                                    <label><input id="knob_friday" type="checkbox" name="check" checked> <span
                                            class="label-text">Friday</span></label>
                                    <label><input id="knob_saturday" type="checkbox" name="check" checked> <span
                                            class="label-text">Saturday</span></label>
                                    <label><input id="knob_sunday" type="checkbox" name="check" checked> <span
                                            class="label-text">Sunday</span></label>

                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onclick="setTimeLogsOfSchedule()" class="btn btn-primary">Save changes
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script src="{{asset('assets/admin/plugins/knob/d3.js')}}"></script>

    <script src="{{asset('assets/admin/plugins/knob/plugin.min.js')}}"></script>
    <script src="{{ asset('assets/admin/plugins/clock/timedropper.js')}}"></script>

    <script>
        $(document).ready(function () {
            $(".onlytimepicker").timeDropper({
                setCurrentTime: true
            });
            $(document).on('click', '.btn-time-model', function () {
                let rfid_date_limit_id = $(this).data('id');
                showLoader();
                $.ajax({
                    method: 'GET',
                    url: '{{route('admin.rfid.get-rfid-schedule-week')}}',
                    data: {
                        rfid_date_limit_id: rfid_date_limit_id
                    },
                    success: function (data) {
                        hideLoader();
                        let schedules = data.schedules;
                        let limit = data.limit;
                        $("#schedule_name_label").html(limit.name);
                        $("#schedule_form #rfid_date_limit_id").val(limit.id);

                        if (schedules.length == 0) {
                            $('#time-model').modal('show');
                        } else {
                            setTimeLogSavedValues(schedules);
                            $('#time-model').modal('show');
                        }

                    }
                });

            });
            $(".timerangewheel").timerangewheel({
                indicatorWidth: 12,
                handleRadius: 15,
                handleStrokeWidth: 1,
                accentColor: '#2C3E50',
                handleIconColor: "#fff",
                handleStrokeColor: "#fff",
                handleFillColorStart: "#374149",
                handleFillColorEnd: "#374149",
                tickColor: "#8a9097",
                rangeTotal: 12,
                indicatorBackgroundColor: "#E74C3C",
                data: {"start": "00:00", "end": "12:00"},
                onChange: function (timeObj) {
                    $(".start").html("Start: " + convert24to12Hours(timeObj.start));
                    $(".end").html("End: " + convert24to12Hours(timeObj.end));
                    $("#clock_start_time").val(timeObj.start).trigger('change');
                    $("#clock_end_time").val(timeObj.end).trigger('change');
                    $(".duration").html("Duration: " + timeObj.duration);
                }
            });


            initializeTimes("12:00 am", "12:00 pm")
            $(document).on('change', '#knob_alldays', function (e) {
                setAllDaysChecked($(this).is(':checked'))
            });

            $(document).on('change', '#knob_monday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#knob_tuesday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#knob_wednesday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#knob_thursday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#knob_friday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#knob_saturday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#knob_sunday', function (e) {
                $("#knob_alldays").prop("checked", false);
            });

            $(document).on('change', '#clock_start_time', function (e) {

                if ($("#knob_monday").is(':checked')) {
                    $("#monday_start").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_tuesday").is(':checked')) {
                    $("#tuesday_start").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_wednesday").is(':checked')) {
                    $("#wednesday_start").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_thursday").is(':checked')) {
                    $("#thursday_start").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_friday").is(':checked')) {
                    $("#friday_start").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_saturday").is(':checked')) {
                    $("#saturday_start").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_sunday").is(':checked')) {
                    $("#sunday_start").val(convert24to12Hours($(this).val()))
                }

            });

            $(document).on('change', '#clock_end_time', function (e) {

                if ($("#knob_monday").is(':checked')) {
                    $("#monday_end").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_tuesday").is(':checked')) {
                    $("#tuesday_end").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_wednesday").is(':checked')) {
                    $("#wednesday_end").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_thursday").is(':checked')) {
                    $("#thursday_end").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_friday").is(':checked')) {
                    $("#friday_end").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_saturday").is(':checked')) {
                    $("#saturday_end").val(convert24to12Hours($(this).val()))
                }
                if ($("#knob_sunday").is(':checked')) {
                    $("#sunday_end").val(convert24to12Hours($(this).val()))
                }
            });

        });

        function convert24to12Hours(time) {
            // Check correct time format and split into components
            time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

            if (time.length > 1) { // If time format correct
                time = time.slice(1);  // Remove full string match value
                time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM
                time[0] = +time[0] % 12 || 12; // Adjust hours
            }


            return time.join(''); // return adjusted time or original string
        }

        function initializeTimes($start, $end) {

            $("#monday_start").val(convert24to12Hours($start))
            $("#tuesday_start").val(convert24to12Hours($start))
            $("#wednesday_start").val(convert24to12Hours($start))
            $("#thursday_start").val(convert24to12Hours($start))
            $("#friday_start").val(convert24to12Hours($start))
            $("#saturday_start").val(convert24to12Hours($start))
            $("#sunday_start").val(convert24to12Hours($start))

            $("#monday_end").val(convert24to12Hours($end))
            $("#tuesday_end").val(convert24to12Hours($end))
            $("#wednesday_end").val(convert24to12Hours($end))
            $("#thursday_end").val(convert24to12Hours($end))
            $("#friday_end").val(convert24to12Hours($end))
            $("#saturday_end").val(convert24to12Hours($end))
            $("#sunday_end").val(convert24to12Hours($end))

        }

        function setAllDaysChecked(flag) {
            $("#knob_monday").prop("checked", flag);
            $("#knob_tuesday").prop("checked", flag);
            $("#knob_wednesday").prop("checked", flag);
            $("#knob_thursday").prop("checked", flag);
            $("#knob_friday").prop("checked", flag);
            $("#knob_saturday").prop("checked", flag);
            $("#knob_sunday").prop("checked", flag);
        }

        function setTimeLogSavedValues($schedules) {

            $schedule_form = $("#schedule_form");

            $schedules.forEach(element => {

                switch (element.day) {
                    case 1: // monday
                        $schedule_form.find("#monday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#monday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#monday_status").prop("checked", element.status == 1);
                        break;
                    case 2: // tuesday
                        $schedule_form.find("#tuesday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#tuesday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#tuesday_status").prop("checked", element.status == 1);
                        break;
                    case 3: // wednesday
                        $schedule_form.find("#wednesday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#wednesday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#wednesday_status").prop("checked", element.status == 1);
                        break;
                    case 4: // thursday
                        $schedule_form.find("#thursday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#thursday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#thursday_status").prop("checked", element.status == 1);
                        break;
                    case 5: // friday
                        $schedule_form.find("#friday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#friday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#friday_status").prop("checked", element.status == 1);
                        break;
                    case 6: // saturday
                        $schedule_form.find("#saturday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#saturday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#saturday_status").prop("checked", element.status == 1);
                        break;
                    case 7: // sunday
                        $schedule_form.find("#sunday_start").val(convert24to12Hours(element.start_time));
                        $schedule_form.find("#sunday_end").val(convert24to12Hours(element.end_time));
                        $schedule_form.find("#sunday_status").prop("checked", element.status == 1);
                        break;
                }

            });

        }

        function setTimeLogsOfSchedule() {

            $schedule_form = $("#schedule_form");
            var schedule_id = $("#schedule_form #rfid_date_limit_id").val();
            var days = [
                {
                    schedule_id: schedule_id,
                    day: "monday",
                    start_time: $schedule_form.find("#monday_start").val(),
                    end_time: $schedule_form.find("#monday_end").val(),
                    status: $schedule_form.find("#monday_status").is(':checked')
                },
                {
                    schedule_id: schedule_id,
                    day: "tuesday",
                    start_time: $schedule_form.find("#tuesday_start").val(),
                    end_time: $schedule_form.find("#tuesday_end").val(),
                    status: $schedule_form.find("#tuesday_status").is(':checked')
                },
                {
                    schedule_id: schedule_id,
                    day: "wednesday",
                    start_time: $schedule_form.find("#wednesday_start").val(),
                    end_time: $schedule_form.find("#wednesday_end").val(),
                    status: $schedule_form.find("#wednesday_status").is(':checked')
                },
                {
                    schedule_id: schedule_id,
                    day: "thursday",
                    start_time: $schedule_form.find("#thursday_start").val(),
                    end_time: $schedule_form.find("#thursday_end").val(),
                    status: $schedule_form.find("#thursday_status").is(':checked')
                },
                {
                    schedule_id: schedule_id,
                    day: "friday",
                    start_time: $schedule_form.find("#friday_start").val(),
                    end_time: $schedule_form.find("#friday_end").val(),
                    status: $schedule_form.find("#friday_status").is(':checked')
                },
                {
                    schedule_id: schedule_id,
                    day: "saturday",
                    start_time: $schedule_form.find("#saturday_start").val(),
                    end_time: $schedule_form.find("#saturday_end").val(),
                    status: $schedule_form.find("#saturday_status").is(':checked')
                },
                {
                    schedule_id: schedule_id,
                    day: "sunday",
                    start_time: $schedule_form.find("#sunday_start").val(),
                    end_time: $schedule_form.find("#sunday_end").val(),
                    status: $schedule_form.find("#sunday_status").is(':checked')
                }
            ];

            showLoader();
            $.ajax({
                method: 'POST',
                data: {
                    days: days,
                    rfid_date_limit_id: schedule_id
                },
                url: '{{route('admin.rfid.set-rfid-schedule-week')}}'
            });

        }

        function deleteTimeLogsOfSchedule() {
            $schedule_form = $("#schedule_form");
            let rfid_date_limit_id = $("#schedule_form #rfid_date_limit_id").val();
            $.ajax({
                method: 'POST',
                url: '{{route('admin.rfid.delete-rfid-schedule-week')}}',
                data: {
                    rfid_date_limit_id: rfid_date_limit_id
                }
            });
        }

        let columns = [{
            data: 'id',
        },
            {
                data: 'name',
            },
            {
                data: 'start_end',
            },
            {
                data: 'schedule_days',
            },
            {
                data: function (data) {
                    return data.links;
                }
            },

        ];

        zuulDataTable('#rfidScheduleTable',
            "{{ route('admin.rfid.rfids-schedule-data') }}?rfid_id={{ $rfid->id ?? null }}", columns, [4]);

        $('.search-btn').click(function () {
            var ajax = "{{ route('admin.rfid.rfids-schedule-data') }}?rfid_id={{ $rfid->id ?? null }}&" +
                $("#search-filter").serialize();
            zuulDataTable('#rfidScheduleTable', ajax, columns, [5]);
        })

    </script>
@endpush
