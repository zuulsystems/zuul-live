@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Schedule</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Schedule</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.rfid.rfids-schedules.index')."?".Request::getQueryString()}}"
                                   class="add_advertiser">
                                    <i class="fas fa-list"></i> RFID Schedules
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.rfid.rfids-schedules.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <input type="hidden" name="rfid_id" value="{{request('rfid_id')}}">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="name">Name </label>
                                                        <input type="text" name="name" id="name"
                                                               placeholder="Scheduler Name" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="start_end">Date Range</label>
                                                        <input type="text" class="form-control" name="start_end"
                                                               id="start_end"/>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>


                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')

    <script>
        //Date range picker
        $('#start_end').daterangepicker({
            startDate: moment(),
            endDate: moment().add(1, 'year')
        })

    </script>
@endpush
