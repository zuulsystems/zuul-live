@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Network</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Network</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.network.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Network List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.network.update',$network->id)}}"
                                  class="ajax-form" enctype="multipart/form-data">

                                {{ method_field('PUT') }}

                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="registered_ip">IP Address</label>
                                                <input type="text" name="registered_ip" id="registered_ip"
                                                       placeholder="IP Address"
                                                       class="form-control" value="{{ $network->registered_ip }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Community</label>
                                                <select class="form-control" name="community_id" id="community_id">
                                                    @foreach($communities as $community)
                                                        <option
                                                            value="{{ $community->id }}" {{ $network->community_id == $community->id ? "selected" : "" }}>{{ $community->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6" style="display: none" id="phone_number">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" name="phone_number" class="form-control"
                                                       data-inputmask='"mask": "999-999-9999"' data-mask
                                                       placeholder="Phone Number"/>
                                            </div>
                                        </div>

                                        <div class="col-md-6" style="display: none" id="ip_address">
                                            <div class="form-group">
                                                <label for="ip_address">IP Address</label>
                                                <input type="text" name="ip_address" id="ip_address"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#attach_to').change(function () {
                var $attachTo = $(this).val();
                if ($attachTo == "device") {
                    $('#phone_number').css('display', 'none');
                    $('#ip_address').css('display', 'none');
                    $('#mac_address').css('display', 'block');
                } else if ($attachTo == "user") {
                    $('#mac_address').css('display', 'none');
                    $('#ip_address').css('display', 'none');
                    $('#phone_number').css('display', 'block');
                } else if ($attachTo == "network") {
                    $('#mac_address').css('display', 'none');
                    $('#phone_number').css('display', 'none');
                    $('#ip_address').css('display', 'block');
                }
            });
        });

    </script>
@endpush
