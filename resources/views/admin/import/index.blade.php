@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Import List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Import List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body collapse">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="import_name">Import Name</label>
                                                <input type="text" name="import_name" id="import_name"
                                                       placeholder="Import Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="file_name">File Name</label>
                                                <input type="text" name="file_name" id="file_name"
                                                       placeholder="File Name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{ route('admin.import.create') }}">
                                    <i class="fas fa-plus-circle"></i> Add Table CSV
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="housesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Import Date</th>
                                    <th>Import Name</th>
                                    <th>File Name</th>
                                    <th>Status</th>
                                    <th>Total Rec</th>
                                    <th>Import</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- House Model--->

@endsection
@push('scripts')
    <script>
        let columns = [{
            data: 'id',
        },
            {
                data: 'import_date',
            },
            {
                data: 'module_name',
            },
            {
                data: 'file_name',
            },
            {
                data: 'status',
            },
            {
                data: 'total_records',
            },
            {
                data: function (data) {
                    return data.links;
                }
            },

        ];

        zuulDataTable('#housesTable',
            "{{ route('admin.import-data') }}?communityId={{ $communityId ?? null }}", columns, [6]);

        $('.search-btn').click(function () {
            var ajax = "{{ route('admin.import-data') }}?communityId={{ $communityId ?? null }}&" +
                $("#search-filter").serialize();
            zuulDataTable('#housesTable', ajax, columns, [6]);
        });

        $(document).on('click', '.start_import', function () {
            let module = $(this).data('id');
            let url = "{{ route('admin.import.show',':id') }}";
            showLoader();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', module),
            });
        });

    </script>
@endpush
