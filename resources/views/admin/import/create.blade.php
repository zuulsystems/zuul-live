@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add CSV Table</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add CSV Table</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.import.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Import List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.import.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Select Excel/CSV File To Upload </label>
                                        <input type="file" name="table_csv" id="table_csv"
                                               class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="module_name">Model</label>
                                        <select name="module_name" id="module_name" class="form-control">
                                            <option value="communities">Communities</option>
                                            <option value="houses">Houses</option>
                                            <option value="scanners">Scanners</option>
                                            <option value="announcements">Announcements</option>
                                            <option value="auth_logs">Auth Logs</option>
                                            <option value="users">Users</option>
                                            <option value="users_profile">Users Profile</option>
                                            <option value="role_user">Role User</option>
                                            <option value="update_guard_admin">Update Role for Guard Admin</option>
                                            <option value="contact_collections" data-select2-id="3">Contact
                                                Collections
                                            </option>
                                            <option value="contacts" data-select2-id="3">Contacts</option>
                                            <option value="contact_groups" data-select2-id="3">Contact Groups</option>
                                            <option value="user_contacts" data-select2-id="3">User Contacts</option>
                                            <option value="vehicles" data-select2-id="3">Vehicles</option>
                                            <option value="events" data-select2-id="3">Events</option>
                                            <option value="passes" data-select2-id="3">Passes</option>
                                            <option value="pass_users" data-select2-id="3">Pass Users</option>
                                            <option value="pass_vehicles" data-select2-id="3">Pass Vehicles</option>
                                            <option value="pass_requests" data-select2-id="3">Pass Request</option>
                                            <option value="scan_logs" data-select2-id="3">Scan Logs</option>
                                            <option value="dashboard_settings" data-select2-id="3">Dashboard Settings
                                            </option>
                                            <option value="parental_controls" data-select2-id="3">Parental Controls
                                            </option>
                                            <option value="email_templates" data-select2-id="3">Email Templates</option>
                                            <option value="device_types" data-select2-id="3">Device Types</option>
                                            <option value="zuul_notifications" data-select2-id="3">Zuul Notifications
                                            </option>
                                            <option value="parental_control_notifications" data-select2-id="3">Parental
                                                Control Notifications
                                            </option>
                                            <option value="sms_logs" data-select2-id="3">Sms Logs</option>
                                            <option value="traffic_locations" data-select2-id="8">Traffic Camera
                                                Locations
                                            </option>
                                            <option value="traffic_tickets" data-select2-id="8">Traffic Tickets</option>
                                            <option value="traffic_ticket_images" data-select2-id="8">Traffic Ticket
                                                Images
                                            </option>
                                            <option value="traffic_ticket_templates" data-select2-id="8">Traffic Ticket
                                                Email Templates
                                            </option>
                                            <option value="traffic_ticket_payment_logs" data-select2-id="8">Traffic
                                                Ticket Payment Logs
                                            </option>
                                            <option value="traffic_ticket_email_logs" data-select2-id="8">Traffic Ticket
                                                Email Logs
                                            </option>
                                            <option value="vendors" data-select2-id="8">Vendors</option>
                                            <option value="vendor_vehicles" data-select2-id="8">Vendor Vehicles</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Upload Table </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
