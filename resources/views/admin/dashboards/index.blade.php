@extends('admin.layout.master')
@push('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
@endpush

<style>
    .container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
        width: 100%;
        padding-right: 7.5px;
        padding-top: 10.5px;
        padding-bottom: 5px;
        padding-left: 7.5px;
        margin-right: auto;
        margin-left: auto;
        border: 2px solid #ede7e7;
        background: white !important;
        border-radius: 7px !important;
    }
</style>
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dashboard Setting</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard Setting</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

        </section>
    </div>
@endsection
@push('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        // Summernote
        $('.text-editor').summernote();
    </script>
@endpush
