@extends('admin.layout.master')
@section('content')
    <style>
        #notification-list li {
            padding-left: 10px;
        }

        .nav-stacked {
            display: block;
        }

        .nav-stacked li span {
            float: right;
        }

        .nav-stacked li a i {
            font-size: 10px;
        }

        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid card-bg-heading">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    @foreach($widgets as $widget)
                        <div class="col-lg-3 col-6 mb-3">
                            <a href="{{ $widget->url }}">
                                <div class="category-box d-block d-md-flex">
                                    <div class="icon-container category-bg-green"
                                         style="background-color: {{ $widget->color }}!important">
                                        <i class="{{ $widget->icon }}"></i>
                                    </div>
                                    <div class="value-container">
                                        @foreach($widget_names as $key=>$widget_name)
                                            @if($key == $widget->widget_name)
                                                <h3>{{ $widget_name }}</h3>
                                            @endif
                                        @endforeach
                                        @foreach($widget_names as $key=>$widget_name)
                                            @if($key == $widget->widget_name)
                                                @php
                                                    $widgetName = str_replace('_',' ',$widget->widget_name);
                                                    $widgetName = ucwords($widgetName);
                                                @endphp
                                                <p>{{  $widgetName }}</p>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    <!-- ./col -->
                </div>

                <div class="row c1">
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                        <select class="form-control" id="camera_location_marker">
                        </select>
                    </div>
                    <div class="col-md-3">
                        @can('super-admin')
                            <select class="form-control " id="camera_location_community">
                                @if(!auth()->user()->community_id)
                                    <option value="">-- All Communities --</option>
                                @endif
                                @foreach ($cameraCommunityList as $item)
                                    <option value="{{$item->id}}" @if($item->id == auth()->user()->community_id)
                                        'selected'
                                    @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        @endcan
                        @if(auth()->user()->cannot('super-admin'))
                            <input type="hidden" id="camera_location_community"
                                   value="{{ auth()->user()->community_id }}">
                        @endif
                    </div>
                </div>
                <br>
                @canany(['super-admin','is-tl-right'])
                    <div class="row c1 card-bg-heading">
                        <div class="col-md-12">
                            <h5>Camera Location Tickets</h5>
                        </div>
                    </div>
                    <br>
                    <div class="row c1">
                        <div class="col-md-4" id="chartSize">
                            <div class="chart-responsive">
                                <canvas id="chartTickets" height="160" width="205"
                                        style="width: 205px; height: 160px;"/>
                            </div>
                            <!-- ./chart-responsive -->
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="{{url('admin/traffic-tickets/resident-tickets')}}">
                                        <i class="fa fa-circle text-red"></i>
                                        Resident <img style="width: 20px" id="resident-loader"
                                                      src="{{ asset('images/spinner.gif') }}"/>
                                        <span class="pull-right text-red"><span id="residents"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/traffic-tickets/vendor-tickets')}}">
                                        <i class="fa fa-circle text-green"></i>
                                        Vendor <img style="width: 20px" id="vendor-loader"
                                                    src="{{ asset('images/spinner.gif') }}"/>
                                        <span class="pull-right text-green"><span id="vendors"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/traffic-tickets/guest-tickets')}}">
                                        <i class="fa fa-circle text-yellow"></i>
                                        Guest <img style="width: 20px" id="guest-loader"
                                                   src="{{ asset('images/spinner.gif') }}"/>
                                        <span class="pull-right text-yellow"><span id="guests"></span></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('admin/traffic-tickets/unmatched-tickets')}}">
                                        <i class="fa fa-circle text-aqua"></i>
                                        Unmatched <img style="width: 20px" id="unmatched-loader"
                                                       src="{{ asset('images/spinner.gif') }}"/>
                                        <span class="pull-right text-aqua"><span id="flag_tickets"></span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <br>
                            <div id="map" style="width: 100%; height: 430px;"></div>
                        </div>
                    </div>
                    <br>
                @endcanany
                @canany(['super-admin','is-zuul-right'])
                    <div class="row card-bg-heading">
                        <div class="col-md-2">
                            <h5>Scan Logs</h5>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-3">
                                    <select class="form-control" id="graphCategory">
                                        <option value="0">-- All --</option>
                                        <option value="1">Resident</option>
                                        <option value="2">Guest</option>
                                        <option value="3">Vendor</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    @can('super-admin')
                                        <select class="form-control" id="community">
                                            @if(!auth()->user()->community_id)
                                                <option value="0">-- All Communities--</option>
                                            @endif
                                            @foreach ($cameraCommunityList as $item)
                                                <option
                                                    value="{{$item->id}}" @if($item->id == auth()->user()->community_id)
                                                    'selected'
                                                @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    @endcan
                                    @cannot('super-admin')
                                        <input type="hidden" id="community" value="{{ auth()->user()->community_id }}">
                                    @endcannot
                                </div>
                                <div class="col-md-5">
                                    <div id="reportrange"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="fa fa-calendar"></i>&nbsp;
                                        <span></span> <i class="fa fa-caret-down"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                <strong>Passes Scanned: <span id="passDateRange"></span></strong>
                            </p>

                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="passChart" style="height: 180px; width: 703px;" height="180"
                                        width="703"></canvas>
                            </div>

                            @if(auth()->user()->role_id != 1)
                                <div class="chart">
                                    <!-- Sales Chart Canvas -->
                                    <canvas id="passChart1" style="height: 180px; width: 703px;" height="180"
                                            width="703"></canvas>
                                </div>
                            @endif
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>
                @endcanany
                <br>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA&libraries=places"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

    <script>
        //to draw map
        function drawMap(data) {
            getResident();

            var locations = data;
            var html = "<option value='0'>-- All Locations--</option>";

            for (i in data) {
                html += "<option value='' lat='" + data[i][1] + "' lng='" + data[i][2] + "'>" + data[i][0] + "</option>"
            }

            $('#camera_location_marker').html(html);

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: new google.maps.LatLng(locations[0][1], locations[0][2]),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            var bounds = new google.maps.LatLngBounds();

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map
                });

                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    var html = "";
                    return function () {
                        var resident_count = 0;
                        var guest_count = 0;
                        var vendor_count = 0;
                        infowindow.setContent("Loading ...");
                        infowindow.open(map, marker);

                        $.ajax({
                            type: "post",
                            url: "{{ url('admin/get-traffic-summary') }}",
                            data: {
                                location_id: locations[i][3]
                            },
                            success: function (data) {
                                html = "<p><b>Name: </b>" + locations[i][0] + "</p>";
                                html += "<p><b>Unmatched Tickets: </b>" + data[3] + "</p>";
                                html += "<p><b>Resident Tickets: </b>" + data[0] + "</p>";
                                html += "<p><b>Guest Tickets: </b>" + data[1] + "</p>";
                                html += "<p><b>Vendor Tickets: </b>" + data[2] + "</p>";
                                infowindow.close();
                                infowindow.setContent(html);
                                infowindow.open(map, marker);

                            },
                            error: function (xhr, status, error) {
                                console.log(error);
                            }
                        });
                    }
                })(marker, i));


                google.maps.event.addListener(marker, 'mouseout', function () {
                    infowindow.close();
                });
            }

            map.fitBounds(bounds);

            $('#camera_location_marker').on('change', function () {
                var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                var lat = optionSelected.attr("lat");
                var lng = optionSelected.attr("lng");
                map.setZoom(13);
                map.panTo(new google.maps.LatLng(lat, lng));
            });
        }

        //locations
        function getCameraLocationTickets() {
            community_id = $('#camera_location_community').val();
            $.ajax({
                type: "GET",
                url: "{{ url('/admin/getCameraLocationMapData') }}?communityId=" + community_id,
                contentType: false,
                success: function (data) {
                    if (data.length > 0) {
                        drawMap(data);
                    } else {
                        $('.c1').css('display', 'none');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        }


        $(document).on('change', '#camera_location_community', function () {
            getCameraLocationTickets();
        });

        getCameraLocationTickets();

        var startdate = '';
        var enddate = '';

        $(function () {
            var start = moment().startOf('month');
            var end = moment();

            function cb(start, end) {

                startdate = start;
                enddate = end;
                community_id = $('#community').val();
                graphCategory = $('#graphCategory').val();

                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('#passDateRange').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D  MMMM, YYYY'))
                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin/getDashboardScanLogsCharts') }}",
                    data: {
                        startDate: start.format('YYYY-MM-DD'),
                        endDate: end.format('YYYY-MM-DD'),
                        communityId: community_id,
                        type: graphCategory
                    },
                    success: function (data) {
                        console.log(data);
                        drawPassesChart(data[0].label, data[0].resident, data[0].guest, data[0].vendor, 'passChart', 'line', true);
                        drawPassesChart(data[1].label, data[1].resident, data[1].guest, data[1].vendor, 'passChart1', 'bar', false);
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                maxDate: moment(),
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('#community').change(function () {
                cb(startdate, enddate);
            })

            $('#graphCategory').change(function () {
                cb(startdate, enddate);
            })

            cb(start, end);

        });

        function drawPassesChart(labels, resident, guest, vendor, container, chartType, displayLegend) {
            var PassChartData = {
                labels: labels,
                datasets: [
                    {
                        label: 'RESIDENT',
                        backgroundColor: '#dd4b39',
                        borderColor: '#dd4b39',
                        fill: false,
                        data: resident
                    },
                    {
                        label: 'VENDOR',
                        backgroundColor: '#00a65a',
                        borderColor: '#00a65a',
                        fill: false,
                        data: vendor
                    },
                    {
                        label: 'GUEST',
                        backgroundColor: 'rgba(243, 156, 18, 1)',
                        borderColor: 'rgba(243, 156, 18, 1)',
                        fill: false,
                        data: guest
                    }
                ]
            }

            var PassChartOptions = {
                responsive: true,
                title: {
                    display: false,
                    text: 'Chart.js Line Chart - Logarithmic'
                },
                scales: {
                    xAxes: [{
                        display: true,
                    }],
                    yAxes: [{
                        display: true,
                    }]
                },
                legend: {
                    display: displayLegend,
                }
            }

            var passChartCanvas = $('#' + container).get(0).getContext('2d')
            var lineChart = new Chart(passChartCanvas, {
                type: chartType,
                data: PassChartData,
                options: PassChartOptions
            })
        }

        $(function () {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/getAdminDashboardNotifications') }}",
                success: function (data) {
                    var data = data.notifications;
                    var html = '';
                    for (i in data) {
                        if (data[i].status) {
                            var status = '<span class="label label-success pull-right">Success</span>';
                        } else {
                            var status = '<span class="label label-danger pull-right">Failure</span>';
                        }

                        if (data[i].text != null && data[i].created_at != null) {
                            html += '<li class="item">\
                                    <div class="">\
                                        <span class="product-description">' + data[i].text + '</span>\
                                        <br>';

                            let created_at = data[i].created_at;
                            let modifiedCreatedAt = '';
                            if (created_at != null) {
                                modifiedCreatedAt = created_at.replace("T", " ");
                                modifiedCreatedAt = modifiedCreatedAt.replace(".000000", " ");
                                modifiedCreatedAt = modifiedCreatedAt.replace("Z", " ");

                            }
                            html += '<span class="product-description">' + modifiedCreatedAt + '</span>\
                                    </div>\
                                </li>';
                        }
                        $('#notification-list').html(html)
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

        //Camera Location Tickets Functionality
        function ticketChart(resident, guest, vendor, unmatched) {
            $('#residents').html(resident);
            $('#vendors').html(vendor);
            $('#guests').html(guest);
            $('#flag_tickets').html(unmatched);

            var config = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [resident, vendor, guest, unmatched],
                        backgroundColor: ["#f56954", "#00a65a", "#f39c12", "#00c0ef",],
                        label: 'Expenditures'
                    }],
                    labels: ["Resident", "Vendor", "Guest", "Unmatched"]
                },
                options: {
                    responsive: true,
                    legend: {
                        display: false,
                        position: 'bottom',
                    },
                    title: {
                        display: false,
                        text: ''
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                    return parseInt(previousValue) + parseInt(currentValue);
                                });
                                var currentValue = dataset.data[tooltipItem.index];
                                var label = data.labels[tooltipItem.index];
                                var percentage = Math.floor(((currentValue / total) * 100) + 0.5);
                                return label + ": " + percentage + "%";
                            }
                        }
                    }
                }
            };
            var ctx = document.getElementById("chartTickets").getContext("2d");
            window.myDoughnut = new Chart(ctx, config);

            var size = document.getElementById('chartSize').offsetHeight;
            $('#map').css('height', size + 'px')
        }

        var resident = 0;
        var guest = 0;
        var vendor = 0;
        var unmatched = 0;

        function showMapLoaders(type, flag) {
            flag ? $('#' + type + '-loader').show() : $('#' + type + '-loader').hide();
        }

        function getResident() {
            var community_id = '';
            var location_id = '';
            if ($('#camera_location_community').val() != 0) {
                community_id = $('#camera_location_community').val()
            }
            if ($('#camera_location_marker').val() != 0) {
                location_id = $('#camera_location_marker').val()
            }
            showMapLoaders('resident', true);
            showMapLoaders('vendor', true);
            showMapLoaders('guest', true);
            showMapLoaders('unmatched', true);
            $.ajax({
                type: "post",
                url: "{{ url('admin/get-traffic-summary') }}",
                data: {
                    type: 1,
                    community_id: community_id,
                    location_id: location_id
                },
                success: function (data) {
                    showMapLoaders('resident', false);
                    showMapLoaders('vendor', false);
                    showMapLoaders('guest', false);
                    showMapLoaders('unmatched', false);
                    resident = data[0];
                    guest = data[1];
                    vendor = data[2];
                    unmatched = data[3];
                    ticketChart(resident, guest, vendor, unmatched);
                },
                error: function (xhr, status, error) {
                    showMapLoaders('resident', false);
                    console.log(xhr.responseText);
                }
            });
        }
    </script>
@endsection
