@extends('admin.layout.master')
@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .c1 {
            font-weight: bold;
        }

        .container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white;
            border-radius: 7px;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Dashboard Setting</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard Setting</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <form role="form" method="post" action="{{route('admin.settings.dashboard-setting.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-4 c1"><b>Widget</b></div>
                                    <div class="col-md-4 c1"><b>Status</b></div>
                                    <div class="col-md-4 c1"><b>Color</b></div>
                                </div>
                                <br>
                                @foreach($dashboardSettingsData as $dashboardSetting)
                                    <div class="row">
                                        <div class="col-md-4">
                                            {{ $dashboardSetting->widget_name }}
                                        </div>
                                        <div class="col-md-4">
                                            @php
                                                $isChecked = $dashboardSetting->is_allowed == 1 ? 'checked': '';
                                            @endphp
                                            <input type="hidden" name="widgets[]"
                                                   value="{{ $dashboardSetting->widget_name }}">
                                            <label class="switch"><input type="checkbox"
                                                                         name={{ $dashboardSetting->widget_name}} {{ $isChecked }}>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-4">
                                            @php
                                                $redSelect = '';
                                                $aquaSelect = '';
                                                $greenSelect = '';
                                                $yellowSelect = '';

                                                if($dashboardSetting->color == "red") { $redSelect = "selected"; }
                                                if($dashboardSetting->color == "aqua") { $aquaSelect = "selected"; }
                                                if($dashboardSetting->color == "green") { $greenSelect = "selected"; }
                                                if($dashboardSetting->color == "yellow") { $yellowSelect = "selected"; }
                                            @endphp
                                            <select class="form-control"
                                                    name="{{ $dashboardSetting->widget_name."_color" }}">
                                                <option value="red" {{ $redSelect }}>Red</option>
                                                <option value="aqua" {{ $aquaSelect }}>Aqua</option>
                                                <option value="green" {{ $greenSelect }}>Green</option>
                                                <option value="yellow" {{ $yellowSelect }}>Yellow</option>
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
                                <br>
                                <button type="submit" class="btn btn-primary">
                                    <!----> <span>Save </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
    </script>
@endpush
