@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Community</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Community</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.communities.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Communities List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.residential.communities.update', [$community->id])}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Community </label>
                                        <input type="text" name="name" id="name" placeholder="Community Name"
                                               class="form-control" value="{{$community->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="rfid_site_code">RFID Site Code </label>
                                        <input type="number" name="rfid_site_code" id="rfid_site_code"
                                               value="{{$community->rfid_site_code}}" placeholder="RFID Site Code"
                                               class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="timezone_id">Time Zone</label>
                                        <select
                                            id="timezone_id" name="timezone_id" class="form-control select2bs4">
                                            <option value="">Select Timezone</option>
                                            @foreach($timezones as $timezone)
                                                <option value="{{$timezone->id}}"
                                                        @if($timezone->id == $community->timezone_id)selected="true"@endif>{{$timezone->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    @if(auth()->user()->role_id == 1)
                                        <div class="form-group">
                                            <label>Zuul Key</label>
                                            <input type="text" name="zuul_key" id="zuul_key" placeholder="Zuul Key"
                                                   class="form-control"
                                                   value="{{ isset($community->remoteGuard) ? $community->remoteGuard->zuul_key : '' }}"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Zuul Secret</label>
                                            <input type="text" name="zuul_secret" id="zuul_secret"
                                                   placeholder="Zuul Secret" class="form-control"
                                                   value="{{ isset($community->remoteGuard) ? $community->remoteGuard->zuul_secret : '' }}"/>
                                        </div>
                                    @endif


                                    <div class="form-group">
                                        <label>Dial Code</label>
                                        <select class="form-control" name="dial_code">
                                            @foreach($countries as $country)
                                                <option
                                                    value="{{ $country->dial_code }}" {{ $community->dial_code == $country->dial_code ? "selected" : "" }}>{{ "+".$country->dial_code." ".$country->country_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Address </label>
                                        <textarea class="form-control" name="address"
                                                  id="address"> {{$community->address}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="community_right">Community Rights </label>
                                        <select
                                            id="community_right" name="community_right" class="form-control">
                                            <option value="zuul"
                                                    @if($community->community_right == 'zuul')selected="true"@endif>Zuul
                                                Only
                                            </option>
                                            <option value="traffic_logics"
                                                    @if($community->community_right == 'traffic_logics')selected="true"@endif>
                                                Traffic Logix Only
                                            </option>
                                            <option value="both"
                                                    @if($community->community_right == 'both')selected="true"@endif>Both
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="guard_geo_fence">Guard Geo Fence</label>
                                        <select
                                            id="guard_geo_fence" name="guard_geo_fence" class="form-control">
                                            <option
                                                value="inactive" {{$community->guard_geo_fence == 'inactive' ?"selected":""}}>
                                                Inactive
                                            </option>
                                            <option
                                                value="active" {{$community->guard_geo_fence == 'active' ?"selected":""}}>
                                                Active
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact_number">Contact Number </label>
                                        <input type="text" class="form-control" name="contact_number"
                                               id="contact_number" value="{{$community->contact_number}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="emergency_contact_number">Emergency Contact Number </label>
                                        <input type="text" class="form-control" name="emergency_contact_number"
                                               id="emergency_contact_number"
                                               value="{{$community->emergency_contact_number}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="key">Key </label>
                                        <input type="text" class="form-control" name="key" id="key"
                                               value="{{$community->key}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="company_id">Company ID </label>
                                        <input type="number" min="0" class="form-control" name="company_id"
                                               id=company_id" value="{{$community->company_id}}"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="community_image">Community Logo </label>
                                        <input type="file" class="form-control" name="community_image"
                                               id=community_image" accept="image/x-png,image/gif,image/jpeg,image/jpg"/>
                                        <img src="{{$community->community_image_url}}" style="width: 100px">
                                    </div>

                                    <div class="form-group">
                                        <label for="web_relay_available">Web Relay Available </label>
                                        <input type="checkbox" name="web_relay_available" value="1"
                                               {{ $community->web_relay_available == 1 ? 'checked' : ''}} data-bootstrap-switch
                                               data-off-color="danger" data-on-color="success" data-on-text="Enable"
                                               data-off-text="Disable">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save Changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
@endpush
