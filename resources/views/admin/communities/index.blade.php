@extends('admin.layout.master')
@section('content')
    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Communities List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Communities List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">Community</label>
                                                <input type="text" name="name" id="name" placeholder="Community"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <input type="text" name="address" id="address" placeholder="Address"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.communities.create')}}" class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Add Community
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="communitiesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Community / Identifier</th>
                                    <th>Address</th>
                                    <th>House(s)</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="viewKeysModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" id="keysInfo">

                </div>

            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'name',
            },
            {
                data: 'address',
            },
            {
                data: "house_count_edit",
                name: "houses_count",
            },
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];
        zuulDataTableSortable('#communitiesTable', "{{route('admin.residential.communities-data')}}", columns, [4]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.residential.communities-data')}}?" + $("#search-filter").serialize();
            zuulDataTableSortable('#communitiesTable', ajax, columns, [4]);
        })

        function viewKeys(id) {
            $('#keysInfo').html("")
            var url = "{{ route('admin.get-remote-guard-keys') }}";

            $.ajax({
                url: url,
                type: "POST",
                data: {id: id},
                success: function (result) {
                    console.log(result)
                    if (result['remoteGuard'] != null) {
                        var html = "<br>" +
                            "<b><p style='font-size: 16px'>Zuul Key :  " + result.remoteGuard.zuul_key + "</p>" +
                            "<p style='font-size: 16px'>Zuul Secret : " + result.remoteGuard.zuul_secret + "</p></b>" +
                            "<br><br><br>";

                        $('#keysInfo').html(html);

                    }

                    $("#viewKeysModal").modal();
                }
            });
        }
    </script>
@endpush
