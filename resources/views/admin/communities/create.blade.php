@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Add Community</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Community</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.communities.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.residential.communities.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Community </label>
                                        <input type="text" name="name" id="name"
                                               placeholder="Community Name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="rfid_site_code">RFID Site Code </label>
                                        <input type="number" name="rfid_site_code" id="rfid_site_code"
                                               placeholder="RFID Site Code" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="timezone_id">Time Zone</label>
                                        <select
                                            id="timezone_id" name="timezone_id" class="form-control select2bs4">
                                            <option value="">Select Timezone</option>
                                            @foreach($timezones as $timezone)
                                                <option value="{{$timezone->id}}">{{$timezone->description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Dial Code</label>
                                        <select class="form-control" name="dial_code">
                                            @foreach($countries as $country)
                                                <option value="{{ $country->dial_code }}">
                                                    +{{ $country->dial_code }} {{ $country->country_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="address">Address </label>
                                        <textarea class="form-control" name="address" id="address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="community_right">Community Rights </label>
                                        <select
                                            id="community_right" name="community_right" class="form-control">
                                            <option value="zuul">Zuul Only</option>
                                            <option value="traffic_logics">Traffic Logix Only</option>
                                            <option value="both">Both</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="guard_geo_fence">Guard Geo Fence</label>
                                        <select
                                            id="guard_geo_fence" name="guard_geo_fence" class="form-control">
                                            <option value="inactive">Inactive</option>
                                            <option value="active">Active</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="contact_number">Contact Number </label>
                                        <input type="text" class="form-control" name="contact_number"
                                               id="contact_number"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="emergency_contact_number">Emergency Contact Number </label>
                                        <input type="text" class="form-control" name="emergency_contact_number"
                                               id="emergency_contact_number"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="key">Key </label>
                                        <input type="text" class="form-control" name="key" id="key"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="company_id">Company ID </label>
                                        <input type="number" min="0" class="form-control" name="company_id"
                                               id=company_id"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="community_image">Community Logo </label>
                                        <input type="file" class="form-control" name="community_image"
                                               id=community_image" accept="image/x-png,image/gif,image/jpeg,image/jpg"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="web_relay_available">Web Relay Available </label>
                                        <input type="checkbox" name="web_relay_available" value="1"
                                               data-bootstrap-switch
                                               data-off-color="danger" data-on-color="success" data-on-text="Enable"
                                               data-off-text="Disable">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
