@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Traffic Logix Email Templates List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Traffic Logix Email Templates List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.traffic-tickets.tickets-logix-email-templates.create')}}"
                                   class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Add Email Templates
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="trafficLogixemailTemplatesTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Assigned To</th>
                                    <th>Community</th>
                                    <th>Template</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'name',
            },
            {
                data: 'assign_to',
            },
            {
                data: 'community_name',
            },
            {
                data: 'template',
            },
            {
                data: function (data) {
                    return data.edit_link;
                }
            },
        ];
        zuulDataTable('#trafficLogixemailTemplatesTable', "{{route('admin.traffic-tickets.tickets-logix-email-templates-data')}}", columns, [5]);
    </script>
@endpush
