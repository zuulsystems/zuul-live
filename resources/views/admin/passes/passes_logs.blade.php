@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Passes Logs </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">RFID Tracking</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="rfids_tag_num_static">RFID Tag #</label>
                                                <input type="text" name="rfids_tag_num_static" id="rfids_tag_num_static"
                                                       placeholder="RFID Tag #" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="resident_name">Resident Name</label>
                                                <input type="text" name="resident_name" id="resident_name"
                                                       placeholder="Resident Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="scanner_name">Scanner</label>
                                                <input type="text" name="scanner_name" id="scanner_name"
                                                       placeholder="Scanner" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="rfid_scan_date">RFID Scan Date</label>
                                                <input type="date" name="rfid_scan_date" id="rfid_scan_date"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="status">Status</label>
                                            <select class="form-control" id="is_success" name="is_success">
                                                <option value="">All</option>
                                                <option value="1">Success</option>
                                                <option value="0">Error</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">

                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">

                            <table id="rfidLogTable1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>RFID Tag #</th>
                                    <th>Resident Name</th>
                                    <th>Scanner</th>
                                    <th>Scan</th>
                                    <th>Vehicle Info</th>
                                    <th>Log Text</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>

        let columns = [
            {

                data: 'id'
            },
            {
                data: function (data) {
                    let modifiedStyle = '';
                    if (data.is_success == '0') {
                        return '<div class="badge badge-danger">' + data.rfid + '</div>';

                    } else {
                        return data.rfid;
                    }

                },
                name: 'rfid'
            },
            {
                data: 'resident_name'
            },
            {
                data: 'scanner',
            },
            {
                data: 'formatted_scan_date',
                name: 'scan_at',
            },
            {
                data: 'vehicle_info',
            },
            {
                data: 'log_text',
            },
            {
                data: function (data) {
                    return data.formatted_status;
                }
            },
        ];
        zuulDataTableSortable('#rfidLogTable1', "{{url('admin/settings/passes-logs-data')}}", columns, [7]);

        $('.search-btn').click(function () {
            var ajax = "{{url('admin/settings/passes-logs-data')}}?" + $("#search-filter").serialize();
            zuulDataTableSortable('#rfidLogTable1', ajax, columns, [7]);
        })


    </script>
@endpush
