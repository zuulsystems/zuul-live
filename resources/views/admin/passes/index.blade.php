@extends('admin.layout.master')
@section('content')
    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid ">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Passes List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Passes List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community Name</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sender_name">Sender</label>
                                                <input type="text" name="sender_name" id="sender_name"
                                                       placeholder="Sender" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="recipient_name">Recipient</label>
                                                <input type="text" name="recipient_name" id="recipient_name"
                                                       placeholder="Recipient" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_pass_start_date">Pass Start Date</label>
                                                <input type="date" name="formatted_pass_start_date"
                                                       id="formatted_pass_start_date" placeholder="Pass Start Date"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_pass_date">Pass End/Expiry Date</label>
                                                <input type="date" name="formatted_pass_date" id="formatted_pass_date"
                                                       placeholder="Pass End/Expiry Date" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">

                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="residentsTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Sender</th>
                                    <th>Community</th>
                                    <th>Recipients</th>
                                    <th>Start</th>
                                    <th>Expires</th>
                                    <th>Description</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Pass recipients Model--->
    <div class="modal fade" id="pass-recipients-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Recipients</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Scan Date</th>
                                    <th>Expires</th>
                                    <th>License</th>
                                </tr>
                                </thead>
                                <tbody id="result"></tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script>
        //get pass recipients
        $(document).on('click', '.pass-recipients', function () {
            let passId = $(this).data('pass-id');
            showLoader();
            $.ajax({
                url: "{{route('admin.pass-recipients')}}",
                type: "POST",
                data: {
                    passId: passId
                },
                success: function (result) {
                    hideLoader();
                    let html = '';
                    let licenseImageUrl = '';
                    $('#result').html('');

                    if (result.data.hasOwnProperty('pass_users') && result.data.pass_users.length > 0) {
                        $.each(result.data.pass_users, function (key, pass_user) {
                            //check if pass date Expired Or Not
                            let isExpired = (pass_user.ScannedDate || result.data.isExpired == true) ? 'yes' : 'no';
                            licenseImageUrl = '';
                            if (pass_user.user.license_image != '' && pass_user.user.license_image != null) {
                                licenseImageUrl = pass_user.user.license_image_url;
                            }
                            html += `
                                     <tr>
                                        <td>${pass_user.user.id}</td>
                                        <td>${pass_user.user.full_name}</td>
                                        <td>${pass_user.user.email}</td>
                                        <td>${pass_user.user.formatted_phone}</td>
                                        <td>${pass_user.ScannedDate}</td>
                                        <td>${isExpired}</td>
                                        <td><img class="img-fluid print-img" src="${licenseImageUrl}" alt="license image" style="width: 29px"/></td>
                                   </tr>
                            `;
                        });
                        $('#result').html(html);
                    }
                    $('#pass-recipients-modal').modal('show');
                }
            });
        });

        let columns = [
            {
                data: 'id',
            },
            {
                data: 'created_by_name',
                name: 'createdBy.first_name',
            },
            {
                data: 'community_name',
                name: 'community.name',
            },
            {
                data: 'id',
                render: function (id) {
                    return `<button class="btn btn-warning pass-recipients" data-pass-id='${id}'>Recipients</button>`
                },
                sortable: false
            },
            {
                data: 'formatted_pass_start_date',
                name: "pass_start_date"
            },
            {
                data: 'formatted_pass_date',
                name: 'pass_date',
            },
            {
                data: 'description',
            },
            {
                data: 'created_at',
            }
        ];

        zuulDataTableSortableGuestLogs('#residentsTable', "{{route('admin.passes-data')}}?{{Request::getQueryString()}}", columns, [7]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.passes-data')}}?{{Request::getQueryString()}}&" + $("#search-filter").serialize();
            zuulDataTableSortableGuestLogs('#residentsTable', ajax, columns, [7]);
        })
    </script>
@endpush
