@extends('admin.layout.master')
@push('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
@endpush
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Payment Logs</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Payment Logs</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="ticket_id">Ticket ID</label>
                                                <input type="text" name="ticket_id" id="ticket_id"
                                                       placeholder="Ticket ID" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="admin_user">Admin User</label>
                                                <input type="text" name="admin_user" id="admin_user"
                                                       placeholder="Admin User" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="payee_user">Payee User</label>
                                                <input type="text" name="payee_user" id="payee_user"
                                                       placeholder="Payee User" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community">Community</label>
                                                <input type="text" name="community" id="community"
                                                       placeholder="Community" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="from">From</label>
                                                <input type="date" name="from" id="from" placeholder="from"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="to">To</label>
                                                <input type="date" name="to" id="to" placeholder="to"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Payment ID</th>
                                    <th>Payment ID</th>
                                    <th>Ticket ID</th>
                                    <th>Admin User</th>
                                    <th>Payee User</th>
                                    <th>Community</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'id',
            },
            {
                data: 'ticket.ticket_id',
            },
            {
                data: 'admin_user_full_name',
            },
            {
                data: 'payee_user_full_name',
            },
            {
                data: 'community_name',
            },
            {
                data: 'created_at',
            },
            {
                data: 'status',
            },

        ];

        url = "{{route('admin.traffic-payment-logs-data')}}?@if( request()->get('user_id') )user_id={{request()->get('user_id')}}@endif&@if( request()->get('houseId') )houseId={{request()->get('houseId')}}@endif";

        TLDataTable('#datatable', url, columns);


        $('.search-btn').click(function () {
            var ajax = url + '&' + $("#search-filter").serialize();
            TLDataTable('#datatable', ajax, columns);
        })
    </script>
@endpush
