<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zuul Systems Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{url('/')}}"/>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/custom.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/toastr/toastr.min.css')}}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>

        /* Image Print Div CSS */
        .enlargeImgDiv {
            position: absolute;
            text-align: left;
            background: white;
            border: 1px solid black;
            left: 4%;
            padding: 5px;
            width: 30vw;
            z-index: 7;
        }

        /*loading css*/
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: visible;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.3);
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 1500ms infinite linear;
            -moz-animation: spinner 1500ms infinite linear;
            -ms-animation: spinner 1500ms infinite linear;
            -o-animation: spinner 1500ms infinite linear;
            animation: spinner 1500ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */
        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        /*Datatable CSS*/
        .dataTables_wrapper {
            overflow-x: auto;
        }

        .table-responsive,
        .dataTables_scrollBody {
            overflow: visible !important;
        }

        .table-responsive-disabled .dataTables_scrollBody {
            overflow: hidden !important;
        }

        .table-responsive .dataTables_scrollHead {
            overflow: unset !important;
        }

        .table {
            width: 100% !important;
        }

    </style>
    @stack('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm">
<div class="wrapper">
    <!-- Loader -->
    <div class="loading" style="display: none"></div>
    <!-- End Loader-->
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom-0">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>


        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">

            </li>
            <li class="nav-item dropdown user-menu">
                <a href="{{url('run-queue')}}" class="nav-link dropdown-toggle" aria-expanded="false"
                   id="notification-dropdown-link" target="_blank">
                    <i class="fas fa-play"></i>
                </a>
            </li>

            <li class="nav-item dropdown user-menu">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="" aria-expanded="false"
                   id="notification-dropdown-link">
                </a>
                <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right"
                    style="left: inherit; right: 0px; width: 288px;" id="notificationUl">
                </ul>
            </li>


            <li class="nav-item dropdown user-menu">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="d-none d-md-inline">{{ auth()->user()->full_name}}</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                    <!-- User image -->
                    <li class="user-header bg-primary">
                        <img src="{{ auth()->user()->profile_image_url }}" class="img-circle elevation-2"
                             alt="User Image">
                        <p>
                            {{ auth()->user()->full_name}}
                            <small>{{ auth()->user()->role->name }}</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-4 text-center">
                                <a href="{{route('admin.profile.index')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="col-4 text-center">
                                <a href="{{route('admin.password.index')}}"
                                   class="btn btn-default btn-flat float-right">Password</a>
                            </div>
                            <div class="col-4 text-center">
                                <form action="{{route('logout')}}" method="post" id="logout-form" name="logout-form">
                                    @csrf
                                    <a href="#" class="btn btn-default btn-flat float-right"
                                       onclick="return $('#logout-form').submit()">Logout</a>
                                </form>
                            </div>
                        </div>
                        <!-- /.row -->
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('admin.layout.sidebar')

    <!-- Content Wrapper. Contains page content -->
    @yield('content')

    <!-- /.content-wrapper -->
    @include('admin.layout.footer')

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/admin/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<script src="{{ asset('assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('assets/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- Select2 -->
<script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Toastr -->
<script src="{{ asset('assets/admin/plugins/toastr/toastr.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
    $(document).ready(function () {
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    });
</script>

<!-- DataTables Buttons -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>

<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/admin/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/admin/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<!-- daterangepicker -->
<script src="{{ asset('assets/admin/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('assets/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<!-- overlayScrollbars -->
<script src="{{ asset('assets/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('assets/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/admin/dist/js/demo.js')}}"></script>

<script src="{{asset('assets/admin/dist/js/jquery.form.min.js')}}"></script>
{{--<script src="{{asset('assets/admin/dist/js/jquery.maskedinput.min.js')}}"></script>--}}
<script src="{{asset('assets/admin/plugins//inputmask/min/jquery.inputmask.bundle.min.js')}}"></script>
<script src="{{asset('assets/admin/dist/js/main.js')}}"></script>

<!-- 25-5-2022 22:37:00 -->
<script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
<!-- 25-5-2022 22:37:00 -->

<script src="{{ url('assets/frontend/js/custom.js') }}"></script>

<script>

    var firebaseConfig = {

        apiKey: "AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA",
        authDomain: "zuul-master.firebaseapp.com",
        databaseURL: "https://zuul-master.firebaseio.com",
        projectId: "zuul-master",
        storageBucket: "zuul-master.appspot.com",
        messagingSenderId: "1013551636023",
        appId: "1:1013551636023:web:55e1b9e9286170e003e9b2",
        measurementId: "G-9YZ7WDH9C4"


    };

    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();


    function initFirebaseMessagingRegistration() {
        messaging
            .requestPermission()
            .then(function () {
                return messaging.getToken()
            })
            .then(function (token) {
                console.log(token);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '{{ route("admin.residential.save-token") }}',
                    type: 'POST',
                    data: {
                        token: token
                    },
                    dataType: 'JSON',
                    success: function (response) {
                    },
                    error: function (err) {
                        console.log('User Chat Token Error' + err);
                    },
                });


            }).catch(function (err) {
            toastr.error('User Chat Token Error' + err, null, {timeOut: 3000, positionClass: "toast-bottom-right"});
        });
    }

    messaging.onMessage(function (payload) {

        $('#notification').css('color', 'red');
        $('#notification-dropdown-link').attr('data-toggle', 'dropdown');

        const noteTitle = payload.notification.title;

        var notificationInfo = payload.notification.title.split(",");

        var fileName = notificationInfo[1];

        const noteOptions = {
            body: payload.notification.body,
            icon: payload.notification.icon,
        };

        var url = "{{ route("admin.residential.imported-files") }}";

        //for house import
        if (notificationInfo[0] == "house-import") {
            url = "{{ route("admin.residential.house-imported-files") }}";
            url += "?file_name=" + notificationInfo[1];
        } else if (notificationInfo[0] == "resident-import") {
            var url = "{{ route("admin.residential.imported-files") }}";
            url += "?file_name=" + notificationInfo[1];
        } else if (notificationInfo[0] == "rfid-import") {
            var url = "{{ route("admin.rfid.rfid-imported-files") }}";
            url += "?file_name=" + notificationInfo[1];
        }

        //Getting current date and time
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        localStorage.setItem("notificationTitle", notificationInfo[1]);


        localStorage.setItem("notificationStartTime", dateTime);
        localStorage.setItem("url", url);

        var notificationHTML = '<li class="user-header bg-primary" style="height: auto; background-color: #83bd83 !important;"><a href="' + url + '"><div class="row"><div class="col-md-3"><i class="fas fa-bell" style="font-size: 39px;"></i> </div><div class="col-md-9"><p style="text-align: left">Your file with name ' + notificationInfo[1] + ' has been imported successfully<small></small></p></div></div></a></li>';

        $('#notificationUl').html(notificationHTML);

    });

    $(document).ready(function () {

        var notificationStartTime = localStorage.getItem('notificationStartTime');

        //Getting current date and time
        var today = new Date();
        var date = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        var date1 = new Date(notificationStartTime);
        var date2 = new Date(dateTime);

        var oDiff = {};

        var diff = date2.getTime() - date1.getTime();

        var msec = diff;
        var hh = Math.floor(msec / 1000 / 60 / 60);
        msec -= hh * 1000 * 60 * 60;
        var mm = Math.floor(msec / 1000 / 60);
        msec -= mm * 1000 * 60;
        var ss = Math.floor(msec / 1000);
        msec -= ss * 1000;

        if (hh >= 1) {
            localStorage.clear();
            $('#notification-dropdown-link').attr('data-toggle', '');
            $('#notificationUl').html('');

        }

        var noteTitle = localStorage.getItem('notificationTitle');
        var url = localStorage.getItem('url');

        if (noteTitle != null && url != null) {
            $('#notification-dropdown-link').attr('data-toggle', 'dropdown');
            var notificationHTML = '<li class="user-header bg-primary" style="height: auto; background-color: #83bd83 !important;"><a href="' + url + '"><div class="row"><div class="col-md-3"><i class="fas fa-bell" style="font-size: 39px;"></i> </div><div class="col-md-9"><p style="text-align: left">Your file with name ' + noteTitle + ' has been imported successfully<small></small></p></div></div></a></li>';

            $('#notificationUl').html(notificationHTML);

        }

        //csvs import notifications function
        function getCSVNotification() {
            $.ajax({
                url: "{{ route('admin.csv-notifications') }}",
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    if (result.notifications.length > 0) {
                        $('#notificationUl').html('');
                        $('#notification-dropdown-link').attr('data-toggle', 'dropdown');


                        for (var a = 0; a < result.notifications.length; a++) {
                            var importFileURLPage = "";

                            if (result.notifications[a].type == "house-import") {
                                importFileURLPage = "{{ route('admin.residential.house-imported-files') }}";
                            } else if (result.notifications[a].type == "resident-import") {
                                importFileURLPage = "{{ route('admin.residential.imported-files') }}";
                            } else if (result.notifications[a].type == "rfid-import") {
                                importFileURLPage = "{{ route('admin.rfid.rfid-imported-files') }}";
                            }

                            importFileURLPage = importFileURLPage + "?file_name=" + result.notifications[a].file_name;

                            var notificationHTML = '<li class="user-header bg-primary" style="height: auto; background-color: #83bd83 !important;"><a href="' + importFileURLPage + '"><div class="row"><div class="col-md-2"><i class="fas fa-bell" style="font-size: 39px;"></i> </div><div class="col-md-10"><p style="text-align: left">Your file with name ' + result.notifications[a].file_name + ' has been imported successfully<small></small></p></div></div></a></li>';

                            if (result.notifications[a].viewed == '0') {
                                $('#notification').css('color', 'red');

                                $.ajax({
                                    url: "{{ route('admin.csv-notification-viewed') }}",
                                    type: "POST",
                                    data: {id: result.notifications[a].id},
                                });
                            }
                            $('#notificationUl').append(notificationHTML);


                        }
                    }
                }
            });
        }

    });
</script>

@stack('scripts')
</body>
</html>
