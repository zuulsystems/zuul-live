<aside class="main-sidebar sidebar-dark-primary elevation-4 sidebar-no-expand">
    <!-- Brand Logo -->
    <a href="{{route('admin.dashboard')}}" class="brand-link">
        <img src="{{ asset('assets/admin/dist/img/logo.png')}}" alt="AdminLTE Logo"
             class="brand-image"
             style="opacity: .8">
        <span class="brand-text font-weight-light"> Zuul Systems</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @cannot('is-guard-admin')
                    <li class="nav-item">
                        <a href="{{route('admin.dashboard')}}"
                           class="nav-link  {{ request()->is('admin/dashboard*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-home"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview {{ request()->is('admin/settings*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/settings*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-cog"></i>
                            <p>
                                Initial Settings
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @cannot('is-guard-admin')
                                <li class="nav-item">
                                    <a href="{{route('admin.settings.dashboard-setting.index')}}"
                                       class="nav-link {{ request()->is('admin/settings/dashboard-setting*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Dashboard Setting</p>
                                    </a>
                                </li>
                            @endcannot
                            @can('super-admin')
                                <li class="nav-item">
                                    <a href="{{route('admin.settings.email-templates.index')}}"
                                       class="nav-link {{ request()->is('admin/settings/email-templates*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Email Templates</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('admin.settings.printer-templates.index')}}"
                                       class="nav-link {{ request()->is('admin/settings/printer-templates*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Printer Templates</p>
                                    </a>
                                </li>
                            @endcan
                            @can('community-admin')
                                <li class="nav-item">
                                    <a href="{{route('admin.settings.printer-templates.index')}}"
                                       class="nav-link {{ request()->is('admin/settings/printer-templates*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Printer Templates</p>
                                    </a>
                                </li>
                            @endcan

                            <li class="nav-item">
                                <a href="{{route('admin.settings.scanners.index')}}"
                                   class="nav-link {{ request()->is('admin/settings/scanners*') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Scanners</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.settings.cameras.index')}}"
                                   class="nav-link {{ request()->is('admin/settings/cameras*') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Cameras</p>
                                </a>
                            </li>
                            @canany(['super-admin','community-admin'])
                                <li class="nav-item">
                                    <a href="{{route('admin.settings.offensive-words.index')}}"
                                       class="nav-link {{ request()->is('admin/settings/offensive-words*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Offensive Words</p>
                                    </a>
                                </li>
                            @endcanany
                            @canany(['super-admin','tl-admin','kiosk'])
                                <li class="nav-item">
                                    <a href="{{route('admin.settings.auth-logs')}}"
                                       class="nav-link {{ request()->is('admin/settings/auth-logs*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Authentication Logs</p>
                                    </a>
                                </li>
                            @endcanany
                        </ul>
                    </li>

                    @can('kiosk')
                        @cannot('kiosk')
                            <li class="nav-item">
                                <a href="{{route('admin.user.guards.index')}}"
                                   class="nav-link {{ request()->is('admin/user/guards*') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Security Guards</p>
                                </a>
                            </li>
                        @endcannot

                        <li class="nav-item has-treeview {{ request()->is('admin/rfid*') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ request()->is('admin/rfid*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    Residential Lane
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('admin.rfid.rfids.index')}}"
                                       class="nav-link {{ request()->is('admin/rfid/rfids*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>RFID tags</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('admin.rfid.rfid-logs.index')}}"
                                       class="nav-link {{ request()->is('admin/rfid/rfid-logs*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>RFID Tracking</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('admin.rfid.logs.index')}}"
                                       class="nav-link {{ request()->is('admin/rfid/logs*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Activity Tracking</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    @endcan

                    @canany(['super-admin','is-zuul-right','is-tl-right','kiosk'])
                        <li class="nav-item has-treeview {{ request()->is('admin/user*') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ request()->is('admin/user*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Users Tab
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @canany(['super-admin','community-admin','kiosk'])
                                    @cannot('only-tl-right')
                                        <li class="nav-item">
                                            <a href="{{route('admin.user.guests.index')}}"
                                               class="nav-link {{ request()->is('admin/user/guests*') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Guests</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{route('admin.user.banned-guests')}}"
                                               class="nav-link {{ request()->is('admin/user/banned-guests*') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Guest Banned List</p>
                                            </a>
                                        </li>
                                    @endcannot
                                @endcan
                                @cannot(['is-guard-admin'])
                                    <li class="nav-item">
                                        <a href="{{route('admin.user.community-admins.index')}}"
                                           class="nav-link {{ request()->is('admin/user/community-admins*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Community Admins</p>
                                        </a>
                                    </li>
                                @endcannot
                                @cannot(['is-guard-admin'])
                                    @cannot('kiosk')
                                        @cannot('community-admin')
                                            <li class="nav-item">
                                                <a href="{{route('admin.user.kiosk-admins.index')}}"
                                                   class="nav-link {{ request()->is('admin/user/kiosk-admins*') ? 'active' : '' }}">
                                                    <i class="far fa-circle nav-icon"></i>
                                                    <p>Kiosk Admins</p>
                                                </a>
                                            </li>
                                        @endcannot
                                    @endcannot
                                @endcannot
                                @canany(['super-admin','community-admin','is-zuul-right','is-guard-admin'])
                                    @if(auth()->user()->hasRole('guard_admin')  && auth()->user()->can('only-tl-right'))
                                    @else
                                        <li class="nav-item">
                                            <a href="{{route('admin.user.guards.index')}}"
                                               class="nav-link {{ request()->is('admin/user/guards*') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Security Guards</p>
                                            </a>
                                        </li>
                                    @endif
                                @endcanany
                            </ul>
                        </li>
                    @endcanany
                @endcannot
                @can('is-guard-admin')
                    <li class="nav-item">
                        <!-- {{ request()->is('admin/user/banned-guests*') ? 'active' : '' }} -->
                        <a href="{{route('admin.user.banned-guests')}}"
                           class="nav-link {{ request()->is('admin/user/banned-guests*') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Guest Banned List</p>
                        </a>
                    </li>
                @endcan
                @canany(['is-guard-admin'])
                    <li class="nav-item">
                        <a href="{{route('admin.residential.residents.index')}}"
                           class="nav-link {{ request()->is('admin/residential/residents*') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Residents</p>
                        </a>
                    </li>
                @endcannot

                @canany(['super-admin','community-admin','kiosk'])
                    <li class="nav-item has-treeview {{ request()->is('admin/residential*') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link {{ request()->is('admin/residential*') ? 'active' : '' }}">
                            <i class="nav-icon fas fa-user"></i>
                            <p>
                                Resident Communities
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>

                        <ul class="nav nav-treeview">
                            @canany(['super-admin'])
                                <li class="nav-item">
                                    <a href="{{route('admin.residential.communities.index')}}"
                                       class="nav-link {{ request()->is('admin/residential/communities*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Communities</p>
                                    </a>
                                </li>
                            @endcan
                            @cannot('is-guard-admin')
                                <li class="nav-item">
                                    <a href="{{route('admin.residential.houses.index')}}"
                                       class="nav-link {{ request()->is('admin/residential/houses*') ? 'active' : '' }}">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Houses</p>
                                    </a>
                                </li>
                            @endcannot

                            <li class="nav-item">
                                <a href="{{route('admin.residential.residents.index')}}"
                                   class="nav-link {{ request()->is('admin/residential/residents*') ? 'active' : '' }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Residents</p>
                                </a>
                            </li>
                            @canany(['super-admin','is-zuul-right','is-guard-admin','kiosk'])
                                @cannot('is-guard-admin')
                                    @if(auth()->user()->hasRole("kiosk") ||auth()->user()->hasRole("super_admin") || auth()->user()->hasRole("sub_admin") && auth()->user()->cannot('only-tl-right')|| auth()->user()->hasRole("guard_admin") && auth()->user()->community->community_right != "traffic_logics")
                                        <li class="nav-item">
                                            <a href="{{route('admin.residential.announcements.index')}}"
                                               class="nav-link {{ request()->is('admin/residential/announcements*') ? 'active' : '' }}">
                                                <i class="far fa-circle nav-icon"></i>
                                                <p>Announcements</p>
                                            </a>
                                        </li>
                                    @endif
                                @endcannot
                            @endcanany
                        </ul>
                    </li>

                    @cannot('kiosk')
                        @cannot('community-admin')
                            <li class="nav-item">
                                <a href="{{route('admin.iono-heartbeat.index')}}"
                                   class="nav-link {{ request()->is('admin/iono-heartbeat*') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-heartbeat"></i>
                                    <p>
                                        IONO Heartbeat
                                    </p>
                                </a>
                            </li>
                        @endcannot
                    @endcannot

                @endcanany
                @canany(['super-admin','is-zuul-right','community-admin'])

                    @cannot('is-guard-admin')
                        @cannot('only-tl-right')
                            <li class="nav-item">
                                <a href="{{route('admin.passes.index')}}"
                                   class="nav-link {{ request()->is('admin/passes*') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-ticket-alt"></i>
                                    <p>
                                        Passes
                                    </p>
                                </a>
                            </li>

                        @endcannot
                    @endcannot

                    @can('kiosk')
                        <li class="nav-item">
                            <a href="{{url('admin/settings/scan-logs')}}"
                               class="nav-link ">
                                <i class="nav-icon fas fa-ticket-alt"></i>
                                <p>
                                    Guest Log
                                </p>
                            </a>
                        </li>

                    @endcan



                    @canany(['super-admin','community-admin','is-guard-admin','kiosk'])
                        @canany(['is-guard-admin','is-zuul-right','super-admin','community-admin'])
                            @cannot(['only-tl-right'])
                                @cannot('kiosk')
                                    <li class="nav-item">
                                        <a href="{{route('admin.guest-logs.index')}}"
                                           class="nav-link {{ request()->is('admin/guest-logs*') ? 'active' : '' }}">
                                            <i class="nav-icon fas fa-ticket-alt"></i>
                                            <p>
                                                Guest Log
                                            </p>
                                        </a>
                                    </li>
                                @endcannot
                            @endcannot
                        @endcanany


                        @cannot('only-tl-right')
                            @cannot('community-admin')
                                @cannot('is-guard-admin')
                                    <li class="nav-item">
                                        <a href="{{route('admin.pass-requests')}}"
                                           class="nav-link {{ request()->is('admin/pass-requests*') ? 'active' : '' }}">
                                            <i class="nav-icon fas fa-ticket-alt"></i>
                                            <p>
                                                Pass Requests
                                            </p>
                                        </a>
                                    </li>
                                @endcannot
                            @endcannot
                        @endcannot
                        @cannot('kiosk')
                            <li class="nav-item has-treeview {{ request()->is('admin/rfid*') ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link {{ request()->is('admin/rfid*') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Residential Lane
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('admin.rfid.rfids.index')}}"
                                           class="nav-link {{ request()->is('admin/rfid/rfids*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>RFID tags</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.rfid.rfid-logs.index')}}"
                                           class="nav-link {{ request()->is('admin/rfid/rfid-logs*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>RFID Tracking</p>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="{{route('admin.rfid.logs.index')}}"
                                           class="nav-link {{ request()->is('admin/rfid/logs*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Activity Tracking</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        @endcannot
                    @endcanany
                @endcanany

                {{--                Mark:- New Tab on Adam's request without permission--}}
                @php
                    $isActive = Request::is('admin/vendors*') || Request::is('admin/vehicles*') || Request::is('admin/locations*') || Request::is('admin/email-logs*') || Request::is('admin/traffic-payment-logs*');
                    $isOpen = Request::is('admin/vendors*', 'admin/vehicles*', 'admin/locations*', 'admin/email-logs*', 'admin/traffic-payment-logs*');
                @endphp

                <li class="nav-item has-treeview {{ $isOpen ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ $isActive ? 'active' : '' }}">
                        <i class="nav-icon fas fa-ticket-alt"></i>
                        <p>Ticketing Module
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('admin.vendors.index')}}"
                               class="nav-link {{ request()->is('admin/vendors*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-shopping-cart"></i>
                                <p>
                                    Vendors
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.vehicles.index')}}"
                               class="nav-link {{ request()->is('admin/vehicles*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-car"></i>
                                <p>
                                    Vehicles
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.locations.index')}}"
                               class="nav-link {{ request()->is('admin/locations*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-camera"></i>
                                <p>
                                    Camera Location
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.email-logs.index')}}"
                               class="nav-link {{ request()->is('admin/email-logs*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-envelope"></i>
                                <p>
                                    Email Logs
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.traffic-payment-logs.index')}}"
                               class="nav-link {{ request()->is('admin/traffic-payment-logs*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-envelope"></i>
                                <p>
                                    Payment
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                @cannot('is-guard-admin')
                    @cannot('kiosk')
                        @canany(['super-admin','is-tl-right'])
                            <li class="nav-item has-treeview {{ request()->is('admin/traffic-tickets*') ? 'menu-open' : '' }}">
                                <a href="#"
                                   class="nav-link {{ request()->is('admin/traffic-tickets*') ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-ticket-alt"></i>
                                    <p>Tickets
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('admin.traffic-tickets.tickets-logix-communities.index')}}"
                                           class="nav-link {{ request()->is('admin/traffic-tickets/tickets-logix-communities*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Communities</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.traffic-tickets.resident-tickets.index')}}"
                                           class="nav-link {{ request()->is('admin/traffic-tickets/resident-tickets*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Resident Ticket</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.traffic-tickets.guest-tickets.index')}}"
                                           class="nav-link {{ request()->is('admin/traffic-tickets/guest-tickets*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Guest Tickets</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.traffic-tickets.vendor-tickets.index')}}"
                                           class="nav-link {{ request()->is('admin/traffic-tickets/vendor-tickets*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Vendor Tickets</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.traffic-tickets.unmatched-tickets.index')}}"
                                           class="nav-link {{ request()->is('admin/tickets/unmatched-tickets*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Unmatched Tickets</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.traffic-tickets.tickets-logix-email-templates.index')}}"
                                           class="nav-link {{ request()->is('admin/traffic-tickets/tickets-logix-email-templates*') ? 'active' : '' }}">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Traffic Logix Email Templates</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endcanany
                    @endcannot
                @endcannot

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
