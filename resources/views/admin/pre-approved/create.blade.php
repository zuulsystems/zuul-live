@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Pre-Approved Guest</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Pre-Approved Guest</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.pre-approved.index',['userId'=>$userId])}}"
                                   class="add_advertiser">
                                    <i class="fas fa-list"></i> Pre-Approved Guest List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.pre-approved.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">

                                    <div class="row" id="temporary-contact-div" style="display: none">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="hidden" name="userId" value="{{ $userId }}">
                                                <label for="temporary_duration">Contact Duration</label>
                                                <select name="temporary_duration" id="temporary_duration"
                                                        class="form-control">
                                                    <option value="6" selected="">6 Hours</option>
                                                    <option value="12">12 Hours</option>
                                                    <option value="24">24 Hours</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_name">Contact Name</label>
                                                <input type="text" name="contact_name" id="contact_name"
                                                       placeholder="Contact Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Shared</label>
                                                <select class="form-control" name="is_dnc_shared">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="dial_code">Country code</label>
                                                @php
                                                    $url = route("get-phone-format-by-dial-code");
                                                @endphp
                                                <select name="dial_code" id="dial_code" class="form-control"
                                                        onchange="changePhoneDigitsFormat1('{{ $url }}')">
                                                    @foreach($countries as $country)
                                                        <option
                                                            value="{{$country->dial_code}}">{{$country->country_name}}
                                                            +{{$country->dial_code}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" name="phone_number" class="form-control"
                                                       placeholder="Phone Number" id="phone_number"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var $phoneFormat = "{{ $phoneFormat }}";
            $('#phone_number').inputmask({mask: $phoneFormat});
        });
    </script>
@endpush
