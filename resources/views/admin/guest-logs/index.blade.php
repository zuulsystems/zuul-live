@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }

        .daterangepicker {
            top: 50% !important;
            left: 50% !important;
            transform: translate(-10%, -10%);
        }

    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Guest Log List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Guest Log List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community Name</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sender_name">Sender</label>
                                                <input type="text" name="sender_name" id="sender_name"
                                                       placeholder="Sender" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="event_name">Recipient</label>
                                                <input type="text" name="receipt" id="receipt" placeholder="Recipient"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="event_name">Event Name</label>
                                                <input type="text" name="event_name" id="event_name"
                                                       placeholder="Event Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="event_name">License Plate</label>
                                                <input type="text" name="license_plate" id="license_plate"
                                                       placeholder="License Plate" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="event_name">Scan By (Gate)</label>
                                                <input type="text" name="scan_by" id="scan_by"
                                                       placeholder="Scan By (Gate)" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="scan_date">Pass Scan Date Range</label>
                                                <input type="text" name="scan_date2" id="scan_date2"
                                                       placeholder="Pass Scan Date Range" class="form-control">
                                                <input type="hidden" name="scan_date" id="scan_date"
                                                       placeholder="Pass Scan Date Range" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="guestLogTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <?php if(!auth()->user()->hasRole('guard_admin')){?>
                                    <th>Image</th>
                                    <?php
                                    }
                                    ?>
                                    <th>License Plate Image</th>
                                    <th>Sender</th>
                                    <th>Recipient</th>
                                    <th>Community</th>
                                    <th>Event</th>
                                    <th>Start</th>
                                    <th>Expires</th>
                                    <th>Scan Date</th>
                                    <th>Scan By</th>
                                    <th>License</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>


        let columns = [
            {
                data: 'id',
                name: 'id'

            },
            {
                data: 'image',
                name: "id",

            },
            {
                data: 'license_plate_image_url',
                name: "id",
            },
            {
                data: function (data, type, val, meta) {
                    return `${data.sender_name} <br/> ${data.sender_phone} <br/> ${data.sender_full_address}`
                },
                name: "id",

            },
            {
                data: function (data, type, val, meta) {
                    return `<b>${data.guard_display_name}</b> <br/> ${data.receiver_name} <br/> ${data.receiver_phone} <br/> ${data.receiver_full_address}`
                },
                name: "id",

            },
            {
                data: 'community_name',
                name: "id",
            },
            {
                data: 'event_name',
                name: "id",
            },
            {
                data: 'formatted_pass_start_date',
                name: "id",
            },
            {
                data: 'formatted_pass_date',
                name: "id",
            },
            {
                data: 'scan_date',
                name: "id",
            },
            {
                data: 'scan_by',
                sortable: false
            },
            {
                data: "vehicle_info"
            },
            {
                data: function (data) {
                    let html = '';
                    html = `<p>${data.log_text}</p>`;
                    if (data.is_success == '1') {
                        html += `<span class="badge badge-success">SUCCESS</span>`;
                    } else {
                        html += `<span class="badge badge-danger">ERROR</span>`;
                    }
                    return html;
                },
                name: "is_success"
            },
        ];
        let check = "<?= !auth()->user()->hasRole('guard_admin') ?>";
        if (check) {
            zuulDataTableSortableGuestLogs('#guestLogTable', "{{route('admin.guest-logs-data')}}?{{Request::getQueryString()}}", columns, [11]); // 11 for admin and guard for 9
            $('.search-btn').click(function () {
                var ajax = "{{route('admin.guest-logs-data')}}?{{Request::getQueryString()}}&" + $("#search-filter").serialize();

                if ($('#scan_date2').val() === '') {
                    ajax = ajax.replace(/(scan_date|scan_date2)=[^&]+&?/g, '');
                }

                ajax = ajax + "{{ Request::getQueryString() }}";

                zuulDataTableSortableGuestLogs('#guestLogTable', ajax, columns, [11]);
            })
        } else {
            delete columns.splice(0, 1);
            zuulDataTableSortableGuestLogs('#guestLogTable', "{{route('admin.guest-logs-data')}}?{{Request::getQueryString()}}", columns, [9]); // 11 for admin and guard for 9
            $('.search-btn').click(function () {
                var ajax = "{{route('admin.guest-logs-data')}}?{{Request::getQueryString()}}&" + $("#search-filter").serialize();

                if ($('#scan_date2').val() === '') {
                    ajax = ajax.replace(/(scan_date|scan_date2)=[^&]+&?/g, '');
                }

                ajax = ajax + "{{ Request::getQueryString() }}";

                zuulDataTableSortableGuestLogs('#guestLogTable', ajax, columns, [9]);
            })
        }


    </script>

    <script>
        $(function () {
            $('input[name="scan_date"]').daterangepicker({
                opens: 'center',
                locale: {
                    format: 'MM-DD-YYYY'
                }
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('MM-DD-YYYY') + ' to ' + end.format('MM-DD-YYYY'));
                document.getElementById("scan_date2").setAttribute("value", start.format('MM-DD-YYYY') + ' - ' + end.format('MM-DD-YYYY'))
            });
        });


        document.addEventListener('DOMContentLoaded', function () {
            const inputField = document.getElementById('scan_date');
            const changeColorButton = document.getElementById('scan_date2');

            changeColorButton.addEventListener('click', function () {
                $('#scan_date').click();
            });
        });

    </script>

@endpush
