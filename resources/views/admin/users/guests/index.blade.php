@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Guest List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Guest List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_name">Name</label>
                                                <input type="text" name="full_name" id="full_name" placeholder="Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_phone">Phone Number</label>
                                                <input type="text" name="formatted_phone" id="formatted_phone"
                                                       placeholder="Phone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="guestTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Role</th>
                                    <th>Created At</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Make Resident Model--->
        <div class="modal fade" id="make-resident-model">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Make Resident</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form role="form" method="post" action="{{route('admin.user.make-resident')}}"
                          class="ajax-form">
                        <input type="hidden" name="user_id" id="user_id" value="">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email </label>
                                        <input type="email" name="email" id="email"
                                               placeholder="Email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="revoke_residential_right">Communities Assigned</label>
                                        <select
                                            id="community_id" name="community_id" class="form-control select2bs4"
                                            required>
                                            <option value="">Select Community</option>
                                            @foreach($communities as $community)
                                                <option value="{{$community->id}}">{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="house_id">House Assigned</label>
                                        <select id="house_id" name="house_id" class="form-control select2bs4" required>
                                            <option value="">Select House</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- Permission Model--->
        <div class="modal fade" id="guest-permission">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Guest Permissions</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form role="form" method="post" action="{{route('admin.user.assign-guest-permission')}}"
                          class="ajax-form" id="guest-permission-form">
                        <input type="hidden" name="user_id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="can_manage_family">Is License Locked </label>
                                        <input type="checkbox" name="is_license_locked" id="is_license_locked" value="1"
                                               data-bootstrap-switch
                                               data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                               data-off-text="No">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- Banned Permission Model--->
        <div class="modal fade" id="banned-permission">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Banned on Community</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form role="form" method="post" action="{{route('admin.user.assign-banned-permission')}}"
                          class="ajax-form" id="guest-permission-form2">
                        <input type="hidden" name="banned_user_id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="can_manage_family">Select Communities </label>
                                        <select class="form-control " multiple id="communities" name="communities[]">
                                            <option value="">-- All Communities --</option>
                                            @foreach ($communities as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Banned</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'full_name',
                name: 'first_name',
            },
            {
                data: 'formatted_phone',
                name: 'phone_number',
            },
            {
                data: 'email',
            },
            {
                data: 'mailing_address',
            },
            {
                data: 'role_name',
                name: 'role.name',
            },
            {
                data: 'created_at',
            },
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];
        zuulDataTableSortable('#guestTable', "{{route('admin.user.guest-data')}}?userId={{$userId ?? null}}", columns, [7]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.user.guest-data')}}?userId={{$userId ?? null}}&" + $("#search-filter").serialize();
            zuulDataTableSortable('#guestTable', ajax, columns, [7]);
        })

        $(document).on('click', '.make-resident-model', function () {
            let id = $(this).data('id');
            $('input[name="user_id"]').val(id);
            $.ajax({
                url: "{{route('admin.user.get-user')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let user = result.user;
                        $('input[name="email"]').val(user.email);
                        $('#resident-permission-modal').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
            $('#make-resident-model').modal('show');
        });


        //guest permission model
        $(document).on('click', '.banned-permission', function () {
            let id = $(this).data('id');
            showLoader();
            $.ajax({
                url: "{{route('admin.user.check-banned-permission')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let permission = result.permission;
                        $('input[name="banned_user_id"]').val(id);
                        $('#banned-permission').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        //guest permission model
        $(document).on('click', '.guest-permission', function () {
            let id = $(this).data('id');
            $("#is_license_locked").bootstrapSwitch('state', false);
            showLoader();
            $.ajax({
                url: "{{route('admin.user.check-guest-permission')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let permission = result.permission;
                        $('input[name="user_id"]').val(id);
                        if (permission.isLicenseLocked === true) {
                            $("#is_license_locked").bootstrapSwitch('state', true);
                        }
                        $('#guest-permission').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    let options = '';
                    if (result.status === true) {
                        $.each(result.result, function (key, row) {
                            options += `<option value="${row.id}">${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                }
            });
        });
    </script>
@endpush
