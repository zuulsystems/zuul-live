@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Guest Banned List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Guest Banned List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_name">Name</label>
                                                <input type="text" name="full_name" id="full_name" placeholder="Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_phone">Phone Number</label>
                                                <input type="text" name="formatted_phone" id="formatted_phone"
                                                       placeholder="Phone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="email">License Plate</label>
                                                <input type="text" name="license_plate" id="license_plate"
                                                       placeholder="License Plate" class="form-control">
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                                <h3 class="card-title">
                                    <a href="{{url('admin/user/banned-user')}}">
                                        <i class="fas fa-file-import"></i> Add Banned User
                                    </a>
                                </h3>
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="guestTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Communities</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>License Plate</th>
                                    <th>Created At</th>
                                    @if(!auth()->user()->hasRole('guard_admin'))
                                        <th>Action(s)</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Make Resident Model--->
        <div class="modal fade" id="make-resident-model">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Make Resident</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form role="form" method="post" action="{{route('admin.user.make-resident')}}"
                          class="ajax-form">
                        <input type="hidden" name="user_id" id="user_id" value="">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email </label>
                                        <input type="email" name="email" id="email"
                                               placeholder="Email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="revoke_residential_right">Communities Assigned</label>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="house_id">House Assigned</label>
                                        <select id="house_id" name="house_id" class="form-control select2bs4" required>
                                            <option value="">Select House</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- Permission Model--->
        <div class="modal fade" id="guest-permission">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Guest Permissions</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form role="form" method="post" action="{{route('admin.user.assign-guest-permission')}}"
                          class="ajax-form" id="guest-permission-form">
                        <input type="hidden" name="user_id">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="can_manage_family">Is License Locked </label>
                                        <input type="checkbox" name="is_license_locked" id="is_license_locked" value="1"
                                               data-bootstrap-switch
                                               data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                               data-off-text="No">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- Add Banned User Model -->
        <div class="modal fade" id="resident-import-modal">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Banned User</h4>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                id="closeResidentsImportModalBtn">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ route('admin.user.add-banned-user') }}"
                          enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row">
                                @if(auth()->user()->hasRole('super_admin'))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="community">Community</label>
                                            <select class="form-control" name="community" id="retrieve_community">
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone_number">Phone Number</label>
                                        <input type="text" class="form-control" name="phone_number"
                                               placeholder="Phone Number" data-inputmask='"mask": "999-999-9999"'
                                               data-mask required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Banned Guest Name" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="license_plate">License Plate </label>
                                        <input type="text" class="form-control" name="license_plate"
                                               placeholder="License Plate" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" class="btn btn-primary" id="add_banned_user_success">Add</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </div>
@endsection
@push('scripts')
    <script>

        var error_message = '<?php echo $errors->first();?>'
        var error = '<?php echo $errors->any();?>'
        if (error) {
            if (error_message == 'OK') {
                notifySuccess("Banned Guest Added!")
            } else {
                notifyError(error_message)
            }
        }

        var columns = [
            {
                data: 'id',
            },
            {
                data: 'communities'
            },
            {
                data: 'full_name',
                name: 'first_name',
            },
            {
                data: 'formatted_phone',
                name: 'phone_number',
            },
            {
                data: 'license_plate',
            },
            {
                data: 'created_at',
            },
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];

        let check = "<?= !auth()->user()->hasRole('guard_admin') ?>";
        if (check) {
            zuulDataTableSortableGuestLogs('#guestTable', "{{route('admin.user.guest-band-data')}}?userId={{$userId ?? null}}", columns, [6]);

            $('.search-btn').click(function () {
                var ajax = "{{route('admin.user.guest-band-data')}}?userId={{$userId ?? null}}&" + $("#search-filter").serialize();
                zuulDataTableSortableGuestLogs('#guestTable', ajax, columns, [5]);
            })
        } else {
            delete columns.splice(5, 1);

            zuulDataTableSortableGuestLogs('#guestTable', "{{route('admin.user.guest-band-data')}}?userId={{$userId ?? null}}", columns, [5]);

            $('.search-btn').click(function () {
                var ajax = "{{route('admin.user.guest-band-data')}}?userId={{$userId ?? null}}&" + $("#search-filter").serialize();
                zuulDataTableSortableGuestLogs('#guestTable', ajax, columns, [5]);
            })
        }


        $(document).on('click', '.make-resident-model', function () {
            let id = $(this).data('id');
            $('input[name="user_id"]').val(id);
            $.ajax({
                url: "{{route('admin.user.get-user')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let user = result.user;
                        $('input[name="email"]').val(user.email);
                        $('#resident-permission-modal').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
            $('#make-resident-model').modal('show');
        });

        //guest permission model
        $(document).on('click', '.guest-permission', function () {
            let id = $(this).data('id');
            $("#is_license_locked").bootstrapSwitch('state', false);
            showLoader();
            $.ajax({
                url: "{{route('admin.user.check-guest-permission')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let permission = result.permission;
                        $('input[name="user_id"]').val(id);
                        if (permission.isLicenseLocked === true) {
                            $("#is_license_locked").bootstrapSwitch('state', true);
                        }
                        $('#guest-permission').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        $(document).ready(function () {

            $('#closeResidentsImportModalBtn').click(function () {
                $("#resident-import-modal").css("display", "none");
                $('.a').css('display', 'none');
            });


            getModalUserData();
            $('.reset-search').click(function () {
                $('input').val('');

                if (check) {
                    zuulDataTableSortable('#guestTable', "{{route('admin.user.guest-band-data')}}?userId={{$userId ?? null}}", columns, [8]);
                } else {
                    delete columns.splice(8, 1);

                    zuulDataTableSortable('#guestTable', "{{route('admin.user.guest-band-data')}}?userId={{$userId ?? null}}", columns, [7]);
                }

            });

            function getModalUserData() {
                $.ajax({
                    type: 'GET',
                    url: "{{route('admin.residential.communities-data')}}",
                    success: function (result) {
                        var options = '<option disabled selected>Select Community</option>';
                        $.each(result['data'], function (key, row) {
                            options += `<option value="${row.id}">${row.name}</option>`;
                        });
                        $('#retrieve_community').html(options);
                    }
                });
            }
        });
    </script>
@endpush
