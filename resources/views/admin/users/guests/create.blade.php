@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Add Banned Guest</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Banned Guest</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{url('admin/user/banned-guests')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.user.add-banned-user') }}"
                                  enctype="multipart/form-data" class="ajax-form">
                                <div class="card-body">
                                    <div class="row">
                                        @if(auth()->user()->hasRole('super_admin'))
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="community">Community</label>
                                                    <select class="form-control" name="community">
                                                        <option value="">Select Community</option>
                                                        @foreach($communities as $community)
                                                            <option
                                                                value="{{ $community->id }}">{{ $community->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="dial_code">Dial Code</label>
                                                <select class="form-control" id="dial_code" name="dial_code">
                                                    @foreach($countries as $country)
                                                        <option
                                                            value="{{ $country->dial_code }}">{{$country->country_name}}
                                                            +{{ $country->dial_code }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" class="form-control" name="phone_number"
                                                       id="phone_number">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" name="name"
                                                       placeholder="Banned Guest Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="license_plate">License Plate </label>
                                                <input type="text" class="form-control" name="license_plate"
                                                       placeholder="License Plate">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <button type="submit" class="btn btn-primary" id="add_banned_user_success">Add
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>

        var error_message = '<?php echo $errors->first();?>'
        var error = '<?php echo $errors->any();?>'
        if (error) {
            if (error_message != '') {
                notifyError("Please select community!")
            }
        }

        $(document).ready(function () {
            $('#phone_number, #name, #license').on('keyup', function () {
                if ($('#phone_number').val() == '' && $('#name').val() == '' && $('#license').val() == '') {
                    $('#add_banned_user_success').attr('disabled', 'disabled');
                } else {
                    $('#add_banned_user_success').removeAttr('disabled');
                }
            });

            $('#closeResidentsImportModalBtn').click(function () {
                $("#resident-import-modal").css("display", "none");
                $('.a').css('display', 'none');
            });
            getModalUserData();

            function getModalUserData() {
                showLoader()
                $.ajax({
                    type: 'GET',
                    url: "{{route('admin.residential.communities-data')}}",
                    success: function (result) {
                        var options = '<option disabled selected>Select Community</option>';
                        $.each(result['data'], function (key, row) {
                            options += `<option value="${row.id}">${row.name}</option>`;
                        });
                        $('#retrieve_community').html(options);
                        hideLoader()
                    }
                });
            }

            var $formattedPhone = '999-999-9999';

            $('#phone_number').inputmask({mask: $formattedPhone});

            $('#dial_code').change(function () {

                $.ajax({
                    url: "{{ route("get-phone-format-by-dial-code") }}",
                    type: "POST",
                    data: {dial_code: $(this).val()},
                    dataType: "JSON",
                    success: function (result) {
                        if (result.countryPhoneFormat != null) {
                            $('#phone_number').inputmask({mask: result.countryPhoneFormat.format});
                        } else {
                            $('#phone_number').inputmask({mask: '999-999-9999'});
                        }
                    }
                });

            });
        });
    </script>
@endpush
