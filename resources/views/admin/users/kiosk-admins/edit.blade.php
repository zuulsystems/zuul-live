@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Edit Kiosk Admin</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Kiosk Admin</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.user.kiosk-admins.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.user.kiosk-admins.update', [$kiosk_admin->id])}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first_name">First Name </label>
                                                <input type="text" name="first_name" id="first_name"
                                                       placeholder="First Name" class="form-control"
                                                       value="{{$kiosk_admin->first_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="last_name">Last Name </label>
                                                <input type="text" name="last_name" id="last_name"
                                                       placeholder="Last Name" class="form-control"
                                                       value="{{$kiosk_admin->last_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Email </label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control" value="{{$kiosk_admin->email}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="dial_code">Country code</label>
                                                @php
                                                    $url = route('get-phone-format-by-dial-code')
                                                @endphp
                                                <select id="dial_code" name="dial_code" class="form-control select2bs4"
                                                        onchange="changePhoneDigitsFormat1('{{ $url }}')">
                                                    @foreach($countries as $country)
                                                        <option value="+{{$country->dial_code}}"
                                                                @if($country->dial_code == $kiosk_admin->dial_code)selected="true"@endif>{{$country->country_name}}
                                                            +{{$country->dial_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" class="form-control"
                                                       value="{{$kiosk_admin->phone_number}}" name="phone_number"
                                                       id="phone_number">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="community_id">Community Assigned</label>
                                                <select id="community_id" name="community_id"
                                                        class="form-control select2bs4">
                                                    <option value="">Select Community</option>
                                                    @foreach($communities as $community)
                                                        <option value="{{$community->id}}"
                                                                @if($community->id == $kiosk_admin->community_id)selected="true"@endif>{{$community->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="profile_image">Profile Image</label>
                                                <input type="file" name="profile_image" id="profile_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Profile Picture.</div>
                                                <img src="{{$kiosk_admin->profile_image_url}}" style="width: 100px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var $formattedPhone = "{{ $phoneFormat }}";
            $('#phone_number').inputmask({mask: $formattedPhone});
        });
    </script>
@endpush
