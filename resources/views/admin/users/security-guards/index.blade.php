@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Add Guard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Guard</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_name">Name</label>
                                                <input type="text" name="full_name" id="full_name" placeholder="Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_phone">Phone Number</label>
                                                <input type="text" name="formatted_phone" id="formatted_phone"
                                                       placeholder="Phone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                                <h3 class="card-title">
                                    <a href="{{route('admin.user.guards.create')}}">
                                        <i class="fas fa-plus-circle"></i> Add Guard
                                    </a>
                                </h3>
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="guestTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Community</th>
                                    <th>Created At</th>
                                    @cannot('is-guard-admin')
                                        <th>Action(s)</th>
                                    @endcannot
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Permission Model--->
    <div class="modal fade" id="permission-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Guard Permission(s)</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.user.guard-permission')}}"
                      class="ajax-form" id="guard-permission-form">
                    <input type="hidden" name="user_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="street_address">Can Check License Plate </label>
                                    <input type="checkbox" name="can_check_license_plate" id="can_check_license_plate"
                                           value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="street_address">Guard Admin</label>
                                    <input type="checkbox" name="is_guard_admin" id="is_guard_admin" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="street_address">Remote Guard</label>
                                    <input type="checkbox" name="remote_guard" id="remote_guard" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Revoke Guard Model--->
    <div class="modal fade" id="revoke-guard-permission-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Revoke Permission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.user.revoke-guard')}}"
                      class="ajax-form">
                    <input type="hidden" name="guard_id" id="guard_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="revoke_guard_rights">Are you sure you want to revoke this guard from the
                                        community? </label>
                                    <select class="form-control">
                                        <option>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@push('scripts')
    <script>

        //guard permission model
        $(document).on('click', '.guard-permission-model', function () {
            let id = $(this).data('id');
            $("#remote_guard").bootstrapSwitch('state', false);
            $("#is_guard_admin").bootstrapSwitch('state', false);
            $("#can_check_license_plate").bootstrapSwitch('state', false);
            showLoader();

            $.ajax({
                url: "{{route('admin.user.check-guard-permission')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    console.log(result.data);
                    hideLoader();
                    if (result.status === true) {
                        $('input[name="user_id"]').val(id);
                        if (result.data.isGuardAdmin === true) {
                            $("#is_guard_admin").bootstrapSwitch('state', true);
                        }
                        if (result.data.canCheckLicensePlate === true) {
                            $("#can_check_license_plate").bootstrapSwitch('state', true);
                        }
                        if (result.data.checkRemoteGuard === true) {
                            $("#remote_guard").bootstrapSwitch('state', true);
                        }
                        $('#permission-modal').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        //guard permission model
        $(document).on('click', '.revoke-guard-permission-model', function () {
            let id = $(this).data('id');
            $('#guard_id').val(id);
            $('#revoke-guard-permission-model').modal('show');
        });

        //resend guard verification
        $(document).on('click', '.btn-resend-guard-verification-code', function () {
            let id = $(this).data('id');
            showLoader();
            $.ajax({
                url: "{{route('admin.user.resend-guard-verification-code')}}",
                type: "POST",
                data: {
                    id: id
                }
            });
        });
        /*initialize datatable*/
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'full_name',
                name: "first_name"
            },
            {
                data: 'formatted_phone',
                name: 'phone_number',
            },
            {
                data: 'email',
            },
            {
                data: 'role_name',
                name: 'role.name',
            },
            {
                data: 'community',
                name: 'community.name',
            },
            {
                data: 'created_at',
            },
        ];

        var isGuard = "{{ Gate::allows('is-guard-admin') }}";

        if (isGuard == false) {
            var links = {
                data: function (data) {
                    return data.links;
                }
            };
            columns.push(links);
        }

        zuulDataTableSortableGuestLogs('#guestTable', "{{route('admin.user.guards-data')}}?userId={{$userId ?? null}}", columns, [6, 7]);

        //search filter
        $('.search-btn').click(function () {
            var ajax = "{{route('admin.user.guards-data')}}?userId={{$userId ?? null}}&" + $("#search-filter").serialize();
            zuulDataTableSortableGuestLogs('#guestTable', ajax, columns, [6, 7]);
        });

    </script>
@endpush
