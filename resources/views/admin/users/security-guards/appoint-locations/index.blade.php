@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Guard Appoint Location</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Guard Appoint Location</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="lat">Latitude</label>
                                                <input type="text" name="lat" id="lat" placeholder="Latitude"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="long">Longitude</label>
                                                <input type="text" name="long" id="long"
                                                       placeholder="Longitude" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                                <h3 class="card-title">
                                    <a href="{{route('admin.user.guard-appoint.appoint-locations.create',['userId' => $userId])}}">
                                        <i class="fas fa-plus-circle"></i> Add Guard Appoint Location
                                    </a>
                                </h3>
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="guardAppointLocationTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    @cannot('is-guard-admin')
                                        <th>Action(s)</th>
                                    @endcannot
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>

        /*initialize datatable*/
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'lat',
                name: "lat"
            },
            {
                data: 'long',
                name: 'long',
            }
        ];

        var isGuard = "{{ Gate::allows('is-guard-admin') }}";

        if (isGuard == false) {
            var links = {
                data: function (data) {
                    return data.links;
                }
            };
            columns.push(links);
        }

        zuulDataTableSortable('#guardAppointLocationTable', "{{route('admin.user.guard-appoint.guard-appoint-location-data')}}?userId={{$userId ?? null}}", columns, [3]);

        //search filter
        $('.search-btn').click(function () {
            var ajax = "{{route('admin.user.guard-appoint.guard-appoint-location-data')}}?userId={{$userId ?? null}}&" + $("#search-filter").serialize();
            zuulDataTableSortable('#guardAppointLocationTable', ajax, columns, [3]);
        });

    </script>
@endpush
