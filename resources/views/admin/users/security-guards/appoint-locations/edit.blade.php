@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Update Guard Appoint Location</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Update Guard Appoint Location</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.user.guard-appoint.appoint-locations.index',['userId' => $guardAppointLocation->user_id])}}"
                                   class="add_advertiser">
                                    <i class="fas fa-list"></i> Guards Appoint Locations
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.user.guard-appoint.appoint-locations.update',$guardAppointLocation->id)}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                @method('PUT')
                                <input type="hidden" value="{{$guardAppointLocation->user_id}}" name="guard_id"/>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="lat">Latitude </label>
                                                <input type="text" name="lat" id="lat"
                                                       placeholder="Latitude" class="form-control"
                                                       value="{{$guardAppointLocation->lat}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="long">Longitude </label>
                                                <input type="text" name="long" id="long"
                                                       placeholder="Longitude" class="form-control"
                                                       value="{{$guardAppointLocation->long}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

