@extends('admin.layout.master')
@push('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
@endpush

<style>
    .note-editor.note-frame .note-editing-area .note-editable, .note-editor.note-airframe .note-editing-area .note-editable {
        background-color: #fff;
        color: #000;
        padding: 10px;
        height: 250px;
        overflow: auto;
        word-wrap: break-word;
    }

    .container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
        width: 100%;
        padding-right: 7.5px;
        padding-top: 10.5px;
        padding-bottom: 5px;
        padding-left: 7.5px;
        margin-right: auto;
        margin-left: auto;
        border: 2px solid #ede7e7;
        background: white;
        border-radius: 7px;
    }
</style>
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Printer Template</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Printer Template</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.settings.printer-templates.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.settings.printer-templates.update', [$printerTemplate->id])}}"
                                  class="ajax-form">
                                {{ method_field('PUT') }}

                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Name </label>
                                        <input type="text" name="name" id="name" placeholder="Template Name"
                                               class="form-control" value="{{ $printerTemplate->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select id="community_id" name="community_id" class="form-control">
                                            <option value="">Select</option>
                                            @foreach ($communities as $community)
                                                <option
                                                    value="{{$community->id}}" {{ $printerTemplate->community_id == $community->id ? 'selected' : '' }}>{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="template">Template </label>
                                        <textarea class="form-control text-editor" name="template"
                                                  id="template">{{$printerTemplate->template}}</textarea>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        // Summernote
        $('.text-editor').summernote();
    </script>
@endpush
