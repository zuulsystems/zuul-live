@extends('admin.layout.master')
@push('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
@endpush
@section('content')

    <style>
        .container-fluid, .container-lg, .container-md, .container-sm, .container-xl {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>


    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Edit Scanner</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Scanner</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.settings.scanners.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.settings.scanners.update', [$scanner->id])}}" class="ajax-form"
                                  enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Name </label>
                                        <input type="text" name="name" id="title" placeholder="Scanner Name"
                                               class="form-control" value="{{$scanner->name}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Camera </label>
                                        <select
                                            id="camera_id" name="camera_id" class="form-control select2bs4">
                                            @if(empty($scanner->camera))
                                                <option value="">Not Found</option>
                                            @else
                                                <option value="">Select Camera</option>
                                                @foreach($cameras as $camera)
                                                    <option value="{{$camera->id}}"
                                                            @if($camera->id == $scanner->camera_id)selected="true"@endif>{{$camera->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Mac Address </label>
                                        <input type="text" name="mac_address" id="mac_address" placeholder="Mac Address"
                                               class="form-control" value="{{$scanner->mac_address}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="community_id">Community Assigned</label>
                                        <select
                                            id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach($communities as $community)
                                                <option value="{{$community->id}}"
                                                        @if($community->id == $scanner->community_id)selected="true"@endif>{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="device_type_id">Device Type</label>
                                        <select
                                            id="device_type_id" name="device_type_id" class="form-control">
                                            <option value="">Select Device Type</option>
                                            @foreach($device_types as $device_type)
                                                <option value="{{$device_type->id}}"
                                                        @if($device_type->id == $scanner->device_type_id)selected="true"@endif>{{$device_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="rfid_multiplier">RFID Multiplier</label>
                                        <input type="number" class="form-control" name="rfid_multiplier"
                                               id="rfid_multiplier" placeholder="RFID Multiplier"
                                               value="{{$scanner->rfid_multiplier}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="street_address">Enable/Disable </label>
                                        <input type="checkbox" name="enable" value="1" data-bootstrap-switch
                                               data-off-color="danger" data-on-color="success" data-on-text="Enable"
                                               data-off-text="Disable" @if($scanner->enable)checked="true"@endif>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        // Summernote
        $('.text-editor').summernote();

        $(document).ready(function () {
            $('#community_id').change(function () {
                var $communityId = $(this).val();
                var $url = "{{ route('admin.settings.get-cameras-by-community') }}";
                $.ajax({
                    url: $url,
                    type: "POST",
                    data: {id: $communityId},
                    dataType: 'json',
                    success: function (result) {
                        if (result.length > 0) {
                            var cameras = '';
                            for (i = 0; i < result.length; i++) {
                                cameras += '<option value="' + result[i].id + '">' + result[i].name + '</option>';
                            }

                            $('#camera_id').html(cameras);
                        } else {
                            $('#camera_id').html("<option value=''>Not Found</option>");
                        }
                    }
                });
            });
        });
    </script>
@endpush
