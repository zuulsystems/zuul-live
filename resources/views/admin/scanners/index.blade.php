@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid ">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Scanner List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Scanner List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body ">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name" placeholder="Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community Name</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="mac_address">MAC Address</label>
                                                <input type="text" name="mac_address" id="mac_address"
                                                       placeholder="MAC Address" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                                <h3 class="card-title">
                                    <a href="{{route('admin.settings.scanners.create')}}" class="add_advertiser">
                                        <i class="fas fa-plus-circle"></i> Add Scanner
                                    </a>
                                </h3>
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="scannerTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Camera</th>
                                    <th>Mac address</th>
                                    <th>Community</th>
                                    <th>Qscan/RFID</th>
                                    <th>Last Event</th>
                                    <th>Last Event At</th>
                                    <th>Status</th>
                                    <th>Status At</th>
                                    <th>Created At</th>
                                    @cannot('is-guard-admin')
                                        <th>Action(s)</th>
                                    @endcannot
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'name',
            },

            {
                data: 'formatted_mac_address_and_secret_key',
                name: 'mac_address'
            },
            {
                data: 'community_name',
                name: 'community.name',
            },
            {
                data: 'rfid_multiplier',
                name: 'rfid_multiplier',
            },
            {
                data: 'last_event',
                name: 'last_event'
            },
            {
                data: 'last_event_at',
            },
            {
                data: function (data) {
                    let html = '';
                    if (data.online_status === '1') {
                        html = `<i class="fas fa-dot-circle bg-green"></i>`;
                    } else {
                        html = `<i class="fas fa-dot-circle bg-danger"></i>`;
                    }
                    return html;
                },
                name: 'online_status',
            },
            {
                data: 'online_at',
                name: 'online_at'
            },
            {
                data: 'created_at',
            },
        ];

        var isGuard = "{{ Gate::allows('is-guard-admin') }}";
        var isCommunityAdmin = "{{ Gate::allows('community-admin') }}";


        if (isGuard == false) {
            var editLink = {
                data: function (data) {
                    return data.edit_link;
                },
                name: 'edit_link'
            };
            columns.push(editLink);
        }
        if (isCommunityAdmin == false) {
            var cameraname = {
                data: function (data) {

                    return data.camera_name;
                },
                name: 'id'

            };
            columns.splice(2, 0, cameraname);
        } else {
            var cameraname = {
                data: function (data) {

                    return data.camera_name;
                },
                name: 'camera_name'

            };
            columns.splice(2, 0, cameraname);
        }

        zuulDataTableSortable('#scannerTable', "{{route('admin.settings.scanners-data')}}", columns, [7, 10, 11]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.settings.scanners-data')}}?" + $("#search-filter").serialize();
            zuulDataTableSortable('#scannerTable', ajax, columns, [7, 9]);
        })
    </script>
@endpush
