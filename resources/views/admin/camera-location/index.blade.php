@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Camera Location List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Camera Location List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="camera_id">Camera ID</label>
                                                <input type="text" name="camera_id" id="camera_id"
                                                       placeholder="Camera ID" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="camera_location">Camera Location</label>
                                                <input type="text" name="camera_location" id="camera_location"
                                                       placeholder="Camera Location" class="form-control">
                                            </div>
                                        </div>
                                        @if(Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('tl_admin'))
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="community_name">Community Name</label>
                                                    <input type="text" name="community_name" id="community_name"
                                                           placeholder="Camera Name" class="form-control">
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Camera ID</th>
                                    <th>Camera Location</th>
                                    @if(Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('tl_admin'))
                                        <th>Community Name</th>
                                    @endif
                                    <th>Total Ticket(s)</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="communities-modal">
        <form method="post" action="" id="ajax-form" class="ajax-form">
            @method('PATCH')
            <input type="hidden" name="url" id="url" value="{{route('admin.locations.update', ':id:')}}"/>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><span id="communityHeading"></span> Community</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="communities">Communities</label>
                                    <select class="form-control" name="community_id" id="community_id">
                                        <option value="" disabled selected>Please Select a Community</option>
                                        @forelse($communities as $id => $name)
                                            <option value="{{$id}}">{{$name}}</option>
                                        @empty
                                        @endforelse;
                                    </select>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
        </form>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {

            // DataTable Columns
            let columns = [
                {data: 'id'},
                {data: 'location_id'},
                {data: 'address'},
                // If User is Super Admin or TL Admin
                    @if(Auth::user()->hasRole('super_admin') || Auth::user()->hasRole('tl_admin'))
                {
                    // Show Community Name
                    data: 'community_name.name',

                    //Used for Searching
                    name: 'communityName.name',

                    // Show the Community Name if It Exists otherwise show error
                    render: function (data, type, row) {
                        return row.community_name == null ? "N/A" : data;
                    }
                },
                    @endif
                {
                    data: 'traffic_tickets_count'
                },
                {data: 'action'},
            ];

            //Render Datatables
            TLDataTable('#datatable', "{{route('admin.locations-data')}}?@if(isset($community))community_id={{$community}}@endif", columns);

            //If Add/Change Community is Clicked
            $("body").on('click', '#communitySelect', function () {

                // Remove the Old Selected Attribute
                $('#community_id option:selected').removeAttr('selected');

                // Get the Community ID of current row
                let community = $(this).attr('community');

                // Show Options accroding to add or edit
                if ($(this).attr('community') === undefined) {

                    // Display Add Community Heading on Modal
                    $('#communityHeading').html('Add');

                    // Display Placeholder as default option
                    $('#community_id option:nth-child(1)').attr('selected', 'selected');

                } else {

                    // Display Edit Community as heading on Modal
                    $('#communityHeading').html('Edit');

                    // Selected the Previously Selected Community as default option
                    $(`#community_id option[value="${community}"]`).attr('selected', 'selected');
                }

                //Get The ID of Current Camera
                let camera_id = ($(this).closest('tr').children('td').html());

                // Change the Form Action URL
                $('#ajax-form').attr('action', $('#url').val().replace(':id:', camera_id));

                //Show the Modal
                $('#communities-modal').modal('show');
            });

            $('.search-btn').click(function () {
                var ajax = "{{route('admin.locations-data')}}?" + $("#search-filter").serialize();
                TLDataTable('#datatable', ajax, columns, [4]);
            })

        });

    </script>
@endpush
