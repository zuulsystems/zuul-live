@extends('admin.layout.master')
@push('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
@endpush
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Edit Camera</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Camera</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.settings.cameras.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.settings.cameras.update', [$camera->id])}}" class="ajax-form"
                                  enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Name </label>
                                        <input type="text" name="name" id="title" placeholder="Scanner Name"
                                               class="form-control" value="{{$camera->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Mac Address </label>
                                        <input type="text" name="mac_address" id="mac_address" placeholder="Mac Address"
                                               class="form-control" value="{{$camera->mac_address}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="community_id">Community Assigned</label>
                                        <select
                                            id="community_id" name="community_id" class="form-control">
                                            <option value="">Select Scanner</option>
                                            @foreach($communities as $community)
                                                <option value="{{$community->id}}"
                                                        @if($community->id == $camera->community_id)selected="true"@endif>{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="scanner_id">Scanner Assigned</label>
                                        <select
                                            id="scanner_id" name="scanner_id" class="form-control">
                                            <option value="">Select Scanner</option>
                                            @foreach($scanners as $scanner)
                                                <option value="{{$scanner->id}}"
                                                        @if($scanner->id == $camera->scanner_id)selected="true"@endif>{{$scanner->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="device_type">Device Type</label>
                                        <select
                                            id="device_type" name="device_type" class="form-control">
                                            <option value="">Select Device Type</option>
                                            <option value="lpr" {{($camera->device_type == "lpr")?"selected":""}}>LPR
                                            </option>
                                            <option
                                                value="non_lpr" {{($camera->device_type == "non_lpr")?"selected":""}}>
                                                NON LPR
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="connected_pi_id">Connected Pi Assigned</label>
                                        <select
                                            id="connected_pi_id" name="connected_pi_id" class="form-control">
                                            <option value="">Select Connected Pi</option>
                                            @foreach($connected_pi as $pi)
                                                <option value="{{$pi->id}}"
                                                        @if($pi->id == $camera->connected_pi_id)selected="true"@endif>{{$pi->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="lat">Lat </label>
                                        <input type="text" name="lat" id="lat"
                                               placeholder="Lat" class="form-control" value="{{$camera->lat}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="long">Long </label>
                                        <input type="text" name="long" id="long"
                                               placeholder="Long" class="form-control" value="{{$camera->long}}">
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
    <script>
        // Summernote
        $('.text-editor').summernote();
    </script>
@endpush
