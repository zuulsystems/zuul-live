@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Communities List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Communities List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body  ">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">Community</label>
                                                <input type="text" name="name" id="name" placeholder="Community"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <input type="text" name="address" id="address" placeholder="Address"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="communitiesTicketTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Community</th>
                                    <th>Address</th>
                                    <th>Camera Location(s)</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'name',
            },
            {
                data: 'address',
            },
            {
                data: function (data) {
                    return data.locations_count_edit;
                },
                name: 'id'
            },
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];
        zuulDataTable('#communitiesTicketTable', "{{route('admin.traffic-tickets.tickets-logix-communities-data')}}", columns, [4]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.traffic-tickets.tickets-logix-communities-data')}}?" + $("#search-filter").serialize();
            zuulDataTable('#communitiesTicketTable', ajax, columns, [4]);
        })

        $(document).on('click', '.fetch_tickets', function () {
            let community = $(this).data('id');
            let url = "{{ route('admin.traffic-tickets.fetch-tickets-by-community', ':id') }}";
            showLoader();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', community),
            }).done(hideLoader());

        });
    </script>
@endpush
