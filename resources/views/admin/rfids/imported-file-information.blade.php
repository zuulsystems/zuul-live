@extends('admin.layout.master')
@section('content')
    <h1>Yes</h1>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">

                    <div class="col-md-3">
                        <h1>Imported File Result</h1>
                    </div>
                    <div class="col-md-3">
                        <p id="recordsImports"></p>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Imported File Result</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">

                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <table id="importedFileDataTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>RFID Tag Number</th>
                                    <th>House Assigned</th>
                                    <th>Resident First Name</th>
                                    <th>Resident Last Name</th>
                                    <th>Mailing Address</th>
                                    <th>Driver License Number</th>
                                    <th>License Plate</th>
                                    <th>State Registered</th>
                                    <th>Vehicle Type</th>
                                    <th>Make</th>
                                    <th>Color</th>
                                    <th>Year</th>
                                    <th>Model</th>
                                    <th>Insurance Company Name</th>
                                    <th>Policy Number</th>
                                    <th>Policy Expiration Date</th>
                                    <th>Additional Drivers</th>
                                    <th>Status</th>
                                    <th>Message</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        var count = 0;

        function zuulImportFileDataTableSortable(selector, url, columns, disableColumnSearch = [], DefaultOrderColumn = 0) {

            $(selector).DataTable({
                "processing": true,
                "scrollX": true,
                "serverSide": true,
                "ajax": url,
                "columns": columns,
                "pageLength": 50,
                "destroy": true,
                "searching": false,
                "order": [[DefaultOrderColumn, "desc"]],
                "columnDefs": [
                    {"orderable": false, "targets": disableColumnSearch}
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "initComplete": function () {
                    var api = this.api();

                    // For each column
                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            // Set the header cell to contain the input element
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" placeholder="' + title + '" />');

                            // On every keypress in this input
                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();

                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})';

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                },

            });
        }


        let columns = [
            {data: 'rfid_tag_num'},
            {data: 'house_assigned'},
            {data: 'resident_first_name'},
            {data: 'resident_last_name'},
            {data: 'mailing_address'},
            {data: 'driver_license_number'},
            {data: 'license_plate'},
            {data: 'state_registered'},
            {data: 'vehicle_type'},
            {data: 'make'},
            {data: 'color'},
            {data: 'year'},
            {data: 'model'},
            {data: 'insurance_company_name'},
            {data: 'policy_number'},
            {data: 'policy_expiration_date'},
            {data: 'additional_drivers'},
            {data: 'status'},
            {data: 'message'},
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];

        var ajax = "{{route('admin.rfid.rfid-import-file-information-data')}}?file_name={{$fileName ?? ''}}";
        zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [4]);

        var b = 0;

        function saveRfidsFromRfidCsvTable() {
            var a = 0;
            $.ajax({
                "url": "{{ route("admin.rfid.save-rfids-from-rfid-csvs-table",["file_name"=>$fileName]) }}",
                "type": "GET",
                "dataType": "JSON",
                "success": function (result) {
                    if (result.recordsExecuted == 0) {
                        var $a = $('#recordsImports').html();
                        if ($a != "Completed") {
                            console.log('just one time came here');
                            zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [18]);
                        }
                        $('#recordsImports').html('Completed');
                    } else {
                        var c = parseInt(result.recordsExecuted);
                        b = b + c;
                        $('#recordsImports').html(b + ' records executed successfully');
                        zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [18]);
                    }
                }
            });
        }


        $.ajax({
            url: ajax,
            type: "GET",
            success: function (result) {
                if (result.recordsTotal > 0) {
                    $('#delete_file').css('display', 'inline-block');
                    $('#download_csv').css('display', 'inline-block');
                    $('#download_failed_csv').css('display', 'inline-block');
                    var $fileName = $('#file_name').val();
                    var $url = "{{ route('admin.rfid.download-rfid-imported-file') }}?file_name=" + $fileName;
                    $('#download-import-file-link').attr('href', $url);
                    var $urlFailRecords = "{{ route('admin.rfid.download-fail-records-rfid-imported-file') }}?file_name=" + $fileName;
                    $('#download-failed-import-file-link').attr('href', $urlFailRecords);
                }
            }
        });


        $('.search-btn').click(function () {

            var ajax = "{{route('admin.rfid.rfids-imported-file-data')}}?" + $("#search-filter").serialize();
            zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [4]);

            $.ajax({
                url: ajax,
                type: "GET",
                success: function (result) {
                    if (result.recordsTotal > 0) {
                        $('#delete_file').css('display', 'inline-block');
                        $('#download_csv').css('display', 'inline-block');
                        $('#download_failed_csv').css('display', 'inline-block');
                        var $fileName = $('#file_name').val();
                        var $url = "{{ route('admin.rfid.download-rfid-imported-file') }}?file_name=" + $fileName;
                        $('#download-import-file-link').attr('href', $url);
                        var $urlFailRecords = "{{ route('admin.rfid.download-fail-records-rfid-imported-file') }}?file_name=" + $fileName;
                        $('#download-failed-import-file-link').attr('href', $urlFailRecords);
                    }
                }
            });

            count++;

        });

        $('#delete_file').click(function () {

            var importedFileName = $('#file_name').val();

            $.ajax({
                url: "{{ route('admin.rfid.delete-rfid-imported-file') }}?" + $("#search-filter").serialize(),
                type: "GET",
            });
        });

        $('#file_name').keyup(function () {
            var $fileName = $('#file_name').val();
            var $url = "{{ route('admin.rfid.download-rfid-imported-file') }}?file_name=" + $fileName;
            $('#download-import-file-link').attr('href', $url);
            var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
            $('#download-failed-import-file-link').attr('href', $urlFailRecords);
        });

    </script>
@endpush
