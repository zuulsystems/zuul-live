@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>RFID Tags</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">RFID Tags</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="rfid_tag_num">RFID Tag #</label>
                                                <input type="text" name="rfid_tag_num" id="rfid_tag_num"
                                                       placeholder="RFID Tag #" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="license">License Plate</label>
                                                <input type="text" name="license" id="license"
                                                       placeholder="License Plate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="resident_name">Resident Name</label>
                                                <input type="text" name="resident_name" id="resident_name"
                                                       placeholder="Resident Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="phone">Phone #</label>
                                                <input type="text" name="phone" id="phone" placeholder="Phone"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community Name</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="lot_number">Lot Number</label>
                                                <input type="text" name="lot_number" id="lot_number"
                                                       placeholder="Lot Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <input type="text" name="address" id="address"
                                                       placeholder="Address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="additional_drivers">Additional Drivers</label>
                                                <input type="text" name="additional_drivers" id="additional_drivers"
                                                       placeholder="Additional Drivers" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="0">Active</option>
                                                    <option value="1">Suspended</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">

                                @if(!auth()->user()->hasRole('guard_admin'))
                                    <a href="{{route('admin.rfid.rfids.create')}}" class="add_advertiser">
                                        <i class="fas fa-plus-circle"></i> Add RFID Tags
                                    </a>
                                    @cannot('kiosk')
                                        <a href="javascript:void(0)" class="" data-toggle="modal"
                                           data-target="#rfid-import-modal" id="rfidImportModalBtn">
                                            <i class="fas fa-file-import"></i> import Rfid
                                        </a>
                                        <a href="{{ route('admin.rfid.rfid-imported-files') }}" class="">
                                            <i class="fas fa-file"></i> Imported Files
                                        </a>
                                    @endcannot
                                @endif

                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="rfidTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>RFID Tag #</th>
                                    <th>Resident Name</th>
                                    <th>Address</th>
                                    <th>Phone #</th>
                                    <th>Community</th>
                                    <th>License Plate</th>
                                    <th>Email</th>
                                    <th>Lot Number</th>
                                    <th>Additional Drivers</th>
                                    <th>Vehicle Info</th>
                                    <th>Status</th>
                                    @cannot('is-guard-admin')
                                        <th>Action(s)</th>
                                    @endcannot
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @php
        $showClass = '';
        $rfidImportModel = '';
        if ($errors->any())
        {
        $showClass = 'show';
        $rfidImportModel = 'display: block';
        }
    @endphp

        <!-- House Model--->
    <div class="modal fade" id="rfid-import-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import CSV</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            id="closeRFIDImportModalBtn">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.rfid.import-rfid-tag')}}"
                      enctype="multipart/form-data" class="ajax-form">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="rfid_csv">CSV file to import</label>
                                    <input type="file" name="rfid_csv" id="rfid_csv" class="col-md-12" accept=".csv"/>
                                </div>
                            </div>
                        </div>
                        @if(auth()->user()->role_id == 1)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach ($communities as $community)
                                                <option value="{{ $community->id }}">{{ $community->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('community_id'))
                                            <span style="color:red;padding-top:2px"
                                                  class="error a">Select a community.</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal-footer justify-content-between">
                                <a href="{{ asset('assets/csv-sample/transponder_upload_n.csv') }}"
                                   class="btn btn-default">Download Sample
                                    File</a>
                                <button type="submit" class="btn btn-primary" id="importBtn">Import</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@php
    if(session()->has('rfidImportedSuccessfully'))
    {
@endphp
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-success" aria-live="polite" style="display: block; opacity: 1;">
        <div class="toast-message">RFID imported successfully.</div>
    </div>
</div>
<script>
    setTimeout(function () {
        document.getElementsByClassName('toast-success')[0].style.display = "none";
    }, 1000);
</script>
@php
    }
@endphp

@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'rfid_tag_num',
            },
            {
                data: 'resident_name',
                name: 'resident_name',
            },
            {
                data: 'resident_street_address',
                name: 'resident_street_address',
            },
            {
                data: 'formatted_resident_phone',
                name: 'formatted_resident_phone',
            },
            {
                data: "community_name",
                name: "community_name"
            },
            {
                data: "vehicle_license_plate",
                name: "vehicle_license_plate"
            },
            {
                data: "resident_email",
                name: "resident_email"
            },
            {
                data: "resident_house_lot_number",
                name: "resident_house_lot_number"
            },
            {
                data: "additional_drivers",
                name: "additional_drivers"
            },
            {
                data: "vehicle_info"
            },
            {
                data: 'formatted_status',
                name: 'formatted_status',
            }

        ];
        var isGuard = "{{ Gate::allows('is-guard-admin') }}";

        if (isGuard == false) {
            var links = {
                data: function (data) {
                    return data.links;
                }
            };
            columns.push(links);
        }
        let check = "<?= !auth()->user()->hasRole('guard_admin') ?>";
        if (check) {
            zuulDataTableSortableGuestLogs('#rfidTable',
                "{{ route('admin.rfid.rfids-data') }}?communityId={{ $communityId ?? null }}&userId={{ $userId ?? null }}&houseId={{ $houseId ?? null }}&rfidId={{ $rfidId ?? null }}", columns, [11]);

            $('.search-btn').click(function () {
                var ajax = "{{ route('admin.rfid.rfids-data') }}?communityId={{ $communityId ?? null }}&userId={{ $userId ?? null }}&houseId={{ $houseId ?? null }}&rfidId = {{ $rfidId ?? null }}&" +
                    $("#search-filter").serialize();
                zuulDataTableSortableGuestLogs('#rfidTable', ajax, columns, [10]);
            })

        } else {
            zuulDataTableSortableGuestLogs('#rfidTable',
                "{{ route('admin.rfid.rfids-data') }}?communityId={{ $communityId ?? null }}&userId={{ $userId ?? null }}&houseId={{ $houseId ?? null }}&rfidId={{ $rfidId ?? null }}", columns, [10]);

            $('.search-btn').click(function () {
                var ajax = "{{ route('admin.rfid.rfids-data') }}?communityId={{ $communityId ?? null }}&userId={{ $userId ?? null }}&houseId={{ $houseId ?? null }}&rfidId = {{ $rfidId ?? null }}&" +
                    $("#search-filter").serialize();
                zuulDataTableSortableGuestLogs('#rfidTable', ajax, columns, [9]);
            })

        }

        $('#rfidImportModalBtn').click(function () {
            ("#skipRecordsCSVBtn").css('display', 'none');
            $("#skipErrorMsg").css('display', 'none');
            $('.a').css('display', 'none');
        });

        $('#closeRFIDImportModalBtn').click(function () {
            $("#rfid-import-modal").css("display", "none");
            $('.a').css('display', 'none');
        });

        $('#rfidCSVimport').click(function () {
            $("#csv-complete-modal").css("display", "none");
        });


    </script>
@endpush
