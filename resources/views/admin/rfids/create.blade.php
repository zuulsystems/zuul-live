@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid card-bg-heading">
                <div class="row mb-2 ">
                    <div class="col-sm-6">
                        <h1>Add RFID Tag</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add RFID Tag</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.rfid.rfids.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.rfid.rfids.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="text-center">Applicant Information</h4>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="first_name">RFID Tag Number </label>
                                                        <input type="number" min="1" name="rfid_tag_num"
                                                               id="rfid_tag_num"
                                                               placeholder="RFID Tag Number" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="community_id">Community Assigned</label>
                                                        <select
                                                            id="community_id" name="community_id"
                                                            class="form-control select2bs4">
                                                            <option value="">Select Community</option>
                                                            @foreach($communities as $community)
                                                                <option
                                                                    value="{{$community->id}}">{{$community->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="house_id">House Assigned</label>
                                                        <select id="house_id" name="house_id"
                                                                class="form-control select2bs4">
                                                            <option value="">Select House</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="house_id">Resident Assigned</label>
                                                        <select id="resident_id" name="resident_id"
                                                                class="form-control select2bs4">
                                                            <option value="">Select Resident</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="mailing_address">Mailing Address </label>
                                                        <input type="text" name="mailing_address" id="mailing_address"
                                                               placeholder="Mailing Address " class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <h4 class="text-center">Vehicle Information for Registration</h4>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="driver_license_number">Driver License Number</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Driver License Number"
                                                               name="driver_license_number" id="driver_license_number"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="state">State Registered</label>
                                                    <input type="text" name="state" id="state" class="form-control"
                                                           placeholder="State Registered"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="vehicle_type">Vehicle Type</label>
                                                        <select class="form-control" name="vehicle_type"
                                                                id="vehicle_type">
                                                            <option value="Car">Car</option>
                                                            <option value="SUV">SUV</option>
                                                            <option value="Truck">Truck</option>
                                                            <option value="Commercial">Commercial</option>
                                                            <option value="Golf Cart">Golf Cart</option>
                                                            <option value="Bicycle">Bicycle</option>
                                                            <option value="other">Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="activation_date">Activation Date</label>
                                                    <input type="date" class="form-control" name="activation_date"
                                                           id="activation_date"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="expiration_date">Expiration Date</label>
                                                        <input type="date" class="form-control" name="expiration_date"
                                                               id="expiration_date"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="make">Make</label>
                                                        <input type="text" class="form-control" name="make" id="make"
                                                               placeholder="Make"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="color">Color</label>
                                                        <input type="text" class="form-control" name="color" id="color"
                                                               placeholder="Color"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="year">Year</label>
                                                        <input type="text" class="form-control" name="year" id="year"
                                                               placeholder="Year"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="model">Model</label>
                                                        <input type="text" class="form-control" name="model" id="model"
                                                               placeholder="Model"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="license_plate">License Plate</label>
                                                        <input type="text" class="form-control" name="license_plate"
                                                               id="license_plate" placeholder="License Plate"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="insurance_company_name">Insurance Company
                                                            Name</label>
                                                        <input type="text" class="form-control"
                                                               name="insurance_company_name" id="insurance_company_name"
                                                               placeholder="Insurance Company Name"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="policy_number">Policy Number</label>
                                                        <input type="text" class="form-control" name="policy_number"
                                                               id="policy_number" placeholder="Policy Number"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="model">Policy Expiration Date</label>
                                                        <input type="date" class="form-control"
                                                               name="policy_expiration_date" id="policy_expiration_date"
                                                               placeholder="Policy Expiration Date"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="policy_number">Additional Drivers</label>
                                                        <input type="text" class="form-control"
                                                               name="windshield_parking_sticker"
                                                               id="windshield_parking_sticker"
                                                               placeholder="Additional Drivers"/>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>


                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    let options = '';
                    if (result.status === true) {
                        options += `<option value="" selected>Select House</option>`;
                        $.each(result.result, function (key, row) {
                            options += `<option value="${row.id}">${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="" selected>No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                    $('#resident_id').html('<option value="">Select Resident</option>');
                    $('#street_address').html('');

                }
            });
        });

        /**
         * get user by houses
         */
        $(document).on('change', '#house_id', function () {
            let houseId = $(this).val();
            showLoader();
            $.ajax({
                type: 'GET',
                url: "{{route('admin.rfid.get-all-member-by-house-id')}}",
                data: {
                    houseId: houseId
                },
                success: function (result) {
                    hideLoader();
                    let options = '';
                    if (result.status === true) {
                        options += `<option value="">Select Resident</option>`;
                        $.each(result.result, function (key, row) {
                            options += `<option value="${row.id}" data-street-address="${row.street_address ?? ""}">${row.first_name ?? ""} ${row.last_name ?? ""}</option>`;
                        });
                    } else {
                        options = `<option value="">No Users Found !</option>`;
                    }
                    $('#resident_id').html(options);
                    //Initialize Select2 Elements
                    $('.select2bs4').select2({
                        theme: 'bootstrap4'
                    })
                }
            });
        });
        /**
         * get user by houses
         */
        $(document).on('change', '#resident_id', function () {
            let street_address = $('option:selected', this).attr('data-street-address');
            $('#mailing_address').val(street_address);
        });
    </script>
@endpush
