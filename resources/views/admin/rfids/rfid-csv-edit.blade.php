@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit RFID Csv</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit RFID Csv</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.rfid.rfid-csvs-table-record-update',[$rfidCsv->id])}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="first_name">RFID Tag Number </label>
                                                        <input type="number" min="1" name="rfid_tag_num"
                                                               id="rfid_tag_num"
                                                               placeholder="RFID Tag Number"
                                                               value="{{$rfidCsv->rfid_tag_num}}" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="house_id">House Assigned</label>
                                                        <select id="house_id" name="house_id"
                                                                class="form-control select2bs4">
                                                            <option value="">Select House</option>
                                                            @foreach($houses as $house)
                                                                <option
                                                                    value="{{$house->id}}" {{ $rfidCsv->house_name == $house->house_name ? 'selected' : ''}}>{{ $house->house_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="house_id">Resident Assigned</label>
                                                        <select id="resident_id" name="resident_id"
                                                                class="form-control select2bs4">
                                                            <option value="">Select Resident</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="mailing_address">Mailing Address </label>
                                                        <input type="text" name="mailing_address" id="mailing_address"
                                                               placeholder="Mailing Address"
                                                               value="{{$rfidCsv->mailing_address ?? ""}}"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="driver_license_number">Driver License
                                                            Number </label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Driver License Number"
                                                               value="{{$rfidCsv->driver_license_number ?? ""}}"
                                                               name="driver_license_number" id="driver_license_number"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="state">State Registered</label>
                                                    <input type="text" name="state" id="state" class="form-control"
                                                           placeholder="State Registered"
                                                           value="{{$rfidCsv->state_registered ?? ""}}"/>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="vehicle_type">Vehicle Type </label>
                                                        <input class="form-control" id="vehicle_type"
                                                               name="vehicle_type"
                                                               value="{{ $rfidCsv->vehicle_type  }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="make">Make</label>
                                                        <input type="text" class="form-control" name="make" id="make"
                                                               placeholder="Make" value="{{$rfidCsv->make ?? ""}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="color">Color</label>
                                                        <input type="text" class="form-control" name="color" id="color"
                                                               placeholder="Color" value="{{$rfidCsv->color ?? ""}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="year">Year</label>
                                                        <input type="text" class="form-control" name="year" id="year"
                                                               placeholder="Year" value="{{$rfidCsv->year ?? ""}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="model">Model</label>
                                                        <input type="text" class="form-control" name="model" id="model"
                                                               placeholder="Model" value="{{$rfidCsv->model ?? ""}}"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="license_plate">License Plate</label>
                                                        <input type="text" class="form-control" name="license_plate"
                                                               id="license_plate" placeholder="License Plate"
                                                               value="{{$rfidCsv->license_plate ?? ""}}"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="insurance_company_name">Insurance Company
                                                            Name</label>
                                                        <input type="text" class="form-control"
                                                               name="insurance_company_name" id="insurance_company_name"
                                                               placeholder="Insurance Company Name"
                                                               value="{{$rfidCsv->insurance_company_name ?? ""}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="policy_number">Policy Number</label>
                                                        <input type="text" class="form-control" name="policy_number"
                                                               id="policy_number"
                                                               value="{{$rfidCsv->policy_number ?? ""}}"
                                                               placeholder="Policy Number"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="model">Policy Expiration Date</label>
                                                        <input type="date" class="form-control"
                                                               name="policy_expiration_date" id="policy_expiration_date"
                                                               placeholder="Policy Expiration Date"
                                                               value="{{$rfidCsv->policy_expiration_date ?? ""}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="additional_drivers">Additional Drivers</label>
                                                        <input type="text" class="form-control"
                                                               name="additional_drivers" id="additional_drivers"
                                                               placeholder="Additional Drivers"
                                                               value="{{$rfidCsv->additional_drivers ?? ""}}"/>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Save Changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    let options = '';
                    if (result.status === true) {
                        $.each(result.result, function (key, row) {
                            options += `<option value="${row.id}">${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                }
            });

            /**
             * get user by houses
             */
            $(document).on('change', '#house_id', function () {
                alert(24);
                let houseId = $(this).val();
                showLoader();
                $.ajax({
                    type: 'GET',
                    url: "{{route('admin.rfid.get-all-member-by-house-id')}}",
                    data: {
                        houseId: houseId
                    },
                    success: function (result) {
                        hideLoader();
                        let options = '';
                        if (result.status === true) {
                            options += `<option value="">Select Resident</option>`;
                            $.each(result.result, function (key, row) {
                                options += `<option value="${row.id}" data-street-address="${row.street_address ?? ""}">${row.first_name ?? ""} ${row.last_name ?? ""}</option>`;
                            });
                        } else {
                            options = `<option value="">No Users Found !</option>`;
                        }
                        $('#resident_id').html(options);
                        //Initialize Select2 Elements
                        $('.select2bs4').select2({
                            theme: 'bootstrap4'
                        })
                    }
                });
            });
            /**
             * get user by houses
             */
            $(document).on('change', '#resident_id', function () {
                let street_address = $('option:selected', this).attr('data-street-address');
                $('#mailing_address').val(street_address);
            });
        });

        $(function () {
            $('#house_id').change(function () {
                $(document).on('change', '#house_id', function () {
                    // alert('good');
                    let houseId = $(this).val();
                    showLoader();
                    $.ajax({
                        type: 'GET',
                        url: "{{route('admin.rfid.get-all-member-by-house-id')}}",
                        data: {
                            houseId: houseId
                        },
                        success: function (result) {
                            hideLoader();
                            let options = '';
                            if (result.status === true) {
                                options += `<option value="">Select Resident</option>`;
                                $.each(result.result, function (key, row) {
                                    options += `<option value="${row.id}" data-street-address="${row.street_address ?? ""}">${row.first_name ?? ""} ${row.last_name ?? ""}</option>`;
                                });
                            } else {
                                options = `<option value="">No Users Found !</option>`;
                            }
                            $('#resident_id').html(options);
                            //Initialize Select2 Elements
                            $('.select2bs4').select2({
                                theme: 'bootstrap4'
                            })
                        }
                    });
                });
            })
        })
    </script>
@endpush
