@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Web Relays List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Web Relays List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.settings.web-relays.create')}}" class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Add Web Relay
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="webRelaysTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Mac Address</th>
                                    <th>IP Address</th>
                                    <th>Web Hook</th>
                                    <th>Community</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'mac_address',
            },
            {
                data: 'ip_address',
            },
            {
                data: "web_hook"
            },
            {
                data: "community"
            },
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];
        zuulDataTableSortable('#webRelaysTable', "{{route('admin.settings.web-relays-data')}}", columns, [5]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.residential.communities-data')}}?" + $("#search-filter").serialize();
            zuulDataTableSortable('#communitiesTable', ajax, columns, [4]);
        })
    </script>
@endpush
