@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Add Web Relay</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Web Relay</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.settings.web-relays.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.settings.web-relays.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="mac_address">Mac Address </label>
                                        <input type="text" name="mac_address" id="mac_address"
                                               placeholder="Mac Address" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="ip_address">IP Address </label>
                                        <input type="text" name="ip_address" id="ip_address"
                                               placeholder="IP Address" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="web_hook">Web Hook </label>
                                        <input type="text" name="web_hook" id="web_hook" placeholder="Web Hook"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select
                                            id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach($communities as $community)
                                                <option value="{{$community->id}}">{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
