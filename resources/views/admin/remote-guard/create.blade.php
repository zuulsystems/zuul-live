@extends('admin.layout.master')
@section('content')

    <style>
        body {
            height: 100vh;
            width: 100%;
            margin: 0;
            background: #bdbdc9;
        }


        .select2-container--bootstrap .select2-selection {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
            color: #555555;
            font-size: 14px;
            outline: 0;
        }

        .select2-container--bootstrap .select2-search--dropdown .select2-search__field {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
            color: #555555;
            font-size: 14px;
        }

        .select2-container--bootstrap .select2-search__field {
            outline: 0;
        }

        .select2-container--bootstrap .select2-search__field::-webkit-input-placeholder {
            color: #999;
        }

        .select2-container--bootstrap .select2-search__field:-moz-placeholder {
            color: #999;
        }

        .select2-container--bootstrap .select2-search__field::-moz-placeholder {
            color: #999;
            opacity: 1;
        }

        .select2-container--bootstrap .select2-search__field:-ms-input-placeholder {
            color: #999;
        }

        .select2-container--bootstrap .select2-results__option {
            padding: 6px 12px;
        }

        .select2-container--bootstrap .select2-results__option[role=group] {
            padding: 0;
        }

        .select2-container--bootstrap .select2-results__option[aria-disabled=true] {
            color: #777777;
            cursor: not-allowed;
        }

        .select2-container--bootstrap .select2-results__option[aria-selected=true] {
            background-color: #f5f5f5;
            color: #262626;
        }

        .select2-container--bootstrap .select2-results__option--highlighted[aria-selected] {
            background-color: #337ab7;
            color: #fff;
        }

        .select2-container--bootstrap .select2-results__group {
            color: #777777;
            display: block;
            padding: 6px 12px;
            font-size: 12px;
            line-height: 1.42857143;
            white-space: nowrap;
        }

        .select2-container--bootstrap.select2-container--focus .select2-selection, .select2-container--bootstrap.select2-container--open .select2-selection {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(102, 175, 233, 0.6);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            border-color: #66afe9;
        }

        .select2-container--bootstrap.select2-container--open .select2-selection .select2-selection__arrow b {
            border-color: transparent transparent #999 transparent;
            border-width: 0 4px 4px 4px;
        }

        .select2-container--bootstrap.select2-container--open.select2-container--below .select2-selection {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            border-bottom-color: transparent;
        }

        .select2-container--bootstrap.select2-container--open.select2-container--above .select2-selection {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
            border-top-color: transparent;
        }

        .select2-container--bootstrap .select2-dropdown {
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
            border-color: #66afe9;
            overflow-x: hidden;
            margin-top: -1px;
        }

        .select2-container--bootstrap .select2-selection--single {
            height: 34px;
            line-height: 1.42857143;
            padding: 6px 24px 6px 12px;
        }

        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            /* box-shadow: 0px 0px 3px -2px; */
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid card-bg-heading">
                <div class="row mb-2 ">
                    <div class="col-sm-6">
                        <h1>Add Remote Guard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Remote Guard</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.remote-guard.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.remote-guard.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="identifier">Identifier</label>
                                                <input type="text" name="identifier" id="identifier"
                                                       placeholder="Identifier"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        @if (auth()->user()->hasRole('super_admin'))
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="community_id">Community</label>
                                                    <select
                                                        id="community_id" name="community_id"
                                                        class="form-control select2bs4">
                                                        <option value="">Select Community</option>
                                                        @foreach($communities as $community)
                                                            <option
                                                                value="{{$community->id}}">{{$community->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6" style="display: none" id="phone_number">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" name="phone_number" class="form-control"
                                                       data-inputmask='"mask": "999-999-9999"' data-mask
                                                       placeholder="Phone Number"/>
                                            </div>
                                        </div>

                                        <div class="col-md-6" style="display: none" id="ip_address">
                                            <div class="form-group">
                                                <label for="ip_address">IP Address</label>
                                                <input type="text" name="ip_address" id="ip_address"
                                                       class="form-control"/>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#attach_to').change(function () {
                var $attachTo = $(this).val();
                if ($attachTo == "device") {
                    $('#phone_number').css('display', 'none');
                    $('#ip_address').css('display', 'none');
                    $('#mac_address').css('display', 'block');
                } else if ($attachTo == "user") {
                    $('#mac_address').css('display', 'none');
                    $('#ip_address').css('display', 'none');
                    $('#phone_number').css('display', 'block');
                } else if ($attachTo == "network") {
                    $('#mac_address').css('display', 'none');
                    $('#phone_number').css('display', 'none');
                    $('#ip_address').css('display', 'block');
                }
            });
        });

        $("#mac_address_id").select2({
            tags: true,
            theme: "bootstrap",
        });

    </script>
@endpush
