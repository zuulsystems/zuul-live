@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Offensive Words</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Offensive Words</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name" placeholder="Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.settings.offensive-words.create')}}" class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Create Words
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="AnnouncementsTable" class="table  table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Word</th>
                                    <th>Created At</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'name',
            },
            {
                data: 'created_at',
            },
            {
                data: function (data) {
                    return data.edit_link;
                }
            },
        ];
        zuulDataTableSortable('#AnnouncementsTable', "{{route('admin.settings.offensive-words-data')}}", columns, [3]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.settings.offensive-words-data')}}?" + $("#search-filter").serialize();
            zuulDataTableSortable('#AnnouncementsTable', ajax, columns, [3]);
        })
    </script>
@endpush
