@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Pass Requests Log List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Pass Requests Log List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="sender_name">Sender</label>
                                                <input type="text" name="sender_name" id="sender_name"
                                                       placeholder="Sender" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="requested_user_name">Requested To</label>
                                                <input type="text" name="requested_user_name" id="requested_user_name"
                                                       placeholder="Requested To" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="requested_user_community_name">Community</label>
                                                <input type="text" name="requested_user_community_name"
                                                       id="requested_user_community_name" placeholder="Community"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <input type="text" name="description" id="description"
                                                       placeholder="Description" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="convert_requested_datetime">Requested At</label>
                                                <input type="date" name="convert_requested_datetime"
                                                       id="convert_requested_datetime" placeholder="Requested At"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="guestLogTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Sender</th>
                                    <th>Requested To</th>
                                    <th>Description</th>
                                    <th>Community</th>
                                    <th>Requested At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'sender_name',
                name: 'sendUser.first_name',
            },
            {
                data: 'requested_user_name',
                name: 'requestedUser.first_name',
            },
            {
                data: 'description',
            },
            {
                data: 'requested_user_community_name',
                name: 'requestedUser.community.name',
            },
            {
                data: 'convert_requested_datetime',
                name: 'created_at',
            },
            {
                data: 'status',
                render: function (status) {
                    let html = '';
                    if (status === 'approved') {
                        html = `<span class="badge badge-success">Approved</span>`;
                    } else if (status === 'pending') {
                        html = `<span class="badge badge-warning">Pending</span>`;
                    } else if (status === 'rejected') {
                        html = `<span class="badge badge-danger">Rejected</span>`;
                    }
                    return html;
                }
            }
        ];
        zuulDataTableSortable('#guestLogTable', "{{route('admin.pass-requests-data')}}", columns);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.pass-requests-data')}}?" + $("#search-filter").serialize();
            zuulDataTableSortable('#guestLogTable', ajax, columns);
        })
    </script>
@endpush
