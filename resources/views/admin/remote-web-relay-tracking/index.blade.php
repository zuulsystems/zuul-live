@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Remote Web Relay Tracking List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Remote Web Relay Tracking List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="webRelayTrackingTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Scan Log Id</th>
                                    <th>Mac Address</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @php
        $showClass = '';
      $housesImportModel = '';
      if ($errors->any())
      {
      $showClass = 'show';
      $housesImportModel = 'display: block';
      }
    @endphp
@endsection
@php
    if(session()->has('housesImportedSuccessfully'))
    {
@endphp
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-success" aria-live="polite" style="display: block; opacity: 1;">
        <div class="toast-message">Houses imported successfully.</div>
    </div>
</div>
<script>
    setTimeout(function () {
        document.getElementsByClassName('toast-success')[0].style.display = "none";
    }, 1000);
</script>
@php
    }
@endphp
@push('scripts')
    <script src="{{asset('assets/admin/dist/js/main17-6-2022.js')}}"></script>
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'scan_log_id',
            },
            {
                data: 'web_relay',
            },
            {
                data: function (data) {
                    return data.links;
                }
            }
        ];

        zuulDataTableSortable('#webRelayTrackingTable',
            "{{ route('admin.settings.remote-web-relay-tracking-data') }}", columns, [3]);

        $('.search-btn').click(function () {
            var ajax = "{{ route('admin.residential.houses-data') }}?communityId={{ $communityId ?? null }}&houseId={{ $houseId ?? null }}&" +
                $("#search-filter").serialize();
            zuulDataTableSortable('#housesTable', ajax, columns, [7]);
        })

        $('#closeHousesImportModalBtn').click(function () {
            $("#house-import-modal").css("display", "none");
            $('.a').css('display', 'none');
        });

        $('#notificationUl').css('margin-top', '57px');


    </script>
@endpush
