@extends('admin.layout.master')
@push('css')
    <!-- select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
@endpush

@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Traffic Tickets</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Traffic Tickets</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body collapse">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="ticket_id">Ticket ID</label>
                                                <input type="text" name="ticket_id" id="ticket_id"
                                                       placeholder="Ticket ID" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="license_plate">License Plate</label>
                                                <input type="text" name="license_plate" id="license_plate"
                                                       @if(isset($license)) disabled @endif placeholder="License Plate"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community">Community</label>
                                                <input type="text" name="community" @if(isset($camera_id)) disabled
                                                       @endif id="community" placeholder="Community"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="from">From</label>
                                                <input type="date" name="from" id="from" placeholder="from"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="to">To</label>
                                                <input type="date" name="to" id="to" placeholder="to"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="excess_speed">Excess Speed</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="excess_speed_start"
                                                           id="excess_speed_start" placeholder="1" min="1" max="5"
                                                           class="form-control col-5">
                                                    <div
                                                        class="col-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="excess_speed_end" id="excess_speed_end"
                                                           placeholder="5" min="1" max="5" class="form-control col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="violation_count">Violation Count</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="violation_count_start"
                                                           id="violation_count_start" placeholder="1" min="1" max="5"
                                                           class="form-control col-5 ">
                                                    <div
                                                        class="col-md-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="violation_count_end"
                                                           id="violation_count_end" placeholder="5" min="1" max="5"
                                                           class="form-control col-5 ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-check mt-4">
                                                <input class="form-check-input" type="checkbox" value="true"
                                                       id="displayDismissed" name="displayDismissed">
                                                <label class="form-check-label" for="displayDismissed">
                                                    Display Dismissed
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Ticket ID</th>
                                    <th>License Plate</th>
                                    <th>Community</th>
                                    <th>Speed Limit</th>
                                    <th>MPH</th>
                                    <th>Excess Speed</th>
                                    <th>Date/Time</th>
                                    <th>Image Count</th>
                                    <th>Violation Count</th>
                                    <th>Ticket Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.tickets.includes.modals')

    <!-- Ticket Deatils -->
    <div class="modal fade" id="TicketDetailsUnmatched" tabindex="-1" role="dialog" aria-labelledby="TicketDetailsTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TicketDetailsTitle">Ticket Detail <span class="camera_name"></span>
                        <span id="ticketId"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="ticketDetailsTable">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Ticket</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Location:</th>
                                    <td id="locationUnmatched"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Limit</th>
                                    <td id="speedLimitUnmatched">Jacob</td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Voilation</th>
                                    <td id="speedViolationUnmatched"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Date</th>
                                    <td id="dateUnmatched"></td>
                                </tr>
                                <tr>
                                    <th scope="row">License Plate</th>
                                    <td class="licenseUnmatched"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Camera Name</th>
                                    <td class="camera_nameUnmatched"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit License Plate -->
    <div class="modal fade" id="LicPlateModal" tabindex="-1" role="dialog" aria-labelledby="LicPlateTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LicPlateTitle">Edit License Plate</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="LicPlateBody">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="licPlateName">License Plate</label>
                                <input type="text" class="form-control" id="licPlateName" value="" name="licPlateName"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" id="licSave">Save</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Attach to Resident -->
    <div class="modal fade" id="ResVenModal" tabindex="-1" role="dialog" aria-labelledby="ResVenTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ResVenTitle">Select <span class="ResVenName">Resident / Vendor</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="ResVenBody">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="ResVenName">Select <span class="ResVenName">Resident / Vendor</span></label>
                                <br/>
                                <select class="form-control" id="resvenoptions" style="width: 100%"
                                        name="resvenoptions">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" id="resvensave">Save</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Attach to Vendor -->

    <div class="modal fade" id="VendorChangeModal" tabindex="-1" role="dialog" aria-labelledby="VendorChangeTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="VendorChangeTitle">Select <span class="VendorChangeName">Resident / Vendor</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="VendorChangeBody">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="VendorChangeName">Select <span
                                        class="VendorChangeName">Resident / Vendor</span></label>
                                <br/>
                                <select class="form-control" id="vendorchangeoptions" style="width: 100%"
                                        name="vendorchangeoptions">

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="VendorLicenseChange">Select License Plate</label>
                                <br/>
                                <select class="form-control" id="vendorlicence" style="width: 100%"
                                        name="vendorlicence">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" id="vendorchangesave">Save</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="TicketDetails" tabindex="-1" role="dialog" aria-labelledby="TicketDetailsTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TicketDetailsTitle">Ticket Detail <span id="ticketId"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="ticketDetailsTable">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Vehicle</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Make</th>
                                    <td id="make"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Model</th>
                                    <td id="model">Jacob</td>
                                </tr>
                                <tr>
                                    <th scope="row">Color</th>
                                    <td id="color"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Year</th>
                                    <td id="year"></td>
                                </tr>
                                <tr>
                                    <th scope="row">License Plate</th>
                                    <td class="license"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Ticket</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Location:</th>
                                    <td id="location"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Limit</th>
                                    <td id="speedLimit">Jacob</td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Voilation</th>
                                    <td id="speedViolation"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Date</th>
                                    <td id="date"></td>
                                </tr>
                                <tr>
                                    <th scope="row">License Plate</th>
                                    <td class="license"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Resident</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Full Name</th>
                                    <td id="fullName"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Email</th>
                                    <td id="email"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Phone Number</th>
                                    <td id="phone"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Community</th>
                                    <td id="community_name"></td>
                                </tr>
                                <tr>
                                    <th scope="row">House</th>
                                    <td id="house"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Address</th>
                                    <td id="address"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6" id="resTickDetOnly">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Pass Sent By User</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Full Name</th>
                                    <td id="fullNamePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Email</th>
                                    <td id="emailPass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Phone Number</th>
                                    <td id="phonePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Community</th>
                                    <td id="community_namePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">House</th>
                                    <td id="housePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Address</th>
                                    <td id="addressPass"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Send Email -->
    <div class="modal fade" id="sendImageModal" tabindex="-1" role="dialog" aria-labelledby="sendImageTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Email Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="sendImageBody">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="emailCommunity">Community</label>
                                <input name="emailCommunity" class="form-control" disabled id="communityNameForEmail"
                                       value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="template">Choose Template</label>
                                <select name="template" class="form-control" id="template">
                                    <option>Test 1</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control text-editor" rows="10" id="message"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                    <button type="button" id="sendEmail" class="btn btn-primary float-right">Send Email</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            // DataTable Columns
            let columns = [
                {data: 'id'},
                {data: 'ticket_id'},
                {data: 'license_plate'},
                {data: 'community'},
                {data: 'current_speed_limit'},
                {data: 'violating_speed'},
                {data: 'excess_speed', name: "excess_speed_calculated"},
                {data: 'formatted_time', name: "time"},
                {data: 'ticket_images_count'},
                {
                    data: 'violation_count',
                    name: 'traffic_tickets_count'
                },
                {data: 'ticket_type'},
                {data: 'action'},
            ];

            // Route for Ajax Calls
            // Route for Ajax Calls
            @php
                $title = "unmatched";
                $datatable_route = route('admin.traffic-tickets.unmatched-tickets-data');
                $route = '?';
                if(isset($camera_id))
                {
                    $route .= "camera_id=".$camera_id;
                }
                if(isset($license))
                {
                    $route .= "license=".$license;
                }

                if($route != '?')
                    $datatable_route .= $route
            @endphp



            @include('admin.tickets.includes.scripts')


            var ticketIdToEmailTemplate = function (id, template) {
                $.get(`{{route('admin.traffic-tickets.unmatched-ticket-details')}}?ticket_id=${ticketId}`)
                    .done(function (data) {
                        if (data.status) {
                            // Fetch the Result
                            data = data.result;
                            // Date Time
                            datetime = new Date().toLocaleString();

                            // MAP URL Generator
                            geocode = data.location.geocode.toString();
                            map_url = `https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=300x300&maptype=roadmap&markers=color:red%7Clabel:Z%7C${geocode.slice(1, -1)}&key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA`;

                            console.log(map_url);

                            // Add the Variables
                            template = template.replace('{ticket_id}', data.ticket_id);
                            template = template.replace('{plate_text}', data.license_plate);
                            template = template.replace('{timestamp}', datetime);
                            template = template.replace('{ticket_time}', data.time);
                            template = template.replace('{current_speed_limit}', data.current_speed_limit);
                            template = template.replace('{violating_speed}', data.violating_speed);
                            template = template.replace('{location_address}', data.location.name);
                            template = template.replace('{location_name}', data.location.name);
                            image = "";
                            if (data.ticket_images.length > 0) {
                                image = `<img src="{{env('APP_URL')}}/images/tickets/${data.ticket_images[0].image}" style="width:400px" />`;
                            }
                            template = template.replace("{image}", image);
                            template = template.replace('{notice_number}', parseInt(data.email_log_count) + 1);
                            template = template.replace('{geocode}', `<img src="${map_url}" />`);

                            $('.text-editor').summernote('code', template);
                        }
                    });
            }

            console.log('test');

            $('body').on('click', '#ticketDetailBtnUnmatched', function (e) {

                //Clear the Table
                $('#ticketDetailsTable table tbody td').html('');
                $('#ticketId').html('');


                let ticketId = $(e.target).data('id');
                $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-ticket-details')}}?ticket_id=${ticketId}`).done(function (data) {

                    if (data.status) {
                        data = data.result;

                        // Ticket ID for Title
                        $('#ticketId').html(data.ticket_id);


                        //Ticket Deatils
                        $('#speedLimitUnmatched').html(data.current_speed_limit);
                        $('#speedViolationUnmatched').html(data.violating_speed);
                        $('#dateUnmatched').html(data.time);
                        $('.licenseUnmatched').html(data.license_plate);
                        $('.camera_nameUnmatched').html(data.camera_name);

                        data.location != null ? $('#locationUnmatched').html(data.location.name) : '';

                        // Show the model
                        $('#TicketDetailsUnmatched').modal('show');
                    }

                });

            });

            $('body').on('click', '#editLicBtn', function (e) {
                $('#LicPlateModal').modal('show');
                $('#licPlateName').data('id', $(e.target).data('id'));
                $('#licPlateName').val($(e.target).data('lic'));
            });

            $('body').on('click', '#attachVendorBtn', function (e) {
                VendorChange('vendor', $(e.target).data('id'));
            });

            $('body').on('click', '#attachResidentBtn', function (e) {
                ResidentChange('resident', $(e.target).data('id'));
            });

            $('body').on('click', '#licSave', function (e) {
                ticketId = $('#licPlateName').data('id');
                lic = $('#licPlateName').val();
                showLoader();
                $.post("{{route('admin.traffic-tickets.unmatched-license-change')}}", {
                    ticketId,
                    lic
                }).done(function (data) {
                    if (data.bool) {
                        TLDataTable('#datatable', "{{$datatable_route}}", columns);
                        notifySuccess("License Plate Has been updated");
                        $('#LicPlateModal').modal('hide');
                    }
                });
                hideLoader();
            });

            $('body').on('click', '#resvensave', function (e) {
                ticketId = $('#resvenoptions').data('id');
                ticket_type = 'resident';
                user_id = $('#resvenoptions').val();
                $('#ResVenModal').modal('hide');
                showLoader();
                $.post("{{route('admin.traffic-tickets.unmatched-attach-user')}}", {
                    ticketId,
                    ticket_type,
                    user_id
                }).done(function (data) {
                    if (data.bool) {
                        TLDataTable('#datatable', "{{$datatable_route}}", columns);
                        notifySuccess("Ticket has been updated");
                    } else {
                        $('#ResVenModal').modal('show');
                    }
                });
                hideLoader();

            });

            $('body').on('click', '#vendorchangesave', function (e) {
                ticketId = $('#vendorchangeoptions').data('id');
                ticket_type = 'vendor';
                user_id = $('#vendorchangeoptions').val();
                license = $('#vendorlicence').val();
                if (license == "")
                    notifyError('Please Select License');
                else {
                    console.log({ticketId, ticket_type, user_id, license});
                    $('#VendorChangeModal').modal('hide');
                    showLoader();
                    $.post("{{route('admin.traffic-tickets.unmatched-attach-user')}}", {
                        ticketId,
                        ticket_type,
                        user_id,
                        license
                    }).done(function (data) {
                        if (data.bool) {
                            TLDataTable('#datatable', "{{$datatable_route}}", columns);
                            notifySuccess("Ticket has been updated");
                        } else {
                            $('#VendorChangeModal').modal('show');
                        }
                    });
                    hideLoader();
                }


            });

        });

        function ResidentChange(type = 'resident', ticketid) {
            $('.ResVenName').html(type);
            if ($('#resvenoptions').data('select2')) {
                $('#resvenoptions').select2("destroy");
            }
            $('#resvenoptions').empty();
            $('#resvenoptions').data('id', ticketid);
            $('#resvenoptions').data('user_type', type);

            $.get(`{{route('admin.traffic-tickets.unmatched-attach-user')}}?user_type=${type}`).done(function (data) {
                $('#resvenoptions').append('<option></option>');
                for (person of data.result) {
                    $('#resvenoptions').append(`<option value=${person.id}>${person.full_name}</option>`);
                }
                $("#resvenoptions").select2({
                    placeholder: "Select Resident",
                    dropdownParent: $('#ResVenModal')
                });
                $('#ResVenModal').modal('show');
            });
        }

        function VendorChange(type, ticketid) {
            $('.VendorChangeName').html(type);
            if ($('#vendorchangeoptions').data('select2')) {
                $('#vendorchangeoptions').select2("destroy");
            }

            if ($('#vendorlicence').data('select2')) {
                $('#vendorlicence').select2("destroy");
            }

            $('#vendorchangeoptions').empty();
            $('#vendorlicence').empty();
            $('#vendorlicence').attr('disabled', 'disabled');


            $('#vendorchangeoptions').data('id', ticketid);
            $('#vendorchangeoptions').data('user_type', type);

            $.get(`{{route('admin.traffic-tickets.unmatched-attach-user')}}?user_type=${type}`).done(function (data) {
                $('#vendorchangeoptions').append('<option></option>');
                for (person of data.result) {
                    $('#vendorchangeoptions').append(`<option value=${person.id}>${person.vendor_name}</option>`);
                }

                $("#vendorchangeoptions").select2({
                    id: '-1', // the value of the option
                    placeholder: "Select Vendor",
                    dropdownParent: $('#VendorChangeModal')
                });
                $('#VendorChangeModal').modal('show');
            });
        }

        //On Vendor Change
        $('#vendorchangeoptions').on('change', function (e) {
            $('#vendorlicence').empty();
            $('#vendorlicence').attr('disabled', 'disabled');

            vendor_id = $('#vendorchangeoptions').val();
            $.get(`{{route('admin.traffic-tickets.unmatched-attach-license')}}?vendor_id=${vendor_id}`).done(function (data) {
                if ((data.bool && data.result != null) && !Array.isArray(data.result)) {
                    for (const [id, license] of Object.entries(data.result)) {
                        $('#vendorlicence').append(`<option value=${license}>${license}</option>`);
                    }
                    $('#vendorlicence').removeAttr('disabled');
                } else {
                    $('#vendorlicence').append(`<option>No Vehicles</option>`);
                }
                $("#vendorchangeoptions").select2({
                    dropdownParent: $('#VendorChangeModal')
                });
                $('#VendorChangeModal').modal('show');
            });
        });


        //Ticket Detail Button
        $('body').on('click', '#ticketDetailBtn', function (e) {

            //Clear the Table
            $('#ticketDetailsTable table tbody td').html('');
            $('#ticketId').html('');


            let ticketId = $(e.target).data('id');
            $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-ticket-details')}}?ticket_id=${ticketId}`).done(function (data) {

                if (data.status) {
                    pass_user = data.pass;
                    data = data.result;

                    // Ticket ID for Title
                    $('#ticketId').html(data.ticket_id);


                    //Ticket Deatils
                    $('#speedLimit').html(data.current_speed_limit);
                    $('#speedViolation').html(data.violating_speed);
                    $('#date').html(data.time);
                    $('.license').html(data.license_plate);
                    data.location != null ? $('#location').html(data.location.name) : '';


                    if (data.vehicle != null) {
                        // Vehicle Details
                        $('#make').html(data.vehicle.make);
                        $('#model').html(data.vehicle.model);
                        $('#color').html(data.vehicle.color);
                        $('#year').html(data.vehicle.year);
                    }

                    // Add User Details
                    if (data.user != null) {
                        $('#fullName').html(data.user.full_name);
                        $('#email').html(data.user.email);
                        $('#phone').html(data.user.phone_number);

                        // Add House Details
                        if (data.user.house != null) {
                            house = data.user.house
                            $('#house').html(house.house_name);
                            $('#address').html(house.house_detail);
                        }


                    }


                    if (data.ticket_type == "resident") {
                        $("#resTickDetOnly").show();
                        // Add User Details
                        if (pass_user != null && (pass_user.pass != null && pass_user.pass.created_by != null)) {
                            $('#fullNamePass').html(pass_user.pass.created_by.full_name);
                            $('#emailPass').html(pass_user.pass.created_by.email);
                            $('#phonePass').html(pass_user.pass.created_by.phone_number);
                            $('#addressPass').html(pass_user.pass.created_by.formattedAddress);

                            // Add House Details
                            if (pass_user.pass.created_by.house != null) {
                                house = pass_user.pass.created_by.house
                                $('#housePass').html(house.house_name);
                            }

                            pass_user.pass.created_by.community != null ? $('#community_namePass').html(pass_user.pass.created_by.community.name) : '';

                        }
                    } else {
                        $("#resTickDetOnly").hide();
                    }

                    // Add Community
                    (data.location != null && data.location.community != null) ? $('#community_name').html(data.location.community.name) : '';

                    // Show the model
                    $('#TicketDetails').modal('show');
                }

            });

        });

        // On SendMail Click
        $('body').on('click', '.sendMail', function (e) {

            // Remove the Old Options and Disabled Attribute
            $('#template').removeAttr('disabled');
            $('#template').removeAttr('ticket');
            $('#message').attr('disabled');
            $('#message').empty();
            $('#template').empty();

            // Variables if No Data is Found / Error Occured
            let disabled = false;
            let reason = null;

            // For First Email Template
            first = true;

            //Ticket ID - To Fetch Ticket Email
            ticketId = $(e.target).data('id');

            // Get Community ID
            let community_id = $(e.target).data('communityid');

            // Check if Community ID Exists
            if (community_id != null || community_id != '') {

                // Get the Traffic Logix Email teampltes for Community
                $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-email-templates')}}?community_id=${community_id}`)
                    .done(function (data) {
                        if (data.status) {
                            if (data.result != null && data.result.email_templates.length) {
                                $('#communityNameForEmail').val(data.result.name);
                                data = data.result.email_templates;
                                $('#template').attr('ticket', ticketId);
                                for (options of data) {
                                    //TOdo: Add Email Slug as Value
                                    $('#template').append(`<option value="${options.assign_to}" data-template='${options.template}'>${options.name}</option`);
                                    if (first) {
                                        ticketIdToEmailTemplate(ticketId, options.template);
                                        first = false;
                                    }
                                }
                                $('#sendImageModal').modal('show');
                            } else {
                                disabled = true;
                                reason = "No Email Templates Found";
                            }
                        } else {
                            disabled = true;
                            reason = "Cannot Fetch Email Templates";
                        }

                        // If Disabled
                        if (disabled) {
                            if (reason == null)
                                $('#communityNameForEmail').val(reason);
                            else
                                $('#template').html(`<option>${reason}</option`);

                            $('#template').attr('disabled', 'disabled');
                            $('#message').attr('disabled', 'disabled');
                            $('#sendImageModal').modal('show');
                        }
                    });


            } else {
                disabled = true;
            }

            // If Disabled
            if (disabled) {
                if (reason == null)
                    $('#communityNameForEmail').val(reason);
                else
                    $('#template').html(`<option>${reason}</option`);

                $('#template').attr('disabled', 'disabled');
                $('#message').attr('disabled', 'disabled');
                $('#sendImageModal').modal('show');
            }


        });

        // On Template change, change the inner html
        $('body').on('change', '#template', function (e) {
            template = $(e.target).find(':selected').data('template');
            ticketId = $(e.target).attr('ticket');
            ticketIdToEmailTemplate(ticketId, template);
        });

        //  On Send Email Click
        $('body').on('click', '#sendEmail', function (e) {
            e.preventDefault();
            // Template
            template = $(".text-editor").summernote('code');

            // Email Assign To
            assign_to = $('#template option:selected').attr('value');

            //Ticket ID
            ticketId = $('#template').attr('ticket');

            // Title
            title = $('#template option:selected').html();


            // Send VIA POST Request
            $.post(`{{route('admin.traffic-tickets.'.strtolower($title).'-email-send')}}`, {
                ticketId,
                title,
                template,
                assign_to
            }, function (data) {
                $('#sendImageModal').modal('hide');
                notifySuccess("Email Sent");
            });

        });

        var ticketIdToEmailTemplate = (id, template) => {
            $.get(`{{route('admin.traffic-tickets.unmatched-ticket-details')}}?ticket_id=${ticketId}`)
                .done(function (data) {
                    if (data.status) {
                        // Fetch the Result
                        data = data.result;
                        // Date Time
                        datetime = new Date().toLocaleString();

                        // MAP URL Generator
                        geocode = data.location.geocode.toString();
                        map_url = `https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=300x300&maptype=roadmap&markers=color:red%7Clabel:Z%7C${geocode.slice(1, -1)}&key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA`;

                        console.log(map_url);

                        // Add the Variables
                        template = template.replace('{ticket_id}', data.ticket_id);
                        template = template.replace('{plate_text}', data.license_plate);
                        template = template.replace('{timestamp}', datetime);
                        template = template.replace('{ticket_time}', data.time);
                        template = template.replace('{current_speed_limit}', data.current_speed_limit);
                        template = template.replace('{violating_speed}', data.violating_speed);
                        template = template.replace('{location_address}', data.location.name);
                        template = template.replace('{location_name}', data.location.name);
                        image = "";
                        if (data.ticket_images.length > 0) {
                            image = `<img src="{{env('APP_URL')}}/images/tickets/${data.ticket_images[0].image}" style="width:400px" />`;
                        }
                        template = template.replace("{image}", image);
                        template = template.replace('{notice_number}', parseInt(data.email_log_count) + 1);
                        template = template.replace('{geocode}', `<img src="${map_url}" />`);

                        $('.text-editor').summernote('code', template);
                    }
                });
        }


    </script>
@endpush
