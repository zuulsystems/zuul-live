@extends('admin.layout.master')
@push('css')
    <!-- select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
@endpush

@section('content')
    @include('admin.layout.datatable-css')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Unmatched Tickets</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Unmatched tickets</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <div class="bulk-dismissed-ticket-div float-left" style="display:none;">
                                        <button class="btn btn-sm btn-primary" id="bulkDismissedTicketBtn">Bulk Dismiss
                                            Ticket
                                        </button>
                                    </div>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">

                                            <div class="form-group">
                                                <label for="ticket_id">Ticket ID</label>
                                                <input type="text" name="ticket_id" id="ticket_id"
                                                       placeholder="Ticket ID" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="license_plate">License Plate</label>
                                                <input type="text" name="license_plate" id="license_plate"
                                                       placeholder="License Plate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community">Community</label>
                                                <input type="text" name="community" id="community"
                                                       placeholder="Community" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="from">From</label>
                                                <input type="date" name="from" id="from" placeholder="from"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="to">To</label>
                                                <input type="date" name="to" id="to" placeholder="to"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="excess_speed">Excess Speed</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="excess_speed_start"
                                                           id="excess_speed_start" placeholder="1" min="1" max="5"
                                                           class="form-control col-5">
                                                    <div
                                                        class="col-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="excess_speed_end" id="excess_speed_end"
                                                           placeholder="5" min="1" max="5" class="form-control col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="violation_count">Violation Count</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="violation_count_start"
                                                           id="violation_count_start" placeholder="1" min="1" max="5"
                                                           class="form-control col-5 ">
                                                    <div
                                                        class="col-md-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="violation_count_end"
                                                           id="violation_count_end" placeholder="5" min="1" max="5"
                                                           class="form-control col-5 ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-check mt-4">
                                                <input class="form-check-input" type="checkbox" value="true"
                                                       id="displayDismissed" name="displayDismissed">
                                                <label class="form-check-label" for="displayDismissed">
                                                    Display Dismissed
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Ticket ID</th>
                                    <th>License Plate</th>
                                    <th>Community</th>
                                    <th>Speed Limit</th>
                                    <th>MPH</th>
                                    <th>Excess Speed</th>
                                    <th>Date/Time</th>
                                    <th>Image Count</th>
                                    <th>Violation Count</th>
                                    <th>Ticket Type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.tickets.includes.modals')

    <!-- Ticket Deatils -->
    <div class="modal fade" id="TicketDetailsUnmatched" tabindex="-1" role="dialog" aria-labelledby="TicketDetailsTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TicketDetailsTitle">Ticket Detail <span class="camera_name"></span>
                        <span id="ticketId"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="ticketDetailsTable">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Ticket</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Location:</th>
                                    <td id="location"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Limit</th>
                                    <td id="speedLimit">Jacob</td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Voilation</th>
                                    <td id="speedViolation"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Date</th>
                                    <td id="date"></td>
                                </tr>
                                <tr>
                                    <th scope="row">License Plate</th>
                                    <td class="license"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Camera Name</th>
                                    <td class="camera_name"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit License Plate -->
    <div class="modal fade" id="LicPlateModal" tabindex="-1" role="dialog" aria-labelledby="LicPlateTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LicPlateTitle">Edit License Plate</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="LicPlateBody">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="licPlateName">License Plate</label>
                                <input type="text" class="form-control" id="licPlateName" value="" name="licPlateName"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" id="licSave">Save</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Attach to Resident -->
    <div class="modal fade" id="ResVenModal" tabindex="-1" role="dialog" aria-labelledby="ResVenTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ResVenTitle">Select <span class="ResVenName">Resident / Vendor</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="ResVenBody">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="ResVenName">Select <span class="ResVenName">Resident / Vendor</span></label>
                                <br/>
                                <select class="form-control" id="resvenoptions" style="width: 100%"
                                        name="resvenoptions">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" id="resvensave">Save</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Attach to Vendor -->

    <div class="modal fade" id="VendorChangeModal" tabindex="-1" role="dialog" aria-labelledby="VendorChangeTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="VendorChangeTitle">Select <span class="VendorChangeName">Resident / Vendor</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="VendorChangeBody">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="VendorChangeName">Select <span
                                        class="VendorChangeName">Resident / Vendor</span></label>
                                <br/>
                                <select class="form-control" id="vendorchangeoptions" style="width: 100%"
                                        name="vendorchangeoptions">

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="VendorLicenseChange">Select License Plate</label>
                                <br/>
                                <select class="form-control" id="vendorlicence" style="width: 100%"
                                        name="vendorlicence">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-right" id="vendorchangesave">Save</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {

            // DataTable Columns
            let columns = [
                {data: 'mass_dismissed_ticket'},
                {data: 'ticket_id', name: "id"},
                {data: 'license_plate'},
                {data: 'community', name: "location.community.name"},
                {data: 'current_speed_limit'},
                {data: 'violating_speed'},
                {data: 'excess_speed', name: "excess_speed_calculated"},
                {data: 'formatted_time', name: "time"},
                {data: 'ticket_images_count'},
                {data: 'total_violation_count_sort', name: 'total_violation_count'},
                {data: 'ticket_type'},
                {data: 'action'},
            ];

            // Route for Ajax Calls
            @php
                $title = "unmatched";
                $datatable_route = route('admin.traffic-tickets.unmatched-tickets-data');
                if(isset($camera_id))
                    $datatable_route .= "?camera_id=".$camera_id;
            @endphp

            @include('admin.tickets.includes.scripts_unmtached_ticket')

            //New Line
            // On checkbox Change for bulk dismissed tickets
            $(document).on('change', '.checkbox-mass-dismissed-ticket', function () {
                if ($(this).is(":checked")) {
                    $('.bulk-dismissed-ticket-div').fadeIn();
                } else if ($(this).is(":not(:checked)")) {
                    if ($(".checkbox-mass-dismissed-ticket:checked").length < 1)
                        $('.bulk-dismissed-ticket-div').fadeOut();
                }
            });

            //Bulk dismissed tickets Click
            $('#bulkDismissedTicketBtn').on('click', function (e) {
                e.preventDefault();
                let ids = [];
                $('.checkbox-mass-dismissed-ticket:checked').each(function () {
                    var id = $(this).attr('data-id');
                    ids.push(id);
                });
                console.log(ids);
                // return;
                if (confirm("Do you want to dismiss the tickets?")) {
                    showLoader();
                    // URL With Placeholder
                    let url = "{{route('admin.traffic-tickets.bulk-dismissed-ticket')}}";
                    // Replace Placeholder with TicketID
                    url = url.replace(':id:', ticketId);
                    // Delete the ticket
                    $.post(url, {'ids': ids}).done(function (data) {
                        if (data.status) {
                            // Re-Render the table
                            $('.bulk-dismissed-ticket-div').fadeOut();
                            TLUnMatchedDataTable('#datatable', "{{$datatable_route}}", columns);
                        }
                    });
                }

            })
            //End Line

            $('body').on('click', '#ticketDetailBtnUnmatched', function (e) {

                //Clear the Table
                $('#ticketDetailsTable table tbody td').html('');
                $('#ticketId').html('');


                let ticketId = $(e.target).data('id');
                $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-ticket-details')}}?ticket_id=${ticketId}`).done(function (data) {

                    if (data.status) {
                        data = data.result;

                        // Ticket ID for Title
                        $('#ticketId').html(data.ticket_id);


                        //Ticket Deatils
                        $('#speedLimit').html(data.current_speed_limit);
                        $('#speedViolation').html(data.violating_speed);
                        $('#date').html(data.time);
                        $('.license').html(data.license_plate);
                        $('.camera_name').html(data.camera_name);

                        data.location != null ? $('#location').html(data.location.name) : '';


                        // Show the model
                        $('#TicketDetailsUnmatched').modal('show');
                    }

                });

            });

            $('body').on('click', '#editLicBtn', function (e) {
                $('#LicPlateModal').modal('show');
                $('#licPlateName').data('id', $(e.target).data('id'));
                $('#licPlateName').val($(e.target).data('lic'));
            });

            $('body').on('click', '#attachVendorBtn', function (e) {
                VendorChange('vendor', $(e.target).data('id'));
            });

            $('body').on('click', '#attachResidentBtn', function (e) {
                ResidentChange('resident', $(e.target).data('id'));
            });

            $('body').on('click', '#licSave', function (e) {
                ticketId = $('#licPlateName').data('id');
                lic = $('#licPlateName').val();
                showLoader();
                $.post("{{route('admin.traffic-tickets.unmatched-license-change')}}", {
                    ticketId,
                    lic
                }).done(function (data) {
                    if (data.bool) {
                        TLDataTable('#datatable', "{{$datatable_route}}", columns);
                        notifySuccess("License Plate Has been updated");
                        $('#LicPlateModal').modal('hide');
                    }
                });
                hideLoader();
            });

            $('body').on('click', '#resvensave', function (e) {
                ticketId = $('#resvenoptions').data('id');
                ticket_type = 'resident';
                user_id = $('#resvenoptions').val();
                $('#ResVenModal').modal('hide');
                showLoader();
                $.post("{{route('admin.traffic-tickets.unmatched-attach-user')}}", {
                    ticketId,
                    ticket_type,
                    user_id
                }).done(function (data) {
                    if (data.bool) {
                        TLDataTable('#datatable', "{{$datatable_route}}", columns);
                        notifySuccess("Ticket has been updated");
                    } else {
                        $('#ResVenModal').modal('show');
                    }
                });
                hideLoader();

            });

            $('body').on('click', '#vendorchangesave', function (e) {
                ticketId = $('#vendorchangeoptions').data('id');
                ticket_type = 'vendor';
                user_id = $('#vendorchangeoptions').val();
                license = $('#vendorlicence').val();
                if (license == "")
                    notifyError('Please Select License');
                else {
                    console.log({ticketId, ticket_type, user_id, license});
                    $('#VendorChangeModal').modal('hide');
                    showLoader();
                    $.post("{{route('admin.traffic-tickets.unmatched-attach-user')}}", {
                        ticketId,
                        ticket_type,
                        user_id,
                        license
                    }).done(function (data) {
                        if (data.bool) {
                            TLDataTable('#datatable', "{{$datatable_route}}", columns);
                            notifySuccess("Ticket has been updated");
                        } else {
                            $('#VendorChangeModal').modal('show');
                        }
                    });
                    hideLoader();
                }


            });

            // On SendMail Click
            $('body').on('click', '.sendMail', function (e) {

                // Remove the Old Options and Disabled Attribute
                $('#template').removeAttr('disabled');
                $('#template').removeAttr('ticket');
                $('#message').attr('disabled');
                $('#message').empty();
                $('#template').empty();

                // Variables if No Data is Found / Error Occured
                let disabled = false;
                let reason = null;

                // For First Email Template
                first = true;

                //Ticket ID - To Fetch Ticket Email
                ticketId = $(e.target).data('id');

                // Get Community ID
                let community_id = $(e.target).data('communityid');

                // Check if Community ID Exists
                if (community_id != null || community_id != '') {

                    // Get the Traffic Logix Email teampltes for Community
                    $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-email-templates')}}?community_id=${community_id}`)
                        .done(function (data) {
                            if (data.status) {
                                if (data.result != null && data.result.email_templates.length) {
                                    $('#communityNameForEmail').val(data.result.name);
                                    data = data.result.email_templates;
                                    $('#template').attr('ticket', ticketId);
                                    for (options of data) {
                                        //TOdo: Add Email Slug as Value
                                        $('#template').append(`<option value="${options.assign_to}" data-template='${options.template}'>${options.name}</option`);
                                        if (first) {
                                            ticketIdToEmailTemplate(ticketId, options.template);
                                            first = false;
                                        }
                                    }
                                    $('#sendImageModal').modal('show');
                                } else {
                                    disabled = true;
                                    reason = "No Email Templates Found";
                                }
                            } else {
                                disabled = true;
                                reason = "Cannot Fetch Email Templates";
                            }

                            // If Disabled
                            if (disabled) {
                                if (reason == null)
                                    $('#communityNameForEmail').val(reason);
                                else
                                    $('#template').html(`<option>${reason}</option`);

                                $('#template').attr('disabled', 'disabled');
                                $('#message').attr('disabled', 'disabled');
                                $('#sendImageModal').modal('show');
                            }
                        });


                } else {
                    disabled = true;
                }

                // If Disabled
                if (disabled) {
                    if (reason == null)
                        $('#communityNameForEmail').val(reason);
                    else
                        $('#template').html(`<option>${reason}</option`);

                    $('#template').attr('disabled', 'disabled');
                    $('#message').attr('disabled', 'disabled');
                    $('#sendImageModal').modal('show');
                }


            });

            // On Template change, change the inner html
            $('body').on('change', '#template', function (e) {
                template = $(e.target).find(':selected').data('template');
                ticketId = $(e.target).attr('ticket');
                ticketIdToEmailTemplate(ticketId, template);
            });

            //  On Send Email Click
            $('body').on('click', '#sendEmail', function (e) {
                e.preventDefault();
                // Template
                template = $(".text-editor").summernote('code');

                // Email Assign To
                assign_to = $('#template option:selected').attr('value');

                //Ticket ID
                ticketId = $('#template').attr('ticket');

                // Title
                title = $('#template option:selected').html();


                // Send VIA POST Request
                $.post(`{{route('admin.traffic-tickets.'.strtolower($title).'-email-send')}}`, {
                    ticketId,
                    title,
                    template,
                    assign_to
                }, function (data) {
                    $('#sendImageModal').modal('hide');
                    notifySuccess("Email Sent");
                });

            });
        });

        function ResidentChange(type = 'resident', ticketid) {
            $('.ResVenName').html(type);
            if ($('#resvenoptions').data('select2')) {
                $('#resvenoptions').select2("destroy");
            }
            $('#resvenoptions').empty();
            $('#resvenoptions').data('id', ticketid);
            $('#resvenoptions').data('user_type', type);

            $.get(`{{route('admin.traffic-tickets.unmatched-attach-user')}}?user_type=${type}`).done(function (data) {
                $('#resvenoptions').append('<option></option>');
                for (person of data.result) {
                    $('#resvenoptions').append(`<option value=${person.id}>${person.full_name}</option>`);
                }
                $("#resvenoptions").select2({
                    placeholder: "Select Resident",
                    dropdownParent: $('#ResVenModal')
                });
                $('#ResVenModal').modal('show');
            });
        }

        function VendorChange(type, ticketid) {
            $('.VendorChangeName').html(type);
            if ($('#vendorchangeoptions').data('select2')) {
                $('#vendorchangeoptions').select2("destroy");
            }

            if ($('#vendorlicence').data('select2')) {
                $('#vendorlicence').select2("destroy");
            }

            $('#vendorchangeoptions').empty();
            $('#vendorlicence').empty();
            $('#vendorlicence').attr('disabled', 'disabled');


            $('#vendorchangeoptions').data('id', ticketid);
            $('#vendorchangeoptions').data('user_type', type);

            $.get(`{{route('admin.traffic-tickets.unmatched-attach-user')}}?user_type=${type}`).done(function (data) {
                $('#vendorchangeoptions').append('<option></option>');
                for (person of data.result) {
                    $('#vendorchangeoptions').append(`<option value=${person.id}>${person.vendor_name}</option>`);
                }

                $("#vendorchangeoptions").select2({
                    id: '-1', // the value of the option
                    placeholder: "Select Vendor",
                    dropdownParent: $('#VendorChangeModal')
                });
                $('#VendorChangeModal').modal('show');
            });
        }

        //On Vendor Change
        $('#vendorchangeoptions').on('change', function (e) {
            $('#vendorlicence').empty();
            $('#vendorlicence').attr('disabled', 'disabled');

            vendor_id = $('#vendorchangeoptions').val();
            $.get(`{{route('admin.traffic-tickets.unmatched-attach-license')}}?vendor_id=${vendor_id}`).done(function (data) {
                if ((data.bool && data.result != null) && !Array.isArray(data.result)) {
                    for (const [id, license] of Object.entries(data.result)) {
                        $('#vendorlicence').append(`<option value=${license}>${license}</option>`);
                    }
                    $('#vendorlicence').removeAttr('disabled');
                } else {
                    $('#vendorlicence').append(`<option>No Vehicles</option>`);
                }
                $("#vendorchangeoptions").select2({
                    dropdownParent: $('#VendorChangeModal')
                });
                $('#VendorChangeModal').modal('show');
            });
        });

        $(document).ready(function () {
            $('#notificationUl').css('margin-top', '57px');
        });
    </script>
@endpush
