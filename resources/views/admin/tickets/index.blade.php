@extends('admin.layout.master')
@push('css')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/summernote/summernote-bs4.css')}}">
@endpush
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{@$title}} Tickets</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">{{@$title}} tickets</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>

                                <div class="card-tools">
                                    <div class="sendemailbulk float-left" style="display:none;">
                                        <button class="btn btn-sm btn-primary" id="sendEmailToAll">Send Bulk Email
                                        </button>
                                    </div>
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="ticket_id">Ticket ID</label>
                                                <input type="text" name="ticket_id" id="ticket_id"
                                                       placeholder="Ticket ID" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="license_plate">License Plate</label>
                                                <input type="text" name="license_plate" id="license_plate"
                                                       placeholder="License Plate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="vehicle">Vehicle</label>
                                                <input type="text" name="vehicle" id="vehicle" placeholder="Vehicle"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="is_paid">Payment</label>
                                                <select class="form-control" name="is_paid">
                                                    <option value="" selected disabled>Select One</option>
                                                    <option value="1">Received</option>
                                                    <option value="0">Pending</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="household">Household</label>
                                                <input type="household" name="household" id="household"
                                                       placeholder="household" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community">Community</label>
                                                <input type="text" name="community" id="community"
                                                       placeholder="Community" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="from">From</label>
                                                <input type="date" name="from" id="from" placeholder="from"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="to">To</label>
                                                <input type="date" name="to" id="to" placeholder="to"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="excess_speed">Excess Speed</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="excess_speed_start"
                                                           id="excess_speed_start" placeholder="1" min="1" max="99"
                                                           class="form-control col-5">
                                                    <div
                                                        class="col-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="excess_speed_end" id="excess_speed_end"
                                                           placeholder="99" min="1" max="99" class="form-control col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="email_count">Email Count</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="email_count_start" id="email_count_start"
                                                           placeholder="1" min="1" max="99" class="form-control col-5">
                                                    <div
                                                        class="col-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="email_count_end" id="email_count_end"
                                                           placeholder="99" min="1" max="99" class="form-control col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="violation_count">Violation Count</label>
                                                <div class="row mx-1">
                                                    <input type="number" name="violation_count_start"
                                                           id="violation_count_start" placeholder="1" min="1" max="99"
                                                           class="form-control col-5 ">
                                                    <div
                                                        class="col-md-2 form-control text-center font-weight-bolder border-0">
                                                        -
                                                    </div>
                                                    <input type="number" name="violation_count_end"
                                                           id="violation_count_end" placeholder="99" min="1" max="99"
                                                           class="form-control col-5 ">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-check mt-4">
                                                <input class="form-check-input" type="checkbox" value="true"
                                                       id="displayDismissed" name="displayDismissed">
                                                <label class="form-check-label" for="displayDismissed">
                                                    Display Dismissed
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th><i class="fas fa-envelope"></i></th>
                                    <th>Payment</th>
                                    <th>Ticket ID</th>
                                    <th>License Plate</th>
                                    <th>Vehicle</th>
                                    <th>Current Speed Limit</th>
                                    <th>Violating Speed</th>
                                    <th>Excess Speed</th>
                                    <th>Time</th>
                                    <th>Community</th>
                                    <th>Household</th>
                                    <th>Email Count</th>
                                    <th>Violation Count</th>
                                    <th>Image Count</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.tickets.includes.modals')
    <!-- Ticket Deatils -->
    <div class="modal fade" id="TicketDetails" tabindex="-1" role="dialog" aria-labelledby="TicketDetailsTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TicketDetailsTitle">Ticket Detail <span id="ticketId"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="ticketDetailsTable">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Vehicle</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Make</th>
                                    <td id="make"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Model</th>
                                    <td id="model">Jacob</td>
                                </tr>
                                <tr>
                                    <th scope="row">Color</th>
                                    <td id="color"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Year</th>
                                    <td id="year"></td>
                                </tr>
                                <tr>
                                    <th scope="row">License Plate</th>
                                    <td class="license"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Ticket</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Location:</th>
                                    <td id="location"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Limit</th>
                                    <td id="speedLimit">Jacob</td>
                                </tr>
                                <tr>
                                    <th scope="row">Speed Voilation</th>
                                    <td id="speedViolation"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Date</th>
                                    <td id="date"></td>
                                </tr>
                                <tr>
                                    <th scope="row">License Plate</th>
                                    <td class="license"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Resident</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Full Name</th>
                                    <td id="fullName"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Email</th>
                                    <td id="email"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Phone Number</th>
                                    <td id="phone"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Community</th>
                                    <td id="community_name"></td>
                                </tr>
                                <tr>
                                    <th scope="row">House</th>
                                    <td id="house"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Address</th>
                                    <td id="address"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6" id="resTickDetOnly">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col" colspan="2" class="table-primary">Pass Sent By User</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Full Name</th>
                                    <td id="fullNamePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Email</th>
                                    <td id="emailPass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Phone Number</th>
                                    <td id="phonePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Community</th>
                                    <td id="community_namePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">House</th>
                                    <td id="housePass"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Address</th>
                                    <td id="addressPass"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Send Email -->
    <div class="modal fade" id="sendImageModal" tabindex="-1" role="dialog" aria-labelledby="sendImageTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Email Ticket</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" id="sendImageBody">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="emailCommunity">Community</label>
                                <input name="emailCommunity" class="form-control" disabled id="communityNameForEmail"
                                       value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="template">Choose Template</label>
                                <select name="template" class="form-control" id="template">
                                    <option>Test 1</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control text-editor" rows="10" id="message"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                    <button type="button" id="sendEmail" class="btn btn-primary float-right">Send Email</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('assets/admin/plugins/summernote/summernote-bs4.min.js')}}"></script>

    <script>
        function ticketIdToEmailTemplate(id, template) {
            $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-ticket-details')}}?ticket_id=${ticketId}`)
                .done(function (data) {
                    if (data.status) {
                        // Fetch the Result
                        data = data.result;
                        // Date Time
                        datetime = new Date().toLocaleString();

                        // MAP URL Generator
                        geocode = data.location.geocode.toString();
                        map_url = `https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=300x300&maptype=roadmap&markers=color:red%7Clabel:Z%7C${geocode.slice(1, -1)}&key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA`;

                        console.log(map_url);

                        // Add the Variables
                        template = template.replace('{ticket_id}', data.ticket_id);
                        template = template.replace('{plate_text}', data.license_plate);
                        template = template.replace('{timestamp}', datetime);
                        template = template.replace('{ticket_time}', data.time);
                        template = template.replace('{current_speed_limit}', data.current_speed_limit);
                        template = template.replace('{violating_speed}', data.violating_speed);
                        template = template.replace('{location_address}', data.location.name);
                        template = template.replace('{location_name}', data.location.name);
                        image = "";
                        if (data.ticket_images.length > 0) {
                            image = `<img src="{{env('APP_URL')}}/images/tickets/${data.ticket_images[0].image}" style="width:400px" />`;
                        }
                        template = template.replace("{image}", image);
                        template = template.replace('{notice_number}', parseInt(data.email_log_count) + 1);
                        template = template.replace('{geocode}', `<img src="${map_url}" />`);

                        $('.text-editor').summernote('code', template);
                    }
                });
        }


        $(document).ready(function () {

            $('.text-editor').summernote();

            // DataTable Columns
            let columns = [
                {data: 'id'},
                {data: 'mass_mail'},
                {data: 'is_paid'},
                {data: 'ticket_id'},
                {data: 'license_plate'},
                {data: 'vehicle'},
                {data: 'current_speed_limit'},
                {data: 'violating_speed'},
                {data: 'excess_speed'},
                {data: 'formatted_time'},
                {data: 'community'},
                {data: 'household'},
                {data: 'email_log_count'},
                {data: 'violation_count', name: "violation_count_raw"},
                {data: 'ticket_images_count'},
                {data: 'action'},
            ];

            // Route for Ajax Calls
            @php $datatable_route = route('admin.traffic-tickets.'.strtolower($title).'-tickets-data') @endphp @if(isset($houseId))'?houseId='.$houseId;
            @endif
            @include('admin.tickets.includes.scripts')

            $('body').on('click', '#ticketDetailBtn', function (e) {

                //Clear the Table
                $('#ticketDetailsTable table tbody td').html('');
                $('#ticketId').html('');


                let ticketId = $(e.target).data('id');
                $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-ticket-details')}}?ticket_id=${ticketId}`).done(function (data) {

                    if (data.status) {
                        pass_user = data.pass;
                        vendor = data.vendor;
                        data = data.result;

                        // Ticket ID for Title
                        $('#ticketId').html(data.ticket_id);


                        //Ticket Deatils
                        $('#speedLimit').html(data.current_speed_limit);
                        $('#speedViolation').html(data.violating_speed);
                        $('#date').html(data.time);
                        $('.license').html(data.license_plate);
                        data.location != null ? $('#location').html(data.location.name) : '';


                        if (data.vehicle != null) {
                            // Vehicle Details
                            $('#make').html(data.vehicle.make);
                            $('#model').html(data.vehicle.model);
                            $('#color').html(data.vehicle.color);
                            $('#year').html(data.vehicle.year);
                        }

                        if (data.ticket_type == 'vendor' && vendor != null) {
                            $('#fullName').html(vendor.vendor_name);
                            $('#email').html(vendor.email);
                            $('#phone').html(vendor.phone);
                            $('#house').html('N/A');
                            $('#address').html(vendor.formattedAddress);
                        } else
                            // Add User Details
                        if (data.user != null) {
                            $('#fullName').html(data.user.full_name);
                            $('#email').html(data.user.email);
                            $('#phone').html(data.user.phone_number);

                            // Add House Details
                            if (data.user.house != null) {
                                house = data.user.house
                                $('#house').html(house.house_name);
                                $('#address').html(house.house_detail);
                            }


                        }


                        if (data.ticket_type == "resident") {
                            $("#resTickDetOnly").show();
                            // Add User Details
                            if (pass_user != null && (pass_user.pass != null && pass_user.pass.created_by != null)) {
                                $('#fullNamePass').html(pass_user.pass.created_by.full_name);
                                $('#emailPass').html(pass_user.pass.created_by.email);
                                $('#phonePass').html(pass_user.pass.created_by.phone_number);
                                $('#addressPass').html(pass_user.pass.created_by.formattedAddress);

                                // Add House Details
                                if (pass_user.pass.created_by.house != null) {
                                    house = pass_user.pass.created_by.house
                                    $('#housePass').html(house.house_name);
                                }

                                pass_user.pass.created_by.community != null ? $('#community_namePass').html(pass_user.pass.created_by.community.name) : '';

                            }
                        } else {
                            $("#resTickDetOnly").hide();
                        }

                        // Add Community
                        (data.user != null && data.user.community != null) ? $('#community_name').html(data.user.community.name) : '';

                        // Show the model
                        $('#TicketDetails').modal('show');
                    }

                });

            });

            // On SendMail Click
            $('body').on('click', '.sendMail', function (e) {

                // Remove the Old Options and Disabled Attribute
                $('#template').removeAttr('disabled');
                $('#template').removeAttr('ticket');
                $('#message').attr('disabled');
                $('#message').empty();
                $('#template').empty();

                // Variables if No Data is Found / Error Occured
                let disabled = false;
                let reason = null;

                // For First Email Template
                first = true;

                //Ticket ID - To Fetch Ticket Email
                ticketId = $(e.target).data('id');

                // Get Community ID
                let community_id = $(e.target).data('communityid');

                // Check if Community ID Exists
                if (community_id != null || community_id != '') {

                    // Get the Traffic Logix Email teampltes for Community
                    $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-email-templates')}}?community_id=${community_id}`)
                        .done(function (data) {
                            if (data.status) {
                                if (data.result != null && data.result.email_templates.length) {
                                    $('#communityNameForEmail').val(data.result.name);
                                    data = data.result.email_templates;
                                    $('#template').attr('ticket', ticketId);
                                    for (options of data) {
                                        //TOdo: Add Email Slug as Value
                                        $('#template').append(`<option value="${options.assign_to}" data-template='${options.template}'>${options.name}</option`);
                                        if (first) {
                                            ticketIdToEmailTemplate(ticketId, options.template);
                                            first = false;
                                        }
                                    }
                                    $('#sendImageModal').modal('show');
                                } else {
                                    disabled = true;
                                    reason = "No Email Templates Found";
                                }
                            } else {
                                disabled = true;
                                reason = "Cannot Fetch Email Templates";
                            }

                            // If Disabled
                            if (disabled) {
                                if (reason == null)
                                    $('#communityNameForEmail').val(reason);
                                else
                                    $('#template').html(`<option>${reason}</option`);

                                $('#template').attr('disabled', 'disabled');
                                $('#message').attr('disabled', 'disabled');
                                $('#sendImageModal').modal('show');
                            }
                        });


                } else {
                    disabled = true;
                }

                // If Disabled
                if (disabled) {
                    if (reason == null)
                        $('#communityNameForEmail').val(reason);
                    else
                        $('#template').html(`<option>${reason}</option`);

                    $('#template').attr('disabled', 'disabled');
                    $('#message').attr('disabled', 'disabled');
                    $('#sendImageModal').modal('show');
                }


            });

            // On Template change, change the inner html
            $('body').on('change', '#template', function (e) {
                template = $(e.target).find(':selected').data('template');
                ticketId = $(e.target).attr('ticket');
                ticketIdToEmailTemplate(ticketId, template);
            });

            //  On Send Email Click
            $('body').on('click', '#sendEmail', function (e) {
                e.preventDefault();
                // Template
                template = $(".text-editor").summernote('code');

                // Email Assign To
                assign_to = $('#template option:selected').attr('value');

                //Ticket ID
                ticketId = $('#template').attr('ticket');

                // Title
                title = $('#template option:selected').html();


                // Send VIA POST Request
                $.post(`{{route('admin.traffic-tickets.'.strtolower($title).'-email-send')}}`, {
                    ticketId,
                    title,
                    template,
                    assign_to
                }, function (data) {
                    $('#sendImageModal').modal('hide');
                    notifySuccess("Email Sent");
                });

            });

            // On checkbox
            $(document).on('change', '.checkboxrow', function () {
                if ($(this).is(":checked")) {
                    $('.sendemailbulk').fadeIn();
                } else if ($(this).is(":not(:checked)")) {
                    if ($(".checkboxrow:checked").length < 1)
                        $('.sendemailbulk').fadeOut();
                }
            });

            /* send email to all */
            $('#sendEmailToAll').click(function () {
                let ids = [];
                var checkedLength = $(".checkboxrow:checked").length;
                if (checkedLength > 0) {

                    $('#send_bulk_email_template_modal').modal('show');
                } else {
                    alert('Select any record to send an email')
                }

                //Send email to selected rows


            });

        });
        // Future Work: Optimize and Merge with Print Function
        $('#setBulkEmailTemplateToSendBtn').on('click', function (e) {
            e.preventDefault();
            showLoader();
            let ids = [];
            $('.checkboxrow:checked').each(function () {
                var id = $(this).attr('data-id');
                ids.push(id);
            });
            $.ajax({
                url: '{{route('admin.traffic-tickets.bulk-email-send')}}',
                type: 'post',
                data: {
                    'ids': ids,
                    'template': $('#assign_bulk_to').val(),
                    '_token': '{{csrf_token()}}',
                    'type': 'email'
                },
                success: function (response) {
                    $('.checkboxrow:checked').each(function () {
                        $(this).attr('checked', false);
                    });
                    $('#send_bulk_email_template_modal').modal('hide');
                    hideLoader();
                }
            })
        })
        // Future Work: Optimize and Merge with Mail Function
        $(document).on('click', '.printTemplateBulk', function (e) {
            e.preventDefault();
            showLoader();
            let ids = [];
            $('.checkboxrow:checked').each(function () {
                var id = $(this).attr('data-id');
                ids.push(id);
            });
            $.ajax({
                url: '{{route('admin.traffic-tickets.bulk-email-print')}}',
                type: 'post',
                data: {
                    'ids': ids,
                    'template': $('#assign_bulk_to').val(),
                    '_token': '{{csrf_token()}}',
                    'type': 'print'
                },
                success: function (result) {
                    $('.checkboxrow:checked').each(function () {
                        $(this).attr('checked', false);
                    });
                    $('#send_bulk_email_template_modal').modal('hide');
                    hideLoader();

                    if (result.status) {
                        var mywindow = window.open('', 'PRINT');
                        mywindow.document.write(result['data']);
                        mywindow.document.close(); // necessary for IE >= 10
                        mywindow.focus(); // necessary for IE >= 10*/
                        setInterval(function () {
                            mywindow.print();
                        }, 3000);
                    } else {
                        notifyError("Something Went Wrong");
                    }


                }
            })

        });
    </script>
@endpush
