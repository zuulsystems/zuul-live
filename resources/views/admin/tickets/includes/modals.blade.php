<!-- Ticket Images -->
<div class="modal fade" id="TicketImagesModal" tabindex="-1" role="dialog" aria-labelledby="ticketImagesTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ticketImagesTitle">Ticket Images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="ticketImagesBody">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade send_bulk_email_template_modal" id="send_bulk_email_template_modal" tabindex="-1" role="dialog"
     data-id="">
    <div class="modal-dialog">
        <div class="modal-content" id="send_bulk_email_template_modal">
            <form method="post" class="ticket_email_form" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Bulk Email Templates</h4>
                </div>
                <div class="card-body">
                    <form method="get" action="" class="searchForm">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="assign_bulk_to">Choose Template</label>
                                        <select name="assign_bulk_to" id="assign_bulk_to" class="form-control">
                                            <option value="traffic_ticket_email">Traffic Ticket Email</option>
                                            <option value="traffic_ticket_reminder_email">Traffic Ticket Reminder
                                                Email
                                            </option>
                                            <option value="traffic_ticket_warning_email">Traffic Ticket Warning Email
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /.box-body -->
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-primary pull-left printTemplateBulk">Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary action-btn" id="setBulkEmailTemplateToSendBtn">Send
                    </button>
                </div>
            </form>
            </form>
        </div>
    </div>
</div>


