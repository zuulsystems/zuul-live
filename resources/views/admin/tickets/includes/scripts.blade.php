
        //URL for datatables
        url = "{{$datatable_route}}@if( request()->get('user_id') )?user_id={{request()->get('user_id')}}@endif";


        //Render Datatables
        TLDataTable('#datatable',url,columns);

        $('.search-btn').click(function(){
            data = formDataForDataTable('#search-filter');
            TLDataTable('#datatable', url, columns, [], "post", data);
        })




        $('body').on( 'click', '#ticketImagesBtn',function(e){
              //Clear the Modal body
              $('#ticketImagesBody').empty();

              // TicketID
              let ticketId = $(e.target).data('ticketid');

              //Fetch The Images
              $.get(`{{route('admin.traffic-tickets.'.strtolower($title).'-ticket-images')}}?ticket_id=${ticketId}`).done(function (response){
                    if(response.status){
                        for(image of response.result){
                        $('#ticketImagesBody').append(`
                        <div class="col-md-12">
                                <a class="btn btn-xs btn-danger float-right" style="position: relative; top: 34px; right: 7px;" href="${image.image}" download >Save Image</a>
                                <img src="${image.image}" alt="${image.image}" title="${image.image}" class="img-fluid">
                        </div>`);
                    }

                    $('#TicketImagesModal').modal('show');

                }
              });
        });

        $('body').on( 'click', '#deleteTicketBtn',function(e){
            if(confirm("Do you want to dismiss the ticket?")){
                // TicketID
                let ticketId = $(e.target).data('id');
                // URL With Placeholder
                let url = "{{route('admin.traffic-tickets.'.strtolower($title).'-tickets.destroy', ':id:')}}";
                // Replace Placeholder with TicketID
                url = url.replace(':id:', ticketId);
                // Delete the ticket
                $.post(url, {'_method': 'delete'}).done(function (data){
                    if(data.status){
                        // Re-Render the table
                        TLDataTable('#datatable',"{{$datatable_route}}",columns);
                    }
                });
            }

        });

        $('body').on( 'click', '#undeleteTicketBtn',function(e){
            if(confirm("Do you want to undismiss the ticket?")){
                // TicketID
                let ticketId = $(e.target).data('id');
                // URL With Placeholder
                let url = "{{route('admin.traffic-tickets.'.strtolower($title).'-tickets.restore', ':id:')}}";
                // Replace Placeholder with TicketID
                url = url.replace(':id:', ticketId);
                // Delete the ticket
                $.post(url, {'_method': 'patch'}).done(function (data){
                    if(data.status){
                        // Re-Render the table
                        TLDataTable('#datatable',"{{$datatable_route}}",columns);
                    }
                });
            }

        });

        // On Payment Click
        $('body').on('change', '.payment', function(e){
            ticketId = $(e.target).data('ticketid');
            checked = this.checked;
            $.post(`{{route('admin.traffic-tickets.'.strtolower($title).'-payment')}}`, {ticketId, checked},
                (data) => {
                    if(data.status){
                        label = checked ? "Paid" : "Pending";
                        $(this).parent().find('label').text(label);
                        notifySuccess("Ticket marked as "+label);
                    } else {
                        notifyError("Somethig Went Wrong. Cannot mark ticket as paid.");
                        $(this).prop("checked", !checked);
                    }
                }
            );
        });
