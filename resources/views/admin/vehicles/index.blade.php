@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Vehicles List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Vehicles List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="license_plate">License Plate</label>
                                                <input type="text" name="license_plate" id="license_plate"
                                                       placeholder="License Plate" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="make">Make</label>
                                                <input type="text" name="make" id="make" placeholder="Make"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="model">Model</label>
                                                <input type="text" name="model" id="model" placeholder="Model"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="year">Year</label>
                                                <input type="text" name="year" id="year" placeholder="Year"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="color">Color</label>
                                                <input type="text" name="color" id="color" placeholder="Color"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="user">User</label>
                                                <input type="text" name="user" id="user" placeholder="User"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="vehicleTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>License Plate</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Year</th>
                                    <th>Color</th>
                                    <th>User</th>
                                    <th>Tickets</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="attachWithVendorModal" tabindex="-1" role="dialog" aria-labelledby="VendorChangeTitle"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{route('admin.attach-vehicle-with-vendor')}}" method="POST" class="ajax-form">
                    <div class="modal-header">
                        <h5 class="modal-title" id="VendorChangeTitle"> Connect a Vendor -<span
                                class="licensePlate"> </span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="VendorChangeBody">
                            <input type="hidden" value="" id="attach_vehicle_id" name="attach_vehicle_id">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="VendorChangeName">Select a vendor to attach License plate</span></label>
                                    <br/>
                                    <select class="form-control select2bs4" name="vendor_id" style="width: 100%">
                                        @foreach($vendors as $vendor)
                                            <option value="{{$vendor->id}}">{{$vendor->vendor_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary float-right" id="vendorchangesave">Save</button>
                        <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: function (data) {
                    if (data.license_plate == null || data.license_plate == '') {
                        return data.license_plate;
                    } else {
                        return data.license_plate.toUpperCase();
                    }

                },
                name: 'license_plate'

            },
            {
                data: 'make',
            },
            {
                data: 'model',
            },
            {
                data: 'year',
            },
            {
                data: 'color',
            },
            {
                data: 'user_name',
                name: 'user_name'

            },
            {
                data: 'ticket_count',
                name: 'ticket_count',

            },
            {
                data: 'edit',
            },
        ];

        zuulDataTableSortableGuestLogs('#vehicleTable', "{{route('admin.vehicles-data')}}?{{Request::getQueryString()}}", columns, [8]);

        $('.search-btn').click(function () {
            var ajax = "{{route('admin.vehicles-data')}}?{{Request::getQueryString()}}&" + $("#search-filter").serialize();
            zuulDataTableSortableGuestLogs('#vehicleTable', ajax, columns, [8]);
        });

        //connect with vendor model click event
        $(document).on('click', '.btn-connect-with-vendor-model', function () {
            let id = $(this).data('id');
            let license_plate = $(this).data('name');
            console.log(license_plate)
            $('#attach_vehicle_id').val(id);
            $('.licensePlate').html(license_plate);
            $('#attachWithVendorModal').modal('show');
        });
    </script>
@endpush
