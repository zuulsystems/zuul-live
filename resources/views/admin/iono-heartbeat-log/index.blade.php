@extends('admin.layout.master')
@section('content')
    @include('admin.layout.datatable-css')
    <style>

        .dropdown-menu.show {
            position: absolute !important;
            top: 50% !important;
            left: -39% !important;
            transform: translateX(-50%) translateY(-50%) !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>IONO Heartbeat List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">IONO Heartbeat List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="connected_pi_mac_address">Connected Pi Mac Address</label>
                                                <input type="text" name="connected_pi_mac_address"
                                                       id="connected_pi_mac_address"
                                                       placeholder="Connected Pi Mac Address" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community">Community</label>
                                                <input type="text" name="community" id="community"
                                                       placeholder="Community" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="type">Type</label>
                                                <select name="type" class="form-control">
                                                    <option value="">All</option>
                                                    <option value="pi">Pi</option>
                                                    <option value="iono">Iono</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="">All</option>
                                                    <option value="1">Active</option>
                                                    <option value="0">Deactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                                <button type="button" class="btn btn-warning reset-search">
                                    <span>Reset </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="ionoHeartbeatLogTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Connected Pi Mac Address</th>
                                    <th>Community</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Time Stamp</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @php
        $showClass = '';
      $housesImportModel = '';
      if ($errors->any())
      {
      $showClass = 'show';
      $housesImportModel = 'display: block';
      }
    @endphp
        <!-- House Model--->
    <div class="modal fade" id="house-import-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import CSV</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            id="closeHousesImportModalBtn">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{ route('admin.residential.import-houses') }}"
                      enctype="multipart/form-data" class="ajax-form">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="house_csv">CSV file to import</label>
                                    <input type="file" name="house_csv" id="house_csv" class="col-md-12" accept=".csv"/>
                                </div>
                            </div>
                        </div>
                        <span class="text-danger" id="field"></span>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="fileUploadInformationTable">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer justify-content-between">
                        <a href="{{ asset('assets/csv-sample/house.csv') }}" class="btn btn-default">Download Sample
                            File</a>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@php
    if(session()->has('housesImportedSuccessfully'))
    {
@endphp
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-success" aria-live="polite" style="display: block; opacity: 1;">
        <div class="toast-message">Houses imported successfully.</div>
    </div>
</div>
<script>
    setTimeout(function () {
        document.getElementsByClassName('toast-success')[0].style.display = "none";
    }, 1000);
</script>
@php
    }
@endphp
@push('scripts')
    <script src="{{asset('assets/admin/dist/js/main17-6-2022.js')}}"></script>
    <script>
        let columns = [
            {
                data: 'id',
            },
            {
                data: 'connected_pi_mac_address',
            },
            {
                data: 'community_name',
            },
            {
                data: 'type',
            },
            {
                data: function (data) {
                    return data.formatted_status;
                }
            },
            {
                data: 'created_at',
            },
        ];

        zuulDataTableSortable('#ionoHeartbeatLogTable',
            "{{ route('admin.iono-heartbeat-logs-data') }}", columns, [4]);

        $('.search-btn').click(function () {
            var ajax = "{{ route('admin.iono-heartbeat-logs-data') }}?" +
                $("#search-filter").serialize();
            zuulDataTableSortable('#ionoHeartbeatLogTable', ajax, columns, [4]);
        })

        $('#closeHousesImportModalBtn').click(function () {
            $("#house-import-modal").css("display", "none");
            $('.a').css('display', 'none');
        });

        $('#notificationUl').css('margin-top', '57px');

        $(document).ready(function () {
            $('.reset-search').click(function () {
                $('input').val('');
                $('select').val('');
                zuulDataTableSortable('#ionoHeartbeatLogTable', "{{ route('admin.iono-heartbeat-logs-data') }}", columns, [4]);

            });
        });
    </script>
@endpush
