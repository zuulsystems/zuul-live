@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid card-bg-heading">
                <div class="row mb-2 ">
                    <div class="col-sm-6">
                        <h1>Add Resident</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Resident</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.residents.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Go Back
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.residential.residents.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <input type="hidden" value="{{$formClass}}" name="formClass"/>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first_name">First Name </label>
                                                <input type="text" name="first_name" id="first_name"
                                                       placeholder="First Name" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="last_name">Last Name </label>
                                                <input type="text" name="last_name" id="last_name"
                                                       placeholder="Last Name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Email </label>
                                                <input type="text" name="email" id="email"
                                                       placeholder="Email" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="dial_code">Country code</label>
                                                @php
                                                    $url = route('get-phone-format-by-dial-code');
                                                @endphp
                                                <select
                                                    id="dial_code" name="dial_code" class="form-control select2bs4"
                                                    onchange="changePhoneDigitsFormat1('{{ $url }}')">
                                                    @foreach($countries as $country)
                                                        <option
                                                            value="+{{$country->dial_code}}">{{$country->country_name}}
                                                            +{{$country->dial_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <div
                                                    style=" font-size: 12px;display: inline;color: #1f2d3d;padding: 0 5px;border-radius: 5px;cursor: default;border: 1px solid black;"
                                                    id="add-new-row">Add Additional Phone Number(s)<i
                                                        class="fa fa-plus-circle" style="margin-left: 10px"></i></div>
                                                <div id="remove-field"
                                                     style=" font-size: 12px;display: none;color: red;padding: 0 5px;border-radius: 5px;cursor: default;border: 1px solid red;margin-left:5px"
                                                     id="add-new-row">Remove field<i class="fa fa-times"
                                                                                     style="margin-left: 10px"></i>
                                                </div>
                                                <input type="text" class="form-control" name="phone_number"
                                                       id="phone_number">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row" id="additional_phone_number1" style="display: none">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number1_dial">Dial Code</label>
                                                        @php
                                                            $url = route('get-phone-format-by-dial-code');
                                                        @endphp
                                                        <select name="additional_phone_number1_dial"
                                                                class="form-control"
                                                                onchange="changePhoneDigitsFormatofAdditionalPhone(this.value,'{{ $url }}','addditional_phone_number1')">
                                                            @foreach($countriesForAdditionalPhoneNumbers as $country)
                                                                <option
                                                                    value="+{{$country->dial_code}}">{{$country->country_name}}
                                                                    +{{$country->dial_code}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number1">Additional Phone Number
                                                            1</label>
                                                        <input type="text" class="form-control additional_phone_number1"
                                                               name="additional_phone_number1"
                                                               id="addditional_phone_number1">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number1_type">Type</label>
                                                        <select name="additional_phone_number1_type"
                                                                class="form-control">
                                                            <option disabled selected>Select</option>
                                                            <option value="Main">Main</option>
                                                            <option value="Home">Home</option>
                                                            <option value="Office">Office</option>
                                                            <option value="Work">Work</option>
                                                            <option value="Friend">Friend</option>
                                                            <option value="Parents">Parents</option>
                                                            <option value="Colleague">Colleague</option>
                                                            <option value="Relative">Relative</option>
                                                            <option value="Siblings">Siblings</option>
                                                            <option value="School/College">School/College</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row" id="additional_phone_number2" style="display: none">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number2_dial">Dial Code</label>
                                                        @php
                                                            $url = route('get-phone-format-by-dial-code');
                                                        @endphp
                                                        <select name="additional_phone_number2_dial"
                                                                class="form-control"
                                                                onchange="changePhoneDigitsFormatofAdditionalPhone(this.value,'{{ $url }}','addditional_phone_number2')">
                                                            @foreach($countriesForAdditionalPhoneNumbers as $country)
                                                                <option
                                                                    value="+{{$country->dial_code}}">{{$country->country_name}}
                                                                    +{{$country->dial_code}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number2">Additional Phone Number
                                                            2</label>
                                                        <input type="text" class="form-control additional_phone_number2"
                                                               name="additional_phone_number2"
                                                               id="addditional_phone_number2">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number2_type">Type</label>
                                                        <select name="additional_phone_number2_type"
                                                                class="form-control">
                                                            <option disabled selected>Select</option>
                                                            <option value="Main">Main</option>
                                                            <option value="Home">Home</option>
                                                            <option value="Office">Office</option>
                                                            <option value="Work">Work</option>
                                                            <option value="Friend">Friend</option>
                                                            <option value="Parents">Parents</option>
                                                            <option value="Colleague">Colleague</option>
                                                            <option value="Relative">Relative</option>
                                                            <option value="Siblings">Siblings</option>
                                                            <option value="School/College">School/College</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row" id="additional_phone_number3" style="display: none">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label for="additional_phone_number3_dial">Dial Code</label>
                                                    @php
                                                        $url = route('get-phone-format-by-dial-code');
                                                    @endphp
                                                    <select name="additional_phone_number3_dial" class="form-control">
                                                        @foreach($countriesForAdditionalPhoneNumbers as $country)
                                                            <option
                                                                value="+{{$country->dial_code}}">{{$country->country_name}}
                                                                +{{$country->dial_code}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number3">Additional Phone Number
                                                            3</label>
                                                        <input type="text" class="form-control additional_phone_number3"
                                                               name="additional_phone_number3"
                                                               id="additional_phone_number3">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="additional_phone_number3_type">Type</label>
                                                        <select name="additional_phone_number3_type"
                                                                class="form-control">
                                                            <option disabled selected>Select</option>
                                                            <option value="Main">Main</option>
                                                            <option value="Home">Home</option>
                                                            <option value="Office">Office</option>
                                                            <option value="Work">Work</option>
                                                            <option value="Friend">Friend</option>
                                                            <option value="Parents">Parents</option>
                                                            <option value="Colleague">Colleague</option>
                                                            <option value="Relative">Relative</option>
                                                            <option value="Siblings">Siblings</option>
                                                            <option value="School/College">School/College</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="community_id">Community Assigned</label>
                                                <select
                                                    id="community_id" name="community_id"
                                                    class="form-control select2bs4">
                                                    <option value="">Select Community</option>
                                                    @foreach($communities as $community)
                                                        <option value="{{$community->id}}">{{$community->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="house_id">House Assigned</label>
                                                <div style="display: inline-block; margin-left: 7px;">
                                                    <input type="radio" id="houseAddress">
                                                    <span> By Address </span>
                                                </div>
                                                <div style="display: inline-block; margin-left: 7px;">
                                                    <input type="radio" id="houseName">
                                                    <span> By Name </span>
                                                </div>
                                                <select id="house_id" name="house_id" class="form-control select2bs4">
                                                    <option value="">Select House</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="is_head_of_family">Is Head Of Family</label>
                                                <select id="is_head_of_family" name="is_head_of_family"
                                                        class="form-control">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="special_instruction">Special Instructions</label>
                                                <textarea name="special_instruction" id="special_instruction"
                                                          class="form-control"
                                                          placeholder="Write Additional Instructions ..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="profile_image">Profile Image</label>
                                                <input type="file" name="profile_image" id="profile_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Profile Picture.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="license_image">Profile License</label>
                                                <input type="file" name="license_image" id="license_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Drivers/Student ID.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $('#houseName').prop('checked', true);
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    $('#houseAddress').prop('checked', false);
                    let options = '';
                    if (result.status === true) {
                        $.each(result.result, function (key, row) {
                            options += `<option value="${row.id}">${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found!</option>`;
                    }
                    $('#house_id').html(options);
                }
            });
        });

        $(document).ready(function () {
            $('#houseAddress').click(function () {
                $('#houseName').prop('checked', false);
                var $communityId = $('#community_id').val();

                if ($communityId == "") {
                    alert('Please select community');
                    $(this).prop('checked', false);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route("admin.residential.house-by-address") }}",
                        data: {community_id: $communityId},
                        success: function (house) {

                            if (house.length > 0) {
                                var houseHTML = '';

                                for (a = 0; a < house.length; a++) {
                                    houseHTML += "<option value='" + house[a].id + "'>" + house[a].house_detail + "</option>";
                                }

                                $('#house_id').html(houseHTML);
                            } else {
                                var houseHTML = "<option value=''>No House Found!</option>";
                                $('#house_id').html(houseHTML);
                            }
                        }
                    });
                }

            });

            $('#houseName').click(function () {
                $('#houseAddress').prop('checked', false);
                var $communityId = $('#community_id').val();

                if ($communityId == "") {
                    alert('Please select community');
                    $(this).prop('checked', false);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "{{route('admin.residential.get-house-by-community-id')}}",
                        data: {
                            communityId: $communityId,
                        },
                        success: function (result) {
                            $('#houseAddress').prop('checked', false);
                            let options = '';
                            if (result.status === true) {
                                $.each(result.result, function (key, row) {
                                    options += `<option value="${row.id}">${row.house_name}</option>`;
                                });
                            } else {
                                options = `<option value="">No Houses Found!</option>`;
                            }
                            $('#house_id').html(options);
                        }
                    });
                }

            });

            var click_count = 0;
            $('#add-new-row').click(function () {
                document.getElementById("remove-field").style.display = "inline";
                click_count = click_count + 1;
                console.log(click_count);
                if (click_count < 3) {
                    var element_name = "additional_phone_number" + click_count;
                    document.getElementById(element_name).style.display = null;


                    if (click_count == 2) {
                        document.getElementById("add-new-row").style.display = 'none';
                    }


                }
            });

            $('#remove-field').click(function () {
                document.getElementById("add-new-row").style.display = 'inline';
                var element_name = "additional_phone_number" + click_count;
                var ex_class = '.' + element_name;
                $(ex_class).val('');
                document.getElementById(element_name).style.display = 'none';
                click_count = click_count - 1;

                if (click_count == 0) {
                    document.getElementById("remove-field").style.display = "none";
                }
                console.log(click_count);
            });

            var $phoneFormat = "{{ $phoneFormat }}";
            $('#phone_number').inputmask({mask: $phoneFormat});
            $('.additional_phone_number1').inputmask({mask: $phoneFormat});
            $('.additional_phone_number2').inputmask({mask: $phoneFormat});
            $('.additional_phone_number3').inputmask({mask: $phoneFormat});


        });
    </script>
@endpush
