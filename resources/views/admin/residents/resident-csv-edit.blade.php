@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Resident Csv</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Resident Csv</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.residents.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Residents List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.residential.resident-csvs-table-record-update', [$residentCsv->id])}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first_name">First Name </label>
                                                <input type="text" name="first_name" id="first_name"
                                                       placeholder="First Name" class="form-control"
                                                       value="{{$residentCsv->first_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="last_name">Last Name </label>
                                                <input type="text" name="last_name" id="last_name"
                                                       placeholder="Last Name" class="form-control"
                                                       value="{{$residentCsv->last_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email">Email </label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control" value="{{$residentCsv->email}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" name="phone_number" id="phone_number"
                                                       placeholder="Phone Number" class="form-control mask-phone"
                                                       value="{{$residentCsv->phone_number}}"
                                                       data-inputmask='"mask": "999-999-9999"' data-mask>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="house_id">House Assigned</label>
                                                <div style="display: inline-block; margin-left: 7px;">
                                                </div>
                                                <select id="house_id" name="house_id" class="form-control select2bs4">
                                                    <option value="">Select House</option>
                                                    @foreach($houses as $house)
                                                        <option
                                                            value="{{ $house->id }}" {{ $house->house_name == $residentCsv->house_name ? "selected" : ""}}>{{ $house->house_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="is_head_of_family">Is Head Of Family</label>
                                                <select id="is_head_of_family" name="is_head_of_family"
                                                        class="form-control">
                                                    <option value="0" @if($residentCsv->role_id == 5) selected @endif>
                                                        No
                                                    </option>
                                                    <option value="1"
                                                            @if($residentCsv->head_of_family == 4) selected @endif>Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="special_instruction">Special Instructions</label>
                                                <textarea name="special_instruction" id="special_instruction"
                                                          class="form-control"
                                                          placeholder="Write Additional Instructions ...">{{$residentCsv->special_instructions}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <span>Save Changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    $('#houseAddress').prop('checked', false);
                    let options = '';
                    if (result.status === true) {
                        $.each(result.result, function (key, row) {
                            if (communityId == "{{ $residentCsv->community_id }}") {
                                var selected = '';
                                if (row.id == "{{ $residentCsv->house_id }}") {
                                    selected = 'selected="true"'
                                }
                            }
                            options += `<option value="${row.id}" ${selected}>${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                }
            });
        });

        $(document).ready(function () {
            $('#community_id').change();
        })

        $(document).ready(function () {
            $('#houseAddress').click(function () {

                var $communityId = "{{ $residentCsv->community_id }}";


                if ($communityId == "") {
                    alert('Please select community');
                    $(this).prop('checked', false);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route("admin.residential.house-by-address") }}",
                        data: {community_id: $communityId},
                        success: function (house) {

                            if (house.length > 0) {
                                var houseHTML = '';

                                for (a = 0; a < house.length; a++) {
                                    houseHTML += "<option value='" + house[a].id + "'>" + house[a].house_detail + "</option>";
                                }

                                $('#house_id').html(houseHTML);
                            } else {
                                var houseHTML = "<option value=''>No House Found !</option>";
                                $('#house_id').html(houseHTML);
                            }
                        }
                    });
                }

            });
        });
    </script>
@endpush
