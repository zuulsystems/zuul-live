@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Imported Files</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Imported Files</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!--13-4-2022-->
                        <div class="card-body table-responsive">
                            <table id="importedFileDataTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>File Name</th>
                                    <th>Total Records</th>
                                    <th>Processed Records</th>
                                    <th>Community</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!--13-4-2022-->
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        var count = 0;

        function zuulImportFileDataTableSortable(selector, url, columns, disableColumnSearch = [], DefaultOrderColumn = 0) {

            $(selector).DataTable({
                "processing": true,
                "scrollX": true,
                "serverSide": true,
                "ajax": url,
                "columns": columns,
                "pageLength": 50,
                "destroy": true,
                "searching": false,
                "order": [[DefaultOrderColumn, "desc"]],
                "columnDefs": [
                    {"orderable": false, "targets": disableColumnSearch}
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "initComplete": function () {
                    var api = this.api();

                    // For each column
                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            // Set the header cell to contain the input element
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" placeholder="' + title + '" />');

                            // On every keypress in this input
                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();

                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})';

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                },

            });
        }

        let columns = [
            {data: 'file_name'},
            {data: 'total_records'},
            {data: 'process_records'},
            {data: 'community'},
            {data: 'created_at'},
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];

        var ajax = "{{route('admin.residential.imported-file-data')}}?file_name={{$fileName ?? ''}}";
        zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [5], 4);

        $.ajax({
            url: ajax,
            type: "GET",
            success: function (result) {
                if (result.recordsTotal > 0) {
                    $('#delete_file').css('display', 'inline-block');
                    $('#download_csv').css('display', 'inline-block');
                    $('#download_failed_csv').css('display', 'inline-block');
                    var $fileName = $('#file_name').val();
                    var $url = "{{ route('admin.residential.download-imported-file') }}?file_name=" + $fileName;
                    $('#download-import-file-link').attr('href', $url);
                    var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
                    $('#download-failed-import-file-link').attr('href', $urlFailRecords);
                }
            }
        });


        $('.search-btn').click(function () {

            var ajax = "{{route('admin.residential.imported-file-data')}}?" + $("#search-filter").serialize();
            zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [10]);

            $.ajax({
                url: ajax,
                type: "GET",
                success: function (result) {
                    if (result.recordsTotal > 0) {
                        $('#delete_file').css('display', 'inline-block');
                        $('#download_csv').css('display', 'inline-block');
                        $('#download_failed_csv').css('display', 'inline-block');
                        var $fileName = $('#file_name').val();
                        var $url = "{{ route('admin.residential.download-imported-file') }}?file_name=" + $fileName;
                        $('#download-import-file-link').attr('href', $url);
                        var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
                        $('#download-failed-import-file-link').attr('href', $urlFailRecords);
                    }
                }
            });

            count++;

        });

        $('#delete_file').click(function () {

            var importedFileName = $('#file_name').val();

            $.ajax({
                url: "{{ route('admin.residential.delete-imported-file') }}?" + $("#search-filter").serialize(),
                type: "GET",
            });
        });

        $('#file_name').keyup(function () {
            var $fileName = $('#file_name').val();
            var $url = "{{ route('admin.residential.download-imported-file') }}?file_name=" + $fileName;
            $('#download-import-file-link').attr('href', $url);
            var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
            $('#download-failed-import-file-link').attr('href', $urlFailRecords);
        });

    </script>
@endpush
