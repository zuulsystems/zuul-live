@extends('admin.layout.master')
@section('content')
    <style>
        .table-responsive, .dataTables_scrollBody {

            min-height: 257px;
        }

        #residentsTable .dropdown-menu.show {
        }

        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }
    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>Residents List</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Residents List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_name">Name</label>
                                                <input type="text" name="full_name" id="full_name" placeholder="Name"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_phone">Phone Number</label>
                                                <input type="text" name="formatted_phone" id="formatted_phone"
                                                       placeholder="Phone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="community_name">Community</label>
                                                <input type="text" name="community_name" id="community_name"
                                                       placeholder="Community" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="house_name">House</label>
                                                <input type="text" name="house_name" id="house_name"
                                                       placeholder="House Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="full_address">Address</label>
                                                <input type="text" name="full_address" id="full_address"
                                                       placeholder="Address" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="special_instruction">Special Instructions</label>
                                                <input type="text" name="special_instruction" id="special_instruction"
                                                       placeholder="Special Instructions" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="is_suspended_select">Status</label>
                                                <select name="is_suspended_select" id="is_suspended_select"
                                                        class="form-control">
                                                    <option value="all">All</option>
                                                    <option value="1">Suspended</option>
                                                    <option value="2">Unsuspended</option>

                                                </select>

                                                <input type="hidden" name="is_suspended" value="all" id="is_suspended">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="complete_profile">Completed Profile</label>
                                                <select class="form-control" name="complete_profile">
                                                    <option value="all">All</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            @cannot('is-guard-admin')
                                <h3 class="card-title">
                                    <a href="{{route('admin.residential.residents.create')}}" class="add_advertiser">
                                        <i class="fas fa-plus-circle"></i> Add Resident
                                    </a>
                                    <a href="javascript:void(0)" class="" data-toggle="modal"
                                       data-target="#resident-import-modal">
                                        <i class="fas fa-file-import"></i> import Resident
                                    </a>
                                    <a href="{{ route('admin.residential.imported-files') }}" class="">
                                        <i class="fas fa-file"></i> Imported Files
                                    </a>
                                </h3>
                            @endcannot
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="residentsTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Community</th>
                                    <th>House</th>
                                    <th>Special Instructions</th>
                                    <th>Created At</th>
                                    <th>Completed Profile</th>
                                    @php
                                        if(empty($_GET['userId']))
                                        {
                                    @endphp
                                    <th>Action(s)</th>
                                    @php
                                        }
                                    @endphp
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Permission Model--->
    <div class="modal fade" id="resident-permission-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Resident Permissions</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.residential.assign-resident-permission')}}"
                      class="ajax-form" id="guard-permission-form">
                    <input type="hidden" name="user_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="can_manage_family">Can Manage Household </label>
                                    <input type="checkbox" name="can_manage_family" id="can_manage_family" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="can_send_passes">Can Send Passes</label>
                                    <input type="checkbox" name="can_send_passes" id="can_send_passes" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_parental_control">Allow Parental Control </label>
                                    <input type="checkbox" name="allow_parental_control" id="allow_parental_control"
                                           value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="is_license_locked">Is License Locked</label>
                                    <input type="checkbox" name="is_license_locked" id="is_license_locked" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Permission Model--->
    <div class="modal fade" id="resident-permission-modal-list">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Pre-Approved Guest Log List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <table id="residentsTableGuestLog" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Action(s)</th>
                    </tr>
                    </thead>
                    <tbody id="tbody">
                    </tbody>
                </table>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    @php
        $showClass = '';
      $residentsImportModel = '';
      if ($errors->any())
      {
      $showClass = 'show';
      $residentsImportModel = 'display: block';
      }
    @endphp

        <!-- Resident Import Model -->
    <div class="modal fade" id="resident-import-modal">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import CSV</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            id="closeResidentsImportModalBtn">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{ route('admin.residential.import-residents') }}"
                      enctype="multipart/form-data" class="ajax-form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="house_csv">CSV file to import</label>
                                    <input type="file" name="resident_csv" id="resident_csv" class="col-md-12"
                                           accept=".csv"/>
                                    @error('rfid_csv')
                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                        </span>
                                    @enderror
                                    @if ($errors->has('resident_csv'))
                                        <span style="color:red;padding-top:2px"
                                              class="error a">{{ $errors->first('resident_csv') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if(auth()->user()->role_id == '1')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach ($communities as $community)
                                                <option value="{{ $community->id }}">{{ $community->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('community_id'))
                                            <span style="color:red;padding-top:2px"
                                                  class="error a">Select a community.</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer justify-content-between">
                        <a href="{{ asset('assets/csv-sample/resident.csv') }}" class="btn btn-default">Download Sample
                            File</a>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Revoke Model--->
    <div class="modal fade" id="revoke-permission-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Revoke Permission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.residential.revoke-resident')}}"
                      class="ajax-form">
                    <input type="hidden" name="user_id" id="revoke_user_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="revoke_residential_right">Revoke Residential Rights </label>
                                    <select class="form-control">
                                        <option>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="checkbox" name="remove_from_family" class="control-input"
                                           id="remove_from_family">
                                    <label class="control-label" for="remove_from_family">&nbsp;&nbsp;CONFIRM: Remove
                                        from household members as well</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="suspend-resident-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="suspendTitle">Suspend Resident</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.residential.suspend-resident')}}"
                      class="ajax-form">
                    <input type="hidden" name="user_id" id="user_id" value="">
                    <input type="hidden" name="suspend" id="suspend">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="suspend-user" id="suspend-user"></label>
                                    <select class="form-control">
                                        <option>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- The Additional Contact Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Additional Phone Numbers</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Phone Number</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody id="additional_contact_row">
                        </tbody>
                    </table>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    </div>

@endsection
@push('scripts')
    <script>


        $(document).on('click', '.additional_info', function () {
            document.getElementById("additional_contact_row").innerHTML = "";
            showLoader();
            let id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "{{route('admin.residential.get-resident-contacts')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    var counter = 1;
                    if (result.status === true) {
                        $.each(result.user, function (key, value) {
                            $("#additional_contact_row").append(`<tr><td>${counter}</td><td>${'+1 (' + value['contact'][0] + value['contact'][1] + value['contact'][2] + ') ' + value['contact'][3] + value['contact'][4] + value['contact'][5] + '-' + value['contact'][6] + value['contact'][7] + value['contact'][8] + value['contact'][9]}</td><td>${value['type']}</td></tr>`);
                            counter = counter + 1;
                        });
                        console.log(result);
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        function myFunction(item, index, arr) {
            $('#tbody').append(`
                <tr>
                    <td>${item.id}</td>
                    <td>${item.contact_name}</td>
                    <td>${item.formatted_phone_number}</td>
                    <td> <a onclick="sendQuickPass('${item.created_by}', '${item.contact_name}', '${item.formatted_phone_number}')"><i class="fa fa-paper-plane" ></i> </a></td>
                </tr>
            `);
        }

        function sendQuickPass(id, contact_name, phone_number) {
            $.ajax({
                type: "post",
                url: "{{ url('create-quick-pass') }}",
                data: {
                    resident_id: id,
                    display_name: contact_name,
                    phone_number: phone_number,
                    visitor_type: 'friends_family'
                },
                success: function (data) {
                    notifySuccess('Successfully Send Quick Pass');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }

        //resident permission model
        $(document).on('click', '.guard-permission-model', function () {
            let id = $(this).data('id');
            $("#can_manage_family").bootstrapSwitch('state', false);
            $("#can_send_passes").bootstrapSwitch('state', false);
            $("#allow_parental_control").bootstrapSwitch('state', false);
            $("#is_license_locked").bootstrapSwitch('state', false);
            showLoader();
            $.ajax({
                url: "{{route('admin.residential.check-resident-permission')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let permission = result.permission;
                        $('input[name="user_id"]').val(id);
                        if (permission.canManageFamily === true) {
                            $("#can_manage_family").bootstrapSwitch('state', true);
                        }
                        if (permission.canSendPasses === true) {
                            $("#can_send_passes").bootstrapSwitch('state', true);
                        }
                        if (permission.allowParentalControl === true) {
                            $("#allow_parental_control").bootstrapSwitch('state', true);
                        }
                        if (permission.isLicenseLocked === true) {
                            $("#is_license_locked").bootstrapSwitch('state', true);
                        }
                        $('#resident-permission-modal').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });


        $(document).on('click', '.revoke-permission-model', function () {
            let id = $(this).data('id');
            $('#revoke_user_id').val(id);
            showLoader();
            $.ajax({
                url: "{{route('admin.residential.get-resident')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        $('#revoke-permission-model').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        //suspend resident
        $(document).on('click', '.suspend-resident-model', function () {
            let id = $(this).data('id');
            $.ajax({
                url: "{{route('admin.residential.get-resident')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let user = result.user;
                        $('input[name="user_id"]').val(id);

                        var suspendTitle;
                        var suspendMessage;

                        if (user.is_suspended == '1') {
                            suspendTitle = "Unsuspend Resident";
                            suspendMessage = "Are you sure you want to unsuspend this user into the database?"

                        } else if (user.is_suspended == '0') {
                            suspendTitle = "Suspend Resident";
                            suspendMessage = "Are you sure you want to suspend this user into the database?";
                        }

                        $('#suspend').val(user.is_suspended);
                        $('#suspendTitle').html(suspendTitle);
                        $('#suspend-user').html(suspendMessage);
                        $('#suspend-resident-model').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
            $('#suspend-resident-model').modal('show');
        });

        let columns = [
            {
                data: 'id',
            },
            {
                data: 'full_name',
                name: 'first_name'
            },
            {
                data: 'formatted_phone',
                name: "phone_number"
            },
            {
                data: 'email',
            },
            {
                data: 'house_detail'
            },
            {


                data: function (data) {

                    if (data.is_suspended == '0') {
                        suspendTitle = "Unsuspended";

                    } else if (data.is_suspended == '1') {
                        suspendTitle = "Suspended";

                    }
                    return suspendTitle;
                },
                name: 'is_suspended',
            },
            {
                data: 'role_name',
                name: 'role_id'
            },
            {
                data: 'name'
            },
            {
                data: 'house_name'
            },
            {
                data: 'special_instruction',
            },
            {
                data: 'created_at',
            },
            {
                data: 'is_completed_profile',
                name: 'license_image'
            }
        ];

        var userId = "{{ Request::get('userId') }}";

        var disAbleColumnSort = [];

        if (userId == "") {
            var actions = {
                data: function (data) {
                    return data.links;
                }
            };
            columns.push(actions);
            disAbleColumnSort = [12];
        } else {
        }

        var isGuard = "{{ Gate::allows('is-guard-admin') }}";

        if (isGuard == false) {

            jQuery('#is_suspended_select').change(function () {

                $("#is_suspended").val(this.value);
            });

            zuulDataTableSortableGuestLogs('#residentsTable', "{{ route('admin.residential.resident-data') }}?houseId={{$houseId ?? null}}&userId={{$userId ?? null}}&phoneNumber={{$phoneNumber ?? null}}", columns, disAbleColumnSort); // 11 for admin, 10 for guard


            $('.search-btn').click(function () {
                var ajax = "{{route('admin.residential.resident-data')}}?houseId={{$houseId ?? null}}&userId={{$userId ?? null}}&phoneNumber={{$phoneNumber ?? null}}&" + $("#search-filter").serialize();
                zuulDataTableSortableGuestLogs('#residentsTable', ajax, columns, disAbleColumnSort);


            })

        } else {


            jQuery('#is_suspended_select').change(function () {

                $("#is_suspended").val(this.value);
            });
            zuulDataTableSortableGuestLogs('#residentsTable', "{{ route('admin.residential.resident-data') }}?houseId={{$houseId ?? null}}&userId={{$userId ?? null}}&phoneNumber={{$phoneNumber ?? null}}", columns, disAbleColumnSort); // 11 for admin, 10 for guard

            $('.search-btn').click(function () {
                var ajax = "{{route('admin.residential.resident-data')}}?houseId={{$houseId ?? null}}&userId={{$userId ?? null}}&phoneNumber={{$phoneNumber ?? null}}&" + $("#search-filter").serialize();
                zuulDataTableSortableGuestLogs('#residentsTable', ajax, columns, disAbleColumnSort);

            })
        }

        $('#closeResidentsImportModalBtn').click(function () {
            $("#resident-import-modal").css("display", "none");
            $('.a').css('display', 'none');
        });


    </script>
@endpush
