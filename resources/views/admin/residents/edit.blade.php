@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Resident</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Resident</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.residents.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Residents List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.residential.residents.update', [$resident->id])}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first_name">First Name </label>
                                                <input type="text" name="first_name" id="first_name"
                                                       placeholder="First Name" class="form-control"
                                                       value="{{$resident->first_name}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="last_name">Last Name </label>
                                                <input type="text" name="last_name" id="last_name"
                                                       placeholder="Last Name" class="form-control"
                                                       value="{{$resident->last_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Email </label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control" value="{{$resident->email}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label for="dial_code">Country code</label>
                                                @php
                                                    $url = route('get-phone-format-by-dial-code');
                                                @endphp
                                                <select
                                                    id="dial_code" name="dial_code" class="form-control select2bs4"
                                                    onchange="changePhoneDigitsFormat1('{{ $url }}', true)">
                                                    @foreach($countries as $country)
                                                        <option value="+{{$country->dial_code}}"
                                                                @if($country->dial_code == $resident->dial_code)selected="true"@endif>{{$country->country_name}}
                                                            +{{$country->dial_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <div
                                                    style=" font-size: 12px;display: inline;color: #1f2d3d;padding: 0 5px;border-radius: 5px;cursor: default;border: 1px solid black;margin-left: 5px"
                                                    id="add-new-row">Add Additional Phone Number(s)<i
                                                        class="fa fa-plus-circle"
                                                        style="margin-left: 10px"></i>
                                                </div>
                                                <div id="remove-field"
                                                     style=" font-size: 12px;display: none;color: red;padding: 0 5px;border-radius: 5px;cursor: default;border: 1px solid red;margin-left:5px"
                                                     id="add-new-row">Remove field<i class="fa fa-times"
                                                                                     style="margin-left: 10px"></i>
                                                </div>
                                                <input type="text" name="phone_number" id="phone_number"
                                                       placeholder="Phone Number" class="form-control mask-phone"
                                                       value="{{$resident->phone_number}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container-fluid" id="additional_phone_number" style="display: none">

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="community_id">Community Assigned</label>
                                                <select
                                                    id="community_id" name="community_id"
                                                    class="form-control select2bs4">
                                                    <option value="">Select Community</option>
                                                    @foreach($communities as $community)
                                                        <option value="{{$community->id}}"
                                                                @if($community->id == $resident->community_id)selected="true"@endif>{{$community->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="house_id">House Assigned</label>
                                                <div style="display: inline-block; margin-left: 7px;">
                                                    <input type="radio" id="houseAddress">
                                                    <span> By Address </span>
                                                </div>
                                                <select id="house_id" name="house_id" class="form-control select2bs4">
                                                    <option value="">Select House</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="is_head_of_family">Is Head Of Family</label>
                                                <select id="is_head_of_family" name="is_head_of_family"
                                                        class="form-control">
                                                    <option value="0" @if($resident->role_id == 5)selected="true"@endif>
                                                        No
                                                    </option>
                                                    <option value="1" @if($resident->role_id == 4)selected="true"@endif>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="special_instruction">Special Instructions</label>
                                                <textarea name="special_instruction" id="special_instruction"
                                                          class="form-control"
                                                          placeholder="Write Additional Instructions ...">{{$resident->special_instruction}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="profile_image">Profile Image</label>
                                                <input type="file" name="profile_image" id="profile_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Profile Picture.</div>
                                                <img src="{{$resident->profile_image_url}}" style="width: 100px;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="license_image">Profile License</label>
                                                <input type="file" name="license_image" id="license_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Drivers/Student ID.</div>
                                                <img src="{{$resident->license_image_url}}" style="width: 100px;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <span>Save Changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    $('#houseAddress').prop('checked', false);
                    let options = '';
                    if (result.status === true) {
                        $.each(result.result, function (key, row) {
                            if (communityId == "{{ $resident->community_id }}") {
                                var selected = '';
                                if (row.id == "{{ $resident->house_id }}") {
                                    selected = 'selected="true"'
                                }
                            }
                            options += `<option value="${row.id}" ${selected}>${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                }
            });
        });

        $(document).ready(function () {
            $('#community_id').change();
        })

        $(document).ready(function () {
            $('#houseAddress').click(function () {

                var $communityId = $('#community_id').val();

                if ($communityId == "") {
                    alert('Please select community');
                    $(this).prop('checked', false);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route("admin.residential.house-by-address") }}",
                        data: {community_id: $communityId},
                        success: function (house) {

                            if (house.length > 0) {
                                var houseHTML = '';

                                for (a = 0; a < house.length; a++) {
                                    houseHTML += "<option value='" + house[a].id + "'>" + house[a].house_detail + "</option>";
                                }

                                $('#house_id').html(houseHTML);
                            } else {
                                var houseHTML = "<option value=''>No House Found !</option>";
                                $('#house_id').html(houseHTML);
                            }
                        }
                    });
                }

            });

            var $phoneFormat = "{{ $phoneFormat }}";

            var resident_id = '<?php echo $resident_id;?>'
            var click_count = 0;
            var counter = 0;
            console.log(resident_id);
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-resident-contacts')}}",
                data: {
                    id: resident_id
                },
                success: function (result) {
                    hideLoader();
                    let options = '';
                    if (result.status === true) {
                        $.each(result['user'], function (key, row) {
                            console.log(row)
                            counter = counter + 1;
                            click_count = click_count + 1;
                            document.getElementById("remove-field").style.display = "inline";

                            if (counter == 2) {
                                document.getElementById("add-new-row").style.display = 'none';
                            }

                            options += ` <div id="additional_phone_number${counter}" class="row">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-7">
                                        <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="additional_phone_dial${counter}">Dial Code</label>
                                                <select name="additional_phone_dial${counter}" class="form-control"  onchange="changePhoneDigitsFormatofAdditionalPhone(this.value,'{{ $url }}','addditional_phone_number${counter}')">
                                                    <option disabled selected>+${row['dial_code']}</option>
                                                     @foreach($countries as $country)
                            <option value="{{$country->dial_code}}">+{{$country->dial_code}}</option>
                                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="additional_phone_number${counter}">Additional Phone Number ${counter}</label>
                                                <input type="text" class="form-control additional_phone_number${counter}" name="additional_phone_number${counter}" value="${row['contact']}" id="addditional_phone_number${counter}">
                                                <input type="hidden" class="form-control" name="additional_phone_id${counter}" value="${row['id']}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="additional_phone_number${counter}_type">Type</label>
                                                <select name="additional_phone_number${counter}_type" class="form-control">
                                                    <option disabled selected>${row['type']}</option>
                                                    <option value="Main">Main</option>
                                                    <option value="Home">Home</option>
                                                    <option value="Office">Office</option>
                                                    <option value="Work">Work</option>
                                                    <option value="Friend">Friend</option>
                                                    <option value="Parents">Parents</option>
                                                    <option value="Colleague">Colleague</option>
                                                    <option value="Relative">Relative</option>
                                                    <option value="Siblings">Siblings</option>
                                                    <option value="School/College">School/College</option>
                                                </select>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>`;


                            document.getElementById("additional_phone_number").style.display = null;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#additional_phone_number').html(options);
                },
                complete: function () {

                    var $a = "{{ count($additionalPhoneNumbersFormats) }}";
                    $a = parseInt($a);
                    if ($a > 0) {
                        if ($a == 1) {
                            var $additionalPhoneNumberPhoneFormat1 = "{{ $additionalPhoneNumbersFormats[0] ?? "" }}";
                            $('.additional_phone_number1').inputmask({mask: $additionalPhoneNumberPhoneFormat1});
                        } else if ($a == 2) {
                            // alert('here');
                            var $additionalPhoneNumberPhoneFormat1 = "{{ $additionalPhoneNumbersFormats[0] ?? ""}}";
                            var $additionalPhoneNumberPhoneFormat2 = "{{ $additionalPhoneNumbersFormats[1] ?? ""}}";

                            $('.additional_phone_number1').inputmask({mask: $additionalPhoneNumberPhoneFormat1});
                            $('.additional_phone_number2').inputmask({mask: $additionalPhoneNumberPhoneFormat2});
                        }
                    }

                }
            });

            $('#add-new-row').click(function () {
                document.getElementById("remove-field").style.display = "inline";
                click_count = click_count + 1;
                console.log("Add on count =>> ", click_count);
                if (click_count < 3) {
                    var options = '';
                    counter = counter + 1;
                    console.log(counter);

                    options += ` <div id="additional_phone_number${counter}" class="row">
                                        <div class="col-md-5">
                                        </div>
                                        <div class="col-md-7">
                                        <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="additional_phone_dial${counter}">Dial Code</label>
                                                <select name="additional_phone_dial${counter}" class="form-control" onchange="changePhoneDigitsFormatofAdditionalPhone(this.value,'{{ $url }}','addditional_phone_number${counter}')">
                                                     @foreach($countries as $country)
                    <option value="{{$country->dial_code}}">+{{$country->dial_code}}</option>
                                                                            @endforeach
                    </select>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="additional_phone_number${counter}">Additional Phone Number ${counter}</label>
                                                <input type="hidden" class="form-control additional_phone_id${counter}" name="additional_phone_id${counter}" >
                                                <input type="text" class="form-control additional_phone_number${counter}" id="addditional_phone_number${counter}" name="additional_phone_number${counter}">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="additional_phone_number${counter}_type">Type</label>
                                                <select name="additional_phone_number${counter}_type" class="form-control">
                                                    <option disabled selected>Select</option>
                                                    <option value="Main">Main</option>
                                                    <option value="Home">Home</option>
                                                    <option value="Office">Office</option>
                                                    <option value="Work">Work</option>
                                                    <option value="Friend">Friend</option>
                                                    <option value="Parents">Parents</option>
                                                    <option value="Colleague">Colleague</option>
                                                    <option value="Relative">Relative</option>
                                                    <option value="Siblings">Siblings</option>
                                                    <option value="School/College">School/College</option>
                                                </select>
                                            </div>
                                        </div></div></div>`;


                    document.getElementById("additional_phone_number").style.display = null;

                    var id = '#additional_phone_number';

                    if (counter < 3) {
                        $(id).append(options);
                    }


                    if (click_count == 2) {
                        document.getElementById("add-new-row").style.display = 'none';
                    }


                }

                var $url = "{{ route('get-phone-format-by-dial-code') }}"
                changePhoneDigitsFormat1($url, true);

            });

            $('#remove-field').click(function () {

                counter = counter - 1;
                document.getElementById("add-new-row").style.display = 'inline';
                var element_name = "additional_phone_number" + click_count;
                var element_id = "additional_phone_id" + click_count;
                var ex_class = '.' + element_name;
                var ex_class2 = '.' + element_id;
                $(ex_class).val('');
                $(ex_class2).val('');
                console.log("Should be removed=>> ", element_name);

                document.getElementById(element_name).style.display = 'none';
                click_count = click_count - 1;

                if (click_count == 0) {
                    document.getElementById("remove-field").style.display = "none";
                }
            });

            var $phoneFormat = "{{ $phoneFormat }}";
            $('#phone_number').inputmask({mask: $phoneFormat});

        });
    </script>
@endpush
