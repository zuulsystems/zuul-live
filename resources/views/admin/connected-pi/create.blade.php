@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Connected Pi</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Connected Pi</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.connected-pi.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Connected Pi List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.connected-pi.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="mac_address">MAC Address </label>
                                        <input type="text" name="mac_address" id="mac_address"
                                               placeholder="MAC Address" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Name </label>
                                        <input class="form-control" name="name" id="name" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="dynamic_url">Dynamic URL</label>
                                        <input type="text" name="dynamic_url" id="dynamic_url"
                                               placeholder="Dynamic URL" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="uid">UID</label>
                                        <input type="text" class="form-control" name="uid"
                                               id="uid" placeholder="UID"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select
                                            id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach($communities as $community)
                                                <option value="{{$community->id}}">{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="camera_email">Camera Email</label>
                                        <input type="text" class="form-control" name="camera_email"
                                               id="camera_email" placeholder="Camera Email"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="camera_password">Camera Password</label>
                                        <input type="text" class="form-control" name="camera_password"
                                               id="camera_password" placeholder="Camera Password"/>
                                    </div>

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
