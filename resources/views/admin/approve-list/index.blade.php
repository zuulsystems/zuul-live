@extends('admin.layout.master')
@section('content')
    <style>
        #residentsTable .dropdown-menu.show {
        }
    </style>
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Pre Approve List
                            [{{ $residentData->first_name . " " . $residentData->last_name . " - " . formattedPhone($residentData->phone_number) }}
                            ]</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Residents List</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>


        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.approve-list-byid', $idFromURL)}}"
                                   class="add_advertiser">
                                    <i class="fas fa-plus-circle"></i> Add List
                                </a>

                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="residentsTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone No</th>
                                    <th>Dial Code</th>
                                    <th>Action(s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Permission Model--->
    <div class="modal fade" id="resident-permission-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Resident Permissions</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.residential.assign-resident-permission')}}"
                      class="ajax-form" id="guard-permission-form">
                    <input type="hidden" name="user_id">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="can_manage_family">Can Manage Household </label>
                                    <input type="checkbox" name="can_manage_family" id="can_manage_family" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="can_send_passes">Can Send Passes</label>
                                    <input type="checkbox" name="can_send_passes" id="can_send_passes" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_parental_control">Allow Parental Control </label>
                                    <input type="checkbox" name="allow_parental_control" id="allow_parental_control"
                                           value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="is_license_locked">Is License Locked</label>
                                    <input type="checkbox" name="is_license_locked" id="is_license_locked" value="1"
                                           data-bootstrap-switch
                                           data-off-color="danger" data-on-color="success" data-on-text="Yes"
                                           data-off-text="No">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Permission Model--->
    <div class="modal fade" id="resident-permission-modal-list">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Pre-Approved Guest Log List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <table id="residentsTableGuestLog" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Action(s)</th>
                    </tr>
                    </thead>
                    <tbody id="tbody">
                    </tbody>
                </table>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    @php
        $showClass = '';
      $residentsImportModel = '';
      if ($errors->any())
      {
      $showClass = 'show';
      $residentsImportModel = 'display: block';
      }
    @endphp

        <!-- Resident Import Model -->
    <div class="modal fade" id="resident-import-modal">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Import CSV</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            id="closeResidentsImportModalBtn">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{ route('admin.residential.import-residents') }}"
                      enctype="multipart/form-data" class="ajax-form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="house_csv">CSV file to import</label>
                                    <input type="file" name="resident_csv" id="resident_csv" class="col-md-12"
                                           accept=".csv"/>
                                    @error('rfid_csv')
                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                        </span>
                                    @enderror
                                    @if ($errors->has('resident_csv'))
                                        <span style="color:red;padding-top:2px"
                                              class="error a">{{ $errors->first('resident_csv') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if(auth()->user()->role_id == '1')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="community_id">Community</label>
                                        <select id="community_id" name="community_id" class="form-control select2bs4">
                                            <option value="">Select Community</option>
                                            @foreach ($communities as $community)
                                                <option value="{{ $community->id }}">{{ $community->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('community_id'))
                                            <span style="color:red;padding-top:2px"
                                                  class="error a">Select a community.</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer justify-content-between">
                        <a href="{{ asset('assets/csv-sample/resident.csv') }}" class="btn btn-default">Download Sample
                            File</a>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Revoke Model--->
    <div class="modal fade" id="revoke-permission-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Revoke Permission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.residential.revoke-resident')}}"
                      class="ajax-form">
                    <input type="hidden" name="user_id" id="revoke_user_id" value="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="revoke_residential_right">Revoke Residential Rights </label>
                                    <select class="form-control">
                                        <option>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="checkbox" name="remove_from_family" class="control-input"
                                           id="remove_from_family">
                                    <label class="control-label" for="remove_from_family">&nbsp;&nbsp;CONFIRM: Remove
                                        from household members as well</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="suspend-resident-model">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="suspendTitle">Suspend Resident</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" method="post" action="{{route('admin.residential.suspend-resident')}}"
                      class="ajax-form">
                    <input type="hidden" name="user_id" id="user_id" value="">
                    <input type="hidden" name="suspend" id="suspend">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="suspend-user" id="suspend-user"></label>
                                    <select class="form-control">
                                        <option>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@push('scripts')
    <script>
        //resident permission model

        $(document).on('click', '.guard-permission-model-list', function () {
            let id = $(this).data('id');
            showLoader();
            $.ajax({


                url: `{{URL('get-contact-in-dnc-by-resident-id')}}/${id}`,
                type: "GET",
                success: function (result) {
                    hideLoader();
                    if (result.bool === true) {
                        $('input[name="user_id"]').val(id);

                        $('#tbody').html(``);

                        result.result.list.forEach(myFunction);


                        $('#resident-permission-modal-list').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        function myFunction(item, index, arr) {
            $('#tbody').append(`
                <tr>
                    <td>${item.id}</td>
                    <td>${item.contact_name}</td>
                    <td>${item.formatted_phone_number}</td>
                    <td> <a onclick="sendQuickPass('${item.created_by}', '${item.contact_name}', '${item.formatted_phone_number}')"><i class="fa fa-paper-plane" ></i> </a></td>
                </tr>
            `);
        }

        function sendQuickPass(id, contact_name, phone_number) {
            $.ajax({
                type: "post",
                url: "{{ url('create-quick-pass') }}",
                data: {
                    resident_id: id,
                    display_name: contact_name,
                    phone_number: phone_number,
                    visitor_type: 'friends_family'
                },
                success: function (data) {
                    notifySuccess('Successfully Send Quick Pass');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }

        //resident permission model
        $(document).on('click', '.guard-permission-model', function () {
            let id = $(this).data('id');
            $("#can_manage_family").bootstrapSwitch('state', false);
            $("#can_send_passes").bootstrapSwitch('state', false);
            $("#allow_parental_control").bootstrapSwitch('state', false);
            $("#is_license_locked").bootstrapSwitch('state', false);
            showLoader();
            $.ajax({
                url: "{{route('admin.residential.check-resident-permission')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let permission = result.permission;
                        $('input[name="user_id"]').val(id);
                        if (permission.canManageFamily === true) {
                            $("#can_manage_family").bootstrapSwitch('state', true);
                        }
                        if (permission.canSendPasses === true) {
                            $("#can_send_passes").bootstrapSwitch('state', true);
                        }
                        if (permission.allowParentalControl === true) {
                            $("#allow_parental_control").bootstrapSwitch('state', true);
                        }
                        if (permission.isLicenseLocked === true) {
                            $("#is_license_locked").bootstrapSwitch('state', true);
                        }
                        $('#resident-permission-modal').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });


        $(document).on('click', '.revoke-permission-model', function () {
            let id = $(this).data('id');
            $('#revoke_user_id').val(id);
            showLoader();
            $.ajax({
                url: "{{route('admin.residential.get-resident')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        $('#revoke-permission-model').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
        });

        //suspend resident
        $(document).on('click', '.suspend-resident-model', function () {
            let id = $(this).data('id');
            $.ajax({
                url: "{{route('admin.residential.get-resident')}}",
                type: "POST",
                data: {
                    id: id
                },
                success: function (result) {
                    hideLoader();
                    if (result.status === true) {
                        let user = result.user;
                        $('input[name="user_id"]').val(id);

                        var suspendTitle;
                        var suspendMessage;

                        if (user.is_suspended == '1') {
                            suspendTitle = "Unsuspend Resident";
                            suspendMessage = "Are you sure you want to unsuspend this user into the database?"
                        } else if (user.is_suspended == '0') {
                            suspendTitle = "Suspend Resident";
                            suspendMessage = "Are you sure you want to suspend this user into the database?";
                        }

                        $('#suspend').val(user.is_suspended);
                        $('#suspendTitle').html(suspendTitle);
                        $('#suspend-user').html(suspendMessage);
                        $('#suspend-resident-model').modal('show');
                    } else {
                        notifyError(result.message);
                    }
                }
            });
            $('#suspend-resident-model').modal('show');
        });

        let columns = [
            {
                data: 'id',
            },
            {
                data: 'contact_name',
            },
            {
                data: 'formatted_phone_number',
            },
            {
                data: 'dial_code',
            },
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];
        var isGuard = "{{ Gate::allows('is-guard-admin') }}";

        function zuulDataTableSortableSearch(selector, url, columns, disableColumnSearch = [], DefaultOrderColumn = 0) {

            $(selector).DataTable({
                "processing": true,
                "scrollX": true,
                "serverSide": true,
                "ajax": url,
                "columns": columns,
                "pageLength": 50,
                "destroy": true,
                "searching": true,
                "order": [[DefaultOrderColumn, "desc"]],
                "columnDefs": [
                    {"orderable": false, "targets": disableColumnSearch}
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
        }

        zuulDataTableSortableSearch('#residentsTable', "{{ route('admin.residential.approved-data', ["$idFromURL"]) }}?houseId={{$houseId ?? null}}&userId={{$userId ?? null}}&phoneNumber={{$phoneNumber ?? null}}", columns, [4]); // 11 for admin, 10 for guard

        $('#closeResidentsImportModalBtn').click(function () {
            $("#resident-import-modal").css("display", "none");
            $('.a').css('display', 'none');
        });

        function getUserInfo(contact_name) {
            $.ajax({
                type: "post",
                url: "{{ url('create-quick-pass') }}",
                data: contact_name,
                dataType: "json",
                success: function (response) {
                    if (response.bool) {
                        notifySuccess(response.message);
                    } else {
                        notifyError(result.message);
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });
        }

    </script>
@endpush
