@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Imported Files Information</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Imported Files Information</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="name">File Name</label>
                                                <input type="text" name="file_name" id="file_name"
                                                       placeholder="File Name" class="form-control"
                                                       value="{{ $fileName }}">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="first_name">First Name</label>
                                                <input type="text" name="first_name" id="first_name"
                                                       placeholder="First Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input type="text" name="last_name" id="last_name"
                                                       placeholder="Last Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="formatted_phone">Phone Number</label>
                                                <input type="text" name="formatted_phone" id="formatted_phone"
                                                       placeholder="Phone Number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="house_name">House Name</label>
                                                <input type="text" name="house_name" id="house_name"
                                                       placeholder="House Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="head_of_family">Head of Family</label>
                                                <input type="text" name="head_of_family" id="head_of_family"
                                                       placeholder="Head of family" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="special_instructions">Special Instructions</label>
                                                <input type="text" name="special_instructions" id="special_instructions"
                                                       placeholder="Special Instructions" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="temporary_password">Temporary Password</label>
                                                <input type="text" name="temporary_password" id="temporary_password"
                                                       placeholder="Temporary Password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="">Select Status</option>
                                                    <option value="Success">Success</option>
                                                    <option value="Failure">Failure</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <input type="text" name="message" id="message" placeholder="Message"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                                <input type="hidden" id="importedFileName">
                                <button class="btn btn-danger" id="delete_file" style="display: none">Delete File
                                </button>
                                <button class="btn btn-info" id="download_csv" style="display: none"><a
                                        style="color: white" href="" id="download-import-file-link">Download CSV</a>
                                </button>
                                <button class="btn btn-warning" id="download_failed_csv" style="display: none"><a
                                        style="color: white" href="" id="download-failed-import-file-link">Download
                                        Failed Records CSV</a></button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <table id="importedFileDataTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>House Name</th>
                                    <th>Head of family</th>
                                    <th>Special Instructions</th>
                                    <th>Temporary Password</th>
                                    <th>Status</th>
                                    <th>Message</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        var count = 0;

        function zuulImportFileDataTableSortable(selector, url, columns, disableColumnSearch = [], DefaultOrderColumn = 0) {

            $(selector).DataTable({
                "processing": true,
                "scrollX": true,
                "serverSide": true,
                "ajax": url,
                "columns": columns,
                "pageLength": 50,
                "destroy": true,
                "searching": false,
                "order": [[DefaultOrderColumn, "desc"]],
                "columnDefs": [
                    {"orderable": false, "targets": disableColumnSearch}
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "initComplete": function () {
                    var api = this.api();

                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" placeholder="' + title + '" />');

                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();

                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})';

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);
                                });
                        });
                },

            });
        }

        let columns = [
            {data: 'first_name'},
            {data: 'last_name'},
            {data: 'email'},
            {data: 'phone_number'},
            {data: 'house_name'},
            {data: 'head_of_family'},
            {data: 'special_instructions'},
            {data: 'temporary_password'},
            {data: 'status'},
            {data: 'message'},
            {
                data: function (data) {
                    return data.links;
                }
            },
        ];

        var ajax = "{{route('admin.residential.imported-file-data')}}?file_name={{$fileName ?? ''}}";
        zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [10]);

        $.ajax({
            url: ajax,
            type: "GET",
            success: function (result) {
                if (result.recordsTotal > 0) {
                    $('#delete_file').css('display', 'inline-block');
                    $('#download_csv').css('display', 'inline-block');
                    $('#download_failed_csv').css('display', 'inline-block');
                    var $fileName = $('#file_name').val();
                    var $url = "{{ route('admin.residential.download-imported-file') }}?file_name=" + $fileName;
                    $('#download-import-file-link').attr('href', $url);
                    var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
                    $('#download-failed-import-file-link').attr('href', $urlFailRecords);
                }
            }
        });


        $('.search-btn').click(function () {

            var ajax = "{{route('admin.residential.imported-file-data')}}?" + $("#search-filter").serialize();
            zuulImportFileDataTableSortable('#importedFileDataTable', ajax, columns, [10]);

            $.ajax({
                url: ajax,
                type: "GET",
                success: function (result) {
                    if (result.recordsTotal > 0) {
                        $('#delete_file').css('display', 'inline-block');
                        $('#download_csv').css('display', 'inline-block');
                        $('#download_failed_csv').css('display', 'inline-block');
                        var $fileName = $('#file_name').val();
                        var $url = "{{ route('admin.residential.download-imported-file') }}?file_name=" + $fileName;
                        $('#download-import-file-link').attr('href', $url);
                        var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
                        $('#download-failed-import-file-link').attr('href', $urlFailRecords);
                    }
                }
            });

            count++;

        });

        $('#delete_file').click(function () {

            var importedFileName = $('#file_name').val();

            $.ajax({
                url: "{{ route('admin.residential.delete-imported-file') }}?" + $("#search-filter").serialize(),
                type: "GET",
            });
        });

        $('#file_name').keyup(function () {
            var $fileName = $('#file_name').val();
            var $url = "{{ route('admin.residential.download-imported-file') }}?file_name=" + $fileName;
            $('#download-import-file-link').attr('href', $url);
            var $urlFailRecords = "{{ route('admin.residential.download-fail-records-imported-file') }}?file_name=" + $fileName;
            $('#download-failed-import-file-link').attr('href', $urlFailRecords);
        });

    </script>
@endpush
