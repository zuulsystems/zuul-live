@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Pre-Approved</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Add Pre-Approved</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.approve-list.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Pre-Approved List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post" action="{{route('admin.residential.approve-list.store')}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                <input type="hidden" value="{{$formClass}}" name="formClass"/>
                                <input type="hidden" value="{{$resedentId}}" name="resident_id"/>

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first_name">Full Name </label> <span
                                                    class="text-danger">*</span>
                                                <input type="text" name="contact_name" id="contact_name"
                                                       placeholder="Full Name" class="form-control">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label> <span
                                                    style="color:#343a40;">(Optional)</span>
                                                <input type="text" class="form-control" name="phone_number"
                                                       id="phone_number"
                                                       data-inputmask='"mask": "999-999-9999"' data-mask>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <!----> <span>Add </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>


        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();

            showLoader();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                dataType: "json",
                accept: 'application/json',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },

                success: function (result) {

                    hideLoader();
                    $('#houseAddress').prop('checked', false);
                    let options = '';

                    if (result.status === true) {

                        $.each(result.result, function (key, row) {


                            options += `<option value="${row.id}">${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                },
                error: function (errr) {
                    console.log(errr);
                }
            });
        });

        $(document).ready(function () {
            $('#houseAddress').click(function () {

                var $communityId = $('#community_id').val();

                if ($communityId == "") {
                    alert('Please select community');
                    $(this).prop('checked', false);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route("admin.residential.house-by-address") }}",
                        data: {community_id: $communityId},
                        success: function (house) {

                            if (house.length > 0) {
                                var houseHTML = '';

                                for (a = 0; a < house.length; a++) {
                                    houseHTML += "<option value='" + house[a].id + "'>" + house[a].house_detail + "</option>";
                                }

                                $('#house_id').html(houseHTML);
                            } else {
                                var houseHTML = "<option value=''>No House Found !</option>";
                                $('#house_id').html(houseHTML);
                            }
                        }
                    });
                }

            });
        });
    </script>
@endpush
