@extends('admin.layout.master')
@section('content')
    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Pre-Approved</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Edit Pre-Approved</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <a href="{{route('admin.residential.approve-list.index')}}" class="add_advertiser">
                                    <i class="fas fa-list"></i> Residents List
                                </a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form role="form" method="post"
                                  action="{{route('admin.residential.approve-list.update', [$resident->id])}}"
                                  class="ajax-form" enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="first_name">First Name </label>
                                                <input type="text" name="first_name" id="first_name"
                                                       placeholder="First Name" class="form-control"
                                                       value="{{$resident->first_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="last_name">Last Name </label>
                                                <input type="text" name="last_name" id="last_name"
                                                       placeholder="Last Name" class="form-control"
                                                       value="{{$resident->last_name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="email">Email </label>
                                                <input type="text" name="email" id="email" placeholder="Email"
                                                       class="form-control" value="{{$resident->email}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="dial_code">Country code</label>
                                                <select
                                                    id="dial_code" name="dial_code" class="form-control select2bs4">
                                                    @foreach($countries as $country)
                                                        <option value="+{{$country->dial_code}}"
                                                                @if($country->dial_code == $resident->dial_code)selected="true"@endif>{{$country->country_name}}
                                                            +{{$country->dial_code}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" name="phone_number" id="phone_number"
                                                       placeholder="Phone Number" class="form-control mask-phone"
                                                       value="{{$resident->phone_number}}"
                                                       data-inputmask='"mask": "999-999-9999"' data-mask>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="community_id">Community Assigned</label>
                                                <select
                                                    id="community_id" name="community_id"
                                                    class="form-control select2bs4">
                                                    <option value="">Select Community</option>
                                                    @foreach($communities as $community)
                                                        <option value="{{$community->id}}"
                                                                @if($community->id == $resident->community_id)selected="true"@endif>{{$community->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="house_id">House Assigned</label>
                                                <div style="display: inline-block; margin-left: 7px;">
                                                    <input type="radio" id="houseAddress">
                                                    <span> By Address </span>
                                                </div>
                                                <select id="house_id" name="house_id" class="form-control select2bs4">
                                                    <option value="">Select House</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="is_head_of_family">Is Head Of Family</label>
                                                <select id="is_head_of_family" name="is_head_of_family"
                                                        class="form-control">
                                                    <option value="0" @if($resident->role_id == 5)selected="true"@endif>
                                                        No
                                                    </option>
                                                    <option value="1" @if($resident->role_id == 4)selected="true"@endif>
                                                        Yes
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="special_instruction">Special Instructions</label>
                                                <textarea name="special_instruction" id="special_instruction"
                                                          class="form-control"
                                                          placeholder="Write Additional Instructions ...">{{$resident->special_instruction}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="profile_image">Profile Image</label>
                                                <input type="file" name="profile_image" id="profile_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Profile Picture.</div>
                                                <img src="{{$resident->profile_image_url}}" style="width: 100px;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="license_image">Profile License</label>
                                                <input type="file" name="license_image" id="license_image" class=""
                                                       accept='image/*'/>
                                                <div class="help-block">Upload Drivers/Student ID.</div>
                                                <img src="{{$resident->license_image_url}}" style="width: 100px;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        <span>Save Changes </span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>

        /**
         * get houses by community
         */
        $(document).on('change', '#community_id', function () {
            let communityId = $(this).val();
            showLoader();
            $.ajax({
                type: 'POST',
                url: "{{route('admin.residential.get-house-by-community-id')}}",
                data: {
                    communityId: communityId
                },
                success: function (result) {
                    hideLoader();
                    $('#houseAddress').prop('checked', false);
                    let options = '';
                    if (result.status === true) {
                        $.each(result.result, function (key, row) {
                            if (communityId == "{{ $resident->community_id }}") {
                                var selected = '';
                                if (row.id == "{{ $resident->house_id }}") {
                                    selected = 'selected="true"'
                                }
                            }
                            options += `<option value="${row.id}" ${selected}>${row.house_name}</option>`;
                        });
                    } else {
                        options = `<option value="">No Houses Found !</option>`;
                    }
                    $('#house_id').html(options);
                }
            });
        });

        $(document).ready(function () {
            $('#community_id').change();
        })

        $(document).ready(function () {
            $('#houseAddress').click(function () {

                var $communityId = $('#community_id').val();

                if ($communityId == "") {
                    alert('Please select community');
                    $(this).prop('checked', false);
                } else {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route("admin.residential.house-by-address") }}",
                        data: {community_id: $communityId},
                        success: function (house) {

                            if (house.length > 0) {
                                var houseHTML = '';

                                for (a = 0; a < house.length; a++) {
                                    houseHTML += "<option value='" + house[a].id + "'>" + house[a].house_detail + "</option>";
                                }

                                $('#house_id').html(houseHTML);
                            } else {
                                var houseHTML = "<option value=''>No House Found !</option>";
                                $('#house_id').html(houseHTML);
                            }
                        }
                    });
                }

            });
        });
    </script>
@endpush
