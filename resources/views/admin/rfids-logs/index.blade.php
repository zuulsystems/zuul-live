@extends('admin.layout.master')
@section('content')

    <style>
        .card-bg-heading {
            width: 100%;
            padding-right: 7.5px;
            padding-top: 10.5px;
            padding-bottom: 5px;
            padding-left: 7.5px;
            margin-right: auto;
            margin-left: auto;
            border: 2px solid #ede7e7;
            background: white !important;
            border-radius: 7px !important;
        }

        .daterangepicker {
            top: 50% !important;
            left: 50% !important;
            transform: translate(50%, -40%);
        }

    </style>
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 card-bg-heading">
                    <div class="col-sm-6">
                        <h1>RFID Tracking </h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">RFID Tracking</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Search Filter</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"
                                            title="Collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" method="get" id="search-filter">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="rfids_tag_num_static">RFID Tag #</label>
                                                <input type="text" name="rfids_tag_num_static" id="rfids_tag_num_static"
                                                       placeholder="RFID Tag #" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="resident_name">Resident Name</label>
                                                <input type="text" name="resident_name" id="resident_name"
                                                       placeholder="Resident Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="scanner_name">Scanner</label>
                                                <input type="text" name="scanner_name" id="scanner_name"
                                                       placeholder="Scanner" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="rfid_scan_date">RFID Scan Date</label>

                                                <input type="text" name="rfid_scan_date2" id="rfid_scan_date2"
                                                       placeholder="RFID Scan Date Range" class="form-control">
                                                <input type="hidden" name="rfid_scan_date" id="rfid_scan_date"
                                                       placeholder="RFID Scan Date Range" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="status">Status</label>
                                            <select class="form-control" id="is_success" name="is_success">
                                                <option value="">All</option>
                                                <option value="1">Success</option>
                                                <option value="0">Error</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary search-btn">
                                    <span>Search </span>
                                </button>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">

                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">

                            <table id="rfidLogTable1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>RFID Tag #</th>
                                    <th>Resident Name</th>
                                    <th>Scanner</th>
                                    <th>Scan</th>
                                    <th>Vehicle Info</th>
                                    <th>Log Text</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>

        //rfidDataTable1
        let columns = [
            {

                data: 'id'
            },
            {
                data: function (data) {
                    let modifiedStyle = '';
                    if (data.is_success == '0') {
                        return '<div class="badge badge-danger">' + data.rfid + '</div>';

                    } else {
                        return data.rfid;
                    }

                },
                name: 'rfid'
            },
            {
                data: 'resident_name'
            },
            {
                // data: 'scanner_name',
                data: 'scanner',
            },
            {
                data: 'formatted_scan_date',
                name: 'scan_at',
            },
            {
                data: 'vehicle_info',
            },
            {
                data: 'log_text',
            },
            {
                data: function (data) {
                    return data.formatted_status;
                }
            },
        ];

        // Check if houseId is set in the current URL's query parameters
        var currentUrl = window.location.href;
        var urlParams = new URLSearchParams(currentUrl.split('?')[1]);
        var houseId = urlParams.get('houseId');
        var userId = urlParams.get('userId');

        // Construct the initial AJAX URL
        var initialAjaxUrl = "{{ route('admin.rfid.rfid-logs-data') }}";

        // If houseId exists in the URL, add it to the URL
        if (houseId) {
            initialAjaxUrl += '?houseId=' + houseId;
        }

        // If userId exists in the URL, add it to the URL
        if (userId) {
            initialAjaxUrl += '?userId=' + userId;
        }

        zuulDataTableSortableGuestLogs('#rfidLogTable1', initialAjaxUrl, columns, [7]);
        var is_scan_date = false

        $('.search-btn').click(function () {
            // Serialize the form data
            var formData = $("#search-filter").serialize();
            if(!is_scan_date){
                formData = formData.replace(/&?rfid_scan_date=[^&]*/g, '');
                formData = formData.replace(/&?rfid_scan_date2=[^&]*/g, '');
            }


            // Check if houseId is set in the current URL's query parameters
            var currentUrl = window.location.href;
            var urlParams = new URLSearchParams(currentUrl.split('?')[1]);
            var houseId = urlParams.get('houseId');
            var userId = urlParams.get('userId');

            // If houseId exists in the URL, add it to the form data
            if (houseId) {
                formData += '&houseId=' + houseId;
            }

            // If userId exists in the URL, add it to the URL
            if (userId) {
                formData += '?userId=' + userId;
            }

            // Create the AJAX URL with the serialized form data
            var ajaxUrl = "{{ route('admin.rfid.rfid-logs-data') }}?" + formData;

            // Remove empty scan_date2 parameters
            if ($('#scan_date2').val() === '') {
                ajaxUrl = ajaxUrl.replace(/(rfid_scan_date|rfid_scan_date2)=[^&]+&?/g, '');
            }

            // Call your DataTable function with the updated URL
            zuulDataTableSortableGuestLogs('#rfidLogTable1', ajaxUrl, columns, [7]);
        });

    </script>

    <script>
        $(function () {
            $('input[name="rfid_scan_date"]').daterangepicker({
                opens: 'center',
                locale: {
                    format: 'MM-DD-YYYY'
                }
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('MM-DD-YYYY') + ' to ' + end.format('MM-DD-YYYY'));
                document.getElementById("rfid_scan_date2").setAttribute("value", start.format('MM-DD-YYYY') + ' - ' + end.format('MM-DD-YYYY'))
            });
        });


        document.addEventListener('DOMContentLoaded', function () {
            const inputField = document.getElementById('rfid_scan_date');
            const changeColorButton = document.getElementById('rfid_scan_date2');

            changeColorButton.addEventListener('click', function () {
                $('#rfid_scan_date').click();
                is_scan_date = true
            });
        });

    </script>
@endpush
