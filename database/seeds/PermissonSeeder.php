<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('permissions')->forceDelete();
        $permissions = [
            [ 'name' => 'Head Of Family', 'slug' => 'head_of_family'],
            [ 'name' => 'Can Manage Family', 'slug' => 'can_manage_family'],
            [ 'name' => 'Can Send Passes', 'slug' => 'can_send_passes'],
            [ 'name' => 'Can Retract Sent Passes', 'slug' => 'can_retract_sent_passes'],
            [ 'name' => 'Allow Parental Control', 'slug' => 'allow_parental_control'],
            [ 'name' => 'Is License Locked', 'slug' => 'is_license_locked'],
            [ 'name' => 'Can Check License Plate', 'slug' => 'can_check_license_plate'],
            [ 'name' => 'Notify When Member Guest Arrive', 'slug' => 'notify_when_member_guest_arrive'],
            [ 'name' => 'Notify When Member Send Pass To Others', 'slug' => 'notify_when_member_send_pass_to_others'],
            [ 'name' => 'Notify When Member Receive A Pass', 'slug' => 'notify_when_member_receive_a_pass'],
            [ 'name' => 'Notify When Member Retract A Pass', 'slug' => 'notify_when_member_retract_a_pass'],
            [ 'name' => 'Notify When Member Update A Pass', 'slug' => 'notify_when_member_update_a_pass'],
            [ 'name' => 'Notify When Pass Retracted Included Member', 'slug' => 'notify_when_pass_retracted_included_member'],
            [ 'name' => 'Notify When Pass Updated Included Member', 'slug' => 'notify_when_pass_updated_included_member'],
            [ 'name' => 'Notify When Member Request A Pass With Offensive Words', 'slug' => 'notify_when_member_request_a_pass_with_offensive_words'],
            [ 'name' => 'Blacklist Member To Receive Request A Pass', 'slug' => 'blacklist_member_to_receive_request_a_pass'],
            [ 'name' => 'Allow Parental Controls To Member', 'slug' => 'allow_parental_controls_to_member'],
            [ 'name' => 'Notify When Member Request A Pass', 'slug' => 'notify_when_member_request_a_pass'],
        ];
        DB::table('permissions')->insert($permissions);
    }
}
