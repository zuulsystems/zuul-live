<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimezoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('timezonses')->forceDelete();
        $timezonses = [
            ['id' => 1, 'name' => 'Eastern', 'abbreviation' => 'America/New_York', 'description' => 'Eastern - America/New_York'],
            ['id' => 2, 'name' => 'Central', 'abbreviation' => 'America/Chicago', 'description' => 'Central - America/Chicago'],
            ['id' => 3, 'name' => 'Mountain', 'abbreviation' => 'America/Denver', 'description' => 'Mountain - America/Denver'],
            ['id' => 4, 'name' => 'Mountain no DST', 'abbreviation' => 'America/Phoenix', 'description' => 'Mountain no DST - America/Phoenix'],
            ['id' => 5, 'name' => 'Pacific', 'abbreviation' => 'America/Los_Angeles', 'description' => 'Pacific - America/Los_Angeles'],
            ['id' => 6, 'name' => 'Alaska', 'abbreviation' => 'America/Anchorage', 'description' => 'Alaska - America/Anchorage'],
            ['id' => 7, 'name' => 'Hawaii', 'abbreviation' => 'America/Adak', 'description' => 'Hawaii - America/Adak'],
            ['id' => 8, 'name' => 'Hawaii no DST', 'abbreviation' => 'Pacific/Honolulu', 'description' => 'Hawaii no DST - Pacific/Honolulu'],
            ['id' => 9, 'name' => 'Karachi', 'abbreviation' => 'Asia/Karachi', 'description' => 'Asia - Asia/Karachi'],
        ];
        DB::table('timezones')->insert($timezonses);
    }
}
