<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('events')->forceDelete();
       /* $events = [
            ['id' => 1, 'name' => 'Lunch', 'description' => 'Morning Brunch ' ],
            ['id' => 2, 'name' => 'Breakfast', 'description' => 'Rush Hours to office'],
            ['id' => 3, 'name' => 'Dinner', 'description' => 'just a happy hour'],
            ['id' => 4, 'name' => 'Ceremony', 'description' => 'one year now'],
            ['id' => 5, 'name' => 'Birthday Party', 'description' => 'WOW its my birthday '],
            ['id' => 6, 'name' => 'Family Dinner', 'description' => 'Family is family'],
            ['id' => 7, 'name' => 'Casual Gathering', 'description' => 'just a quick meetup'],
            ['id' => 8, 'name' => 'Movie Night', 'description' => 'long nights must be enjoyable '],
            ['id' => 9, 'name' => 'Event Pass', 'description' => 'Random unnamed event'],
            ['id' => 10, 'name' => 'Instant Pass', 'description' => 'Instant/Anonymous Pass'],
            ['id' => 11, 'name' => 'Quick Pass From Guard', 'description' => 'Quick Pass Allowed From Guard ']
        ];*/
         $events = [
             ['id' => 2, 'name' => 'Lunch', 'description' => 'Morning Brunch' ],
             ['id' => 3, 'name' => 'Breakfast', 'description' => 'Rush Hours to office' ],
             ['id' => 4, 'name' => 'Dinner', 'description' => 'just a happy hour' ],
             ['id' => 5, 'name' => 'Ceremony', 'description' => 'one year now' ],
             ['id' => 6, 'name' => 'Birthday Party', 'description' => 'WOW its my birthday' ],
             ['id' => 7, 'name' => 'Family Dinner', 'description' => 'Family is family' ],
             ['id' => 8, 'name' => 'Casual Gathering', 'description' => 'just a quick meetup' ],
             ['id' => 9, 'name' => 'Movie Night', 'description' => 'long nights must be enjoyable' ],
             ['id' => 14, 'name' => 'Event Pass', 'description' => 'Random unnamed event' ],
             ['id' => 15, 'name' => 'Instant Pass', 'description' => 'Instant/Anonymous Pass' ],
             ['id' => 16, 'name' => 'Quick Pass From Guard', 'description' => 'Quick Pass Allowed From Guard ' ],
         ];
        DB::table('events')->insert($events);
    }
}
