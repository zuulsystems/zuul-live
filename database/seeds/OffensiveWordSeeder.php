<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OffensiveWordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('offensive_words')->forceDelete();
        $offensive_words = [
            ['id' => 1, 'name' => 'kill', 'created_by' => '1'],
            ['id' => 2, 'name' => 'bullet', 'created_by' => '1'],
            ['id' => 3, 'name' => 'shoot', 'created_by' => '1'],
            ['id' => 4, 'name' => 'gun', 'created_by' => '1'],
            ['id' => 5, 'name' => 'knife', 'created_by' => '1'],
            ['id' => 6, 'name' => 'stab', 'created_by' => '1'],
        ];
        DB::table('offensive_words')->insert($offensive_words);
    }
}
