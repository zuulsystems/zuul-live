<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('roles')->forceDelete();
        $roles = [
            ['id' => 1, 'name' => 'Super Administrator / Owner', 'slug' => 'super_admin'],
            ['id' => 2, 'name' => 'Sub-Administrators (Community Admins)', 'slug' => 'sub_admin'],
            ['id' => 3, 'name' => 'Employees', 'slug' => 'employees'],
            ['id' => 4, 'name' => 'Head of the Household', 'slug' => 'family_head'],
            ['id' => 5, 'name' => 'Household Member', 'slug' => 'family_member'],
            ['id' => 6, 'name' => 'Guests / Outsiders One Time', 'slug' => 'guests_outsiders_one_time'],
            ['id' => 7, 'name' => 'Guests / Outsiders Daily', 'slug' => 'guests_outsiders_daily'],
            ['id' => 8, 'name' => 'Guards / Security Personnel', 'slug' => 'guards'],
            ['id' => 9, 'name' => 'Vendor', 'slug' => 'vendors'],
            ['id' => 10, 'name' => 'Traffic Logix Administrator / Owner', 'slug' => 'tl_admin'],
            ['id' => 11, 'name' => 'Guard-Administrator / (Guard Admins)', 'slug' => 'guard_admin'],
            ['id' => 12, 'name' => 'Kiosk', 'slug' => 'kiosk']
        ];
        DB::table('roles')->insert($roles);
    }
}
