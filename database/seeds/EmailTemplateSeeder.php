<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email_types = [
            ['email_type' => 'zuul',
                'name' => 'Become Community Admin', 'template' => "<!DOCTYPE html>
<html>
<head>
</head>
<body>
<p>Dear {admin_name}:</p>
<p>Your {role_description} account has been setup on ZUUL Systems. Please check password reset link and create your password to access zuul systems.</p>
<p>{reset_password_link}</p><p>Thank you,&nbsp;</p>
<p>{sender}</p>
</body>
</html>", 'template_code' => 'become-community-admin', 'assign_to' => 'become_community_admin'],
            ['name' => 'Become Resident Email', 'template' => "<!DOCTYPE html>
<html>
<head>
</head>
<body>
<p>Dear {receiver}:</p>
<p>We received a request from your account that you would like to become a {role_description}. Here is your verification code:&nbsp; {verification_code}</p>
<p>Press the Become a Resident button and enter your code.</p>
<p>If no request was sent, please ignore this email.</p>
<p>Thank you,&nbsp;</p>
<p>{sender}</p>
</body>
</html>", 'template_code' => 'become-resident-email', 'assign_to' => 'become_resident','email_type' => 'zuul'],
            ['email_type' => 'zuul',
                'name' => 'Welcome Login Email', 'template' => "<!DOCTYPE html>
<html>
<head>
</head>
<body>
<p>Dear {receiver}:</p>
<p>Your {user_type} account has been setup on ZUUL Systems under phone number {phone_number}. Once you have downloaded the app, please look to the bottom of the first screen where it says \"Already a Member?\" and press Sign In.&nbsp; Make sure you use the same phone number above when signing up, and enter the temporary password.&nbsp; DO NOT COPY AND PASTE. YOU MUST TYPE IT IN!&nbsp;</p>
<p>{temporary_password}</p>
<p>Click here to download the app</p>
<ul>
<li>Android: {android_link}</li>
<li>iOS: {ios_link}</li>
</ul>
<p>You will now be directed to create a new password. When your new password is confirmed you will be brought to your dashboard. Go to the menu tab at the top of your dashboard and press Edit Profile. Once your profile is completed, you may begin to use ZUUL. You can also manage your vehicles, import contacts and create passes.</p>
<p>In case of any questions or concerns, please contact us at: <a href=\"mailto:support@zuulsystems.com\">support@zuulsystems.com</a>. If you haven't sent a request, please ignore this email.</p>
<p>Thank you,&nbsp;</p>
<p>{sender}</p>
</body>
</html>", 'template_code' => 'welcome-login-email', 'assign_to' => 'welcome_login'],
            ['email_type' => 'zuul',

                'name' => 'QR Code Via Email',
                'template' => '<!DOCTYPE html>
                    <html>
                    <head>
                    </head>
                    <body>
                    <p>Dear {receiver}:</p>
                    <p>You have been invited to&nbsp; {event_name} by {sender_name}.&nbsp; Below are the details of the invite:</p>
                    <table style="border-collapse: collapse; width: 100%;">
                    <tbody>
                    <tr>
                    <td style="width: 50%;">Sender</td>
                    <td style="width: 50%;">{sender_name}</td>
                    </tr>
                    <tr>
                    <td style="width: 50%;">Sender Community</td>
                    <td style="width: 50%;">{sender_community}</td>
                    </tr>
                    <tr>
                    <td style="width: 50%;">Sender Phone</td>
                    <td style="width: 50%;">{sender_phone}</td>
                    </tr>
                    <tr>
                    <td style="width: 50%;">From</td>
                    <td style="width: 50%;">{pass_start_date}</td>
                    </tr>
                    <tr>
                    <td style="width: 50%;">To</td>
                    <td style="width: 50%;">{pass_date}</td>
                    </tr>
                    <tr>
                    <td style="width: 50%;">At</td>
                    <td style="width: 50%;">{description}</td>
                    </tr>
                    </tbody>
                    </table>
                    <p>If you have already installed the app, please ignore this message. Otherwise, below is your scannable QR Code which you MUST bring with you to the residence.</p>
                    <p>{qr_code}</p>
                    <p>You will be requested to provide this code to the guard at the community gate for verification. Also, you MUST bring proper ID if you&nbsp; haven\'t already set it up on the app.</p>
                    <p>Thank you,&nbsp;</p>
                    <p>{sender}</p>
                    </body>
                    </html>',
                'template_code' => 'qr-code-via-email',
                'assign_to' => 'qr_to_email'
//                'email_type' => 'zuul'

            ],
            [
                'email_type' => 'trafficlogix',

                'name' => 'Traffic Logix Violation Email',
                'template' => '<!DOCTYPE html>
                    <html>
                    <head>
                    </head>
                    <body>
                    <div style="max-width: 1000px; margin: 0 auto;">
                    <table style="width: 100%; border: 1px solid black; border-collapse: collapse;">
                    <tbody>
                    <tr style="background: #ad4c4c; color: white; border: 1px solid black; border-collapse: collapse;">
                    <td colspan="12">
                    <h4>AUTOMATED TRAFFIC VIOLATION</h4>
                    <p>City of Chicago Department of Finance <br />Search &amp; Pay For Your Tickets On-Line at: <br />www.cityofchicago.org/finance <br />312.PARK (7275) 312.744.7277 (TTY - For Hearing Impaired)</p>
                    </td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="background: #c4b3b3; color: white; border: 1px solid black; border-collapse: collapse;" colspan="2">Notice Date:</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">{timestamp}</td>
                    <td style="border: 1px solid black; border-collapse: collapse;">&nbsp;</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="background: #c4b3b3; color: white; border: 1px solid black; border-collapse: collapse;" colspan="2">Notice Number:</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">{notice_number}</td>
                    <td style="border: 1px solid black; border-collapse: collapse;">&nbsp;</td>
                    </tr>
                    <tr style="background: #ad4c4c; color: white; border: 1px solid black; border-collapse: collapse;">
                    <td colspan="12">VIOLATION</td>
                    </tr>
                    <tr style="background: #c4b3b3; color: white; border: 1px solid black; border-collapse: collapse;">
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">TICKET NUMBER</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">LICENCE PLATE</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="8">DATE/TIME OF VIOLATION</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">{ticket_id}</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">{plate_text}</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="8">{ticket_time}</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="background: #c4b3b3; color: white; border: 1px solid black; border-collapse: collapse;" colspan="2">SPEED LIMIT</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2">{current_speed_limit} mph</td>
                    <td style="background: #c4b3b3; color: white; border: 1px solid black; border-collapse: collapse;" colspan="8">VIOLATION IMAGE</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="background: #c4b3b3; color: white; border: 1px solid black; border-collapse: collapse;" colspan="2" rowspan="1">VIOLATION SPEED</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="2" rowspan="1">{violating_speed} mph</td>
                    <td style="border: 1px solid black; border-collapse: collapse;" colspan="8" rowspan="10">{image}</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="background: #c4b3b3; color: white;" colspan="4" rowspan="1" width="30%">LOCATION ADDRESS</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td colspan="4" rowspan="1">{location_address}</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="background: #c4b3b3; color: white;" colspan="4" rowspan="1">LOCATION MAP</td>
                    </tr>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td style="width: 300px; height: 300px;" colspan="4" rowspan="1" width="300">{geocode}</td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    </body>
                    </html>',
                'template_code' => 'traffic-logix-violation-email',
                'assign_to' => 'traffic_ticket_email',
//                'email_type' => 'trafficlogix'

            ],
            [
                'email_type' => 'trafficlogix',
                'name' => 'Traffic Logix Violation Reminder Email',
                'template' => '<!DOCTYPE html>
                    <html>
                    
                    <head>
                    </head>
                    
                    <body>
                    <div style="max-width: 800px; margin: 0 auto;">
                    <table style="width: 100%; border: 1px solid black; border-collapse: collapse;">
                    <tbody>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td colspan="4">
                    <h2 style="padding: 0px; font-size: 25px; text-align: center;">Island Estates <br />Neighborhood <br />Association</h2>
                    </td>
                    <td colspan="8">
                    <h2 style="text-align: center;">Island Estates Speed Notice # 2</h2>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="12">
                    <div style="padding: 30px 14% 30px 15px;">
                    <p>The Island Estates Neighborhood Association is committed to upholding the Covenants, Conditions and Restrictions of the Island Estates Homeowners Association-Speed being one of them.</p>
                    <p>This letter indicates that a vehicle registered to you at {location_name} was significantly exceeding the posted 25 MPH speed limit as shown below.</p>
                    </div>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4" width="100px">
                    <h3 style="text-align: left; margin: 0px; padding: 0px 0% 0px 15px; font-size: 17px;">Location of Speed Camera:</h3>
                    </td>
                    <td colspan="8">
                    <h3 style="text-align: center; margin: 0px; padding: 0px;">Citation Information:</h3>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6" rowspan="1" width="40%">
                    <div style="text-align: left; padding: 0px 0% 0px 15px;">{geocode}</div>
                    <!-- <img src="" >  --></td>
                    <td colspan="6">
                    <table style="padding: 5px; position: relative; right: 15px; border: 1px solid black; margin-right: 15px; width: 100%;">
                    <tbody>
                    <tr>
                    <td>Vehicle License Plate:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{plate_text}</td>
                    </tr>
                    <tr>
                    <td>Date Observed:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{ticket_time}</td>
                    </tr>
                    <tr>
                    <td>Posted Speed Limit:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{current_speed_limit} mph</td>
                    </tr>
                    <tr>
                    <td>Vehicle Speed Limit:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{violating_speed} mph</td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6">
                    <div style="padding: 30px 0 15px 15px;">
                    <p>Safety is paramount in our community. <br />We request that you, or whoever <br />was posted speed limit.</p>
                    <p>We appreciate your cooperation in <br />this matter.</p>
                    <p>Feel free to direct any questions or <br />concerns to our Speed Task Force <br />Committee at <a href="mailto:info@islandestates.org">info@islandestates.org</a></p>
                    <p>Very Truly Yours,</p>
                    <p>Island Estates Neighborhood <br />Board of Directors</p>
                    </div>
                    </td>
                    <td colspan="6">
                    <div style="padding: 0 15px 0 0;">{image}</div>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    </body>
                    
                    </html>',
                'template_code' => 'traffic-logix-violation-reminder-email',
                'assign_to' => 'traffic_ticket_reminder_email',
//                'email_type' => 'trafficlogix'
            ],
            [
                'email_type' => 'trafficlogix',
                'name' => 'Traffic Logix Violation Reminder Email',
                'template' => '<!DOCTYPE html>
                    <html>
                    
                    <head>
                    </head>
                    
                    <body>
                    <div style="max-width: 800px; margin: 0 auto;">
                    <table style="width: 100%; border: 1px solid black; border-collapse: collapse;">
                    <tbody>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td colspan="4">
                    <h2 style="padding: 0px; font-size: 25px; text-align: center;">Island Estates <br />Neighborhood <br />Association</h2>
                    </td>
                    <td colspan="8">
                    <h2 style="text-align: center;">Island Estates Speed Notice # 2</h2>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="12">
                    <div style="padding: 30px 14% 30px 15px;">
                    <p>The Island Estates Neighborhood Association is committed to upholding the Covenants, Conditions and Restrictions of the Island Estates Homeowners Association-Speed being one of them.</p>
                    <p>This letter indicates that a vehicle registered to you at {location_name} was significantly exceeding the posted 25 MPH speed limit as shown below.</p>
                    </div>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4" width="100px">
                    <h3 style="text-align: left; margin: 0px; padding: 0px 0% 0px 15px; font-size: 17px;">Location of Speed Camera:</h3>
                    </td>
                    <td colspan="8">
                    <h3 style="text-align: center; margin: 0px; padding: 0px;">Citation Information:</h3>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6" rowspan="1" width="40%">
                    <div style="text-align: left; padding: 0px 0% 0px 15px;">{geocode}</div>
                    <!-- <img src="" >  --></td>
                    <td colspan="6">
                    <table style="padding: 5px; position: relative; right: 15px; border: 1px solid black; margin-right: 15px; width: 100%;">
                    <tbody>
                    <tr>
                    <td>Vehicle License Plate:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{plate_text}</td>
                    </tr>
                    <tr>
                    <td>Date Observed:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{ticket_time}</td>
                    </tr>
                    <tr>
                    <td>Posted Speed Limit:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{current_speed_limit} mph</td>
                    </tr>
                    <tr>
                    <td>Vehicle Speed Limit:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{violating_speed} mph</td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6">
                    <div style="padding: 30px 0 15px 15px;">
                    <p>Safety is paramount in our community. <br />We request that you, or whoever <br />was posted speed limit.</p>
                    <p>We appreciate your cooperation in <br />this matter.</p>
                    <p>Feel free to direct any questions or <br />concerns to our Speed Task Force <br />Committee at <a href="mailto:info@islandestates.org">info@islandestates.org</a></p>
                    <p>Very Truly Yours,</p>
                    <p>Island Estates Neighborhood <br />Board of Directors</p>
                    </div>
                    </td>
                    <td colspan="6">
                    <div style="padding: 0 15px 0 0;">{image}</div>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    </body>
                    
                    </html>',
                'template_code' => 'traffic-logix-violation-reminder-email',
                'assign_to' => 'traffic_ticket_reminder_email',
//                'email_type' => 'trafficlogix'
            ],
            [
                'email_type' => 'trafficlogix',
                'name' => 'Traffic Logix Violation Warning Email',
                'template' => '<!DOCTYPE html>
                    <html>
                    
                    <head>
                    </head>
                    
                    <body>
                    <div style="max-width: 800px; margin: 0 auto;">
                    <table style="width: 100%; border: 1px solid black; border-collapse: collapse;">
                    <tbody>
                    <tr style="border: 1px solid black; border-collapse: collapse;">
                    <td colspan="4">
                    <h2 style="padding: 0px; font-size: 25px; text-align: center;">Island Estates <br />Neighborhood <br />Association</h2>
                    </td>
                    <td colspan="8">
                    <h2 style="text-align: center;">Island Estates Speed Notice # 2</h2>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="12">
                    <div style="padding: 30px 14% 30px 15px;">
                    <p>The Island Estates Neighborhood Association is committed to upholding the Covenants, Conditions and Restrictions of the Island Estates Homeowners Association-Speed being one of them.</p>
                    <p>This letter indicates that a vehicle registered to you at {location_name} was significantly exceeding the posted 25 MPH speed limit as shown below.</p>
                    </div>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="4" width="100px">
                    <h3 style="text-align: left; margin: 0px; padding: 0px 0% 0px 15px; font-size: 17px;">Location of Speed Camera:</h3>
                    </td>
                    <td colspan="8">
                    <h3 style="text-align: center; margin: 0px; padding: 0px;">Citation Information:</h3>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6" rowspan="1" width="40%">
                    <div style="text-align: left; padding: 0px 0% 0px 15px;">{geocode}</div>
                    <!-- <img src="" >  --></td>
                    <td colspan="6">
                    <table style="padding: 5px; position: relative; right: 15px; border: 1px solid black; margin-right: 15px; width: 100%;">
                    <tbody>
                    <tr>
                    <td>Vehicle License Plate:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{plate_text}</td>
                    </tr>
                    <tr>
                    <td>Date Observed:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{ticket_time}</td>
                    </tr>
                    <tr>
                    <td>Posted Speed Limit:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{current_speed_limit} mph</td>
                    </tr>
                    <tr>
                    <td>Vehicle Speed Limit:</td>
                    <td style="border: 1px solid black; text-align: center; padding: 10px;">{violating_speed} mph</td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="6">
                    <div style="padding: 30px 0 15px 15px;">
                    <p>Safety is paramount in our community. <br />We request that you, or whoever <br />was posted speed limit.</p>
                    <p>We appreciate your cooperation in <br />this matter.</p>
                    <p>Feel free to direct any questions or <br />concerns to our Speed Task Force <br />Committee at <a href="mailto:info@islandestates.org">info@islandestates.org</a></p>
                    <p>Very Truly Yours,</p>
                    <p>Island Estates Neighborhood <br />Board of Directors</p>
                    </div>
                    </td>
                    <td colspan="6">
                    <div style="padding: 0 15px 0 0;">{image}</div>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </div>
                    </body>
                    
                    </html>',
                'template_code' => 'traffic-logix-violation-warning-email',
                'assign_to' => 'traffic_ticket_warning_email',
//                'email_type' => 'trafficlogix'
            ],
            [
                'email_type' => 'zuul',
                'name' => 'Security Guard Email',
                'template' => '<p style="">Dear {receiver}:</p><p style="">Your account has been setup on ZUUL Systems under phone number {phone_number}. Once you have downloaded the app, go to Enter Guard Verification Code and enter the highlighted verification code below. Make sure you use the same phone number above and your newly created password when signing in.</p><p style="">{verification_code}</p><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">Click here to download the app</p><ul style="margin-bottom: 10px;"><li>Android: {android_link}</li><li>iOS: {ios_link}</li></ul><p style=""><br></p><p style="">Thank you,&nbsp;</p>',
                'template_code' => 'security-guard-email',
                'assign_to' => 'security_guard_email',
//                'email_type' => 'trafficlogix'
            ],
            ['name' => 'Become Household Member Email', 'template' => "<p>Dear {receiver}:<br></p><p>Your ZUUL Systems account, using phone number {phone_number}, has been updated to a Resident account by your community admin, {admin_name}.<br></p><p>Please log out of your ZUUL account, and log back in.&nbsp; You should now be able to create a pass (bottom right corner of your dashboard).&nbsp; If you have any questions or concerns, please email us at support@zuulsystems.com.<br></p><p>Thank you,<br></p><p>ZUUL Support</p>", 'template_code' => 'become-household-email', 'assign_to' => 'become_family','email_type' => 'zuul'],

            ['name' => 'Guest Arrived', 'template' => "<p>{receiver} has now entered the community. Arrival date and time was {timestamp}</p><p>Thank You</p><p>Zuul Systems</p><p></p>", 'template_code' => 'become-household-email', 'assign_to' => 'guest_arrival','email_type' => 'zuul']
        ];
        DB::table('email_templates')->insert($email_types);
    }
}
