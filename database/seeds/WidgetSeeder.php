<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $widgets = [
            ['name' => 'guest', 'community_right' => 'zuul'],
            ['name' => 'guard', 'community_right' => 'zuul'],
            ['name' => 'house', 'community_right' => 'zuul'],
            ['name' => 'resident', 'community_right' => 'zuul'],
            ['name' => 'announcement', 'community_right' => 'zuul'],
        ];
        DB::table('widgets')->insert($widgets);
    }
}
