<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call([
             RoleSeeder::class,
             PermissonSeeder::class,
             UserSeeder::class,
             TimezoneSeeder::class,
             CountrySeeder::class,
             EmailTypeSeeder::class,
             EmailTemplateSeeder::class,
             OffensiveWordSeeder::class,
             EventSeeder::class,
             DeviceTypeSeeder::class,
             PrinterTemplateSeeder::class
         ]);
    }
}
