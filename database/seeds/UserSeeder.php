<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->forceDelete();
        $users = [
            [
                'id' => '1',
                'first_name' => 'Adam',
                'last_name' => 'Lucks',
                'email' => 'admin@zuulsystems.com',
                'password' => '$2y$10$nkYGOu84dyG68m.Q9JhqkOvTN03FMyB95F34YBKnrcptvwYZxFbLq',
                'profile_image' => '',
                'web_token' => 'e969RfSlbR4:APA91bF8_1_BUR87r5wZ1rQ4PO-_qn3EIMYE6tgWKsjZk6dhJvqGb92de_KjLZiVA1XSPbS3ZT_bTPaWQ4t9JJAjWL8FtCSAMB0OcZWk28K1KiIErtn5hxwbvvmzsCy279ltA-LR2WlI',
                'is_verified' => '1',
                'role_id' => '1',
            ],
            [
                'id' => '2',
                'first_name' => 'Jayme',
                'last_name' => '',
                'email' => 'jayme@zuulsystems.com',
                'password' => '$2y$10$4mUrEYPuMovq/Hm4tgVQL.XTcFuyJpPIYUmyZ3v1TpcWsB4JKOUhK',
                'profile_image' => '',
                'web_token' => 'e969RfSlbR4:APA91bF8_1_BUR87r5wZ1rQ4PO-_qn3EIMYE6tgWKsjZk6dhJvqGb92de_KjLZiVA1XSPbS3ZT_bTPaWQ4t9JJAjWL8FtCSAMB0OcZWk28K1KiIErtn5hxwbvvmzsCy279ltA-LR2WlI',
                'is_verified' => '1',
                'role_id' => '12',
            ]
        ];
        DB::table('users')->insert($users);
    }
}
