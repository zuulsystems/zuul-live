<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeviceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $events = [
             ['id' => 10, 'slug' => 'controller','name' => 'Controller' ],
             ['id' => 20, 'slug' => 'controller_awid','name' => 'Controller + Awid Reader' ],
             ['id' => 30, 'slug' => 'controller_maxtek','name' => 'Controller + MaxTek Reader' ],
             ['id' => 200, 'slug' => 'barcode_scanner','name' => 'Barcode Scanner' ],
         ];
        DB::table('device_types')->insert($events);
    }
}
