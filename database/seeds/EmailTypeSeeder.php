<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email_types = [
            ['id' => 1, 'name' => 'Become Household', 'email_type' => 'admin', 'slug' => 'become_family'],
            ['id' => 2, 'name' => 'Become Resident', 'email_type' => 'admin', 'slug' => 'become_resident'],
            ['id' => 3, 'name' => 'Become Community Admin', 'email_type' => 'admin', 'slug' => 'become_community_admin'],
            ['id' => 4, 'name' => 'QR To Email', 'email_type' => 'admin', 'slug' => 'qr_to_email'],
            ['id' => 5, 'name' => 'Random Password', 'email_type' => 'admin', 'slug' => 'random_password'],
            ['id' => 6, 'name' => 'Welcome Login', 'email_type' => 'admin', 'slug' => 'welcome_login'],
            ['id' => 7, 'name' => 'Offensive Words', 'email_type' => 'admin', 'slug' => 'offensive_words'],
            ['id' => 8, 'name' => 'Duplicate Verification Email', 'email_type' => 'admin', 'slug' => 'duplicate_verification_email'],
            ['id' => 9, 'name' => 'Two Factor Verification Email', 'email_type' => 'admin', 'slug' => 'two_factor_verification_email'],
            ['id' => 10, 'name' => 'Traffic Ticket Email', 'email_type' => 'tl', 'slug' => 'traffic_ticket_email'],
            ['id' => 11, 'name' => 'Traffic Ticket Reminder Email', 'email_type' => 'tl', 'slug' => 'traffic_ticket_reminder_email'],
            ['id' => 12, 'name' => 'Traffic Ticket Warning Email', 'email_type' => 'tl', 'slug' => 'traffic_ticket_warning_email'],
            ['id' => 13, 'name' => 'Request To Become Resident', 'email_type' => 'admin', 'slug' => 'request_to_become_resident'],
            ['id' => 14, 'name' => 'Resident Import CSV', 'email_type' => 'admin', 'slug' => 'resident_import_csv'],
            ['id' => 15, 'name' => 'Security Guard Email', 'email_type' => 'admin', 'slug' => 'security_guard_email'],
            ['id' => 16, 'name' => 'Guest Arrival', 'email_type' => 'admin', 'slug' => 'guest_arrival']
        ];
        DB::table('email_types')->insert($email_types);
    }
}
