<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Node\ZuulNodeController;
use Illuminate\Http\Request;

//testing routes

//houses
Route::resource('family-members', 'API\HouseHoldMemberController')->only(['index', 'store', 'destroy']);


Route::get('rg-test-234564', function () {

    $scanner = new ZuulNodeController();
    $req = new Request();
    $req->request->add([
        "a" => "b"
    ]);
    $req->setMethod("POST");
    return $scanner->scannedQrCodeKiosk($req);
});

Route::group(['middleware' => ['auth:api', 'checkKioskApi', 'checkNode']], function () {

});
Route::get('get-rfid-codes', 'API\ScannerController@getRfidCodes');

Route::middleware(['checkNode'])->group(function () {

    Route::post('get-guard-residents', 'API\GuardController@getGuardResidentsKiosk');
    Route::post('open-web-relay', 'API\GuardController@openRemoteWebRelay');
    Route::get('get_all_rf_code/{id}', 'API\ScannerController@getRfidCodesWithKiosk');
    Route::get('scanned_rf_code/{id}/{rfcode}', 'API\ScannerController@scannedRfidCodeWithKiosk');
    Route::get('/scanned-qr-code/{type}/{id}/{qr_code}', 'API\ScanController@scannedQrCode');
    Route::post("send-guard-scan-by-qr-by-kiosk", 'API\GuardController@sendGuardScanByQrByKiosk');

    //quick pass
    Route::post('create-quick-pass', 'API\GuardController@createQuickPass');

    Route::get('ping', 'Node\ZuulNodeController@pong');
    Route::post('socket-connection', 'Node\ZuulNodeController@socketConnection');
    Route::post('request-for-socket-connection', 'Node\ZuulNodeController@guardrequest'); //->middleware('auth:api');
    Route::post('image-store', 'Node\ZuulNodeController@imageStore');
    Route::post('get-image', 'Node\ZuulNodeController@getImage');//->middleware('auth:api');
    Route::post('auth-remote-guard', 'Node\ZuulNodeController@authRemoteGuard');
});

Route::post('connected-pie/add', "Node\RemoteGuardController@addConnectedPie");

Route::post('remote-guard/ping', "Node\RemoteGuardController@ping");
Route::post('remote-guard/type', "Node\RemoteGuardController@type");
Route::get('remote-guard/communities', "Node\RemoteGuardController@communities");
Route::post('remote-guard/add', "Node\RemoteGuardController@addRemoteGuard");
Route::post('remote-guard/keysgenerate', "Node\RemoteGuardController@keysgenerate");
Route::post('remote-guard/update', "Node\RemoteGuardController@update");
Route::get('remote-guard/all', "Node\RemoteGuardController@all");
