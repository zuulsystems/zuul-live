<?php

use App\Http\Controllers\Admin\UserController;
use App\Models\User;
use App\Models\UserContact;
use App\Models\RfidLog;
use App\Models\Rfid;
use App\Services\UserContactService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('guzzle-hit-camera', function () {

    $url = "http://107.128.123.204:9000/hooks/response";
    try {
        $apiData = [
            "mac_address" => "64:25:5E:02:35:93",
            "status" => "true"
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];

        Http::withHeaders($headers)->post($url, $apiData);

    } catch (Exception $exception) {
        return [
            'bool' => false,
            'message' => $exception->getMessage()
        ];
    }

});

Route::get('tmp-emails', function () {


    $users = User::where('community_id', 57)->whereNotNull('temp_password')->pluck('id');

    for ($i = 0; $i < count($users); $i++) {

        $userService = new UserService(new User(), new UserContactService(new UserContact()));
        $userController = new UserController($userService);

        $user = User::where('id', $users[$i])->first();
        if ($user) {

            if ($user->temp_email_sent == 0 && !empty($user->email) && $user->email != '' && filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                $user = $userController->resendTemporaryPassword($users[$i]);

                $updateUser = $userService->update([
                    'temp_email_sent' => 1
                ], $users[$i]);

                sleep(3);

            }

        }


    }

    return response()->json(['bool' => true, 'message' => 'All records emails sent']);

});


Route::get('/update-camera-image', 'API\CameraController@updateCamImg');
Route::post('/active-camera-list/{id}', 'API\CameraController@activeCameraList');
Route::post('/snap-camera-image/{id}', 'API\CameraController@snapImage');

Route::get('/ring-central-call-logs', 'API\RingCentralController@index');
Route::get('/ring-central-message-list', 'API\RingCentralController@voiceMessageList');

Route::post('/zuul-webhook', 'API\ScanController@webhook');
Route::get('/zuul-webhook-call', 'API\ScanController@webhookCall');

Route::get('/scanned-qr-code/{type}/{id}/{qr_code}', 'API\ScanController@scannedQrCode');
Route::get('/scanned-qr-code-hit/{type}/{id}/{qr_code}', 'API\ScanController@scannedQrCode');

Route::get('get-base-url', function (Request $request) {
    return getBaseUrl($request);
});

Route::get('get-socket-base-url', function (Request $request) {
    return getSocketBaseUrl($request);
});

//Mark:- Reset Password API for Deep Link Feature
Route::post('reset-password-deep-link', function (Request $request) {
    return resetPassword($request);
});

Route::middleware('checkZuulApi')->group(function () {
    Route::get('/get-sound-location', 'API\CommonController@getSoundLocation');
    Route::post('/get-added-hour-date', 'API\CommonController@getAddedHourDate');


// Auth Controller Routes
    Route::post('login', 'API\AuthController@login');
    Route::post('verify-guard-login', 'API\AuthController@verifyGuardLogin');

//User Routes
    Route::post('signup', 'API\UserController@store');
    Route::get('number-exists/{phone}', 'API\UserController@checkPhoneExistence');
    // Route::get('number-exists/{phone}', 'API\UserController@checkPhoneExistence');
    //Get All country dial code
    Route::get('get-active-country-dial-code', 'API\UserController@getActiveCountryDialCodes');

    Route::middleware('auth:api')->group(function () {

        //User Routes
        Route::get('can-send-pass/{phone}', 'API\UserController@checkPassPerm');
        Route::post('update-password', 'API\UserController@updatePassword');
        Route::get('user', 'API\UserController@show');
        Route::post('save-fcm-token', 'API\UserController@saveFcmToken');
        Route::post('check-email-already-in-use', 'API\UserController@checkEmailAlreadyInUse');
        Route::get('get-license-lock-status', 'API\UserController@getLicenseLockStatus');
        Route::post('become-resident', 'API\UserController@becomeResident');
        Route::post('update-user-fields', 'API\UserController@updateUserFields');

        //User Contacts
        Route::resource('user-contacts', 'API\UserContactsController');
        Route::get('user-contacts-count', 'API\UserContactsController@userContactsCount');

        Route::get('user-contacts-dnc-count', 'API\UserContactsController@userContactsDncCount');

        Route::get('pre-approved-count', 'API\UserContactsController@preApprovedCount');

        //Mark:- Get Pass Information By Pass ID
        Route::get('pass-information/{qr_code}', 'API\PassController@passInformation');

        Route::post('add-contact-by-created-by-id', 'API\UserContactsController@addContactByCreatedById');
        Route::post('add-contact-by-user-id', 'API\UserContactsController@addContactByUserId');
        Route::post('bulk-import-user-contact', 'API\UserContactsController@bulkImportUserContact');
        Route::post('bulk-delete-user-contact', 'API\UserContactsController@destroyMany');
        Route::post('add-contact-in-favorite', 'API\UserContactsController@addContactInFavorite');
        Route::post('remove-contact-in-favorite', 'API\UserContactsController@removeContactInFavorite');
        Route::post('add-contact-in-dnc', 'API\UserContactsController@addContactInDnc');
        Route::post('remove-contact-in-dnc', 'API\UserContactsController@removeContactInDnc');
        Route::post('add-contact-as-shared-dnc', 'API\UserContactsController@addContactSharedDnc');
        Route::get('get-contact-in-dnc-by-resident-id/{id}', 'API\UserContactsController@getContactInDncByResidentId');
        Route::get('get-users-can-send-pass', 'API\UserContactsController@getUsersCanSendPass');
        Route::get('get-users-can-send-pass/{search?}', 'API\UserContactsController@getUsersCanSendPass');
        Route::get('get-phone-format-by-country-code/{user_id}', 'API\ResidentController@getPhoneFormatByCountryCode');
        Route::get('get-resident-additional-phone/{id}', 'API\ResidentController@index');
        // Route::post('create-resident-additional-phone', 'API\ResidentController@store');
        Route::get('edit-resident-additional-phone/{id}', 'API\ResidentController@edit');
        Route::post('update-resident-additional-phone/{id}', 'API\ResidentController@update');
        Route::get('delete-resident-additional-phone/{id}', 'API\ResidentController@destroy');

        //Contact Groups
        Route::resource('contact-groups', 'API\UserContactGroupsController');
        Route::post('bulk-delete-group', 'API\UserContactGroupsController@destroyMany');
        Route::post('add-contact-in-group', 'API\UserContactGroupsController@addContactInGroup');
        Route::post('bulk-remove-contact-from-group', 'API\UserContactGroupsController@bulkRemoveContactFromGroup');
        Route::post('get-user-contact-by-group', 'API\UserContactGroupsController@getUserContactByGroup');

        Route::get('get-resident-additional-phone/{id}', 'API\ResidentController@index');
        Route::post('create-resident-additional-phone', 'API\ResidentController@store');
        Route::get('edit-resident-additional-phone/{id}', 'API\ResidentController@edit');
        Route::post('update-resident-additional-phone/{id}', 'API\ResidentController@update');
        Route::get('delete-resident-additional-phone/{id}', 'API\ResidentController@destroy');

        //events
        Route::resource('events', 'API\EventController');

        //Passes
        Route::resource('passes', 'API\PassController');
        Route::post('retract-sent-pass', 'API\PassController@retractSentPass');
        Route::get('get-user-sent-passes/{id}', 'API\PassController@getUserSentPasses');
        Route::get('get-user-received-passes/{id}', 'API\PassController@getUserReceivedPasses');

        Route::get('pass-information/{passId}', 'API\PassController@passInformation');

        //active passes
        Route::get('active-passes-data/{userId}', 'API\ActivePassController@activePassesData');
        Route::get('active-passes-dates/{userId}', 'API\ActivePassController@activePassesDates');
        Route::get('passes-detail/{passUserId}', 'API\ActivePassController@getPassDetail');
        Route::get('passes-detail-by-family-member/{passUserId}/{userId}', 'API\ActivePassController@getPassDetailByFamilyMember');
        Route::get('active-passes-count/{userId}', 'API\ActivePassController@activePassesCount');

        //sent passes
        Route::get('sent-passes-data/{userId}', 'API\SentPassController@sentPassesData');
        Route::get('sent-passes-dates/{userId}', 'API\SentPassController@sentPassesDates');
        Route::get('get-pass-recipients/{passId}', 'API\SentPassController@passRecipients');
        Route::get('remove-pass-recipient/{passUserId}', 'API\SentPassController@removePassRecipient');
        Route::post('add-recipient-in-pass/{passId}', 'API\SentPassController@addRecipientInPass');
        Route::get('get-scanned-pass-recipients/{passId}', 'API\SentPassController@scanPassRecipients');

        //archived passes / scanned passes
        Route::get('scanned-passes-data/{userId}', 'API\ScannedPassController@scannedPassesData');
        Route::get('scanned-passes-dates/{userId}', 'API\ScannedPassController@scannedPassesDates');

        //Send Request for pass api
        Route::post('send-request-for-pass', 'API\PassController@sendRequestForAPass');
        Route::post('send-pass-request-from-contact', 'API\PassController@sendPassRequestFromContact');

        Route::post('update-user-notification-settings', 'API\UserController@updateUserNotificationSettings');

        //Reject request pass api
        Route::post('reject-pass-request', 'API\PassRequestController@rejectPassRequest');


        //Vehicles - Only Store, Update and Destroy Routes are Valid
        Route::resource('vehicles', 'API\VehicleController')->only(['store', 'update', 'destroy', 'index'])->shallow();
        Route::post('fetch-pass-vehicle', 'API\VehiclePassController@fetchPassVehicle');
        Route::patch('set-pass-vehicle', 'API\VehiclePassController@setPassVehicle');
        Route::post('add-vehicle-by-guard', 'API\VehicleController@addVehicleByGuard');

        Route::get('house-residents-vehicles', 'API\VehicleController@houseResidentsVehicles');

        //Scan pass by type Guard / Scanner
        Route::get('/get-license-plate-of-scan-log/{scanLogId}', 'API\ScanController@getLicensePlateOfScanLog');
        Route::get('get-user-licence-image-via-kiosk-final-image/{type}/{id}', 'API\ScanController@getUserLicenseImageViaKiosk');

        // Household members
        Route::resource('family-members', 'API\HouseHoldMemberController')->only(['index', 'store', 'destroy']);
        Route::get('family-members-count', 'API\HouseHoldMemberController@familyMembersCount');

        //roles api
        Route::get('roles', 'API\UserController@roles');

        //Accept request pass api
        Route::post('accept-pass-request', 'API\PassRequestController@acceptPassRequest');

        //Profile api
        Route::post('profile', 'API\UserController@profile');

        Route::post('update-user-profile/{userId}', 'API\UserController@updateUserProfile');

        Route::post('update-user-profile-byfile/{userId}', 'API\UserController@updateUserProfileByfile');

        Route::post('update-scan-log/{scanLogId}', 'API\UserController@updateScanLog');


        //Mark Read Parental Control Notifications api
        Route::post('mark-read-pc-notification', 'API\NotificationController@markReadParentalControlNotification');

        //Mark read push notifications api
        Route::post('mark-read-push-notification', 'API\NotificationController@markReadPushNotification');

        //Mark guard resident api
        Route::middleware('guardDeactivate')->group(function () {
            Route::post('get-guard-residents', 'API\GuardController@getGuardResidents');
            Route::post('create-quick-pass', 'API\GuardController@createQuickPass');

            //Scan Logs
            Route::resource('scan-logs', 'API\ScanLogController');
            Route::get('get-scan-qr-by-kiosk', 'API\GuardController@getScanQrByKiosk');
            Route::get('receive-scan-qr-by-kiosk/{socket_id}', 'API\GuardController@receiveScanQrByKiosk');

            //get guard appoint location
            Route::get('get-guard-appoint-locations', 'API\GuardController@getGuardAppointLocations');
            Route::post('set-guard-appoint-locations', 'API\GuardController@setGuardAppointLocations');
            Route::post('open-gate-via-kiosk', 'API\GuardController@openGateViaKiosk');
            Route::post('get-user-licence-image-via-kiosk', 'API\GuardController@getUserLicenceImageViaKiosk');

            //Get user pass guest logs api
            Route::post('get-user-pass-guest-logs', 'API\PassController@GetUserScannedPasses');


            //Scan Logs
            Route::resource('scan-logs', 'API\ScanLogController');

            Route::get('/printer-visitor-pass-template/{scanLogId}', 'API\ScanController@getPrinterVisitorPassTemplate');
            Route::get('/printer-guest-pass-template/{scanLogId}', 'API\ScanController@getGuestParkingPassTemplate');

            Route::get('/printer-template/{scanLogId}', 'API\ScanController@getPrinterTemplate');

            //Web Relay List
            Route::get('get-web-relays', 'API\GuardController@getWebRelays');

            //setting web relay of guard in users table
            Route::get('set-guard-web-relay/{id}', 'API\GuardController@setGuardWebRelay');

            Route::post('open-web-relay', 'API\GuardController@openWebRelay');
            Route::get('web-relay-status', 'API\GuardController@getWebRelayStatus');


        });

        //Get user pass guest logs api
        Route::post('get-user-pass-guest-logs', 'API\PassController@GetUserScannedPasses');


        //Parental Control
        Route::resource('parental-control', 'API\ParentalController');
        Route::get('get-parental-control-options/{id}', 'API\ParentalController@parentalControlOptions');
        Route::post('set-parental-control-options/{id}', 'API\ParentalController@setParentalControlOptions');

        //notifications
        Route::get('get-notification-count', 'API\NotificationController@getNotificationCount');
        Route::get('get-parental-notifications', 'API\NotificationController@getParentalControlNotifications');
        Route::get('get-zuul-notifications', 'API\NotificationController@getZuulControlNotifications');
        Route::get('remove-notification/{id}', 'API\NotificationController@removeNotification');


        Route::get('get-all-counts', 'API\CommonController@getNotificationContactPreApprovedPassesCounts');

        //update password api
        Route::post('update-password', 'API\UserController@updatePassword');
        //verify email address
        Route::post('verify-email-address', 'API\UserController@verifyEmailAddress');
        //announcements
        Route::resource('announcements', 'API\AnnouncementController');

        Route::post('set-manage-family-permission', 'API\UserPermissionController@setCanManageFamilyPermission');
        Route::post('set-send-passes-permission', 'API\UserPermissionController@setCanSendPassPermission');
        Route::post('set-allow-parental-permission', 'API\UserPermissionController@setAllowParentalPermission');

        //User Contacts
        Route::resource('cameras', 'API\CameraController');
        Route::get('/get-camera-count', 'API\CameraController@getCameraCount');

        Route::get('/get-camera-snap/{camera_id}', 'API\ScanController@getCameraSnap');
        Route::get('/set-doorbird-camera-initiate', 'API\CameraController@setDoorbirdCameraInitiate');
        Route::get('/get-doorbird-camera-info', 'API\CameraController@getDoorbirdCameraInfo');

        Route::get('check-stream', 'API\CameraController@checkCurl');
        Route::get('camera-stream', 'API\CameraController@fetchStream');
        Route::get('guard-front-camera-snapshot', 'API\CameraController@guardFrontCameraSnapshot');

        Route::post('capture-com-initialize', 'API\CameraController@captureComInitialize');
        Route::get('get-intercom-stream', 'API\CameraController@getIntercomStream');

        //Temp User Delete
        Route::post('remove-user-temp', 'API\UserController@tempUserDeleteBySelf');


        // pre approved list by guard

        Route::post('add-contact-in-dnc-by-guard-by-resident-id', 'API\UserContactsController@addUserContactByGuard2');
        Route::post('add-contact-in-dnc-by-resident-id', 'API\UserContactsController@addUserContactByGuard');
        Route::post('delete-contact-in-dnc-by-guard-by-resident-id', 'API\UserContactsController@deleteUserContactsByGuard');


        Route::group(['middleware' => 'checkKioskApi'], function () {
            Route::get('test', function () {
                return response()->json("Kiosk rights");
            });
        });

    });

    //set camera location
    Route::post('set-camera-location', 'API\CameraController@updateCameraLatLong');

    //Verify guard login api
    Route::post('verify-guard-login', 'API\UserController@verifyGuardLogin');


    //Forget Password api
    Route::post('forget-password', 'API\ResetPasswordController@sendResetPasswordCode');

    //Reset password api
    Route::post('send-reset-password-code', 'API\ResetPasswordController@sendResetPasswordCode');
    Route::post('check-request-email', 'API\ResetPasswordController@checkDuplicateEmail');
    Route::post('verify-reset-password-verification-code', 'API\ResetPasswordController@verifyResetPasswordVerificationCode');

});
//for scanner verification

Route::post('testing', 'API\ScannerController@testing');
Route::post('store-license-plate-to-scan-log/{scanLogId}', 'API\ScannerController@updateLicensePlateImageInScanLog');

//for rfid
Route::middleware(['zKeyBasicAuthScannerRfid'])->group(function () {
    Route::get('get-rfid-codes', 'API\ScannerController@getRfidCodes');
    Route::get('scanned-rfid-code/{rfidcode}', 'API\ScannerController@scannedRfidCode');
    Route::get('get_all_rf_code', 'API\ScannerController@getRfidCodes');
    Route::get('scanned_rf_code/{rfcode}', 'API\ScannerController@scannedRfidCode');
});

Route::middleware(['checkNode'])->group(function () {
    Route::get('ping', 'Node\ZuulNodeController@pong');
    Route::post('socket-connection', 'Node\ZuulNodeController@socketConnection');
    Route::post('request-for-socket-connection', 'Node\ZuulNodeController@guardrequest'); //->middleware('auth:api');
    Route::post('image-store', 'Node\ZuulNodeController@imageStore');
    Route::post('get-image', 'Node\ZuulNodeController@getImage')->middleware('auth:api');
    Route::post('auth-camera', 'Node\ZuulNodeController@authcamera');
});

Route::post('remote-guard/ping', "Node\RemoteGuardController@ping");
Route::post('remote-guard/type', "Node\RemoteGuardController@type");
Route::get('remote-guard/communities', "Node\RemoteGuardController@communities");
Route::post('remote-guard/add', "Node\RemoteGuardController@addRemoteGuard");
Route::post('remote-guard/keysgenerate', "Node\RemoteGuardController@keysgenerate");
Route::post('remote-guard/update', "Node\RemoteGuardController@update");
Route::get('remote-guard/all', "Node\RemoteGuardController@all");


Route::resource('iono-heartbeat-logs', 'API\IonoHeartbeatLogController');

