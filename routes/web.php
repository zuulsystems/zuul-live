<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('create-quick-pass', 'API\GuardController@createQuickPass2');


Route::get('/reset-app', function () {
    $code = request()->input('code') ?? "";
    return view('auth.reset_password')->with('code', $code);
})->name('reset-app');
Route::get('testings', 'Frontend\TestController@index')->name('testing');
Route::get('fire-base-testing', 'Frontend\TestController@index')->name('fire-base-testing');

Route::get('/clear_cache', function () {
    $exitCode = Illuminate\Support\Facades\Artisan::call('cache:clear');
    $exitCode .= Illuminate\Support\Facades\Artisan::call('view:clear');
});
Route::get('ring-central', 'API\RingCentralController@viewLoad');
Route::get('ring-central-voice-messages', 'API\RingCentralController@ringCentralVoiceMessageView');

Route::get('nd/{notificationId}', 'Frontend\DashboardController@notificationDetail')->name('notification-detail');
Route::get('notification-submit', 'Frontend\DashboardController@notificationSubmit')->name('notification-submit');
Route::get('anonymous-request-a-pass', 'Frontend\FrontendPassRequestController@index')->name('anonymous-request-a-pass');
Route::post('anonymous-request-a-pass-submit', 'Frontend\FrontendPassRequestController@store')->name('anonymous-request-a-pass-submit');

Route::resource('two-factor-verification', 'TwoFactorVerification');

Route::get('password-reset', 'Frontend\PasswordController@passwordReset')->name('password-reset');
Route::post('password-reset-save', 'Frontend\PasswordController@passwordResetPost')->name('password-reset-save');

// new addition reset password @mazhar
Route::post('reset-password', 'Admin\UserController@resetPassword');


Route::get('get-contact-in-dnc-by-resident-id/{id}', 'API\UserContactsController@getContactInDncByResidentId2')->name('dnc-by-resident');

//admin routes
Route::group(['middleware' => ['auth:web', 'admin', 'adminDeactivate'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::post('get-remote-guard-keys', 'Admin\RemoteGuardController@getRemoteGuardKeys')->name('get-remote-guard-keys');


    Route::get('/', 'Admin\DashboardController@index')->name('dashboard')->middleware('checkGuardAdmin');
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard')->middleware('checkGuardAdmin');

    Route::resource('dashboard-setting', 'Admin\DashboardSettingController')->middleware('checkGuardAdmin');

    Route::resource('connected-pi', 'Admin\ConnectedPiController');
    Route::get('connected-pi-data', 'Admin\ConnectedPiController@connectedPiData')->name('connected-pi-data');
    Route::resource('passes', 'Admin\PassController');
    Route::get('passes-data', 'Admin\PassController@passesData')->name('passes-data');
    Route::post('pass-recipients', 'Admin\PassController@passRecipients')->name('pass-recipients');

    Route::resource('network', 'Admin\NetworkController');
    Route::get('network-data', 'Admin\NetworkController@networkData')->name('network-data');

    Route::resource('remote-guard', 'Admin\RemoteGuardController');
    Route::get('remote-guard-data', 'Admin\RemoteGuardController@remoteGuardData')->name('remote-guard-data');

    Route::group(['middleware' => 'checkKiosk'], function () {

        Route::get('queuejob', function () {
            Artisan::call('queue:work');
        });
        // Profile
        Route::resource('password', 'Admin\PasswordController');
        Route::resource('profile', 'Admin\ProfileController');

        Route::resource('pre-approved', 'Admin\PreApproveController');
        Route::get('pre-approved-data', 'Admin\PreApproveController@preApprovedData')->name('pre-approved-data');

        Route::resource('iono-heartbeat', 'Admin\IonoHeartbeatLogController');
        Route::get('iono-heartbeat-logs-data', 'Admin\IonoHeartbeatLogController@ionoHeartbeatLogsData')->name('iono-heartbeat-logs-data');

        //csv notifications
        Route::post('csv-notifications', 'Admin\DashboardController@csvNotifications')->name('csv-notifications');
        Route::post('csv-notification-viewed', 'Admin\DashboardController@csvNotificationViewed')->name('csv-notification-viewed');

    });

    //users routes
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {

        //guests routes
        Route::resource('guests', 'Admin\UserController')->middleware(['checkGuardAdmin']);
        Route::get('/banned-guests', 'Admin\UserController@bandGuest')->middleware(['checkGuardAdmin'])->name('banned-guests'); // 'checkCommunityAdmin',

        Route::post('/add-banned-user', 'Admin\UserController@addbandGuest')->middleware(['checkGuardAdmin'])->name('add-banned-user');
        Route::get('/banned-user', 'Admin\UserController@addbandGuestForm')->name('add-banned-user-form');
        Route::get('/guest-data', 'Admin\UserController@guestData')->name('guest-data');
        Route::get('/guest-band-data', 'Admin\UserController@guestBandData')->name('guest-band-data');
        Route::get('/get-community-list', 'Admin\UserController@getCommunity')->middleware(['checkGuardAdmin'])->name('get-community-list');
        Route::post('resend-temporary-password-guest/{id}', 'Admin\UserController@resendTemporaryPassword')->name('resend-temporary-password-guest');
        //revoke resident
        Route::post('revoke-resident', 'Admin\UserController@revokeResident')->name('revoke-resident');
        Route::post('delete-guest/{userId}', 'Admin\UserController@deleteGuest')->name('delete-guest');

        Route::post('check-guest-permission', 'Admin\UserController@checkGuestPermission')->name('check-guest-permission');
        Route::post('check-banned-permission', 'Admin\UserController@checkBannedPermission')->name('check-banned-permission');

        Route::post('assign-banned-permission', 'Admin\UserController@assignBannedPermission')->name('assign-banned-permission');
        Route::post('assign-guest-permission', 'Admin\UserController@assignGuestPermission')->name('assign-guest-permission');
        Route::post('make-resident', 'Admin\UserController@makeResident')->name('make-resident');
        Route::post('get-user', 'Admin\UserController@getUser')->name('get-user');
        Route::post('suspend-guest/{userId}', 'Admin\UserController@suspendGuest')->name('suspend-guest');
        Route::post('unsuspend-guest/{userId}', 'Admin\UserController@unsuspendGuest')->name('unsuspend-guest');

        //community admins routes
        Route::resource('/community-admins', 'Admin\CommunityAdminController')->middleware('checkGuardAdmin');
        Route::get('/community-admins-data', 'Admin\CommunityAdminController@communityAdminsData')->name('community-admins-data');
        Route::post('delete-community-admin/{userId}', 'Admin\CommunityAdminController@deleteCommunityAdmin')->name('delete-community-admin');
        Route::post('deactivate-admin-status', 'Admin\CommunityAdminController@deactivateCommunityAdmin')->name('deactivate-community-admin');
        Route::post('activate-admin-status', 'Admin\CommunityAdminController@activateCommunityAdmin')->name('activate-community-admin');

        Route::group(['middleware' => 'checkKiosk'], function () {

            //Mark:- Kiosk Admin Routes
            Route::resource('/kiosk-admins', 'Admin\KioskAdminController')->middleware('checkGuardAdmin');
            Route::get('/kiosk-admins-data', 'Admin\KioskAdminController@kioskAdminsData')->name('kiosk-admins-data');
            Route::post('delete-kiosk-admin/{userId}', 'Admin\KioskAdminController@deleteKioskAdmin')->name('delete-kiosk-admin');
            Route::post('deactivate-kiosk-status', 'Admin\KioskAdminController@deactivateKioskAdmin')->name('deactivate-kiosk-admin');
            Route::post('activate-kiosk-status', 'Admin\KioskAdminController@activateKioskAdmin')->name('activate-kiosk-admin');

        });

        //security guards routes
        Route::resource('guards', 'Admin\GuardController')->middleware('checkGuardAdmin');
        Route::get('/guards-data', 'Admin\GuardController@guardData')->name('guards-data');

        Route::group(['middleware' => 'checkKiosk'], function () {

            Route::post('/send-reset-link', 'Admin\GuardController@sendResetLink')->name('send-reset-link');
            Route::post('/check-guard-permission', 'Admin\GuardController@checkGuardPermission')->name('check-guard-permission');
            Route::post('/guard-permission', 'Admin\GuardController@guardPermission')->name('guard-permission');
            Route::post('delete-guard/{userId}', 'Admin\GuardController@deleteGuard')->name('delete-guard');
            Route::post('deactivate-guard-status', 'Admin\GuardController@deactivateGuard')->name('deactivate-guard');
            Route::post('activate-guard-status', 'Admin\GuardController@activateGuard')->name('activate-guard');
            //revoke guard
            Route::post('revoke-guard', 'Admin\GuardController@revokeGuard')->name('revoke-guard');
            Route::post('resend-guard-verification-code', 'Admin\GuardController@resendGuardVerificationCode')->name('resend-guard-verification-code');

            Route::group(['prefix' => 'guard-appoint', 'as' => 'guard-appoint.'], function () {
                Route::resource('appoint-locations', 'Admin\GuardAppointLocationController')->middleware('checkGuardAdmin');
                Route::get('guard-appoint-location-data', 'Admin\GuardAppointLocationController@guardAppointLocationData')->name('guard-appoint-location-data');
            });
        });

    });


    //residential prefix
    Route::group(['prefix' => 'residential', 'as' => 'residential.'], function () {

        //communities routes
        Route::resource('communities', 'Admin\CommunityController');
        Route::get('communities-data', 'Admin\CommunityController@communityData')->name('communities-data');
        Route::post('deactive-community', 'Admin\CommunityController@deactivateCommunity')->name('deactive-community')->middleware('can:super-admin');
        Route::post('active-community', 'Admin\CommunityController@activeCommunity')->name('active-community')->middleware('can:super-admin');
        Route::post('fetch-community-locations/{communityId}', 'Admin\CommunityController@fetchCommunityLocations')->name('fetch-community-locations')->middleware('can:super-admin');
        Route::get('change-community-status/{communityId}', 'Admin\CommunityController@changeCommunityStatus')->name('change-community-status')->middleware('can:super-admin');
        //houses routes
        Route::resource('houses', 'Admin\HouseController');
        Route::get('houses-data', 'Admin\HouseController@houseData')->name('houses-data');
        Route::post('import-houses', 'Admin\HouseController@importHouses1')->name('import-houses');
        Route::get('houses-imported-files', 'Admin\HouseController@importedFiles')->name('house-imported-files');
        Route::get('houses-imported-file-data', 'Admin\HouseController@importedFileData')->name('houses-imported-file-data');
        Route::get('delete-house-imported-file', 'Admin\HouseController@deleteImportedFile')->name('delete-house-imported-file');
        Route::get('download-house-imported-file', 'Admin\HouseController@downloadImportedFile')->name('download-house-imported-file');
        Route::get('download-fail-records-house-imported-file', 'Admin\HouseController@downloadFailRecordsImportedFile')->name('download-fail-records-house-imported-file');
        Route::get('houses-imported-saved-records', 'Admin\HouseController@houseImportedFileSavedRecords')->name('house-imported-file-saved-records');
        Route::get('houses-imported-imported-file-saved-records-data', 'Admin\HouseController@houseImportedFileSavedRrecordsData')->name('houses-imported-file-saved-records-data');
        Route::get('save-houses-from-house-csvs-table', 'Admin\HouseController@saveHousesFromHouseCsvsTable')->name('save-houses-from-house-csvs-table');

        Route::get('houses-import-file-information', 'Admin\HouseController@housesImportFileInformation')->name('houses-import-file-information');
        Route::get('houses-import-file-information-data', 'Admin\HouseController@housesImportFileInformationData')->name('houses-import-file-information-data');
        Route::get('house-csvs-table-record-edit/{id}', 'Admin\HouseController@houseCsvsRecordEdit')->name('house-csvs-table-record-edit');
        Route::post('house-csvs-table-record-update/{id}', 'Admin\HouseController@houseCsvsRecordUpdate')->name('house-csvs-table-record-update');

        // house-csv-files is table name
        Route::post('house-csv-files-delete/{id}', 'Admin\HouseController@houseCsvFilesDelete')->name('house-csv-files-delete');

        // house-csvs table record delete
        Route::post('house-csvs-delete/{id}', 'Admin\HouseController@houseCsvsDelete')->name('house-csvs-delete');
        Route::get('dump-to-houses-total-number-family-members', 'Admin\HouseController@updateHousesfromResidentTable');

        Route::get('house-completed-profile-column-dump', 'Admin\HouseController@houseCompletedProfileColumnDump');

        //residents routes
        Route::resource('residents', 'Admin\ResidentController');
        Route::get('residents-data', 'Admin\ResidentController@residentData')->name('resident-data');
        Route::post('delete-resident/{userId}', 'Admin\ResidentController@deleteResident')->name('delete-resident');

        Route::group(['middleware' => 'checkKiosk'], function () {


        });

        Route::post('get-house-by-community-id', 'Admin\ResidentController@getHouseByCommunityId')->name('get-house-by-community-id');

        //Announcement routes
        Route::resource('announcements', 'Admin\AnnouncementController')->middleware(['checkGuardAdmin', 'checkCommunityAdmin']);
        Route::get('announcements-data', 'Admin\AnnouncementController@announcementData')->name('announcements-data');

        Route::group(['middleware' => 'checkKiosk'], function () {

            //delete community residents 27-7-2022
            Route::get('delete-community-residents-house-members', 'Admin\ResidentController@deleteCommunityResidentsHouseMembers');
            Route::get('delete-community-residents-house-head', 'Admin\ResidentController@deleteCommunityResidentsHouseHead');

            Route::post('resend-temporary-password/{id}', 'Admin\ResidentController@resendTemporaryPassword')->name('resend-temporary-password');
            Route::get('imported-files', 'Admin\ResidentController@importedFiles')->name('imported-files');
            Route::get('delete-imported-file', 'Admin\ResidentController@deleteImportedFile')->name('delete-imported-file');
            Route::get('download-imported-file', 'Admin\ResidentController@downloadImportedFile')->name('download-imported-file');
            Route::get('download-fail-records-imported-file', 'Admin\ResidentController@downloadFailRecordsImportedFile')->name('download-fail-records-imported-file');

            Route::post('check-resident-permission', 'Admin\ResidentController@checkResidentPermission')->name('check-resident-permission');
            Route::post('assign-resident-permission', 'Admin\ResidentController@assignResidentPermission')->name('assign-resident-permission');
            //import-residents
            Route::post('import-residents', 'Admin\ResidentController@importResident')->name('import-residents');
            Route::get('imported-file-data', 'Admin\ResidentController@importedFileData')->name('imported-file-data');

            Route::get('resident-imported-file-saved-records', 'Admin\ResidentController@residentImportedFileSavedRecords')->name('resident-imported-file-saved-records');
            Route::get('resident-imported-file-saved-records-data', 'Admin\ResidentController@residentImportedFileSavedRecordsData')->name('resident-imported-file-saved-records-data');
            Route::get('save-residents-from-resident-csvs-table', 'Admin\ResidentController@saveResidentsFromResidentCsvsTable')->name('save-residents-from-resident-csvs-table');

            Route::get('resident-import-file-information', 'Admin\ResidentController@residentImportFileInformation')->name('resident-import-file-information');
            Route::get('resident-import-file-information-data', 'Admin\ResidentController@residentImportFileInformationData')->name('resident-import-file-information-data');

            Route::get('resident-csvs-table-record-edit/{id}', 'Admin\ResidentController@residentCsvsTableRecordEdit')->name('resident-csvs-table-record-edit');
            Route::post('resident-csvs-table-record-update/{id}', 'Admin\ResidentController@residentCsvsRecordUpdate')->name('resident-csvs-table-record-update');

            // resident-csv-files is table name
            Route::post('resident-csv-files-delete/{id}', 'Admin\ResidentController@residentCsvFilesDelete')->name('resident-csv-files-delete');

            // to delete resident-csvs table record
            Route::post('resident-csvs-delete/{id}', 'Admin\ResidentController@residentCsvsDelete')->name('resident-csvs-delete');

            Route::get('test', 'Admin\ResidentController@test');

            //revoke resident
            Route::post('revoke-resident', 'Admin\ResidentController@revokeResident')->name('revoke-resident');

            Route::post('get-resident', 'Admin\ResidentController@getResident')->name('get-resident');

            //additional contacts
            Route::post('get-resident-contacts', 'Admin\ResidentController@getResidentContact')->name('get-resident-contacts');

            //suspend resident
            Route::post('suspend-resident', 'Admin\ResidentController@suspendResident')->name('suspend-resident');

            Route::post('house-by-address', 'Admin\ResidentController@houseByAddress')->name('house-by-address');

            Route::get('/test1', 'Admin\ResidentController@test1');
            Route::post('/save-token', 'Admin\ResidentController@saveToken')->name('save-token');
            Route::post('/send-notification', 'Admin\ResidentController@sendNotification')->name('send.notification');

            Route::get('/test2', 'Admin\ResidentController@test2');

            // Approve List
            Route::resource('approve-list', 'Admin\ApproveListController');
            Route::get('approve-list-byid/{id}', 'Admin\ApproveListController@createdbyResedentID')->name('approve-list-byid');
            Route::get('approved-data/{id}', 'Admin\ApproveListController@residentData')->name('approved-data');
            Route::post('delete-approved/{contract_id}/{resident_id}', 'Admin\ApproveListController@destroy')->name('delete-approve-by-guard');


        });

    });

//    });

    //setting prefix
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {

        //dashboard setting routes
        Route::resource('dashboard-setting', 'Admin\DashboardSettingController')->middleware('checkGuardAdmin');

        Route::group(['middleware' => 'checkKiosk'], function () {

            Route::get('dashboard-setting-data', 'Admin\DashboardSettingController@dashboardData')->name('dashboard-setting-data');
            Route::post('dashboard-setting-web-relay', 'Admin\DashboardSettingController@enableWebRelay')->name('dashboard-setting-web-relay');

            //email templates routes
            Route::resource('email-templates', 'Admin\EmailTemplateController')->middleware(['checkCommunityAdmin', 'checkGuardAdmin']);
            Route::get('email-templates-data', 'Admin\EmailTemplateController@emailTemplateData')->name('email-templates-data');

            Route::get('guest-arrival-email', 'Admin\EmailTemplateController@guestArrivalEmail');

            //printer templates routes
            Route::resource('printer-templates', 'Admin\PrinterTemplateController');
            Route::get('printer-templates-data', 'Admin\PrinterTemplateController@printerTemplateData')->name('printer-templates-data');
            Route::post('delete-printer-template/{userId}', 'Admin\PrinterTemplateController@deletePrinterTemplate')->name('delete-printer-template');


        });

        //scanners routes
        Route::resource('scanners', 'Admin\ScannerController')->middleware('checkGuardAdmin');

        //guest logs routes
        Route::get('scan-logs', 'Admin\GuestLogController@index');


        Route::get('scanners-data', 'Admin\ScannerController@scannerData')->name('scanners-data');

        Route::post('get-cameras-by-community', 'Admin\CameraController@getCamerasByCommunity')->name('get-cameras-by-community');


        //auth logs routes
        Route::get('auth-logs', 'Admin\AuthLogController@index')->name('auth-logs')->middleware('checkCommunityAdmin', 'checkGuardAdmin');

        Route::get('auth-logs-data', 'Admin\AuthLogController@authLogData')->name('auth-logs-data');
        Route::group(['middleware' => 'checkKiosk'], function () {

            //offensive words routes
            Route::resource('offensive-words', 'Admin\OffensiveWordController')->middleware('checkGuardAdmin');
            Route::get('offensive-words-data', 'Admin\OffensiveWordController@offensiveWordsData')->name('offensive-words-data');


        });
        //camera routes
        Route::resource('cameras', 'Admin\CameraController')->middleware('checkGuardAdmin');
        Route::get('cameras-data', 'Admin\CameraController@cameraData')->name('cameras-data');
        Route::resource('web-relay-tracking', 'Admin\WebRelayTrackingController');
        Route::get('web-relay-tracking-data', 'Admin\WebRelayTrackingController@webRelayTrackingData')->name('web-relay-tracking-data');


        Route::group(['middleware' => 'checkKiosk'], function () {

            Route::resource('web-relays', 'Admin\WebRelayController');
            Route::get('web-relays-data', 'Admin\WebRelayController@webRelayData')->name('web-relays-data');

            //Mark:- Remote Web Relay Trackings
            Route::resource('remote-web-relay-tracking', 'Admin\RemoteWebRelayTrackingController');
            Route::get('remote-web-relay-tracking-data', 'Admin\RemoteWebRelayTrackingController@webRelayTrackingData')->name('remote-web-relay-tracking-data');

        });
    });


    Route::get('guest-logs-data', 'Admin\GuestLogController@guestLogData')->name('guest-logs-data');

    //request passes routes
    Route::get('pass-requests', 'Admin\PassRequestController@index')->name('pass-requests');
    Route::get('pass-requests-data', 'Admin\PassRequestController@passRequestData')->name('pass-requests-data');

    Route::group(['middleware' => 'checkKiosk'], function () {

        //traffic tickets prefix
        Route::group(['middleware' => 'checkGuardAdmin', 'prefix' => 'traffic-tickets', 'as' => 'traffic-tickets.'], function () {

            //community tickets
            Route::resource('tickets-logix-communities', 'Admin\CommunityTicketController');
            Route::get('tickets-logix-communities-data', 'Admin\CommunityTicketController@communityTicketData')->name('tickets-logix-communities-data');
            Route::get('fetch-tickets-by-community/{id}', 'Admin\CommunityTicketController@fetchCommunityTicketData')->name('fetch-tickets-by-community');

            //dashboard setting routes
            Route::resource('resident-tickets', 'Admin\ResidentTicketController')->middleware('checkGuardAdmin');
            Route::get('resident-ticket-data', 'Admin\ResidentTicketController@residentTicketData')->name('resident-tickets-data');
            Route::post('resident-ticket-data', 'Admin\ResidentTicketController@residentTicketData');
            Route::get('resident-ticket-details', 'Admin\ResidentTicketController@ticketDetails')->name('resident-ticket-details');
            Route::get('resident-ticket-Images', 'Admin\ResidentTicketController@ticketImages')->name('resident-ticket-images');
            Route::get('resident-email-templates', 'Admin\ResidentTicketController@getTicketTemplatesByCommunity')->name('resident-email-templates');
            Route::post('resident-email-send', 'Admin\ResidentTicketController@sendEmail')->name('resident-email-send');
            Route::post('resident-payment', 'Admin\ResidentTicketController@payment')->name('resident-payment');
            Route::patch('resident-restore/{id}', 'Admin\ResidentTicketController@restore')->name('resident-tickets.restore');


            //dashboard setting routes
            Route::resource('guest-tickets', 'Admin\GuestTicketController')->middleware('checkGuardAdmin');
            Route::get('guest-tickets-data', 'Admin\GuestTicketController@guestTicketData')->name('guest-tickets-data');
            Route::post('guest-tickets-data', 'Admin\GuestTicketController@guestTicketData');
            Route::get('guest-ticket-details', 'Admin\GuestTicketController@ticketDetails')->name('guest-ticket-details');
            Route::get('guest-ticket-Images', 'Admin\GuestTicketController@ticketImages')->name('guest-ticket-images');
            Route::get('guest-email-templates', 'Admin\GuestTicketController@getTicketTemplatesByCommunity')->name('guest-email-templates');
            Route::post('guest-email-send', 'Admin\GuestTicketController@sendEmail')->name('guest-email-send');
            Route::post('guest-payment', 'Admin\GuestTicketController@payment')->name('guest-payment');
            Route::patch('guest-restore/{id}', 'Admin\GuestTicketController@restore')->name('guest-tickets.restore');


            //dashboard setting routes
            Route::resource('vendor-tickets', 'Admin\VendorTicketController')->middleware('checkGuardAdmin');
            Route::get('vendor-tickets-data', 'Admin\VendorTicketController@vendorTicketData')->name('vendor-tickets-data');
            Route::post('vendor-tickets-data', 'Admin\VendorTicketController@vendorTicketData');
            Route::get('vendor-ticket-details', 'Admin\VendorTicketController@ticketDetails')->name('vendor-ticket-details');
            Route::get('vendor-ticket-Images', 'Admin\VendorTicketController@ticketImages')->name('vendor-ticket-images');
            Route::get('vendor-email-templates', 'Admin\VendorTicketController@getTicketTemplatesByCommunity')->name('vendor-email-templates');
            Route::post('vendor-email-send', 'Admin\VendorTicketController@sendEmail')->name('vendor-email-send');
            Route::post('vendor-payment', 'Admin\VendorTicketController@payment')->name('vendor-payment');
            Route::patch('vendor-restore/{id}', 'Admin\VendorTicketController@restore')->name('vendor-tickets.restore');


            //dashboard setting routes
            Route::resource('unmatched-tickets', 'Admin\UnmatchedTicketController')->middleware('checkGuardAdmin');
            Route::get('unmatched-tickets-data', 'Admin\UnmatchedTicketController@unmatchedTicketData')->name('unmatched-tickets-data');
            Route::post('unmatched-tickets-data', 'Admin\UnmatchedTicketController@unmatchedTicketData');
            Route::get('unmatched-ticket-details', 'Admin\UnmatchedTicketController@ticketDetails')->name('unmatched-ticket-details');
            Route::get('unmatched-ticket-Images', 'Admin\UnmatchedTicketController@ticketImages')->name('unmatched-ticket-images');
            Route::post('unmatched-payment', 'Admin\UnmatchedTicketController@payment')->name('unmatched-payment');
            Route::post('unmatched-license-change', 'Admin\UnmatchedTicketController@changeLicense')->name('unmatched-license-change');
            Route::get('unmatched-attach-user', 'Admin\UnmatchedTicketController@getAttachUser')->name('unmatched-attach-user');
            Route::post('unmatched-attach-user', 'Admin\UnmatchedTicketController@attachUser');
            Route::get('unmatched-attach-license', 'Admin\UnmatchedTicketController@getAttachLicense')->name('unmatched-attach-license');
            Route::get('tickets-by-license-plate/{id}', 'Admin\UnmatchedTicketController@ticketsByLicense')->name('tickets-by-license-plate');
            Route::get('unmatched-email-templates', 'Admin\UnmatchedTicketController@getTicketTemplatesByCommunity')->name('unmatched-email-templates');
            Route::post('unmatched-email-send', 'Admin\UnmatchedTicketController@sendEmail')->name('unmatched-email-send');
            Route::post('bulk-email-send', 'Admin\UnmatchedTicketController@sendBulkEmail')->name('bulk-email-send');
            Route::patch('restore/{id}', 'Admin\UnmatchedTicketController@restore')->name('unmatched-tickets.restore');
            Route::post('bulk-email-print', 'Admin\UnmatchedTicketController@printBulkEmail')->name('bulk-email-print');
            Route::post('bulk-dismissed-ticket', 'Admin\UnmatchedTicketController@bulkDismissedTicket')->name('bulk-dismissed-ticket');

            Route::get('delete-unmatched-records-nv-community', 'Admin\UnmatchedTicketController@deleteUnmatchedRecordsNvCommunity');

            //traffic logics email templates
            Route::resource('tickets-logix-email-templates', 'Admin\TrafficEmailTemplateController');
            Route::get('tickets-logix-email-templates-data', 'Admin\TrafficEmailTemplateController@trafficEmailTemplateData')->name('tickets-logix-email-templates-data');

        });

        //email logs routes
        Route::resource('email-logs', 'Admin\EmailLogController')->middleware('checkGuardAdmin');
        Route::get('email-logs-data', 'Admin\EmailLogController@emailLogsData')->name('email-logs-data');

        //traffic payment logs routes
        Route::get('traffic-payment-logs-data', 'Admin\TrafficPaymentLogController@trafficPaymentLogsData')->name('traffic-payment-logs-data');
        Route::resource('traffic-payment-logs', 'Admin\TrafficPaymentLogController')->middleware('checkGuardAdmin');

        //location routes
        Route::resource('locations', 'Admin\LocationController')->only(['index', 'update', 'show'])->middleware('checkGuardAdmin');
        Route::get('locations-data', 'Admin\LocationController@locationsData')->name('locations-data');

        // Profile
        Route::resource('profile', 'Admin\ProfileController');

        // Password
        Route::resource('password', 'Admin\PasswordController');


        //guest logs routes
        Route::resource('guest-logs', 'Admin\GuestLogController')->middleware('checkGuardAdmin');


    });

    Route::group(['middleware' => 'checkGuardAdmin', 'prefix' => 'rfid', 'as' => 'rfid.'], function () {
        //rfids
        Route::resource('rfids', 'Admin\RfidController');
        Route::get('rfids-data', 'Admin\RfidController@rfidData')->name('rfids-data');
        Route::get('get-all-member-by-house-id', 'Admin\RfidController@getAllUsersByHouseId')->name('get-all-member-by-house-id');

        Route::get('save-vehicle-info-to-rfids-table', 'Admin\RfidController@saveVehicleInfoData');

        Route::post('import-rfid-tag1', 'Admin\RfidController@importRfidTag1')->name('import-rfid-tag');
        Route::post('suspendOrUnsuspend', 'Admin\RfidController@suspendOrUnsuspend')->name('suspendOrUnsuspend');
        Route::get('rfids-imported-files', 'Admin\RfidController@importedFiles')->name('rfid-imported-files');
        Route::get('rfids-imported-file-data', 'Admin\RfidController@importedFileData')->name('rfids-imported-file-data');
        Route::get('delete-rfid-imported-file', 'Admin\RfidController@deleteImportedFile')->name('delete-rfid-imported-file');
        Route::get('download-rfid-imported-file', 'Admin\RfidController@downloadImportedFile')->name('download-rfid-imported-file');
        Route::get('download-fail-records-rfid-imported-file', 'Admin\RfidController@downloadFailRecordsImportedFile')->name('download-fail-records-rfid-imported-file');

        Route::get('rfid-imported-file-saved-records', 'Admin\RfidController@rfidImportedFileSavedRecords')->name('rfid-imported-file-saved-records');
        Route::get('rfid-imported-file-saved-records-data', 'Admin\RfidController@rfidImportedFileSavedRecordsData')->name('rfid-imported-file-saved-records-data');
        Route::get('save-rfids-from-rfid-csvs-table', 'Admin\RfidController@saveRfidsFromRfidCsvsTable')->name('save-rfids-from-rfid-csvs-table');

        Route::get('rfid-import-file-information', 'Admin\RfidController@rfidImportFileInformation')->name('rfid-import-file-information');
        Route::get('rfid-import-file-information-data', 'Admin\RfidController@rfidImportFileInformationData')->name('rfid-import-file-information-data');

        Route::get('rfid-csvs-table-record-edit/{id}', 'Admin\RfidController@rfidCsvsTableRecordEdit')->name('rfid-csvs-table-record-edit');
        Route::post('rfid-csvs-table-record-update/{id}', 'Admin\RfidController@rfidCsvsRecordUpdate')->name('rfid-csvs-table-record-update');

        // rfid_csv_files table delete record
        Route::post('rfid-csv-files-delete/{id}', 'Admin\RfidController@rfidCsvFilesDelete')->name('rfid-csv-files-delete');

        // rfid-csvs table record delete
        Route::post('rfid-csvs-delete/{id}', 'Admin\RfidController@rfidCsvsDelete')->name('rfid-csvs-delete');


        Route::get('test', 'Admin\RfidController@readImportedFile');
        //RfidSchduleController
        Route::resource('rfids-schedules', 'Admin\RfidSchduleController');
        Route::get('rfids-schedule-data', 'Admin\RfidSchduleController@rfidScheduleData')->name('rfids-schedule-data');
        Route::post('set-rfid-schedule-week', 'Admin\RfidSchduleController@setRfidScheduleWeek')->name('set-rfid-schedule-week');
        Route::get('get-rfid-schedule-week', 'Admin\RfidSchduleController@getRfidScheduleWeek')->name('get-rfid-schedule-week');
        Route::post('delete-rfid-schedule-week', 'Admin\RfidSchduleController@deleteRfidScheduleWeek')->name('delete-rfid-schedule-week');

        Route::resource('rfid-logs', 'Admin\RfidLogController');
        Route::get('rfid-logs-data', 'Admin\RfidLogController@rfidLogData')->name('rfid-logs-data');


        Route::resource('logs', 'Admin\LogsController');
        Route::get('logs-data', 'Admin\LogsController@LogsData')->name('logs-data');

        Route::get('dump-rfid-logs-to-static-rfid-logs-table', 'Admin\RfidLogController@dumpRfidLogsToStaticRfidLogsTable');
    });

    Route::group(['middleware' => 'checkKiosk'], function () {
        //vendors routes
        Route::resource('vendors', 'Admin\VendorController')->middleware('checkGuardAdmin');
        Route::get('vendors-data', 'Admin\VendorController@vendorsData')->name('vendors-data');

        Route::get('vendor-vehicles', 'Admin\VendorController@vendorVehicles')->name('vendor-vehicles')->middleware('checkGuardAdmin');
        Route::get('vendor-vehicles-data', 'Admin\VendorController@vendorVehiclesData')->name('vendor-vehicles-data');

        //vehicles routes
        Route::resource('vehicles', 'Admin\VehicleController')->middleware('checkGuardAdmin');
        Route::get('vehicles-data', 'Admin\VehicleController@vehiclesData')->name('vehicles-data');
        Route::post('attach-vehicle-with-vendor', 'Admin\VehicleController@attachVehicleWithVendor')->name('attach-vehicle-with-vendor');

        Route::get('traffic_logix/get_traffic_summary', 'Admin\TrafficController@get_traffic_summary')->name('trafficlogix.get_traffic_summary');
        Route::get('getCameraLocationMapData', 'Admin\TrafficController@getCameraLocationMapData')->name('getCameraLocationMapData');

        //Dashboard Scan Log Charts
        Route::post('getDashboardScanLogsCharts', 'Admin\DashboardController@getDashboardScanLogsCharts')->name('post_dashboard');

        //Recent Notifications
        Route::post('getAdminDashboardNotifications', 'Admin\DashboardController@getAdminDashboardNotifications')->name('get-admin-dashboard-notifications');

        //Dashboard Camera Location Charts
        Route::post('get-traffic-summary', 'Admin\DashboardController@getTrafficSummary')->name('get-admin-dashboard-notifications');

    });

});

Route::get('/', 'Frontend\DashboardController@login');

//frontend routes
Route::group(['middleware' => ['auth:web', 'frontend']], function () {
    Route::resource('account-setting', 'Frontend\AccountSettingsController');
    Route::post('store-browser-token', 'Frontend\DashboardController@storeBrowserToken')->name('store-browser-token');

    //check if user completed his profile or not
    Route::middleware('checkProfileCompleted')->group(function () {
        Route::resource('change-password', 'Frontend\PasswordController');

        Route::get('/dashboard', 'Frontend\DashboardController@index')->name('dashboard');
        Route::get('get-passes-by-type', 'Frontend\DashboardController@getPassByType')->name('get-passes-by-type');
        Route::get('sent-test-message', 'Frontend\DashboardController@sentTestMessage')->name('sent-test-message');

        // Mark:- Get QR Code Info For Messages
        Route::get('qr-code/{passId}', 'Frontend\DashboardController@qrCode');

        //vehicles routes
        Route::resource('vehicles', 'Frontend\VehicleController');
        Route::get('vehicles-data', 'Frontend\VehicleController@vehiclesData')->name('vehicles-data');


        Route::resource('events', 'Frontend\EventController')->middleware('checkGuest');
        Route::get('events-data', 'Frontend\EventController@eventData')->name('events-data');


        //household members routes
        Route::resource('household-members', 'Frontend\HouseHoldMemberController')->middleware('checkGuest');
        Route::get('household-members-data', 'Frontend\HouseHoldMemberController@houseHoldMembersData')->name('household-members-data');

        Route::post('/update-user-notification-settings', 'Frontend\DashboardController@updateUserNotificationSettings')->name('update-user-notification-settings');

        //contacts routes
        Route::resource('contacts', 'Frontend\UserContactController');
        Route::get('contacts-data', 'Frontend\UserContactController@userContactData')->name('contacts-data');
        Route::post('add-contact-in-group', 'Frontend\UserContactController@addContactInGroup')->name('add-contact-in-group');
        Route::post('add-or-remove-favourite-contact', 'Frontend\UserContactController@addOrRemoveFavouriteContact');
        Route::post('send-pass-request-from-contact', 'Frontend\UserContactController@sendPassRequestFromContact')->name('send-pass-request-from-contact');

        Route::resource('pre-approved', 'Frontend\PreApproveController');
        Route::get('pre-approved-data', 'Frontend\PreApproveController@preApprovedData')->name('pre-approved-data');

        //contact groups
        Route::resource('contact-groups', 'Frontend\ContactGroupController');
        Route::get('contact-groups-data', 'Frontend\ContactGroupController@contactGroupData')->name('contact-groups-data');
        Route::post('group-contact-delete', 'Frontend\ContactGroupController@groupContactDelete')->name('group-contact-delete');

        //zuul and push notifications functionality
        Route::resource('notifications', 'Frontend\NotificationController');
        Route::get('notifications-data', 'Frontend\NotificationController@notificationsData')->name('notifications-data');
        Route::post('mark-read-notification', 'Frontend\NotificationController@markReadNotification')->name('mark-read-notification');
        Route::post('mark-all-read-notification', 'Frontend\NotificationController@markAllReadNotification')->name('mark-all-read-notification');

        //passes prefix
        Route::group(['prefix' => 'passes', 'as' => 'passes.'], function () {


            // passes
            Route::resource('passes', 'Frontend\PassController')->middleware('checkGuest');

            Route::get('passes-data', 'Admin\PassController@passesData')->name('passes-data');
            Route::post('pass-recipients', 'Admin\PassController@passRecipients')->name('pass-recipients');

            //active passes
            Route::resource('active-passes', 'Frontend\ActivePassController');
            Route::get('active-passes-data', 'Frontend\ActivePassController@activePassesData')->name('active-passes-data');
            Route::get('active-passes-dates', 'Frontend\ActivePassController@activePassesDates')->name('active-passes-dates');
            Route::post('set-user-pass-vehicle', 'Frontend\ActivePassController@setUserPassVehicle')->name('set-user-pass-vehicle');
            Route::post('passes-detail', 'Frontend\ActivePassController@getPassDetail')->name('passes-detail');

            //sent passes
            Route::resource('sent-passes', 'Frontend\SentPassController')->middleware('checkGuest');
            Route::get('sent-passes-data', 'Frontend\SentPassController@sentPassesData')->name('sent-passes-data');
            Route::get('sent-passes-dates', 'Frontend\SentPassController@sentPassesDates')->name('sent-passes-dates');
            Route::post('retract-pass-request', 'Frontend\SentPassController@retractSentPass')->name('retract-pass-request');


            //archive passes / scanned passes
            Route::resource('archive-passes', 'Frontend\ArchivePassController');
            Route::get('scanned-passes-data', 'Frontend\ArchivePassController@scannedPassesData')->name('scanned-passes-data');
            Route::get('scanned-passes-dates', 'Frontend\ArchivePassController@scannedPassesDates')->name('scanned-passes-dates');
            //pass requests
            Route::resource('pass-requests', 'Frontend\PassRequestController');
            Route::post('reject-pass-request', 'Frontend\PassRequestController@rejectPassRequest')->name('reject-pass-request');
            Route::post('accept-pass-request', 'Frontend\PassRequestController@acceptPassRequest')->name('accept-pass-request');


        });

        //parental controls
        Route::resource('parental-control', 'Frontend\ParentalController');
    });

});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('test', fn() => "Hi");

Route::post('testPost', function () {
    echo 24;
});


//Mark:- QR CODE web view for sms redirection

//For Individuals
Route::get('qr_code/{qr}', 'Frontend\PassRequestController@qr_Code')->name('qr_code');

//For Group
Route::get('qr_code/{qr}/group', 'Frontend\PassRequestController@qr_CodeGroup')->name('qr_code_group');

//For Group Data..
Route::get('qr_code/group/data/{phone}', 'Frontend\PassRequestController@qrGetGroupData')->name('qr_code_group_data');

Route::post('get-phone-number-format-by-dial-code', 'Frontend\UserContactController@getPhoneNumberFormatByDialCode')->name('get-phone-format-by-dial-code');


//Mark:- To trigger php queue command
Route::get('/run-queue', 'Admin\DashboardController@runQueue')->name('run-queue');

Route::get('/kiosk-data-dump', function () {
    dumpData();
});

