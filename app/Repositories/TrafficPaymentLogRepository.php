<?php

namespace App\Repositories;

use App\Models\TrafficPaymentLog;
use App\Repositories\RepositoryInterface;
use Exception;

class TrafficPaymentLogRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var TrafficPaymentLog
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param TrafficPaymentLog $trafficPaymentLog
     */
    public function __construct(TrafficPaymentLog $trafficPaymentLog)
    {
        $this->model = $trafficPaymentLog;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {

            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function data($community_id = null,$payUserIds = null)
    {
        try {
            $result = $this->model->with('payee:id,first_name,last_name')
                                    ->with('user:id,first_name,last_name')
                                    ->with('ticket.community:id,name')
                                    ->with('ticket:id,ticket_id,community_id');

            // For Admin and TL Admin
            if(!empty($community_id)){
                $result->whereHas('ticket', function($q) use ($community_id) {
                    $q->where('community_id', $community_id);
                });
            }

            if (!empty($payUserIds)){
                $result->whereIn('payee_user_id',$payUserIds);
            }

            return [
                'bool' => true,
                'result' => $result->get()
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

 }
