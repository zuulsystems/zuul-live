<?php

namespace App\Repositories;

use App\Models\PassRequest;
use App\Repositories\RepositoryInterface;
use App\Models\User;
use Exception;

class PassRequestRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var PassRequest
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param PassRequest $passRequest
     */
    public function __construct(PassRequest $passRequest)
    {
        $this->model = $passRequest;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $queryBuilder = $this->model::query()
                ->select('pass_requests.*')
                ->with([
                'requestedUser:id,first_name,last_name,community_id,license_image',
                'sendUser:id,first_name,last_name,license_image',
                'requestedUser.community',
                'sendUser.community',
            ]);
            //get request passes by community Id
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->whereHas('requestedUser.community', function ($community) use ($communityId) {
                    $community->where('id', $communityId);
                });
            }
            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
