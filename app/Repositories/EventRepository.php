<?php

namespace App\Repositories;

use App\Models\Event;
use App\Repositories\RepositoryInterface;
use Exception;

class EventRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var Event
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->model = $event;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getEvents($limit = null, $search = null, $userId = null)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->where(function($query) use ($userId) {
                $query->where('created_by', $userId)->orWhereNull('created_by');
            });

            if (!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', "%$search%");
                });
            }
            $result = !empty($limit)?$queryBuilder->paginate($limit):$queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getEventWithRight($userId, $id)
    {
        try {
            $result = $this->model->where('created_by', $userId)->where('id', $id)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
