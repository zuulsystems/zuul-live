<?php

namespace App\Repositories;

use App\Models\AuthLog;
use App\Models\Rfid;
use App\Models\RfidDateLimit;
use App\Models\RfidDateLimitWeek;
use App\Repositories\RepositoryInterface;
use Carbon\Carbon;
use Exception;

class RfidDateLimitRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var AuthLog
     */
    public $model;

    /**
     * RfidDateLimitRepository constructor.
     *
     * @param RfidDateLimit $rfidDateLimit
     */
    public function __construct(RfidDateLimit $rfidDateLimit)
    {
        $this->model = $rfidDateLimit;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     */
    public function all()
    {
        try {
            $queryBuilder = $this->model::with('rfidDateLimitWeeks');
            $result = $queryBuilder->orderByDesc('id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $rfidDateLimit = $this->find($id)['result'];
            if (!empty($rfidDateLimit)){
                $rfidDateLimit->rfidDateLimitWeeks()->delete();
            }
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $rfidId
     * @param $startDate
     * @param $endDate
     * @param null $id
     * @return array
     */
    public function findBetweenStartEndDates($rfidId, $startDate, $endDate, $id = null)
    {
        // Sudo code to add an entry
        // - Any time an entry is created or updated, the input range will be checked
        // - If the start of input date is found between any other schedule, it will prompt error
        // - If the end of input date is found between any other schedule, it will prompt error
        // - Otherwise it will add an entry
        try {
            $queryBuilder = $this->model::query();
            $queryBuilder->where('rfid_id', $rfidId);
            if (!empty($id)) {
                $queryBuilder->where('id', '!=', $id);
            }
            $result = $queryBuilder->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];

        }
    }

    /**
     * @param $rfidId
     * @return mixed
     */
    public function getByRfid($rfidId)
    {
        try {
            $queryBuilder = $this->model::where('rfid_id',$rfidId)->with('rfidDateLimitWeeks');
            $result = $queryBuilder->orderByDesc('id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $rfidId
     * @param $startDate
     * @param $endDate
     * @param $rfidTagNums
     * @return array
     */
    public function findBetweenStartEndDatesByRfidTagNums($rfidId, $startDate, $endDate, $rfidTagNums)
    {
        // Sudo code to add an entry
        // - Any time an entry is created or updated, the input range will be checked
        // - If the start of input date is found between any other schedule, it will prompt error
        // - If the end of input date is found between any other schedule, it will prompt error
        // - Otherwise it will add an entry
        try {
            $queryBuilder = $this->model::query()->with('rfidDateLimitWeeks');
            //$queryBuilder->where('rfid_id', $rfidId);
            $queryBuilder->whereHas('rfid', function ($rfid)use($rfidTagNums){
                $rfid->whereIn('rfid_tag_num',$rfidTagNums);
            });
            $result = $queryBuilder->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate)->first();
            if ($result){
                if ($result->rfidDateLimitWeeks->isNotEmpty()){
                    dd($result->rfidDateLimitWeeks);
                }
                dd("Adasdasd");
            }
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];

        }
    }

    /**
     *
     * @param $rfidcode
     * @return bool
     */
    public function checkIfScanTagLieInTimeRange($rfidcode){

        $rfid = Rfid::where('rfid_tag_num', $rfidcode)->first();
        if(!$rfid){
            return false;
        }

        $limits = RfidDateLimit::where('rfid_id', $rfid->id)->get();
        if($limits->isNotEmpty()){

            // limits can be more then one
            $inBetween = RfidDateLimit::query()->where('rfid_id', $rfid->id);
            $tz = $rfid->community->timezone->abbreviation ?? config('app.timezone');
            $dateRightNow = Carbon::now($tz)->format('Y-m-d');
            $inBetween->where('start_date' , '<=', $dateRightNow );
            $inBetween->where('end_date' , '<=', $dateRightNow );
            $inBetween = $inBetween->first();

            if(empty($inBetween)){
                return false;
            }

            // if found a range ... check if it has weeks enabled
            $timeInBetween = RfidDateLimitWeek::where('rfid_date_limit_id', $inBetween->id )->count();
            if($timeInBetween > 0){
                $dayRightNow = Carbon::now($tz)->format('N');
                $dayValid = RfidDateLimitWeek::where('rfid_date_limit_id', $inBetween->id );
                $dayValid = $dayValid->where('day', intval($dayRightNow) )->where('status', 1);
                $timeRightNow = Carbon::now($tz)->format('H:i:s');
                $dayValid->where('start_time' , '<=', $timeRightNow );
                $dayValid->where('end_time' , '>=', $timeRightNow );
                $dayValid = $dayValid->first();

                if(!$dayValid){
                    return false;
                }
                return true;
            }

            return true;
        }

        return true;
    }
}
