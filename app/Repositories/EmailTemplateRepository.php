<?php

namespace App\Repositories;

use App\Models\EmailTemplate;
use App\Repositories\RepositoryInterface;
use Exception;

class EmailTemplateRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var EmailTemplate
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param EmailTemplate $emailTemplate
     */
    public function __construct(EmailTemplate $emailTemplate)
    {
        $this->model = $emailTemplate;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::where('email_type','zuul');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    public function getTicketTemplatesByCommunityAndType($community_id, $type){

        // Get the Ticket Templates by Community ID
        try {
            $result = $this->model->where('community_id', $community_id)
                    ->where('email_type', $type)
                    ->with('community:id,name')
                    ->get();

            // Return the Result to User on Success
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {

            // Return the Error Message to user to user on Failure
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * @param $communityId
     * @return mixed
     */
    public function getTrafficLogixEmailTemplate($communityId = null)
    {
        try {
            $queryBuilder = $this->model;
            if (!empty($communityId)){
                $queryBuilder =  $queryBuilder->where('community_id',$communityId)->whereNotNull('community_id');
            }
            $result = $queryBuilder->where('email_type','trafficlogix')->orderByDesc('id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $communityId
     * @param $assignTo
     * @param null $id
     * @return mixed
     */
    public function getByCommunityAssignTo($communityId ,$assignTo,$id = null)
    {
        try {
            $queryBuilder = $this->model;
            if (!empty($id)){
                $queryBuilder = $queryBuilder->where('id','!=',$id);
            }
            $result = $queryBuilder->where('community_id',$communityId)
                ->whereNotNull('community_id')
                ->where('assign_to',$assignTo)
                ->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

 }
