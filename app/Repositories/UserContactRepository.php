<?php

namespace App\Repositories;

use App\Models\ContactCollection;
use App\Models\UserContact;
use App\Repositories\RepositoryInterface;
use Exception;

class UserContactRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var UserContact
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param UserContact $userContact
     */
    public function __construct(UserContact $userContact)
    {
        $this->model = $userContact;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model::with([
                'contact',
            ])->where('id', $id)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            ContactCollection::where('user_contact_id',$id)->delete(); #todo need to review for SoftDelete
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function deleteByResidentId($contact_id, $resident_id){

        try {
            $result = $this->model->where([
                'id' => $contact_id,
                'created_by' => $resident_id
            ])->delete();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }
    /**
     * @param null $userId
     * @param int  $limit
     * @param null $search
     *
     * @param null $isFavourite
     *
     * @return mixed
     */
    public function getByUserId($userId, $limit = null, $search = null, $isFavourite = false, $isDnc = false)
    {
        try {
            $queryBuilder = $this->model;

            $queryBuilder = $queryBuilder->with([
                'contact',
            ]);
            $queryBuilder = $queryBuilder->where('created_by', $userId);

            // dd($userId, $limit, $search, $isFavourite, $isDnc);

            if (!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where('contact_name', 'LIKE', "%$search%");
                    $query->OrWhereHas('contact',function ($query)use($search){
                        $query->where('phone_number', 'LIKE', "%$search%");
                    });
                });
            }


            if($isDnc){
                $queryBuilder = $queryBuilder->where('is_dnc', '1');


            }

            if ($isFavourite) {
                $queryBuilder = $queryBuilder->where('is_favourite', '1');
            }

            $queryBuilder = $queryBuilder->orderBy('contact_name','ASC');

            $result = !empty($limit)?$queryBuilder->paginate($limit):$queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * @param null $userId
     * @param int  $limit
     * @param null $search
     *
     * @param null $isFavourite
     *
     * @return mixed
     */
    public function getByUserIdAdmin($userId, $limit = null, $search = null, $isFavourite = false, $isDnc = false, $communityId = null)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'contact','createdBy'
            ]);
            $queryBuilder = $queryBuilder->where('created_by', $userId);

            if ($communityId != null){
                $queryBuilder = $queryBuilder->whereHas('createdBy', function ($createdBy)use($communityId){
                    $createdBy->where('community_id',$communityId);
                });
            }

            if($isDnc){
                $queryBuilder = $queryBuilder->where('is_dnc', '1');


            }

            if ($isFavourite) {
                $queryBuilder = $queryBuilder->where('is_favourite', '1');
            }

            $queryBuilder = $queryBuilder->orderBy('contact_name','ASC');

            $result = $queryBuilder->get();


            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    public function getSharedDncContacts($userIds)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'contact',
            ]);

            $queryBuilder = $queryBuilder->whereIn('created_by', $userIds);
            $queryBuilder = $queryBuilder->where('is_dnc', '1');
            $queryBuilder = $queryBuilder->where('is_dnc_shared', '1');

            $queryBuilder = $queryBuilder->orderBy('contact_name','ASC');

            $result = !empty($limit)?$queryBuilder->paginate($limit):$queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    public function getSharedDncContactsApi($userIds)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'contact',
            ]);

            $queryBuilder = $queryBuilder->whereIn('created_by', $userIds);
            $queryBuilder = $queryBuilder->where('is_dnc', '0');
            $queryBuilder = $queryBuilder->where('is_dnc_shared', '1');
            $queryBuilder = $queryBuilder->orderBy('contact_name','ASC');

            $result = !empty($limit)?$queryBuilder->paginate($limit):$queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            //$result = $this->model->updateOrCreate($where, $data);
            //$result = $this->model::withTrashed()->updateOrCreate($where, $data)->restore();
            $result = $this->model::withTrashed()->updateOrCreate($where, $data);
            if ($result->trashed()){
                $result->restore();
            }
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get all user contacts by user id and check if users has passes permissions
     * @param null $userId
     * @return mixed
     */
    public function getAllByUserHasPermissionCanSendPassById($userId,$search)
    {
         try {
        $queryBuilder = $this->model;
        $queryBuilder = $queryBuilder->with('contact','contact.user','contact.user.permissions','contact.user.role');
        $queryBuilder = $queryBuilder->where(function ($where){
            $where->whereHas('contact.user',function ($where){
                $where->where('role_id','4'); // Head of family
            });
            $where->OrwhereHas('contact.user.permissions',function ($where){
                $where->where('slug','can_send_passes');
            });
        });

        if ($search != null) {
            $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                $query->where('contact_name', 'LIKE', "$search%")
                    ->orWhereHas('contact', function ($subQuery) use ($search) {
                        $subQuery->where('phone_number', 'LIKE', "%$search%");
                    });
            });
        }

        $queryBuilder = $queryBuilder->where('created_by',$userId);

        $result = $queryBuilder->get();
        return [
            'bool' => true,
            'result' => $result
        ];
         } catch (Exception $exception) {
             return [
                 'bool' => false,
                 'message' => $exception->getMessage()
             ];
         }
    }

    /**
     * get by ids
     * @param array $ids
     * @return mixed
     */
    public function getByIds($ids =array()){
        return $this->model->whereIn('id',$ids)->get();
    }

    /**
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllByUserId($userId){
        return $this->model::with('contact')->where('created_by',$userId)->get();
    }

    /**
     * get by ids
     * @param $createdBy
     * @param $phoneNumber
     * @return mixed
     */
    public function getByIdAndPhoneNumber($createdBy,$phoneNumber){
        try {
            $result =  $this->model::with('contact')->where('created_by', $createdBy)
                ->whereHas('contact', function ($contact) use ($phoneNumber) {
                    $contact->where('phone_number', $phoneNumber);
                })->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get all user contacts by user id and check if users has passes permissions
     * @param $Id
     * @return mixed
     */
    public function getUserContactPermission($Id)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with('contact','contact.user','contact.user.permissions','contact.user.role');

            $queryBuilder = $queryBuilder->where(function ($where){
                $where->whereHas('contact.user',function ($where){
                    $where->where('role_id','4'); // Head of family
                });
                $where->OrwhereHas('contact.user.permissions',function ($where){
                    $where->where('slug','can_send_passes');
                });
            });

            $queryBuilder =$queryBuilder->where('user_contacts.id',$Id);
            $result = $queryBuilder->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get single user contact for user for update
     * @param $id
     * @param $
     * @param $createdBy
     * @param $phone_number
     * @return mixed
     */
    public function getSingleUserContactForUpdate($id,$createdBy,$phone_number){
        return $this->model::where('id','!=',$id)->where('created_by',$createdBy)->with('contact')->whereHas('contact',function ($where) use($phone_number){
            $where->where('phone_number',$phone_number);
        })->first();
    }
}
