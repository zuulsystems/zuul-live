<?php


namespace App\Repositories;
use App\Repositories\RepositoryInterface;
use App\Models\DashboardSetting;
use Exception;

class DashboardSettingRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var User
     */
    public $model;

    /**
     * DashboardSetting constructor.
     *
     * @param DashboardSetting $dashboardSetting
     */
    public function __construct(DashboardSetting $dashboardSetting)
    {
        $this->model = $dashboardSetting;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            return $this->model->create($data);
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage(),
            ];
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function insert(array $data)
    {
        try {
            return $this->model->insert($data);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            return $this->model->findOrFail($id)->update($data);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            return $this->model::all();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function find($id): array
    {
        try {
            $data = $this->model->with('role')->whereId($id)->first();
            return [
                'bool' => true,
                'data' => $data,
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'data' => $exception->getMessage(),
            ];
        }
    }


    public function delete($id)
    {
        try {
            return $this->model->destroy($id);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    //get dashboard settings
    public function getDashboardSetting($id)
    {
        try {
            $dashboardSetting = $this->model;
            return $dashboardSetting->where('user_id',$id)->get();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    //update dashboard settings
    public function updateDashboardSettings(array $data, $userId, $widget)
    {
        try {
            $dashboardSetting = $this->model;
            return $this->model->where(['user_id'=>$userId,'widget_name'=>$widget])->update($data);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    //
    public function getWidgets($userId)
    {
        try {
            $dashboardSetting = $this->model;
            $results = $dashboardSetting->where(['user_id'=>$userId,'is_allowed'=>1])->get();
            return $results;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
