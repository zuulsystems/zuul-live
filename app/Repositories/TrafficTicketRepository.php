<?php

namespace App\Repositories;

use App\Http\Controllers\Admin\VendorVehiclesController;
use App\Http\Requests\Vehicle;
use App\Models\TrafficTicket;
use App\Repositories\RepositoryInterface;
use App\Models\PassUser;
use App\Models\Location;
use App\Models\VehicleVendor;
use Exception;

class TrafficTicketRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var TrafficTicket
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param TrafficTicket $trafficTicket
     */
    public function __construct(TrafficTicket $trafficTicket)
    {
        $this->model = $trafficTicket;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function bulkDelete($ids)
    {
        try {
            $result = $this->model::whereIn('id',$ids)->delete();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function restore($id)
    {
        try {
            $result = $this->model->withTrashed()->find($id)->restore();;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function data($type = null, $communityId = null, $displayDismissed = false, $location_id = null, $license = null,$userIds = null){
    try {

            $query = $this->model;
            if($displayDismissed)
               $query = $query->withTrashed();

            if(!is_null($type))
                $query = $query->where('ticket_type', $type);

            if(!is_null($license))
                $query = $query->where('license_plate', $license);

            if (!empty($userIds)){
                $query = $query->whereIn('user_id', $userIds);
            }

            # Todo: Make Role ID Human Readable
            if(!empty($location_id)){
                $location = Location::find($location_id);
                if(\Auth::user()->role_id == 2 && $location->community_id = getCommunityIdByUser())
                    $query = $query->where('location_id', $location_id);
                else if(\Auth::user()->role_id == 1)
                    $query = $query->where('location_id', $location_id);
                else
                    abort(403);
            }

            $query = $query->with([
                'community' => function($community){ $community->select('id','name'); },
                'location' => function($location){ $location->with('community')->select('id','name','community_id'); },
                'vehicle:id,license_plate,make,model',
                'vehicle.vendor:id,email,community_id',
                'user' => function($user){ $user->select('id','role_id', 'house_id','email')->with([
                    'house' => function($house){
                        return $house->select('id', 'house_name');
                    }]);
                }
            ]);

            // Restrict User By Community ID
            if(!empty($communityId)){
                $query = $query->whereHas('location', function($query) use ($communityId){
                    $query->where('community_id', $communityId);
                });
            }


            $result = $query->withCount('emailLog')->withCount('ticketImages')->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            dd($exception->getMessage());
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    # TODO - Code Optimization - Quick Function Creation for Deployment
    public function unmatchedData($type = null, $communityId = null, $displayDismissed = false, $location_id = null, $license = null,$userIds = null, $request= null){
        try {
                $query = $this->model::query();
                 $query = $query->select('traffic_tickets.*',\DB::raw('(violating_speed - current_speed_limit) AS excess_speed_calculated'));
                if($displayDismissed)
                   $query = $query->withTrashed();


            if(!is_null($license)){
                $query = $query->where('license_plate', $license);
            }

                if (!empty($userIds)){
                    $query = $query->whereIn('user_id', $userIds);
                }

                # Todo: Make Role ID Human Readable
                if(!empty($location_id)){
                    $location = Location::find($location_id);
                    if(\Auth::user()->hasRole('sub_admin') && $location->community_id = getCommunityIdByUser())
                        $query = $query->where('location_id', $location_id);
                    else if(\Auth::user()->hasRole('super_admin'))
                        $query = $query->where('location_id', $location_id);
                    else
                        abort(403);
                }

                $query = $query->with([
                    'community' => function($community){ $community->select('id','name'); },
                    'location' => function($location){ $location->with('community')->select('id','name','community_id'); },
                    'location.community',
                    'vehicle:id,license_plate,make,model',
                    'vehicle.vendor:id,email,community_id',
                    'user' => function($user){ $user->select('id','role_id', 'house_id','email')->with([
                        'house' => function($house){
                            return $house->select('id', 'house_name');
                        }]);
                    }
                ]);



                // Restrict User By Community ID
                if(!empty($communityId)){
                    $query = $query->whereHas('location', function($query) use ($communityId){
                        $query->where('community_id', $communityId);
                    });
                }


                // Filters
                if($request->has('ticket_id') && !empty($request->ticket_id)){
                    $query->where('ticket_id', $request->ticket_id);
                }

                if($request->has('community') && !empty($request->community)){
                    $community = $request->community;
                    $query = $query->whereHas('location', function($query) use ($community){
                        $query = $query->whereHas('community', function($query) use ($community){
                            $query->where('name', 'like', '%'.$community.'%');
                        });
                    });
                }

                if($request->has('from') && !empty($request->from)){
                    $query->where('time', ">=",  $request->from);
                }

                if($request->has('to') && !empty($request->to)){
                    $query->where('time', "<=",  $request->to);
                }

                if($request->has('excess_speed_start') && !empty($request->excess_speed_start)){
                    $query->whereRaw('violating_speed - current_speed_limit >= '.$request->excess_speed_start);
                }

                if($request->has('excess_speed_end') && !empty($request->excess_speed_end)){
                    $query->whereRaw('violating_speed - current_speed_limit <= '.$request->excess_speed_end);
                }

                if($request->has('excess_speed_end') && !empty($request->excess_speed_end)){
                    $query->whereRaw('violating_speed - current_speed_limit <= '.$request->excess_speed_end);
                }

                $query->withCount('trafficTickets');
                if($request->has('violation_count_start') && !empty($request->violation_count_start)){
                    $query->having('traffic_tickets_count', '>=', $request->violation_count_start);
                }
                if($request->has('violation_count_end') && !empty($request->violation_count_end)){
                    $query->having('traffic_tickets_count', '<=', $request->violation_count_end);
                }

                $result = $query->withCount('emailLog')->withCount('ticketImages');

                return [
                    'bool' => true,
                    'result' => $result
                ];
        } catch (Exception $exception) {
                dd($exception->getMessage());
                return [
                    'bool' => false,
                    'message' => $exception->getMessage()
                ];
        }
    }
    public function ticketDetails($ticketId){
        try {
            // Attributes to be selected from DB
            $attributes = explode(',', 'id,ticket_id,user_id,community_id,location_id,license_plate,current_speed_limit,violating_speed,time,camera_name,ticket_type');

            // Get the Result
            $result = $this->model->select($attributes)
            ->with('vehicle:id,make,model,color,year,license_plate')
            ->with('location.community:id,name')
            ->with('user.house:id,house_detail,house_name')
            ->with('user:id,first_name,middle_name,last_name,phone_number,email,house_id,community_id')
            ->with('user.community:id,name')
            ->with('location:id,name,geocode,community_id')
            ->with('ticketImages')
            ->withCount('emailLog')->withCount('ticketImages')
            ->find($ticketId);

            $ticket = $result->toArray();

            //QuickFix - Vendor Details
            $vendor = null;
            if(!is_null($ticket['vehicle']))
                $vendor = VehicleVendor::with('vendor')->where('vehicle_id', $ticket['vehicle']['id'])->orderByDesc('id')->first();

            if(!is_null($vendor)){
                $vendor = $vendor->vendor;
            }

            //QuickFix - Pass User Details
            $pass = PassUser::where('updated_at', "<=", $ticket['time'])
                        ->where('is_scan', '1')
                        ->whereHas('vehicle', function($vehicle) use ($ticket) {
                            return $vehicle->where('license_plate', $ticket['license_plate']);
                        })->with('pass.createdBy.community')
                        ->with('pass.createdBy.house')
                        ->orderBy('updated_at', 'desc')->first();
            return [
                'bool' => true,
                'result' => $result,
                'vendor' => $vendor,
                'pass' => $pass,
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $communityId, int $location_id, int $type
     * @return mixed
     * @throws Exception
     */
    public function getTickets($communityId = null, $location_id = 0, $type = null)
    {
        try {
            $query = $this->model::with('vehicles', 'location', 'location.community');

            if(!empty($communityId)){
                $query->whereHas('location.community', function($q) use ($communityId){
                    $q->where('community_id', $communityId);
                });
            }

            if($type == 2)
            {
                $query->where('traffic_tickets.location_id', 0);
            }
            else
            {
                $query->where('traffic_tickets.location_id', $location_id);
            }

            $result = $query->get()->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $communityId, int $location_id, int $type
     * @return mixed
     * @throws Exception
     */
    public function getTicketsCount($communityId = null, $location_id = 0, $type = null)
    {
        try {
            $query = $this->model::with('vehicles', 'location', 'location.community');

            if(!empty($communityId)){
                //$query->where('traffic_tickets.community_id', $communityId);
                $query->whereHas('location.community', function($location) use ($communityId){
                    $location->where('community_id', $communityId);
                });
            }

            if($type == 1){
                $query->where('traffic_tickets.ticket_type', 'resident');
            }
            else if($type == 2){
                $query->where('traffic_tickets.ticket_type', 'guest');
            }
            else if($type == 3){
                $query->where('traffic_tickets.ticket_type', 'vendor');
            }
            else if($type == 4){
                $query->where('traffic_tickets.ticket_type', 'unmatched');
            }

            if(!empty($location_id)){
                $query->where('traffic_tickets.location_id', $location_id);
            }

            $result = $query->get()->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function findMultiple($array){
        try {
            $result = $this->model->whereIn('id', $array);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $licensePlate
     * @return array
     */
    public function getUnmatchedTicketByLicensePlate($licensePlate){
        try {
            $result = $this->model->where('ticket_type','unmatched')->whereNotNull('license_plate')->where('license_plate',$licensePlate)->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $licensePlate
     * @return array
     */
    public function getAllUnmatchedTicket(){
        try {
            $result = $this->model->where('ticket_type','unmatched')->whereDate('time','>=','2021-10-05')->whereNotNull('license_plate')->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param $licensePlate
     * @param $type
     * @return array
     */
    public function getTicketByLicensePlate($licensePlate,$type){
        try {
            $result = $this->model->where('ticket_type',$type)->whereNotNull('license_plate')->where('license_plate',$licensePlate)->orderByDesc('id')->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param $ticketIds
     * @param $residentId
     * @param $communityId
     * @return array
     */
    public function attachTicketsToResident($ticketIds,$residentId,$communityId){
        try {
            $result = $this->model->whereIn('id',$ticketIds)->update([
                'ticket_type' => 'resident',
                'user_id' => $residentId,
                'community_id' => $communityId,
            ]);
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
 }
