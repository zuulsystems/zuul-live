<?php


namespace App\Repositories;

use App\Models\Pass;
use App\Repositories\RepositoryInterface;
use Exception;

class PassRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var Pass
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Pass $pass
     */
    public function __construct(Pass $pass)
    {
        $this->model = $pass;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model;
            if(!empty($communityId)){
                $query = $query->where('community_id', $communityId);
            }
            $result = $query->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->with(['event', 'createdBy', 'community'])->whereId($id)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage(),
            ];
        }
    }

    /**
     * get resident list
     *
     * @param null $communityId
     *
     * @return mixed
     */
    public function getPassList($communityId = null, $houseId = null, $userId = null)
    {
        try {
            $queryBuilder = $this->model::query()
                ->select('passes.*')
                ->with([
                'createdBy' => function ($createdBy) {
                    $createdBy->select('id', 'first_name', 'last_name','community_id');
                },
                'community' => function ($community) {
                    $community->select('id', 'name', 'timezone_id');
                },
                'passUsers'
            ]);
            //get passes by community Id
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->where('passes.community_id', $communityId);
            }

            if (!empty($houseId)) {
                $queryBuilder = $queryBuilder->where('house_id', $houseId);
            }

            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('created_by', $userId);
            }

            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result,
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage(),
            ];
        }
    }


    /**
     * @param null $userId
     * @param $isExpired
     * @param null $search
     *
     * @param null $filterType
     * @param null $filterData
     * @param int $limit
     * @return mixed
     */
    public function getSentPassesCreatedBy($userId, $isExpired,$search = null,$filterType = null,$filterData = null , $limit = 10)
    {
        try {
            $currentDateTime = date('Y-m-d H:i:s');
            $queryBuilder = $this->model;
            $expire = ($isExpired)? "<=": ">="; // get expired or active passes
            $queryBuilder = $queryBuilder->with([
                'createdBy',
                'addedUsers',
                'event',
                'community'
            ]);
            //search all data
            if(!empty($search)){
                $queryBuilder->where(function ($where)use ($search){
                    $where->where('description', 'like', "%$search%");
                    $where->OrWhereHas('event', function($passEvent) use ($search){
                        $passEvent->where('name', 'like', "%$search%");
                    });
                });

            }

            //search by filter type
            if(!empty($filterType) && !empty($filterData)){

                if($filterType == 'event_name'){
                    $queryBuilder->whereHas('event', function($passEvent) use ($filterData){
                        $passEvent->where('name', 'like', "%$filterData%");
                    });
                }
                if($filterType == "date"){
                    $date = date('Y-m-d', strtotime($filterData));
                    $queryBuilder->whereDate('pass_start_date', $date);
                }
            }
            $queryBuilder = $queryBuilder->where('created_by', $userId)->where('pass_date', $expire,  $currentDateTime)->orderByDesc('id');
            $result = $queryBuilder->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * get sent passes dates created by
     * @param $userId
     * @param $isExpired
     * @return mixed
     */
    public function getSentPassesDatesCreatedBy($userId,$isExpired){
        try{
            $expire = ($isExpired)? "<=": ">=";
            $result =  $this->model::select('pass_date','passes.pass_start_date')
            ->where('created_by', $userId)
            ->where('passes.pass_date',$expire, date('Y-m-d H:i:s'))
            ->groupBy(\DB::raw("date_format(passes.pass_start_date, '%Y%m%d')"))
            ->orderBy('pass_start_date', 'ASC')
            ->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get passes recipients by passId
     * @param $passId
     * @return mixed
     */
    public function getPassRecipients($passId){
        return $this->model::with([
          //  'addedUsers',
            'passUsers',
            'passUsers.user'
        ])
        ->where('id',$passId)
        ->first();
    }
 }
