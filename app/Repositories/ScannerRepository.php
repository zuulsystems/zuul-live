<?php

namespace App\Repositories;

use App\Models\Scanner;
use App\Repositories\RepositoryInterface;
use Exception;

class ScannerRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var Scanner
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Scanner $scanner
     */
    public function __construct(Scanner $scanner)
    {
        $this->model = $scanner;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model::with([
                'community' => function($community){
                    $community->select('id','name');
                },
                'camera' => function($camera){
                    $camera->select('id','name');
                }
            ]);
            if(!empty($communityId)){
                $query->where('community_id',$communityId);
            }

            if(!empty(auth()->user()->kioskPermission))
            {
                $communityIds = explode(",",auth()->user()->kioskPermission->communities);
                $query->whereIn('community_id',$communityIds);
            }

            $result = $query->select('scanners.*');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->with('camera')->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $macAddress
     * @param $secretKey
     * @return array
     */
    public function getByCredentials($macAddress,$secretKey){
        try {
            $result = $this->model::where('mac_address', $macAddress)->where('secret_key',$secretKey )->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
 }
