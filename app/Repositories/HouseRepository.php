<?php

namespace App\Repositories;

use App\Models\House;
use App\Repositories\RepositoryInterface;
use Exception;

class HouseRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var House
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param House $house
     */
    public function __construct(House $house)
    {
        $this->model = $house;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model;
            if(!empty($communityId)) {
                $query = $query->where('community_id',$communityId);
            }
            $result = $query->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get houses list
     * @param null $communityId
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function housesList($communityId = null, $houseId = null)
    {
        try {

            $queryBuilder =  $this->model::query()
                ->select('houses.*');
            if ($communityId != null){
                $queryBuilder = $queryBuilder->where('community_id',$communityId);
            }
            if ($houseId != null){
                $queryBuilder = $queryBuilder->where('id',$houseId);
            }

            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get house by community Id
     * @param $communityId
     * @return
     */
    public function getHousesByCommunityId($communityId){
        try {
            $result = $this->model->where('community_id',$communityId)->orderBy('house_name','ASC')->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get house by house name
     * @param $houseName
     * @param $communityId
     * @param $data
     * @return
     */
    public function getOrCreateHouseByHouseNameCommunityId($houseName,$communityId,$data){
        try {
            $result = $this->model->where('house_name',$houseName)->where('community_id',$communityId)->first();

            if ($result == null){
                $result = $this->model->create($data);
            }

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            $result = $this->model->updateOrCreate($where, $data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $communityId
     * @param $houseDetail
     * @return array
     */
    public function getHouseByCommunityAndAddress($communityId,$houseDetail){
        try {
            $result = $this->model->where('community_id',$communityId)->where('house_detail',$houseDetail)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * House with resident
     *
     * @param $houseId
     * @return array
     */
    public function getHouseWithResidents($id){
        try {
            $result = $this->model->with('residents')->where('id', $id)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * House by address
     *
     * @param $houseId
     * @return array
     */
    public function houseByAddress($communityId, $houseAddress){
        try {
            $result = $this->model->where('community_id',$communityId)->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
 }


