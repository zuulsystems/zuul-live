<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Repositories\RepositoryInterface;
use Exception;

class VehicleRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var Vehicle
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Vehicle $vehicle
     */
    public function __construct(Vehicle $vehicle)
    {
        $this->model = $vehicle;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @param null $userId
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null, $userId = null, $vehicleId = null)
    {
        try {
            $queryBuilder = $this->model::query()
                ->select('vehicles.*')
                ->whereNull('vehicles.deleted_at')
               ->withCount('trafficTickets');
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->whereHas('user.community', function($community) use($communityId) {
                    $community->where('id', $communityId );
                });
            }
            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('user_id',$userId);
            }
            if (!empty($vehicleId)) {
                $queryBuilder = $queryBuilder->whereIn('id',$vehicleId);
            }

            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $userId
     * @param int $limit
     * @param null $search
     * @return mixed
     */
    public function getByUserId($userId,$limit = 50,$search = null)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->where('user_id',$userId);
            if(!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where('make', 'LIKE', "%$search%")->orWhere('year', 'LIKE', "%$search%")->orWhere('color', 'LIKE', "%$search%")
                        ->orWhere('license_plate', 'LIKE', "%$search%");
                });
            }
            $result = $queryBuilder->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            $result = $this->model->updateOrCreate($where, $data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }



}
