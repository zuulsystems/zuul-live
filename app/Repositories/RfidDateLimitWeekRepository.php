<?php

namespace App\Repositories;

use App\Models\AuthLog;
use App\Models\RfidDateLimitWeek;
use App\Repositories\RepositoryInterface;
use Exception;

class RfidDateLimitWeekRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var AuthLog
     */
    public $model;

    /**
     * RfidDateLimitRepository constructor.
     *
     * @param RfidDateLimitWeek $rfidDateLimitWeek
     */
    public function __construct(RfidDateLimitWeek $rfidDateLimitWeek)
    {
        $this->model = $rfidDateLimitWeek;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     */
    public function all()
    {
        try {
            $queryBuilder = $this->model;
            $result = $queryBuilder->orderByDesc('id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            $result = $this->model->updateOrCreate($where, $data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get by date limit Id
     * @param $rfidDateLimitId
     * @return array
     */
    public function findByrfidDateLimitId($rfidDateLimitId)
    {
        try {
            $result = $this->model->where('rfid_date_limit_id',$rfidDateLimitId)->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * delete by date limit Id
     * @param $rfidDateLimitId
     * @return array
     */
    public function deleteByRfidDateLimitId($rfidDateLimitId)
    {
        try {
            $result = $this->model->where('rfid_date_limit_id',$rfidDateLimitId)->delete();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
}
