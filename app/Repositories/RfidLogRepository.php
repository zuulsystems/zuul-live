<?php

namespace App\Repositories;

use App\Models\RfidLog;
use App\Repositories\RepositoryInterface;
use Exception;

class RfidLogRepository implements RepositoryInterface
{
    /**
     * Holds the instance of RfidLog model.
     *
     * @var RfidLog
     */
    public $model;

    /**
     * RfidDateLimitRepository constructor.
     *
     * @param RfidLog $rfidLog
     */
    public function __construct(RfidLog $rfidLog)
    {
        $this->model = $rfidLog;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     */
    public function all($communityId = null, $houseId = null, $userId = null)
    {
        try {
            $queryBuilder = $this->model::query()
                ->with([
                'rfid',
                'rfid.user',
                'rfid.vehicle',
                'scanByScanner',
            ]);
            if (!empty($communityId)){
                $queryBuilder = $queryBuilder->where('community_id',$communityId);
            }
            if (!empty($houseId)){
                $queryBuilder = $queryBuilder->whereHas('rfid', function($query) use ($houseId) {
                    $query->where('house_id', $houseId);
                });
            }
            if (!empty($userId)){
                $queryBuilder = $queryBuilder->whereHas('rfid.user', function($query) use($userId) {
                    $query->where('id', $userId);
                });
            }
           $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $rfidId
     * @param $startDate
     * @param $endDate
     * @param null $id
     * @return array
     */
    public function findBetweenStartEndDates($rfidId, $startDate, $endDate, $id = null)
    {
        // Sudo code to add an entry
        // - Any time an entry is created or updated, the input range will be checked
        // - If the start of input date is found between any other schedule, it will prompt error
        // - If the end of input date is found between any other schedule, it will prompt error
        // - Otherwise it will add an entry
        try {
            $queryBuilder = $this->model::query();
            $queryBuilder->where('rfid_id', $rfidId);
            if (!empty($id)) {
                $queryBuilder->where('id', '!=', $id);
            }
            $result = $queryBuilder->where('start_date', '<=', $endDate)->where('end_date', '>=', $startDate)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];

        }
    }
}
