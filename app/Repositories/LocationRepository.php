<?php

namespace App\Repositories;

use App\Models\Location;
use App\Repositories\RepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class LocationRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var Location
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Location $location
     */
    public function __construct(Location $location)
    {
        $this->model = $location;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    function updateByCameraId(array $data, $id)
    {
        try {
            $result = $this->model->where('location_id', $id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model;
            if(!empty($communityId)){
                $query =  $query->where('community_id', $communityId);
            }
            $result = $query->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $communityId
     * @return mixed
     * @throws Exception
     */
    public function getLocations($communityId)
    {
        try {
            $query = $this->model
                    ->select('locations.id', 'locations.community_id', 'name', 'geocode',
                        DB::raw('count(traffic_tickets.id) as total')
                    )
                   ->leftJoin('traffic_tickets', 'locations.id', '=',  'traffic_tickets.location_id')
                 ->groupBy('locations.id')->whereNotNull('locations.community_id');

            if($communityId){
                $query->where('locations.community_id', $communityId);
            }
            $result = $query->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            dd($exception);

            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getLocationData($community_id = null){
        try {
                $result =  $this->model->with('communityName')->withCount('trafficTickets')
                    ->when($community_id, function($query, $community_id){
                    return $query->where('community_id', $community_id);
                })->orderByDesc('location_id')->get();

                return [
                    'bool' => true,
                    'result' => $result
                ];

            } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
        ];
        }
    }

 }
