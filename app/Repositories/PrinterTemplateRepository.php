<?php

namespace App\Repositories;

use App\Models\PrintersTemplate;
use App\Repositories\RepositoryInterface;
use Exception;

class PrinterTemplateRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var PrinterTemplate
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param PrinterTemplate $printerTemplate
     */
    public function __construct(PrintersTemplate $printerTemplate)
    {
        $this->model = $printerTemplate;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $result = $this->model->with('community');

            if(!empty($communityId))
            {
                $result = $result->where('community_id', $communityId);
            }

            $result = $result->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * @param $printerTemplateId
     * @return array
     */
    public function deleteUser($printerTemplateId){
        try {
            if (!auth()->user()->hasRole('super_admin')){
                return [
                    'bool' => false,
                    'message' => 'User has Not Permission to purge'
                ];
            }
            $user =  $this->model->find($printerTemplateId);
            if ($user){
                return ['bool'=>true, 'result'=>$user->delete($printerTemplateId)];
            }
            return [
                'bool' => false,
                'message' => 'Printer Template Not Exist'
            ];
        }catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
