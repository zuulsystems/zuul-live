<?php

namespace App\Repositories;

use App\Models\Community;
use App\Repositories\RepositoryInterface;
use Exception;

class CommunityRepository implements RepositoryInterface
{
 /**
     * Holds the instance of User model.
     *
     * @var Community
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Community $community
     */
    public function __construct(Community $community)
    {
        $this->model = $community;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function updateWithOutTryCatch(array $data, $id)
    {
        return $this->model->findOrFail($id)->update($data);
    }
    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model;
            if(!empty($communityId)) {
                $query = $query->where('id',$communityId);
            }

            if(!empty(auth()->user()->kioskPermission))
            {
                $communityIds = explode(",", auth()->user()->kioskPermission->communities);
                $query = $query->whereIn('id',$communityIds);
            }

            $result = $query->orderBy('name','ASC')->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function getByCommunityId($id)
    {
        try {
            $result = $this->model->where('id',$id)->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get community list
     */
    public function communityList(){
        try {
            $result = $this->model::query()
            ->select('communities.*')
            ->withCount('houses');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get single community by its Id
     */
    public function getById($id){
        try {
            $result = $this->model::find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @return mixed
     * @throws Exception
     */
    public function allCount()
    {
        try {
            $result = $this->model::all()->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getTicketTemplatesByCommunityAndType($community_id, $type){

        try {
            $result = $this->model->with('emailTemplates')
            ->whereHas('emailTemplates', function($query) use ($type){
                $query->where('email_type', $type);
            })
            ->find($community_id);
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {

            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getTicketTemplatesByCommunitySlugAndType($community_id, $type, $slug){

        try {
            $result = $this->model->with('emailTemplates')
            ->whereHas('emailTemplates', function($query) use ($type, $slug, $community_id){
                $query->where('email_type', $type)->where('assign_to', $slug);
            })
            ->find($community_id);
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function cameraCommunityList($communityId = null)
    {
        try {
            $query = $this->model::with('locations');

            $query = $query->whereHas('locations');

            if(auth()->user()->role_id != 1){
                $query = $query->where('id', $communityId);
            }

            $result = $query->orderBy('name','ASC')->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get community list
     * @param null $communityId
     * @return array
     */
    public function communityListWithCameraLocations($communityId = null){
        try {
            $queryBuilder = $this->model;
            if (!empty($communityId)){
                $queryBuilder = $queryBuilder->where('id',$communityId);
            }
            $result =  $queryBuilder->whereHas('locations');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

 }
