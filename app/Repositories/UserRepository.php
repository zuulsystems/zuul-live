<?php

namespace App\Repositories;

use App\Models\UserAdditionalContact;
use App\Models\UserPermission;
use App\Repositories\RepositoryInterface;
use App\Models\User;
use App\Models\Pass;
use App\Models\PassUser;
use App\Models\ScanLog;
use App\Models\StaticScanLog;
use App\Models\Notification;
use App\Helpers\UserCommon;
use Exception;
use App\Models\BannedCommunityUser;

class UserRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var User
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function updateWithOutTryCatch(array $data, $id)
    {
        return $this->model->findOrFail($id)->update($data);
    }
    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function find($id): array
    {

        try {
            $result = $this->model->with('role','community','bandCommunities')->whereId($id)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $phone
     *
     * @return array
     */
    public function findByPhone($phone): array
    {
        try {
            $result = $this->model->with('role')->wherePhoneNumber($phone)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get guest list
     * @param $request
     * @param bool $isCommunityAdmin
     * @param null $userId
     * @return array
     */
    public function getGuestList($request,$isCommunityAdmin = false,$userId = null)
    {
        try {
            $queryBuilder = collect();
            if (
                ($isCommunityAdmin == true) ||
                (!empty($request->email) || !empty($request->full_name) || !empty($request->formatted_phone) || !empty($userId))
            ){
                $queryBuilder = $this->model::whereHas('role', function ($q) {
                    $q->guest();
                })->with(['role:id,name'])->select('users.*');

            }

            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('id', $userId);
            }
            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getBandGuestList($request,$isCommunityAdmin = false,$userId = null)
    {
        $bandUserIds = BannedCommunityUser::pluck('user_id');

        $community_id = BannedCommunityUser::pluck('community_id');
        try {

            $queryBuilder = collect();

            //Check if user is community admin
            //Community Admin can only filter the records
            if (
                ($isCommunityAdmin != true) ||
                (!empty($request->email) || !empty($request->full_name) || !empty($request->formatted_phone))
            ){
                if (auth()->user()->hasRole('super_admin')){
                    $queryBuilder =  $this->model::with('vehicles')->whereIn('id', $bandUserIds)->select('users.*');
                }else{
                    $queryBuilder =  $this->model::whereIn('id', $bandUserIds)->whereHas('bandCommunities', function ($attachmentQuery) {
                        $attachmentQuery->where('community_id', auth()->user()->community_id);
                    })->select('users.*');
                }



            }
            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('id', $userId);
            }



            $result = $queryBuilder->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    public function updateAdditionalContacts($additional_phone_array,$id)
    {
        $count = 0;
        for ($i = 0; $i < 8; $i++) {

            if ($i%4==0 && $additional_phone_array[$i+2] != '' && $additional_phone_array[$i] != 0) {

                $formatted_phone = explode('-',$additional_phone_array[$i+2]);
                $dial_code = $additional_phone_array[$i+1];
                $type = $additional_phone_array[$i+3] != 'Select'? $additional_phone_array[$i+3] : 'Work';
                $contact = str_replace("-","",$additional_phone_array[$i+2]);

                $updateAdditionalContact = UserAdditionalContact::where('id',$additional_phone_array[$i])->update([
                    'contact'=>$contact,
                    'type'=>$type,
                    'dial_code'=>$dial_code
                ]);

            }else if($i%4==0){

                $remove_id = $additional_phone_array[$i];
                $user = UserAdditionalContact::find($remove_id);

                if($user != null){
                    $user->delete();
                }else if($additional_phone_array[$i+2] != ''){
                    $type = ($additional_phone_array[$i+3] != 'Select') ? $additional_phone_array[$i+3] : 'Work';
                    $dial_code = $additional_phone_array[$i+1];
                    $contact = str_replace("-","",$additional_phone_array[$i+2]);
                    UserAdditionalContact::create([
                        'user_id'=>$id,
                        'contact'=>$contact,
                        'type'=>$type,
                        'dial_code'=>$dial_code
                    ]);

                }
            }

        }

        return $count;
    }

    /**
     * get community admin list
     * @param null $communityId
     * @param null $userId
     * @return array
     */
    public function getCommunityAdminList($communityId = null,$userId = null)
    {
        try {
            $queryBuilder = $this->model::query();
            $queryBuilder = $queryBuilder->whereHas('role', function ($q) {
                $q->communityAdmin();
            });
            $queryBuilder = $queryBuilder->with([
                'role' => function ($role) {
                    $role->select('id', 'name');
                },
                'community' => function ($community) {
                    $community->select('id', 'name');
                },
            ]);
            //get users community wise
            if ($communityId != null) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }
            //get by user id
            if ($userId != null) {
                $queryBuilder = $queryBuilder->where('id', $userId);
            }
            $result = $queryBuilder->select('users.*');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get guard list
     * @param $communityId
     * @return
     */
    public function getCommunityGuardList($communityId,$userId = null)
    {
        try {
            $queryBuilder = $this->model::query();
            $queryBuilder =  $queryBuilder->where(function ($where){
                $where->whereHas('role', function ($q) {
                    $q->guard();
                })->orwhereHas('role', function ($q) {
                    $q->guardAdmin();
                });
            });
            //get passes by community Id
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }

            if(!empty(auth()->user()->kioskPermission))
            {
                $communityIds = explode(",", auth()->user()->kioskPermission->communities);
                $queryBuilder = $queryBuilder->whereIn('community_id', $communityIds);
            }

            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('id', $userId);
            }
            $queryBuilder = $queryBuilder->with([
                'role:id,name',
                'community:id,name'
            ]);
            $result = $queryBuilder->select('users.*');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * get kiosk admin list
     * @param null $communityId
     * @param null $userId
     * @return array
     */
    public function getkioskAdminList($communityId = null,$userId = null)
    {
        try {
            $queryBuilder = $this->model::query();
            $queryBuilder = $queryBuilder->whereHas('role', function ($q) {
                $q->kioskAdmin();
            });
            $queryBuilder = $queryBuilder->with([
                'role' => function ($role) {
                    $role->select('id', 'name');
                },
                'community' => function ($community) {
                    $community->select('id', 'name');
                },
            ]);
            //get users community wise
            if ($communityId != null) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }
            //get by user id
            if ($userId != null) {
                $queryBuilder = $queryBuilder->where('id', $userId);
            }
            $result = $queryBuilder->select('users.*');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get resident list
     * @param null $communityId
     * @return mixed
     */
    public function getResidentList($communityId = null, $houseId = null,$userId = null,$phoneNumber = null,$is_suspended = null,$complete_profile = null)
    {
        try {

            //new code 15-8-2022
            $queryBuilder = $this->model::query();
            $queryBuilder = $queryBuilder->select('users.id', 'first_name','users.dial_code', 'last_name', 'email', 'phone_number', 'users.community_id', 'house_id', 'role_id', 'mailing_address', 'license_image', 'special_instruction', 'users.created_at','is_suspended','houses.house_name','houses.house_detail','communities.name');
            $queryBuilder = $queryBuilder->with([
                'role:id,name'
            ]);
            $queryBuilder = $queryBuilder->join('houses','users.house_id','=','houses.id');
            $queryBuilder = $queryBuilder->join('communities','users.community_id','=','communities.id');
            //new code 15-8-2022
            $queryBuilder = $queryBuilder->whereHas('role', function ($q) {
                $q->resident();
            });

            //get resident by community Id
            if (!empty($communityId)&&empty($userId)) {
                $queryBuilder = $queryBuilder->where('users.community_id', $communityId);
            }
            if (!empty($houseId)) {
                $queryBuilder = $queryBuilder->where('house_id', $houseId);
            }
            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('users.id', $userId);
            }
            if (!empty($phoneNumber)) {
                $queryBuilder = $queryBuilder->where('phone_number', $phoneNumber);
            }

            if (!empty($is_suspended) ) {
                if($is_suspended == 2){

                    $queryBuilder = $queryBuilder->where('is_suspended', '0');
                }elseif($is_suspended == 'on'){

                    $queryBuilder = $queryBuilder->where('is_suspended', '1');
                }elseif($is_suspended == 'all'){

                }
                else{
                    $queryBuilder = $queryBuilder->where('is_suspended', $is_suspended);
                }
            }
            if($complete_profile != 'all') {
                if($complete_profile == 'yes'){
                    $queryBuilder = $queryBuilder->whereNotNull('license_image');
                }elseif($complete_profile == 'no'){
                    $queryBuilder = $queryBuilder->whereNull('license_image');
                }
            }

            //new code 15-8-2022
            $queryBuilder = $queryBuilder->where('users.deleted_at', null);
            //new code 15-8-2022

            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get vendors list
     * @param null $communityId
     * @return mixed
     */
    public function getVendorList($communityId = null)
    {
        try {
            $queryBuilder = $this->model::whereHas('role', function ($q) {
                $q->vendor();
            });
            $queryBuilder = $queryBuilder->with([
                'vehicles' => function ($vehicles) {
                    $vehicles->select('id', 'user_id');
                },
                'community' => function ($community) {
                    $community->select('id', 'name');
                }
            ]);
            //get vendors by community Id
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }
            $queryBuilder = $queryBuilder->withCount('trafficTickets');
            $queryBuilder = $queryBuilder->select('id', 'first_name', 'last_name', 'email', 'phone_number', 'community_id','mailing_address', 'special_instruction', 'created_at');
            $result = $queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * check if user not exist then create new user
     * @param $phoneNumber
     * @param $data
     * @return mixed
     */
    public function getOrCreateUserByPhone($phoneNumber,$data){
        try {
            $result = array();
            $user = $this->model::where('phone_number',$phoneNumber)->first();
            if (!empty($user)){
                $this->model::where('id', $user->id)->update($data); // update user
                $user = $this->model->find( $user->id); // get user
                $result['is_already_exist'] = true;
                $result['user'] = $user;
            } else {
                $user = $this->model->create($data); // create new user
                $result['is_already_exist'] = false;
                $result['user'] = $user;
            }

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $columns
     * @param $values
     * @param array $operators
     * @param array $whereIn
     * @return array
     */
    public function whereAttribute($columns, $values, $operators = [], $whereIn = []): array
    {
        try {
            $q = new $this->model;

            foreach ($columns as $key => $column) {
                if (array_key_exists($key, $operators) && !empty($operators[$key]))
                    $q = $q->where($column, $operators[$key], $values[$key]);
                else
                    $q = $q->where($column, $values[$key]);
            }

            if (count($whereIn) > 0)
                $q = $q->whereIn($whereIn['column'], $whereIn['values']);

            $result = $q->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $houseId
     * @param $limit
     * @param null $search
     * @return array
     */
    public function getUsersByHouseId($UserId,$limit,$search = null){
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->where('house_id',$UserId)->whereNotNull('house_id')->whereNotNull('community_id');
            $queryBuilder = $queryBuilder->with([
                'passes','passUsers','passUsers.pass'
            ]);
            if(!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where(\DB::raw('CONCAT(first_name, " ", last_name)'), 'like', "%$search%")
                        ->orWhere('phone_number', 'LIKE', "%$search%");
                });
            }
            $result = $queryBuilder->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $UserId
     * @param $limit
     * @param null $search
     * @return array
     */
    public function getUsersByHouseIdForAdmin($UserId,$limit,$search = null, $communityId=null){
        try {
            $user = User::find($UserId);

            if(!empty($user))
            {
                if(!empty($communityId))
                {
                    if($user->community_id != $communityId)
                    {
                        return [];
                    }
                }

                if(!empty($user->house_id)){
                    $houseId = $user->house_id;
                }
                else
                {
                    return [];
                }

                $queryBuilder = $this->model;
                $queryBuilder = $queryBuilder->where('house_id',$houseId)->whereNotNull('house_id')->whereNotNull('community_id');
                $queryBuilder = $queryBuilder->with([
                    'passes','passUsers','passUsers.pass'
                ]);
                $result = $queryBuilder->get();

            }
            else
            {
                return [];
            }

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param $houseId
     * @param $limit
     * @param null $search
     * @return array
     */
    public function getUsersCountByHouseId($UserId,$limit,$search = null){
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->where('house_id',$UserId)->whereNotNull('house_id')->whereNotNull('community_id');
            $queryBuilder = $queryBuilder->with([
                'passes','passUsers','passUsers.pass'
            ]);
            if(!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where(\DB::raw('CONCAT(first_name, , last_name)'), 'like', "%$search%")
                        ->orWhere('phone_number', 'LIKE', "%$search%");
                });
            }
            $result = $queryBuilder->get()->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    public function getUsersByHouseIdApi($house_id,$limit,$search = null){
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->where('house_id',$house_id)->whereNotNull('house_id')->whereNotNull('community_id');
            $queryBuilder = $queryBuilder->with([
                'passes','passUsers','passUsers.pass'
            ]);
            if(!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where(\DB::raw('CONCAT(first_name, , last_name)'), 'like', "%$search%")
                        ->orWhere('phone_number', 'LIKE', "%$search%");
                });
            }
            $result = $queryBuilder->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get all users by houseId
     * @param $houseId
     * @return array
     */
    public function getAllUsersByHouseId($houseId){
        try {
            $result = $this->model->where('house_id',$houseId)->whereNotNull('house_id')->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param $phoneNumber
     * @return mixed
     */
    public function getUserByPhoneNumber($phoneNumber){
        try {
            $result = $this->model::where('phone_number',$phoneNumber)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get head of family
     * @param $userId
     * @param $communityId
     * @param $houseId
     * @return array
     */
    public function headOfFamily($userId,$communityId,$houseId){

        try {
            $result = $this->model::whereNotNull('community_id')->where('community_id', $communityId )
                ->where('house', $houseId )
                ->where('id', '!=', $userId)
                ->where('head_of_family', 1 )->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @param $roleIds
     * @return mixed
     */
    public function totalUser($communityId = null, $roleIds=null)
    {
        try {
            $query = $this->model;

            if(!empty($communityId)) {
                $query = $query->where('community_id',$communityId);
            }

            if (is_array($roleIds)){
                $query = $query->whereIn('role_id',$roleIds);
            } else {
                $query = $query->where('role_id',$roleIds);
            }

            $result = $query->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $phoneNumber
     * @param $data
     * @return array
     * @throws Exception
     */
    public function getOrCreateUserByPhoneNumbers($phoneNumber,$data){

        $user = $this->model::where('phone_number',$phoneNumber)->first();
        //user already exist
        if (empty($user)){
            $user = $this->create($data); // create new user
            if($user['bool']){
                $user =  $user['result'];
            }
        }

        return $user;
    }

    /**
     * @param $communityId , $id
     * @param int $limit
     * @param null $search
     * @return array
     */
    public function getGuardResidents($communityId, $limit = null, $search= null, $page= null)
    {
        try {
            $queryBuilder = $this->model::with('additionalPhone')->where(function ($where){
                $where->where('role_id',5)
                    ->whereHas('permissions', function ($query) {
                        $query->where('slug', 'can_send_passes');
                    })
                    ->orWhere('role_id',4);
            });
            if (!empty($search)) {
                $names = explode(" ", $search);
                $queryBuilder = $queryBuilder->where(function ($where)use($names,$search){
                    $where->where(\DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $search . '%');
                    $where->orWhere('phone_number', 'LIKE', '%' . $search . '%');

                    $where->orWhereHas('house',function($query) use ($search) {
                        $query->where('house_detail', 'like', "%{$search}%");
                    });

                });
            }
            $queryBuilder = $queryBuilder->where('community_id', $communityId);
            $queryBuilder = $queryBuilder->with(['house', 'role']);
            $queryBuilder = $queryBuilder->orderBy('first_name','ASC');


            if($page == '-1'){
                $result =  $queryBuilder->get();
            }else{
                if(!empty($limit)){
                    $result =  $queryBuilder->paginate($limit);
                } else {
                    $result =  $queryBuilder->get();
                }
            }

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $verificationCode
     * @return array
     * @throws Exception
     */
    public function checkVerificationCode($verificationCode)
    {
        try {
            $result = $this->model->whereNotNull('verification_code')->whereRaw("BINARY verification_code COLLATE utf8mb4_bin = ?", [$verificationCode])->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * @param $userId
     * @return array
     */
    public function deleteUser($userId){
        try {
            if (!auth()->user()->hasRole('super_admin')){
                return [
                    'bool' => false,
                    'message' => 'User has Not Permission to purge'
                ];
            }


            $user =  $this->find($userId);
            if ($user['result']){
                $user['result']->deleteResidentData();
                return $this->delete($userId);
            }
            return [
                'bool' => false,
                'message' => 'User Not Exist'
            ];
        }catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    public function tempUserDeleteBySelf($userId){
        try {
            $user =  $this->find($userId);
            if ($user['result']){
                if($user['result']->is_delete == '0') {
                    return $this->update([
                        'is_delete' => '1'
                    ], $userId);
                }
                else {
                    return [
                        'bool' => false,
                        'message' => 'Something Went Wrong, Please try again'
                    ];
                }
            }
        }catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }
    public function suspendUser($userId){
        try {
            if (!auth()->user()->hasRole('super_admin')){
                return [
                    'bool' => false,
                    'message' => 'User has Not Permission to suspend'
                ];
            }
            $user =  $this->find($userId);

            if ($user['result']){
                return $this->update([
                    'is_suspended' => '1'
                ], $userId);
            }
            return [
                'bool' => false,
                'message' => 'User Not Exist'
            ];
        }catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }
    public function unsuspendUser($userId){
        try {
            $user =  $this->find($userId);

            if ($user['result']){
                return $this->update([
                    'is_suspended' => '0'
                ], $userId);
            }
            return [
                'bool' => false,
                'message' => 'User Not Exist'
            ];
        }catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * @param $email
     *
     * @return array
     */
    public function findByEmail($email): array
    {
        try {
            $result = $this->model->where('email', $email)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    // for checking multiple accounts using same email
    public function findMultipleEmails($email): array
    {
        try {
            $result = $this->model->where('email', $email)->get()->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    // for checking email and phone both
    public function checkEmailAndPhone($email, $phone): array
    {
            $result = $this->model->where('email', $email)->where('phone_number', $phone)->exists();

            if ($result) {
                return [
                    'bool' => true,
                    'result' => $result
                ];
            } else {
                return [
                    'bool' => false,
                    'result' => $result
                ];
            }


    }

    /**
     * @param $email
     *
     * @return array
     */
    public function findByEmailAndPhone($email,$phone,$dial_code): array
    {
        try {
            $result = $this->model->where('email', $email)->where('phone_number', $phone)->where('dial_code', $dial_code)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $email
     *
     * @return array
     */
    public function findAllUserByEmail($email): array
    {
        try {
            $result = $this->model->where('email', $email)->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    public function getByAttribute(array $values, array $with = [], $pluck = '', $return_type = 'json', $order = '')
    {
        $queryBuilder = $this->model;
        foreach ($values as $column => $value)
        {
            if (strpos($column, '~') !== false) {
                $column = explode('~', $column);
                $queryBuilder = $queryBuilder->where($column[0], $column[1], $value);
            }
            else
                $queryBuilder = $queryBuilder->where($column, $value);
        }

        if( count($with) > 0 )
            $queryBuilder = $queryBuilder->with($with);


        if( $order != '' )
            $queryBuilder = $queryBuilder->orderBy($order);

        if( $pluck != '' ) {
            $queryBuilder = $queryBuilder->get()->pluck($pluck);

            if( $return_type == 'array' )
                $queryBuilder->toArray();

            return $queryBuilder;
        }

        if( $return_type == 'array' )
            return $queryBuilder->toArray();


        return $queryBuilder->get();
    }

    /**
     * @param $commmunityId, $houseId
     * @return array
     * @throws Exception
     */
    public function getHeadOfFamily($communityId, $houseId)
    {
        try {
            $result = $this->model::where(['community_id'=>$communityId,'house_id'=>$houseId,'role_id'=>4])->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get all head of the family by community
     * @param $communityId
     * @return array
     */
    public function getUserByCommunity($communityId)
    {
        try {
            $result = $this->model->where('community_id', $communityId)->whereNotNull('community_id')->where('role_id',4)->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getUserByCommunityNew($communityId, $phone)
    {
        try {
            if (!auth()->user()->hasRole('super_admin')){
                $result = $this->model->where('community_id', $communityId)->wherePhoneNumber($phone)->first();
                return [
                    'bool' => true,
                    'result' => $result
                ];
            }else{
                $result = $this->model->wherePhoneNumber($phone)->first();
                return [
                    'bool' => true,
                    'result' => $result
                ];
            }


        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            $result = $this->model->updateOrCreate($where, $data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get user by community id ,house id ,phone number
     * @param $communityId
     * @param $houseId
     * @param $phoneNumber
     * @return mixed
     */
    public function getUserByCommunityHousePhoneNumber($communityId,$houseId,$phoneNumber){
        try {
            $result = $this->model::where('community_id',$communityId)->where('house_id',$houseId)->where('phone_number',$phoneNumber)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $verificationCode
     * @return array
     * @throws Exception
     */
    public function checkResetVerificationCode($verificationCode)
    {
        try {
            $result = $this->model->whereNotNull('reset_verification_code')->Where('reset_verification_code', $verificationCode)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get all residents by house ids
     * @param $communityId
     * @param $houseIds
     * @return array
     */
    public function getAllResidentsByHouseIds($communityId, $houseIds){
        try {
            $result = $this->model
                ->with('permissions')
                ->where('community_id',$communityId)
                ->whereIn('house_id',$houseIds)
                ->whereIn('role_id',[4,5]) // family member and head of the family
                ->whereNotNull('house_id')
                ->whereNotNull('community_id')
                ->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * update residents to Guest
     * @param $residentIds
     * @return array
     */
    public function updateResidentToGuestWithOutTryCatch($residentIds){

        UserPermission::whereIn('user_id',$residentIds)->forceDelete();
        return $this->model::whereIn('id',$residentIds)->update([
            'role_id' => 7, //guest
            'house_id' => null,
            'community_id' => null
        ]);

    }

    /**
     * deactivate community admin & guard
     * @param $communityId
     * @return mixed
     */
    public function deactivateCommunityAdminAndGuardWithOutTryCatch($communityId){

        return $this->model::where('community_id',$communityId)
            ->whereIn('role_id',[2,8,11])//community , guard, guard admin
            ->update([
                'status' => 'deactivate'
            ]);
    }

    /**
     * activate community admin & guard
     * @param $communityId
     * @return mixed
     */
    public function activateCommunityAdminAndGuardWithOutTryCatch($communityId){

        return $this->model::where('community_id',$communityId)
            ->whereIn('role_id',[2,8,11])//community , guard, guard admin
            ->update([
                'status' => 'active'
            ]);
    }

    /**
     * update residents to Guest
     * @param $guestId
     * @param $data
     * @param $permissions
     * @return array
     */
    public function updateGuestToResident($guestId,$data,$permissions){
        try {
            UserPermission::insert($permissions);
            $result = $this->model::where('id',$guestId)->update($data);
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
}
