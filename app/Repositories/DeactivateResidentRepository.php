<?php

namespace App\Repositories;

use App\Models\DeactivateResidents;
use App\Repositories\RepositoryInterface;
use Exception;

class DeactivateResidentRepository implements RepositoryInterface
{
    /**
     * Holds the instance of DeactivateResident model.
     *
     * @var DeactivateResidents
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param DeactivateResidents $deactivateResident
     */
    public function __construct(DeactivateResidents $deactivateResident)
    {
        $this->model = $deactivateResident;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function insert(array $data)
    {
        try {
            $result = $this->model->insert($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param array $data
     * @return mixed
     *
     */
    function directInsertWithOutTryCatch(array $data)
    {
        return $this->model::insert($data);
    }
    /**
     * @param array $data
     * @param int   $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $communityId
     * @return mixed
     */
    public function getAllByCommunity($communityId)
    {
        try {
            $result = $this->model::where('community_id',$communityId)->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function bulkDelete($ids)
    {
        try {
            $result = $this->model::whereIn('id',$ids)->delete();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param $ids
     * @return mixed
     */
    public function bulkDeleteWithOutTryCatch($ids)
    {
        return $this->model::whereIn('id',$ids)->delete();
    }

    /**
     * delete by userId
     * @param $userId
     * @return array
     */
    public function deleteByUserId($userId)
    {
        try {
            $result = $this->model->where('user_id',$userId)->delete();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
}
