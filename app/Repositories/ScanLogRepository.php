<?php

namespace App\Repositories;

use App\Models\ScanLog;
use App\Repositories\RepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class ScanLogRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var ScanLog
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param ScanLog $scanLog
     */
    public function __construct(ScanLog $scanLog)
    {
        $this->model = $scanLog;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = ScanLog::findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null, $userId = null, $houseId = null)
    {
        try {
            $queryBuilder = ScanLog::query()->with([
                'passUser.pass.community',
                'passUser.pass.createdBy:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code,dial_code',
                'passUser.pass.createdBy.house',
                'passUser.user.house',
                'passUser.user:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code,dial_code',
                'passUser.pass.event',
                'passUser.vehicle',
                'scanner',
                'scanBy',
                'staticScanLog'
            ]);

            //get by community Id
            if (!empty($communityId)){
                $queryBuilder = $queryBuilder->whereHas('passUser.pass.community', function($community) use($communityId) {
                    $community->where('id', $communityId );
                });
            }

            if(!empty($userId)){
                $queryBuilder = $queryBuilder->whereHas('passUser.pass.createdBy', function($createdBy) use($userId) {
                    $createdBy->where('id', $userId );
                });
            }
//
            if(!empty($houseId)){
                $queryBuilder = $queryBuilder->whereHas('passUser.pass.createdBy.house', function($community) use($houseId) {
                    $community->where('id', $houseId );
                });
            }

            $result  = $queryBuilder->orderByDesc('id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = ScanLog::find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $userId
     * @param int $limit
     * @return mixed
     */
    public function getScanLogs($userId, $limit = null)
    {
        try {
            $result = ScanLog::where('scan_by', $userId)
                ->orderBy('created_at', 'DESC')
                ->with([
                    'passUser.pass.event:name,id',
                    'passUser.pass.createdBy:id,first_name,last_name,phone_number',
                    'addedUser'
                ])
                ->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function getLatestScanLogById($id){
        try {
            $result = $this->model::where('scan_by',$id)->where('is_success','1')->orderBy('id','DESC')->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    public function getLatestScanLogByUserId($id){
        try {
            $result = $this->model::where('id',$id)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $communityId, $role, $startDate, $endDate
     * @return mixed
     */
    public function getScanLogsDashboardDateRange($communityId = null, $role = null, $startDate = null, $endDate = null)
    {
        try {
            $scanData = DB::table('scan_logs')->select('users.role_id', DB::raw('COUNT(`users`.`role_id`) AS total'), DB::raw('DATE(scan_logs.created_at) AS scan_date'), DB::raw('TIME(scan_logs.created_at) AS scan_time'))
                ->join('pass_users', 'pass_users.id', '=', 'scan_logs.pass_user_id')
                ->join('users', 'pass_users.user_id', '=', 'users.id')
                ->whereIn('users.role_id', $role)
                ->whereBetween(DB::raw('DATE(scan_logs.created_at)'), [$startDate, $endDate])
                ->groupBy('users.role_id')
                ->groupBy(DB::raw('DATE(scan_logs.created_at)'))
                ->orderBy('scan_logs.created_at');

            if(!empty($communityId)){
                $scanData = $scanData->where('scan_logs.community_id', $communityId);
            }

            $result = $scanData->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $communityId, $role, $startDate, $endDate
     * @return mixed
     */
    public function getScanLogsDashboardTimeRange($role, $startDate, $endDate)
    {
        try {
            $result = DB::table('scan_logs')->select('users.role_id', DB::raw('COUNT(`users`.`role_id`) AS total'), DB::raw('DATE(scan_logs.created_at) AS scan_date'), DB::raw('TIME(scan_logs.created_at) AS scan_time'), 'scan_logs.id as scan_log_id')
                ->join('pass_users', 'pass_users.id', '=', 'scan_logs.pass_user_id')
                ->join('users', 'pass_users.user_id', '=', 'users.id')
                ->whereIn('users.role_id', $role)
                ->whereBetween(DB::raw('DATE(scan_logs.created_at)'), [$startDate, $endDate])
                ->where('scan_logs.community_id', 61)
                ->groupBy('users.role_id')
                ->groupBy(DB::raw('DATE(scan_logs.created_at)'))
                ->orderBy('scan_logs.created_at')
                ->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function set_Pass_Vehicle($passUserId)
    {
        try {
            $result = ScanLog::where('pass_user_id',$passUserId)->orderBy('created_at','DESC')->get();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $scanLogId
     * @return mixed
     */
    public function getPrinterTemplate($scanLogId)
    {
        try {

            $result = ScanLog::with(['passUser','passUser.pass','passUser.vehicle','passUser.user','passUser.user.house'])
                ->where('id', $scanLogId)
                ->first();

            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }
}
