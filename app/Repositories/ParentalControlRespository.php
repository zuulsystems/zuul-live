<?php

namespace App\Repositories;

use App\Models\ParentalControl;
use App\Repositories\RepositoryInterface;
use Exception;

class ParentalControlRespository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var House
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param House $house
     */
    public function __construct(ParentalControl $parentControl)
    {
        $this->model = $parentControl;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getParentalControlValues($userId, $memberId)
    {
        try {
            $result = $this->model->where(['parent_id' => $userId, 'member_id' => $memberId])->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function firstOrCreate($where , $data){
        try {
            $result = $this->model->updateOrCreate($where,$data);
            if($result){
                $result = $this->model->where($where)->first() ;
                return [
                    'bool' => true,
                    'result' => $result
                ];
            }
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    //check if member is in blacklist
    public function checkMemberBlackList($memberId)
    {
        try {
            $parentalControl = $this->model;
            $result = $parentalControl->where(['member_id' => $memberId, 'blacklist_member_to_receive_request_a_pass' => '1'])->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $parentId, int $memberId
     * @return mixed
     * @throws Exception
     */
    public function getParentalControls($parentId, $memberId) {
        try{
            $result = $this->model::where(['parent_id'=>$parentId, 'member_id'=>$memberId])->first();

            if(empty($result)){
                $data= ['parent_id'=>$parentId, 'member_id'=>$memberId];
                $this->create($data);
            }

            $result = $this->model::where(['parent_id'=>$parentId, 'member_id'=>$memberId])->first();

            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
}
