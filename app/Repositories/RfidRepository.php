<?php

namespace App\Repositories;

use App\Models\AuthLog;
use App\Models\Rfid;
use App\Repositories\RepositoryInterface;
use Exception;

class RfidRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var AuthLog
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Rfid $rfid
     */
    public function __construct(Rfid $rfid)
    {
        $this->model = $rfid;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     */
    public function all($communityId = null, $userId = null, $houseId = null, $rfidId = null)
    {
        try {
            $queryBuilder = $this->model::with([
                'user:id,first_name,last_name,role_id,house_id,phone_number,email,street_address' ,
                'user.house',
                'community'
            ]);
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }
            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('user_id', $userId);
            }
            if (!empty($houseId)) {
                $queryBuilder = $queryBuilder->where('house_id', $houseId);
            }
            if(auth()->user()->hasRole('guard_admin')){
                $queryBuilder = $queryBuilder->whereIn('suspend', ['1', '0']);
            }
            if (!empty($rfidId)) {
                $queryBuilder = $queryBuilder->where('id', $rfidId);
            }
            if(!empty(auth()->user()->kioskPermission))
            {
                $communityIds = explode(",",auth()->user()->kioskPermission->communities);
                $queryBuilder = $queryBuilder->whereIn('community_id', $communityIds);
            }


            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Get unsuspended rfids
     *
     * @param null $communityId
     * @return mixed
     */
    public function getUnsuspendedRfids($communityId = null, $userId = null, $houseId = null)
    {
        try {
            $queryBuilder = $this->model::with([
                'user:id,first_name,last_name,role_id,house_id,phone_number,email,street_address' ,
                'user.house',
                'community'
            ]);
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }
            if (!empty($userId)) {
                $queryBuilder = $queryBuilder->where('user_id', $userId);
            }
            if (!empty($houseId)) {
                $queryBuilder = $queryBuilder->where('house_id', $houseId);
            }

            $queryBuilder = $queryBuilder->where('suspend', '0');

            $result = $queryBuilder->orderByDesc('id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function find_store_duplicate($rfid_tag_num, $community_id)
    {
        try {
            $result = $this->model->where('rfid_tag_num',$rfid_tag_num)->where('community_id',$community_id)->first();
            if(!$result){
                return [
                    'bool' => false,
                    'result' => $result
                ];
            }else{
                return [
                    'bool' => true,
                    'result' => $result
                ];

            }

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $rfid = $this->find($id)['result'];
            if (!empty($rfid)){
                if ($rfid->rfidDateLimits->isNotEmpty()){
                   foreach ($rfid->rfidDateLimits as $rfidDateLimit ){
                       $rfidDateLimit->rfidDateLimitWeeks()->delete();
                   }
                    $rfid->rfidDateLimits()->delete();
                }
                $rfid->rfidLogs()->delete();
            }
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function findByRfidTAgNum($rfidTagNum)
    {
        try {
            $result = $this->model->where('rfid_tag_num',$rfidTagNum)->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $rfidTagNum
     * @return array
     */
    public function findByTagNum($rfidTagNum)
    {
        try {
            $result = $this->model::where('rfid_tag_num',$rfidTagNum)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * unsuspended rfid by tag number
     *
     * @param $rfidTagNum
     * @return array
     */
    public function findByTagNumUnsuspendedRfid($rfidTagNum,$community_id)
    {
        try {
            $result = $this->model::with('vehicle')->where(['rfid_tag_num'=>$rfidTagNum,'community_id'=>$community_id,'suspend'=> '0'])->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $licensePlate
     * @return array
     */
    public function findByLicensePlate($licensePlate)
    {
        try {
            $result = $this->model::where('license',$licensePlate)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
}
