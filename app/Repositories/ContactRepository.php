<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Repositories\RepositoryInterface;
use Exception;

class ContactRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var Contact
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Contact $Contact
     */
    public function __construct(Contact $Contact)
    {
        $this->model = $Contact;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model::with('userContacts')->whereId($id)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result =  $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $userId
     * @param int  $limit
     * @param null $search
     *
     * @return mixed
     */
    public function getByUserId($userId, $limit = 50, $search = null)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'userContacts',
                'userContacts.contact',
            ]);
            $queryBuilder = $queryBuilder->where('created_by', $userId);
            if (!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where('group_name', 'LIKE', "%$search%");
                });
            }

            if (is_null($limit)) {
                return $queryBuilder->get();
            }

            $result = $queryBuilder->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
