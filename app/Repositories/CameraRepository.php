<?php

namespace App\Repositories;

use App\Models\Camera;
use App\Repositories\RepositoryInterface;
use Exception;

class CameraRepository implements RepositoryInterface
{
 /**
     * Holds the instance of Camera model.
     *
     * @var Scanner
     */
    public $model;

    /**
     * CameraRepository constructor.
     *
     * @param Camera $camera
     */
    public function __construct(Camera $camera)
    {
        $this->model = $camera;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

     /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @return mixed
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model::with([
                'community',
                'scanner',
                'connectedpie'
            ]);
            if(!empty($communityId)){
                $query->where('community_id',$communityId);
            }

            if(!empty(auth()->user()->kioskPermission))
            {
                $communityIds = explode(",",auth()->user()->kioskPermission->communities);
                $query->whereIn('community_id',$communityIds);
            }

            $result = $query;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $macAddress
     * @param $secretKey
     * @return array
     */
    public function getByCredentials($macAddress,$secretKey){
        try {
            $result = $this->model::where('mac_address', $macAddress)->where('secret_key',$secretKey )->first();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function findByColumn($column,$value)
    {
        try {
            $result = $this->model::where($column,$value)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function findByCameraCount($column,$value,$communityId)
    {
        try {
            $result = $this->model::where($column,$value)->where('community_id',$communityId)->count();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
 }
