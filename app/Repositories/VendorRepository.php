<?php

namespace App\Repositories;

use App\Models\Vendor;
use App\Models\VehicleVendor;
use App\Repositories\RepositoryInterface;
use Exception;

class VendorRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var Vendor
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Vendor $vendor
     */
    public function __construct(Vendor $vendor)
    {
        $this->model = $vendor;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $communityId
     * @param null $userId
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null, $userId = null)
    {
        try {
            $query = $this->model::with(['community','vehicles'=> function($vehicles){
                $vehicles->withCount('trafficTickets');
            }
            ]);

            if(!empty($communityId)){
                $result = $query->where('community_id', $communityId);
            }
            $result = $query->get();

            // dd($result, "result");

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

        /**
     * get vendors list
     * @param null $communityId
     * @return mixed
     */
    public function getVendorList($communityId = null)
    {
        try {
            $queryBuilder = $this->model::with([
                'vehicle_vendor' => function ($data) {
                    $data->select('vehicle_id', 'vendor_id');
                },
                'community' => function ($community) {
                    $community->select('id', 'name');
                }
            ]);
            //get vendors by community Id
            if (!empty($communityId)) {
                $queryBuilder = $queryBuilder->where('community_id', $communityId);
            }
            $queryBuilder = $queryBuilder->select('id', 'vendor_name', 'email', 'phone', 'community_id','address', 'created_at');
            $result = $queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            $result = $this->model->updateOrCreate($where, $data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    public function getVendorVehicleLicensePlates($vendor_id){
        try {
            $result = VehicleVendor::where('vendor_id', $vendor_id)->with('vehicles')->get()->pluck('vehicles')->pluck('license_plate', 'id');
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
