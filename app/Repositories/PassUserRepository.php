<?php

namespace App\Repositories;

use App\Models\PassUser;
use App\Repositories\RepositoryInterface;
use DB;
use Exception;

class PassUserRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var PassUser
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param PassUser $passUser
     */
    public function __construct(PassUser $passUser)
    {
        $this->model = $passUser;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model::with('pass');
            if (!empty($communityId)) {
                $query->whereHas('pass', function ($pass) use ($communityId) {
                    $pass->where('community_id', $communityId);
                });
            }
            $result = $query->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function with($table, $id)
    {
        try {
            $result = $this->model->with($table)->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getPassUsersByUser($userId, $limit = null)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'pass',
                'pass.community',
                'pass.house',
                'pass.createdBy',
                'pass.event',
                'pass.createdBy.community',
            ]);
            $queryBuilder = $queryBuilder->where('user_id', $userId);

            $result = $queryBuilder->paginate($limit);
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $userId , $limit
     *
     * @return mixed
     * @throws Exception
     */
    public function getUserScannedPasses($userId, $limit = 50)
    {
        try {
            $result = $this->model::where('user_id', $userId)
                ->where('is_scan', '1')
                ->whereHas('pass')
                ->with('pass.event', 'user', 'scanLogs', 'pass.community', 'pass.house')
                ->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $userId
     * @param $isExpired
     * @param null $search
     * @param null $filterType
     * @param null $filterData
     * @param int $limit
     * @return array
     */
    public function getActivePassUserByUser($userId, $isExpired, $search = null, $filterType = null, $filterData = null, $limit = 10)
    {
        try {
            $expire = ($isExpired) ? "<=" : ">="; // get expired or active passes
            $currentDate = date('Y-m-d H:i:s');
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'pass',
                'user',
                'pass.community',
                'pass.house',
                'pass.createdBy',
                'pass.event',
                'pass.createdBy.community',
            ]);

            $queryBuilder->whereHas('pass', function ($pass) use ($expire, $currentDate) {
                $pass->where('pass_date', $expire, $currentDate)->orderBy('pass_date', 'desc');
            });


            //search by filter type
            if (!empty($search) || (!empty($filterType) && !empty($filterData))) {

                if (!empty($search)) {
                    $queryBuilder->where(function ($query) use ($search) {
                        $query->whereHas('pass.event', function ($passEvent) use ($search) {
                            $passEvent->where('name', 'like', "$search%");
                        })->orWhereHas('pass.community', function ($passCommunity) use ($search) {
                            $passCommunity->where('name', 'like', "$search%");
                        })->orWhereHas('pass.createdBy', function ($user) use ($search) {
                            $user->where('first_name', 'like', "$search%")->orWhere('last_name', 'like', "$search%");
                        });
                    });
                }

                if ($filterType == 'event_name') {
                    $queryBuilder->whereHas('pass.event', function ($passEvent) use ($filterData) {
                        $passEvent->where('name', 'like', "$filterData%");
                    });
                } else if ($filterType == "community") {
                    $queryBuilder->whereHas('pass.community', function ($passCommunity) use ($filterData) {
                        $passCommunity->where('name', 'like', "$filterData%");
                    });
                } else if ($filterType == "sender_last_name") {
                    $queryBuilder->whereHas('pass.createdBy', function ($user) use ($filterData) {
                        $user->where('first_name', 'like', "$filterData%")->orWhere('last_name', 'like', "%$filterData");
                    });
                } else if ($filterType == "date") {
                    $date = date('Y-m-d', strtotime($filterData));
                    $queryBuilder->whereHas('pass', function ($q) use ($date) {
                        $q->whereDate('pass_start_date', $date);
                    });
                }

            }

            $queryBuilder = $queryBuilder->where('user_id', $userId)->where('is_scan', '0')->orderByDesc('id');

            $result = $queryBuilder->paginate($limit);
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $userId
     * @param $isExpired
     * @return array
     */
    public function getActivePassUserByUserCount($userId, $isExpired)
    {
        try {
            $expire = ($isExpired) ? "<=" : ">="; // get expired or active passes
            $currentDate = date('Y-m-d H:i:s');
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'pass',
                'user',
                'pass.community',
                'pass.house',
                'pass.createdBy',
                'pass.event',
                'pass.createdBy.community',
            ]);

            $queryBuilder->whereHas('pass', function ($pass) use ($expire, $currentDate) {
                $pass->where('pass_date', $expire, $currentDate)->orderBy('pass_date', 'desc');
            });

            $queryBuilder = $queryBuilder->where('user_id', $userId)->where('is_scan', '0')->orderByDesc('id')->count();

            $result = $queryBuilder;

            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get active passes dates by user
     * @param $userId
     * @param $isExpired
     * @return mixed
     */
    public function getActivePassUserDatesByUser($userId, $isExpired)
    {
        $queryBuilder = $this->model;
        $expire = ($isExpired) ? "<=" : ">=";
        return $queryBuilder::select(
            'pass_users.id as pass_user_id',
            'pass_users.user_id',
            'passes.id as pass_id',
            'passes.pass_date',
            'passes.pass_start_date')
            ->join('passes', 'pass_users.pass_id', 'passes.id')
            ->where('pass_users.user_id', $userId)
            ->where('pass_users.is_scan', '0')
            ->where('passes.pass_date', $expire, date('Y-m-d H:i:s'))
            ->whereNull('passes.deleted_at') // passes without trashed
            ->groupBy(DB::raw("date_format(passes.pass_start_date, '%Y%m%d')"))
            ->orderBy('passes.pass_start_date', 'ASC')
            ->get();
    }

    /**
     * get scanned passes by user
     * @param $userId
     * @param null $search
     * @param null $filterType
     * @param null $filterData
     * @param int $limit
     * @return array
     */
    public function getScannedPassUserByUser($userId, $search = null, $filterType = null, $filterData = null, $limit = 10)
    {
        try {
            $queryBuilder = $this->model;
            $currentDate = date('Y-m-d H:i:s');
            $queryBuilder = $queryBuilder->with([
                'pass',
                'user',
                'pass.community',
                'pass.createdBy',
                'pass.event',
                'pass.createdBy.community',
            ]);
            #todo need to discuss ->withTrashed() functionality

            //search all data
            if (!empty($search)) {
                $queryBuilder->where(function ($where) use ($search) {
                    $where->WhereHas('pass.createdBy', function ($pass) use ($search) {
                        $pass->where('first_name', 'like', "%$search%")->orWhere('last_name', 'like', "%$search%");
                    });
                    $where->orWhereHas('pass.event', function ($passEvent) use ($search) {
                        $passEvent->where('name', 'like', "%$search%");
                    });
                    $where->orWhereHas('pass.community', function ($passCommunity) use ($search) {
                        $passCommunity->where('name', 'like', "%$search%");
                    });
                });
            }

            //search by filter type
            if (!empty($filterType) && !empty($filterData)) {

                if ($filterType == 'event_name') {
                    $queryBuilder->whereHas('pass.event', function ($passEvent) use ($filterData) {
                        $passEvent->where('name', 'like', "%$filterData%");
                    });
                }
                if ($filterType == "community") {
                    $queryBuilder->whereHas('pass.community', function ($passCommunity) use ($filterData) {
                        $passCommunity->where('name', 'like', "%$filterData%");
                    });
                }
                if ($filterType == "sender_last_name") {
                    $queryBuilder->whereHas('pass.createdBy', function ($user) use ($filterData) {
                        $user->where('first_name', 'like', "%$filterData%")->orWhere('last_name', 'like', "%$filterData%");
                    });
                }
                if ($filterType == "date") {
                    $date = date('Y-m-d', strtotime($filterData));
                    $queryBuilder->whereHas('pass', function ($q) use ($date) {
                        $q->whereDate('pass_start_date', $date);
                    });
                }
            }

            $queryBuilder = $queryBuilder->where('user_id', $userId)->where('is_scan', '1')->orderByDesc('id');

            $result = $queryBuilder->paginate($limit);
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get scanned passes dates by user
     * @param $userId
     * @return mixed
     */
    public function getScannedPassUserDatesByUser($userId)
    {
        try {
            $result = $this->model::select(
                'pass_users.id as pass_user_id',
                'pass_users.user_id',
                'passes.id as pass_id',
                'passes.pass_date',
                'passes.pass_start_date')
                ->join('passes', 'pass_users.pass_id', 'passes.id')
                ->where('pass_users.user_id', $userId)->where('is_scan', '1')
                ->whereNull('passes.deleted_at') // passes without trashed
                ->groupBy(DB::raw("date_format(passes.pass_start_date, '%Y%m%d')"))
                ->orderBy('passes.pass_start_date', 'ASC')
                ->get();
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get pass detail by passUserId
     * @param $passUserId
     * @param $userId
     * @return mixed
     */
    public function getSinglePassUser($passId, $userId)
    {
        try {
            $result = $this->model::select('*')
                ->with([
                    'pass',
                    'user',
                    'user.userContacts',
                    'pass.community',
                    'pass.createdBy',
                    'pass.event',
                    'pass.createdBy.community',
                    'vehicle'
                ])
                ->where('pass_id', $passId)
                ->where('user_id', $userId)
                ->first();
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getFamilyPassUser($passId, $userId)
    {
        try {
            $result = $this->model::select('*')
                ->with([
                    'pass',
                    'user',
                    'user.userContacts',
                    'pass.community',
                    'pass.createdBy',
                    'pass.event',
                    'pass.createdBy.community',
                    'vehicle'
                ])
                ->where('pass_id', $passId)
                ->where('user_id', $userId)
                ->first();
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Delete notification by passId
     *
     * @param $passId
     * @return mixed
     */
    public function deleteByPass($passId)
    {
        try {
            $result = $this->model::where('pass_id', $passId)->delete();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Get pass User by QrCode
     * @param $qrCode
     * @return array
     */

    public function getByQrCode($qrCode)
    {
        try {
            $result = $this->model::select('*')
                ->WhereHas('user', function ($q) {
                })
                ->with([
                    'user',
                    'pass.community',
                    'pass.createdBy',
                ])
                ->where('qr_code', $qrCode)
                ->first();
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Get pass User by QrCode
     * @param $data
     * @param $qrCode
     * @return array
     */

    public function updateOrGetByQrCode($data, $qrCode)
    {
        try {
            $result = $this->model::where('qr_code', $qrCode)->update($data);
            if ($result) {
                $result = $this->model::where('qr_code', $qrCode)->first();
                return [
                    'bool' => true,
                    'result' => $result
                ];
            } else {
                return [
                    'bool' => false,
                    'message' => 'Something went wrong.Please try again.'
                ];
            }
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Get pass User by QrCode For notifications
     * @param $qrCode
     * @return array
     */

    public function getByQrCodeForNotification($qrCode)
    {
        try {
            $result = $this->model::select('*')
                ->with([
                    'user',
                    'pass.event',
                    'pass.community',
                    'pass.createdBy'
                ])
                ->where('qr_code', $qrCode)
                ->first();
            return [
                'bool' => true,
                'result' => $result
            ];

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get passes recipients by passId
     * @param $passId
     * @param null $isScan
     * @return mixed
     */
    public function getPassRecipients($passId, $isScan = false)
    {
        try {
            $queryBuilder = $this->model::with([
                'user',
                'successScanLog'
            ]);
            if ($isScan) {
                $queryBuilder->where('is_scan', '1');
            }
            $queryBuilder->where('pass_id', $passId);
            $result = $queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
