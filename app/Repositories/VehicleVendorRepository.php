<?php

namespace App\Repositories;

use App\Models\VehicleVendor;
use App\Repositories\RepositoryInterface;
use Exception;

class VehicleVendorRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var VehicleVendor
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param VehicleVendor $vehicleVendor
     */
    public function __construct(VehicleVendor $vehicleVendor)
    {
        $this->model = $vehicleVendor;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all($communityId = null)
    {
        try {
            $query = $this->model::with([
                'community' => function($community){
                    $community->select('id','name');
                }
            ]);
            if(!empty($communityId)){
                $query->where('community_id',$communityId);
            }
            $result = $query->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getVendorVehicles($vendorId)
    {
        try {
            $result = $this->model->where('vendor_id', $vendorId)->pluck('vehicle_id');

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $where
     * @param $data
     *
     * @return mixed
     */
    public function updateOrCreate($where, $data)
    {
        try {
            $result = $this->model::updateOrCreate($where, $data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
