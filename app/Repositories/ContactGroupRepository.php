<?php

namespace App\Repositories;

use App\Models\ContactGroup;
use App\Repositories\RepositoryInterface;
use Exception;

class ContactGroupRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var ContactGroup
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param ContactGroup $contactGroup
     */
    public function __construct(ContactGroup $contactGroup)
    {
        $this->model = $contactGroup;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model::with('userContacts')->whereId($id)->first();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $group =  $this->find($id)['result'];
            if (!empty($group->userContacts)){
                $group->userContacts()->detach();
            }
            if ($group){
                $group->delete();
            }
            $result =  $group;

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param null $userId
     * @param int  $limit
     * @param null $search
     *
     * @return mixed
     */
    public function getByUserId($userId, $limit = 50, $search = null)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = $queryBuilder->with([
                'userContacts',
                'userContacts.contact',
            ]);
            $queryBuilder = $queryBuilder->where('created_by', $userId);
            if (!empty($search)) {
                $queryBuilder = $queryBuilder->where(function ($query) use ($search) {
                    $query->where('group_name', 'LIKE', "%$search%");
                });
            }
            $queryBuilder = $queryBuilder->orderBy('group_name','ASC');
            $result = !empty($limit)?$queryBuilder->paginate($limit):$queryBuilder->get();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

}
