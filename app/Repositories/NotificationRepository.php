<?php

namespace App\Repositories;

use App\Models\Notification;
use App\Repositories\RepositoryInterface;
use Exception;

class NotificationRepository implements RepositoryInterface
{
    /**
     * Holds the instance of User model.
     *
     * @var Notification
     */
    public $model;

    /**
     * UserRepository constructor.
     *
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->model = $notification;
    }

    /**
     * @param array $data
     *
     * @return mixed
     * @throws Exception
     */
    function create(array $data)
    {
        try {
            $result = $this->model->create($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return mixed
     * @throws Exception
     */
    function update(array $data, $id)
    {
        try {
            $result = $this->model->findOrFail($id)->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            $result = $this->model::all();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function find($id)
    {
        try {
            $result = $this->model->find($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param int $id
     *
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $result = $this->model->destroy($id);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * @param
     *
     * @return mixed
     * @throws Exception
     */
    public function getNotifications($notificationType, $userId, $limit = 50)
    {
        try {
            $queryBuilder = $this->model;
            $queryBuilder = Notification::where([
                'type' => $notificationType,
                'user_id' => auth()->user()->id,
            ])->with('passRequest.sendUser');

            $result = $queryBuilder->paginate($limit);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * Deleting notification of user who received pass request
     *
     * @param int $id, $pass_request_id
     *
     * @return mixed
     * @throws Exception
     */
    public function deleteNotifcation($id, $pass_request_id)
    {
        try {
            $result = $this->model->where(['user_id'=>$id, 'pass_request_id'=>$pass_request_id])->delete();

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
    /**
     * Mark read push or parental control notifications
     *
     * @param array $data, int $id, string $type
     *
     * @return mixed
     * @throws Exception
     */
    public function updateNotification(array $data, $id, $type)
    {
        try {
            $result = $this->model->where(['id'=>$id, 'type'=>$type])->update($data);

            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * Get notifications by type
     * @param $userId
     * @param $type
     * @param int $limit
     * @return mixed
     */
    public function getNotificationsByType($userId,$type,$limit = 10){
        $queryBuilder = $this->model;
        $queryBuilder = $queryBuilder::with('passRequest')->where('type',$type)->orderBy('created_at', 'desc');
        if($type == 'parental_control'){
            $queryBuilder->where('parent_id', $userId);
        } else {
            $queryBuilder->where('user_id', $userId);
        }
        return $queryBuilder->paginate($limit);
    }

    /**
     * @param $type
     * @param $userId
     * @return int
     */
    public function getTotalNotifications($userId,$type = 'zuul'){
        $queryBuilder = $this->model;
        $queryBuilder = $queryBuilder::where('type',$type)->where('is_read','0');
            if($type == 'parental_control'){
                $queryBuilder->where('parent_id', $userId);
            } else {
                $queryBuilder->where('user_id', $userId);
            }
       return $queryBuilder->count();
    }

    /**
     * Delete notification by passId
     *
     * @param $passId
     * @return mixed
     */
    public function deleteNotificationByPass($passId)
    {
        try {
            $result = $this->model->zuul()->where('category','send_pass')->where('pass_id',$passId)->delete();
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * get unread announcements notifications
     * @param $userId
     * @param int $limit
     * @return array
     */
    public function getUnreadAnnouncements($userId,$limit = 10){
        try {

          $result = $this->model->zuul()->where('user_id', $userId)
            ->where('category','unread_announcement')
            ->where('is_read', '0')
            ->with('announcement')
            ->orderBy('created_at', 'DESC')
            ->paginate($limit);
            return [
                'bool' => true,
                'result' => $result
            ];
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }
}
