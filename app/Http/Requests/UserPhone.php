<?php

namespace App\Http\Requests;

use App\Helpers\APIResponse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UserPhone extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function merge(array $input)
    {
        $phone = preg_replace('/\D/', '', $input['phone']);
        $phone = substr($phone, -10);
        return [
            'phone' => $phone
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|integer|min:10'
        ];
    }

    public function messages()
    {
        return [
            'phone' => 'Please provide a valid phone number'
        ];
    }

    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return JsonResponse|void
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        return APIResponse::error($validator->errors()->first());
    }
}
