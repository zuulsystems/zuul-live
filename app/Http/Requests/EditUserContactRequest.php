<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this['phone_number'] = trim(str_replace("-", "", $this['phone_number']));
        return [
            'contact_name' => 'required',
            'phone_number' => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'contact_name.required' => 'Please enter member name',
            'phone_number.required' => 'Please select phone number',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->has('phone_number') && preg_match_all( "/[0-9]/", $this['phone_number'] ) < 10) {
                $validator->errors()->add('phone_number', 'Phone number must be at least 10 digit long');
            }
        });
    }
}
