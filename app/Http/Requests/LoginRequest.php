<?php

namespace App\Http\Requests;

use App\Helpers\APIResponse;
use App\Rules\PhoneNumberLengthChecker;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(APIResponse::error($validator->errors()->first()));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->request->all();
        return [
            'phone_number' => ['required', new PhoneNumberLengthChecker($data['dial_code'])],
            'password' => 'required',
            'dial_code' => 'required',
        ];
    }
}
