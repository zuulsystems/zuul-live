<?php

namespace App\Http\Requests;

use App\models\CountryPhoneFormat;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AddUserContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this['phone_number'] = trim(str_replace("-", "", $this['phone_number']));
        return [
            'contact_name' => 'required',
            'phone_number' => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'contact_name.required' => 'Please enter member name',
            'phone_number.required' => 'Please select phone number',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $dialCode = str_replace("+","",$this['dial_code']);

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code',$dialCode)->first();

            $phoneNumberDigits = 10;

            if(!empty($countryPhoneFormat))
            {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            if ($this->has('phone_number') && preg_match_all( "/[0-9]/", $this['phone_number'] ) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least '.$phoneNumberDigits.' digit long');
            }

            if (auth()->user()->contacts()->where('phone_number',$this['phone_number'])->count() > 0) {
                $validator->errors()->add('phone_number', 'Phone number already exist');
            }

        });
    }
}
