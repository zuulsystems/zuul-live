<?php

namespace App\Http\Requests;

use App\Rules\ValidatePhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class FrontendPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone_number' => onlyPhoneDigits($this['phone_number']),
            'resident_phone_number' => onlyPhoneDigits($this['resident_phone_number'])
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'phone_number' => ['required',new ValidatePhoneNumber],
            'resident_phone_number' => ['required',new ValidatePhoneNumber],
            'comment' => 'required',
        ];
    }
}
