<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\TwoFactorCodeEmail;
use App\Models\AuthLog;
use App\Models\CountryPhoneFormat;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $userName = "phone_number";//email

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param mixed $user
     * @return mixed
     */
    public function authenticated(Request $request, User $user)
    {
        if (isset(auth()->user()->community) && auth()->user()->community->community_status == "inactive") {
            //Mark:- Track Activity
            callUserActivityLogs("User Logged Out", __FUNCTION__, __FILE__, __DIR__, true, 'Logged Out');

            Auth::logout();
            session()->flash('deactivated', 'Community Features are disabled, please contact admin@zuulsystems.com');
            return redirect()->route('login');

        }

        if (auth()->user()->is_reset_password == "1") {
            return redirect()->route('password-reset');
        }


        //Mark:- Track Activity
        callUserActivityLogs("User Logged In", __FUNCTION__, __FILE__, __DIR__, true, 'Login Successfully');

        $role = $user->role->slug;
        AuthLog::create([
            'ip_address' => $request->ip(),
            'user_agent' => $request->userAgent(),
            'user_id' => $user->id
        ]);

        if ($this->userName == "email") {
            $checkTwoFactorAuth = User::where(['id' => $user->id, 'is_two_factor' => '1'])->first();
            if (!empty($checkTwoFactorAuth)) {
                $data["code"] = rand(100000, 999999);
                $user->is_two_factor_code = $data["code"];
                $user->is_two_factor_on = 1;
                $user->save();
                Mail::to($user->email)->send(new TwoFactorCodeEmail($data));
                return redirect(Route('two-factor-verification.index'));
            }
        }

        if ($role == 'family_head' || $role == 'family_member') {
            return redirect()->route('dashboard');
        } else if (
            $role == 'super_admin' ||
            $role == 'sub_admin' ||
            $role == 'tl_admin' ||
            $role == 'guard_admin'
        ) {
            return redirect()->route('admin.dashboard');
        } else if ($role == 'kiosk') {
            return redirect()->route('admin.settings.scanners.index');
        }
    }

    /**
     * Validate the user login request.
     *
     * @param Request $request
     * @return void
     *
     * @throws ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $userType = $request->userType;
        if ($userType == 'admin') {
            $this->redirectTo = "/admin";
            $this->userName = "email";
        } else if ($userType == 'guard') {
            $this->userName = "phone_number";
            $this->redirectTo = "/admin";
        } else if ($userType == 'guest_resident') {
            $this->userName = "phone_number";
            $this->redirectTo = "/dashboard";
        } else if ($userType == 'kiosk') {
            $this->userName = "email";
            $this->redirectTo = "/dashboard";
        }

        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return $this->userName;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {

        $request->phone_number = str_replace("(", "", $request->phone_number);
        $request->phone_number = str_replace(")", "", $request->phone_number);
        $request->phone_number = str_replace("-", "", $request->phone_number);

        $userType = $request->userType;
        //default login  (Super admin, Community Admin, TL Admin, Guard Admin)
        $credentials = ['email' => $request->email, 'password' => $request->password, 'role_id' => [1, 2, 10, 11]];
        if ($userType == 'admin') { //can only login (Super admin, Community Admin, TL Admin, Guard Admin)
            $credentials = ['email' => $request->email, 'password' => $request->password, 'role_id' => [1, 2, 10, 11]];
        } else if ($userType == 'guard') { //can only  login (Security Guard)
            $credentials = ['phone_number' => $request->phone_number, 'password' => $request->password, 'role_id' => 11];
        } else if ($userType == 'guest_resident') { //can only  login (Resident, Head of the family, Guests / Outsiders Daily)
            $credentials = ['phone_number' => $request->phone_number, 'password' => $request->password, 'role_id' => [4, 5, 7]];
        } else if ($userType == 'kiosk') { //can only  login (Resident, Head of the family, Guests / Outsiders Daily)
            $credentials = ['email' => $request->email, 'password' => $request->password, 'role_id' => [12]];
        }

        return $this->guard()->attempt(
            $credentials, $request->filled('remember')
        );

    }

}
