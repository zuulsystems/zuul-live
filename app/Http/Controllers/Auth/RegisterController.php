<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\CountryPhoneFormat;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\UserCommon;

class RegisterController extends Controller
{
    use UserCommon;

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['phone_number'] = str_replace("(", "", $data['phone_number']);
        $data['phone_number'] = str_replace(")", "", $data['phone_number']);
        $data['phone_number'] = str_replace("-", "", $data['phone_number']);

        $validations = [
            'name' => 'required|string|max:255'
        ];

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $data['dial_code'])->first();
        $phoneDigits = 10;
        if (!empty($countryPhoneFormat)) {
            $phoneDigits = $countryPhoneFormat->digit;
        }

        $validations['phone_number'] = 'required|digits:' . $phoneDigits;

        $validations['password'] = 'required||confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/';

        $validator = Validator::make($data, $validations);

        return $validator->after(function ($validator) use ($data) {
            $phoneNumber = $data['phone_number'];
            $phoneNumberMessage = 'The phone number has already been taken';
            //check if phone number already exist
            if (!empty($phoneNumber)) {
                //check if guest and created there password
                $userByPhone = User::where('phone_number', $phoneNumber)->whereNotNull('phone_number')->first();
                if (!empty($userByPhone) && ($userByPhone->hasRole('guests_outsiders_daily') || $userByPhone->hasRole('guests_outsiders_one_time')) &&
                    (!empty($userByPhone->password))) {
                    $validator->errors()->add('phone_number', $phoneNumberMessage);
                } else if (!empty($userByPhone) && (!$userByPhone->hasRole('guests_outsiders_daily') && !$userByPhone->hasRole('guests_outsiders_one_time'))) {
                    $validator->errors()->add('phone_number', $phoneNumberMessage);
                }
            }
        });
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $names = $this->splitName($data['name']);

        $dialCode = $data['dial_code'];

        $phoneFormat = "999-999-9999";

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $data['dial_code'])->first();
        if (!empty($countryPhoneFormat)) {
            $phoneFormat = $countryPhoneFormat->format;
        }

        $data['phone_number'] = str_replace("(", "", $data['phone_number']);
        $data['phone_number'] = str_replace(")", "", $data['phone_number']);
        $data['phone_number'] = str_replace("-", "", $data['phone_number']);

        $user = User::where('phone_number', $data['phone_number'])->whereNotNull('phone_number')->first();
        if (!empty($user) && ($user->hasRole('guests_outsiders_daily') || $user->hasRole('guests_outsiders_one_time')) &&
            (empty($user->password))
        ) {
            $updateUser = User::where('id', $user->id)->update([
                'first_name' => $names["first_name"],
                'middle_name' => $names["middle_name"],
                'last_name' => $names["last_name"],
                'password' => bcrypt($data['password'])
            ]);

            //Mark:- Track Activity
            callUserActivityLogs("Signup, Override the previous account", __FUNCTION__, __FILE__, __DIR__, true, 'Signup, Override the previous account');

            if ($updateUser) {
                return User::find($user->id);
            }

        } else {
            return User::create([
                'dial_code' => $data['dial_code'],
                'first_name' => $names["first_name"],
                'middle_name' => $names["middle_name"],
                'last_name' => $names["last_name"],
                'phone_number' => $data['phone_number'] ?? '',
                'role_id' => 7,
                'password' => $data['password'] ? bcrypt($data['password']) : null,
            ]);

            //Mark:- Track Activity
            callUserActivityLogs("New User Registered", __FUNCTION__, __FILE__, __DIR__, true, 'Signup Successfully');

        }

    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {

        //User register now here you can run any code you want
        if (
            $user->hasRole('family_head') ||
            $user->hasRole('family_member') ||
            $user->hasRole('guests_outsiders_daily')
        ) {
            $this->redirectTo = '/dashboard';
        }
    }
}
