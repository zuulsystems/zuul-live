<?php

namespace App\Http\Controllers\Node;

use App\Helpers\APIResponse;
use App\Http\Controllers\API\ScanController;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Models\BannedCommunityUser;
use App\Models\Camera;
use App\Models\CameraSocket;
use App\Models\CameraSocketImage;
use App\Models\ConnectedPie;
use App\Models\RemoteGuard;
use App\Models\RemoteGuardImage;
use App\Models\RemoteGuardSocket;
use App\Models\Scanner;
use App\Models\User;

// use Hamcrest\Core\HasToString;
use App\Services\NotificationService;
use App\Services\OffensiveWordService;
use App\Services\ParentalControlService;
use App\Services\PassRequestService;
use App\Services\PassService;
use App\Services\UserContactService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\ScanLog;
use App\Services\PassUserService;
use App\Http\Resources\QuickPassCollection;
use App\Services\ScanLogService;
use App\Services\StaticScanLogService;

use App\S3ImageHelper;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\VarDumper\Cloner\Data;

class ZuulNodeController extends Controller
{
    public $passUserService;
    public $scanLogService;
    public $staticScanLogService;
    /**
     * @var NotificationService
     */
    public $notificationService;


    //
    public function pong()
    {
        try {

            return response()->json(["data" => "pong"]);
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function socketConnection(Request $request)
    {
        $data = $request->all();
        try {

            $cms = RemoteGuardSocket::updateOrCreate(
                ['remote_guard_id' => $data['remote_guard_id']],
                ['socket_id' => $data['socketId']]
            );

            if (!$cms) {
                return APIResponse::error("Something went wrong");
            }

            if ($cms->remote_guards && $cms->remote_guards->scanner && $cms->remote_guards->scanner->id) {
                $cms['scanner_id'] = $cms->remote_guards->scanner->id;
            }

            if ($cms->remote_guards && $cms->remote_guards->community && $cms->remote_guards->community->webRelay && $cms->remote_guards->community->webRelay->id) {
                $cms['web_relay_id'] = $cms->remote_guards->community->webRelay->id;
            }


            return APIResponse::success($cms, "Socket created");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function getRfidCodes(Request $request)
    {

        $authUser = $request->header('php-auth-user');
        $authPass = $request->header('php-auth-pw');
        $scanner = $this->scannerService->getByCredentials($authUser, $authPass)['result'];
        if (empty($scanner)) {
            return APIResponse::error('Something went wrong.Please try again.');
        }
        $rfids = array();
        $pkr = [];
        $communityRfids = $this->rfidService->getUnsuspendedRfids($scanner->community_id)['result']->get();

        if ($communityRfids->isNotEmpty()) {
            $rfids = $communityRfids->pluck('rfid_tag_num');
        }

        if (!empty($rfids)) {
            foreach ($rfids as $rfid) {
                if ($this->rfidDateLimitService->checkIfScanTagLieInTimeRange($rfid)) {
                    if ($rfid) {
                        if ($scanner->community->id != 61) {
                            $rfidCode = $scanner->rfid_multiplier * $scanner->community->rfid_site_code + $rfid;
                            $pkr[] = $rfidCode;
                        } else {
                            $rfidCode = $rfid;
                            $pkr[] = $rfidCode;
                        }


                    }

                }
            }
        }
        $list = implode("\n", $pkr);
        return response($scanner->mac_address . "\n" . $list, 200)->header('Content-Type', 'text/plain');
    }

    public function guardrequest(Request $request)
    {
        try {
            $data = $request->all();

            $userId = auth()->user()->id;
            $user = User::where(['id' => $userId])->first();
            $communityId = $user->community_id;
            $cameraId = $user->camera_id;


            if (!$communityId) {
                return APIResponse::error("Community not found");
            }

            if (!$cameraId) {
                return APIResponse::error("Camera not found");
            }

            $camera = Camera::where(['community_id' => $communityId, 'id' => $cameraId])->first();

            if (!$camera) {
                return APIResponse::error("Camera not found");
            }

            $piId = $camera->connected_pi_id ?? 0;

            $pi = ConnectedPie::where(['id' => $piId])->first();

            $macAddress = $pi->mac_address;

            $socket = CameraSocket::where(['mac_address' => $macAddress])->first();

            if (!$socket) {
                return APIResponse::error('Socket not found');
            }

            $socket['scan_log_id'] = $data['scan_log_id'];
            $url = "https://sockets.zuulsystems-security.com/api/v1/guard/guard-camera";
            $apiData = ['data' => $socket];

            $headers = [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ];

            $response = Http::withHeaders($headers)->post($url, ['data' => $socket, 'camera' => $camera->mac_address]);
            if ($response->getBody()) {
                return APIResponse::success([$response], "Camera fetched");

            } else {
                return APIResponse::error("response not found");
            }

            return APIResponse::success([$response], "Camera fetched");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function imageStore(Request $request)
    {
        try {
            $requestData = $request->all();

            $remoteGuard = RemoteGuardSocket::where(['socket_id' => $requestData['socket_id']])->first();

            if (empty($requestData["image"])) {
                return APIResponse::error('Please select license plate imagee');
            }

            $socket_camera_image = $requestData['image'];

            if (strpos($socket_camera_image, 'data:image/jpeg;base64,') !== 0) {
                // Append the prefix to the base64 image string
                $socket_camera_image = 'data:image/jpeg;base64,' . $socket_camera_image;
            }

            if (base64_decode($socket_camera_image, true) == true) {
                return APIResponse::error('Something went wrong. Please try again.');
            }

            $imageType = "";

            if (isset($requestData["image_type"])) {
                if (gettype($requestData["image_type"]) != "integer") {
                    return APIResponse::error("Type must be either 0 or 1");
                }
                switch ($requestData["image_type"]) {
                    // 0 for License Plate
                    case 0:
                    {
                        $imageType = "Driver License";
                        break;
                    }
                    //1 for Driver License
                    case 1:
                    {
                        $imageType = "License Plate";
                        break;

                    }
                }
            } else {
                return APIResponse::error("Type must be select");
            }

            $socket_camera_image = $this->delete_all_between("data:image", "base64,", $socket_camera_image);
            $camera_image = base64_decode($socket_camera_image);
            $cameraImageName = md5(uniqid()) . '.' . 'png';
            $pathAndFileName = "dynamic/lic_images/" . $cameraImageName;


            $s3uploadResp;
            $requestUrl = $request->url;
            //
            if (strpos($requestUrl, '127.0.0.1') !== false) {

                $path = public_path("images/kiosk/{$cameraImageName}");
                file_put_contents("images/kiosk/{$cameraImageName}", $camera_image);
                $s3uploadResp = "images/kiosk/" . $cameraImageName;

            } else {
                $s3uploadResp = S3ImageHelper::storeImageInBucket($pathAndFileName, $camera_image);
            }

            if (!$s3uploadResp) {
                return APIResponse::error('Something went wrong.Please try again.');
            }


            $scan_log_id = $requestData['scan_log_id'];


            if (!$remoteGuard) {
                return APIResponse::error('something went wrong');
            }

            $plateNumm = "";
            if (isset($requestData["plateNum"])) {
                if (sizeof($requestData["plateNum"]) > 0) {
                    $plateNumm = $requestData["plateNum"][0];
                }
            }

            // new
            $imgUrl = $this->getLicenseImageUrlAttribute($s3uploadResp);
            $val1 = explode("?", $imgUrl);
            $cleanedString = str_replace('https://zuul-private-files.s3.us-east-2.amazonaws.com/', '', $val1[0]);

            $scanId = RemoteGuardImage::where('scan_log_id', $scan_log_id)->where('image_type', $imageType)->delete();

            $cmsObj = [
                'remote_guard_id' => $remoteGuard->remote_guard,
                'ImageURL' => $cleanedString,
                'scan_log_id' => $scan_log_id,
                'image_type' => $imageType,
                'url' => $requestUrl,
                'plateNum' => $plateNumm
            ];

            $cms = RemoteGuardImage::create($cmsObj);

            // store the image into scan log as well or store in the users licence image
            // ==============


            // get the user who is the responsible of scan the log of
            $scanlog = Scanlog::where(['id' => $scan_log_id])->first();
            if ($scanlog) {
                // get the user of scan log

                if ($imageType == "Driver License") {
                    $scanlog->addedUser->license_image = $s3uploadResp;
                    $scanlog->addedUser->save();
                    $scanlog->license_image = $s3uploadResp;
                    $scanlog->save();
                }


            }


            // ==============


            if (!$cms) {
                return APIResponse::error('Something went wrong , Image does not stored');
            }


            return APIResponse::success($cmsObj, "Image stored");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage(),
                'exception' => $exception
            ];
        }
    }

    private function delete_all_between(string $beginning, string $end, string $string)
    {
        return S3ImageHelper::delete_all_between($beginning, $end, $string);
    }

    public function getLicenseImageUrlAttribute($license_image)
    {
        return getS3Image($license_image);
    }

    public function getImage(Request $request)
    {
        try {
            $request = $request->all();
            $scanLogId = $request['scan_log_id'];

            $image = CameraSocketImage::where(['scan_log_id' => $scanLogId])->orderBy('id', 'desc')->first();

            if (!$image) {
                return APIResponse::error("image not found");
            }

            return APIResponse::success([$image], 'Image fetched');
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function authRemoteGuard(Request $request)
    {

        try {
            $data = $request->all();
            $zuulKey = $data["zuul_key"];
            $zuulSecretKey = $data["zuul_secret"];


            if (!$zuulKey && !$zuulSecretKey) {
                return APIResponse::error('Key or Secret does not authenticate!');
            }

            $remoteGuard = RemoteGuard::where(['zuul_key' => $zuulKey, "zuul_secret" => $zuulSecretKey])->first();

            if (!$remoteGuard) {
                return APIResponse::error('Remote guard does not authenticate!');
            }


            return APIResponse::success([$remoteGuard], "Camera fetched");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


}
