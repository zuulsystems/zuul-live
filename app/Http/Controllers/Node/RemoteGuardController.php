<?php

namespace App\Http\Controllers\Node;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\QuickPassCollection;
use App\Http\Resources\RemoteGuardCollection;
use App\Jobs\SendNotificationsForValidateQrCode;
use App\Models\Camera;
use App\Models\Community;
use App\Models\ConnectedPie;
use App\Models\RemoteGuard;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use RingCentral\SDK\Http\ApiResponse as HttpApiResponse;

class RemoteGuardController extends Controller
{

    public function ping()
    {
        return APIResponse::success(["pong"], "pong message");
    }

    public function type(Request $request)
    {
        $type = $request->all();
        try {

            $data = "";

            switch ($type['type']) {
                case 1: //for connected pie
                    $data = ConnectedPie::all();
                    break;
                case 2: //for camera
                    $data = Camera::all();
                    break;
                case 3: // for user
                    $data = User::all();
                    break;
                case 4: //for network
                    $data = ['status' => "success"];
                    break;
            }

            if ($data == '') {
                return APIResponse::error("Something went wrong");
            }
            return APIResponse::success($data, "type fetched");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function all()
    {
        try {
            $data = RemoteGuard::all();

            if (!$data) {
                return APIResponse::error("No data found");
            }
            $data = collect($data)->map(function ($item) {
                return [
                    'id' => $item->id,
                    'identifier' => $item->identifier,
                    'type' => $item->type,
                    'meta_id' => $item->meta_id,
                    'community_id' => $item->community_id
                ];
            });
            return APIResponse::success($data, "Remote guard successfully fetched");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function communities()
    {
        try {
            $communities = Community::all();
            return APIResponse::success($communities, 'Communities fetched');
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function addRemoteGuard(Request $request)
    {
        $data = $request->all();
        try {

            $mac_address = $data['mac_address'];
            $connectedPie = ConnectedPie::updateOrCreate(
                ["mac_address" => $mac_address],
                ["name" => $data["name"], "mac_address" => $mac_address, "uid" => $data["uid"]]
            );
            if (!$connectedPie) {
                APIResponse::error("Remote guard did not add");
            }
            $remoteGuard = RemoteGuard::updateOrCreate(['meta_id' => $connectedPie['id'], 'type' => 1], [
                'identifier' => $data['identifier'],
                'meta_id' => $connectedPie['id'],
                'type' => 1,
                'community_id' => $data['community_id']
            ]);

            if (!$remoteGuard) {
                APIResponse::error("Remote guard did not add");
            }
            return APIResponse::success($remoteGuard, "remote guard add succesfully");
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function keysgenerate(Request $request)
    {
        try {

            $id = $request->all();

            $zuulKey = str::random(16);
            $zuulSecretKey = str::random(16);

            $createKeys = RemoteGuard::where(['id' => $id])->first();
            $createKeys->update(['zuul_key' => $zuulKey, 'zuul_secret_key' => $zuulSecretKey]);

            $createKeys = Arr::except($createKeys, ["id", "identifier", "type", "meta_id", "community_id", "created_at", "updated_at", "deleted_at"]);
            if (!$createKeys) {
                return APIResponse::error('Something went wrong');
            }
            $keys = ["zuul_key" => $createKeys["zuul_key"], "zuul_secret_key" => $createKeys["zuul_secret_key"]];
            return APIResponse::success($keys, "keys created successfully");

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        try {
            $remoteGuard = RemoteGuard::where(['id' => $data['id']])->first();

            if (!$remoteGuard) {
                return APIResponse::error('Something went wrong');
            }

            $connectedPie = ConnectedPie::where(['id' => $remoteGuard['meta_id']])->first();

            if (!$connectedPie) {
                return APIResponse::error('Something went wrong');
            }

            $obj = [
                'mac_address' => $data['mac_address'],
                'name' => $data['name'],
                'uid' => $data['uid']
            ];

            $connectedPie->update($obj);

            if (!$connectedPie) {
                return APIResponse::error('Something went wrong');
            }


            $obj = [
                'identifier' => $data['identifier'],
                'community_id' => $data['community_id']
            ];

            $remoteGuard->update($obj);

            if (!$remoteGuard) {
                return APIResponse::error('Something went wrong');
            }

            return APIResponse::success(['remteGuard' => $remoteGuard, 'ConnectedPie' => $connectedPie], 'Update successfully');

        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }

    public function createQuickPass(Request $request)
    {

        $passData = $request->all();
        $passData['phone_number'] = $request->phone_number;
        $validator = Validator::make($passData, [
            'resident_id' => 'required',
            'display_name' => 'required',
            'phone_number' => ['required', new ValidatePhoneNumber],
        ], [
            'resident_id.required' => 'Please select resident',
            'display_name.required' => 'Please enter display name',
            'phone_number.required' => 'Please enter phone number'
        ]);
        if (!empty($request->user_id)) {
            $userId = $request->user_id;
        } else {
            $userId = auth()->id();
        }

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $createdBy = $passData['resident_id'];
        $pass = $this->passService->createQuickPass($passData, $createdBy);

        if ($pass['bool']) {
            //+++++++++++++ Scan Qrcode ++++++++++++++++
            $qrCode = $pass['qrcodes'][0];

            $scanQrCode = $this->passUserService->scanQrCode('guard', $userId, $qrCode, 1, $passData['display_name']);

            if (isset($scanQrCode['bool']) && $scanQrCode['bool'] == false) {
                return APIResponse::error($scanQrCode['message']);
            }
            $scanLog = ScanLog::with(
                'passUser.pass.community',
                'passUser.pass.createdBy:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code,email',
                'passUser.pass.createdBy.house',
                'passUser.user.house',
                'passUser.user:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code',
                'passUser.pass.event',
                'passUser.vehicle',
                'scanner',
                'scanBy'
            )->where('id', $scanQrCode['scanLog']->id)->first();


            if (!empty($scanLog->guard_display_name)) {
                $guestName = $scanLog->guard_display_name;
            } else {
                $guestName = $passData['display_name'];
            }


            $senderFullName = $scanLog->passUser->pass->createdBy->fullName;
            $senderFormattedPhone = $scanLog->passUser->pass->createdBy->FormattedPhone;
            $senderFormattedAddress = $scanLog->passUser->pass->createdBy->formattedAddress;

            if (!empty($scanLog->guard_display_name)) {
                $receiverFullName = $scanLog->guard_display_name;
            } else if (!empty($scanLog->passUser->user->fullName)) {
                $receiverFullName = $scanLog->passUser->user->fullName;
            }

            $receiverFormattedPhone = $scanLog->passUser->user->FormattedPhone;
            $receiverFormattedAddress = $scanLog->passUser->user->formattedAddress;


            $community = $scanLog->passUser->pass->community->name;
            $event = $scanLog->passUser->pass->event->name;
            $start = $scanLog->passUser->pass->pass_start_date;
            $expire = $scanLog->passUser->pass->pass_date;
            $scanAt = $scanLog->created_at;
            $scannedBy = $scanLog->ScanByName;


            $licensePlate = '';
            $color = '';
            $make = '';
            $model = '';
            $year = '';

            if (!empty($scanLog->passUser->vehicle)) {
                $licensePlate = $scanLog->passUser->vehicle->license_plate;
                $color = $scanLog->passUser->vehicle->color;
                $make = $scanLog->passUser->vehicle->make;
                $model = $scanLog->passUser->vehicle->model;
                $year = $scanLog->passUser->vehicle->year;
            }

            $data = array(
                'sender_full_name' => $senderFullName,
                'sender_formatted_phone' => $senderFormattedPhone,
                'sender_formatted_address' => $senderFormattedAddress,
                'recipient_full_name' => $receiverFullName,
                'recipient_formatted_phone' => $receiverFormattedPhone,
                'recipient_formatted_address' => $receiverFormattedAddress,
                'community_name' => $community,
                'event_name' => $event,
                'start' => $start,
                'expires' => $expire,
                'scan_date' => $scanAt,
                'scan_by' => $scannedBy,
                'license_plate' => $licensePlate,
                'color' => $color,
                'make' => $make,
                'model' => $model,
                'year' => $year,
                'scan_log_id' => $scanQrCode['scanLog']->id,
            );


            $staticScanLog = $this->staticScanLogService->create($data);

            $passSentByEmail = $scanLog->passUser->pass->createdBy->email;

            sendGuestArrivalEmailToResident($passSentByEmail, $staticScanLog, true);

            if ($scanQrCode['bool'] || true) {
                //Send scan pass notifications
                $passUser = $this->passUserService->getByQrCode($qrCode)['result'];


                SendNotificationsForValidateQrCode::dispatch($passUser)->delay(Carbon::now()->addSeconds(2));
                return APIResponse::success((new QuickPassCollection($passUser))->setScanLog($scanQrCode['scanLog']), $scanQrCode['message']);

            } else {
                return APIResponse::error($scanQrCode['message']);
            }
            //+++++++++++ End Qrcode ++++++++++++++++++
        }
        return APIResponse::error($pass['message']);
    }

}
