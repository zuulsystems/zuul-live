<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Models\RfidDateLimitWeek;
use App\Repositories\RfidDateLimitRepository;
use App\Services\RfidDateLimitWeekService;
use App\Services\RfidService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class RfidSchduleController extends Controller
{
    use UserCommon;

    public $rfidService;
    public $rfidDateLimitService;
    public $rfidDateLimitWeekService;
    protected $folderLink = 'admin.rfids-schedules.';

    public function __construct(
        RfidService              $rfidService,
        RfidDateLimitRepository  $rfidDateLimitService,
        RfidDateLimitWeekService $rfidDateLimitWeekService
    )
    {
        $this->rfidService = $rfidService;
        $this->rfidDateLimitService = $rfidDateLimitService;
        $this->rfidDateLimitWeekService = $rfidDateLimitWeekService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function index(Request $request)
    {
        $rfid_id = ($request->has('rfid_id') && $request->rfid_id) ? $request->rfid_id : null;
        $data['rfid'] = $this->rfidService->find($rfid_id)['result'];
        if (empty($data['rfid'])) {
            abort(404);
        }
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get rfid schedule list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function rfidScheduleData(Request $request)
    {
        $rfidId = ($request->has('rfid_id') && $request->rfid_id) ? $request->rfid_id : null;
        return datatables()->of($this->rfidDateLimitService->getByRfid($rfidId)['result'])
            ->addColumn('formatted_status', function ($model) {
                return $model->status == '1' ? 'Active' : 'Inactive';
            })
            ->addColumn('schedule_days', function ($model) {
                $days = '';
                if ($model->rfidDateLimitWeeks->isNotEmpty()) {
                    $times = array();
                    foreach ($model->rfidDateLimitWeeks as $rfidDateLimitWeek) {
                        $time = $rfidDateLimitWeek;
                        $time['start_time'] = Carbon::parse($rfidDateLimitWeek['start_time'])->format('h:i');
                        $time['end_time'] = Carbon::parse($rfidDateLimitWeek['end_time'])->format('h:i');
                        array_push($times, $time);
                    }
                    if (!empty($times)) {

                        foreach ($times as $key => $value) {
                            $lblclass = intval($value['status'] == 1) ? 'badge-success' : 'badge-secondary';
                            $str = $value['day'] . " &emsp;" . $value['start_time'] . " - " . $value['end_time'];
                            $days .= "<span class='badge $lblclass'> $str </span>";
                            $days .= "<br>";
                        }
                    }
                }
                return $days;
            })
            ->addColumn('links', function ($model) {
                return '
                <div class="btn-group">
                 <a class="btn btn-primary btn-sm "  href="' . route('admin.rfid.rfids-schedules.edit', [$model->id]) . '"><i class="fa fa-pen" aria-hidden="true"></i> </a>
                 <a class="btn btn-warning btn-sm btn-time-model" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="fa fa-clock" aria-hidden="true"></i> </a>
                  <form action="' . route('admin.rfid.rfids-schedules.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>

                                </form>
                </div>
                ';
            })
            ->filter(function ($instance) use ($request) {

                if (isset($request->schedule_name)) {
                    $instance->where('name', 'like', "%{$request->get('schedule_name')}%");
                }
                if (isset($request->start_date)) {
                    $instance->whereDate('start_date', '>=', "%{$request->get('start_date')}%");
                }
                if (isset($request->end_date)) {
                    $instance->whereDate('end_date', '<=', "%{$request->get('end_date')}%");
                }

            })
            ->rawColumns(['links', 'schedule_days'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function store(Request $request)
    {
        $rfidSchduleData = $request->all();
        $validation = [
            'name' => 'required',
            'start_end' => 'required',
        ];

        $validator = Validator::make($rfidSchduleData, $validation);
        if ($validator->fails()) {
            $validator->validate();
        }
        $startDate = Carbon::parse(trim(explode("-", $rfidSchduleData['start_end'])[0]))->format('Y-m-d');
        $endDate = Carbon::parse(trim(explode("-", $rfidSchduleData['start_end'])[1]))->format('Y-m-d');
        $result = $this->rfidDateLimitService->findBetweenStartEndDates($rfidSchduleData['rfid_id'], $startDate, $endDate);
        if (!empty($result['result'])) {
            return response()->json(['status' => false, 'message' => "Start Date is overlapping {$result["result"]->name} schedule"]);
        }
        $rfid = $this->rfidService->find($rfidSchduleData['rfid_id'])['result'];
        if (empty($rfid)) {
            return response()->json(['status' => false, 'message' => "Rfid Not Found"]);
        }
        $rfidDateLimit = $this->rfidDateLimitService->create([
            'rfid_id' => $rfidSchduleData['rfid_id'],
            'name' => $rfidSchduleData['name'],
            'start_end' => $rfidSchduleData['start_end'],
            'start_date' => $startDate,
            'end_date' => $endDate,
        ]);
        if ($rfidDateLimit['bool']) {
            // user activity logs
            callUserActivityLogs("Add RFID Schedules", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.rfid.rfids-schedules.index', ['rfid_id' => $rfidSchduleData['rfid_id']])];
        } else {
            // user activity logs
            callUserActivityLogs("Add RFID Schedules", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $data['communities'] = getCommunities();
        $rfidDateLimit = $this->rfidDateLimitService->find($id);
        if (!$rfidDateLimit['bool']) {
            abort(500);
        }
        if (!$rfidDateLimit['result']) {
            abort(404);
        }
        $data['rfidDateLimit'] = $rfidDateLimit['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        $rfidSchduleData = $request->all();
        $validation = [
            'name' => 'required',
            'start_end' => 'required',
        ];

        $validator = Validator::make($rfidSchduleData, $validation);
        if ($validator->fails()) {
            $validator->validate();
        }
        $startDate = Carbon::parse(trim(explode("-", $rfidSchduleData['start_end'])[0]))->format('Y-m-d');
        $endDate = Carbon::parse(trim(explode("-", $rfidSchduleData['start_end'])[1]))->format('Y-m-d');
        $result = $this->rfidDateLimitService->findBetweenStartEndDates($rfidSchduleData['rfid_id'], $startDate, $endDate, $id);
        if (!empty($result['result'])) {
            return response()->json(['status' => false, 'message' => "Start Date is overlapping {$result["result"]->name} schedule"]);
        }
        $rfid = $this->rfidService->find($rfidSchduleData['rfid_id'])['result'];
        if (empty($rfid)) {
            return response()->json(['status' => false, 'message' => "Rfid Not Found"]);
        }
        $rfidDateLimit = $this->rfidDateLimitService->update([
            'rfid_id' => $rfidSchduleData['rfid_id'],
            'name' => $rfidSchduleData['name'],
            'start_end' => $rfidSchduleData['start_end'],
            'start_date' => $startDate,
            'end_date' => $endDate,
        ], $id);
        if ($rfidDateLimit['bool']) {
            // user activity logs
            callUserActivityLogs("Edit RFID Schedules", __FUNCTION__, __FILE__, __DIR__, true, "Updated Successfully");

            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.rfid.rfids-schedules.index', ['rfid_id' => $rfidSchduleData['rfid_id']])];
        } else {
            // user activity logs
            callUserActivityLogs("Edit RFID Schedules", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $rfidDateLimit = $this->rfidDateLimitService->find($id)['result'];
        if (empty($rfidDateLimit)) {
            return response()->json(['status' => false, 'message' => 'Something went wrong.Please try again.']);
        }
        $result = $this->rfidDateLimitService->delete($id);
        if ($result['bool']) {
            // user activity logs
            callUserActivityLogs("Delete RFID Tags", __FUNCTION__, __FILE__, __DIR__, true, "Delete Successfully");

            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.rfid.rfids-schedules.index', ['rfid_id' => $rfidDateLimit->rfid_id])];
        } else {
            // user activity logs
            callUserActivityLogs("Delete RFID Tags", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * set rfid date limit week
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws Exception
     */
    public function setRfidScheduleWeek(Request $request)
    {
        $rfidDateLimitId = $request->rfid_date_limit_id;
        $rfidDateLimit = $this->rfidDateLimitService->find($rfidDateLimitId)['result'];
        if (empty($rfidDateLimit)) {
            return response()->json(['status' => false, 'message' => 'Something went wrong. Please try again.']);
        }
        $data = $request->all();
        $days = $data['days'];
        $arr = [];
        if (!empty($days)) {
            foreach ($days as $day) {
                $status = filter_var($day['status'], FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
                $arr[] = $this->rfidDateLimitWeekService->updateOrCreate(
                    ['rfid_date_limit_id' => $day['schedule_id'], 'day' => date("N", strtotime($day["day"]))],
                    [
                        "start_time" => Carbon::parse($day["start_time"]),
                        "end_time" => carbon::parse($day["end_time"]),
                        "status" => $status
                    ]
                );
            }
        }

        // user activity logs
        callUserActivityLogs("RFID Week Schedule", __FUNCTION__, __FILE__, __DIR__, true, "Updated Successfully");

        return response()->json([
            'status' => true,
            'message' => 'Updated Successfully',
            'redirect' => route('admin.rfid.rfids-schedules.index', ['rfid_id' => $rfidDateLimit->rfid_id])
        ]);
    }

    /**
     * get rfid date limit week
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function getRfidScheduleWeek(Request $request)
    {
        $rfidDateLimitId = $request->rfid_date_limit_id;
        $rfidDateLimit = $this->rfidDateLimitService->find($rfidDateLimitId)['result'];
        if (empty($rfidDateLimit)) {
            return response()->json(['status' => false, 'message' => 'Something went wrong.Please try again.']);
        }
        $rfidDateLimitWeeks = $this->rfidDateLimitWeekService->findByrfidDateLimitId($rfidDateLimitId)['result'];
        if ($rfidDateLimitWeeks->isNotEmpty()) {
            $times = array();
            foreach ($rfidDateLimitWeeks as $rfidDateLimitWeek) {
                $time = $rfidDateLimitWeek;
                $time['start_time'] = Carbon::parse($rfidDateLimitWeek['start_time'])->format('h:i');
                $time['end_time'] = Carbon::parse($rfidDateLimitWeek['end_time'])->format('h:i');
                array_push($times, $time);
            }
            $response = ['status' => true, 'schedules' => $times, 'limit' => $rfidDateLimit];

        } else {
            $response = ['status' => true, 'schedules' => [], 'limit' => $rfidDateLimit];
        }
        return response()->json($response);

    }

    /**
     * delete rfid date limit week
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function deleteRfidScheduleWeek(Request $request)
    {
        $rfidDateLimitId = $request->rfid_date_limit_id;
        $rfidDateLimit = $this->rfidDateLimitService->find($rfidDateLimitId)['result'];
        if (empty($rfidDateLimit)) {
            return response()->json(['status' => false, 'message' => 'Something went wrong.Please try again.']);
        }
        $result = $this->rfidDateLimitWeekService->deleteByRfidDateLimitId($rfidDateLimitId);
        if ($result['bool']) {
            // user activity logs
            callUserActivityLogs("Delete RFID Week Schedule", __FUNCTION__, __FILE__, __DIR__, true, "Delete Successfully");

            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.rfid.rfids-schedules.index', ['rfid_id' => $rfidDateLimit->rfid_id])];
        } else {
            // user activity logs
            callUserActivityLogs("Delete RFID Week Schedule", __FUNCTION__, __FILE__, __DIR__, true, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }
}
