<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TrafficTicketService;
use App\Services\TrafficTicketImageService;
use App\Services\CommunityService;
use App\Services\EmailLogService;
use App\Services\TrafficPaymentLogService;
use App\Helpers\Tickets;
use Illuminate\Http\Response;


class VendorTicketController extends Controller
{
    use Tickets;

    private $trafficTicketService, $trafficTicketImageService, $emailLogService, $trafficPaymentLogService;

    public function __construct(TrafficTicketService      $trafficTicketService,
                                TrafficTicketImageService $trafficTicketImageService,
                                CommunityService          $communityService,
                                EmailLogService           $emailLogService,
                                TrafficPaymentLogService  $trafficPaymentLogService)
    {
        $this->trafficTicketService = $trafficTicketService;
        $this->trafficTicketImageService = $trafficTicketImageService;
        $this->communityService = $communityService;
        $this->emailLogService = $emailLogService;
        $this->trafficPaymentLogService = $trafficPaymentLogService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Vendor Ticket Controller
        return view('admin.tickets.index')->with('title', 'Vendor');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function vendorTicketData(Request $request)
    {
        $data = $this->trafficTicketService->data('vendor', getCommunityIdByUser());
        if ($data['bool']) {
            return $this->generateDatatable($request, $data['result']);
        } else {
            return [];
        }
    }
}
