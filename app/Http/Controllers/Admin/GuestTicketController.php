<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TrafficTicketService;
use App\Services\TrafficTicketImageService;
use App\Services\CommunityService;
use App\Services\EmailLogService;
use App\Services\TrafficPaymentLogService;
use App\Helpers\Tickets;
use Illuminate\Http\Response;


class GuestTicketController extends Controller
{

    use Tickets;

    private $trafficTicketService, $trafficTicketImageService, $emailLogService, $trafficPaymentLogService;

    public function __construct(TrafficTicketService      $trafficTicketService,
                                TrafficTicketImageService $trafficTicketImageService,
                                CommunityService          $communityService,
                                EmailLogService           $emailLogService,
                                TrafficPaymentLogService  $trafficPaymentLogService)
    {
        $this->trafficTicketService = $trafficTicketService;
        $this->trafficTicketImageService = $trafficTicketImageService;
        $this->communityService = $communityService;
        $this->emailLogService = $emailLogService;
        $this->trafficPaymentLogService = $trafficPaymentLogService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.tickets.index')->with('title', 'Guest');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function guestTicketData(Request $request)
    {
        $data = $this->trafficTicketService->data('guest', getCommunityIdByUser());
        if ($data['bool']) {
            return $this->generateDatatable($request, $data['result']);
        } else {
            return;
        }
    }

    public function getTicketTemplatesByCommunity($community_id)
    {

        $result = $this->emailTemplateService->getTicketTemplatesByCommunity();

    }

    public function sendEmail(Request $request)
    {

    }
}
