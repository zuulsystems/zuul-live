<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\PassRequestService;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PassRequestController extends Controller
{
    public $passRequestService;
    protected $folderLink = 'admin.pass-requests.';

    public function __construct(PassRequestService $passRequestService)
    {
        $this->passRequestService = $passRequestService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Gate::any(['super-admin', 'community-admin', 'is-zuul-right'])) {
            return view($this->folderLink . 'index');
        }
        return abort(403, 'This action is unauthorized.');
    }

    /**
     * get pass request list
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function passRequestData(Request $request)
    {
        #todo need to fetch sender, requested to license image from aws bucket
        return datatables()->of($this->passRequestService->all(getCommunityIdByUser())['result'])
            ->addColumn('sender_name', function ($model) {
                $license_image = (!empty($model->sendUser) && !empty($model->sendUser->license_image)) ? '<img class="img-fluid print-img" src="' . $model->sendUser->licenseImageUrl . '" style="width: 29px">' : "";
                return (
                !empty($model->sendUser)
                )
                    ? "{$license_image}<br/>{$model->sendUser->fullName}" : "-";
            })
            ->addColumn('requested_user_name', function ($model) {
                $license_image = (!empty($model->requestedUser) && !empty($model->requestedUser->license_image)) ? '<img class="img-fluid print-img" src="' . $model->requestedUser->licenseImageUrl . '" style="width: 29px">' : "";
                return (
                !empty($model->requestedUser)
                )
                    ? "{$license_image}<br/>{$model->requestedUser->fullName}" : "-";
            })
            ->addColumn('requested_user_community_name', function ($model) {
                return (
                    !empty($model->requestedUser) &&
                    !empty($model->requestedUser->community)
                )
                    ? $model->requestedUser->community->name : "-";
            })
            ->addColumn('convert_requested_datetime', function ($model) {
                return (!empty($model->created_at)) ? date('M j, Y g:i A', $model->convertedRequestedAt()) : "-";
            })
            ->addColumn('description', function ($model) {
                return (!empty($model->description)) ? $model->description : "-";
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->sender_name)) {
                    $sender_names = explode(" ", $request->get('sender_name'));
                    $instance->whereHas('sendUser', function ($where) use ($request, $sender_names) {
                        $where->where('first_name', $sender_names);
                        $where->orWhere(function ($query) use ($sender_names) {
                            $query->whereIn('last_name', $sender_names);
                        });
                    });
                }
                if (isset($request->requested_user_name)) {

                    $requested_user_names = explode(" ", $request->get('requested_user_name'));
                    $instance->whereHas('requestedUser', function ($where) use ($request, $requested_user_names) {
                        $where->where('first_name', $requested_user_names);
                        $where->orWhere(function ($query) use ($requested_user_names) {
                            $query->whereIn('last_name', $requested_user_names);
                        });
                    });
                }
                if (isset($request->requested_user_community_name)) {
                    $instance->whereHas('requestedUser.community', function ($where) use ($request) {
                        $where->where('name', 'like', "%{$request->get('requested_user_community_name')}%");
                    });

                }
                if (isset($request->description)) {
                    $instance->where('description', 'like', "%{$request->get('description')}%");
                }
                if (isset($request->convert_requested_datetime)) {
                    $instance->whereDate('created_at', $request->get('convert_requested_datetime'));
                }
            })
            ->rawColumns(['sender_name', 'requested_user_name'])
            ->make(true);
    }
}
