<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Jobs\ImporttResidents;
use App\Models\BannedCommunityUser;
use App\Models\Country;
use App\Models\House;
use App\Models\User;
use App\Rules\ValidateName;
use App\Services\CommunityService;
use App\Services\HouseService;
use App\Services\UserContactService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\Store;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Database;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ResidentsSkippedRecordsExports;
use App\Mail\SendResidentImportFileGeneration;
use App\Models\Contact;
use App\Models\ContactCollection;
use App\Services\ContactService;
use Throwable;

class ApproveListController extends Controller
{
    public $userService;
    public $communityService;
    public $houseService;
    public $userContactService;
    public $database;
    protected $folderLink = 'admin.approve-list.';
    private $User;

    public function __construct(
        UserService        $userService,
        CommunityService   $communityService,
        HouseService       $houseService,
        UserContactService $userContactService
    )
    {
        $this->userService = $userService;
        $this->communityService = $communityService;
        $this->houseService = $houseService;
        $this->userContactService = $userContactService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $formClass = 'ajax-form-super-admin';
        if (auth()->user()->hasRole('sub_admin')) {
            $formClass = 'ajax-form';
        }
        $data['formClass'] = $formClass;
        $data['communities'] = getCommunities();
        $data['countries'] = Country::all();
        return view($this->folderLink . 'create', $data);
    }

    public function createdbyResedentID($id)
    {

        $formClass = 'ajax-form-super-admin';
        if (auth()->user()->hasRole('sub_admin')) {
            $formClass = 'ajax-form';
        }
        $data['formClass'] = $formClass;
        $data['communities'] = getCommunities();
        $data['countries'] = Country::all();
        $data['resedentId'] = $id;
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $contactData = $request->all();


        $isFromPreApproved = true;

        $validation = [
            'contact_name' => ['required', new ValidateName]
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',
        ];
        if ($contactData['phone_number'] == "") {
            $contactData['phone_number'] = '1111111111';
        }
        $contactData['phone_number'] = trim(str_replace("-", "", $contactData['phone_number']));

        $validator = Validator::make($contactData, $validation, $messages);
        $validator->after(function ($validator) use ($contactData, $isFromPreApproved) {


        });

        if ($validator->fails()) {
            $validator->validate();
        }


        $contactData['is_dnc'] = 1;
        $contactData['is_from_pre_approved'] = $isFromPreApproved;

        $userContact = $this->userContactService->createOrUpdateUserContact($contactData, $contactData['resident_id']);
        if ($userContact['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.residential.approve-list.show', [$contactData['resident_id']])];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

        $data['communities'] = getCommunities();
        $data['idFromURL'] = $id;
        $data['residentData'] = $this->userService->find($id)['result'];

        return view($this->folderLink . 'index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

        $data['communities'] = getCommunities();
        $data['countries'] = Country::all();
        $resident = $this->userService->find($id);
        if (!$resident['bool']) {
            abort(500);
        }
        if (!$resident['result']) {
            abort(404);
        }
        $data['resident'] = $resident['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $residentData = $request->all();

        $residentData = Arr::except($request->all(), ['profile_image', 'license_image']);

        $residentData['phone_number'] = trim(str_replace("-", "", $residentData['phone_number']));
        $validation = [
            'first_name' => ['required', new ValidateName],
            'last_name' => ['required', new ValidateName],
            'email' => 'required|email',
            //'email' => 'required|email|unique:users,email,'.$id,
            'phone_number' => 'required|unique:users,phone_number,' . $id,
            'community_id' => 'required',
            'house_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
            'house_id.required' => 'Please select house'
        ];
        $validator = Validator::make($residentData, $validation, $messages);
        $validator->after(function ($validator) use ($residentData) {
            if (isset($residentData['phone_number']) && preg_match_all("/[0-9]/", $residentData['phone_number']) < 10) {
                $validator->errors()->add('phone_number', 'Phone number must be at least 10 digit long');
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }

        if ($request->is_head_of_family == '1') { // head of family
            $residentData['role_id'] = 4;
        } else { // house hold resident
            $residentData['role_id'] = 5;
        }
        //upload profile image
        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_' . time() . '.' . $request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $residentData['profile_image'] = $fileName;
        }
        //upload license image to s3 bucket
        if ($request->hasFile('license_image')) {
            $path = Storage::disk('s3')->put("dynamic/lic_images", $request->file('license_image'));
            $residentData['license_image'] = $path;
        }

        $resident = $this->userService->update($residentData, $id);
        if ($resident['bool']) {
            $user = $this->userService->find($id)['result'];
            //head of family rights
            if ($user->hasRole('family_head')) {
                $user->assignPermission('can_send_passes');
                $user->assignPermission('can_manage_family');
                $user->assignPermission('allow_parental_control');
            }
            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.residential.approve-list.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id, $resident_id)
    {
        ContactCollection::where('user_contact_id', $id)->delete();
        $this->userContactService->delete($id);
        $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.residential.approve-list.show', [$resident_id])];
        return response()->json($response);
    }

    public function residentData(Request $request, $id)
    {
        $contactUsers = collect($this->userContactService->getContactList($id, null, '', false, true))->all();
        $residentsData = datatables()->of($contactUsers)
            ->addColumn('contact_name', function ($model) {
                return $model['contact_name'];
            })
            ->addColumn('formatted_phone_number', function ($model) {
                return $model['formatted_phone_number'] ?? "";
            })
            ->addColumn('dial_code', function ($model) {
                return $model['dial_code'] ?? "";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model['created_at'])) ? $model['created_at']->diffForHumans() : "";
            })
            ->addColumn('links', function ($model) {
                $links = '<div class="dropdown">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                        <div class="dropdown-menu">';

                if (auth()->user()->hasRole('guard_admin')) {

                }

                if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('sub_admin')) {


                }

                $links .= "<a class='dropdown-item' onclick='getUserInfo(" . json_encode($model) . ")' href='#'><i class='fa fa-paper-plane' aria-hidden='true'></i> Send Quick Pass </a>";
                $links .= '<form action="' . route('admin.residential.delete-approve-by-guard', [$model['id'], $model['created_by']]) . '" method="POST" class="ajax-form">
                                ' . csrf_field() . '
                                <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                            </form>';

                $links .= '</div>
                </div>';
                return $links;
            })->rawColumns(['links'])
            ->toJson();

        return $residentsData;
    }
}
