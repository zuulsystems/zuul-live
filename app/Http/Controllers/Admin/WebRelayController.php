<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\WebRelay;

use App\Models\WebRelayTracking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use PhpOffice\PhpSpreadsheet\Calculation\Web;

class WebRelayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('admin.web-relays.index');
    }

    /**
     * get web relays list
     */
    public function webRelayData(Request $request)
    {

        $webRelays = new WebRelay;
        auth()->user()->role_id == 2 ? $webRelays = $webRelays->where('community_id', auth()->user()->community_id) : "";
        $webRelays = $webRelays->get();

        return datatables()->of($webRelays)
            ->addColumn('community', function ($model) {
                return !empty($model->community) ? $model->community->name : "";
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="' . route('admin.settings.web-relays.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                                <form action="' . route('admin.settings.web-relays.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $webRelayData = $request->all();
        $validation = [
            'mac_address' => 'required',
            'ip_address' => 'required',
            'web_hook' => 'required',
            'community_id' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'ip_address.required' => 'Please enter ip address',
            'web_hook.required' => 'Please enter web hook',
            'community_id.required' => 'Please select community'
        ];
        $validator = Validator::make($webRelayData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $webRelay = WebRelay::create($webRelayData);

        if ($webRelay) {
            $response = ['status' => true, 'message' => 'Added successfully', 'redirect' => route('admin.settings.web-relays.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong'];
        }

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $data['communities'] = getCommunities();
        return view('admin.web-relays.create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $data['webRelay'] = WebRelay::find($id);
        if (empty($data['webRelay'])) {
            abort(404);
        }
        $data['communities'] = getCommunities();
        return view('admin.web-relays.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $webRelayData = $request->all();
        $validation = [
            'mac_address' => 'required',
            'ip_address' => 'required',
            'web_hook' => 'required',
            'community_id' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'ip_address.required' => 'Please enter ip address',
            'web_hook.required' => 'Please enter web hook',
            'community_id.required' => 'Please select community'
        ];
        $validator = Validator::make($webRelayData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $webRelay = WebRelay::find($id);
        $webRelay->mac_address = $request->mac_address;
        $webRelay->ip_address = $request->ip_address;
        $webRelay->web_hook = $request->web_hook;
        $webRelay->community_id = $request->community_id;
        $webRelay->save();

        if ($webRelay) {
            $response = ['status' => true, 'message' => 'Updated successfully', 'redirect' => route('admin.settings.web-relays.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong'];
        }

        return response()->json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $webRelayTracking = WebRelayTracking::where('web_relay_id', $id);
        $webRelayTracking->delete();

        $webRelay = WebRelay::find($id);
        $webRelay->delete();
        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.settings.web-relays.index')]);
    }
}
