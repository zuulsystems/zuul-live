<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ImportAnnouncements;
use App\Jobs\ImportAuthLogs;
use App\Jobs\ImportCheckGuardAdmin;
use App\Jobs\ImportCommunities;
use App\Jobs\ImportContactCollections;
use App\Jobs\ImportContactGroups;
use App\Jobs\ImportContacts;
use App\Jobs\ImportDashboardSettings;
use App\Jobs\ImportEmailTemplates;
use App\Jobs\ImportDeviceTypes;
use App\Jobs\ImportEvents;
use App\Jobs\ImportHouses;
use App\Jobs\ImportParentalControls;
use App\Jobs\ImportNotifications;
use App\Jobs\ImportParentalControlNotifications;
use App\Jobs\ImportPasses;
use App\Jobs\ImportPassRequest;
use App\Jobs\ImportPassUsers;
use App\Jobs\ImportPassVehicles;
use App\Jobs\ImportRoleUser;
use App\Jobs\ImportScanlogs;
use App\Jobs\ImportScanners;
use App\Jobs\ImportResidents;
use App\Jobs\ImportSmsLogs;
use App\Jobs\ImportUserContacts;
use App\Jobs\ImportUserProfile;
use App\Jobs\ImportVehicles;
use App\Jobs\ImportZuulNotifications;
use App\Jobs\ImportTrafficLocations;
use App\Jobs\ImportTrafficTickets;
use App\Jobs\ImportTrafficTicketImages;
use App\Jobs\ImportTrafficEmailTemplates;
use App\Jobs\ImportTrafficTicketPaymentLogs;
use App\Jobs\ImportTrafficTicketEmailLogs;
use App\Jobs\ImportVendors;
use App\Jobs\ImportVendorVehicles;
use App\Models\Community;
use App\Models\Import;
use App\Services\CommunityService;
use App\Services\HouseService;
use Exception;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class AdminImportController extends Controller
{
    public $houseService;
    public $communityService;
    protected $folderLink = 'admin.import.';

    public function __construct(
        HouseService     $houseService,
        CommunityService $communityService
    )
    {
        $this->houseService = $houseService;
        $this->communityService = $communityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data["communityId"] = $request->communityId ?? '';
        $data['data'] = Import::all();
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get houses list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function importData(Request $request)
    {

        return datatables()->of(Import::query())
            ->addColumn('import_date', function ($model) {
                return date('d M, Y', strtotime($model->import_date));
            })
            ->addColumn('links', function ($model) {
                return ' <button class="btn btn-primary btn-sm start_import" data-id ="' . $model->id . '">Import</button>
                    <form action="' . route('admin.import.destroy', [$model->id]) . '" class="ajax-form" method="post">
                         <input type="hidden" name="_method" value="DELETE" />
                        <button type="submit" class="btn btn-danger btn-sm" data-id ="' . $model->id . '">Delete </button>
                        </form>
                ';
            })
            ->filter(function ($instance) use ($request) {

                if (isset($request->import_name)) {
                    $instance->where('module_name', 'like', "%{$request->get('module_name')}%");
                }
                if (isset($request->file_name)) {
                    $instance->where('file_name', 'like', "%{$request->get('file_name')}%");
                }
            })
            ->rawColumns(['links', 'residents_count'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function store(Request $request)
    {
        $original_name = '';
        $module_name = '';
        if ($request->hasFile('table_csv')) {
            $original_name = $request->file('table_csv')->getClientOriginalName();
            $module_name = $request->module_name;
            $count = Import::where('file_name', $original_name)->count();
            if ($count > 0) {
                return response()->json(['status' => false, 'message' => 'File is already exists']);
            }
        }

        $validator = Validator::make($request->all(), [
            'table_csv' => 'required',
            'module_name' => 'required',
        ], ['table_csv.required' => '<strong>Error!</strong> Select a file to import.',
            'module_name.required' => 'Please select table name',
        ]);


        if ($validator->fails()) {
            $validator->validate();
        }

        $filename = $original_name;
        Storage::disk('local')->putFileAs('/csv_import/', $request->file('table_csv'), $filename);

        $import = Import::create([
            'import_date' => now(),
            'module_name' => $module_name,
            'file_name' => $filename,
            'status' => 1,
        ]);
        if ($import) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.import.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $csv = Import::find($id);
        $file = storage_path("app/csv_import/{$csv->file_name}");
        $data = $this->csvToArrayImport($file, 20000, 0);
        $data = $this->utf8Converter($data);
        switch ($csv->module_name) {
            case 'communities':
                dispatch(new ImportCommunities($data));
                break;
            case 'houses':
                dispatch(new ImportHouses($data));
                break;
            case 'users':
                dispatch(new ImportResidents($data));
                break;
            case 'users_profile':
                dispatch(new ImportUserProfile($data));
                break;
            case 'role_user':
                dispatch(new ImportRoleUser($data));
                break;
            case 'update_guard_admin':
                dispatch(new ImportCheckGuardAdmin($data));
                break;
            case 'scanners':
                dispatch(new ImportScanners($data));
                break;
            case 'announcements':
                dispatch(new ImportAnnouncements($data));
                break;
            case 'auth_logs':
                dispatch(new ImportAuthLogs($data));
                break;
            case 'contact_collections':
                dispatch(new ImportContactCollections($data));
                break;
            case 'contacts':
                dispatch(new ImportContacts($data));
                break;
            case 'contact_groups':
                dispatch(new ImportContactGroups($data));
                break;
            case 'user_contacts':
                dispatch(new ImportUserContacts($data));
                break;
            case 'vehicles':
                dispatch(new ImportVehicles($data));
                break;
            case 'passes':
                dispatch(new ImportPasses($data));
                break;
            case 'pass_users':
                dispatch(new ImportPassUsers($data));
                break;
            case 'pass_vehicles':
                dispatch(new ImportPassVehicles($data));
                break;
            case 'pass_requests':
                dispatch(new ImportPassRequest($data));
                break;
            case 'events':
                dispatch(new ImportEvents($data));
                break;
            case 'dashboard_settings':
                dispatch(new ImportDashboardSettings($data));
                break;
            case 'scan_logs':
                dispatch(new ImportScanlogs($data));
                break;
            case 'parental_controls':
                dispatch(new ImportParentalControls($data));
                break;
            case 'email_templates':
                dispatch(new ImportEmailTemplates($data));
                break;
            case 'device_types':
                dispatch(new ImportDeviceTypes($data));
                break;
            case 'zuul_notifications':
                dispatch(new ImportZuulNotifications($data));
                break;
            case 'parental_control_notifications':
                dispatch(new ImportParentalControlNotifications($data));
                break;
            case 'sms_logs':
                dispatch(new ImportSmsLogs($data));
                break;
            case 'traffic_locations':
                dispatch(new ImportTrafficLocations($data));
                break;
            case 'traffic_tickets':
                dispatch(new ImportTrafficTickets($data));
                break;
            case 'traffic_ticket_images':
                dispatch(new ImportTrafficTicketImages($data));
                break;
            case 'traffic_ticket_templates':
                dispatch(new ImportTrafficEmailTemplates($data));
                break;
            case 'traffic_ticket_payment_logs':
                dispatch(new ImportTrafficTicketPaymentLogs($data));
                break;
            case 'traffic_ticket_email_logs':
                dispatch(new ImportTrafficTicketEmailLogs($data));
                break;
            case 'vendors':
                dispatch(new ImportVendors($data));
                break;
            case 'vendor_vehicles':
                dispatch(new ImportVendorVehicles($data));
                break;
            default:
                return response()->json(['status' => false, 'message' => 'No job found'], 422);
        }

        return response()->json(['status' => true, 'message' => 'Import Job is processing'], 200);
    }

    /**
     * @param string $filename
     * @param int $rows
     * @param int $skip
     * @param string $delimiter
     * @return array|bool
     */
    public function csvToArrayImport($filename = '', $rows = 100, $skip = 0, $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        $i = 0;
        if (($handle = fopen($filename, 'r')) !== false) {

            while (($row = fgetcsv($handle, 100000, $delimiter)) !== false) {
                if ($i > 0 && $skip > 0) {
                    $skip--;
                    continue;
                }

                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);

                if ($i == $rows)
                    break;

                $i++;
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * utf-8 Converter
     * @param $array
     * @return mixed
     */
    public function utf8Converter($array)
    {
        array_walk_recursive($array, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    public function destroy($id)
    {
        $import = Import::find($id);
        if ($import) {
            $isExists = File::exists(storage_path("app/csv_import/{$import->file_name}"));
            if ($isExists) {
                $file = storage_path("app/csv_import/{$import->file_name}");
                unlink($file);
            }
            $import->delete();
        }
        return response()->json(['status' => true, 'message' => 'Deleted Successfully', 'redirect' => route('admin.import.index')]);
    }

}
