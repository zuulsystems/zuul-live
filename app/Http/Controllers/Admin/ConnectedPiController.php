<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ConnectedPie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ConnectedPiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('admin.connected-pi.index');
    }

    public function connectedPiData()
    {
        $user = auth()->user();
        $connectedPis = ConnectedPie::query()->with('community')->where('community_id', $user->community_id);

        return datatables()->of($connectedPis)
            ->addColumn('community_name', function ($model) {
                return !empty($model->community) ? $model->community->name : "";
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="' . route('admin.connected-pi.edit', $model->id) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                            <form action="' . route('admin.connected-pi.destroy', $model->id) . '" method="POST" class="ajax-form">
                ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'mac_address' => 'required',
            'name' => 'required',
            'dynamic_url' => 'required',
            'uid' => 'required',
            'community_id' => 'required',
            'camera_email' => 'required',
            'camera_password' => 'required',
        ]);

        ConnectedPie::create($request->all());

        return response()->json(['status' => true, 'message' => 'Connected pi saved successfully', 'redirect' => route('admin.connected-pi.index')]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $data['communities'] = getCommunities();
        return view('admin.connected-pi.create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $data["connectedPi"] = ConnectedPie::find($id);
        $data['communities'] = getCommunities();
        return view('admin.connected-pi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'mac_address' => 'required',
            'name' => 'required',
            'dynamic_url' => 'required',
            'uid' => 'required',
            'community_id' => 'required',
            'camera_email' => 'required|email',
            'camera_password' => 'required',
        ]);

        ConnectedPie::find($id)->update($request->all());

        return response()->json(['status' => true, 'message' => 'Connected pie updated successfully', 'redirect' => route('admin.connected-pi.index')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        ConnectedPie::find($id)->delete();
        return response()->json(['status' => true, 'message' => 'Connected pi deleted successfully', 'redirect' => route('admin.connected-pi.index')]);
    }
}
