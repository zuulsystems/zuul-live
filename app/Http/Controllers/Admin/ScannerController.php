<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DeviceType;
use App\Services\CommunityService;
use App\Services\ScannerService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\Camera;
use Illuminate\Validation\ValidationException;

class ScannerController extends Controller
{
    public $scannerService;
    public $communityService;
    protected $folderLink = 'admin.scanners.';

    public function __construct(
        ScannerService   $scannerService,
        CommunityService $communityService
    )
    {
        $this->scannerService = $scannerService;
        $this->communityService = $communityService;
        $this->middleware('checkGuardAdmin', ['except' => ['index', 'scannerData']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get scanners list
     * @return mixed
     * @throws Exception
     */
    public function scannerData(Request $request)
    {
        $scannerData = datatables()->of($this->scannerService->all(getCommunityIdByUser())['result'])
            ->addColumn('camera_name', function ($model) {
                return $model->camera->name ?? "-";
            })
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "-";
            })
            ->addColumn('last_event', function ($model) {
                return (!empty($model->last_event)) ? $model->last_event : "-";
            })
            ->addColumn('rfid_multiplier', function ($model) {
                return (!empty($model->rfid_multiplier)) ? $model->rfid_multiplier : "-";
            })
            ->addColumn('last_event_at', function ($model) {
                return (!empty($model->last_event_at)) ? $model->last_event_at : "-";
            })
            ->addColumn('online_at', function ($model) {
                return (!empty($model->online_at)) ? $model->online_at : "-";
            })
            ->addColumn('formatted_mac_address_and_secret_key', function ($model) {
                $html = "{$model->mac_address}<br/><span style='font-size: 7px;'>{$model->secret_key}</span>";
                return $html;
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('edit_link', function ($model) {
                if (!Gate::allows('is-guard-admin')) {
                    return '<a href="' . route('admin.settings.scanners.edit', [$model->id]) . '">Edit </a>';
                }
                return '';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->name)) {
                    $instance->where('name', 'like', "%{$request->get('name')}%");
                }
                if (isset($request->mac_address)) {
                    $instance->where('mac_address', 'like', "%{$request->get('mac_address')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($where) use ($request) {
                        $where->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
            })
            ->rawColumns(['edit_link', 'formatted_mac_address_and_secret_key'])
            ->toJson();

        return $scannerData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'mac_address' => 'required|unique:scanners',
            'community_id' => 'required',
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $scannerData = $request->all();
        $scannerData['secret_key'] = \Str::random(10);
        $community = $this->scannerService->create($scannerData);
        if ($community['bool']) {

            // user activity logs
            callUserActivityLogs("Add Scanner", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.settings.scanners.index')];
        } else {

            // user activity logs
            callUserActivityLogs("Add Scanner", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $communities = $this->communityService->all(getCommunityIdByUser());
        $cameras = Camera::query();

        if (!empty(auth()->user()->kioskPermission)) {
            $communityIds = explode(",", auth()->user()->kioskPermission->communities);
            $cameras = $cameras->whereIn('community_id', $communityIds);
        }

        $cameras = $cameras->get();

        if (!$communities['bool']) {
            abort(500);
        }
        $data['communities'] = $communities['result'];
        $data['cameras'] = $cameras;
        $data['device_types'] = DeviceType::all();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $communities = $this->communityService->all(getCommunityIdByUser());
        $cameras = Camera::all();
        if (!$communities['bool']) {
            abort(500);
        }
        $data['communities'] = $communities['result'];
        $data['cameras'] = $cameras;
        $data['device_types'] = DeviceType::all();
        $scanner = $this->scannerService->find($id);

        if (!empty($scanner['result']->camera)) {
            $data['cameras'] = Camera::where('community_id', $scanner['result']->camera->community_id)->get();
        }


        if (!$scanner['bool']) {
            abort(500);
        }
        if (!$scanner['result']) {
            abort(404);
        }
        $data['scanner'] = $scanner['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'mac_address' => 'required|unique:scanners,mac_address,' . $id,
            'community_id' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $scannerData = $request->all();
        $scannerData['secret_key'] = \Str::random(10);

        $scannerData['enable'] = isset($request->enable) ? '1' : '0';

        $scanner = $this->scannerService->update($scannerData, $id);

        if ($scanner['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Scanner", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.settings.scanners.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Scanner", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
