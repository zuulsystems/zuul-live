<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AuthLogService;
use Illuminate\Http\Request;

class AuthLogController extends Controller
{
    public $authLogService;
    protected $folderLink = 'admin.auth-logs.';

    public function __construct(
        AuthLogService $authLogService
    )
    {
        $this->authLogService = $authLogService;
    }

    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get authentication logs
     */
    public function authLogData()
    {
        return datatables()->of($this->authLogService->all()['result'])
            ->addColumn('full_name', function ($model) {
                return ($model->user()->exists()) ? $model->user->fullName : "";
            })
            ->addColumn('role_name', function ($model) {
                return ($model->user()->exists()) ? $model->user->role->name ?? "" : "";
            })
            ->addColumn('logged_in_at', function ($model) {
                return (!empty($model->login_at)) ? date('m-d-Y h:i:s A', strtotime($model->login_at)) : "";
            })
            ->filterColumn('full_name', function ($query, $keyword) {
                $names = explode(" ", $keyword);
                $query->whereHas('user', function ($where) use ($names) {
                    $where->where('first_name', $names);
                    $where->orWhere(function ($query) use ($names) {
                        $query->whereIn('last_name', $names);
                    });
                });
            })
            ->toJson();
    }
}
