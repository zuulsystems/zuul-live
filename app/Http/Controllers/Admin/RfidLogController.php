<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\Scanner;
use App\Models\StaticRfidLog;
use App\Models\Rfid;
use App\Models\RfidLog;
use App\Services\CommunityService;
use App\Services\HouseService;
use App\Services\RfidLogService;
use App\Services\RfidService;
use App\Services\UserService;
use App\Services\VehicleService;
use App\Services\StaticRfidLogService;
use App\Services\ScannerService;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Riverline\MultiPartParser\Converters\Commun;
use App\Models\User;

class RfidLogController extends Controller
{
    use UserCommon;

    public $rfidLogService;
    public $staticRfidLogService;
    public $scannerService;
    public $communityService;
    protected $folderLink = 'admin.rfids-logs.';

    public function __construct(
        RfidLogService       $rfidLogService,
        StaticRfidLogService $staticRfidLogService,
        ScannerService       $scannerService,
        CommunityService     $communityService
    )
    {
        $this->rfidLogService = $rfidLogService;
        $this->staticRfidLogService = $staticRfidLogService;
        $this->scannerService = $scannerService;
        $this->communityService = $communityService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data["houseId"] = $request->houseId ?? "";
        $data["userId"] = $request->userId ?? "";
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get houses list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function rfidLogData(Request $request)
    {
        $communityId = null;

        if ($request->has('communityId') && !empty($request->communityId)) {
            $communityId = $request->communityId;
        }


        if (auth()->user()->kioskPermission) {
            $communityId = auth()->user()->kioskPermission->communities;

        }

        $houseId = null;
        if ($request->has('houseId') && !empty($request->houseId)) {
            $houseId = $request->houseId;
        }
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }


        $scanner = "";
        if (auth()->user()->role_id == 12) {
            // get name of scanner
            $scanner = Scanner::where('community_id', auth()->user()->kioskPermission->communities)->whereNull('deleted_at')->first();
        } else {
            if (auth()->user()->role_id != 1) {
                // get name of scanner
                $scanner = Scanner::where('community_id', auth()->user()->community_id)->whereNull('deleted_at')->first();
            } else {

                $user = User::where(['id' => $userId])->first();
                if ($user) {
                    $scanner = Scanner::where('community_id', $user->community_id)->whereNull('deleted_at')->first();

                } else {
                    $scanner = Scanner::whereNull('deleted_at')->pluck('name')->toArray();
                }
            }
        }

        $staticRfidLogs = StaticRfidLog::query();


        if ($scanner) {
            if (auth()->user()->role_id != 1) {
                $staticRfidLogs = $staticRfidLogs->where('scanner', $scanner->name);
            }

        } else if (!empty(auth()->user()->kioskPermission)) {
            $communityIds = explode(",", auth()->user()->kioskPermission->communities);
            $scanners = Scanner::select('name')->whereIn('community_id', $communityIds)->get();
            $staticRfidLogs = $staticRfidLogs->whereIn('scanner', $scanners);
        } else {
            $staticRfidLogs = $staticRfidLogs->whereIn('scanner', $scanner);
        }



        if ($houseId != null) {
            $rfidColumn = 'house_id';
            $rfidValue = $houseId;
        } elseif ($userId != null) {
            $rfidColumn = 'user_id';
            $rfidValue = $userId;
        }

        if (isset($rfidColumn) && isset($rfidValue)) {
            $rfid = Rfid::where($rfidColumn, $rfidValue)->pluck('id')->toArray();
            $rfidLog = RfidLog::whereIn('rfid_id', $rfid)->pluck('id')->toArray();

            $staticRfidLogs = $staticRfidLogs->whereIn('rfid_log_id', $rfidLog);

        }

        return datatables()->of($staticRfidLogs)
            ->addColumn('formatted_scan_date', function ($model) {
                return (!empty($model->scan_at)) ? date('m/d/Y h:i a', strtotime($model->scan_at)) : "";
            })
            ->addColumn('formatted_status', function ($model) {
                return $model->is_success == '1' ? '<div class="badge badge-success">Success</div>' : '<div class="badge badge-danger">ERROR</div>';
            })
            ->addColumn('resident_name', function ($model) {
                return (!empty($model->resident_name)) ? $model->resident_name : "-";
            })
            ->addColumn('vehicle_info', function ($model) {
                return (!empty($model->vehicle_info)) ? $model->vehicle_info : "-";
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->rfids_tag_num_static)) {

                    $instance->where('rfid', 'like', '%' . $request->get('rfids_tag_num_static') . '%');

                }

                if (isset($request->resident_name)) {

                    $instance->where('resident_name', 'like', '%' . $request->get('resident_name') . '%');

                }

                if (isset($request->scanner_name)) {

                    $instance->where('scanner', 'like', '%' . $request->get('scanner_name') . '%');

                }

                if (isset($request->rfid_scan_date)) {

                    $date = explode(' - ', $request->rfid_scan_date); // Split the date range using ' - ' separator
                    $start_date = trim($date[0]); // Remove any leading/trailing spaces
                    $end_date = trim($date[1]); // Remove any leading/trailing spaces


                    $start_date = DateTime::createFromFormat('m-d-Y', $start_date);
                    $start_date = $start_date->format('Y-m-d');

                    $end_date = DateTime::createFromFormat('m-d-Y', $end_date);
                    $end_date = $end_date->format('Y-m-d');


                    if ($start_date != $end_date) {

                        $end_date = date('Y-m-d', strtotime('+1 day', strtotime($end_date)));
                        $instance->whereBetween('scan_at', [$start_date, $end_date]);
                    } else {
                        $instance->whereDate('scan_at', $start_date);
                    }
                }

                if (isset($request->is_success) && !empty($request->is_success)) {
                    $instance->where('is_success', $request->get('is_success'));
                } else if (isset($request->is_success) && $request->is_success == '0') {
                    $instance->where('is_success', $request->get('is_success'));
                }


            })
            ->rawColumns(['formatted_status'])
            ->make(true);

    }

    /*
     *  Save rfid logs table record to static_rfid_logs
     *
     */
    public function dumpRfidLogsToStaticRfidLogsTable()
    {
        $rfidLogs = $this->rfidLogService->all()['result']->get();


        foreach ($rfidLogs as $rfidLog) {
            $scannerName = '';
            if (!empty($rfidLog->scanner_id)) {
                $scanner = $this->scannerService->find($rfidLog->scanner_id);
                $scannerName = $scanner['result']->name;
            }
            $communityName = '';
            if (!empty($rfidLog->community_id)) {
                $community = $this->communityService->find($rfidLog->community_id);
                $communityName = $community['result']->name;
            }

            $staticRfidLog = $this->staticRfidLogService->StaticRfidLogByRfidLogId($rfidLog->id);

            $scanAt = (!empty($rfidLog->scan_at)) ? date('Y-m-d h:i a', strtotime($rfidLog->scan_at)) : "";

            if (empty($staticRfidLog)) {
                $data = array(
                    'rfid' => $rfidLog->rfid->rfid_tag_num ?? "",
                    'scanner' => $scannerName,
                    'resident_name' => !empty($rfidLog->rfid) && !empty($rfidLog->rfid->user) ? $rfidLog->rfid->user->first_name . " " . $rfidLog->rfid->user->last_name : "",
                    'community_name' => $communityName,
                    'type' => 'rfid',
                    'is_success' => 1,
                    'log_text' => 'RFID Scanned Successfully',
                    'scan_at' => $scanAt,
                    'vehicle_info' => !empty($rfidLog->rfid) && !empty($rfidLog->rfid->vehicle) ? $rfidLog->rfid->vehicle->vehicleInfo : "",
                    'rfid_log_id' => $rfidLog->id,
                );

                $this->staticRfidLogService->create($data);
            }
        }

    }


}
