<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\GuardAppointLocation;
use App\Rules\ValidateName;
use App\Services\CommunityService;
use App\Services\GuardAppointLocationService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class GuardAppointLocationController extends Controller
{
    public $userService;
    public $communityService;
    public $guardAppointLocationService;
    protected $folderLink = 'admin.users.security-guards.appoint-locations.';

    public function __construct(
        UserService                 $userService,
        CommunityService            $communityService,
        GuardAppointLocationService $guardAppointLocationService
    )
    {
        $this->userService = $userService;
        $this->communityService = $communityService;
        $this->guardAppointLocationService = $guardAppointLocationService;
        $this->middleware('checkGuardAdmin', ['except' => ['index', 'guardData']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $userId = $request->userId ?? "";
        $this->checkUserPermission();
        $guard = $this->userService->find($userId)['result'];
        if (empty($guard)) {
            abort(404);
        }
        $data["userId"] = $userId;
        return view($this->folderLink . 'index', $data);
    }

    /**
     * role => super admin, community rights => is-zuul-right
     * check user roles
     */
    function checkUserPermission()
    {
        if (!Gate::any(['super-admin', 'community-admin', 'is-zuul-right', 'is-guard-admin'])) {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * get guards data
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function guardAppointLocationData(Request $request)
    {
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        $guardData = datatables()->of($this->guardAppointLocationService->all()['result'])
            ->addColumn('links', function ($model) {
                if (!Gate::allows('is-guard-admin')) {
                    $dropDown = '<div class="dropdown">
                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Action
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="' . route('admin.user.guard-appoint.appoint-locations.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>';

                    if (auth()->user()->hasRole('super_admin')) {
                        $dropDown .= '<form action="' . route('admin.user.guard-appoint.appoint-locations.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    <input type="hidden" name="_method" value="DELETE" />
                                    ' . csrf_field() . '
                                    <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>';
                    }
                    $dropDown .= '
                      </div>
                    </div>';

                    return $dropDown;
                }
                return '';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->lat)) {
                    $instance->where('lat', 'like', "%{$request->get('lat')}%");
                }
                if (isset($request->long)) {
                    $instance->where('long', 'like', "%{$request->get('long')}%");
                }
            })
            ->rawColumns(['links'])
            ->toJson();

        return $guardData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function store(Request $request)
    {
        $guardData = $request->all();
        $validation = [
            'guard_id' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ];
        $messages = [
            'lat.required' => 'Please enter Latitude',
            'long.required' => 'Please enter Longitude',
        ];
        $validator = Validator::make($guardData, $validation, $messages);

        if ($validator->fails()) {
            $validator->validate();
        }
        $guardAppointLocationData = [
            'user_id' => $request->guard_id,
            'lat' => $request->lat,
            'long' => $request->long,
        ];
        $guardAppointLocation = $this->guardAppointLocationService->create($guardAppointLocationData);
        if ($guardAppointLocation['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.user.guard-appoint.appoint-locations.index', ['userId' => $request->guard_id])];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->checkUserPermission();
        $userId = $request->userId ?? "";
        $guard = $this->userService->find($userId)['result'];
        if (empty($guard)) {
            abort(404);
        }
        $data['userId'] = $userId;
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $this->checkUserPermission();
        $guardAppointLocation = $this->guardAppointLocationService->find($id)['result'];
        if (empty($guardAppointLocation)) abort(404);
        $data['guardAppointLocation'] = $guardAppointLocation;
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        $guardData = $request->all();
        $validation = [
            'guard_id' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ];
        $messages = [
            'lat.required' => 'Please enter Latitude',
            'long.required' => 'Please enter Longitude',
        ];
        $validator = Validator::make($guardData, $validation, $messages);

        if ($validator->fails()) {
            $validator->validate();
        }
        $guardAppointLocationData = [
            'user_id' => $request->guard_id,
            'lat' => $request->lat,
            'long' => $request->long,
        ];
        $guardAppointLocation = $this->guardAppointLocationService->update($guardAppointLocationData, $id);
        if ($guardAppointLocation['bool']) {
            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.user.guard-appoint.appoint-locations.index', ['userId' => $request->guard_id])];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $deleteGuardAppointLocation = $this->guardAppointLocationService->delete($id);
        if ($deleteGuardAppointLocation['bool']) {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => url()->previous()];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }


}
