<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\ScanLogService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class GuestLogController extends Controller
{
    protected $folderLink = 'admin.guest-logs.';
    public $scanLogService;

    public function __construct(ScanLogService $scanLogService)
    {
        $this->scanLogService = $scanLogService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get guest logs list
     * @return mixed
     * @throws Exception
     */
    public function guestLogData(Request $request)
    {
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }

        $houseId = null;
        if ($request->has('houseId') && !empty($request->houseId)) {
            $houseId = $request->houseId;
        }
        #todo need to fetch license image from aws bucket
        $result = $this->scanLogService->all(getCommunityIdByUser(), $userId, $houseId)['result'];

        return datatables()->of($result)
            ->addColumn('image', function ($model) {
                return (
                !empty($model->LicenseImageUrl)
                )
                    ? '<img class="img-fluid print-img" src="' . $model->LicenseImageUrl . '" style="width: 29px"> ' : "-";
            })
            ->addColumn('license_plate_image_url', function ($model) {
                if (Str::contains($model->license_plate_image, "https://firebasestorage.googleapis.com") || Str::contains($model->license_plate_image, "data:image/jpeg;base64,")) {
                    return (!empty($model->license_plate_image)) ? '<img class="img-fluid print-img" src="' . $model->license_plate_image . '" style="width: 29px"> ' : "-";
                }
                return (!empty($model->license_plate_image)) ? '<img class="img-fluid print-img" src="' . getS3Image($model->license_plate_image) . '" style="width: 29px"> ' : "-";
            })
            ->addColumn('sender_name', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->createdBy)
                )
                    ? $model->passUser->pass->createdBy->fullName : "";
            })
            ->addColumn('sender_phone', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->createdBy)
                )
                    ? $model->passUser->pass->createdBy->formatted_phone_by_dial_code : "";
            })
            ->addColumn('guard_display_name', function ($model) {
                return (
                !empty($model->guard_display_name)
                )
                    ? $model->guard_display_name : "";
            })
            ->addColumn('sender_full_address', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->createdBy) &&
                    !empty($model->passUser->pass->createdBy->house)
                )
                    ? $model->passUser->pass->createdBy->formattedAddress : "";
            })
            ->addColumn('receiver_name', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->user)
                )
                    ? $model->passUser->user->fullName : "";
            })
            ->addColumn('receiver_phone', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->user)
                )
                    ? $model->passUser->user->formatted_phone_by_dial_code : "";
            })
            ->addColumn('receiver_full_address', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->user) &&
                    !empty($model->passUser->user->house)
                )
                    ? $model->passUser->user->formattedAddress : "";
            })
            ->addColumn('community_name', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->community)
                )
                    ? $model->passUser->pass->community->name : "";
            })
            ->addColumn('event_name', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->event)
                )
                    ? $model->passUser->pass->event->name : "";
            })
            ->addColumn('formatted_pass_start_date', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->pass_start_date) &&
                    !empty($model->passUser->pass->convertedPassStartDate())
                )
                    ? date('M j, Y g:i A', $model->passUser->pass->convertedPassStartDate()) : "";
            })
            ->addColumn('formatted_pass_date', function ($model) {
                return (
                    !empty($model->passUser) &&
                    !empty($model->passUser->pass) &&
                    !empty($model->passUser->pass->pass_start_date) &&
                    !empty($model->passUser->pass->convertedPassDate())
                )
                    ? date('M j, Y g:i A', $model->passUser->pass->convertedPassDate()) : "";
            })
            ->addColumn('scan_date', function ($model) {
                return $model->scanDate;
            })
            ->addColumn('scan_by', function ($model) {
                return $model->ScanByName;
            })
            ->addColumn('vehicle_info', function ($model) {

                if (!empty($model->passUser) && !empty($model->passUser->vehicle)) {
                    return (!empty($model->passUser->vehicle->vehicleInfo)) ? $model->passUser->vehicle->vehicleInfo : "-";
                } else if (!empty($model->staticScanLog)) {
                    // return $model->staticScanLog;
                    return (!empty($model->staticScanLog->vehicleInfo) && property_exists($model->staticScanLog, 'vehicleInfo')) ? $model->staticScanLog->vehicleInfo : "-";
                } else {
                    return "-";
                }

            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->community_name)) {
                    $instance->whereHas('passUser.pass.community', function ($where) use ($request) {
                        $where->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
                if (isset($request->sender_name)) {

                    $instance->whereHas('staticScanLog', function ($where) use ($request) {
                        $where->where('sender', 'LIKE', '%' . $request->get('sender_name') . '%');
                    });
                }
                if (isset($request->receipt)) {

                    $instance->whereHas('staticScanLog', function ($where) use ($request) {
                        $where->where('recipient', 'LIKE', '%' . $request->get('receipt') . '%');
                    });
                }
                if (isset($request->event_name)) {
                    $instance->whereHas('passUser.pass.event', function ($where) use ($request) {
                        $where->where('name', 'like', "%{$request->get('event_name')}%");
                    });
                }

                if (isset($request->license_plate)) {

                    $instance->whereHas('passUser.vehicle', function ($where) use ($request) {
                        $where->where('license_plate', 'like', "%{$request->get('license_plate')}%");
                    });


                }
                if (isset($request->formatted_pass_start_date)) {
                    $instance->whereHas('passUser.pass', function ($where) use ($request) {
                        $where->whereDate('pass_start_date', $request->get('formatted_pass_start_date'));
                    });
                }
                if (isset($request->formatted_pass_date)) {
                    $instance->whereHas('passUser.pass', function ($where) use ($request) {
                        $where->whereDate('pass_date', $request->get('formatted_pass_date'));
                    });
                }
                if (isset($request->scan_date)) {


                    $date = explode(' - ', $request->scan_date); // Split the date range using ' - ' separator
                    $start_date = trim($date[0]);
                    $end_date = trim($date[1]);

                    $start_date = strtotime(str_replace('-', '/', $start_date));
                    $start_date = date('Y-m-d', $start_date);

                    $end_date = strtotime(str_replace('-', '/', $end_date));
                    $end_date = date('Y-m-d', $end_date);


                    if($start_date != $end_date){

                        $end_date = date('Y-m-d', strtotime('+1 day', strtotime($end_date)));
                        $instance->where('created_at', '>=', $start_date)
                            ->where('created_at', '<=', $end_date);
                    }else{
                        $instance->whereDate('created_at', $start_date);
                    }
                }
                if (isset($request->scan_by)) {
                    $instance->whereHas('scanBy', function ($query) use ($request) {
                        $query->where('first_name', 'like', "{$request->get('scan_by')}%");
                    });
                }

            })
            ->rawColumns(['image', 'license_plate_image_url'])
            ->make(true);
    }
}
