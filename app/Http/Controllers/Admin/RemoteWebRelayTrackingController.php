<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RemoteWebRelayTracking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Matrix\Builder;

class RemoteWebRelayTrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.remote-web-relay-tracking.index');
    }

    public function webRelayTrackingData()
    {
        $webRelayTracking = RemoteWebRelayTracking::with(['webRelay', 'guardUser']);

        if (auth()->user()->role_id == 2) {
            $webRelayTracking = $webRelayTracking->whereHas('webRelay', function ($query) {
                $query->where('community_id', auth()->user()->community_id);
            });
        }

        $webRelayTracking = $webRelayTracking->get();

        return datatables()->of($webRelayTracking)
            ->addColumn('web_relay', function ($model) {
                return $model->webRelay->mac_address;
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                              <form action="' . route('admin.settings.remote-web-relay-tracking.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $webRelayTracking = RemoteWebRelayTracking::find($id);
        $webRelayTracking->delete();

        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.settings.remote-web-relay-tracking.index')]);
    }
}
