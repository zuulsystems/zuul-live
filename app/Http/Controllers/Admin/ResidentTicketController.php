<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\HouseService;
use Illuminate\Http\Request;
use App\Services\TrafficTicketService;
use App\Services\TrafficTicketImageService;
use App\Services\CommunityService;
use App\Services\EmailLogService;
use App\Services\TrafficPaymentLogService;
use App\Helpers\Tickets;
use Illuminate\Http\Response;


class ResidentTicketController extends Controller
{
    use Tickets;

    private $trafficTicketService, $trafficTicketImageService, $emailLogService, $trafficPaymentLogService, $houseService;

    public function __construct(TrafficTicketService      $trafficTicketService,
                                TrafficTicketImageService $trafficTicketImageService,
                                CommunityService          $communityService,
                                EmailLogService           $emailLogService,
                                TrafficPaymentLogService  $trafficPaymentLogService,
                                HouseService              $houseService
    )
    {
        $this->trafficTicketService = $trafficTicketService;
        $this->trafficTicketImageService = $trafficTicketImageService;
        $this->communityService = $communityService;
        $this->emailLogService = $emailLogService;
        $this->trafficPaymentLogService = $trafficPaymentLogService;
        $this->houseService = $houseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data["houseId"] = $request->houseId ?? '';
        $data["title"] = 'Resident';
        return view('admin.tickets.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function residentTicketData(Request $request)
    {
        $displayDismissed = $request->boolean('displayDismissed', false);
        $houseId = $request->input('houseId', null);
        $userIds = [];

        if ($houseId) {
            $house = $this->houseService->find($houseId)['result'];

            if ($house && $house->residents->isNotEmpty()) {
                $userIds = $house->residents->pluck('id')->toArray();
            }
        }

        $communityId = getCommunityIdByUser();
        $data = $this->trafficTicketService->data('resident', $communityId, $displayDismissed, null, null, $userIds);

        return $data['bool'] ? $this->generateDatatable($request, $data['result']) : [];
    }



}

