<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use App\Rules\ValidateName;
use App\Services\CommunityService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PHPUnit\Framework\Constraint\Count;

class GuardController extends Controller
{
    protected $folderLink = 'admin.users.security-guards.';
    public $userService;
    public $communityService;

    public function __construct(
        UserService $userService,
        CommunityService $communityService
    )
    {
        $this->userService = $userService;
        $this->communityService = $communityService;
        $this->middleware('checkGuardAdmin', ['except' => ['index', 'guardData']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userId = null;
        $this->checkUserPermission();
        $data["userId"] = $request->userId ?? "";
        return view($this->folderLink . 'index',$data);
    }

    /**
     * get guards data
     * @return mixed
     * @throws \Exception
     */
    public function guardData(Request $request)
    {
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)){
            $userId = $request->userId;
        }
        $guardData = datatables()->of($this->userService->getCommunityGuardList(getCommunityIdByUser(),$userId)['result'])
            ->addColumn('full_name', function ($model) {
                return $model->fullname;
            })
            ->addColumn('role_name', function ($model) {
                return (!empty($model->role->name)) ? $model->role->name : "-";
            })
            ->addColumn('formatted_phone', function ($model) {
                return (!empty($model->formattedPhone)) ? $model->formattedPhone : "-";
            })
            ->addColumn('community', function ($model) {
                return (!empty($model->community->name)) ? $model->community->name : "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('guard_status', function ($model) {
                $status = "Active";
                if ($model->status == 'deactivate'){
                    $status = "Deactivate";
                }
                return $status;
            })
            ->addColumn('links', function ($model) {
                if(!Gate::allows('is-guard-admin')){
                    $dropDown = '<div class="dropdown">
                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Action
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="' . route('admin.user.guards.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>';

                    if(auth()->user()->cannot('only-tl-right')&&auth()->user()->cannot('kiosk'))
                    {
                        $dropDown .= '<a class="dropdown-item revoke-guard-permission-model" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="fa fa-user" aria-hidden="true"></i> Revoke Guard</a>';
                        if (!empty($model->verification_code) && $model->is_reset_password == '1'){
                            $dropDown .= '<a class="dropdown-item btn-resend-guard-verification-code" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="fa fa-user" aria-hidden="true"></i> Resend Verification Code</a>';
                        }

                    }
                    if (auth()->user()->hasRole('super_admin')){
                        $dropDown .= '<form action="' . route('admin.user.delete-guard', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>';
                    }
                    if ($model->status == 'active'){
                        if(auth()->user()->cannot('kiosk'))
                        {
                            $dropDown .= '<a class="dropdown-item deactivate-guard-status" data-id="'.$model->id.'" href="javascript:void(0)"><i class="fa fa-user-lock" aria-hidden="true"></i> Deactivate</a>';
                        }
                    } else {
                        $dropDown .= '<a class="dropdown-item active-guard-status" data-id="'.$model->id.'" href="javascript:void(0)"><i class="fa fa-lock-open" aria-hidden="true"></i> Active</a>';
                    }
                    if(auth()->user()->cannot('kiosk'))
                    {
                        $dropDown .= '
                        <a class="dropdown-item guard-permission-model" data-id="' . $model->id . '"  href="javascript:void(0)"><i class="fa fa-lock" aria-hidden="true"></i> Permission</a>
                      </div>
                    </div>';
                    }

                    return $dropDown;
                }
                return '';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->full_name)) {
                    $instance->where(\DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $request->get('full_name') . '%');

                }
                if (isset($request->formatted_phone)) {
                    $formatted_phone = onlyPhoneDigits($request->get('formatted_phone'));
                    $instance->where('phone_number', 'like', "%{$formatted_phone}%");
                }
                if (isset($request->email)) {
                    $instance->where('email','like', "%{$request->get('email')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use($request){
                         $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
            })
            ->rawColumns(['links'])
            ->toJson();

        return $guardData;
    }

    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGuard(Request $request,$userId){
        $user = $this->userService->deleteUser($userId);
        if ($user['bool']){
            $response = ['status' => true, 'message' => 'Purged Successfully','redirect' =>route('admin.user.guards.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something Went Wrong, Please try again'];
        }
        return response()->json($response);
    }
    /**
     * send reset link
     * @param Request $request
     * @return
     * @throws \Exception
     */
    public function sendResetLink(Request $request)
    {
        $user = $this->userService->find($request->id)['result'];
        if ($user) {
            $response = sendResetPasswordLink($user->email);

            if ($response) {
                $response = ['status' => true, 'message' => 'Password Reset link has been sent to email.'];
            } else {
                $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
            }
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    /**
     * check guard permissions
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function checkGuardPermission(Request $request){
        $user = $this->userService->find($request->id);
        if ($user['bool']) {
            $isGuardAdmin = false;
            //check if user is guard admin
            if ($user['result']->isGuardAdmin()){
                $isGuardAdmin = true;
            }
            $canCheckLicensePlate = $user['result']->hasPermission('can_check_license_plate');
            $checkRemoteGuard = $user['result']->enable_remote_guard == 1 ? true : false;
            $response = ['status' => true, 'data' => ['isGuardAdmin' => $isGuardAdmin, 'canCheckLicensePlate' => $canCheckLicensePlate,'checkRemoteGuard'=>$checkRemoteGuard]];

        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    /**
     * set guard permissions
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function guardPermission(Request $request){
        $userId = $request->user_id;
        $user = $this->userService->find($userId);

        //assign permission can check license plate
        if ($request->has('can_check_license_plate') && $request->can_check_license_plate == '1' ){
            $user['result']->assignPermission('can_check_license_plate');
        } else{
            $user['result']->revokePermission('can_check_license_plate');
        }
        //assign role guard admin
        if ($request->has('is_guard_admin') && $request->is_guard_admin == '1' ){
            $this->userService->update(['role_id' => 11],$userId); // guard admin role
        } else {
            $this->userService->update(['role_id' => 8],$userId);// guard role
        }

        //Toggle remote guard feature
        if ($request->has('remote_guard') && $request->remote_guard == '1' ){
            $this->userService->update(['enable_remote_guard' => 1],$userId); // remote guard feature to 1
        } else {
            $this->userService->update(['enable_remote_guard' => 0],$userId);// remote guard feature to 0
        }

        // user activity logs
        callUserActivityLogs("Guard Permission(s)", __FUNCTION__, __FILE__, __DIR__, true, "Updated Successfully");

        return response()->json(['status' => true,'message' => 'Updated Successfully','redirect' =>route('admin.user.guards.index')]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function create()
    {
        $this->checkUserPermission();
        $data['communities'] = getActivateCommunities();

        $data['phoneFormat'] = "999-999-9999";

        $data['countries'] = activeCountries();

        if(!empty(auth()->user()->community_id))
        {
            $countryPhoneFormat = CountryPhoneFormat::where('dial_code',auth()->user()->community->dial_code)->first();
            if(!empty($countryPhoneFormat))
            {
                $data['phoneFormat'] = $countryPhoneFormat->format;
            }
        }

        return view($this->folderLink . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $guardData = $request->all();
        $guardData['phone_number'] = trim(str_replace("-", "", $guardData['phone_number']));
        $validation = [
            'first_name' => ['required',new ValidateName],
            'last_name' => ['required',new ValidateName],
            'email' => 'required|email',
            'phone_number' => 'required',
            'community_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($guardData, $validation, $messages);
        $validator->after(function ($validator)use ($guardData) {

            $dialCode = str_replace("+","",$guardData['dial_code']);
            $phoneNumberDigits = 10;
            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

            if(!empty($countryPhoneFormat))
            {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            $phoneNumber = $guardData['phone_number'];
            $email = $guardData['email'];
            //check if phone number length less than 10 then show error message
            if (isset($guardData['phone_number']) && preg_match_all( "/[0-9]/", $guardData['phone_number'] ) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least '.$phoneNumberDigits.' digit long');
            }
            //check if phone number already exist
            if (!empty($guardData['phone_number'])) {
                $userByPhone = $this->userService->findByPhone($phoneNumber)['result'];
                if ($userByPhone) {
                    $phoneNumberMessage = 'The phone number has already been taken';
                    if (auth()->user()->hasRole('super_admin')){
                        $url =  generateUrlByUserRoleId($userByPhone);
                        $phoneNumberMessage = '<a href="'.$url.'" target="_blank">The phone number has already been taken.</a>';
                    }
                    $validator->errors()->add('phone_number',$phoneNumberMessage );
                }
            }
            //check if email already exist
            if (!empty($email)) {
                $userByEmail = $this->userService->findByEmail($email)['result'];
                if ($userByEmail) {
                    $emailMessage = 'The phone number has already been taken';
                    if (auth()->user()->hasRole('super_admin')){
                        $url =  generateUrlByUserRoleId($userByEmail);
                        $emailMessage = '<a href="'.$url.'" target="_blank">The email has already been taken.</a>';
                    }
                    $validator->errors()->add('email', $emailMessage);
                }
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }
        $verificationCode = guardVerificationCode(); // generate verification code
        $randomPassword = generateRandomStringWithSpecialCharacter();
        $guardData['password'] = bcrypt($randomPassword);
        $guardData['temp_password'] = $randomPassword;
        $guardData['verification_code'] = $verificationCode;
        $guardData['created_by'] = auth()->user()->id;
        $guardData['role_id'] = 8; // Guards / Security Personnel

        $request->rover_mode ? $guardData['rover_mode'] = '1' : $guardData['rover_mode'] = '0';
        $request->in_onboarded ? $guardData['in_onboarded'] = '1' : $guardData['in_onboarded'] = '0';

        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_'.time().'.'.$request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $guardData['profile_image'] = $fileName;
        }

        $guardData['is_reset_password'] = '1';
        $guard = $this->userService->create($guardData);
        if ($guard['bool']) {
            $emailData['user'] = $guard['result'];
            $emailData['type'] = 'guard';
            $emailData['hash'] = $guardData['verification_code'];
            dynamicEmail($emailData, $guard['result']->email);

            // user activity logs
            callUserActivityLogs("Add Security Guard", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Security Guard", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong.Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkUserPermission();
        $data['communities'] = getActivateCommunities();

        $data['countries'] = activeCountries();

        $guard = $this->userService->find($id);

        if(!$guard['bool'])
        {
            abort(500);
        }

        if(!$guard['result'])
        {
            abort(404);
        }

        $data['guard'] = $guard['result'];

        $data['phoneFormat'] = "999-999-9999";

        $dialCode;

        if(!empty(auth()->user()->community_id))
        {
            $dialCode = str_replace("+","",auth()->user()->community->dial_code);
        }
        else
        {
            $dialCode = str_replace("+","",$data['guard']->dial_code);
        }

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
        if(!empty($countryPhoneFormat))
        {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $guardData = $request->all();
        $guardData['phone_number'] = trim(str_replace("-", "", $guardData['phone_number']));
        $validation = [
            'first_name' => ['required',new ValidateName],
            'last_name' => ['required',new ValidateName],
            'email' => 'required|email|unique:users,email,' . $id,
            'phone_number' => 'required|unique:users,phone_number,' . $id,
            'community_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($guardData, $validation, $messages);
        $validator->after(function ($validator)use ($guardData) {

            $phoneNumberDigits = 10;
            $countryPhoneFormat = CountryPhoneFormat::where('dial_code',$guardData['dial_code'])->first();
            if(!empty($countryPhoneFormat))
            {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            if (isset($guardData['phone_number']) && preg_match_all( "/[0-9]/", $guardData['phone_number'] ) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least '.$phoneNumberDigits.' digit long');
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }

        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_'.time().'.'.$request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $guardData['profile_image'] = $fileName;
        }

        $request->rover_mode ?  $guardData['rover_mode'] = '1' : $guardData['rover_mode'] = '0';
        $request->in_onboarded ?  $guardData['in_onboarded'] = '1' : $guardData['in_onboarded'] = '0';

        $community = $this->userService->update($guardData, $id);
        if ($community['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Security Guard", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Security Guard", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = $this->userService->delete($id);
        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Delete Security Guard", __FUNCTION__, __FILE__, __DIR__, true, "Delete Successfully");

            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete Security Guard", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * role => super admin, community rights => is-zuul-right
     * check user roles
     */
    function checkUserPermission()
    {
        if (!Gate::any(['super-admin','community-admin', 'is-zuul-right','is-guard-admin','kiosk'])) {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Revoke Resident
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function revokeGuard(Request $request)
    {
        $user = $this->userService->find($request->guard_id);
        if (empty($user['result'])){
            return response()->json(['status' => false, 'message' => 'No Guard Found']);
        }
        if (!$user['result']->hasRole('guards') && !$user['result']->hasRole('guard_admin')){
            return response()->json(['status' => false, 'message' => 'User Is Not A Guard']);
        }
        $user['result']->revokePermission('head_of_family');
        $user['result']->revokePermission('can_manage_family');
        $user['result']->revokePermission('can_send_passes');
        $user['result']->revokePermission('allow_parental_control');

        $user['result']->community_id = null;
        $user['result']->house_id = null;
        $user['result']->role_id = 7;
        $user['result']->save();

        // user activity logs
        callUserActivityLogs("Revoke Guard Permission", __FUNCTION__, __FILE__, __DIR__, true, "Success");

        $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.user.guards.index')];
        return response()->json($response);
    }
    /**
     * Resend Guard Verification code
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function resendGuardVerificationCode(Request $request)
    {
        $guard = $this->userService->find($request->id);
        if (empty($guard['result'])){
            return response()->json(['status' => false, 'message' => 'No Guard Found']);
        }
        if (!$guard['result']->hasRole('guards') && !$guard['result']->hasRole('guard_admin')){
            return response()->json(['status' => false, 'message' => 'User Is Not A Guard']);
        }
        $emailData['user'] = $guard['result'];
        $emailData['type'] = 'guard';
        $emailData['hash'] = $guard['result']->verification_code;
        dynamicEmail($emailData, $guard['result']->email);

        // user activity logs
        callUserActivityLogs("Resend Guard Verification Code", __FUNCTION__, __FILE__, __DIR__, true, "Success");

        $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.user.guards.index')];
        return response()->json($response);
    }

    /**
     * deactivate community admin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deactivateGuard(Request $request){
        $updateCommunityAdmin = $this->userService->update([
            'status' => 'deactivate'
        ],$request->communityAdminId);

        if ($updateCommunityAdmin['bool']) {
            // user activity logs
            callUserActivityLogs("Deactivate Security Guard", __FUNCTION__, __FILE__, __DIR__, true, "Deactivated Successfully");

            $response = ['status' => true, 'message' => 'Deactivated Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Deactivate Security Guard", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * activate community admin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function activateGuard(Request $request){
        $updateCommunityAdmin = $this->userService->update([
            'status' => 'active'
        ],$request->communityAdminId);

        if ($updateCommunityAdmin['bool']) {
            // user activity logs
            callUserActivityLogs("Activate Security Guard", __FUNCTION__, __FILE__, __DIR__, true, "Activated Successfully");

            $response = ['status' => true, 'message' => 'Activated Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Activate Security Guard", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }
}
