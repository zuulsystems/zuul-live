<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Camera;
use App\Models\DeviceType;
use App\Services\CameraService;
use App\Services\CommunityService;
use App\Services\ScannerService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\ConnectedPie;
use Illuminate\Validation\ValidationException;

class CameraController extends Controller
{
    public $scannerService;
    public $cameraService;
    protected $folderLink = 'admin.cameras.';

    public function __construct(
        ScannerService $scannerService,
        CameraService  $cameraService
    )
    {
        $this->scannerService = $scannerService;
        $this->cameraService = $cameraService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get scanners list
     * @return mixed
     * @throws Exception
     */
    public function cameraData(Request $request)
    {
        return datatables()->of($this->cameraService->all(getCommunityIdByUser())['result'])
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "-";
            })
            ->addColumn('scanner_name', function ($model) {
                return $model->scanner->name ?? "-";
            })
            ->addColumn('formatted_mac_address_and_secret_key', function ($model) {
                $html = "{$model->mac_address}<br/><span style='font-size: 7px;'>{$model->secret_key}</span>";
                return $html;
            })
            ->addColumn('formatted_device_type', function ($model) {
                $device_type = '-';
                if ($model->device_type == 'lpr') {
                    $device_type = 'LPR';
                } else if ($model->device_type == 'non_lpr') {
                    $device_type = 'NON LPR';
                } else if ($model->device_type == 'doorbird') {
                    $device_type = 'Doorbird';
                }
                return $device_type;
            })
            ->addColumn('connected_pi_name', function ($model) {
                return $model->connectedpie->name ?? "-";
            })
            ->addColumn('lat', function ($model) {
                return (!empty($model->lat)) ? $model->lat : "-";
            })
            ->addColumn('long', function ($model) {
                return (!empty($model->long)) ? $model->long : "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('edit_link', function ($model) {
                return '<a href="' . route('admin.settings.cameras.edit', [$model->id]) . '">Edit </a>';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->name)) {
                    $instance->where('name', 'like', "%{$request->get('name')}%");
                }
                if (isset($request->mac_address)) {
                    $instance->where('mac_address', 'like', "%{$request->get('mac_address')}%");
                }
                if (isset($request->scanner_name)) {
                    $instance->whereHas('scanner', function ($where) use ($request) {
                        $where->where('name', 'like', "%{$request->get('scanner_name')}%");
                    });
                }
            })
            ->rawColumns(['edit_link', 'formatted_mac_address_and_secret_key'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'mac_address' => 'required|unique:cameras',
            'community_id' => 'required',
            'device_type' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $cameraData = $request->all();
        $cameraData['secret_key'] = \Str::random(10);
        $community = $this->cameraService->create($cameraData);
        if ($community['bool']) {
            // user activity logs
            callUserActivityLogs("Add Camera", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.settings.cameras.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Camera", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $scanners = $this->scannerService->all(getCommunityIdByUser());
        if (!$scanners['bool']) {
            abort(500);
        }

        $data['communities'] = getCommunities();
        $data['scanners'] = $scanners['result']->get();

        if (getCommunityIdByUser() != null) {
            $data['connected_pi'] = ConnectedPie::query()->where('community_id', getCommunityIdByUser())->get();
        } else {
            $data['connected_pi'] = ConnectedPie::query()->where(getCommunityIdByUser())->get();
        }

        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $scanners = $this->scannerService->all(getCommunityIdByUser());
        if (!$scanners['bool']) {
            abort(500);
        }
        $data['communities'] = getCommunities();
        $data['scanners'] = $scanners['result']->get();

        // add connected pi key
        if (getCommunityIdByUser() != null) {
            $data['connected_pi'] = ConnectedPie::query()->where('community_id', getCommunityIdByUser())->get();
        } else {
            $data['connected_pi'] = ConnectedPie::query()->where(getCommunityIdByUser())->get();
        }

        $camera = $this->cameraService->find($id);
        if (!$camera['bool']) {
            abort(500);
        }
        if (!$camera['result']) {
            abort(404);
        }
        $data['camera'] = $camera['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'mac_address' => 'required|unique:cameras,mac_address,' . $id,
            'community_id' => 'required',
            'device_type' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $scannerData = $request->all();
        $scannerData['secret_key'] = \Str::random(10);
        $scanner = $this->cameraService->update($scannerData, $id);

        if ($scanner['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Camera", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.settings.cameras.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Camera", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCamerasByCommunity(Request $request)
    {
        $data['cameras'] = Camera::where('community_id', $request->id)->get();
        return response()->json($data['cameras']);
    }
}
