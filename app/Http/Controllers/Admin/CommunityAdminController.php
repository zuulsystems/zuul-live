<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\DynamicEmail;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use App\Models\House;
use App\Models\OffensiveWord;
use App\Models\Rfid;
use App\Models\User;
use App\Models\Vendor;
use App\Rules\ValidateName;
use App\Services\CommunityService;
use App\Services\UserService;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CommunityAdminController extends Controller
{
    public $userService;
    public $communityService;
    protected $folderLink = 'admin.users.community-admins.';

    public function __construct(
        UserService      $userService,
        CommunityService $communityService
    )
    {
        $this->userService = $userService;
        $this->communityService = $communityService;
        $this->middleware('checkCommunityAdmin', ['except' => ['index', 'communityAdminsData', 'edit', 'create', 'update', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data["userId"] = $request->userId ?? "";
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get community admins data
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function communityAdminsData(Request $request)
    {

        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        return datatables()->of($this->userService->getCommunityAdminList(getCommunityIdByUser(), $userId)['result'])
            ->addColumn('full_name', function ($model) {
                return $model->fullname;
            })
            ->addColumn('role_name', function ($model) {
                return $model->role->name ?? "";
            })
            ->addColumn('formatted_phone', function ($model) {
                return $model->formattedPhone;
            })
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "-";
            })
            ->addColumn('community_admin_status', function ($model) {
                $status = "Active";
                if ($model->status == 'deactivate') {
                    $status = "Deactivate";
                }
                return $status;
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "";
            })
            ->addColumn('links', function ($model) {
                $items = '';
                if (auth()->user()->hasRole('sub_admin') || auth()->user()->hasRole('super_admin')) {
                    $items = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="' . route('admin.user.community-admins.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>';

                    if (auth()->user()->hasRole('super_admin')) {
                        $items .= '<form action="' . route('admin.user.delete-community-admin', [$model->id]) . '" method="POST" class="ajax-form">
                                        ' . csrf_field() . '
                                        <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                    </form>';
                    }

                    if ($model->status == 'active') {
                        $items .= '<a class="dropdown-item deactivate-admin-status" data-id="' . $model->id . '" href="javascript:void(0)"><i class="fa fa-user-lock" aria-hidden="true"></i> Deactivate</a>';
                    } else {
                        $items .= '<a class="dropdown-item active-admin-status" data-id="' . $model->id . '" href="javascript:void(0)"><i class="fa fa-lock-open" aria-hidden="true"></i> Active</a>';
                    }

                    $items .= '</div>
                        </div>';
                } else {
                    $items = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                        </div>';
                }
                return $items;

            })
            ->rawColumns(['links'])
            ->filter(function ($instance) use ($request) {
                if (isset($request->full_name)) {
                    $instance->where(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $request->get('full_name') . '%');

                }
                if (isset($request->formatted_phone)) {
                    $formatted_phone = onlyPhoneDigits($request->get('formatted_phone'));
                    $instance->where('phone_number', 'like', "%{$formatted_phone}%");
                }
                if (isset($request->email)) {
                    $instance->where('email', 'like', "%{$request->get('email')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
            })
            ->toJson();
    }

    /**
     * delete resident
     * @param Request $request
     * @param $userId
     */
    public function deleteCommunityAdmin(Request $request, $userId)
    {

        $offensiveWords = OffensiveWord::where('created_by', $userId);
        $offensiveWords->delete();

        $houses = House::where('created_by', $userId);
        $houses->forceDelete();

        $users = User::where('created_by', $userId)->get();
        foreach ($users as $user) {
            $user = $this->userService->deleteUser($user->id);
        }

        $rfids = Rfid::where('created_by', $userId);
        $rfids->forceDelete();

        $vendors = Vendor::where('created_by', $userId);
        $vendors->forceDelete();

        $user = $this->userService->deleteUser($userId);
        if ($user['bool']) {
            $response = ['status' => true, 'message' => 'Purged Successfully', 'redirect' => route('admin.user.community-admins.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something Went Wrong, Please try again'];
        }
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $communityAdminData = $request->all();
        $communityAdminData['phone_number'] = trim(str_replace("-", "", $communityAdminData['phone_number']));
        $validation = [
            'first_name' => ['required', new ValidateName],
            'last_name' => ['required', new ValidateName],
            'email' => 'required|email',
            'phone_number' => 'required',
            'community_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($communityAdminData, $validation, $messages);
        $validator->after(function ($validator) use ($communityAdminData) {
            $phoneNumber = $communityAdminData['phone_number'];
            $email = $communityAdminData['email'];

            $dialCode = str_replace("+", "", $communityAdminData['dial_code']);

            $phoneNumberDigits = 10;

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }


            //check if phone number length less than 10 then show error message
            if (isset($communityAdminData['phone_number']) && preg_match_all("/[0-9]/", $communityAdminData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }
            //check if phone number already exist
            if (!empty($communityAdminData['phone_number'])) {
                $userByPhone = $this->userService->findByPhone($phoneNumber)['result'];
                if ($userByPhone) {
                    $phoneNumberMessage = 'The phone number has already been taken';
                    if (auth()->user()->hasRole('super_admin')) {
                        $url = generateUrlByUserRoleId($userByPhone);
                        $phoneNumberMessage = '<a href="' . $url . '" target="_blank">The phone number has already been taken.</a>';
                    }
                    $validator->errors()->add('phone_number', $phoneNumberMessage);
                }
            }
            //check if email already exist
            if (!empty($email)) {
                $userByEmail = $this->userService->findByEmail($email)['result'];
                if ($userByEmail) {
                    $emailMessage = 'The email has already been taken';
                    if (auth()->user()->hasRole('super_admin')) {
                        $url = generateUrlByUserRoleId($userByEmail);
                        $emailMessage = '<a href="' . $url . '" target="_blank">The email has already been taken.</a>';
                    }
                    $validator->errors()->add('email', $emailMessage);
                }
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }
        $communityAdminData['created_by'] = auth()->user()->id;
        $communityAdminData['role_id'] = 2; // community admin
        //upload profile image to s3 bucket
        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_' . time() . '.' . $request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $communityAdminData['profile_image'] = $fileName;
        }

        $communityAdmin = $this->userService->create($communityAdminData)['result'];
        if ($communityAdmin) {
            $emailData['user'] = $communityAdmin;
            $emailData['type'] = 'community_admin';
            dynamicEmail($emailData, $communityAdmin->email);

            // user activity logs
            callUserActivityLogs("Add Community Admin", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.user.community-admins.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Community Admin", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $data['communities'] = getActivateCommunities();

        $data['countries'] = activeCountries();

        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['communities'] = getActivateCommunities();

        $data['countries'] = activeCountries();

        $community_admin = $this->userService->find($id);
        if (!$community_admin['bool']) {
            abort(500);
        }
        if (!$community_admin['result']) {
            abort(404);
        }
        $data['community_admin'] = $community_admin['result'];

        $data['phoneFormat'] = "999-999-9999";
        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $data['community_admin']->dial_code)->first();
        if (!empty($countryPhoneFormat)) {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $user = $this->userService->delete($id);
        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Delete Community Admin", __FUNCTION__, __FILE__, __DIR__, true, "Delete Successfully");

            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.user.community-admins.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete Community Admin", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * deactivate community admin
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function deactivateCommunityAdmin(Request $request)
    {
        $updateCommunityAdmin = $this->userService->update([
            'status' => 'deactivate'
        ], $request->communityAdminId);

        if ($updateCommunityAdmin['bool']) {
            // user activity logs
            callUserActivityLogs("Deactivate Community Admin", __FUNCTION__, __FILE__, __DIR__, true, "Deactivated Successfully");

            $response = ['status' => true, 'message' => 'Deactivated Successfully', 'redirect' => route('admin.user.community-admins.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Deactivate Community Admin", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $residentData = $request->all();
        $residentData['phone_number'] = trim(str_replace("-", "", $residentData['phone_number']));
        $validation = [
            'first_name' => ['required', new ValidateName],
            'last_name' => ['required', new ValidateName],
            'email' => 'required|email|unique:users,email,' . $id,
            'phone_number' => 'required|unique:users,phone_number,' . $id,
            'community_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($residentData, $validation, $messages);
        $validator->after(function ($validator) use ($residentData) {

            $phoneNumberDigits = 10;

            $dialCode = str_replace("+", "", $residentData['dial_code']);
            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            if (isset($residentData['phone_number']) && preg_match_all("/[0-9]/", $residentData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }

        //upload profile image to s3 bucket
        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_' . time() . '.' . $request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $residentData['profile_image'] = $fileName;
        }

        $community_admin = $this->userService->update($residentData, $id);
        if ($community_admin['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Community Admin", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.user.community-admins.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Community Admin", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong.Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * activate community admin
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function activateCommunityAdmin(Request $request)
    {
        $updateCommunityAdmin = $this->userService->update([
            'status' => 'active'
        ], $request->communityAdminId);

        if ($updateCommunityAdmin['bool']) {
            // user activity logs
            callUserActivityLogs("Activate Community Admin", __FUNCTION__, __FILE__, __DIR__, true, "Activated Successfully");

            $response = ['status' => true, 'message' => 'Activated Successfully', 'redirect' => route('admin.user.community-admins.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Activate Community Admin", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }
}
