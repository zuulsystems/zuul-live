<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmailType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Services\PrinterTemplateService;


class PrinterTemplateController extends Controller
{
    public $printerTemplateService;
    protected $folderLink = 'admin.printer-templates.';

    public function __construct(PrinterTemplateService $printerTemplateService)
    {
        $this->printerTemplateService = $printerTemplateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get printer templates list
     * @return mixed
     * @throws Exception
     */
    public function printerTemplateData()
    {
        $communityId = '';
        if (!empty(getCommunityIdByUser())) {
            $communityId = getCommunityIdByUser();
        }
        return datatables()->of($this->printerTemplateService->all($communityId)['result'])
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "";
            })
            ->addColumn('edit_link', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="' . route('admin.settings.printer-templates.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>

                                  <form action="' . route('admin.settings.delete-printer-template', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>
                                </div>
                        </div>';
            })
            ->addColumn('community', function ($model) {
                return (!empty($model->community->name)) ? $model->community->name : '-';
            })
            ->rawColumns(['template', 'edit_link'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => 'required',
            'community_id' => 'required',
            'template' => 'required'
        ];
        $messages = [
            'name.required' => 'Please enter  name',
            'community_id.required' => 'Please select community to',
            'template.required' => 'Please enter template',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $printerTemplateData = $request->all();

        $printerTemplate = $this->printerTemplateService->create($printerTemplateData);

        if ($printerTemplate['bool']) {

            // user activity logs
            callUserActivityLogs("Add Printer Template", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.settings.printer-templates.index')];
        } else {

            // user activity logs
            callUserActivityLogs("Add Printer Template", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['communities'] = getActivateCommunities();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['printerTemplate'] = $this->printerTemplateService->find($id)['result'];
        $data['communities'] = getActivateCommunities();
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'name' => 'required',
            'community_id' => 'required',
            'template' => 'required'
        ];
        $messages = [
            'name.required' => 'Please enter  name',
            'community_id.required' => 'Please select community to',
            'template.required' => 'Please enter template',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $printerTemplateData = $request->all();

        $printerTemplate = $this->printerTemplateService->update($printerTemplateData, $id);

        if ($printerTemplate['bool']) {

            // user activity logs
            callUserActivityLogs("Edit Printer Template", __FUNCTION__, __FILE__, __DIR__, true, "Updated Successfully");

            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.settings.printer-templates.index')];
        } else {

            // user activity logs
            callUserActivityLogs("Edit Printer Template", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * delete TEMPLATE
     * @param Request $request
     * @param $userId
     */
    public function deletePrinterTemplate(Request $request, $userId)
    {

        $user = $this->printerTemplateService->deleteUser($userId);

        if ($user['bool']) {

            // user activity logs
            callUserActivityLogs("Delete Printer Template", __FUNCTION__, __FILE__, __DIR__, true, "Purged Successfully");

            $response = ['status' => true, 'message' => 'Purged Successfully', 'redirect' => route('admin.settings.printer-templates.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete Printer Template", __FUNCTION__, __FILE__, __DIR__, false, "Something Went Wrong, Please try again.");

            $response = ['status' => false, 'message' => 'Something Went Wrong, Please try again.'];
        }
        return response()->json($response);
    }
}
