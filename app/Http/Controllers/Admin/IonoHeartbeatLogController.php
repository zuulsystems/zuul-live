<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ConnectedPie;
use App\Models\IonoHeartbeatLog;
use App\Models\Scanner;
use App\Services\IonoHeartbeatLogService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IonoHeartbeatLogController extends Controller
{
    private $ionoHeartbeatLogService;

    public function __construct(IonoHeartbeatLogService $ionoHeartbeatLogService)
    {
        $this->ionoHeartbeatLogService = $ionoHeartbeatLogService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.iono-heartbeat-log.index');
    }

    public function ionoHeartbeatLogsData(Request $request)
    {
        $ionoHeartbeatLogs = IonoHeartbeatLog::get();

        foreach ($ionoHeartbeatLogs as $ionoHeartbeatLog) {
            if ($ionoHeartbeatLog->type == 'pi') {
                $connectedPie = ConnectedPie::where('mac_address', $ionoHeartbeatLog->connected_pi_mac_address)->first();
                $ionoHeartbeatLog['community_name'] = "";
                if (!empty($connectedPie)) {
                    $ionoHeartbeatLog['community_name'] = $connectedPie->name;
                }
            } else if ($ionoHeartbeatLog->type == 'iono') {
                $scanner = Scanner::with('community')->where('mac_address', $ionoHeartbeatLog->connected_pi_mac_address)->first();
                $ionoHeartbeatLog['community_name'] = "";
                if (!empty($scanner)) {
                    $ionoHeartbeatLog['community_name'] = $scanner->community->name;
                }
            }
        }

        return datatables()->of($ionoHeartbeatLogs)
            ->editColumn('created_at', function ($model) {
                return $model->created_at;
            })
            ->addColumn('formatted_status', function ($model) {
                return $model->status == '1' ? '<div class="badge badge-success">Success</div>' : '<div class="badge badge-danger">Fail</div>';
            })
            ->filter(function ($instance) use ($request) {

                if ($request->has('connected_pi_mac_address') && !empty($request->connected_pi_mac_address)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return containsInsensitive($row['connected_pi_mac_address'], $request->connected_pi_mac_address);
                    });
                }
                if ($request->has('community') && !empty($request->community)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return containsInsensitive($row['community_name'], $request->community);
                    });
                }
                if ($request->has('type') && !empty($request->type)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if ($row['type'] == $request->type) {
                            return true;
                        }
                    });
                }
                if (isset($request->status)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if ($row['status'] == $request->status) {
                            return true;
                        }
                    });
                }

            })
            ->rawColumns(['formatted_status', 'links'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->ionoHeartbeatLogService->delete($id);
        return response()->json(['status' => true, 'message' => 'Iono Heartbeat log deleted successfully', 'redirect' => route('admin.iono-heartbeat.index')]);
    }
}
