<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Services\VendorService;
use App\Services\VehicleVendorService;
use App\Services\VehicleService;
use Illuminate\Validation\ValidationException;

class VendorController extends Controller
{
    public $userService;
    public $vendorService;
    public $vehicleVendorService;
    public $vehicleService;
    protected $folderLink = 'admin.vendors.';

    public function __construct(UserService $userService, VendorService $vendorService, VehicleVendorService $vehicleVendorService, VehicleService $vehicleService)
    {
        $this->userService = $userService;
        $this->vendorService = $vendorService;
        $this->vehicleVendorService = $vehicleVendorService;
        $this->vehicleService = $vehicleService;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get vendors list
     * @return mixed
     * @throws Exception
     */
    public function vendorsData(Request $request)
    {
        return datatables()->of($this->vendorService->all(getCommunityIdByUser())['result'])
            ->addColumn('formatted_phone', function ($model) {
                return $model->phone ?? "-";
            })
            ->addColumn('vehicle_count', function ($model) {
                return $model->vehicle_vendor->count() ?? "-";
            })
            ->addColumn('ticket_count', function ($model) {
                $vendor_ticket_count = 0;
                if ($model->vehicles->isNotEmpty()) {
                    foreach ($model->vehicles as $vehicle) {
                        $vendor_ticket_count += $vehicle->traffic_tickets_count;
                    }
                }
                return $vendor_ticket_count;
            })
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="' . route('admin.vendor-vehicles', ['vendorId' => $model->id]) . '"><i class="fa fa-eye" aria-hidden="true"></i> View Vehicles </a>
                            <a class="dropdown-item" href="' . route('admin.vendors.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                            </div>
                        </div>';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['vendor_name'], $request->get('name')) ? true : false;
                    });
                }
                if (isset($request->email)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['email'], $request->get('email')) ? true : false;
                    });
                }
                if (isset($request->phone_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['phone'], $request->get('phone_number')) ? true : false;
                    });
                }
                if (isset($request->community_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['community_name'], $request->get('community_name')) ? true : false;
                    });
                }
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $vendorData = $request->all();
        $vendorData['phone'] = trim(str_replace("-", "", $vendorData['phone']));
        $validation = [
            'vendor_name' => 'required',
            'email' => 'required|email|unique:vendors',
            'phone' => 'required|unique:vendors',
            'address' => 'required',
        ];
        $messages = [
            'vendor_name.required' => 'Please enter name',
            'email.required' => 'Please enter email',
            'phone.required' => 'Please select phone number',
            'address.required' => 'Please enter address',
        ];
        $validator = Validator::make($vendorData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $vendorData['community_id'] = auth()->user()->community_id;// set community
        $vendorData['created_by'] = auth()->user()->id;

        $vendor = $this->vendorService->create($vendorData);
        if ($vendor['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.vendors.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['countries'] = Country::all();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['countries'] = Country::all();
        $vendor = $this->vendorService->find($id);
        if (!$vendor['bool']) {
            abort(500);
        }
        if (!$vendor['result']) {
            abort(404);
        }
        $data['vendor'] = $vendor['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $vendorData = $request->all();
        $vendorData['phone'] = trim(str_replace("-", "", $vendorData['phone']));
        $validation = [
            'vendor_name' => 'required',
            'email' => 'required|email|unique:vendors,email,' . $id,
            'phone' => 'required|unique:vendors,phone,' . $id,
            'address' => 'required'
        ];
        $messages = [
            'vendor_name.required' => 'Please enter name',
            'email.required' => 'Please enter email',
            'phone.required' => 'Please select phone number',
            'address.required' => 'Please enter address',
        ];
        $validator = Validator::make($vendorData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $vendor = $this->vendorService->update($vendorData, $id);
        if ($vendor['bool']) {
            $response = ['status' => true, 'message' => 'update Successfully', 'redirect' => route('admin.vendors.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function sendEmail(Request $request)
    {
    }

    public function vendorVehicles(Request $request)
    {
        $data["vendorId"] = $request->vendorId ?? "";
        return view('admin.vendor-vehicles.index', $data);
    }

    public function vendorVehiclesData(Request $request)
    {
        $vendorId = null;
        if ($request->has('vendorId') && !empty($request->vendorId)) {
            $vendorId = $request->vendorId;
        }

        $vendorVehicle = $this->vehicleVendorService->getVendorVehicles($vendorId)['result'];

        return datatables()->of($this->vehicleService->all(null, null, $vendorVehicle)['result'])
            ->addColumn('user_name', function ($model) {
                return (!empty($model->user)) ? $model->user->fullName : "-";
            })
            ->addColumn('user_name', function ($model) {
                return (!empty($model->user)) ? $model->user->fullName : "-";
            })
            ->addColumn('ticket_count', function ($model) {
                if (!empty($model->trafficTickets)) {
                    return $model->trafficTickets->count();
                }
                return "0";
            })
            ->addColumn('edit', function ($model) {
                return '<a href="' . route("admin.vehicles.edit", $model->id) . '">Edit</a>';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->license_plate)) {
                    $instance->where('license_plate', 'like', "%{$request->get('license_plate')}%");
                }
                if (isset($request->make)) {
                    $instance->where('make', 'like', "%{$request->get('make')}%");
                }
                if (isset($request->model)) {
                    $instance->where('model', 'like', "%{$request->get('model')}%");
                }
                if (isset($request->year)) {
                    $instance->where('year', 'like', "%{$request->get('year')}%");
                }
                if (isset($request->color)) {
                    $instance->where('color', 'like', "%{$request->get('color')}%");
                }
                if (isset($request->user)) {
                    $full_names = explode(" ", $request->get('user'));
                    $instance->whereHas('user', function ($where) use ($request, $full_names) {
                        $where->where('first_name', $full_names);
                        $where->orWhere(function ($query) use ($full_names) {
                            $query->whereIn('last_name', $full_names);
                        });
                    });
                }
            })
            ->rawColumns(['edit'])
            ->toJson();
    }
}
