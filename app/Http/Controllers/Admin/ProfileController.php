<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{
    public $userService;
    protected $folderLink = 'admin.profile.';

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'profile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $residentData = $request->all();

        $residentData["is_two_factor"] = $request->is_two_factor ? '1' : '0';

        $validation = [
            'first_name' => 'required',
            'last_name' => 'required',
        ];

        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
        ];

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        //upload profile picture
        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_' . time() . '.' . $request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $residentData['profile_image'] = $fileName;
        }

        $user = $this->userService->update($residentData, $id);
        if ($user['bool']) {
            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.profile.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }
}
