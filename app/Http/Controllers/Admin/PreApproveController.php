<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\Contact;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\UserContactService;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Framework\Constraint\Count;

class PreApproveController extends Controller
{
    private $contactSrvc;
    private $userSrvc;


    public function __construct(UserContactService $userContactService, UserService $userService)
    {
        $this->contactSrvc = $userContactService;
        $this->userSrvc = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //
        $data["communityId"] = $request->communityId ?? '';
        $data['communities'] = getCommunities();
        $data['houseId'] = $request->houseId ?? '';
        $data['userId'] = $request->userId ?? '';
        return view('admin.pre-approved.index', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        //
        $data['communities'] = getCommunities();

        $data['countries'] = activeCountries();

        $data['phoneFormat'] = "999-999-9999";

        if (!empty(auth()->user()->community_id)) {
            $dialCode = str_replace("+", "", auth()->user()->community->dial_code);

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
            if (!empty($countryPhoneFormat)) {
                $data['phoneFormat'] = $countryPhoneFormat->format;
            }
        }


        $data['userId'] = $request->userId ?? '';
        return view('admin.pre-approved.create', $data);
    }

    /**
     * Pre Approved List.
     *
     * @return Response
     */
    public function preApprovedData(Request $request)
    {


        $user_id = $request->userId;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $is_favourite = ($request->has('is_favourite') && $request->is_favourite == '1') ? true : false;
        $is_dnc = true;

        $communityId = null;
        if (!empty(getCommunityIdByUser())) {
            $communityId = getCommunityIdByUser();
        }

        $data = $this->contactSrvc->getContactListForAdmin($user_id, 30, $search, $is_favourite, $is_dnc, $communityId);

        if ($data->count() > 0) {
            $data = $data->toArray();
        }

        if ($is_dnc) {
            $household_members = $this->userSrvc->getHouseHoldMemberDetailForAdmin($user_id, null, $search, $communityId);
            if (!empty($household_members)) {
                $familyMemberIds = $household_members->pluck('id');
                $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds);

                if ($familyMemberSharedContacts->count() > 0) {
                    $familyMemberSharedContacts = $familyMemberSharedContacts->toArray();
                    if (count($data) > 0) {
                        $data = array_merge($familyMemberSharedContacts, $data);
                    } else {
                        $data = $familyMemberSharedContacts;
                    }
                }


            }

        }

        $houseData = datatables()->of($data)
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                                <!--<a class="dropdown-item" href="' . route('admin.pre-approved.edit', $model['id']) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>-->
                            <form action="' . route('admin.pre-approved.destroy', [$model['id']]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>
                            </div>
                        </div>';
            })
            ->filter(function ($instance) use ($request) {

                if (isset($request->contact_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['contact_name'], $request->contact_name);
                    });
                }

                if (isset($request->phone_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['formatted_phone_number'], $request->phone_number);
                    });
                }
            })
            ->rawColumns(['links'])
            ->toJson();

        return $houseData;
    }

    /**
     * Determine if a given string contains all array values.
     * Not case sensitive
     *
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public function containsInsensitive($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && mb_stripos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $contactData = $request->all();


        $isFromPreApproved = true;

        $validation = [
            'contact_name' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',
        ];


        $validator = Validator::make($contactData, $validation, $messages);


        if (!empty($request->phone_number)) {


            $contactData['phone_number'] = str_replace("-", "", $contactData['phone_number']);
            $contactData['phone_number'] = str_replace("_", "", $contactData['phone_number']);
            $contactData['phone_number'] = str_replace("(", "", $contactData['phone_number']);
            $contactData['phone_number'] = str_replace(")", "", $contactData['phone_number']);

            $phoneNumberDigits = 10;

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $contactData['dial_code'])->first();
            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            $validator->after(function ($validator) use ($contactData, $phoneNumberDigits) {

                if (strlen($contactData['phone_number']) != $phoneNumberDigits) {
                    $validator->errors()->add("phone_number", "Phone Number should be " . $phoneNumberDigits . " digits long");
                }
            });

        }


        if ($validator->fails()) {
            $validator->validate();
        }

        if ($isFromPreApproved) {
            $contactData['is_dnc'] = 1;
        }
        $contactData['is_from_pre_approved'] = $isFromPreApproved;

        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData, $request->userId);

        if ($userContact['bool']) {
            $response = ['status' => true, 'message' => 'Pre-Approved Guest Added Successfully', 'redirect' => route('admin.pre-approved.index', ['userId' => $request->userId])];

        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['countries'] = activeCountries();

        $data['userContact'] = $this->contactSrvc->find($id)['result'];

        $data['phoneFormat'] = "999-999-9999";

        $dialCode = str_replace("+", "", $data['userContact']->contact->dial_code);

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
        if (!empty($countryPhoneFormat)) {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        $data['userId'] = $data['userContact']->created_by;
        return view('admin.pre-approved.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $contactData = $request->all();

        $validation = [
            'contact_name' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',
        ];

        $validator = Validator::make($contactData, $validation, $messages);

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $contactData['dial_code'])->first();

        if (!empty($request->phone_number)) {
            $contactData['phone_number'] = str_replace("-", "", $contactData['phone_number']);
            $contactData['phone_number'] = str_replace("_", "", $contactData['phone_number']);
            $contactData['phone_number'] = str_replace("(", "", $contactData['phone_number']);
            $contactData['phone_number'] = str_replace(")", "", $contactData['phone_number']);

            $phoneNumberDigits = 10;

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            $validator->after(function ($validator) use ($contactData, $phoneNumberDigits) {

                if (strlen($contactData['phone_number']) != $phoneNumberDigits) {
                    $validator->errors()->add("phone_number", "Phone number should be of " . $phoneNumberDigits . " digits");
                }

            });

        } else {
            $phoneNumberLength = 10;
            $phoneNumber = "1111111111";

            if (!empty($countryPhoneFormat)) {
                $phoneNumber = "";
                $phoneNumberLength = $countryPhoneFormat->digit;

                for ($i = 0; $i < $phoneNumberLength; $i++) {
                    $phoneNumber .= "1";
                }
            }

            $contactData['phone_number'] = $phoneNumber;

        }

        if ($validator->fails()) {
            $validator->validate();
        }

        $userContact = $this->contactSrvc->find($id)['result'];
        $userContact->update(['contact_name' => $request->contact_name, 'is_dnc_shared' => $request->is_dnc_shared]);

        $contact = Contact::find($userContact->contact_id);
        $contact = $contact->update(['phone_number' => $contactData['phone_number'], 'dial_code' => $request->dial_code]);

        $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.pre-approved.index', ['userId' => $request->userId])];
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $userContact = $this->contactSrvc->find($id)['result'];
        $userId = $userContact->created_by;
        $userContact->delete();
        return response()->json(['status' => true, 'message' => 'Pre-Approved Guest deleted successfully', 'redirect' => route('admin.pre-approved.index', ['userId' => $userId])]);
    }
}
