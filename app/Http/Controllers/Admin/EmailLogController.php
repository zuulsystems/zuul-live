<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\EmailLogService;
use Illuminate\Http\Request;

class EmailLogController extends Controller
{

    public $emailLogService;
    protected $folderLink = 'admin.email-logs.';

    public function __construct(EmailLogService $emailLogService)
    {
        $this->emailLogService = $emailLogService;
    }

    public function index()
    {
        return view($this->folderLink . 'index');
    }

    public function emailLogsData(Request $request)
    {
        return datatables()->of($this->emailLogService->data(getCommunityIdByUser())['result'])
            ->addColumn('formatted_ticket_id', function ($model) {
                return (!empty($model->ticket)) ? $model->ticket->ticket_id : "-";
            })
            ->addColumn('ticket_user_full_name', function ($model) {
                return (!empty($model->ticket) && !empty($model->ticket->user)) ? $model->ticket->user->fullName : "-";
            })
            ->addColumn('formatted_ticket_time', function ($model) {
                return (!empty($model->created_at)) ? date('m-d-Y h:i:s A', strtotime($model->created_at)) : "-";
            })
            ->addColumn('pass_created_by', function ($model) {
                return ((!empty($model->pass)) && !empty($model->pass->createdBy)) ? $model->pass->createdBy->full_name : "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->format('d-m-Y h:i:s A') : "-";
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('ticket') && !empty($request->ticket)) {
                    $instance->whereHas('ticket', function ($q) use ($request) {
                        $q->where('id', $request->get('ticket'));
                    });
                }
                if ($request->has('ticket_id') && !empty($request->ticket_id)) {
                    $instance->whereHas('ticket', function ($q) use ($request) {
                        $q->where('ticket_id', $request->get('ticket_id'));
                    });
                }
                if ($request->has('violation_by') && !empty($request->violation_by)) {
                    $instance->whereHas('ticket.user', function ($q) use ($request) {
                        $q->where('first_name', 'like', "%{$request->get('violation_by')}%")
                            ->orWhere('last_name', 'like', "%{$request->get('violation_by')}%");
                    });
                }
                if ($request->has('from') && !empty($request->from)) {
                    $instance->whereHas('ticket', function ($q) use ($request) {
                        $q->where('time', '>=', $request->get('from'));
                    });
                }
                if ($request->has('to') && !empty($request->to)) {
                    $instance->whereHas('ticket', function ($q) use ($request) {
                        $q->where('time', '<=', $request->get('to'));
                    });
                }
            })->toJson();

    }

    public function show($id)
    {
        return view($this->folderLink . 'index', ['ticket' => $id]);
    }
}
