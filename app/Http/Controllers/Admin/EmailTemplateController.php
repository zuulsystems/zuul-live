<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmailType;
use App\Services\EmailTemplateService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Str;

class EmailTemplateController extends Controller
{

    public $emailTemplateService;
    protected $folderLink = 'admin.email-templates.';

    public function __construct(EmailTemplateService $emailTemplateService)
    {
        $this->emailTemplateService = $emailTemplateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get email templates list
     * @return mixed
     * @throws Exception
     */
    public function emailTemplateData()
    {
        return datatables()->of($this->emailTemplateService->all()['result'])
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "";
            })
            ->addColumn('edit_link', function ($model) {
                return '<a href="' . route('admin.settings.email-templates.edit', [$model->id]) . '">Edit </a>';
            })
            ->rawColumns(['template', 'edit_link'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => 'required',
            'assign_to' => 'required',
            'template' => 'required'
        ];
        $messages = [
            'name.required' => 'Please enter  name',
            'assign_to.required' => 'Please select assign to',
            'template.required' => 'Please enter template',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $templateData = $request->all();
        $templateData['created_by'] = auth()->user()->id;
        $templateData['community_id'] = auth()->user()->community_id;
        $templateData['email_type'] = 'zuul';
        $templateData['template_code'] = Str::slug($templateData['name'], '-');
        $data = $request->all();
        $community = $this->emailTemplateService->create($templateData);
        if ($community['bool']) {

            // user activity logs
            callUserActivityLogs("Add Email Template", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.settings.email-templates.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Email Template", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['email_types'] = EmailType::where('email_type', 'admin')->get();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $data['email_types'] = EmailType::where('email_type', 'admin')->get();
        $emailTemplate = $this->emailTemplateService->find($id);
        if (!$emailTemplate['bool']) {
            abort(500);
        }
        if (!$emailTemplate['result']) {
            abort(500);
        }
        $data['emailTemplate'] = $emailTemplate['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'name' => 'required',
            'assign_to' => 'required',
            'template' => 'required'
        ];
        $messages = [
            'name.required' => 'Please enter name',
            'assign_to.required' => 'Please select assign to',
            'template.required' => 'Please enter template',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $templateData = $request->all();
        $templateData['community_id'] = auth()->user()->community_id;
        $templateData['email_type'] = 'zuul';

        $templateData['template_code'] = Str::slug($templateData['name'], '-');
        $data = $request->all();
        $community = $this->emailTemplateService->update($templateData, $id);
        if ($community['bool']) {

            // user activity logs
            callUserActivityLogs("Edit Email Template", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.settings.email-templates.index')];
        } else {

            // user activity logs
            callUserActivityLogs("Edit Email Template", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Guest Arrival Email
     *
     */
    public function guestArrivalEmail()
    {
        sendGuestArrivalToEmail(1, false);
    }
}
