<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\LocationService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Community;
use Illuminate\Support\Str;
use DataTables;

class LocationController extends Controller
{

    protected $folderLink = 'admin.camera-location.';
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();

        // If user is Admin or TL Admin, then send Communities Data to View
        $data['communities'] = ($user->hasRole('super_admin') || $user->hasRole('tl_admin')) ?
            Community::where('community_right', '!=', 'zuul')->orderBy('name', 'ASC')->pluck('name', 'id') :
            [];

        // List of Communities for Admin and TL Admin
        return view($this->folderLink . 'index', $data);
    }


    public function locationsData(Request $request)
    {
        // Admin Check
        $user = Auth::user();
        $is_admin = $user->hasRole('super_admin') || $user->hasRole('tl_admin');
        $admin_check = $is_admin ? null : $user->community_id;

        // Filters
        if ($request->has('community_id')) {
            if ($is_admin || $user->community_id == $request->community_id) {
                $admin_check = $request->community_id;
            } else {
                abort(403);
            }
        }

        // Return DataTables Dropdown
        return Datatables::of($this->locationService->getLocationData($admin_check)['result'])
            ->addColumn('action', function ($location) use ($is_admin) {
                $dropdown = '<div class="dropdown">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                        <div class="dropdown-menu">';

                if ($is_admin) {
                    $dropdown .= '<a class="dropdown-item" href="#" id="communitySelect" ' .
                        (is_null($location->community_id) ? '><i class="fa fa-edit" aria-hidden="true"></i> Add Community</a>' :
                            'community="' . $location->community_id . '"><i class="fa fa-edit" aria-hidden="true"></i> Change Community</a>');
                }

                if ($location->community_id) {
                    $dropdown .= '<a class="dropdown-item" href="' . route('admin.traffic-tickets.unmatched-tickets.show', $location->id) . '"><i class="fa fa-eye" aria-hidden="true"></i> View Tickets</a>';
                }

                return $dropdown .= ' </div>
                    </div>';
            })
            ->filter(function ($instance) use ($request) {
                if ($request->filled('camera_id')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['location_id'] == $request->camera_id;
                    });
                }

                if ($request->filled('camera_location')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['address'], $request->camera_location);
                    });
                }

                if ($request->filled('community_name')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return isset($row['community_name']['name']) &&
                            Str::contains($row['community_name']['name'], $request->community_name);
                    });
                }
            })
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $user = Auth::user();
        // If user is Admin or TL Admin, Then Send Communities Data to View - Will be used in Add/Change Community
        $data['communities'] = $user->hasRole('super_admin') || $user->hasRole('tl_admin') ? Community::where('community_right', '!=', 'zuul')->orderBy('name', 'ASC')->pluck('name', 'id') : [];

        $data['community'] = $id;
        //List of Communitis for Admin and TL Admin
        return view($this->folderLink . 'index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //Get All of the Data
        $data = $request->only('community_id');

        // Apply Required validation
        $validation = [
            'community_id' => 'required|exists:communities,id',
        ];

        //Custom Validation Messages
        $messages = [
            'community_id.required' => 'Please select community',
            'community_id.exists' => 'Please select a valid community'
        ];

        //Make Validator
        $validator = Validator::make($data, $validation, $messages);

        // Show Validation Errors on Faliure
        if ($validator->fails()) {
            $validator->validate();
        }

        // Update the Location
        $location = $this->locationService->updateByCameraId($data, $id);

        // Make the Result
        if ($location['bool']) {

            //Mark:- Track Activity
            callUserActivityLogs("Camera Location's Community Update", __FUNCTION__, __FILE__, __DIR__, true, 'Camera Location Community Update Successfully');

            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.locations.index')];
        } else {

            //Mark:- Track Activity
            callUserActivityLogs("Camera Location's Community Update", __FUNCTION__, __FILE__, __DIR__, false, 'Camera Location Community Update Failed');

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }

        // Return the Result
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
