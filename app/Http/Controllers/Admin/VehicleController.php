<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\VehicleService;
use App\Services\VehicleVendorService;
use App\Services\VendorService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class VehicleController extends Controller
{
    public $vehicleService;
    public $vendorService;
    public $vehicleVendorService;
    protected $folderLink = 'admin.vehicles.';

    public function __construct(
        VehicleService       $vehicleService,
        VendorService        $vendorService,
        VehicleVendorService $vehicleVendorService
    )
    {
        $this->vehicleService = $vehicleService;
        $this->vendorService = $vendorService;
        $this->vehicleVendorService = $vehicleVendorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // dd($this->vendorService->all()['result'], "Here");
        $data["vendors"] = $this->vendorService->all()['result'];
        $data["vendorId"] = $request->vendorId ?? "";
        $data['vehicleId'] = $request->vehicleId ?? "";
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get vehicle list
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function vehiclesData(Request $request)
    {
        // dd($request->all());
        $userId = null;
        if ($request->has('vendorId') && !empty($request->vendorId)) {
            $userId = $request->vendorId;
        }
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }

        $vehicleId = null;

        if ($request->has('vehicleId') && !empty($request->vehicleId)) {
            $vehicleId[0] = $request->vehicleId;
        }


        return datatables()->of($this->vehicleService->all(getCommunityIdByUser(), $userId, $vehicleId)['result'])
            ->addColumn('user_name', function ($model) {
                return (!empty($model->user)) ? $model->user->fullName : "-";
            })
            ->addColumn('make', function ($model) {
                return (!empty($model->make)) ? $model->make : "-";
            })
            ->addColumn('model', function ($model) {
                return (!empty($model->model)) ? $model->model : "-";
            })
            ->addColumn('year', function ($model) {
                return (!empty($model->year)) ? $model->year : "-";
            })
            ->addColumn('color', function ($model) {
                return (!empty($model->color)) ? $model->color : "-";
            })
            ->addColumn('ticket_count', function ($model) {
                return $model->traffic_tickets_count;
            })
            ->addColumn('edit', function ($model) {
                $links = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="' . route("admin.vehicles.edit", $model->id) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                            <a class="dropdown-item btn-connect-with-vendor-model" data-name="' . $model->license_plate . '" data-id="' . $model->id . '" href="javascript:void(0)"><i class="fa fa-truck" aria-hidden="true"></i> Connect With Vendor </a>
                            ';
                if ($model->traffic_tickets_count > 0) {
                    $links .= '<a class="dropdown-item" href="' . route('admin.traffic-tickets.tickets-by-license-plate', $model->license_plate) . '"><i class="fa fa-ticket-alt" aria-hidden="true"></i>
                              View Tickets </a>';
                }

                $links .= '</div>
                        </div>';
                return $links;
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->license_plate)) {
                    $instance->where('license_plate', 'like', "%{$request->get('license_plate')}%");
                }
                if (isset($request->make)) {
                    $instance->where('make', 'like', "%{$request->get('make')}%");
                }
                if (isset($request->model)) {
                    $instance->where('model', 'like', "%{$request->get('model')}%");
                }
                if (isset($request->year)) {
                    $instance->where('year', 'like', "%{$request->get('year')}%");
                }
                if (isset($request->color)) {
                    $instance->where('color', 'like', "%{$request->get('color')}%");
                }
                if (isset($request->user)) {
                    $full_names = explode(" ", $request->get('user'));
                    $instance->whereHas('user', function ($where) use ($request, $full_names) {
                        $where->where('first_name', $full_names);
                        $where->orWhere(function ($query) use ($full_names) {
                            $query->whereIn('last_name', $full_names);
                        });
                    });
                }
            })
            ->rawColumns(['edit'])
            ->toJson();
    }

    /**
     * attach vehicle to vendor
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function attachVehicleWithVendor(Request $request)
    {
        $vehicle = $this->vehicleService->find($request->attach_vehicle_id);
        if (empty($vehicle['result'])) {
            return response()->json(['status' => false, 'message' => 'Vehicle Not found']);
        }
        $vehicleVendor = $this->vehicleVendorService->updateOrCreate(
            ['vehicle_id' => $request->attach_vehicle_id, 'vendor_id' => $request->vendor_id],
            [
                'vehicle_id' => $request->attach_vehicle_id,
                'vendor_id' => $request->vendor_id
            ]);
        if ($vehicleVendor['bool']) {
            // user activity logs
            callUserActivityLogs("Attach Vehicle with Vendor", __FUNCTION__, __FILE__, __DIR__, true, "Connect Vendor successfully");

            $response = ['status' => true, 'message' => 'Connect Vendor successfully', 'redirect' => route('admin.vehicles.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Attach Vehicle with Vendor", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    //to edit vehicle
    public function edit($id)
    {
        $vehicle = $this->vehicleService->find($id);
        if (!$vehicle['bool']) {
            abort(500);
        }
        if (!$vehicle['result']) {
            abort(404);
        }
        $data['vehicle'] = $vehicle['result'];
        return view($this->folderLink . 'edit', $data);
    }

    //to update vehicle
    public function update(Request $request, $id)
    {
        $validation = [
            'license_plate' => 'required'
        ];
        $messages = [
            'license_plate.required' => 'Please enter license plate'
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $vehicleData = $request->all();
        $vehicle = $this->vehicleService->update($vehicleData, $id);
        if ($vehicle['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Vehicle", __FUNCTION__, __FILE__, __DIR__, true, "Edit Successfully");

            $response = ['status' => true, 'message' => 'Edit Successfully', 'redirect' => route('admin.vehicles.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Vehicle", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }
}
