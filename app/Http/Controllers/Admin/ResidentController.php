<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ImporttResidents;
use App\Models\BannedCommunityUser;
use App\Models\Community;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use App\Models\CsvImport;
use App\Models\House;
use App\Models\ResidentCsv;
use App\Models\ResidentCsvFile;
use App\Models\User;
use App\Models\UserAdditionalContact;
use App\Rules\ValidateName;
use App\Services\CommunityService;
use App\Services\HouseService;
use App\Services\UserContactService;
use App\Services\UserService;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\Store;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Database;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ResidentsSkippedRecordsExports;
use App\Mail\SendResidentImportFileGeneration;
use PHPUnit\Framework\Constraint\Count;
use Illuminate\Support\Facades\Log;
use Throwable;


class ResidentController extends Controller
{
    public $userService;
    public $communityService;
    public $houseService;
    public $userContactService;
    public $database;
    protected $folderLink = 'admin.residents.';

    public function __construct(
        UserService        $userService,
        CommunityService   $communityService,
        HouseService       $houseService,
        UserContactService $userContactService
    )
    {
        $this->userService = $userService;
        $this->communityService = $communityService;
        $this->houseService = $houseService;
        $this->userContactService = $userContactService;
    }

    public static function readImportedFile($filePath = array())
    {
        $HEADER = 'first_name,last_name,email,phone_number,house_name,head_of_family,special_instructions';
        $fail_message = '';
        $header = null;
        $csv = array();
        $row = 1;
        $imp_error = 0;

        $communityId = $filePath[1];
        $adminEmail = $filePath[2];

        $filePath = Storage::path($filePath[0]);

        $handle = fopen($filePath, "r");

        $phoneNumberIndex;

        while (($data = fgetcsv($handle, 5000, ',')) !== false) {
            if ($row == 1) {
                $first_row = array_diff($data, explode(",", $HEADER));
                if (count($first_row) == 1) {
                    $imp_error = 1;
                    $fail_message .= "<strong>Error!</strong> Invalid column name somewhere in your CSV file.";
                    $valid_file = 0;
                    return $fail_message;
                }

                $phoneNumberIndex = array_search('phone_number', $data);
            }

            if (!$header) {
                $header = $data;
            } else {
                $data[$phoneNumberIndex] = (!empty($data[$phoneNumberIndex])) ? removeSpecialCharactersFromPhoneNumber($data[$phoneNumberIndex]) : ""; //phone number validation
                $csv[] = array_combine($header, $data);
            }

            $row++;
        }
        fclose($handle);

        $residentArr = $csv;

        $csvSkippedRows = [
        ];

        // Success Insert the Csv data into the database
        for ($i = 0; $i < count($residentArr); $i++) {

            $rowError = false;
            $csvError = '';
            $link = '';
            $comma = false;

            //if empty first name
            if (empty($residentArr[$i]['first_name'])) {
                $rowError = true;
                $csvError .= 'blank first name';
                $comma = true;
            }

            //if empty last name
            if (empty($residentArr[$i]['last_name'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank last name' : $csvError .= 'blank last name';
                $comma = true;
            }

            //if empty email
            if (empty($residentArr[$i]['email'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank email address' : $csvError .= 'blank email address';
                $comma = true;
            }

            //if empty phone number
            if (empty($residentArr[$i]['phone_number'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank phone number' : $csvError .= 'blank phone number';
                $comma = true;
            }

            //if house name not exist
            if (empty($residentArr[$i]['house_name'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank house name' : $csvError .= 'blank house name';
                $comma = true;
            }

            //if phone number exist
            if (!empty($residentArr[$i]['phone_number'])) {
                $phone_number = $residentArr[$i]['phone_number'];

                // number formatting
                $phone_format = $residentArr[$i]['phone_number'];

                $user = User::where('phone_number', $phone_number)->first();
                if (!empty($user)) {

                    $rowError = true;
                    $comma ? $csvError .= ', phone number exist' : $csvError .= 'phone number exist';
                    $comma = true;

                    $bannedCommunityUser = BannedCommunityUser::where('user_id', $user->id)->first();

                    if (!empty($bannedCommunityUser)) // ban guest
                    {
                        if ($user->role_id == 7) {
                        }
                    } else if ($user->role_id == 7) // guest
                    {
                        $link = url('admin/user/guests?userId=' . $user->id);
                    } else if ($user->role_id == 2) // community admin
                    {
                        $link = url('admin/user/community-admins?userId=' . $user->id);
                    } else if ($user->role_id == 8) // for guard
                    {
                        $link = url('admin/user/guards?userId=' . $user->id);
                    } else if ($user->role_id == 11) // for guard admin
                    {
                        $link = url('admin/user/guards?userId=' . $user->id);
                    } else if ($user->role_id == 4) // for house head
                    {
                        $link = url('admin/residential/residents?userId=' . $user->id);
                    } else if ($user->role_id == 5) // for house member
                    {
                        $link = url('admin/residential/residents?userId=' . $user->id);
                    }

                }
            }

            //if wrong email given
            if (!empty($residentArr[$i]['email'])) {
                if (filter_var($residentArr[$i]['email'], FILTER_VALIDATE_EMAIL)) {
                } else {
                    $rowError = true;
                    $comma ? $csvError .= ', wrong email address' : $csvError .= 'wrong email address';
                    $comma = true;
                }
            }

            $assignRoleId;

            //
            if (!$rowError) {
                $house = House::where(['community_id' => $communityId, 'house_name' => $residentArr[$i]['house_name']])->first();
                if (empty($house)) {
                    $rowError = true;
                    $comma ? $csvError .= ', house name does not exist in community' : $csvError .= 'house name does not exist in community';
                    $comma = true;
                } else {
                    if (!empty($residentArr[$i]['head_of_family'])) //if head of family is not empty
                    {
                        if ($residentArr[$i]['head_of_family'] == '1') {
                            $headOfFamily = User::where(['community_id' => $communityId, 'house_id' => $house->id, 'role_id' => 4])->first();
                            if (!empty($headOfFamily)) //if head of family exist
                            {
                                $assignRoleId = 5;
                            } else //if head of family does not exist
                            {
                                $assignRoleId = 4;
                            }
                        } else {
                            $assignRoleId = 5;
                        }
                    } else //if head of family given blank
                    {
                        $assignRoleId = 5;
                    }
                }
            }

            if (!$rowError)// if there's no error in row
            {
                $randomPassword = generateRandomNumber();
                $password = bcrypt($randomPassword);

                $house = House::where(['community_id' => $communityId, 'house_name' => $residentArr[$i]['house_name']])->first();

                $userData = [
                    'first_name' => $residentArr[$i]['first_name'],
                    //'middle_name' => $residentArr[$i]['middle_name'],
                    'last_name' => $residentArr[$i]['last_name'],
                    'email' => $residentArr[$i]['email'],
                    'dial_code' => '+1',
                    'phone_number' => $phone_number,
                    'created_by' => auth()->id(),
                    'community_id' => $communityId,
                    'house_id' => $house->id,
                    'role_id' => $assignRoleId, //set user role ( head of family / house hold resident )
                    'special_instruction' => $residentArr[$i]['special_instructions'],
                    'password' => $password,
                    'temp_password' => $randomPassword,
                    'is_reset_password' => '1',
                ];

                $user = User::create($userData);

                $headdOfFamily = '';
                //
                if ($user->role_id == 4) {
                    $headdOfFamily = 'Household Head';
                } else if ($user->role_id == 5) {
                    $headdOfFamily = 'Household member';
                }

                $csvError = "Success";

                $skipRow = array(
                    'first_name' => $residentArr[$i]['first_name'],
                    'last_name' => $residentArr[$i]['last_name'],
                    'email' => $residentArr[$i]['email'],
                    'phone_number' => $residentArr[$i]['phone_number'],
                    'house_name' => $residentArr[$i]['house_name'],
                    'head_of_family' => $headdOfFamily,
                    'special_instructions' => $residentArr[$i]['special_instructions'],
                    'temp_password' => $randomPassword,
                    'status' => $csvError,
                    'message' => 'Record imported successfully'
                );

                array_push($csvSkippedRows, $skipRow);

                if ($user) {
                    $emailData['user'] = $user;
                    $emailData['type'] = 'resident';

                    try {
                    } catch (Throwable $e) {
                    }
                }

                //assign permissions
                if ($user->hasRole('family_head')) {
                    $user->assignPermission('can_send_passes');
                    $user->assignPermission('can_manage_family');
                    $user->assignPermission('allow_parental_control');
                } else {
                    $user->assignPermission('can_send_passes');
                }

            } else {

                $skipRow = array(
                    'first_name' => $residentArr[$i]['first_name'],
                    'last_name' => $residentArr[$i]['last_name'],
                    'email' => $residentArr[$i]['email'],
                    'phone_number' => $residentArr[$i]['phone_number'],
                    'house_name' => $residentArr[$i]['house_name'],
                    'head_of_family' => '',
                    'special_instructions' => $residentArr[$i]['special_instructions'],
                    'temp_password' => '',
                    'status' => 'Failure',
                    'message' => $csvError
                );

                array_push($csvSkippedRows, $skipRow);
            }
        }

        if (!Storage::exists('/storage/app/public/csvfilesresult')) {
            Storage::makeDirectory('/storage/app/public/csvfilesresult', 0775, true); //creates directory
        }

        $fileName = "resident_success_skip" . time() . ".txt";
        $successSkippedFile = "/storage/app/public/csvfilesresult/" . $fileName;

        $handle = fopen("storage/app/storage/app/public/csvfilesresult/" . $fileName, 'w');


        foreach ($csvSkippedRows as $user) {
            fputcsv($handle, [
                $user['first_name'],
                $user['last_name'],
                $user['email'],
                $user['phone_number'],
                $user['house_name'],
                $user['head_of_family'],
                $user['special_instructions'],
                $user['temp_password'],
                $user['status'],
                $user['message']
            ]);
        }
        fclose($handle);

        //email
        $emailData['fileName'] = $fileName;
        $emailData['email'] = 'abc';
        $emailData['csvBelongsTo'] = 'residents';
        $emailData['subject'] = 'Resident Import File Generation';

        CsvImport::create([
            'email' => $adminEmail,
            'type' => 'resident-import',
            'file_name' => $fileName
        ]);

        Mail::to($adminEmail)->send(new SendResidentImportFileGeneration($emailData));


    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data["houseId"] = $request->houseId ?? "";
        $data["userId"] = $request->userId ?? "";
        $data["phoneNumber"] = $request->phoneNumber ?? "";
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get residents list
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function residentData(Request $request)
    {

        $houseId = null;
        $userId = null;
        $phoneNumber = null;
        $is_suspended = null;
        $complete_profile = null;
        if ($request->has('houseId') && !empty($request->houseId)) {
            $houseId = $request->houseId;
        }
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        if ($request->has('phoneNumber') && !empty($request->phoneNumber)) {
            $phoneNumber = $request->phoneNumber;
        }
        if ($request->has('is_suspended') && !empty($request->is_suspended)) {
            $is_suspended = $request->is_suspended;
        }
        if ($request->has('complete_profile') && !empty($request->complete_profile)) {
            $complete_profile = $request->complete_profile;
        }

        $residentsData = $this->userService->getResidentList(getCommunityIdByUser(), $houseId, $userId, $phoneNumber, $is_suspended, $complete_profile)['result'];

        $residentsData = datatables()->of($residentsData)
            ->addColumn('full_name', function ($model) {
                return $model->fullname;
            })
            ->addColumn('formatted_phone', function ($model) {
                return numberFormat($model->phone_number, $model->dial_code);
            })
            ->addColumn('special_instruction', function ($model) {
                return (!empty($model->special_instruction)) ? $model->special_instruction : "-";
            })
            ->addColumn('house_detail', function ($model) {
                return (!empty($model->house_detail)) ? $model->house_detail : "-";
            })
            ->addColumn('role_name', function ($model) {
                return $model->role->name ?? "";
            })
            ->addColumn('is_completed_profile', function ($model) {
                return (
                !empty($model->license_image)
                ) ? '<span class="badge badge-success" style="width: 43px;">Yes</span>' : '<span class="badge badge-warning" style="width: 43px;">No</span>';
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "";
            })
            ->addColumn('links', function ($model) {
                $links = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if (auth()->user()->hasRole('guard_admin')) {

                    $links .= '<!--<a class="dropdown-item guard-permission-model-list" href="' . route('admin.residential.approve-list.show', [$model->id]) . '"><i class="fa fa-lock" aria-hidden="true"></i> Pre-approved guest list</a>-->
                                        <a class="dropdown-item" href="' . route('admin.pre-approved.index', ["userId" => $model->id]) . '"><i class="fa fa-check" aria-hidden="true"></i> Pre Approved </a>';
                }

                if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('sub_admin')) {
                    $links .= '<form action="' . route('admin.residential.resend-temporary-password', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-key" aria-hidden="true"></i> Resend Temporary Password</button>
                                </form>';

                    $links .= '<a class="dropdown-item guard-permission-model" data-id="' . $model->id . '"  href="javascript:void(0)"><i class="fa fa-lock" aria-hidden="true"></i> Permission</a>';
                    $suspendListItemHtml = '';
                    if ($model->is_suspended == '0') {
                        $suspendListItemHtml = 'Suspend Resident';
                    } else {
                        $suspendListItemHtml = 'Unsuspend Resident';
                    }
                    $links .= '<a class="dropdown-item suspend-resident-model" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="fa fa-user" aria-hidden="true"></i> ' . $suspendListItemHtml . '</a>';
                    if (auth()->user()->role_id != 11) {
                        //role 11 is guard admin
                        $links .= '<a class="dropdown-item" href="' . route('admin.passes.index', ['userId' => $model->id]) . '"><i class="nav-icon fas fa-ticket-alt" aria-hidden="true"></i> Passes </a>';
                    }
                    $links .= '<a class="dropdown-item" href="' . route('admin.guest-logs.index', ['userId' => $model->id]) . '"><i class="nav-icon fas fa-ticket-alt" aria-hidden="true"></i> Guest Logs </a>';
                    if (auth()->user()->role_id != 11) {
                        //role 11 is guard admin
                    }
                    $links .= '<a class="dropdown-item revoke-permission-model" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="fa fa-user" aria-hidden="true"></i> Revoke Resident</a>
                                    <a class="dropdown-item" href="' . route('admin.residential.residents.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                                    <a class="dropdown-item" href="' . route('admin.rfid.rfids.index', ['userId' => $model->id]) . '"><i class="far fa-circle nav-icon" aria-hidden="true"></i> RFID </a>
                                    <a class="dropdown-item" href="' . route('admin.rfid.rfid-logs.index', ['userId' => $model->id]) . '"><i class="far fa-circle nav-icon" aria-hidden="true"></i> RFID Tracking </a>
                                    <a class="dropdown-item" href="' . route('admin.pre-approved.index', ["userId" => $model->id]) . '"><i class="fa fa-check" aria-hidden="true"></i> Pre Approved </a>';
                    if (auth()->user()->role_id != 11) {
                        //role 11 is guard admin
                        $links .= '<a class="dropdown-item" href="' . route('admin.traffic-tickets.resident-tickets.index') . '?user_id=' . $model->id . '"><i class="fa fa-ticket-alt" aria-hidden="true"></i> TL Tickets </a>
                                    <a class="dropdown-item" href="' . route('admin.traffic-payment-logs.index') . '?user_id=' . $model->id . '"><i class="fa fa-ticket-alt" aria-hidden="true"></i> TL Payments </a>';
                    }
                }
                if (auth()->user()->hasRole('super_admin')) {
                    $links .= '<form action="' . route('admin.residential.delete-resident', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>';
                }

                $links .= '</div>
                        </div>';
                return $links;
            })
            ->filter(function ($instance) use ($request) {

                if (isset($request->full_name)) {

                    $instance->where(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $request->get('full_name') . '%');
                }
                if (isset($request->formatted_phone)) {
                    $formatted_phone = onlyPhoneDigits($request->get('formatted_phone'));
                    $instance->where('phone_number', 'like', "%{$formatted_phone}%");
                }
                if (isset($request->email)) {
                    $instance->where('email', 'like', "%{$request->get('email')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
                if (isset($request->house_name)) {
                    $instance->whereHas('house', function ($q) use ($request) {
                        $q->where('house_name', 'like', "%{$request->get('house_name')}%");
                    });
                }
                if (isset($request->full_address)) {
                    $instance->whereHas('house', function ($q) use ($request) {
                        $q->where('house_detail', 'like', "%{$request->get('full_address')}%");
                    });
                }
                if (isset($request->special_instruction)) {
                    $instance->where('special_instruction', 'like', "%{$request->get('special_instruction')}%");
                }
            })
            ->rawColumns(['links', 'is_completed_profile'])
            ->toJson();

        return $residentsData;
    }

    /**
     * delete resident
     * @param Request $request
     * @param $userId
     */
    public function deleteResident(Request $request, $userId)
    {
        $houseId = $this->userService->find($userId)['result']->house_id;
        $user = $this->userService->deleteUser($userId);

        $model = User::find($userId);

        if ($model) {
            $model->forceDelete();
        }

        $houseCompletedProfileStatus = "no";
        $houseResidents = User::where('house_id', $houseId)->whereIn('role_id', [4, 5])->get();
        foreach ($houseResidents as $houseResident) {
            if (!empty($houseResident->license_image)) {
                $houseCompletedProfileStatus = "yes";
            }
        }

        $this->houseService->update(['completed_profile' => $houseCompletedProfileStatus], $houseId);

        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Delete Resident", __FUNCTION__, __FILE__, __DIR__, true, "Purged Successfully");

            $response = ['status' => true, 'message' => 'Purged Successfully', 'redirect' => route('admin.residential.residents.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete Resident", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $residentData = $request->all();

        $additional_phone_array = [
            $residentData['additional_phone_id1'] ?? 0,
            $residentData['additional_phone_dial1'] ?? 0,
            $residentData['additional_phone_number1'] ?? '',
            $residentData['additional_phone_number1_type'] ?? '',
            $residentData['additional_phone_id2'] ?? 0,
            $residentData['additional_phone_dial2'] ?? 0,
            $residentData['additional_phone_number2'] ?? '',
            $residentData['additional_phone_number2_type'] ?? '',
            $residentData['additional_phone_id3'] ?? 0,
            $residentData['additional_phone_dial3'] ?? 0,
            $residentData['additional_phone_number3'] ?? '',
            $residentData['additional_phone_number3_type'] ?? ''
        ];

        $additional_contact_update = $this->userService->updateAdditionalContacts($additional_phone_array, $id);
        $residentData = Arr::except($request->all(), ['profile_image', 'license_image']);

        $residentData['phone_number'] = trim(str_replace("-", "", $residentData['phone_number']));
        $validation = [
            'first_name' => ['required', new ValidateName],
            'last_name' => ['required', new ValidateName],
            'email' => 'required|email',
            'phone_number' => 'required|unique:users,phone_number,' . $id,
            'community_id' => 'required',
            'house_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
            'house_id.required' => 'Please select house'
        ];
        $validator = Validator::make($residentData, $validation, $messages);

        $dialCode = str_replace("+", "", $residentData['dial_code']);
        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();


        $phoneNumberDigits = 10;

        if (!empty($countryPhoneFormat)) {
            $phoneNumberDigits = $countryPhoneFormat->digit;
        }

        $additionPhoneNumber1Digits = 10;

        if (!empty($residentData['additional_phone_number1'])) {
            $residentData['additional_phone_dial1'] = str_replace("+", "", $residentData['additional_phone_dial1']);

            $additionalPhoneNumber1Format = CountryPhoneFormat::where('dial_code', $residentData['additional_phone_dial1'])->first();

            if (!empty($additionalPhoneNumber1Format)) {
                $additionPhoneNumber1Digits = $additionalPhoneNumber1Format->digit;
            } else {
                $additionPhoneNumber1Digits = 10;
            }

        }

        $additionPhoneNumber2Digits = 10;

        if (!empty($residentData['additional_phone_number2'])) {
            $residentData['additional_phone_dial2'] = str_replace("+", "", $residentData['additional_phone_dial2']);

            $additionalPhoneNumber2Format = CountryPhoneFormat::where('dial_code', $residentData['additional_phone_dial2'])->first();

            if (!empty($additionalPhoneNumber2Format)) {
                $additionPhoneNumber2Digits = $additionalPhoneNumber2Format->digit;
            } else {
                $additionPhoneNumber2Digits = 10;
            }

        }

        $validator->after(function ($validator) use ($residentData, $phoneNumberDigits) {

            if (isset($residentData['phone_number']) && preg_match_all("/[0-9]/", $residentData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }
        });
        $validator->after(function ($validator) use ($residentData, $additionPhoneNumber1Digits) {

            if (isset($residentData['additional_phone_number1']) && preg_match_all("/[0-9]/", $residentData['additional_phone_number1']) < $additionPhoneNumber1Digits) {
                $validator->errors()->add('additional_phone_number1', 'Phone number must be at least ' . $additionPhoneNumber1Digits . ' digit long');
            }
        });
        $validator->after(function ($validator) use ($residentData, $additionPhoneNumber2Digits) {

            if (isset($residentData['additional_phone_number2']) && preg_match_all("/[0-9]/", $residentData['additional_phone_number2']) < $additionPhoneNumber2Digits) {
                $validator->errors()->add('additional_phone_number2', 'Phone number must be at least ' . $additionPhoneNumber2Digits . ' digit long');
            }
        });
        $validator->after(function ($validator) use ($residentData, $phoneNumberDigits) {

            if (isset($residentData['additional_phone_number3']) && preg_match_all("/[0-9]/", $residentData['additional_phone_number3']) < $phoneNumberDigits) {
                $validator->errors()->add('additional_phone_number3', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }

        if ($request->is_head_of_family == '1') { // head of family
            $residentData['role_id'] = 4;
        } else { // house hold resident
            $residentData['role_id'] = 5;
        }
        //upload profile image
        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_' . time() . '.' . $request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $residentData['profile_image'] = $fileName;
        }
        //upload license image to s3 bucket
        if ($request->hasFile('license_image')) {
            $path = Storage::disk('s3')->put("dynamic/lic_images", $request->file('license_image'));
            $residentData['license_image'] = $path;
        }

        $community = $this->communityService->find($residentData['community_id']);
        $residentData['community_name'] = $community['result']->name;

        $resident = $this->userService->update($residentData, $id);
        if ($resident['bool']) {
            $user = $this->userService->find($id)['result'];

            $houseCompletedProfileStatus = "no";
            $houseResidents = User::where('house_id', $residentData['house_id'])->whereIn('role_id', [4, 5])->get();
            foreach ($houseResidents as $houseResident) {
                if (!empty($houseResident->license_image)) {
                    $houseCompletedProfileStatus = "yes";
                }
            }

            $house = House::find($residentData['house_id']);

            $house->completed_profile = $houseCompletedProfileStatus;
            $house->save();

            //head of family rights
            if ($user->hasRole('family_head')) {
                $user->assignPermission('can_send_passes');
                $user->assignPermission('can_manage_family');
                $user->assignPermission('allow_parental_control');
            }

            // user activity logs
            callUserActivityLogs("Edit Resident", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.residential.residents.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Resident", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * check resident permissions
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function checkResidentPermission(Request $request)
    {
        $user = $this->userService->find($request->id)['result'];
        if ($user) {
            $permission['canManageFamily'] = $user->hasPermission('can_manage_family');
            $permission['canSendPasses'] = $user->hasPermission('can_send_passes');
            $permission['allowParentalControl'] = $user->hasPermission('allow_parental_control');
            $permission['isLicenseLocked'] = $user->hasPermission('is_license_locked');
            $response = ['status' => true, 'permission' => $permission];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    /**
     * assign resident permissions
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function assignResidentPermission(Request $request)
    {
        $userId = $request->user_id;
        $user = $this->userService->find($userId)['result'];

        //assign permission can manage family
        if ($request->has('can_manage_family') && $request->can_manage_family == '1') {
            $user->assignPermission('can_manage_family');
        } else {
            $user->revokePermission('can_manage_family');
        }
        //assign permission can send passes
        if ($request->has('can_send_passes') && $request->can_send_passes == '1') {
            $user->assignPermission('can_send_passes');
        } else {
            $user->revokePermission('can_send_passes');
        }

        //assign permission allow parental control
        if ($request->has('allow_parental_control') && $request->allow_parental_control == '1') {
            $user->assignPermission('allow_parental_control');
        } else {
            $user->revokePermission('allow_parental_control');
        }

        //assign permission is license locked
        if ($request->has('is_license_locked') && $request->is_license_locked == '1') {
            $user->assignPermission('is_license_locked');
        } else {
            $user->revokePermission('is_license_locked');
        }

        // user activity logs
        callUserActivityLogs("Resident Permissions", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

        return response()->json(['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.residential.residents.index')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $residentData = $request->all();

        $additional_phone_array = [
            $residentData['additional_phone_number1_dial'],
            $residentData['additional_phone_number1'],
            $residentData['additional_phone_number1_type'],
            $residentData['additional_phone_number2_dial'],
            $residentData['additional_phone_number2'],
            $residentData['additional_phone_number2_type'],
        ];

        $residentData['phone_number'] = trim(str_replace("-", "", $residentData['phone_number']));
        $validation = [
            'first_name' => ['required', new ValidateName],
            'last_name' => ['required', new ValidateName],
            'email' => 'required|email',
            'phone_number' => 'required',
            'community_id' => 'required',
            'house_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'community_id.required' => 'Please select community',
            'house_id.required' => 'Please select house'
        ];
        $validator = Validator::make($residentData, $validation, $messages);
        $validator->after(function ($validator) use ($residentData) {
            $phoneNumber = $residentData['phone_number'];
            $email = $residentData['email'];
            $community_id = auth()->user()->community_id;

            $dialCode = str_replace("+", "", $residentData['dial_code']);
            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
            $phoneNumberDigits = 10;

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            $residentData['phone_number'] = str_replace("-", "", $residentData['phone_number']);
            $residentData['phone_number'] = str_replace("_", "", $residentData['phone_number']);

            //check if phone number length less than 10 then show error message
            if (isset($residentData['phone_number']) && preg_match_all("/[0-9]/", $residentData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }

            $additionalPhoneNUmber1DialCode = str_replace("+", "", $residentData["additional_phone_number1_dial"]);
            $countryPhoneFormatAddPhnNum1 = CountryPhoneFormat::where('dial_code', $additionalPhoneNUmber1DialCode)->first();

            $addPhnNum1Dgts = 10;

            if (!empty($countryPhoneFormatAddPhnNum1)) {
                $addPhnNum1Dgts = $countryPhoneFormatAddPhnNum1->digit;
            }

            $additionalPhoneNUmber2DialCode = str_replace("+", "", $residentData["additional_phone_number2_dial"]);
            $countryPhoneFormatAddPhnNum2 = Country::where('dial_code', $additionalPhoneNUmber2DialCode)->first();

            $addPhnNum2Dgts = 10;

            if (!empty($countryPhoneFormatAddPhnNum2)) {
                $addPhnNum2Dgts = $countryPhoneFormatAddPhnNum2->digit;
            }


            if (isset($residentData['additional_phone_number1']) && $residentData['additional_phone_number1'] != null && preg_match_all("/[0-9]/", $residentData['additional_phone_number1']) < $addPhnNum1Dgts) {
                $validator->errors()->add('additional_phone_number1', 'Additional Phone number must be at least ' . $addPhnNum1Dgts . ' digit long');
            }
            if (isset($residentData['additional_phone_number2']) && $residentData['additional_phone_number2'] != null && preg_match_all("/[0-9]/", $residentData['additional_phone_number2']) < $addPhnNum2Dgts) {
                $validator->errors()->add('additional_phone_number2', 'Additional Phone number must be at least ' . $addPhnNum2Dgts . ' digit long');
            }
            if (isset($residentData['additional_phone_number3']) && $residentData['additional_phone_number3'] != null && preg_match_all("/[0-9]/", $residentData['additional_phone_number3']) < $phoneNumberDigits) {
                $validator->errors()->add('additional_phone_number3', 'Additional Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }
            if (!empty($residentData['phone_number'])) {
                $userByPhone = $this->userService->findByPhone($phoneNumber)['result'];
                $userByCommunityId = $this->userService->getUserByCommunityNew($community_id, $phoneNumber)['result'];


                if ($userByPhone) {
                    $phoneNumberMessage = 'The phone number has already been taken';
                    if ($userByCommunityId == null) {
                        if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('sub_admin')) {
                            $url = generateUrlByUserRoleId($userByPhone);
                            $phoneNumberMessage = '<a href="' . $url . '" target="_blank">The phone number has already been taken.</a>';
                        }
                        $validator->errors()->add('phone_number', $phoneNumberMessage);

                    } else {
                        if (auth()->user()->hasRole('super_admin') || auth()->user()->hasRole('sub_admin')) {
                            $url = generateUrlByUserRoleId($userByPhone);
                            $phoneNumberMessage = '<a href="' . $url . '" target="_blank">The phone number has already been taken.</a>';
                        }
                        $validator->errors()->add('phone_number', $phoneNumberMessage);
                    }

                }
            }

        });
        if ($validator->fails()) {
            $validator->validate();
        }
        $randomPassword = generateRandomNumber();
        $residentData['password'] = bcrypt($randomPassword);
        $residentData['temp_password'] = $randomPassword;
        $residentData['created_by'] = auth()->user()->id;

        if ($request->is_head_of_family == '1') { // head of family
            $isHeadOfFamily = $this->userService->getHeadOfFamily($residentData['community_id'], $residentData['house_id']);
            if (!empty($isHeadOfFamily['result'])) {
                $residentData['role_id'] = 5;
            } else {
                $residentData['role_id'] = 4;
            }
        } else { // house hold resident
            $residentData['role_id'] = 5;
        }
        //upload profile image
        if ($request->hasFile('profile_image')) {
            $fileName = 'profile_' . time() . '.' . $request->profile_image->extension();
            $request->profile_image->move(public_path('images/profile_images'), $fileName);
            $residentData['profile_image'] = $fileName;
        }
        //upload license image to s3 bucket
        if ($request->hasFile('license_image')) {
            $path = Storage::disk('s3')->put("dynamic/lic_images", $request->file('license_image'));
            $residentData['license_image'] = $path;
        }

        $residentData['is_reset_password'] = '1';
        $residentData['is_verified'] = '1';

        $resident = $this->userService->create($residentData);
        for ($i = 0; $i < 6; $i++) {
            if (($i == 1 || $i == 4) && $additional_phone_array[$i] != null) {
                $formatted_phone = str_replace("-", "", $additional_phone_array[$i]);
                $formatted_phone = str_replace("_", "", $formatted_phone);


                $type = $additional_phone_array[$i + 1] != 'Select' ? $additional_phone_array[$i + 1] : 'Work';
                $dial_code = $additional_phone_array[$i - 1];

                $createAdditionalContact = UserAdditionalContact::create(['user_id' => $resident['result']->id, 'contact' => $formatted_phone, 'type' => $type, 'dial_code' => $dial_code]);
            }
        }

        if ($resident['bool']) {
            //assign permissions
            if ($resident['result']->hasRole('family_head')) {
                $resident['result']->assignPermission('can_send_passes');
                $resident['result']->assignPermission('can_manage_family');
                $resident['result']->assignPermission('allow_parental_control');
            } else {
                $resident['result']->assignPermission('can_send_passes');
            }
            $emailData['user'] = $resident['result'];
            $emailData['type'] = 'resident';
            dynamicEmail($emailData, $resident['result']->email);

            $houseCompletedProfileStatus = "no";
            $houseResidents = User::where('house_id', $residentData['house_id'])->whereIn('role_id', [4, 5])->get();
            foreach ($houseResidents as $houseResident) {
                if (!empty($houseResident->license_image)) {
                    $houseCompletedProfileStatus = "yes";
                }
            }


            $house = $this->houseService->find($residentData['house_id']);
            $totalHouseMembers = intval($house['result']->total_house_members);
            $modifiedTotalHouseMembers = $totalHouseMembers + 1;
            $totalHouseMemberColumnLength = strlen($modifiedTotalHouseMembers);
            $zeroToAdd = 2 - $totalHouseMemberColumnLength;
            for ($i = 0; $i < $zeroToAdd; $i++) {
                $modifiedTotalHouseMembers = '0' . $modifiedTotalHouseMembers;
            }
            $house['result']->total_house_members = $modifiedTotalHouseMembers;
            $house['result']->completed_profile = $houseCompletedProfileStatus;
            $house['result']->save();

            // user activity logs
            callUserActivityLogs("Add Resident", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.residential.residents.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Resident", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $formClass = 'ajax-form-super-admin';
        if (auth()->user()->hasRole('sub_admin')) {
            $formClass = 'ajax-form';
        }
        $data['formClass'] = $formClass;
        $data['communities'] = getCommunities();

        $data['phoneFormat'] = "999-999-9999";

        $data['countries'] = activeCountries();
        $data['countriesForAdditionalPhoneNumbers'] = Country::where('active', 1)->get();

        if (!empty(auth()->user()->community_id)) {
            $dialCode = str_replace("+", "", auth()->user()->community->dial_code);

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
            if (!empty($countryPhoneFormat)) {
                $data['phoneFormat'] = $countryPhoneFormat->format;
            }
        }

        return view($this->folderLink . 'create', $data);
    }

    /**
     * get houses by community Id
     * @param Request $request
     * @return mixed
     */
    public function getHouseByCommunityId(Request $request)
    {

        $result = $this->houseService->getHousesByCommunityId($request->communityId)['result'];

        if ($result->isNotEmpty()) {
            $response = ['status' => true, 'result' => $result];
        } else {
            $response = ['status' => false];
        }
        return response($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['communities'] = getCommunities();

        $data['countries'] = activeCountries();

        $resident = $this->userService->find($id);
        $data['resident_id'] = $id;
        if (!$resident['bool']) {
            abort(500);
        }
        if (!$resident['result']) {
            abort(404);
        }
        $data['resident'] = $resident['result'];

        $data['additionalPhoneNumbersFormats'] = [];

        if (count($data['resident']->additionalPhone) > 0) {
            foreach ($data['resident']->additionalPhone as $add) {
                $dialCode = str_replace("+", "", $add->dial_code);
                $additionalPhoneNumberFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
                if (!empty($additionalPhoneNumberFormat)) {
                    array_push($data['additionalPhoneNumbersFormats'], $additionalPhoneNumberFormat->format);
                } else {
                    array_push($data['additionalPhoneNumbersFormats'], "999-999-9999");
                }
            }
        }

        $data['phoneFormat'] = "999-999-9999";

        $dialCode;

        !empty(auth()->user()->community_id) ? $dialCode = str_replace("+", "", auth()->user()->community->dial_code) : $dialCode = str_replace("+", "", $data['resident']->dial_code);

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
        if (!empty($countryPhoneFormat)) {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $this->userService->delete($id);
        $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.residential.residents.index')];
        return response()->json($response);
    }

    /**
     * Send temporary password in email.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function resendTemporaryPassword($id)
    {
        $resident = $this->userService->find($id);
        if ($resident['result']) {
            $randomPassword = generateRandomNumber();
            $residentData['password'] = bcrypt($randomPassword);
            $residentData['temp_password'] = $randomPassword;
            $residentData['is_reset_password'] = '1';
            $result = $this->userService->update($residentData, $resident['result']->id);
            if ($result['bool']) {
                $resident = $this->userService->find($id);
                $emailData['user'] = $resident['result'];
                $emailData['type'] = 'resident';
                dynamicEmail($emailData, $resident['result']->email);

                // user activity logs
                callUserActivityLogs("Resend Temporary Password", __FUNCTION__, __FILE__, __DIR__, true, "Success");

                $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.residential.residents.index')];
            } else {
                // user activity logs
                callUserActivityLogs("Resend Temporary Password", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

                $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
            }
        } else {
            // user activity logs
            callUserActivityLogs("Resend Temporary Password", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Revoke Resident
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function revokeResident(Request $request)
    {
        $user = $this->userService->find($request->user_id);

        if (!empty($request->remove_from_family)) {
            #Todo Need to review ['house_id'=>null, 'is_verified'=>0]
            $this->userService->update(['house_id' => null, 'is_verified' => 0, 'role_id' => 7], $request->user_id);
        }

        $user['result']->revokePermission('head_of_family');
        $user['result']->revokePermission('can_manage_family');
        $user['result']->revokePermission('can_send_passes');
        $user['result']->revokePermission('allow_parental_control');

        $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.residential.residents.index')];
        return response()->json($response);
    }

    public function houseResidents($id)
    {
        $data["houseId"] = $id;
        return view($this->folderLink . 'index', $data);
    }

    // For Csv upload

    /**
     * Save Import resident csv
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function importResident(Request $request)
    {
        $rules['resident_csv'] = 'required|file';
        $messages = array(
            'resident_csv.required' => 'Select a file to import.',
            'resident_csv.file' => 'Select a file to import.',
        );

        if (auth()->user()->role_id == '1') {
            $rules['community_id'] = 'required';
            $messages['community_id.required'] = 'Select community.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $validator->validate();


        }

        // Validation if wrong headers in csv
        $csv = $request->file('resident_csv');
        $filename = $csv->getRealPath();

        if (($handle = fopen($filename, 'r')) !== false) {

            $HEADER = 'first_name,last_name,email,phone_number,house_name,head_of_family,special_instructions,dial_code';
            $a = 0;

            while (($data = fgetcsv($handle, 5000, ',')) !== false) {

                if ($a == 0) {
                    $headerSent = '';

                    foreach ($data as $key => $value) {
                        $headerSent .= $value . ",";
                    }

                    $headerSent = chop($headerSent, ",");

                    $headerMatch = strpos($HEADER, $headerSent);

                    if (!$headerMatch) {
                    }

                    $headerSent = explode(",", $headerSent);
                    $HEADER = explode(",", $HEADER);

                    //if columns are less or more
                    if (count($headerSent) < 8) {
                        $fail_message = "<strong>Error!</strong> Wrong numbers of columns, extra or missing column in CSV file. " . count($headerSent);
                        $validator->after(function ($validator) use ($fail_message) {
                            $validator->errors()->add('resident_csv', $fail_message);
                        });
                        $validator->validate();
                    } else {
                        if (empty($headerSent[7])) {
                            $fail_message = "<strong>Error!</strong> Dial Code Can not be empty please add one.";
                            $validator->after(function ($validator) use ($fail_message) {
                                $validator->errors()->add('resident_csv', $fail_message);
                            });
                            $validator->validate();
                        }
                    }

                    $standardColumns = array("first_name", "last_name", "email", "phone_number", "house_name", "head_of_family", "special_instructions", "dial_code");

                    //if any wrong column given
                    if (!empty(array_diff($HEADER, $headerSent))) {
                        $fail_message = "<strong>Error!</strong> Invalid column given in your CSV file.";
                        $validator->after(function ($validator) use ($fail_message) {
                            $validator->errors()->add('resident_csv', $fail_message);
                        });
                        $validator->validate();
                    }

                }

                $a++;

            }
        }

//         make a directory  if not exist
        if (!Storage::exists('/storage/app/public/csvfiles')) {
            Storage::makeDirectory('/storage/app/public/csvfiles', 0775, true); //creates directory
        }

        $path = Storage::disk('local')->put("\storage\app\public\csvfiles", $request->file('resident_csv'));

        $path = str_replace('\\', '/', $path);

        $pathParts = explode("/", $path);

        $communityId;
        if (auth()->user()->role_id == '1') {
            $communityId = $request->community_id;
        } else if (auth()->user()->role_id == '2') {
            $communityId = auth()->user()->community_id;
        }

        $data = array($path, $communityId, auth()->user()->email);

        $handle = fopen($filename, "r");

        $a = 0;
        $totalRecords = 0;

        while (($data1 = fgetcsv($handle, 5000, ',')) !== false) {
            if ($a == 0) {

            } else {
                if ($data1[7] == "1" && strlen($data1[3]) == 10) {
                    ResidentCsv::create([
                        'first_name' => $data1[0],
                        'last_name' => $data1[1],
                        'email' => $data1[2],
                        'phone_number' => $data1[3],
                        'house_name' => $data1[4],
                        'head_of_family' => $data1[5],
                        'special_instructions' => $data1[6],
                        'community_id' => $communityId,
                        'file_name' => $pathParts[5],
                        'dial_code' => Str::start($data1[7], '+')
                    ]);
                }

                if ($data1[7] == "504" && strlen($data1[3]) == 8) {
                    ResidentCsv::create([
                        'first_name' => $data1[0],
                        'last_name' => $data1[1],
                        'email' => $data1[2],
                        'phone_number' => $data1[3],
                        'house_name' => $data1[4],
                        'head_of_family' => $data1[5],
                        'special_instructions' => $data1[6],
                        'community_id' => $communityId,
                        'file_name' => $pathParts[5],
                        'dial_code' => Str::start($data1[7], '+')
                    ]);
                }

                $totalRecords++;

            }
            $a++;
        }

        ResidentCsvFile::create([
            'file_name' => $pathParts[5],
            'total_records' => $totalRecords,
            'process_records' => 0,
            'community_id' => $communityId
        ]);


        return response()->json(['status' => true, 'message' => '', 'redirect' => route('admin.residential.resident-imported-file-saved-records', ['file_name' => $pathParts[5]])]);


    }

    // To read CSV File and Import Records

    function csvToArray($handle = '')
    {
        $HEADER = 'first_name,last_name,email,phone_number,house_name,head_of_family,special_instructions';
        $fail_message = '';
        $header = null;
        $csv = array();
        $row = 1;
        $imp_error = 0;

        while (($data = fgetcsv($handle, 10000, ',')) !== false) {
            if ($row == 1) {
                $first_row = array_diff($data, explode(",", $HEADER));
                if (count($first_row) == 1) {
                    $imp_error = 1;
                    $fail_message .= "<strong>Error!</strong> Invalid column name somewhere in your CSV file. ";
                    $valid_file = 0;
                    return $fail_message;
                }
            }

            if (!$header) {
                $header = $data;
            } else {
                $phone_number['formatted_phone_number'] = $data[3]; //formatted phone number
                $data[3] = (!empty($data[3])) ? removeSpecialCharactersFromPhoneNumber($data[3]) : ""; //phone number validation
                $csv[] = array_merge(array_combine($header, $data), $phone_number);
            }

            $row++;
        }
        fclose($handle);
        return $csv;
    }

    /**
     * get resident
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getResident(Request $request)
    {
        $user = $this->userService->find($request->id)['result'];
        if ($user) {
            $response = ['status' => true, 'user' => $user];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }


    /**
     * get resident
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function getResidentContact(Request $request)
    {
        $user = UserAdditionalContact::where('user_id', $request->id)->whereNull('deleted_at')->get();

        if ($user) {
            $response = ['status' => true, 'user' => $user];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    /**
     * suspend resident
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function suspendResident(Request $request)
    {
        $suspendValue = $request->suspend ? '0' : '1';
        $user = $this->userService->update(['is_suspended' => $suspendValue], $request->user_id)['result'];
        if ($request->suspend) {
            // user activity logs
            callUserActivityLogs("Suspend Resident", __FUNCTION__, __FILE__, __DIR__, true, "Unsuspended");

            return response()->json(['status' => true, 'message' => 'Unsuspended', 'redirect' => route('admin.residential.residents.index')]);
        }

        // user activity logs
        callUserActivityLogs("Suspend Resident", __FUNCTION__, __FILE__, __DIR__, true, "Suspended");

        return response()->json(['status' => true, 'message' => 'Suspended', 'redirect' => route('admin.residential.residents.index')]);
    }

    //generate skip house records
    function generateSkipResidentsRecords($skippedRowsArray)
    {
        return Excel::download(new ResidentsSkippedRecordsExports($skippedRowsArray), 'residents_success_skipped_records.csv');
    }


    /**
     * houses by address
     *
     * @param Request $request
     * @return JsonResponse
     */
    function houseByAddress(Request $request)
    {
        $communityId = $request->community_id;
        $houseAddress = $request->house_address;

        $house = $this->houseService->houseByAddress($communityId, $houseAddress)['result'];

        return response($house);
    }

    /**
     * imported files
     *
     * @param Request $request
     * @return JsonResponse
     */
    function importedFiles(Request $request)
    {
        $data['fileName'] = $request->file_name ? $request->file_name : '';
        return view($this->folderLink . 'imported-files', $data);
    }

    public function importedFileData(Request $request)
    {

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/csvfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        $residentCsvFile = ResidentCsvFile::with('community');
        auth()->user()->role_id == 2 ? $residentCsvFile = $residentCsvFile->where('community_id', auth()->user()->community_id) : "";
        $residentCsvFile = $residentCsvFile->get();

        return datatables()->of($residentCsvFile)
            ->addColumn('community', function ($model) {
                return $model->community->name;
            })
            ->editColumn('created_at', function ($model) {
                return !empty($model->created_at) ? $model->created_at : '';
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                               <a class="dropdown-item" href="' . route('admin.residential.resident-imported-file-saved-records', ['file_name' => $model->file_name]) . '"><i class="nav-icon fas fa-tasks" aria-hidden="true"></i> Process Records </a>
                               <a class="dropdown-item" href="' . route('admin.residential.resident-import-file-information', ['file_name' => $model->file_name]) . '"><i class="nav-icon fas fa-eye" aria-hidden="true"></i> View Records </a>
                               <form action="' . route('admin.residential.resident-csv-files-delete', [$model->id]) . '" method="POST" class="ajax-form">
                               ' . csrf_field() . '
                               <input type="hidden" name="id" value="' . $model->id . '">
                               <button class="dropdown-item" type="submit"><i class="fas fa-trash"></i> Delete</button>
                               </form>
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->make(true);

    }


    /**
     * function to delete imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteImportedFile(Request $request)
    {
        if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . $request->file_name)) {
            unlink(storage_path("app/storage/app/public/csvfilesresult") . "/" . $request->file_name);
            return response()->json(['status' => true, 'message' => 'Success! File deleted successfully', 'redirect' => route('admin.residential.imported-files')]);
        } else {
            return response()->json(['status' => false, 'message' => 'Failed! File not found', 'redirect' => route('admin.residential.imported-files')]);
        }
    }

    /**
     * function to download imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadImportedFile(Request $request)
    {

        $fileName = $request->file_name;

        $filepath = storage_path("app\storage\app\public\csvfilesresult\\" . $fileName);

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/csvfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . 'file.csv')) {
            unlink(storage_path("app/storage/app/public/csvfilesresult") . "/" . 'file.csv');
        }

        if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . $request->file_name)) {

            //creating file csv
            $fp = fopen(storage_path("app/storage/app/public/csvfilesresult") . "/" . 'file.csv', 'w');

            fputcsv($fp, [
                'first_name',
                'last_name',
                'email',
                'phone_number',
                'house_name',
                'head_of_family',
                'special_instructions',
                'temp_password',
                'status',
                'message'
            ]);

            foreach ($fileData as $dat) {
                fputcsv($fp, $dat, ",");
            }

            fclose($fp);

            $filepath = storage_path("app/storage/app/public/csvfilesresult/" . 'file.csv');

            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                die();
            }
        } else {
            return back()->with('fileNotFound', 'YES');
        }
    }

    /**
     * function to download imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadFailRecordsImportedFile(Request $request)
    {

        $fileName = $request->file_name;

        $filepath = storage_path("app\storage\app\public\csvfilesresult\\" . $fileName);

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/csvfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . 'failed_records_file.csv')) {
            unlink(storage_path("app/storage/app/public/csvfilesresult") . "/" . 'failed_records_file.csv');
        }

        if (Storage::disk('local')->exists('storage\app\public\csvfilesresult\\' . $request->file_name)) {
            //creating file csv
            $fp = fopen(storage_path("app/storage/app/public/csvfilesresult") . "/" . 'failed_records_file.csv', 'w');

            fputcsv($fp, [
                'first_name',
                'last_name',
                'email',
                'phone_number',
                'house_name',
                'head_of_family',
                'special_instructions',
                'temp_password',
                'status',
                'message'
            ]);


            foreach ($fileData as $dat) {
                if ($dat[8] == "Failure") {
                    fputcsv($fp, $dat, ",");
                }
            }

            fclose($fp);

            $filepath = storage_path("app/storage/app/public/csvfilesresult/" . 'failed_records_file.csv');

            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                die();
            }
        } else {
            return back()->with('fileNotFound', 'YES');
        }
    }


    //real time notification
    public function test1()
    {
        return view('test');
    }

    public function saveToken(Request $request)
    {
        auth()->user()->update(['fcm_web_token' => $request->token]);
        return response()->json(['token saved successfully.']);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function sendNotification(Request $request)
    {
        $firebaseToken = User::whereNotNull('fcm_web_token')->pluck('fcm_web_token')->all();

        $SERVER_API_KEY = 'AAAA6_xinjc:APA91bGnEPZMGuV0CtJj-GgsBLtX11iWMycq1x4qgJ0jzMz13Dgrs8yLSJoBuKfAN4ENWIJomVFTXI2GaD8Ne9DYlHgscWHuJkQDzLpuINLiK-refw4GY9yTtFAXL287WSUlQAfwbV-f';

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);


    }

    public function residentImportedFileSavedRecords(Request $request)
    {
        $data['fileName'] = $request->file_name ? $request->file_name : '';
        return view($this->folderLink . 'imported-file-saved-records', $data);
    }

    public function residentImportedFileSavedRecordsData(Request $request)
    {
        $residentCsvs = ResidentCsv::where('file_name', $request->file_name)->get();

        return datatables()->of($residentCsvs)
            ->addColumn('first_name', function ($model) {
                return $model->first_name;
            })
            ->addColumn('last_name', function ($model) {
                return $model->last_name;
            })
            ->addColumn('email', function ($model) {
                return $model->email;
            })
            ->addColumn('phone_number', function ($model) {
                return numberFormat($model->phone_number, $model->dial_code);
            })
            ->addColumn('house_name', function ($model) {
                return $model->house_name;
            })
            ->addColumn('head_of_family', function ($model) {
                return $model->head_of_family;
            })
            ->addColumn('special_instructions', function ($model) {
                return $model->special_instructions;
            })
            ->addColumn('temporary_password', function ($model) {
                return $model->temporary_password;
            })
            ->addColumn('status', function ($model) {
                return $model->status;
            })
            ->addColumn('message', function ($model) {
                return $model->message;
            })
            ->addColumn('links', function ($model) {

                $actionHTML = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if ($model->status == "failure") {
                    $actionHTML .= '<a class="dropdown-item" href="' . route('admin.residential.resident-csvs-table-record-edit', $model->id) . '"><i class="nav-icon fas fa-edit" aria-hidden="true"></i> Edit </a>';
                }

                $actionHTML .= '<form action="' . route('admin.residential.resident-csvs-delete', [$model->id]) . '" method="POST" class="ajax-form">
                                ' . csrf_field() . '
                                <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                                </form>
                                </div>
                                </div>';

                return $actionHTML;


            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('first_name') && !empty($request->first_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                        return $this::containsInsensitive($row['first_name'], $request->first_name);

                    });
                }
                if ($request->has('last_name') && !empty($request->last_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['last_name'], $request->last_name);
                    });
                }

                if ($request->has('email') && !empty($request->email)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['email'], $request->email);
                    });
                }
                if ($request->has('formatted_phone') && !empty($request->formatted_phone)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['phone_number'], $request->formatted_phone);

                    });
                }
                if ($request->has('house_name') && !empty($request->house_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['house_name'], $request->house_name);

                    });
                }
                if ($request->has('head_of_family') && !empty($request->head_of_family)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['head_of_family'], $request->head_of_family);

                    });
                }
                if ($request->has('special_instructions') && !empty($request->special_instructions)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['special_instructions'], $request->special_instructions);
                    });
                }
                if ($request->has('temporary_password') && !empty($request->temporary_password)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['temporary_password'], $request->temporary_password);
                    });
                }
                if ($request->has('status') && !empty($request->status)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['status'], $request->status);
                    });
                }
                if ($request->has('message') && !empty($request->message)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        $a = stripos($row['message'], $request->message);
                        if (!empty($a)) {
                            return $row['message'];
                        }
                    });
                }

            })
            ->rawColumns(['links'])
            ->make(true);
    }

    /**
     * Determine if a given string contains all array values.
     * Not case sensitive
     *
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public function containsInsensitive($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && mb_stripos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    public function saveResidentsFromResidentCsvsTable(Request $request)
    {
        $residentsCsvs = ResidentCsv::where(['file_name' => $request->file_name, 'mark' => '0'])->limit(10)->get();

        $count = 0;


        if (!empty($residentsCsvs)) {
            if (count($residentsCsvs) > 0) {
                $community = Community::find($residentsCsvs[0]->community_id);
                $dialCode = str_replace("+", "", $community->dial_code);
                $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();
            }

            foreach ($residentsCsvs as $residentsCsv) {
                $rowError = false;
                $csvError = '';
                $comma = false;

                //if empty first name
                if (empty($residentsCsv->first_name)) {
                    $rowError = true;
                    $csvError .= 'blank first name';
                    $comma = true;
                }

                //if empty last name
                if (empty($residentsCsv->last_name)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank last name' : $csvError .= 'blank last name';
                    $comma = true;
                }

                //if empty email
                if (empty($residentsCsv->email)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank email address' : $csvError .= 'blank email address';
                    $comma = true;
                }

                //if empty phone number
                if (empty($residentsCsv->phone_number)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank phone number' : $csvError .= 'blank phone number';
                    $comma = true;
                }

                //if house name not exist
                if (empty($residentsCsv->house_name)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank house name' : $csvError .= 'blank house name';
                    $comma = true;
                }


                $phoneNumber = $residentsCsv->phone_number;

                if (!$rowError) {

                    $dialCodeWithPlus = "+" . $community->dial_code;
                    $user = User::where(['dial_code' => $dialCodeWithPlus, 'phone_number' => $phoneNumber])->first();

                    if (!empty($user)) {

                        $rowError = true;
                        $comma ? $csvError .= ', phone number exist' : $csvError .= 'phone number exist';
                        $comma = true;

                        $bannedCommunityUser = BannedCommunityUser::where('user_id', $user->id)->first();

                    }
                }

                $additionalPhoneNumber1;
                $additionalPhoneNumber2;
                $additionalPhoneNumber3;

                //if wrong email given
                if (!empty($residentsCsv->email)) {
                    if (filter_var($residentsCsv->email, FILTER_VALIDATE_EMAIL)) {
                    } else {
                        $rowError = true;
                        $comma ? $csvError .= ', wrong email address' : $csvError .= 'wrong email address';
                        $comma = true;
                    }
                }

                $assignRoleId;
                $communityId = $residentsCsv->community_id;

                //
                if (!$rowError) {
                    $house = House::where(['community_id' => $residentsCsv->community_id, 'house_name' => $residentsCsv->house_name])->first();
                    if (empty($house)) {
                        $rowError = true;
                        $comma ? $csvError .= ', house name does not exist in community' : $csvError .= 'house name does not exist in community';
                        $comma = true;
                    } else {
                        if (!empty($residentsCsv->head_of_family)) //if head of family is not empty
                        {
                            if ($residentsCsv->head_of_family == '1') {
                                $headOfFamily = User::where(['community_id' => $residentsCsv->community_id, 'house_id' => $house->id, 'role_id' => 4])->first();
                                if (!empty($headOfFamily)) //if head of family exist
                                {
                                    $assignRoleId = 5;
                                } else //if head of family does not exist
                                {
                                    $assignRoleId = 4;
                                }
                            } else {
                                $assignRoleId = 5;
                            }
                        } else //if head of family given blank
                        {
                            $assignRoleId = 5;
                        }
                    }
                }

                $user_check = User::where(['dial_code' => $residentsCsv->dial_code, 'phone_number' => $phoneNumber])->first();

                if (!$rowError && empty($user_check)) // if there's no error in row and user exist
                {
                    $randomPassword = generateRandomNumber();
                    $password = bcrypt($randomPassword);

                    $house = House::where(['community_id' => $communityId, 'house_name' => $residentsCsv->house_name])->first();

                    $dialCodeWithPlus = $residentsCsv->dial_code;


                    $userData = [
                        'first_name' => $residentsCsv->first_name,
                        'last_name' => $residentsCsv->last_name,
                        'email' => $residentsCsv->email,
                        'dial_code' => $dialCodeWithPlus,
                        'phone_number' => $phoneNumber,
                        'created_by' => auth()->id(),
                        'community_id' => $communityId,
                        'house_id' => $house->id,
                        'role_id' => $assignRoleId, //set user role ( head of family / house hold resident )
                        'special_instruction' => $residentsCsv->special_instructions,
                        'password' => $password,
                        'temp_password' => $randomPassword,
                        'is_reset_password' => '1',
                    ];

                    $user = User::create($userData);

                    $types = array('Main', 'Home', 'Office', 'Work', 'Friend', 'Parents', 'Colleague', 'Relative', 'Siblings', 'School/College');

                    if (!empty($residentsCsv->additional_phone_number1)) {
                        $dialCode;

                        if (empty($residentsCsv->additional_phone_number1_dialcode)) {
                            $community = Community::find($residentsCsv->community_id);
                            $dialCode = str_replace("+", "", $community->dial_code);
                        } else {
                            $dialCode = str_replace("+", "", $residentsCsv->additional_phone_number1_dialcode);
                        }

                        $additionalPhoneNumber1Format = CountryPhoneFormat::where('dial_code', $dialCode)->first();

                        if (!empty($additionalPhoneNumber1Format)) {
                            $additionalPhoneNumber1Format->digit;

                            $additionalPhoneNumber1 = str_replace("(", "", $residentsCsv->additional_phone_number1);
                            $additionalPhoneNumber1 = str_replace(")", "", $additionalPhoneNumber1);
                            $additionalPhoneNumber1 = str_replace("-", "", $additionalPhoneNumber1);
                            $additionalPhoneNumber1 = str_replace(" ", "", $additionalPhoneNumber1);

                            if (strlen($additionalPhoneNumber1) != $additionalPhoneNumber1Format->digit) {

                            } else {
                                $typeFound = array_search($residentsCsv->additional_phone_number1_type, $types);

                                $additionalPhoneNumber1Type = $residentsCsv->additional_phone_number1_type;

                                if (!$typeFound) {
                                    $additionalPhoneNumber1Type = "Work";
                                }

                                UserAdditionalContact::create(['user_id' => $user->id, 'dial_code' => $dialCode, 'contact' => $additionalPhoneNumber1, 'type' => $additionalPhoneNumber1Type]);
                            }
                        }

                    }

                    if (!empty($residentsCsv->additional_phone_number2)) {
                        $dialCode;

                        if (empty($residentsCsv->additional_phone_number2_dialcode)) {
                            $community = Community::find($residentsCsv->community_id);
                            $dialCode = str_replace("+", "", $community->dial_code);
                        } else {
                            $dialCode = str_replace("+", "", $residentsCsv->additional_phone_number2_dialcode);
                        }

                        $additionalPhoneNumber2Format = CountryPhoneFormat::where('dial_code', $dialCode)->first();

                        if (!empty($additionalPhoneNumber2Format)) {
                            $additionalPhoneNumber2Format->digit;

                            $additionalPhoneNumber2 = str_replace("(", "", $residentsCsv->additional_phone_number2);
                            $additionalPhoneNumber2 = str_replace(")", "", $additionalPhoneNumber2);
                            $additionalPhoneNumber2 = str_replace("-", "", $additionalPhoneNumber2);
                            $additionalPhoneNumber2 = str_replace(" ", "", $additionalPhoneNumber2);

                            if (strlen($additionalPhoneNumber2) != $additionalPhoneNumber2Format->digit) {

                            } else {
                                $typeFound = array_search($residentsCsv->additional_phone_number2_type, $types);

                                $additionalPhoneNumber2Type = $residentsCsv->additional_phone_number2_type;

                                if (!$typeFound) {
                                    $additionalPhoneNumber2Type = "Work";
                                }

                                UserAdditionalContact::create(['user_id' => $user->id, 'dial_code' => $dialCode, 'contact' => $additionalPhoneNumber2, 'type' => $additionalPhoneNumber2Type]);
                            }

                        }

                    }

                    //assign permissions
                    if ($user->hasRole('family_head')) {
                        $user->assignPermission('can_send_passes');
                        $user->assignPermission('can_manage_family');
                        $user->assignPermission('allow_parental_control');
                    } else {
                        $user->assignPermission('can_send_passes');
                    }

                    $emailData['user'] = $user;
                    $emailData['type'] = 'resident';

                    dynamicEmail($emailData, $user->email, true);

                    $residentsCsv = ResidentCsv::find($residentsCsv->id);
                    $residentsCsv->mark = "1";
                    $residentsCsv->temporary_password = $randomPassword;
                    $residentsCsv->status = "success";
                    $residentsCsv->message = "Record imported successfully";
                    $residentsCsv->save();
                } else {
                    $residentsCsv = ResidentCsv::find($residentsCsv->id);
                    $residentsCsv->mark = "1";
                    $residentsCsv->status = "failure";
                    $residentsCsv->message = $csvError;
                    $residentsCsv->save();

                }

                $count++;

            }

        }

        $residentCsvFile = ResidentCsvFile::where('file_name', $request->file_name)->first();
        $currentProcessedRecords = intval($residentCsvFile->process_records);
        $totalProcessedRecords = $currentProcessedRecords + $count;
        $residentCsvFile->process_records = $totalProcessedRecords;
        $residentCsvFile->save();

        return response()->json(['recordsExecuted' => $count]);
    }

    public function residentImportFileInformation(Request $request)
    {
        $data['fileName'] = $request->file_name;
        return view($this->folderLink . 'imported-file-information', $data);
    }

    public function residentImportFileInformationData(Request $request)
    {
        $residentCsvs = ResidentCsv::where('file_name', $request->file_name)->get();
        return datatables()->of($residentCsvs)
            ->addColumn('links', function ($model) {

                $actionHTML = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if ($model->status == "failure") {
                    $actionHTML .= '<a class="dropdown-item" href="' . route('admin.residential.resident-csvs-table-record-edit', $model->id) . '"><i class="nav-icon fas fa-edit" aria-hidden="true"></i> Edit </a>';
                }

                $actionHTML .= '<form action="' . route('admin.residential.resident-csvs-delete', [$model->id]) . '" method="POST" class="ajax-form">
                                ' . csrf_field() . '
                                <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                                </form>
                                </div>
                                </div>';

                return $actionHTML;


            })
            ->rawColumns(['links'])
            ->make(true);
    }

    public function residentCsvsTableRecordEdit($id)
    {
        $data['residentCsv'] = ResidentCsv::find($id);
        $data['houses'] = House::where('community_id', $data['residentCsv']->community_id)->get();
        return view($this->folderLink . 'resident-csv-edit', $data);
    }

    public function residentCsvsRecordUpdate(Request $request, $id)
    {
        $residentData = $request->all();
        $residentData['phone_number'] = trim(str_replace("-", "", $residentData['phone_number']));
        $validation = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'house_id' => 'required',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'house_id.required' => 'Please select house'
        ];
        $validator = Validator::make($residentData, $validation, $messages);

        if ($validator->fails()) {
            $validator->validate();
        }

        $house = House::find($request->house_id);
        $houseName = $house->house_name;

        $residentCsv = ResidentCsv::find($id);
        $residentCsv->first_name = $request->first_name;
        $residentCsv->last_name = $request->last_name;
        $residentCsv->email = $request->email;
        $residentCsv->phone_number = $residentData['phone_number'];
        $residentCsv->house_name = $houseName;
        $residentCsv->head_of_family = $request->is_head_of_family;
        $residentCsv->special_instructions = $request->special_instruction;
        $residentCsv->mark = '0';
        $residentCsv->status = null;
        $residentCsv->message = null;
        $residentCsv->save();

        $residentCsvFile = ResidentCsvFile::where('file_name', $residentCsv->file_name)->first();
        $currentProcessRecords = intval($residentCsvFile->process_records);
        $currentProcessRecords--;
        $residentCsvFile->process_records = $currentProcessRecords;
        $residentCsvFile->save();

        return response()->json(['status' => true, 'message"=> "Updated Successfully', 'redirect' => route('admin.residential.resident-imported-file-saved-records', ['file_name' => $residentCsv->file_name])]);
    }

    /**
     * To delete resident_csv_files table record and delete records of resident_csvs table of file
     *
     * @param $id
     * @return Response
     */
    public function residentCsvFilesDelete($id)
    {
        $residentCsvFile = ResidentCsvFile::find($id);
        $residentCsvFile->delete();

        $residentCsv = ResidentCsv::where('file_name', $residentCsvFile->file_name);
        $residentCsv->delete();

        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.residential.imported-files')]);
    }

    /*
     * To delete house_csvs table record
     *
     * */
    public function residentCsvsDelete($id)
    {
        $residentCsv = ResidentCsv::find($id);
        $residentCsv->delete();

        $residentCsvFile = ResidentCsvFile::where('file_name', $residentCsv->file_name)->first();
        $totalRecords = intval($residentCsvFile->total_records) - 1;
        $processedRecords = intval($residentCsvFile->process_records) - 1;
        $residentCsvFile->total_records = $totalRecords;
        $residentCsvFile->process_records = $processedRecords;
        $residentCsvFile->save();

        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.residential.resident-import-file-information', ['file_name' => $residentCsv->file_name])]);
    }

    /**
     * To delete residents of community
     *
     */
    public function deleteCommunityResidentsHouseMembers()
    {
        $users = User::where(['community_id' => 62, 'role_id' => 5])->limit(100)->get();

        foreach ($users as $user) {
            $this->userService->deleteUser($user->id);
        }

        echo "Deleted successfully";
    }

    /**
     * To delete residents of community
     *
     */
    public function deleteCommunityResidentsHouseHead()
    {
        $users = User::where(['community_id' => 62, 'role_id' => 4])->limit(100)->get();

        foreach ($users as $user) {
            $this->userService->deleteUser($user->id);
        }

        echo "Deleted successfully";
    }
}
