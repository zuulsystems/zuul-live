<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PasswordController extends Controller
{
    public $userService;
    protected $folderLink = 'admin.password.';

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'password');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'current_password' => 'required',
            'new_password' => 'required',
            'retype_password' => 'required|same:new_password',
        ];
        $messages = [
            'current_password.required' => 'Please enter Current Password',
            'new_password.required' => 'Please enter New Password',
            'retype_password.required' => 'Please enter Re-type Password'
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $profileData['password'] = bcrypt($request->new_password);
        $id = auth()->user()->id;

        $password = $this->userService->update($profileData, $id);
        if ($password['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.password.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }
}
