<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\CommunityService;
use App\Services\TimezoneService;
use Artisan;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CommunityTicketController extends Controller
{
    public $communityService;
    public $timezoneService;
    protected $folderLink = 'admin.community-tickets.';

    public function __construct(
        CommunityService $communityService,
        TimezoneService  $timezoneService
    )
    {
        $this->communityService = $communityService;
        $this->timezoneService = $timezoneService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * Get communities list
     */
    public function communityTicketData(Request $request)
    {
        return datatables()->of($this->communityService->communityListWithCameraLocations(getCommunityIdByUser())['result'])
            ->addColumn('locations_count_edit', function ($model) {
                if ($model->houses->count() > 0) {
                    return '<a href="' . route('admin.locations.show', $model->id) . '">' . $model->locations->count() . '</a>';
                }
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="' . route('admin.locations.show', $model->id) . '"><i class="fa fa-edit" aria-hidden="true"></i> View Camera Locations </a>
                            <a class="dropdown-item fetch_tickets" href="#" data-id="' . $model->id . '"><i class="fa fa-edit" aria-hidden="true"></i> Fetch Tickets </a>
                        </div>
                    </div>';
            })
            ->filter(function ($query) use ($request) {
                if ($request->filled('name')) {
                    $query->where('name', 'like', '%' . $request->input('name') . '%');
                }
                if ($request->filled('address')) {
                    $query->where('address', 'like', '%' . $request->input('address') . '%');
                }
            })
            ->rawColumns(['links', 'locations_count_edit'])
            ->toJson();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => 'required',
            'timezone_id' => 'required',
            'contact_number' => 'required',
            'community_image' => 'nullable|image|mimes:jpeg,png,jpg',
        ];
        $messages = [
            'name.required' => 'Please enter community name',
            'timezone_id.required' => 'Please select Timezone',
            'contact_number.required' => 'Please enter contact number'
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $communityData = $request->all();
        $communityData['created_by'] = auth()->user()->id;

        //upload community logo
        if ($request->hasFile('community_image')) {
            $fileName = 'community_' . time() . '.' . $request->community_image->extension();
            $request->community_image->move(public_path('images/communities'), $fileName);
            $communityData['community_logo'] = $fileName;
        }
        $community = $this->communityService->create($communityData);
        if ($community['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.residential.communities.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $timezones = $this->timezoneService->all();
        if (!$timezones['bool']) {
            abort(500);
        }
        $data['timezones'] = $timezones['result'];
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $timezones = $this->timezoneService->all();
        if (!$timezones['bool']) {
            abort(500);
        }
        $data['timezones'] = $timezones['result'];
        $community = $this->communityService->find($id);
        if (!$community['bool']) {
            abort(500);
        }
        if (!$community['result']) {
            abort(404);
        }
        $data['community'] = $community['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'name' => 'required',
            'timezone_id' => 'required',
            'contact_number' => 'required',
            'community_image' => 'nullable|image|mimes:jpeg,png,jpg',
        ];
        $messages = [
            'name.required' => 'Please enter community name',
            'timezone_id.required' => 'Please select Timezone',
            'contact_number.required' => 'Please enter contact number'
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $communityData = $request->all();

        //upload community logo
        if ($request->hasFile('community_image')) {
            $fileName = 'community_' . time() . '.' . $request->community_image->extension();
            $request->community_image->move(public_path('images/communities'), $fileName);
            $communityData['community_logo'] = $fileName;
        }
        $community = $this->communityService->update($communityData, $id);
        if ($community['bool']) {
            $response = ['status' => true, 'message' => 'Edit Successfully', 'redirect' => route('admin.residential.communities.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $community = $this->communityService->delete($id);
        if ($community['bool']) {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.residential.communities.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    public function fetchCommunityTicketData($id)
    {
        try {
            $company_id = $this->communityService->find($id)['result']->company_id;
            if (empty($company_id))
                throw new Exception("No Company ID");
            dispatch(function () use ($company_id) {
                Artisan::call('fetch:tickets', ['--company_id' => $company_id]);
            });
            return response()->json(['status' => true, 'message' => 'Processing new tickets in Background'], 200);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => 'No Company ID Found'], 404);
        }
    }
}
