<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TrafficTicket;
use App\S3ImageHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\TrafficTicketService;
use App\Services\TrafficTicketImageService;
use App\Services\CommunityService;
use App\Services\EmailLogService;
use App\Services\TrafficPaymentLogService;
use App\Helpers\Tickets;
use App\Services\UserService;
use App\Services\LocationService;
use App\Services\VendorService;
use Exception;
use Illuminate\Http\Response;

class UnmatchedTicketController extends Controller
{
    // Tickets Trait for Datatable
    use Tickets;


    private $trafficTicketService, $trafficTicketImageService, $emailLogService, $trafficPaymentLogService, $userService, $vendorService;

    public function __construct(TrafficTicketService      $trafficTicketService,
                                TrafficTicketImageService $trafficTicketImageService,
                                CommunityService          $communityService,
                                EmailLogService           $emailLogService,
                                TrafficPaymentLogService  $trafficPaymentLogService,
                                UserService               $userService,
                                VendorService             $vendorService,
                                LocationService           $locationService)
    {
        $this->trafficTicketService = $trafficTicketService;
        $this->trafficTicketImageService = $trafficTicketImageService;
        $this->communityService = $communityService;
        $this->emailLogService = $emailLogService;
        $this->trafficPaymentLogService = $trafficPaymentLogService;
        $this->userService = $userService;
        $this->vendorService = $vendorService;
        $this->locationService = $locationService;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.tickets.unmatched.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.tickets.unmatched.show', ['camera_id' => $id]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function ticketsByLicense($license)
    {
        return view('admin.tickets.unmatched.show', ['license' => $license]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    public function unmatchedTicketData(Request $request)
    {
        ini_set('memory_limit', '-1');
        $displayDismissed = $request->has('displayDismissed') && $request->displayDismissed == "true";
        if ($request->has('camera_id')) {
            $data = $this->trafficTicketService->unmatchedData(null, null, $displayDismissed, $request->camera_id, null, null, $request);
        } else if ($request->has('license')) {

            $data = $this->trafficTicketService->unmatchedData(null, null, $displayDismissed, null, $request->license, null, $request);
        } else {
            $data = $this->trafficTicketService->unmatchedData('unmatched', getCommunityIdByUser(), $displayDismissed, null, null, null, $request);
        }

        if ($data['bool']) {
            return $this->generateDatatableUnmatched($request, $data['result']);
        } else {
            return [
                'bool' => true,
                'message' => "Something Went Wrong. Please Try Again later"
            ];

        }

    }

    public function unmatchedTicket(Request $request)
    {
        $displayDismissed = $request->has('displayDismissed') && $request->displayDismissed == "true";
        if ($request->has('camera_id')) {
            $data = $this->trafficTicketService->unmatchedData(null, null, $displayDismissed, $request->camera_id, null, null, $request);
        } else if ($request->has('license')) {
            $data = $this->trafficTicketService->unmatchedData(null, null, $displayDismissed, null, $request->license, null, $request);
        } else
            $data = $this->trafficTicketService->unmatchedData('unmatched', getCommunityIdByUser(), $displayDismissed, null, null, null, $request);

        if ($data['bool']) {
            return $this->generateDatatableUnmatched($request, $data['result']);
        } else {
            return [
                'bool' => true,
                'message' => "Something Went Wrong. Please Try Again later"
            ];

        }
    }

    public function changeLicense(Request $request)
    {
        try {
            $data = $this->trafficTicketService->find($request->ticketId)['result']->toArray();
            if (count($data) > 0 && $data['ticket_type'] == "unmatched") {
                $this->trafficTicketService->update([
                    'license_plate' => $request->lic
                ], $request->ticketId);

                return [
                    'bool' => true,
                    'data' => ""
                ];

            } else {
                return [
                    'bool' => false,
                    'message' => "Invalid Ticket Number!"
                ];
            }
        } catch (Exception $e) {
            return [
                'bool' => false,
                'message' => "Something Went Wrong! Please try again later."
            ];
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function attachUser(Request $request)
    {
        try {
            //Get Ticket Id
            $data = $this->trafficTicketService->find($request->ticketId)['result']->toArray();

            if ($request->ticket_type == 'vendor') {

                $user = $this->vendorService->find($request->user_id)['result'];

                if (count($data) > 0 && $data['ticket_type'] == "unmatched") {
                    $this->trafficTicketService->update([
                        'ticket_type' => $request->ticket_type,
                        'community_id' => $user->community_id,
                        'license_plate' => $request->license
                    ], $request->ticketId);

                    return [
                        'bool' => true,
                        'data' => ""
                    ];
                }
            } else {
                // Get Resident Details
                $user = $this->userService->find($request->user_id)['result'];

                // Change Details
                if (count($data) > 0 && $data['ticket_type'] == "unmatched") {
                    $this->trafficTicketService->update([
                        'ticket_type' => $request->ticket_type,
                        'user_id' => $user->id,
                        'community_id' => $user->community_id,
                    ], $request->ticketId);
                }
                return [
                    'bool' => true,
                    'data' => ""
                ];

            }

        } catch (Exception $e) {

            return $e;
            return [
                'bool' => false,
                'message' => "Something Went Wrong! Please try again later."
            ];
        }

    }

    public function getAttachUser(Request $request)
    {
        try {
            if ($request->user_type == "resident") {
                $data = $this->userService->getResidentList()['result']->get();
            } else {
                $data = $this->vendorService->getVendorList()['result'];
            }
            return [
                'bool' => true,
                'result' => $data
            ];

        } catch (Exception $e) {
            return [
                'bool' => false,
                'message' => "Something Went Wrong! Please try again later."
            ];
        }
    }

    public function getAttachLicense(Request $request)
    {
        try {

            $data = $this->vendorService->getVendorVehicleLicensePlates($request->vendor_id);

            return [
                'bool' => true,
                'result' => $data['result']
            ];

        } catch (Exception $e) {
            return $e;
            return [
                'bool' => false,
                'message' => "Something Went Wrong! Please try again later."
            ];
        }
    }

    /**
     * update existing unmatched tickets
     * @param Request $request
     */
    public function updateExistingUnmatchedTickets(Request $request)
    {
        $unMatchedTickets = $this->trafficTicketService->getAllUnmatchedTicket()['result'];
        if ($unMatchedTickets->isNotEmpty()) {
            foreach ($unMatchedTickets as $unMatchedTicket) {
                $licensePlate = $unMatchedTicket->license_plate;
                $residentTicket = $this->trafficTicketService->getTicketByLicensePlate($licensePlate, 'resident')['result'];
                if (!empty($residentTicket)) {
                    $ticketIds[] = $unMatchedTicket->id;
                    $userId = $residentTicket->user_id;
                    $communityId = $residentTicket->community_id;
                    echo($licensePlate);
                    echo "<br/>";
                    $updateTickets = $this->trafficTicketService->attachTicketsToResident($ticketIds, $userId, $communityId);
                }
            }
        }
    }

    /**
     * bulk dismissed tickets from ticket ids
     * @param Request $request
     * @return JsonResponse
     */
    public function bulkDismissedTicket(Request $request)
    {
        $ids = $request->ids;
        if (!empty($ids)) {
            $data = $this->trafficTicketService->bulkDelete($ids);
            if ($data['bool']) {
                $response = ['status' => true, 'message' => 'Delete Successfully'];
            } else {
                $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
            }
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }

        return response()->json($response);
    }

    public function deleteUnmatchedRecordsNvCommunity()
    {
        $trafficTickets = TrafficTicket::with('ticketImages')->where(['community_id' => 52, 'ticket_type' => 'unmatched'])->limit(100)->get();

        foreach ($trafficTickets as $trafficTicket) {
            echo $trafficTicket->id;
            foreach ($trafficTicket->ticketImages as $trafficTicketImages) {
                echo " " . $trafficTicketImages->id;
                $imagePath = 'images/tickets/' . $trafficTicketImages->image;

                try {
                    $result = S3ImageHelper::deleteImageFromBucket($imagePath);
                } catch (Exception $e) {
                }

                $trafficTicketImages->delete();

            }

            echo "<br>";

            $trafficTicket->delete();
        }

    }

}
