<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PushNotificationTrait;
use App\Http\Controllers\Controller;
use App\Services\AnnouncementService;
use App\Services\CommunityService;
use App\Services\NotificationService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class AnnouncementController extends Controller
{
    use PushNotificationTrait;

    public $announcementService;
    public $communityService;
    public $userService;
    public $notificationService;
    protected $folderLink = 'admin.announcements.';

    public function __construct(
        AnnouncementService $announcementService,
        CommunityService    $communityService,
        UserService         $userService,
        NotificationService $notificationService
    )
    {
        $this->announcementService = $announcementService;
        $this->communityService = $communityService;
        $this->userService = $userService;
        $this->notificationService = $notificationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get announcements list
     */
    public function announcementData(Request $request)
    {
        return datatables()->of($this->announcementService->all(getCommunityIdByUser())['result'])
            ->addColumn('title', function ($model) {
                return (!empty($model->title)) ? $model->title : "-";
            })
            ->addColumn('community_name', function ($model) {
                return (!empty($model->community->name)) ? $model->community->name : "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="' . route('admin.residential.announcements.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>

                            </div>
                        </div>';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->title)) {
                    $instance->where('title', 'like', "%{$request->get('title')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        $validation = [
            'community_id' => 'required',
            'title' => 'required',
            'community_image' => 'nullable|image|mimes:jpeg,png,jpg',
        ];
        $messages = [
            'community_id.required' => 'Please select Community',
            'title.required' => 'Please enter title',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $announcementData = $request->all();
        $announcementData['created_by'] = auth()->user()->id;

        $announcement = $this->announcementService->create($announcementData);
        if ($announcement['bool']) {
            $communityUsers = $this->userService->getUserByCommunity($request->community_id)['result'];
            if ($communityUsers->isNotEmpty()) {
                foreach ($communityUsers as $user) {
                    $notificationData = [
                        'user_id' => $user->id,
                        'type' => 'zuul',
                        'heading' => $request->title,
                        'text' => $request->description,
                        'category' => 'unread_announcement',
                        'announcement_id' => $announcement['result']->id
                    ];
                    $notification = $this->notificationService->create($notificationData);
                    if ($notification['bool']) {
                        $push = [
                            'title' => "Announcements",
                            'description' => $request->description,
                            'nid' => $notification['result']->id,
                        ];
                        $this->sendPushNotificationToUser($push, $user);
                    }
                }
            }

            // user activity logs
            callUserActivityLogs("Add Announcement", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.residential.announcements.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Announcement", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {

        $data['communities'] = getCommunities();
        $announcement = $this->announcementService->find($id);
        if (!$announcement['bool']) {
            abort(500);
        }
        if (!$announcement['result']) {
            abort(500);
        }
        $data['announcement'] = $announcement['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'community_id' => 'required',
            'title' => 'required',
            'community_image' => 'nullable|image|mimes:jpeg,png,jpg',
        ];
        $messages = [
            'community_id.required' => 'Please select Community',
            'title.required' => 'Please enter title',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $announcementData = $request->all();
        if ($request->has('is_must_read') && $request->is_must_read == '1') {
            $announcementData['is_must_read'] = '1';
        } else {
            $announcementData['is_must_read'] = '0';
        }

        $community = $this->announcementService->update($announcementData, $id);
        if ($community['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Announcement", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.residential.announcements.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Announcement", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $announcement = $this->announcementService->delete($id);
        if ($announcement['bool']) {
            // user activity logs
            callUserActivityLogs("Delete Announcement", __FUNCTION__, __FILE__, __DIR__, true, "Delete Successfully");

            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.residential.announcements.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete Announcement", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }
}
