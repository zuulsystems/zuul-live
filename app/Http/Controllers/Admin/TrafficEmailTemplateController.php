<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmailType;
use App\Services\EmailTemplateService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Str;

class TrafficEmailTemplateController extends Controller
{

    public $emailTemplateService;
    protected $folderLink = 'admin.traffic-email-templates.';

    public function __construct(EmailTemplateService $emailTemplateService)
    {
        $this->emailTemplateService = $emailTemplateService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get email templates list
     * @return mixed
     * @throws Exception
     */
    public function trafficEmailTemplateData()
    {
        return datatables()->of($this->emailTemplateService->getTrafficLogixEmailTemplate(getCommunityIdByUser())['result'])
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('edit_link', function ($model) {
                return '<a href="' . route('admin.traffic-tickets.tickets-logix-email-templates.edit', [$model->id]) . '">Edit </a>';
            })
            ->rawColumns(['template', 'edit_link'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => ['required', Rule::unique('email_templates')->where('name', $request->name)->whereNull('deleted_at')],
            'assign_to' => 'required',
            'community_id' => 'required',
            'template' => 'required'
        ];
        $messages = [
            'name.required' => 'Please enter  name',
            'assign_to.required' => 'Please select assign to',
            'community_id.required' => 'Please select community',
            'template.required' => 'Please enter template',
        ];

        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $templateData = $request->all();
        $templateData['created_by'] = auth()->user()->id;
        $templateData['email_type'] = 'trafficlogix';
        $templateData['template_code'] = Str::slug($templateData['name'], '-');
        $data = $request->all();

        $getByCommunityAssign = $this->emailTemplateService->getByCommunityAssignTo($request->community_id, $request->assign_to);
        if (!empty($getByCommunityAssign['result'])) {
            return response()->json(['status' => false, 'message' => 'Template already assigned.']);
        }
        $emailTemplate = $this->emailTemplateService->create($templateData);
        if ($emailTemplate['bool']) {

            //Mark:- Track Activity
            callUserActivityLogs("Traffic Logix Email Template Added", __FUNCTION__, __FILE__, __DIR__, true, 'Template Added Successfully');

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.traffic-tickets.tickets-logix-email-templates.index')];
        } else {

            //Mark:- Track Activity
            callUserActivityLogs("Traffic Logix Email Template Added", __FUNCTION__, __FILE__, __DIR__, false, 'Template Added Failed');

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        $data['email_types'] = EmailType::where('email_type', 'tl')->get();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $data['email_types'] = EmailType::where('email_type', 'tl')->get();
        $emailTemplate = $this->emailTemplateService->find($id);
        if (!$emailTemplate['bool']) {
            abort(500);
        }
        if (!$emailTemplate['result']) {
            abort(500);
        }
        $data['emailTemplate'] = $emailTemplate['result'];
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'name' => ['required', Rule::unique('email_templates')->where('name', $request->name)->whereNull('deleted_at')->ignore($id)],
            'assign_to' => 'required',
            'community_id.required' => 'Please select community',
            'template' => 'required'
        ];
        $messages = [
            'name.required' => 'Please enter name',
            'assign_to.required' => 'Please select assign to',
            'community_id.required' => 'Please select community',
            'template.required' => 'Please enter template',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $templateData = $request->all();
        $templateData['email_type'] = 'trafficlogix';
        $templateData['template_code'] = Str::slug($templateData['name'], '-');
        $getByCommunityAssign = $this->emailTemplateService->getByCommunityAssignTo($request->community_id, $request->assign_to, $id);
        if (!empty($getByCommunityAssign['result'])) {
            return response()->json(['status' => false, 'message' => 'Template already assigned.']);
        }
        $emailTemplate = $this->emailTemplateService->update($templateData, $id);
        if ($emailTemplate['bool']) {

            //Mark:- Track Activity
            callUserActivityLogs("Traffic Logix Email Template Updated", __FUNCTION__, __FILE__, __DIR__, true, 'Template Update Successfull');

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.traffic-tickets.tickets-logix-email-templates.index')];
        } else {

            //Mark:- Track Activity
            callUserActivityLogs("Traffic Logix Email Template Updated", __FUNCTION__, __FILE__, __DIR__, false, 'Template Update Failed');

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
