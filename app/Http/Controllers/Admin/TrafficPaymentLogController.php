<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\HouseService;
use App\Services\TrafficPaymentLogService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TrafficPaymentLogController extends Controller
{
    public $trafficPaymentLogService;
    public $houseService;
    protected $folderLink = 'admin.traffic-payment-logs.';

    public function __construct(
        TrafficPaymentLogService $trafficPaymentLogService,
        HouseService             $houseService
    )
    {
        $this->trafficPaymentLogService = $trafficPaymentLogService;
        $this->houseService = $houseService;
    }


    public function index(Request $request)
    {
        $data["houseId"] = $request->houseId ?? '';
        return view($this->folderLink . 'index');
    }

    public function trafficPaymentLogsData(Request $request)
    {
        $houseId = null;
        $payUserIds = null;
        if ($request->has('houseId') && $request->get('houseId')) {
            $houseId = $request->get('houseId');
        }
        $house = $this->houseService->find($houseId)['result'];
        if (!empty($house)) {
            if ($house->residents->isNotEmpty()) {
                $payUserIds = $house->residents->pluck('id');
            }
        }
        return datatables()->of($this->trafficPaymentLogService->data(getCommunityIdByUser(), $payUserIds)['result'])
            ->addColumn('admin_user_full_name', function ($model) {
                return (!empty($model->user)) ? $model->user->fullName : "-";
            })
            ->addColumn('payee_user_full_name', function ($model) {
                return (!empty($model->payee)) ? $model->payee->fullName : "-";
            })
            ->addColumn('community_name', function ($model) {
                return (!empty($model->ticket) && !empty($model->ticket->community)) ? $model->ticket->community->name : "-";
            })
            ->addColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->format('d-m-Y h:i:s A') : "-";
            })->filter(function ($instance) use ($request) {

                if ($request->has('user_id') && !empty($request->user_id)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['payee']['id'] == $request->user_id;
                    });
                }

                if ($request->has('ticket_id') && !empty($request->ticket_id)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['ticket']['ticket_id'] == $request->ticket_id;
                    });
                }

                if ($request->has('community') && !empty($request->community)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['community_name'], $request->community);
                    });
                }

                if ($request->has('admin_user') && !empty($request->admin_user)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['admin_user_full_name'], $request->admin_user);
                    });
                }

                if ($request->has('payee_user') && !empty($request->payee_user)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['payee_user_full_name'], $request->payee_user);
                    });
                }


                if ($request->has('from') && !empty($request->from)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return strtotime($row['date']) >= strtotime($request->from);
                    });
                }

                if ($request->has('to') && !empty($request->to)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                        return strtotime($row['date']) <= strtotime($request->to);
                    });
                }
            })
            ->toJson();
    }
}
