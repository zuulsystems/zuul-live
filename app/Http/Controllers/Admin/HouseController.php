<?php

namespace App\Http\Controllers\Admin;

use App\Exports\HousesSkippedRecordsExports;
use App\Http\Controllers\Controller;
use App\Mail\SendResidentImportFileGeneration;
use App\Models\CsvImport;
use App\Models\House;
use App\Models\HouseCsv;
use App\Models\HouseCsvFile;
use App\Models\User;
use App\Services\CommunityService;
use App\Services\HouseService;
use Exception;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\ImportHouses;
use function MongoDB\BSON\toJSON;

class HouseController extends Controller
{
    public $houseService;
    public $communityService;
    protected $folderLink = 'admin.houses.';

    public function __construct(
        HouseService     $houseService,
        CommunityService $communityService
    )
    {
        $this->houseService = $houseService;
        $this->communityService = $communityService;
        $this->middleware('checkGuardAdmin', ['except' => ['index', 'houseData']]);
    }

    /**
     * Read imported house queue job file.
     * @param array  file information
     * @return create new file
     **/
    public static function readImportedFile($filePath = array())
    {

        $HEADER = 'house_name,street_address,apartment,city,state,zipcode,lot_number,contact';
        $fail_message = '';
        $header = null;
        $csv = array();
        $row = 1;
        $imp_error = 0;

        $communityId = $filePath[1];
        $adminEmail = $filePath[2];

        $filePath = Storage::path($filePath[0]);

        $handle = fopen($filePath, "r");

        $uniqueStreetAddressIndex;

        while (($data = fgetcsv($handle, 5000, ',')) !== false) {
            if ($row == 1) {
                $first_row = array_diff($data, explode(",", $HEADER));
                if (count($first_row) == 1) {
                    $imp_error = 1;
                    $fail_message .= "<strong>Error!</strong> Invalid column name somewhere in your CSV file.";
                    $valid_file = 0;
                    return $fail_message;
                }

                $uniqueStreetAddressIndex = array_search('house_name', $data);

            }

            if (!$header) {
                $header = $data;
            } else {

                $uniqueStreetAddress['unique_street_address'] = removeSpecialCharacters($data[$uniqueStreetAddressIndex]);//unique street address for validation
                if (!alphaNumaric($uniqueStreetAddress['unique_street_address'])) {
                    $fail_message .= "<strong>Error!</strong> Something went wrong.Please try again.";
                    return $fail_message;
                }
                $csv[] = array_merge(array_combine($header, $data), $uniqueStreetAddress);
            }

            $row++;
        }
        fclose($handle);
        $houseArr = $csv;

        $csvSkippedRows = [];

        // Success Insert the Csv data into the database
        for ($i = 0; $i < count($houseArr); $i++) {
            $rowError = false;
            $csvError = '';
            $comma = false;

            if (empty($houseArr[$i]['house_name'])) {
                $rowError = true;
                $csvError .= 'blank house name';
                $comma = true;
            }

            if (empty($houseArr[$i]['address'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank address' : $csvError .= 'blank address';
            }

            if (!$rowError) {
                $adminUser = User::where('email', $adminEmail)->first();

                $house = House::where(['house_name' => $houseArr[$i]['house_name'], 'house_detail' => $houseArr[$i]['address'], 'community_id' => $communityId])->first();

                $message = '';

                if (!empty($house)) {
                    $message = 'House already exist';

                    $csvError = "Failure";

                } else {
                    $house = House::create([
                        'house_name' => $houseArr[$i]['house_name'],
                        'house_detail' => $houseArr[$i]['address'],
                        'lot_number' => $houseArr[$i]['lot_number'],
                        'phone' => $houseArr[$i]['primary_contact'],
                        'created_by' => $adminUser->id,
                        'community_id' => $communityId
                    ]);

                    $message = 'Record imported Successfully';

                    $csvError = "Success";

                }

                $skipRow = array(
                    '#' => '',
                    'house_name' => $houseArr[$i]['house_name'],
                    'house_detail' => $houseArr[$i]['address'],
                    'lot_number' => $houseArr[$i]['lot_number'],
                    'phone' => $houseArr[$i]['primary_contact'],
                    'status' => $csvError,
                    'message' => $message,
                    'id' => $house->id
                );

                array_push($csvSkippedRows, $skipRow);
            } else {

                $skipRow = array(
                    '#' => '',
                    'house_name' => $houseArr[$i]['house_name'],
                    'house_detail' => $houseArr[$i]['address'],
                    'lot_number' => $houseArr[$i]['lot_number'],
                    'phone' => $houseArr[$i]['primary_contact'],
                    'status' => 'Failure',
                    'message' => $csvError,
                    'id' => ''
                );

                array_push($csvSkippedRows, $skipRow);

            }
        }

        if (!Storage::exists('/storage/app/public/housecsvfilesresult')) {
            Storage::makeDirectory('/storage/app/public/housecsvfilesresult', 0775, true); //creates directory
        }

        $fileName = "house_success_skip" . time() . ".txt";
        $successSkippedFile = "/storage/app/public/housecsvfilesresult/" . $fileName;

        $handle = fopen("storage/app/storage/app/public/housecsvfilesresult/" . $fileName, 'w');

        foreach ($csvSkippedRows as $user) {
            fputcsv($handle, [
                $user['#'],
                $user['house_name'],
                $user['house_detail'],
                $user['lot_number'],
                $user['phone'],
                $user['status'],
                $user['message'],
                $user['id']
            ]);
        }
        fclose($handle);

        //sending email
        $emailData['fileName'] = $fileName;
        $emailData['csvBelongsTo'] = 'houses';
        $emailData['subject'] = 'House Import File Generation';

        CsvImport::create(
            [
                'email' => $adminEmail,
                'type' => 'house-import',
                'file_name' => $fileName
            ]
        );

        Mail::to($adminEmail)->send(new SendResidentImportFileGeneration($emailData));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data["communityId"] = $request->communityId ?? '';
        $data['communities'] = getCommunities();
        $data['houseId'] = $request->houseId ?? '';
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get houses list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function houseData(Request $request)
    {
        $communityId = null;
        $houseId = null;
        if ($request->has('communityId') && !empty($request->communityId)) {
            $communityId = $request->communityId;
        }
        if ($request->has('houseId') && !empty($request->houseId)) {
            $houseId = $request->houseId;
        }
        if (!empty(getCommunityIdByUser())) {
            $communityId = getCommunityIdByUser();
        }
        $houseData = datatables()->of($this->houseService->housesList($communityId, $houseId)['result'])
            ->addColumn('formatted_residents_count', function ($model) {
                if ($model->residents()->count() > 0) {
                    return '<a href="' . route('admin.residential.residents.index', ['houseId' => $model->id]) . '">' . $model->residents->count() . '</a>';
                } else {
                    return 0;
                }
            })
            ->addColumn('community_name', function ($model) {
                return ($model->community()->count() > 0) ? $model->community->name : "";
            })
            ->addColumn('lot_number', function ($model) {
                return (!empty($model->lot_number)) ? $model->lot_number : "-";
            })
            ->addColumn('phone', function ($model) {
                return (!empty($model->phone)) ? $model->phone : "-";
            })
            ->addColumn('house_detail', function ($model) {
                return (!empty($model->house_detail)) ? $model->house_detail : "-";
            })
            ->addColumn('complete_profile', function ($model) {
                $is_complete = '';
                if ($model->completed_profile == "yes") {
                    $is_complete = '<span class="badge badge-success" style="width: 43px;">Yes</span>';
                } else if ($model->completed_profile == "no") {
                    $is_complete = '<span class="badge badge-warning" style="width: 43px;">No</span>';
                } else {
                    $is_complete = '<span class="badge badge-info" style="width: 43px;">Null</span>';
                }

                return $is_complete;

            })
            ->addColumn('links', function ($model) {
                if (!Gate::allows('is-guard-admin')) {

                    $actions = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                             <a class="dropdown-item" href="' . route('admin.residential.houses.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                              <a class="dropdown-item" href="' . route('admin.residential.residents.index', ['houseId' => $model->id]) . '"><i class="nav-icon fas fa-users" aria-hidden="true"></i> HouseHold Members List (' . $model->residents->count() . ') </a>
                            <a class="dropdown-item" href="' . route('admin.guest-logs.index', ['houseId' => $model->id]) . '"><i class="nav-icon fas fa-history" aria-hidden="true"></i> Guest Logs </a>
                                <a class="dropdown-item" href="' . route('admin.passes.index', ['houseId' => $model->id]) . '"><i class="nav-icon fas fa-ticket-alt" aria-hidden="true"></i> Passes </a>

                                <a class="dropdown-item" href="' . route('admin.rfid.rfids.index', ['houseId' => $model->id]) . '"><i class="far fa-circle nav-icon" aria-hidden="true"></i> RFID Tags </a>
                                <a class="dropdown-item" href="' . route('admin.rfid.rfid-logs.index', ['houseId' => $model->id]) . '"><i class="far fa-circle nav-icon" aria-hidden="true"></i> RFID Tracking </a>
                                <a class="dropdown-item" href="' . route('admin.traffic-tickets.resident-tickets.index', ['houseId' => $model->id]) . '"><i class="far fa-circle nav-icon" aria-hidden="true"></i> Tickets </a>
                                 <a class="dropdown-item" href="' . route('admin.traffic-payment-logs.index', ['houseId' => $model->id]) . '"><i class="far fa-circle nav-icon" aria-hidden="true"></i> TL Payments </a>
                                <form action="' . route('admin.residential.houses.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                </form>';

                    if (auth()->user()->hasRole('super_admin')) {

                        $message = "'Are you sure?'";

                        if ($model->residents->count() > 0) {
                            $member = $model->residents->count() > 1 ? "members" : "member";

                            $message = "'Are you sure? " . $model->residents->count() . " household " . $member . " will be purged to guests as well'";
                        }

                        $actions .= '<form action="' . route('admin.residential.houses.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                  ' . method_field("DELETE") . '
                                    ' . csrf_field() . '


                                    <button class="dropdown-item" onclick="return confirm(' . $message . ')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>';
                    }

                    $actions .= '</div>
                        </div>';
                }

                return $actions;
            })
            ->filter(function ($instance) use ($request) {

                if (isset($request->house_name)) {
                    $instance->where('house_name', 'like', "%{$request->get('house_name')}%");
                }
                if (isset($request->full_address)) {
                    $instance->where('house_detail', 'like', "%{$request->get('full_address')}%");
                }
                if (isset($request->lot_number)) {
                    $instance->where('lot_number', 'like', "%{$request->get('lot_number')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
                if (isset($request->phone)) {
                    $instance->where('phone', 'like', "%{$request->get('phone')}%");
                }
                if ($request->has('complete_profile') && !empty($request->complete_profile)) {
                    $instance->where('completed_profile', $request->get('complete_profile'));
                }
            })
            ->rawColumns(['links', 'formatted_residents_count', 'complete_profile'])
            ->toJson();

        return $houseData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws ValidationException
     * @throws Exception
     */
    public function store(Request $request)
    {
        $houseData = $request->all();
        $validation = [
            'house_name' => 'required',
            'house_detail' => 'required',
            'community_id' => 'required',
        ];
        $messages = [
            'house_name.required' => 'Please enter residential sur name',
            'house_detail.required' => 'Please enter address',
            'community_id.required' => 'Please select community'
        ];
        $validator = Validator::make($houseData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $houseData['created_by'] = auth()->user()->id;

        $house = House::where(['community_id' => $houseData['community_id'], 'house_name' => $houseData['house_name'], 'house_detail' => $houseData['house_detail']])->first();

        $community;
        $message = 'Something went wrong.Please try again.';

        if (!empty($house)) {
            $community['bool'] = false;
            $message = 'Duplicate house entry';
        } else {
            if (!empty($houseData['lot_number'])) {
                $lotNumberLength = strlen($houseData['lot_number']);
                $zeroToAdd = 10 - $lotNumberLength;
                $modifiedLotNumber = $houseData['lot_number'];
                for ($i = 0; $i < $zeroToAdd; $i++) {
                    $modifiedLotNumber = '0' . $modifiedLotNumber;
                }
                $houseData['lot_number_ten_digits'] = $modifiedLotNumber;
            }
            $houseData['total_house_members'] = '00';
            $houseData['completed_profile'] = 'no';
            $community = $this->houseService->create($houseData);
            $message = 'Added Successfully';
        }

        if ($community['bool']) {
            // user activity logs
            callUserActivityLogs("Add House", __FUNCTION__, __FILE__, __DIR__, true, $message);

            $response = ['status' => true, 'message' => $message, 'redirect' => route('admin.residential.houses.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add House", __FUNCTION__, __FILE__, __DIR__, false, "Some error occured!");

            $response = ['status' => false, 'message' => $message];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function edit($id)
    {
        $data['communities'] = getCommunities();
        $house = $this->houseService->find($id);
        if (!$house['bool']) {
            abort(500);
        }
        if (!$house['result']) {
            abort(404);
        }
        $data['house'] = $house['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $house = $this->houseService->getHouseWithResidents($id);

        if ($house['result']->residents->count() > 0) {
            $guestData = array(
                'email_verified_at' => null,
                'password' => null,
                'temp_password' => null,
                'profile_image' => null,
                'date_of_birth' => null,
                'street_address' => null,
                'apartment' => null,
                'city' => null,
                'state' => null,
                'zip_code' => null,
                'community_id' => null,
                'house_id' => null,
                'fcm_token' => null,
                'web_token' => null,
                'created_by' => null,
                'verification_code' => null,
                'reset_verification_code' => null,
                'email_verification_code' => null,
                'email_owner_count' => 1,
                'is_email_verified' => '0',
                'is_verified' => '0',
                'push_notification' => '1',
                'sms_notification' => '1',
                'email_notification' => '1',
                'special_instruction' => null,
                'note' => null,
                'is_two_factor' => '0',
                'vacation_mode' => '0',
                'license_type' => 'driving_license',
                'is_suspended' => '0',
                'license_image' => null,
                'license_number' => null,
                'secondary_email' => null,
                'secondary_phone_number' => null,
                'mailing_address' => null,
                'role_id' => 7,
                'is_two_factor_code' => null,
                'is_two_factor_on' => null,
                'front_camera' => null,
                'is_reset_password' => '0',
                'sms_count' => 1,
                'remember_token' => null,
                'api_token' => null,
                'is_delete' => '0',

            );
            $house['result']->residents()->update($guestData);

        }

        $house = $this->houseService->delete($id);

        if ($house['bool']) {
            // user activity logs
            callUserActivityLogs("Delete House", __FUNCTION__, __FILE__, __DIR__, true, "Deleted Successfully");

            $response = ['status' => true, 'message' => 'Deleted Successfully', 'redirect' => route('admin.residential.houses.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete House", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $houseData = $request->all();
        $validation = [
            'house_name' => 'required',
            'house_detail' => 'required',
            'community_id' => 'required',
        ];
        $messages = [
            'house_name.required' => 'Please enter residential sur name',
            'house_detail.required' => 'Please enter  address',
            'community_id.required' => 'Please select community'
        ];
        $validator = Validator::make($houseData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }

        $houseData = $request->all();

        $house = House::where(['community_id' => $houseData['community_id'], 'house_name' => $houseData['house_name'], 'house_detail' => $houseData['house_detail']])->where('id', '!=', $id)->first();

        $message = 'Something went wrong.Please try again.';

        if (!empty($house)) {
            $house['bool'] = false;
            $message = 'House exist';
        } else {
            if (!empty($houseData['lot_number'])) {
                $lotNumberLength = strlen($houseData['lot_number']);
                $zeroToAdd = 10 - $lotNumberLength;
                $modifiedLotNumber = $houseData['lot_number'];

                for ($i = 0; $i < $zeroToAdd; $i++) {
                    $modifiedLotNumber = '0' . $modifiedLotNumber;
                }
                $houseData['lot_number_ten_digits'] = $modifiedLotNumber;
            }

            $house = $this->houseService->update($houseData, $id);

        }

        if ($house['bool']) {
            // user activity logs
            callUserActivityLogs("Edit House", __FUNCTION__, __FILE__, __DIR__, true, "Updated successfully");

            $response = ['status' => true, 'message' => 'Updated successfully', 'redirect' => route('admin.residential.houses.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit House", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong.Please try again.");

            $response = ['status' => false, 'message' => $message];
        }
        return response()->json($response);
    }

    /**
     * showed houses by community
     */
    public function communityHouses($communityId)
    {
        $data["communityId"] = $communityId;
        return view($this->folderLink . 'index', $data);
    }

    /**
     * import houses from CSV
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */

    public function importHouses(Request $request)
    {

        $rules['house_csv'] = 'required|file';

        if (auth()->user()->role_id == 1) {
            $rules['community_id'] = 'required';
        }

        $messages['house_csv.required'] = 'Select a file to import.';
        $messages['house_csv.file'] = 'Select a csv file to import.';

        if (auth()->user()->role_id == 1) {
            $messages['community_id.required'] = 'Select community.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return redirect()->back()->withErrors($validator);

        }

        $csv = $request->file('house_csv');
        $filename = $csv->getRealPath();

        if (($handle = fopen($filename, 'r')) !== false) {

            $houseArr = $this->csvToArray($handle); // Function to validate csv file.

            // Checking Excel Headers
            if (!is_array($houseArr)) {
                // Display Errors
                $validator->after(function ($validator) use ($houseArr) {
                    $validator->errors()->add('house_csv', $houseArr);
                });
                $validator->validate();
            }

            //creating skip rows csv header
            $csvSkippedRows = [
                [
                    '#' => '#',
                    'house_name' => 'house_name',
                    'house_detail' => 'house_detail',
                    'lot_number' => 'lot_number',
                    'phone' => 'phone',
                    'error' => 'error'
                ]
            ];

            $skipCSVFileGeneration = false;

            $communityId;
            if (auth()->user()->role_id == '1') {
                $communityId = $request->community_id;
            } else if (auth()->user()->role_id == '2') {
                $communityId = auth()->user()->community_id;
            }


            // Success Insert the Csv data into the database
            for ($i = 0; $i < count($houseArr); $i++) {
                $rowError = false;
                $csvError = '';

                if (empty($houseArr[$i]['house_name'])) {
                    $rowError = true;
                    $csvError .= 'blank house name';
                }

                if (empty($houseArr[$i]['address'])) {
                    $rowError = true;
                    $csvError .= ', blank address';
                }

                if (!$rowError) {
                    $this->houseService->updateOrCreate([
                        'house_name' => $houseArr[$i]['house_name'],
                        'house_detail' => $houseArr[$i]['address'],
                        'community_id' => $communityId,
                    ],
                        [
                            'house_name' => $houseArr[$i]['house_name'],
                            'house_detail' => $houseArr[$i]['address'],
                            'lot_number' => $houseArr[$i]['lot_number'],
                            'phone' => $houseArr[$i]['primary_contact'],
                            'created_by' => auth()->id(),
                        ]);
                } else {

                    $skipCSVFileGeneration = true;

                    $skipRow = array(
                        '#' => '',
                        'house_name' => $houseArr[$i]['house_name'],
                        'house_detail' => $houseArr[$i]['address'],
                        'lot_number' => $houseArr[$i]['lot_number'],
                        'phone' => $houseArr[$i]['primary_contact'],
                        'error' => $csvError
                    );

                    array_push($csvSkippedRows, $skipRow);

                }
            }

            if ($skipCSVFileGeneration) {
                return $this->generateSkipHousesRecords($csvSkippedRows);
            }

            return redirect()->back()->with('housesImportedSuccessfully', 'Yes');

        }
    }

    // For Csv upload

    function csvToArray($handle = '')
    {
        $HEADER = 'house_name,street_address,apartment,city,state,zipcode,lot_number,contact';
        $fail_message = '';
        $header = null;
        $csv = array();
        $row = 1;
        $imp_error = 0;
        while (($data = fgetcsv($handle, 5000, ',')) !== false) {
            if ($row == 1) {
                $first_row = array_diff($data, explode(",", $HEADER));
                if (count($first_row) == 1) {
                    $imp_error = 1;
                    $fail_message .= "<strong>Error!</strong> Invalid column name somewhere in your CSV file.";
                    $valid_file = 0;
                    return $fail_message;
                }
            }

            if (!$header) {
                $header = $data;
            } else {
                $uniqueStreetAddress['unique_street_address'] = removeSpecialCharacters($data[1]);//unique street address for validation
                if (!alphaNumaric($uniqueStreetAddress['unique_street_address'])) {
                    $fail_message .= "<strong>Error!</strong> Something went wrong.Please try again.";
                    return $fail_message;
                }
                $csv[] = array_merge(array_combine($header, $data), $uniqueStreetAddress);
            }

            $row++;
        }
        fclose($handle);
        return $csv;
    }

    //generate skip house records
    function generateSkipHousesRecords($skippedRowsArray)
    {
        return Excel::download(new HousesSkippedRecordsExports($skippedRowsArray), 'houses_skipped_records.csv');
    }

    public function importHouses1(Request $request)
    {
        $rules['house_csv'] = 'required|file';

        if (auth()->user()->role_id == 1) {
            $rules['community_id'] = 'required';
        }

        $messages['house_csv.required'] = 'Select a file to import.';
        $messages['house_csv.file'] = 'Select a csv file to import.';

        if (auth()->user()->role_id == 1) {
            $messages['community_id.required'] = 'Select community.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $validator->validate();


        }


        // Validation if wrong headers in csv
        $csv = $request->file('house_csv');
        $filename = $csv->getRealPath();


        if (($handle = fopen($filename, 'r')) !== false) {

            $HEADER = '#,house_name,address,lot_number,primary_contact';

            $a = 0;

            while (($data = fgetcsv($handle, 5000, ',')) !== false) {

                if ($a == 0) {
                    $headerSent = '';

                    foreach ($data as $key => $value) {
                        $headerSent .= $value . ",";
                    }

                    $headerSent = chop($headerSent, ",");

                    $headerSent = explode(",", $headerSent);
                    $HEADER = explode(",", $HEADER);

                    //if columns are less or more
                    if (count($headerSent) != 5) {
                        $fail_message = "<strong>Error!</strong> Wrong numbers of columns, extra or missing column in CSV file.";
                        $validator->after(function ($validator) use ($fail_message) {
                            $validator->errors()->add('house_csv', $fail_message);
                        });
                        $validator->validate();
                    }

                    $standardHeaders = ["address", "house_name", "lot_number", "primary_contact"];

                    //if any wrong column given
                    foreach ($standardHeaders as $standardHeader) {
                        if (!array_search($standardHeader, $headerSent)) {
                            $fail_message = "<strong>Error!</strong> Invalid column given in your CSV file.";
                            $validator->after(function ($validator) use ($fail_message) {
                                $validator->errors()->add('house_csv', $fail_message);
                            });
                            $validator->validate();
                        }
                    }

                }

                $a++;

            }
        }

        // make a directory  if not exist
        if (!Storage::exists('/storage/app/public/housecsvfiles')) {
            Storage::makeDirectory('/storage/app/public/housecsvfiles', 0775, true); //creates directory
        }

        $path = Storage::disk('local')->put("\storage\app\public\housecsvfiles", $request->file('house_csv'));

        $path = str_replace('\\', '/', $path);

        $pathParts = explode("/", $path);

        $communityId;
        if (auth()->user()->role_id == '1') {
            $communityId = $request->community_id;
        } else if (auth()->user()->role_id == '2') {
            $communityId = auth()->user()->community_id;
        }

        $data = array($path, $communityId, auth()->user()->email, ['house_name' => $request->house_name, 'address' => $request->address, 'lot_number' => $request->lot_number, 'primary_contact' => $request->primary_contact]);

        $handle = fopen($filename, "r");

        $a = 0;

        $count = 0;

        while (($data1 = fgetcsv($handle, 5000, ',')) !== false) {
            if ($a == 0) {

            } else {
                HouseCsv::create([
                    'house_name' => $data1[1],
                    'address' => $data1[2],
                    'lot_number' => $data1[3],
                    'primary_contact' => $data1[4],
                    'mark' => '0',
                    'community_id' => $communityId,
                    'created_by' => auth()->user()->id,
                    'file_name' => $pathParts[5]]);

                $count++;
            }
            $a++;
        }

        HouseCsvFile::create([
            'file_name' => $pathParts[5],
            'total_records' => $count,
            'process_records' => 0,
            'community_id' => $communityId
        ]);

        return response()->json(['status' => true, 'message' => '', 'redirect' => route('admin.residential.house-imported-file-saved-records', ['file_name' => $pathParts[5]])]);

    }

    /**
     * imported files
     *
     * @param Request $request
     * @return JsonResponse
     */
    function importedFiles(Request $request)
    {
        $data['fileName'] = $request->file_name ? $request->file_name : '';
        return view($this->folderLink . 'imported-files', $data);
    }

    /**
     * Reads file data and give result for table
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function importedFileData(Request $request)
    {
        $houseCsvFile = HouseCsvFile::with('community');
        auth()->user()->role_id == 2 ? $houseCsvFile = $houseCsvFile->where('community_id', auth()->user()->community_id) : "";
        $houseCsvFile = $houseCsvFile->get();

        return datatables()->of($houseCsvFile)
            ->addColumn('community', function ($model) {
                return $model->community->name;
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at : "";
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                               <a class="dropdown-item" href="' . route('admin.residential.house-imported-file-saved-records', ['file_name' => $model->file_name]) . '"><i class="nav-icon fas fa-tasks" aria-hidden="true"></i> Process Records </a>
                               <a class="dropdown-item" href="' . route('admin.residential.houses-import-file-information', ['file_name' => $model->file_name]) . '"><i class="nav-icon fas fa-eye" aria-hidden="true"></i> View Records </a>
                               <form action="' . route('admin.residential.house-csv-files-delete', $model->id) . '" class="ajax-form" method="POST">
                               ' . csrf_field() . '
                               <input type="hidden" name="id" value="' . $model->id . '">
                               <button class="dropdown-item" type="submit"><i class="fas fa-trash"></i> Delete</button>
                                </form>
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->make(true);
    }

    /**
     * function to delete imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteImportedFile(Request $request)
    {
        if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . $request->file_name)) {
            unlink(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . $request->file_name);
            return response()->json(['status' => true, 'message' => 'Success! File deleted successfully', 'redirect' => route('admin.residential.house-imported-files')]);
        } else {
            return response()->json(['status' => false, 'message' => 'Failed! File not found', 'redirect' => route('admin.residential.house-imported-files')]);
        }
    }

    /**
     * function to download imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadImportedFile(Request $request)
    {

        $fileName = $request->file_name;

        $filepath = storage_path("app\storage\app\public\housecsvfilesresult\\" . $fileName);

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . 'file.csv')) {
            unlink(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . 'file.csv');
        }

        if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . $request->file_name)) {
            //creating file csv
            $fp = fopen(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . 'file.csv', 'w');

            fputcsv($fp, [
                '#',
                'house_name',
                'address',
                'lot_number',
                'primary_contact',
                'status',
                'message',
                'id'
            ]);

            foreach ($fileData as $dat) {
                fputcsv($fp, $dat, ",");
            }

            fclose($fp);

            $filepath = storage_path("app/storage/app/public/housecsvfilesresult/" . 'file.csv');

            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                die();
            }
        } else {
            return back()->with('fileNotFound', 'YES');
        }
    }

    /**
     * function to download imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadFailRecordsImportedFile(Request $request)
    {

        $fileName = $request->file_name;

        $filepath = storage_path("app\storage\app\public\housecsvfilesresult\\" . $fileName);

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . 'failed_records_file.csv')) {
            unlink(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . 'failed_records_file.csv');
        }

        if (Storage::disk('local')->exists('storage\app\public\housecsvfilesresult\\' . $request->file_name)) {
            //creating file csv
            $fp = fopen(storage_path("app/storage/app/public/housecsvfilesresult") . "/" . 'failed_records_file.csv', 'w');

            fputcsv($fp, [
                '#',
                'house_name',
                'address',
                'lot_number',
                'primary_contact',
                'status',
                'message'
            ]);

            foreach ($fileData as $dat) {
                if ($dat[5] == "Failure") {
                    fputcsv($fp, $dat, ",");
                }
            }

            fclose($fp);

            $filepath = storage_path("app/storage/app/public/housecsvfilesresult/" . 'failed_records_file.csv');

            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                die();
            }
        } else {
            return back()->with('fileNotFound', 'YES');
        }
    }

    /**
     * To show imported file saved records from database.
     *
     * @param Request
     * @param
     * @return
     */
    public function houseImportedFileSavedRecords(Request $request)
    {
        $data['fileName'] = $request->file_name ? $request->file_name : '';
        return view($this->folderLink . 'imported-file-saved-records', $data);
    }

    /**
     * Reads house_csv table data and give result
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function houseImportedFileSavedRrecordsData(Request $request)
    {
        $fileData = [];
        if (!empty($request->file_name)) {

        }

        $houseCsvData = HouseCsv::where('file_name', $request->file_name)->get();

        return datatables()->of($houseCsvData)
            ->addColumn('house_name', function ($model) {
                return $model->house_name;
            })
            ->addColumn('address', function ($model) {
                return $model->address;
            })
            ->addColumn('lot_number', function ($model) {
                return $model->lot_number;
            })
            ->addColumn('primary_contact', function ($model) {
                return $model->primary_contact;
            })
            ->addColumn('status', function ($model) {
                return $model->status;
            })
            ->addColumn('message', function ($model) {
                return $model->message;
            })
            ->addColumn('links', function ($model) {

                $actionHTML = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if ($model->status == "failure") {
                    $actionHTML .= '<a class="dropdown-item" href="' . route('admin.residential.house-csvs-table-record-edit', $model->id) . '"><i class="nav-icon fas fa-edit" aria-hidden="true"></i> Edit </a>';
                }

                $actionHTML .= '
                    <form action="' . route('admin.residential.house-csvs-delete', [$model->id]) . '" method="POST" class="ajax-form">
                    ' . csrf_field() . '
                    <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                    </form>
                    </div>
                    </div>';

                return $actionHTML;
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('house_name') && !empty($request->house_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['house_name'], $request->house_name);
                    });
                }
                if ($request->has('full_address') && !empty($request->full_address)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['address'], $request->full_address);

                    });
                }
                if ($request->has('lot_number') && !empty($request->lot_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['lot_number'], $request->lot_number);
                    });
                }
                if ($request->has('phone') && !empty($request->phone)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['primary_contact'], $request->phone);
                    });
                }
                if ($request->has('status') && !empty($request->status)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['status'], $request->status);
                    });
                }
                if ($request->has('message') && !empty($request->message)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['message'], $request->message);
                    });
                }

            })
            ->rawColumns(['links'])
            ->make(true);
    }

    /**
     * Determine if a given string contains all array values.
     * Not case sensitive
     *
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public function containsInsensitive($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && mb_stripos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    public function saveHousesFromHouseCsvsTable(Request $request)
    {
        $houseCsvs = HouseCsv::where(['file_name' => $request->file_name, 'mark' => '0'])->limit(10)->get();

        $count = 0;

        foreach ($houseCsvs as $houseCsv) {
            $rowError = false;
            $csvError = '';
            $comma = false;

            if (empty($houseCsv->house_name)) {
                $rowError = true;
                $csvError .= 'blank house name';
                $comma = true;
            }

            if (empty($houseCsv->address)) {
                $rowError = true;
                $comma ? $csvError .= ', blank address' : $csvError .= 'blank address';
                $comma = true;
            }

            if (!$rowError) {
                $house = House::where(['house_name' => $houseCsv->house_name, 'house_detail' => $houseCsv->address, 'community_id' => $houseCsv->community_id])->first();

                $message = '';

                if (!empty($house)) {
                    $csvError = $comma ? $csvError . ', House already exist' : $csvError . 'House already exist';
                    $message = $csvError;
                    $status = "failure";
                } else {
                    $house = House::create([
                        'house_name' => $houseCsv->house_name,
                        'house_detail' => $houseCsv->address,
                        'lot_number' => $houseCsv->lot_number,
                        'phone' => $houseCsv->primary_contact,
                        'created_by' => $houseCsv->created_by,
                        'community_id' => $houseCsv->community_id
                    ]);

                    $message = 'Record imported Successfully';
                    $status = "success";

                }

                $houseCsv = HouseCsv::find($houseCsv->id);
                $houseCsv->mark = '1';
                $houseCsv->status = $status;
                $houseCsv->message = $message;
                $houseCsv->save();
            } else {
                $houseCsv = HouseCsv::find($houseCsv->id);
                $houseCsv->mark = '1';
                $houseCsv->status = 'failure';
                $houseCsv->message = $csvError;
                $houseCsv->save();
            }

            $count++;
        }


        $houseCsvFile = HouseCsvFile::where('file_name', $request->file_name)->first();

        $processedHouseCsvRecords = intval($houseCsvFile->process_records);
        $totalProcessRecords = $processedHouseCsvRecords + $count;
        $houseCsvFile->process_records = $totalProcessRecords;

        $houseCsvFile->save();

        return response()->json(['recordsExecuted' => $count]);
    }

    public function housesImportFileInformation(Request $request)
    {
        $data['fileName'] = $request->file_name;
        return view($this->folderLink . 'imported-file-information', $data);
    }

    public function housesImportFileInformationData(Request $request)
    {
        $fileData = [];

        if (!empty($request->file_name)) {

        }

        $houseCsvData = HouseCsv::where('file_name', $request->file_name)->get();

        return datatables()->of($houseCsvData)
            ->addColumn('house_name', function ($model) {
                return $model->house_name;
            })
            ->addColumn('address', function ($model) {
                return $model->address;
            })
            ->addColumn('lot_number', function ($model) {
                return $model->lot_number;
            })
            ->addColumn('primary_contact', function ($model) {
                return $model->primary_contact;
            })
            ->addColumn('status', function ($model) {
                return $model->status;
            })
            ->addColumn('message', function ($model) {
                return $model->message;
            })
            ->addColumn('links', function ($model) {

                $actionHTML = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if ($model->status == "failure") {
                    $actionHTML .= '<a class="dropdown-item" href="' . route('admin.residential.house-csvs-table-record-edit', $model->id) . '"><i class="nav-icon fas fa-edit" aria-hidden="true"></i> Edit </a>';
                }

                $actionHTML .= '
                    <form action="' . route('admin.residential.house-csvs-delete', [$model->id]) . '" method="POST" class="ajax-form">
                    ' . csrf_field() . '
                    <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                    </form>
                    </div>
                    </div>';

                return $actionHTML;
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('house_name') && !empty($request->house_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['house_name'], $request->house_name);
                    });
                }
                if ($request->has('full_address') && !empty($request->full_address)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['address'], $request->full_address);

                    });
                }
                if ($request->has('lot_number') && !empty($request->lot_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['lot_number'], $request->lot_number);
                    });
                }
                if ($request->has('phone') && !empty($request->phone)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['primary_contact'], $request->phone);
                    });
                }
                if ($request->has('status') && !empty($request->status)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['status'], $request->status);
                    });
                }
                if ($request->has('message') && !empty($request->message)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['message'], $request->message);
                    });
                }

            })
            ->rawColumns(['links'])
            ->make(true);
    }

    public function houseCsvsRecordEdit($id)
    {
        $data['houseCsv'] = HouseCsv::find($id);
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'house-csv-edit', $data);
    }


    public function houseCsvsRecordUpdate(Request $request, $id)
    {
        $request->validate([
            'house_name' => 'required',
            'address' => 'required',
            'lot_number' => 'required',
            'phone' => 'required'
        ]);

        $houseCsv = HouseCsv::find($id);
        $houseCsv->house_name = $request->house_name;
        $houseCsv->address = $request->address;
        $houseCsv->lot_number = $request->lot_number;
        $houseCsv->primary_contact = $request->phone;
        $houseCsv->mark = '0';
        $houseCsv->status = null;
        $houseCsv->message = null;
        $houseCsv->save();

        $houseCsvFile = HouseCsvFile::where('file_name', $houseCsv->file_name)->first();
        $currentProcessRecords = intval($houseCsvFile->process_records);
        $currentProcessRecords--;
        $houseCsvFile->process_records = $currentProcessRecords;
        $houseCsvFile->save();

        return response()->json(['status' => true, 'message' => 'Updated successfully', 'redirect' => route('admin.residential.house-imported-file-saved-records', ['file_name' => $houseCsv->file_name])]);
    }

    /**
     * To delete house_csv_files table record and house_csv table records of file
     *
     * @param $id
     * @return JsonResponse
     */
    public function houseCsvFilesDelete($id)
    {
        $houseCsvFile = HouseCsvFile::find($id);
        $houseCsvFile->delete();

        $houseCsv = HouseCsv::where('file_name', $houseCsvFile->file_name);
        $houseCsv->delete();

        return response()->json(['status' => true, 'message' => 'Deleted Successfully', 'redirect' => route('admin.residential.house-imported-files')]);
    }

    /*
     * To delete house_csvs table record
     *
     * */
    public function houseCsvsDelete($id)
    {
        $houseCsv = HouseCsv::find($id);
        $houseCsv->delete();

        $houseCsvFile = HouseCsvFile::where('file_name', $houseCsv->file_name)->first();
        $totalRecords = intval($houseCsvFile->total_records) - 1;
        $processedRecords = intval($houseCsvFile->process_records) - 1;
        $houseCsvFile->total_records = $totalRecords;
        $houseCsvFile->process_records = $processedRecords;
        $houseCsvFile->save();

        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.residential.houses-import-file-information', ['file_name' => $houseCsv->file_name])]);
    }

    /**
     * to dump completed_profile column of house
     *
     */

    public function houseCompletedProfileColumnDump()
    {
        $houses = House::where('house_members_dumped', 0)->limit(3000)->get();

        if (empty($houses)) {
            echo "All dumped successfully";
            exit;
        }

        foreach ($houses as $house) {

            $houseCompletedProfileStatus = "no";
            $houseResidents = User::where('house_id', $house->id)->whereIn('role_id', [4, 5])->get();
            foreach ($houseResidents as $houseResident) {
                if (!empty($houseResident->license_image)) {
                    $houseCompletedProfileStatus = "yes";
                }
            }

            $house->completed_profile = $houseCompletedProfileStatus;
            $house->house_members_dumped = 1;
            $house->save();
        }

        $houseRemaining = House::where('house_members_dumped', 0)->count();
        echo "records remaining: " . $houseRemaining;

    }

}
