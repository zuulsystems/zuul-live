<?php

namespace App\Http\Controllers\Admin;

use App\Services\DashboardSettingService;
use App\Http\Controllers\Controller;
use App\Models\DashboardSetting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use App\Models\Community;
use ReflectionFunction;

class DashboardSettingController extends Controller
{
    public $folderLink = 'admin.dashboard-setting.';
    public $dashboardSettingService;

    public function __construct(DashboardSettingService $dashboardSettingService)
    {
        $this->dashboardSettingService = $dashboardSettingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $data["dashboard_settings"] = $this->dashboardSettingService->getDashboardSetting($user_id);

        if (auth()->user()->role_id == 2 || auth()->user()->role_id == 12) {
            $data["community_settings"] = Community::where('id', auth()->user()->community_id)->first();
        }

        if (!count($data["dashboard_settings"])) {
            //these widgets are being inserted
            $dashboard_settings = array(
                array('widget_name' => 'guards', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.user.guards.index'), 'icon' => 'fa fa-user'),
                array('widget_name' => 'household_resident', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.residential.residents.index'), 'icon' => 'fa fa-users'),
                array('widget_name' => 'guest', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.user.guests.index'), 'icon' => 'fa fa-users'),
                array('widget_name' => 'community_admin', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.user.community-admins.index'), 'icon' => 'fa fa-users'),
                array('widget_name' => 'communities', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.residential.communities.index'), 'icon' => 'fa fa-users'),
                array('widget_name' => 'houses', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.residential.houses.index'), 'icon' => 'fa fa-home'),
                array('widget_name' => 'announcements', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.residential.announcements.index'), 'icon' => 'fa fa-bullhorn'),
                array('widget_name' => 'rfid_tags', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.rfid.rfids.index'), 'icon' => 'fa fa-tag'),
                array('widget_name' => 'vendors', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.vendors.index'), 'icon' => 'fa fa-truck'),
                array('widget_name' => 'vehicles', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.vehicles.index'), 'icon' => 'fa fa-car'),
                array('widget_name' => 'passes', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.passes.index'), 'icon' => 'fa fa-ticket-alt'),
                array('widget_name' => 'camera_locations', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.locations.index'), 'icon' => 'fa fa-camera'),
                array('widget_name' => 'guest_log', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.guest-logs.index'), 'icon' => 'fa fa-history'),
                array('widget_name' => 'rfid_tracking', 'color' => 'red', 'is_allowed' => 0, 'user_id' => $user_id, 'url' => route('admin.rfid.rfid-logs.index'), 'icon' => 'fa fa-map-marker')
            );

            //community both rights zuul rights and tl rights
            if (Gate::allows('is-zuul-right') || Gate::allows('is-tl-right')) {
                array_splice($dashboard_settings, 3, 2);
                array_splice($dashboard_settings, 9, 1);
            }

            $this->dashboardSettingService->insert($dashboard_settings);
        }

        $data['dashboardSettingsData'] = $this->dashboardSettingService->getDashboardSetting($user_id);
        return view($this->folderLink . 'index', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $widgets = $request->widgets;

        foreach ($widgets as $widget) {
            $color = $widget . "_color";
            $data["is_allowed"] = $request->$widget ? 1 : 0;
            $data["color"] = $request->$color;
            $result = $this->dashboardSettingService->updateDashboardSettings($data, auth()->user()->id, $widget);
        }
        $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.settings.dashboard-setting.index')];

        // user activity logs
        callUserActivityLogs("Update Dashboard Settings", __FUNCTION__, __FILE__, __DIR__);

        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Enable or Disable the Web Relay.
     *
     * @param Request $request
     * @return Response
     */
    public function enableWebRelay(Request $request)
    {
        $status = 0;
        if (isset($request->web_relay_available)) {
            $status = 1;
        }
        Community::where('id', auth()->user()->community_id)->update([
            'web_relay_available' => $status
        ]);
        $response = [
            'status' => true,
            'message' => 'Web Relay Availability Updated!',
            'redirect' => route('admin.settings.dashboard-setting.index')
        ];
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
