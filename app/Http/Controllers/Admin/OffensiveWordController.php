<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\OffensiveWordService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class OffensiveWordController extends Controller
{
    public $offensiveWordService;
    protected $folderLink = 'admin.offensive-words.';

    public function __construct(OffensiveWordService $offensiveWordService)
    {
        $this->offensiveWordService = $offensiveWordService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get offensive words list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function offensiveWordsData(Request $request)
    {
        return datatables()->of($this->offensiveWordService->all()['result'])
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('edit_link', function ($model) {
                return '<a href="' . route('admin.settings.offensive-words.edit', [$model->id]) . '">Edit </a>';
            })
            ->rawColumns(['edit_link'])
            ->filter(function ($instance) use ($request) {
                if (isset($request->name)) {
                    $instance->where('name', 'like', "%{$request->get('name')}%");
                }
            })
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => 'required|unique:offensive_words',
        ];
        $messages = [
            'name.required' => 'Please enter word',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $offensiveWordData = $request->all();
        $offensiveWordData['created_by'] = auth()->user()->id;
        $offensiveWordData['community_id'] = auth()->user()->community_id;

        $offensiveWord = $this->offensiveWordService->create($offensiveWordData);
        if ($offensiveWord['bool']) {
            // user activity logs
            callUserActivityLogs("Add Offensive Word", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.settings.offensive-words.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Add Offensive Word", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        return view($this->folderLink . 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $offensive_word = $this->offensiveWordService->find($id);
        if (!$offensive_word['bool']) {
            abort(500);
        }
        if (!$offensive_word['result']) {
            abort(404);
        }
        $data['offensive_word'] = $offensive_word['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'name' => 'required|unique:offensive_words,name,' . $id,
        ];
        $messages = [
            'name.required' => 'Please enter word',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $offensiveWordData = $request->all();
        $offensiveWordData['community_id'] = auth()->user()->community_id;

        $offensiveWord = $this->offensiveWordService->update($offensiveWordData, $id);
        if ($offensiveWord['bool']) {
            // user activity logs
            callUserActivityLogs("Edit Offensive Word", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.settings.offensive-words.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Edit Offensive Word", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
