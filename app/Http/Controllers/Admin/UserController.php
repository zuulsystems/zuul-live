<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use App\Services\UserService;

//use Dotenv\Validator;
use DB;
use Exception;
use http\Env\Response;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\BannedCommunityUser;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UserController extends Controller
{
    public $userService;
    protected $folderLink = 'admin.users.';

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * load guest list
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $data['communities'] = getCommunities();
        $data["userId"] = $request->userId ?? "";
        return view($this->folderLink . 'guests.index', $data);
    }

    public function bandGuest(Request $request)
    {
        $data["userId"] = $request->userId ?? "";
        return view($this->folderLink . 'guests.band', $data);
    }


    public function addbandGuest(Request $request)
    {
        $data = $request->all();

        $phoneNumberLength = 10;

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $data['dial_code'])->first();

        if (!empty($countryPhoneFormat)) {
            $phoneNumberLength = $countryPhoneFormat->digit;
        }

        $data['phone_number'] = str_replace("(", "", $data['phone_number']);
        $data['phone_number'] = str_replace(")", "", $data['phone_number']);
        $data['phone_number'] = str_replace("-", "", $data['phone_number']);

        $validations = [
            'phone_number' => 'required',
            'name' => 'required',
            'license_plate' => 'required'
        ];

        $messages = [
        ];

        $validator = Validator::make($data, $validations);

        if ($validator->fails()) {
            $validator->validate();
        }

        $validator->after(function ($validator) use ($data) {

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $data['dial_code'])->first();

            $phoneNumberLength = 10;

            if (!empty($countryPhoneFormat)) {
                $phoneNumberLength = $countryPhoneFormat->digit;
            }

            if (isset($data['phone_number']) && preg_match_all("/[0-9]/", $data['phone_number']) < $phoneNumberLength) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberLength . ' digit long');
            }
        });

        if ($validator->fails()) {
            $validator->validate();
        }


        $name = explode(' ', $data['name']);
        $first_name = $name[0];

        if (count($name) == 2) {
            $last_name = $name[1];
        } else {
            $last_name = "";
        }

        $user_data = User::where('phone_number', $data['phone_number'])->where('dial_code', 'like', '%' . $data['dial_code'])->first();

        $community_id = 1;

        if (auth()->user()->community_id != null) {
            $community_id = auth()->user()->community_id;
        } else {
            $community_id = $request->community;
        }


        $phone_number = $data['phone_number'];

        if (!empty($user_data)) {
            $user_id = $user_data->id;
        } else {
            $user_create = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone_number' => $phone_number,
                'dial_code' => Str::start($data['dial_code'], '+'),
                'role_id' => 7,
            ]);

            $user_id = $user_create->id;
        }

        $banned_user = BannedCommunityUser::where('user_id', $user_id)
            ->where('community_id', $community_id)
            ->where('phone_number', $phone_number)
            ->where('license_plate', $data['license_plate'])
            ->first();

        if ($banned_user == null) {
            $result = BannedCommunityUser::create([
                'user_id' => $user_id,
                'community_id' => $community_id,
                'phone_number' => $phone_number,
                'license_plate' => $data['license_plate']
            ]);

            // user activity logs
            callUserActivityLogs("Add Banned Guest", __FUNCTION__, __FILE__, __DIR__, true, "Guest banned successfully");

            return response()->json(['status' => true, 'message' => 'Guest banned successfully', 'redirect' => url('admin/user/banned-guests')]);

        } else {
            // user activity logs
            callUserActivityLogs("Add Banned Guest", __FUNCTION__, __FILE__, __DIR__, true, "User is already banned");

            return response()->json(['status' => true, 'message' => 'User is already banned', 'redirect' => url('admin/user/banned-guests')]);

        }
    }

    public function addbandGuestForm()
    {
        $data['countries'] = activeCountries();
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'guests.create', $data);
    }

    /**
     * get guest data
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function guestData(Request $request)
    {
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        $isCommunityAdmin = (auth()->user()->hasRole('super_admin')) ? true : false;
        return datatables()->of($this->userService->getGuestList($request, $isCommunityAdmin, $userId)['result'])
            ->addColumn('full_name', function ($model) {
                return $model->fullname ?? "-";
            })
            ->addColumn('email', function ($model) {
                return (!empty($model->email)) ? $model->email : "-";
            })
            ->addColumn('mailing_address', function ($model) {
                return (!empty($model->mailing_address)) ? $model->mailing_address : "-";
            })
            ->addColumn('role_name', function ($model) {
                return $model->role->name ?? "-";
            })
            ->addColumn('formatted_phone', function ($model) {
                return $model->formattedPhone ?? "-";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "-";
            })
            ->addColumn('links', function ($model) {
                $links = '<div class="dropdown">
                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Action
                      </button>
                      <div class="dropdown-menu">';
                if (!empty($model->temp_password)) {
                    $links .= '<form action="' . route('admin.user.resend-temporary-password-guest', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-key" aria-hidden="true"></i> Resend Temporary Password</button>
                                </form>';
                }
                $links .= '<a class="dropdown-item" href="' . route('admin.vehicles.index', ['userId' => $model->id]) . '"><i class="nav-icon fas fa-car" aria-hidden="true"></i> Vehicles </a>
                      <a class="dropdown-item guest-permission" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="nav-icon fas fa-user" aria-hidden="true"></i> Guests Permissions </a>
                      <a class="dropdown-item make-resident-model" data-id="' . $model->id . '"  href="javascript:void(0)"><i class="fa fa-user" aria-hidden="true"></i> Make Resident</a>';
                if (auth()->user()->hasRole('super_admin')) {
                    $links .= '<form action="' . route('admin.user.delete-guest', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"  onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                                </form>';
                    $links .= '<form action="' . route('admin.user.suspend-guest', [$model->id]) . '" method="POST" class="ajax-form">
                                ' . csrf_field() . '
                                <a class="dropdown-item banned-permission" data-id="' . $model->id . '"  href="javascript:void(0)" value="' . $model->id . '"><i class="nav-icon fas fa-user" aria-hidden="true"></i> Banned </a>

                            </form>';
                }

                $links .= '</div>
                    </div>';
                return $links;
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->full_name)) {
                    $instance->where(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $request->get('full_name') . '%');
                }
                if (isset($request->email)) {
                    $instance->where('email', 'like', "%{$request->get('email')}%");
                }
                if (isset($request->formatted_phone)) {
                    $formatted_phone = onlyPhoneDigits($request->get('formatted_phone'));
                    $instance->where('phone_number', 'like', "%{$formatted_phone}%");
                }
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    public function guestBandData(Request $request)
    {
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        $isCommunityAdmin = false;
        return datatables()->of($this->userService->getBandGuestList($request, $isCommunityAdmin, $userId)['result'])
            ->addColumn('communities', function ($model) {
                $datas = [];
                foreach ($model->bandCommunities as $community) {
                    array_push($datas, $community->community->name);
                }
                return implode(", ", $datas);
            })
            ->addColumn('full_name', function ($model) {
                return $model->fullname;
            })
            ->addColumn('role_name', function ($model) {
                return $model->role->name ?? "";
            })
            ->addColumn('formatted_phone', function ($model) {
                return (!empty($model->formatted_phone_by_dial_code) && $model->formatted_phone_by_dial_code != " ") ? $model->formatted_phone_by_dial_code : "-";
            })
            ->addColumn('license_plate', function ($model) {
                return (!empty($model->bandCommunities[0]['license_plate'])) ? $model->bandCommunities[0]['license_plate'] : "-";
            })
            ->editColumn('created_at', function ($model) {
                return $model->bandCommunities[0]['created_at']->diffForHumans();
            })
            ->addColumn('links', function ($model) {
                $links = '<div class="dropdown">
                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        Action
                      </button>
                      <div class="dropdown-menu">';
                $links .= '<form action="' . route('admin.user.unsuspend-guest', [$model->id]) . '" method="POST" class="ajax-form">
                                ' . csrf_field() . '
                                <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-unlock" aria-hidden="true"></i> Unbanned</button>
                            </form>';
                $links .= '</div>
                    </div>';
                return $links;
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->full_name)) {

                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return containsInsensitive($row['first_name'] . " " . $row['last_name'], $request->full_name);
                    });

                }
                if (isset($request->email)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return containsInsensitive($row['email'], $request->email);
                    });

                }
                if (isset($request->formatted_phone)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return containsInsensitive($row['formatted_phone'], $request->formatted_phone);
                    });
                }

                if (isset($request->license_plate)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return containsInsensitive($row['license_plate'], $request->license_plate);
                    });
                }

            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $user = $this->userService->delete($id);
        if ($user['bool']) {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.user.guests.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * delete resident
     * @param Request $request
     * @param $userId
     * @return JsonResponse
     */
    public function deleteGuest(Request $request, $userId)
    {
        $user = $this->userService->deleteUser($userId);
        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Delete Guest", __FUNCTION__, __FILE__, __DIR__, true, "Purged Successfully");

            $response = ['status' => true, 'message' => 'Purged Successfully', 'redirect' => route('admin.user.guests.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete Guest", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    public function suspendGuest(Request $request, $userId)
    {
        $user = $this->userService->suspendUser($userId);
        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Suspend Guest", __FUNCTION__, __FILE__, __DIR__, true, "Suspend Successfully");

            $response = ['status' => true, 'message' => 'Suspend Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Suspend Guest", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    public function unsuspendGuest(Request $request, $userId)
    {
        BannedCommunityUser::where([
            'user_id' => $userId
        ])->forcedelete();

        $user = $this->userService->unsuspendUser($userId);
        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Unbanned Guest", __FUNCTION__, __FILE__, __DIR__, true, "Unsuspend Successfully");

            $response = ['status' => true, 'message' => 'Unsuspend Successfully', 'redirect' => url('admin/user/banned-guests')];
        } else {
            // user activity logs
            callUserActivityLogs("Unbanned Guest", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Revoke Resident
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function revokeResident(Request $request)
    {
        $user = $this->userService->find($request->user_id);

        if ($user['result']->role_id == 7) {
            $this->userService->update(['role_id' => 5], $request->user_id);
        }

        $user['result']->revokePermission('head_of_family');
        $user['result']->revokePermission('can_manage_family');
        $user['result']->revokePermission('can_send_passes');
        $user['result']->revokePermission('allow_parental_control');

        $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.user.guests.index')];
        return response()->json($response);
    }

    /**
     * Send temporary password in email.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function resendTemporaryPassword($id)
    {
        $resident = $this->userService->find($id);
        if ($resident['bool']) {
            $emailData['user'] = $resident['result'];
            $emailData['type'] = 'resident';
            dynamicEmail($emailData, $resident['result']->email);
            $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.user.guests.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Check guest permission.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function checkBannedPermission(Request $request)
    {
        $user = $this->userService->find($request->id)['result'];
        if ($user) {
            $permission['isLicenseLocked'] = $user->hasPermission('is_license_locked');
            $response = ['status' => true, 'permission' => $permission];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }


    /**
     * Check guest permission.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function checkGuestPermission(Request $request)
    {
        $user = $this->userService->find($request->id)['result'];
        if ($user) {
            $permission['isLicenseLocked'] = $user->hasPermission('is_license_locked');
            $response = ['status' => true, 'permission' => $permission];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    /**
     * Assign guest permission.
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function assignBannedPermission(Request $request)
    {
        $userId = $request->banned_user_id;
        $communities = $request->communities;
        $user = $this->userService->find($userId)['result'];

        foreach ($communities as $community) {
            BannedCommunityUser::create([
                'user_id' => $userId,
                'community_id' => $community
            ]);
        }

        $user = $this->userService->suspendUser($userId);
        if ($user['bool']) {
            // user activity logs
            callUserActivityLogs("Banned on Community", __FUNCTION__, __FILE__, __DIR__, true, "Suspend Successfully");

            $response = ['status' => true, 'message' => 'Suspend Successfully', 'redirect' => route('admin.user.guards.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Banned on Community", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong Please try again.'];
        }
        return response()->json($response);

    }


    /**
     * Assign guest permission.
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function assignGuestPermission(Request $request)
    {
        $userId = $request->user_id;
        $user = $this->userService->find($userId)['result'];

        //assign permission is license locked
        if ($request->has('is_license_locked') && $request->is_license_locked == '1') {
            $user->assignPermission('is_license_locked');

            // user activity logs
            callUserActivityLogs("Guest Permissions", __FUNCTION__, __FILE__, __DIR__, true, "Assign Permission");
        } else {
            $user->revokePermission('is_license_locked');

            // user activity logs
            callUserActivityLogs("Guest Permissions", __FUNCTION__, __FILE__, __DIR__, true, "Revoke Permission");
        }

        return response()->json(['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.user.guests.index')]);
    }

    /**
     * Make Resident.
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function makeResident(Request $request)
    {
        $roleId = 4;
        //check if there is no head of family then make him head of the family
        $isHeadOfFamily = $this->userService->getHeadOfFamily($request->community_id, $request->house_id);
        if (!empty($isHeadOfFamily['result'])) {
            $roleId = 5;
        }
        //update user info
        $updateUser = $this->userService->update([
            'email' => $request->email,
            'community_id' => $request->community_id,
            'house_id' => $request->house_id,
            'role_id' => $roleId
        ], $request->user_id);

        if ($updateUser['bool']) {
            $user = $this->userService->find($request->user_id);
            //assign permissions
            if ($user['result']->hasRole('family_head')) {
                $user['result']->assignPermission('can_send_passes');
                $user['result']->assignPermission('can_manage_family');
                $user['result']->assignPermission('allow_parental_control');
            } else if ($user['result']->hasRole('family_member')) {
                $user['result']->assignPermission('can_send_passes');
            }
            //send email to guest to become a household member
            $emailData['user'] = $user['result'];
            guestToResidentEmail($emailData, $user['result']->email);

            // user activity logs
            callUserActivityLogs("Make Resident", __FUNCTION__, __FILE__, __DIR__, true, "Success");

            $response = ['status' => true, 'message' => 'Success', 'redirect' => route('admin.user.guests.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Make Resident", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong");

            $response = ['status' => false, 'message' => 'Something went wrong'];
        }
        return response()->json($response);
    }

    /**
     * Get User.
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function getUser(Request $request)
    {
        $user = $this->userService->find($request->id)['result'];
        if ($user) {
            $response = ['status' => true, 'user' => $user];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    public function getCommunity()
    {
        $user = Community::all();
        if ($user) {
            $response = ['status' => true, 'user' => $user];
        } else {
            $response = ['status' => false, 'message' => 'User Not Exist'];
        }
        return response()->json($response);
    }

    public function resetPassword(Request $request)
    {
        $code = $request->code;
        $password = $request->password;

        $pattern = '/^(?=.*\d)(?=.*[A-Z])(?=.*\W).{9,}$/';

        if (!preg_match($pattern, $password)) {
            return back()->with("error", "Minimum 9 characters required. Please enter atleast 1 uppercase, 1 number and 1 special character.");
        }

        $user = User::where('reset_verification_code', $code)->first();

        if ($user) {
            // Update the user's data
            $user->update([
                'password' => Hash::make($password), // Example: Updating the password column
                'reset_verification_code' => NULL,
                'is_reset_password' => 0
            ]);

            return back()->with("success", "Password reset successfully. Please login from your mobile app again!");

        } else {
            return back()->with("error", "Invalid code / Link expired. Try again!");
        }
    }

}
