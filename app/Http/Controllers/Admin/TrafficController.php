<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\TrafficTicket;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Services\LocationService;
use App\Services\TrafficTicketService;

class TrafficController extends Controller
{
    public $locationService;
    public $trafficTicketService;


    public function __construct(
        LocationService      $locationService,
        TrafficTicketService $trafficTicketService
    )
    {
        $this->locationService = $locationService;
        $this->trafficTicketService = $trafficTicketService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     *
     * @param int $id
     * @return Response
     */
    public function get_traffic_summary(Request $request)
    {
        try {
            $communityId = auth()->user()->community_id;
            $tickets = $this->trafficTicketService->getTickets($communityId, $request->location_id, $request->type)['result'];

            return $tickets;
        } catch (Exception $exception) {
            return 'error';
        }
    }

    /**
     * Locations
     *
     * @param int $id
     * @return Response
     */
    public function getCameraLocationMapData(Request $request)
    {
        $communityId = $request->communityId;

        $locations = $this->locationService->getLocations($communityId)["result"];

        $cameraLocation = [];
        foreach ($locations as $location) {
            $link = url('/') . '/admin/traffic_logix/locations?location_id=' . $location->location_id;
            $cameraLocation[] = array(
                $location->name,
                explode(',', str_replace(array('(', ')'), '', $location->geocode))[0],
                explode(',', str_replace(array('(', ')'), '', $location->geocode))[1],
                $location->id,
                $link,
                $location->total,
                $location->paid,
                $location->total - $location->paid,
                $location->location_id,
            );
        }

        return $cameraLocation;
    }


}
