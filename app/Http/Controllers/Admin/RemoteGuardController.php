<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ConnectedPie;
use App\Models\Network;
use App\Models\RemoteGuard;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class RemoteGuardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data["communityId"] = $request->communityId ?? '';
        $data['communities'] = getCommunities();
        $data['houseId'] = $request->houseId ?? '';
        return view('admin.remote-guard.index', $data);
    }

    public function remoteGuardData(Request $request)
    {
        $remoteGuards = RemoteGuard::query();

        if (!empty(auth()->user()->kioskPermission)) {
            $communityIds = explode(",", auth()->user()->kioskPermission->communities);
            $remoteGuards = $remoteGuards->whereIn('community_id', $communityIds);
        }

        return datatables()->of($remoteGuards)
            ->filter(function ($instance) use ($request) {
                if (!empty($request->identifier)) {
                    $instance->where('identifier', 'like', '%' . $request->identifier . '%');
                }
                if (!empty($request->attach_to)) {
                    $instance->where('attach_to', 'like', '%' . $request->attach_to . '%');
                }
                if (!empty($request->meta_name)) {
                    $instance->where('meta_name', 'like', '%' . $request->meta_name . '%');
                }
                if (!empty($request->zuul_key)) {
                    $instance->where('zuul_key', 'like', '%' . $request->zuul_key . '%');
                }
                if (!empty($request->zuul_secret)) {
                    $instance->where('zuul_secret', 'like', '%' . $request->zuul_secret . '%');
                }
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" onclick="viewKeys(' . $model->id . ')"><i class="fa fa-key" aria-hidden="true"></i> View Keys </a>
                            <a class="dropdown-item" href="' . route('admin.remote-guard.edit', $model->id) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                            <form action="' . route('admin.remote-guard.destroy', $model->id) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->toJson();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $remoteGuardData = $request->all();
        $remoteGuardData['attach_to'] = null;
        $remoteGuardData['mac_address_id'] = null;


        $rules = [
            'identifier' => 'required'
        ];
        $messages = [
            'identifier.required' => 'Please enter identifier'
        ];

        if ($remoteGuardData['attach_to'] == "user") {
            $rules['phone_number'] = 'required';
            $messages['phone_number.required'] = ' Please enter phone number';
        } else if ($remoteGuardData['attach_to'] == "network") {
            $rules['ip_address'] = 'required';
            $messages['ip_address.required'] = ' Please enter ip address';
        }


        $validator = Validator::make($remoteGuardData, $rules, $messages);

        if ($validator->fails()) {
            $validator->validate();
        }

        $connectedPie = '';
        $isValueFromDropDown = true;

        $remoteGuardData['phone_number'] = trim(str_replace("-", "", $remoteGuardData['phone_number']));

        if ($remoteGuardData['attach_to'] == "device") {

            $connectedPie = ConnectedPie::where('id', $remoteGuardData['mac_address_id'])->first();

            if ($connectedPie == null) {
                $isValueFromDropDown = false;
                $connectedPieNew = ConnectedPie::create([
                    'mac_address' => $remoteGuardData['mac_address_id'],
                    'community_id' => auth()->user()->community_id,
                ]);

                $connectedPie = ConnectedPie::where('id', $connectedPieNew->id)->first();
            }

            $macAddress = $connectedPie->mac_address;
            $remoteGuard = RemoteGuard::where('meta_name', $macAddress)->first();
            if (!empty($remoteGuard)) {
                $validator->errors()->add('mac_address_id', 'Sorry! Mac address exist');
            }
        } else if ($remoteGuardData['attach_to'] == "user") {

            $guard = User::where('phone_number', $remoteGuardData['phone_number'])->whereIn('role_id', [8, 11])->first();
            if (empty($guard)) {
                $validator->errors()->add('phone_number', 'Sorry! phone number does not belong to any guard');
            } else if (!empty(auth()->user()->kioskPermission)) {
                $communityIds = explode(",", auth()->user()->kioskPermission->communities);
                $userBelongsToCommunity = false;
                if (count($communityIds) > 0) {
                    foreach ($communityIds as $communityId) {
                        $guard->community_id == $communityId ? $userBelongsToCommunity = true : "";
                    }
                }

                if (!$userBelongsToCommunity) {
                    $validator->errors()->add('phone_number', 'Sorry! Guard have different community');
                }

            }

            $remoteGuard = RemoteGuard::where('meta_name', $remoteGuardData['phone_number'])->first();
            if (!empty($remoteGuard)) {
                $validator->errors()->add('phone_number', 'Sorry! phone number exist');
            }

        } else if ($remoteGuardData['attach_to'] == "network") {
            $network = Network::where('registered_ip', $remoteGuardData['ip_address'])->first();
            if (empty($network)) {
                $validator->errors()->add('ip_address', 'Sorry! IP Address not found');
            } else if (!empty(auth()->user()->kioskPermission)) {
                $communityIds = explode(",", auth()->user()->kioskPermission->communities);
                $network = Network::where('registered_ip', $remoteGuardData['ip_address'])->whereIn('community_id', $communityIds)->count();
                if ($network == 0) {
                    $validator->errors()->add('ip_address', 'Sorry! IP belongs to different community');
                }
            }

            $remoteGuard = RemoteGuard::where('meta_name', $remoteGuardData['ip_address'])->first();
            if (!empty($remoteGuard)) {
                $validator->errors()->add('ip_address', 'Sorry! An IP Address exist');
            }
        }


        if ($validator->fails()) {
            $validator->validate();
        }

        $metaId;
        $metaName;
        $communityId;

        if ($remoteGuardData['attach_to'] == "device") {
            $metaId = 0;

            if ($isValueFromDropDown) {
                $connectedPie = ConnectedPie::where('id', $remoteGuardData['mac_address_id'])->first();
                $metaId = $connectedPie->id;
            } else {
                $connectedPie = ConnectedPie::where('mac_address', $remoteGuardData['mac_address_id'])->first();
                $metaId = $connectedPie->id;
            }

            $metaName = $connectedPie->mac_address;
            $communityId = $connectedPie->community_id;

        } else if ($remoteGuardData['attach_to'] == "user") {
            $user = User::where('phone_number', $remoteGuardData['phone_number'])->first();
            $metaId = $user->id;
            $metaName = $remoteGuardData['phone_number'];
            $communityId = $user->community_id;

        } else if ($remoteGuardData['attach_to'] == "network") {
            $network = Network::where('registered_ip', $remoteGuardData['ip_address'])->first();
            $metaId = $network->id;
            $metaName = $remoteGuardData['ip_address'];
            $communityId = $network->community_id;

        }

        if (auth()->user()->role_id == 1) {
            $communityId = $remoteGuardData['community_id'];
        }


        $str = uniqid();
        $zuulkey = md5($str);

        $str = uniqid();
        $zuulSecret = md5($str);

        $remoteGuard = new RemoteGuard;
        $remoteGuard->identifier = $remoteGuardData['identifier'];
        $remoteGuard->attach_to = $remoteGuardData['attach_to'];
        $remoteGuard->meta_id = $metaId;
        $remoteGuard->meta_name = $metaName;
        $remoteGuard->community_id = ($communityId != null) ? $communityId : auth()->user()->community_id;
        $remoteGuard->zuul_key = $zuulkey;
        $remoteGuard->zuul_secret = $zuulSecret;
        $remoteGuard->save();

        return response()->json(['status' => true, 'message' => 'Remote guard added successfully', 'redirect' => route('admin.remote-guard.index')]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['connectedPies'] = ConnectedPie::query();
        $data['networks'] = Network::query();
        $data['communities'] = getCommunities();

        if (!empty(auth()->user()->community_id)) {
            $data['connectedPies'] = $data['connectedPies']->where('community_id', auth()->user()->community_id);
        } else if (!empty(auth()->user()->kioskPermission)) {
            $communityIds = explode(",", auth()->user()->kioskPermission->communities);
            $data['connectedPies'] = $data['connectedPies']->whereIn('community_id', $communityIds);
        }

        $data['connectedPies'] = $data['connectedPies']->get();

        return view('admin.remote-guard.create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['remoteGuard'] = RemoteGuard::find($id);

        $data['connectedPies'] = ConnectedPie::query();

        if (!empty(auth()->user()->community_id)) {
            $data['connectedPies'] = $data['connectedPies']->where('community_id', auth()->user()->community_id);
        } else if (!empty(auth()->user()->kioskPermission)) {
            $communityIds = explode(",", auth()->user()->kioskPermission->community_id);
            $data['connectedPies'] = $data['connectedPies']->whereIn('community_id', $communityIds);
        }

        $data['connectedPies'] = $data['connectedPies']->get();

        return view('admin.remote-guard.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $remoteGuardData = $request->all();

        $rules = [
            'identifier' => 'required'
        ];
        $messages = [
            'identifier.required' => 'Please enter identifier'
        ];

        if ($remoteGuardData['attach_to'] == "user") {
            $rules['phone_number'] = 'required';
            $messages['phone_number.required'] = ' Please enter phone number';
        } else if ($remoteGuardData['attach_to'] == "network") {
            $rules['ip_address'] = 'required';
            $messages['ip_address.required'] = ' Please enter ip address';
        }

        $validator = Validator::make($remoteGuardData, $rules, $messages);

        if ($validator->fails()) {
            $validator->validate();
        }

        $connectedPie = '';
        $isValueFromDropDown = true;

        $remoteGuardData['phone_number'] = trim(str_replace("-", "", $remoteGuardData['phone_number']));

        $remoteGuardData['id'] = $id;


        if ($remoteGuardData['attach_to'] == "device") {
            $connectedPie = ConnectedPie::where('id', $remoteGuardData['mac_address_id'])->first();

            if ($connectedPie == null) {
                $isValueFromDropDown = false;
                $connectedPieNew = ConnectedPie::create([
                    'mac_address' => $remoteGuardData['mac_address_id'],
                    'community_id' => auth()->user()->community_id,
                ]);

                $connectedPie = ConnectedPie::where('id', $connectedPieNew->id)->first();
            }

            $macAddress = $connectedPie->mac_address;
            $remoteGuard = RemoteGuard::where('meta_name', $macAddress)->where('id', '!=', $remoteGuardData['id'])->first();
            if (!empty($remoteGuard)) {
                $validator->errors()->add('mac_address_id', 'Sorry! Mac address exist');
            }
        } else if ($remoteGuardData['attach_to'] == "user") {

            $guard = User::where('phone_number', $remoteGuardData['phone_number'])->whereIn('role_id', [8, 11])->first();
            if (empty($guard)) {
                $validator->errors()->add('phone_number', 'Sorry! phone number does not belong to any guard');
            } else if (!empty(auth()->user()->kioskPermission)) {
                $communityIds = explode(",", auth()->user()->kioskPermission->communities);
                $userBelongsToCommunity = false;
                if (count($communityIds) > 0) {
                    foreach ($communityIds as $communityId) {
                        $guard->community_id == $communityId ? $userBelongsToCommunity = true : "";
                    }
                }

                if (!$userBelongsToCommunity) {
                    $validator->errors()->add('phone_number', 'Sorry! Guard have different community');
                }

            }


            $remoteGuard = RemoteGuard::where('meta_name', $remoteGuardData['phone_number'])->where('id', '!=', $remoteGuardData['id'])->first();
            if (!empty($remoteGuard)) {
                $validator->errors()->add('phone_number', 'Sorry! phone number exist');
            }

        } else if ($remoteGuardData['attach_to'] == "network") {
            $network = Network::where('registered_ip', $remoteGuardData['ip_address'])->first();
            if (empty($network)) {
                $validator->errors()->add('ip_address', 'Sorry! IP Address not found');
            } else if (!empty(auth()->user()->kioskPermission)) {
                $communityIds = explode(",", auth()->user()->kioskPermission->communities);
                $network = Network::where('registered_ip', $remoteGuardData['ip_address'])->whereIn('community_id', $communityIds)->count();
                if ($network == 0) {
                    $validator->errors()->add('ip_address', 'Sorry! IP belongs to different community');
                }
            }

            $remoteGuard = RemoteGuard::where('meta_name', $remoteGuardData['ip_address'])->where('id', '!=', $remoteGuardData['id'])->first();
            if (!empty($remoteGuard)) {
                $validator->errors()->add('ip_address', 'Sorry! IP Address exist');
            }
        }

        if ($validator->fails()) {
            $validator->validate();
        }

        $metaId;
        $metaName;
        $communityId;

        if ($remoteGuardData['attach_to'] == "device") {
            if ($isValueFromDropDown) {
                $connectedPie = ConnectedPie::where('id', $remoteGuardData['mac_address_id'])->first();
            } else {
                $connectedPie = ConnectedPie::where('mac_address', $remoteGuardData['mac_address_id'])->first();
            }

            $metaId = $connectedPie->id;


            $metaName = $connectedPie->mac_address;
            $communityId = $connectedPie->community_id;
        } else if ($remoteGuardData['attach_to'] == "user") {
            $user = User::where('phone_number', $remoteGuardData['phone_number'])->first();
            $metaId = $user->id;
            $metaName = $remoteGuardData['phone_number'];
            $communityId = $user->community_id;
        } else if ($remoteGuardData['attach_to'] == "network") {
            $network = Network::where('registered_ip', $remoteGuardData['ip_address'])->first();
            $metaId = $network->id;
            $metaName = $remoteGuardData['ip_address'];
            $communityId = $network->community_id;
        }

        $remoteGuard = RemoteGuard::find($id);
        $remoteGuard->identifier = $remoteGuardData['identifier'];
        $remoteGuard->attach_to = $remoteGuardData['attach_to'];
        $remoteGuard->meta_id = $metaId;
        $remoteGuard->meta_name = $metaName;
        $remoteGuard->community_id = ($communityId == null) ? $communityId : auth()->user()->community_id;
        $remoteGuard->save();

        return response(['status' => true, 'message' => 'Remote guard updated successfully', 'redirect' => route('admin.remote-guard.index')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $remoteGuard = RemoteGuard::find($id);
        $remoteGuard->delete();

        return response()->json(['status' => true, 'message' => 'Remote guard deleted successfully', 'redirect' => route('admin.remote-guard.index')]);
    }

    public function getRemoteGuardKeys(Request $request)
    {
        $data["remoteGuard"] = RemoteGuard::select('zuul_key', 'zuul_secret')->where('id', $request->id)->first();
        return response()->json($data);
    }
}
