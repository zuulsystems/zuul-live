<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\PassRecipient\PassCollection;
use App\Services\PassService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PassController extends Controller
{
    public $passService;
    protected $folderLink = 'admin.passes.';

    public function __construct(PassService $passService)
    {
        $this->passService = $passService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (Gate::any(['super-admin', 'community-admin', 'is-zuul-right'])) {
            $data["houseId"] = $request->houseId ?? '';
            return view($this->folderLink . 'index', $data);
        }
        return abort(403, 'This action is unauthorized.');
    }

    /**
     * get passes list
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function passesData(Request $request)
    {

        $houseId = null;
        if ($request->has('houseId') && !empty($request->houseId)) {
            $houseId = $request->houseId;
        }

        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        return datatables()->of($this->passService->getPassList(getCommunityIdByUser(), $houseId, $userId)['result'])
            ->addColumn('created_by_name', function ($model) {
                return $model->createdBy->fullName ?? "-";
            })
            ->addColumn('community_name', function ($model) {
                return (!empty($model->community->name)) ? $model->community->name : "-";
            })
            ->addColumn('description', function ($model) {
                return (!empty($model->description)) ? $model->description : "-";
            })
            ->addColumn('formatted_pass_start_date', function ($model) {
                $convertedPassStartDate = $model->convertedPassStartDate();
                return (!empty($convertedPassStartDate)) ? date('M j, Y g:i A', $convertedPassStartDate) : "";
            })
            ->addColumn('formatted_pass_date', function ($model) {
                $convertedPassDate = $model->convertedPassDate();
                return (!empty($convertedPassDate)) ? date('M j, Y g:i A', $convertedPassDate) : "";
            })
            ->editColumn('created_at', function ($model) {

                //Mark:- Just making a temporary condition for now.
                $convertedPassStartDate = $model->convertedPassStartDate();
                return (!empty($convertedPassStartDate)) ? date('M j, Y g:i A', $convertedPassStartDate) : "";

            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($where) use ($request) {
                        $where->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
                if (isset($request->sender_name)) {
                    $names = explode(" ", $request->get('sender_name'));
                    $instance->whereHas('createdBy', function ($where) use ($request, $names) {
                        $where->where('first_name', $names);
                        $where->orWhere(function ($query) use ($names) {
                            $query->whereIn('last_name', $names);
                        });
                    });
                }
                if (isset($request->recipient_name)) {
                    $recipient_names = explode(" ", $request->get('recipient_name'));
                    $instance->whereHas('passUsers.user', function ($where) use ($request, $recipient_names) {
                        $where->where('first_name', $recipient_names);
                        $where->orWhere(function ($query) use ($recipient_names) {
                            $query->whereIn('last_name', $recipient_names);
                        });
                    });
                }
                if (isset($request->formatted_pass_start_date)) {
                    $instance->whereDate('pass_start_date', '=', $request->get('formatted_pass_start_date'));
                }
                if (isset($request->formatted_pass_date)) {
                    $instance->whereDate('pass_date', '=', $request->get('formatted_pass_date'));
                }
            })
            ->toJson();
    }

    /**
     * get passes recipients
     * @param Request $request
     * @return JsonResponse
     */
    public function passRecipients(Request $request)
    {
        $result = $this->passService->getPassRecipients($request->passId);
        return response()->json([
            'data' => new PassCollection($result)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function passLogs()
    {

        return view('admin.passes.passes_logs');
    }
}
