<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\Location;
use App\Models\RemoteGuard;
use App\Services\CommunityService;
use App\Services\LocationService;
use App\Services\TimezoneService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CommunityController extends Controller
{
    public $communityService;
    public $timezoneService;
    public $locationService;
    protected $folderLink = 'admin.communities.';

    public function __construct(
        CommunityService $communityService,
        TimezoneService  $timezoneService,
        LocationService  $locationService
    )
    {
        $this->communityService = $communityService;
        $this->timezoneService = $timezoneService;
        $this->locationService = $locationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get communities list
     */
    public function communityData(Request $request)
    {
        return datatables()->of($this->communityService->communityList()['result'])
            ->addColumn('links', function ($model) {

                $title = '';
                $icon_type = '';
                $remote_guard_id = '';
                $view_key_style = '';
                if ($model->community_status == 'active') {
                    $title = 'Active';
                    $icon_type = 'fa-toggle-on';
                } else {
                    $title = 'Deactive';
                    $icon_type = 'fa-toggle-off';
                }

                if (!empty($model->remoteGuard)) {
                    $remote_guard_id = $model->remoteGuard->id;
                }

                if ($remote_guard_id == '') {
                    $view_key_style = 'display:none';
                }

                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                               <a class="dropdown-item" href="' . route('admin.residential.houses.index', ['communityId' => $model->id]) . '"><i class="nav-icon fas fa-home" aria-hidden="true"></i> View Houses (' . $model->houses_count . ') </a>
                                <a class="dropdown-item" href="' . route('admin.residential.communities.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                                <form action="' . route('admin.residential.communities.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                   <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>

                                  <form action="' . route('admin.residential.fetch-community-locations', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-camera" aria-hidden="true"></i>  Fetch Location</button>
                                </form>
                                <a class="dropdown-item" href="' . route('admin.residential.change-community-status', [$model->id]) . '"><i class="fa ' . $icon_type . '" aria-hidden="true"></i>  ' . $title . ' </a>
                                <a class="dropdown-item"  onclick="viewKeys(' . $remote_guard_id . ')" style="' . $view_key_style . '"><i class="fa fa-key" aria-hidden="true"></i> View Keys </a>
                            </div>
                        </div>';
            })
            ->addColumn('house_count_edit', function ($model) {

                if ($model->houses_count > 0) {
                    return '<a href="' . route('admin.residential.houses.index', ['communityId' => $model->id]) . '">' . $model->houses_count . '</a>';
                }
                return 0;
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->name)) {
                    $instance->where('name', 'like', "%{$request->get('name')}%");
                }
                if (isset($request->address)) {
                    $instance->where('address', 'like', "%{$request->get('address')}%");
                }
            })
            ->rawColumns(['links', 'house_count_edit'])
            ->toJson();
    }

    /**
     * fetch community locations
     * @param Request $request
     * @param $communityId
     * @return JsonResponse
     * @throws Exception
     */
    public function fetchCommunityLocations(Request $request, $communityId)
    {
        $response = $this->fetchLocationsByCommunity($communityId);
        return response()->json($response);
    }

    /**
     * fetch location by companyId and apiKey
     * @param $communityId
     * @return array
     * @throws Exception
     */
    private function fetchLocationsByCommunity($communityId)
    {

        $community = $this->communityService->find(($communityId))['result'];
        if (empty($community))
            return ['status' => false, 'message' => 'No Record Found'];
        // Fetch Location of Single Community

        // Relative URL of Companies
        $api_url = '/locations/' . $community->company_id;

        // API Result from Data
        $data = $this->fetchAPIData($api_url, $community->key);

        // If Fetching Result was Successful
        if ($data['bool']) {

            // Check If Location Exists, If not, Create it
            foreach ($data['result']['locations'] as $location) {

                // Check if Location Already Exists, IF not, Create It
                if (is_null(Location::where('location_id', $location['lid'])->first())) {
                    // Create/Save Location
                    $this->locationService->create([
                        'location_id' => $location['lid'],
                        'name' => $location['name'],
                        'address' => $location['address'],
                        'city' => $location['city'],
                        'state' => $location['state'],
                        'country' => $location['country'],
                        'zip' => $location['zip'],
                        'geocode' => $location['geocode'],
                        'group_id' => $location['group_id'],
                        'direction' => $location['direction'],
                        'timezone_id' => $location['timezone_id'],
                        'tz' => $location['tz'],
                        'polygon' => $location['polygon'],
                        'community_id' => $communityId,
                    ]);
                }
            }

            //Mark:-Tracking Activity
            callUserActivityLogs("Fetch Location", __FUNCTION__, __FILE__, __DIR__, true, "New Location Added Successfully");

            return ['status' => true, 'message' => 'New Location Added'];
        } else {

            //Mark:-Tracking Activity
            callUserActivityLogs("Fetch Location", __FUNCTION__, __FILE__, __DIR__, false, "Fetching Locations Failed.");

            return ['status' => false, 'message' => 'Fetching Locations Failed.'];
        }

    }

    /**
     * Fetch Data from API
     *
     * @param String $api_url Relative URL to be fetched
     * @param String $api_key API Key
     * @param array $options (optional) Additional Guzzle Options
     * @return array
     */
    public function fetchAPIData($api_url, $api_key, $options = [])
    {

        // Default Guzzle Options
        $guzzle_options = [
            'base_uri' => 'https://api.streetsoncloud.com/v1/',
            'defaults' => [
                'verify' => false
            ]
        ];

        // If User provided other options, marge them with our current guzzle options
        if (!empty($options)) {
            $guzzle_options = array_merge($guzzle_options, $options);
        }

        // Create Guzzle Request with Relative URL
        $response = Http::withOptions($guzzle_options)->withHeaders([
            'X-Api-Key' => $api_key
        ])->get($api_url);

        // Return The Result
        return [
            'bool' => $response->ok(),
            'result' => $response->ok() ? $response->json() : []
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $timezones = $this->timezoneService->all();
        if (!$timezones['bool']) {
            abort(500);
        }
        $data['timezones'] = $timezones['result'];
        $data['countries'] = activeCountries();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * fetch community locations
     * @param Request $request
     * @param $communityId
     * @return JsonResponse
     * @throws Exception
     */
    public function changeCommunityStatus($communityId)
    {
        $communityData = Community::where('id', $communityId)->select('community_status')->get();
        $status = ($communityData[0]->community_status == 'active') ? 'inactive' : 'active';

        $updateQuery = Community::where('id', $communityId)->update([
            'community_status' => $status
        ]);

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'name' => 'required',
            'rfid_site_code' => 'required',
            'timezone_id' => 'required',
            'contact_number' => 'required',
            'community_image' => 'nullable|image|mimes:jpeg,png,jpg',
        ];
        $messages = [
            'name.required' => 'Please enter community name',
            'timezone_id.required' => 'Please select Timezone',
            'contact_number.required' => 'Please enter contact number'
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $communityData = $request->all();
        //upload community logo
        if ($request->hasFile('community_image')) {
            $fileName = 'community_' . time() . '.' . $request->community_image->extension();
            $request->community_image->move(public_path('images/communities'), $fileName);
            $communityData['community_logo'] = $fileName;
        }

        if (!$request->web_relay_available) {
            $communityData['web_relay_available'] = 0;
        }

        $community = $this->communityService->update($communityData, $id);

        if (auth()->user()->role_id == 1) {
            if (isset($community1->remoteGuard)) {
                RemoteGuard::where('community_id', $id)->update([
                    'zuul_key' => $request->zuul_key,
                    'zuul_secret' => $request->zuul_secret
                ]);
            }
        }

        if ($community['bool']) {
            $response = ['status' => true, 'message' => 'Edit Successfully', 'redirect' => route('admin.residential.communities.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => 'required',
            'rfid_site_code' => 'required',
            'timezone_id' => 'required',
            'contact_number' => 'required',
            'community_image' => 'nullable|image|mimes:jpeg,png,jpg',
        ];
        $messages = [
            'name.required' => 'Please enter community name',
            'timezone_id.required' => 'Please select Timezone',
            'contact_number.required' => 'Please enter contact number'
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $communityData = $request->all();
        $communityData['created_by'] = auth()->user()->id;

        //upload community logo
        if ($request->hasFile('community_image')) {
            $fileName = 'community_' . time() . '.' . $request->community_image->extension();
            $request->community_image->move(public_path('images/communities'), $fileName);
            $communityData['community_logo'] = $fileName;
        }

        if (!$request->web_relay_available) {
            $communityData['web_relay_available'] = 0;
        }

        $community = $this->communityService->create($communityData);
        if ($community['bool']) {

            //Mark:- Creating Remote Guard Everytime when the community is created
            $community = $community['result'];
            $this->createRemoteGuard($community);

            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.residential.communities.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Creating Remote Guard.
     *
     * @param int $id
     * @return Response
     */
    public function createRemoteGuard($community)
    {

        $str = uniqid();
        $zuulkey = md5($str);

        $str = uniqid();
        $zuulSecret = md5($str);

        $remoteGuard = new RemoteGuard;
        $remoteGuard->identifier = $community->name ?? "";
        $remoteGuard->attach_to = "device";
        $remoteGuard->meta_id = 1;
        $remoteGuard->meta_name = 0;
        $remoteGuard->community_id = $community->id ?? 0;
        $remoteGuard->zuul_key = $zuulkey;
        $remoteGuard->zuul_secret = $zuulSecret;
        $remoteGuard->save();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $timezones = $this->timezoneService->all();
        if (!$timezones['bool']) {
            abort(500);
        }
        $data['timezones'] = $timezones['result'];
        $community = $this->communityService->find($id);
        if (!$community['bool']) {
            abort(500);
        }
        if (!$community['result']) {
            abort(404);
        }
        $data['community'] = $community['result'];
        $data['countries'] = activeCountries();
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $community = $this->communityService->delete($id);
        if ($community['bool']) {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.residential.communities.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }
}
