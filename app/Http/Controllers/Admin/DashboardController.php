<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CsvImport;
use App\Models\User;
use App\Services\AnnouncementService;
use App\Services\HouseService;
use App\Services\CommunityService;
use App\Services\PassService;
use App\Services\PassUserService;
use App\Services\RfidLogService;
use App\Services\RfidService;
use App\Services\VehicleService;
use App\Services\DashboardSettingService;
use App\Services\DashboardService;
use App\Services\UserService;
use App\Services\LocationService;
use App\Services\ScanLogService;
use App\Services\AdminPanelNotificationService;
use App\Services\TrafficTicketService;
use App\Services\VendorService;
use App\Models\Widget;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;
use App\Models\ScanLog;
use Illuminate\Validation\ValidationException;

class DashboardController extends Controller
{
    public $dashboardService;
    public $dashboardSettingsService;
    public $userService;
    public $announcementService;
    public $houseService;
    public $communityService;
    public $passService;
    public $vehicleService;
    public $locationService;
    public $passUserService;
    public $scanLogService;
    public $adminPanelNotificationService;
    public $trafficTicketService;
    public $rfidService;
    public $rfidLogsService;
    public $vendorService;
    protected $folderLink = 'admin.dashboards.';

    public function __construct(DashboardService              $dashboardService,
                                DashboardSettingService       $dashboardSettingsService,
                                UserService                   $userService,
                                AnnouncementService           $announcementService,
                                HouseService                  $houseService,
                                CommunityService              $communityService,
                                PassService                   $passService,
                                VehicleService                $vehicleService,
                                LocationService               $locationService,
                                PassUserService               $passUserService,
                                ScanLogService                $scanLogService,
                                AdminPanelNotificationService $adminPanelNotificationService,
                                TrafficTicketService          $trafficTicketService,
                                RfidService                   $rfidService,
                                RfidLogService                $rfidLogsService,
                                VendorService                 $vendorService)
    {
        $this->dashboardService = $dashboardService;
        $this->dashboardSettingsService = $dashboardSettingsService;
        $this->userService = $userService;
        $this->announcementService = $announcementService;
        $this->houseService = $houseService;
        $this->communityService = $communityService;
        $this->passService = $passService;
        $this->vehicleService = $vehicleService;
        $this->locationService = $locationService;
        $this->passUserService = $passUserService;
        $this->scanLogService = $scanLogService;
        $this->adminPanelNotificationService = $adminPanelNotificationService;
        $this->trafficTicketService = $trafficTicketService;
        $this->rfidService = $rfidService;
        $this->rfidLogsService = $rfidLogsService;
        $this->vendorService = $vendorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     * @throws Exception
     */
    public function index()
    {
        $data['widgets'] = $this->dashboardSettingsService->getWidgets(auth()->user()->id);
        $data['widget_names']['communities'] = $this->communityService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['houses'] = $this->houseService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['announcements'] = $this->announcementService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['vehicles'] = $this->vehicleService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['passes'] = $this->passService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['guards'] = $this->userService->totalUser(getCommunityIdByUser(), [8, 11])['result']; // guard , guard admin
        $data['widget_names']['household_resident'] = $this->userService->totalUser(getCommunityIdByUser(), [4, 5])['result']; //resident, head of the household
        $data['widget_names']['guest'] = $this->userService->totalUser(getCommunityIdByUser(), 7)['result'];
        $data['widget_names']['community_admin'] = $this->userService->totalUser(getCommunityIdByUser(), 2)['result'];
        $data['widget_names']['vendors'] = $this->vendorService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['camera_locations'] = $this->locationService->all(getCommunityIdByUser())['result']->count();
        // $data['widget_names']['guest_log'] = $this->passUserService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['rfid_tags'] = $this->rfidService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['rfid_tracking'] = $this->rfidLogsService->all(getCommunityIdByUser())['result']->count();
        $data['widget_names']['guest_log'] = 0;
        $data['cameraCommunityList'] = $this->communityService->cameraCommunityList(getCommunityIdByUser())['result'];

        return view('admin.dashboard', $data);
    }

    /**
     * get dashboards list
     * @return mixed
     * @throws Exception
     */
    public function dashboardData(Request $request)
    {
        return datatables()->of($this->dashboardService->all())
            ->addColumn('community_name', function ($model) {
                return $model->community->name ?? "";
            })
            ->editColumn('created_at', function ($model) {
                return (!empty($model->created_at)) ? $model->created_at->diffForHumans() : "";
            })
            ->addColumn('edit_link', function ($model) {
                return '<a href="' . route('admin.settings.dashboards.edit', [$model->id]) . '">Edit </a>';
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['first_name'], $request->get('name')) ? true : false;
                    });
                }
                if (isset($request->mac_address)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['mac_address'], $request->get('mac_address')) ? true : false;
                    });
                }
                if (isset($request->community_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['community_name'], $request->get('community_name')) ? true : false;
                    });
                }
            })
            ->rawColumns(['edit_link'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validation = [
            'mac_address' => 'required|unique:dashboards',
            'community_id' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $dashboardData = $request->all();
        $dashboardData['secret_key'] = \Str::random(10);
        $community = $this->dashboardService->create($dashboardData);
        if ($community) {
            if ($community['bool']) {
                $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.settings.dashboards.index')];
            } else {
                $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
            }
            return response()->json($response);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $data['widgets'] = Widget::where('community_right', 'zuul')->get();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = [
            'mac_address' => 'required|unique:dashboards,mac_address,' . $id,
            'community_id' => 'required'
        ];
        $messages = [
            'mac_address.required' => 'Please enter mac address',
            'community_id.required' => 'Please select community',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $dashboardData = $request->all();
        $dashboardData['secret_key'] = \Str::random(10);

        $dashboardData['enable'] = isset($request->enable) ? '1' : '0';

        $dashboard = $this->dashboardService->update($dashboardData, $id);
        if ($dashboard['bool']) {
            $response = ['status' => true, 'message' => 'Update Successfully', 'redirect' => route('admin.settings.dashboards.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Dashboard Charts
     *
     * @param int $id
     * @return Response
     */
    public function getDashboardScanLogsCharts(Request $request)
    {
        $type = $request->type;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $communityId = $request->communityId;


        $createDateRange = [];

        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $dateRanges = new DatePeriod($begin, new DateInterval('P1D'), $end);

        foreach ($dateRanges as $dateRange) {
            $createDateRange[] = $dateRange->format("Y-m-d");
        }

        if ($type == 0) {
            $role = [7, 9, 5, 4];
        }

        if ($type == 1) {
            $role = [5, 4];
        }

        if ($type == 2) {
            $role = [7];
        }

        if ($type == 3) {
            $role = [9];
        }

        $scanData = $this->scanLogService->getScanLogsDashboardDateRange($communityId, $role, $startDate, $endDate)['result'];

        $guest = [];
        $vendor = [];
        $resident = [];
        $i = 0;
        foreach ($createDateRange as $dr) {
            $guest[$i] = 0;
            $vendor[$i] = 0;
            $resident[$i] = 0;
            $label[] = date("d M", strtotime($dr));
            foreach ($scanData as $data) {
                if ($dr == $data->scan_date) {
                    if ($data->role_id == 7) {
                        $guest[$i] = $data->total;
                    }

                    if ($data->role_id == 9) {
                        $vendor[$i] = $data->total;
                    }

                    if (($data->role_id == 5) || ($data->role_id == 4)) {
                        $resident[$i] = $data->total;
                    }
                }
            }
            $i++;
        }

        $responseDate = [];

        if (!empty($label)) {
            $responseDate = [
                'label' => $label,
                'guest' => $guest,
                'vendor' => $vendor,
                'resident' => $resident
            ];
        }

        $timeRanges = [
            ['0:00:00', '1:00:00'],
            ['1:00:00', '2:00:00'],
            ['2:00:00', '3:00:00'],
            ['3:00:00', '4:00:00'],
            ['4:00:00', '5:00:00'],
            ['5:00:00', '6:00:00'],
            ['6:00:00', '7:00:00'],
            ['7:00:00', '8:00:00'],
            ['8:00:00', '9:00:00'],
            ['9:00:00', '10:00:00'],
            ['10:00:00', '11:00:00'],
            ['11:00:00', '12:00:00'],
            ['12:00:00', '13:00:00'],
            ['13:00:00', '14:00:00'],
            ['14:00:00', '15:00:00'],
            ['15:00:00', '16:00:00'],
            ['16:00:00', '17:00:00'],
            ['17:00:00', '18:00:00'],
            ['18:00:00', '19:00:00'],
            ['19:00:00', '20:00:00'],
            ['20:00:00', '21:00:00'],
            ['21:00:00', '22:00:00'],
            ['22:00:00', '23:00:00'],
            ['23:00:00', '24:00:00'],
        ];

        $guest = [];
        $vendor = [];
        $resident = [];
        $i = 0;
        $label = [];

        $scanData = $this->scanLogService->getScanLogsDashboardTimeRange($role, $startDate, $endDate)['result'];

        foreach ($timeRanges as $timeRange) {
            $guest[$i] = 0;
            $vendor[$i] = 0;
            $resident[$i] = 0;

            $label[$i] = (date('H:i', strtotime($timeRange[0])) . '-' . date('H:i', strtotime($timeRange[1])));


            foreach ($scanData as $data) {

                $scanDate = $data->scan_date;

                $scanLogs1 = ScanLog::where('created_at', 'LIKE', "$scanDate%")
                    ->where('community_id', 61)
                    ->get();


                foreach ($scanLogs1 as $data1) {

                    $startTime = Carbon::createFromFormat('H:i:s', $timeRange[0]);
                    $endTime = Carbon::createFromFormat('H:i:s', $timeRange[1]);

                    $date = explode(' ', $data1->created_at);
                    $date = $date[1];

                    $currentTime = Carbon::createFromFormat('H:i:s', $date);


                    if ($currentTime->between($startTime, $endTime, true)) {
                        if ($data->role_id == 7) {
                            $guest[$i] += 1;
                        }

                        if ($data->role_id == 9) {
                            $vendor[$i] += 1;
                        }

                        if (($data->role_id == 5) || ($data->role_id == 4)) {
                            $resident[$i] += 1;
                        }
                    }
                }
            }

            $i++;
        }

        $responseTime = [];

        if (!empty($label)) {
            $responseTime = [
                'label' => $label,
                'guest' => $guest,
                'vendor' => $vendor,
                'resident' => $resident
            ];
        }

        return $response = array($responseDate, $responseTime);
    }

    /**
     * Dashboard Recent Notifications
     *
     * @param Request $request
     * @return Response
     */
    public function getAdminDashboardNotifications(Request $request)
    {
        $result = $this->adminPanelNotificationService->getRecentNotifications(auth()->user()->id)['result'];
        return ["message" => "Notifications", 'notifications' => $result];
    }

    /**
     * Dashboard Camera Location Charts
     *
     * @param Request $request
     * @return Response
     */
    public function getTrafficSummary(Request $request)
    {

        try {
            $communityId = $request->community_id;
            $ticketType = [1, 2, 3, 4];
            $data = array();

            for ($i = 0; $i < 4; $i++) {
                $tickets = $this->trafficTicketService->getTicketsCount($communityId, $request->location_id, $ticketType[$i])['result'];
                $data[$i] = $tickets;
            }

            return $data;
        } catch (Exception $exception) {
            return 'error';
        }
    }

    /**
     * CSV Imports Notifications
     *
     * @return Response
     */
    public function csvNotifications()
    {

        $currentDateTime = Carbon::now();
        $currentDateTimeHourBack = Carbon::now()->subHour();

        $currentDate = $currentDateTime->toDateString();
        $currentTime = $currentDateTime->toTimeString();
        $oneHourBackTime = $currentDateTimeHourBack->toTimeString();

        $notificationRecords = CsvImport::where('email', auth()->user()->email)
            ->whereDate('created_at', $currentDate)
            ->whereTime('created_at', '>=', $oneHourBackTime)
            ->whereTime('created_at', '<=', $currentTime)
            ->orderbyDesc('created_at')
            ->limit(3)
            ->get();

        return response()->json(['notifications' => $notificationRecords]);

    }

    /**
     * Update CSV imports table viewed column
     *
     * @return
     */
    public function csvNotificationViewed(Request $request)
    {
        $csvImport = CsvImport::find($request->id);
        $csvImport->viewed = '1';
        $csvImport->save();
    }

    public function runQueue()
    {
        DB::table('jobs')->truncate();
        DB::table('failed_jobs')->truncate();


        Artisan::call('queue:work');
        return redirect()->back();
    }
}
