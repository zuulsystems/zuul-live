<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Network;
use Illuminate\Http\Request;
use App\Services\CommunityService;
use Illuminate\Http\Response;

class NetworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.networks.index');
    }

    public function networkData(Request $request)
    {
        $result = Network::query()->with('community');

        if (!empty(auth()->user()->kioskPermission)) {
            $communities = explode(",", auth()->user()->kioskPermission->communities);
            $result = $result->whereIn('community_id', $communities);
        }

        return datatables()->of($result)
            ->addColumn('community_name', function ($model) {
                return $model->community->name;
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="' . route('admin.network.edit', $model->id) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
                            <form action="' . route('admin.network.destroy', $model->id) . '" method="POST" class="ajax-form">
                ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>
                            </div>
                        </div>';
            })
            ->filter(function ($instance) use ($request) {
                if (!empty($request->registered_ip)) {
                    $instance->where('registered_ip', 'like', '%' . $request->registered_ip . '%');
                }
                if (!empty($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%" . $request->community_name . "%");
                    });
                }
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'registered_ip' => 'required|unique:networks,registered_ip',
            'community_id' => 'required'
        ]);

        $networkData = $request->all();

        Network::create($networkData);

        return response()->json(['status' => true, 'message' => 'Network added successfully', 'redirect' => route('admin.network.index')]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        return view('admin.networks.create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['network'] = Network::find($id);
        $data['communities'] = getCommunities();
        return view('admin.networks.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'registered_ip' => 'required|unique:networks,registered_ip,' . $id,
            'community_id' => 'required'
        ]);

        $networkData = $request->all();

        Network::find($id)->update($networkData);

        return response()->json(['status' => true, 'message' => 'Network updated successfully', 'redirect' => route('admin.network.index')]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Network::find($id)->delete();
        return response()->json(['status' => true, 'message' => 'Network deleted successfully', 'redirect' => route('admin.network.index')]);
    }
}
