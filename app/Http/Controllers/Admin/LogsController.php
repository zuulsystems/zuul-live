<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Services\LogsService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LogsController extends Controller
{
    use UserCommon;

    public $LogsService;
    protected $folderLink = 'admin.logs.';

    public function __construct(
        LogsService $LogsService
    )
    {
        $this->LogsService = $LogsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get houses list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function LogsData(Request $request)
    {

        $result = $this->LogsService->all()['result'];

        return datatables()->of($result)
            ->addColumn('id', function ($model) {
                return $model->id ?? '-';
            })
            ->addColumn('user', function ($model) {
                return $model->user->first_name . " " . $model->user->last_name ?? '-';
            })
            ->addColumn('action', function ($model) {
                $data = json_decode($model->description, true);
                return isset($data['action']) ? $data['action'] : '-';
            })
            ->addColumn('message', function ($model) {
                $data = json_decode($model->description, true);
                return isset($data['message']) ? $data['message'] : '-';
            })
            ->addColumn('ip', function ($model) {
                return $model->ip ?? '-';
            })
            ->addColumn('device', function ($model) {
                return $model->device ?? '-';
            })
            ->addColumn('status', function ($model) {
                $data = json_decode($model->description, true);
                return isset($data['status']) && $data['status'] == true ? 'Success' : 'Error';
            })
            ->addColumn('created_at', function ($model) {
                $dateString = $model->created_at;
                $timestamp = strtotime($dateString);
                $formattedTime = date("Y-m-d h:i A", $timestamp);

                return $formattedTime;
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->user)) {
                    $instance->whereHas('user', function ($where) use ($request) {
                        $where->where('first_name', 'LIKE', '%' . $request->get('user') . '%');
                    });
                }
                if ($request->has('action')) {
                    $instance->where('description->action', $request->get('action'));
                }
                if ($request->has('ip')) {
                    $instance->where('ip', $request->get('ip'));
                }
            })
            ->make(true);
    }

}
