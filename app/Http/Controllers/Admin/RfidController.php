<?php

namespace App\Http\Controllers\Admin;

use App\Exports\RFIDSkippedRecordsExports;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Jobs\ImportRfids;
use App\Mail\SendResidentImportFileGeneration;
use App\Models\CsvImport;
use App\Models\House;
use App\Models\ResidentCsv;
use App\Models\Rfid;
use App\Models\RfidCsv;
use App\Models\RfidCsvFile;
use App\Models\RfidDateLimit;
use App\Models\TrafficTicket;
use App\Models\User;
use App\Models\Vehicle;
use App\Services\CommunityService;
use App\Services\HouseService;
use App\Services\RfidService;
use App\Services\TrafficTicketService;
use App\Services\UserService;
use App\Services\VehicleService;
use Cassandra\Date;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Exports\UserExports;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Repositories\RfidDateLimitRepository;

class RfidController extends Controller
{
    use UserCommon;

    public $rfidService;
    public $houseService;
    public $communityService;
    public $userService;
    public $vehicleService;
    public $trafficTicketService;
    public $rfidDateLimitService;
    protected $folderLink = 'admin.rfids.';

    public function __construct(
        HouseService            $houseService,
        CommunityService        $communityService,
        RfidService             $rfidService,
        UserService             $userService,
        VehicleService          $vehicleService,
        TrafficTicketService    $trafficTicketService,
        RfidDateLimitRepository $rfidDateLimitService
    )
    {
        $this->houseService = $houseService;
        $this->communityService = $communityService;
        $this->rfidService = $rfidService;
        $this->userService = $userService;
        $this->vehicleService = $vehicleService;
        $this->trafficTicketService = $trafficTicketService;
        $this->rfidDateLimitService = $rfidDateLimitService;
        $this->middleware('checkGuardAdmin', ['except' => ['index', 'communityAdminsData']]);
    }

    public static function readImportedFile($filePath = array())
    {
        $communityId = $filePath[1];
        $adminEmail = $filePath[2];
        $filePath = Storage::path($filePath[0]);

        $handle = fopen($filePath, "r");

        $HEADER = '#,rfid_tag_num,house_assigned,resident_first_name,resident_last_name,mailing_address,driver_license_number,license_plate,state_registered,vehicle_type,make,color,year,model,insurance_company_name,policy_number,policy_expiration_date,additional_drivers';

        $fail_message = '';
        $header = null;
        $csv = array();
        $row = 1;
        $imp_error = 0;
        while (($data = fgetcsv($handle, 5000, ',')) !== false) {
            if ($row == 1) {
                $first_row = array_diff($data, explode(",", $HEADER));
                if (count($first_row) > 0) {
                    $imp_error = 1;
                    $fail_message .= "<strong>Error!</strong> Invalid column name somewhere in your CSV file.";
                    $valid_file = 0;
                    return $fail_message;
                }
            }

            if (!$header) {
                $header = $data;
            } else {
                $csv[] = array_combine($header, $data);
            }

            $row++;
        }
        fclose($handle);
        $rfidArr = $csv;

        $csvSkippedRows = [];

        for ($i = 0; $i < count($rfidArr); $i++) {
            $rfidTagNum = $rfidArr[$i]['rfid_tag_num'];

            $rowError = false;
            $csvError = '';
            $comma = false;

            $houseName = $rfidArr[$i]['house_assigned'];

            //if empty rfid tag number given
            if (empty($rfidArr[$i]['rfid_tag_num'])) {
                $rowError = false;
                $csvError = 'blank rfid tag number';
                $comma = true;
            }

            //if empty house name given
            if (empty($houseName)) {
                $rowError = false;
                $comma ? $csvError .= ', blank house name' : $csvError .= 'blank house name';
                $comma = true;
            }

            //if empty first name given
            if (empty($rfidArr[$i]['resident_first_name'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank first name' : $csvError .= 'blank first name';
                $comma = true;
            }

            //if last name given
            if (empty($rfidArr[$i]['resident_last_name'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank last name' : $csvError .= 'blank last name';
                $comma = true;
            }

            //if license plate is blank
            if (empty($rfidArr[$i]['license_plate'])) {
                $rowError = true;
                $comma ? $csvError .= ', blank license plate' : $csvError .= 'blank license plate';
                $comma = true;
            }

            //if not phone number
            if (!empty($rfidArr[$i]['rfid_tag_num'])) {
                if (!preg_match('/^\d+$/', $rfidArr[$i]['rfid_tag_num'])) {
                    $rowError = true;
                    $comma ? $csvError .= ', Not correct rfid number, it should be a number' : $csvError .= 'Not correct rfid number, it should be a number';
                    $comma = true;
                } else {
                    $rfid = Rfid::where(['community_id' => $communityId, 'rfid_tag_num' => $rfidArr[$i]['rfid_tag_num']])->first();
                    if (!empty($rfid)) {
                        $rowError = true;
                        $comma ? $csvError .= ', rfid number exist' : $csvError .= 'rfid number exist';
                        $comma = true;
                    }
                }
            }

            //if house name does not exist in community
            if (!empty($houseName)) {
                $house = House::where(['community_id' => $communityId, 'house_name' => $houseName])->first();
                if (empty($house)) {
                    $rowError = true;
                    $comma ? $csvError .= ', house does not exist in community' : $csvError .= 'house does not exist in community';
                    $comma = true;
                } else {
                    //
                    if (!empty($rfidArr[$i]['resident_first_name']) && !empty($rfidArr[$i]['resident_last_name'])) {
                        $user = User::where(['community_id' => $communityId, 'house_id' => $house->id, 'first_name' => $rfidArr[$i]['resident_first_name']])->first();
                        if (empty($user)) {
                            $rowError = true;
                            $comma ? $csvError .= ', resident first name does not exist in house' : $csvError .= 'resident first name does not exist in house';
                            $comma = true;
                        }

                        $user = User::where(['community_id' => $communityId, 'house_id' => $house->id, 'last_name' => $rfidArr[$i]['resident_last_name']])->first();
                        if (empty($user)) {
                            $rowError = true;
                            $comma ? $csvError .= ', resident last name does not exist in house' : $csvError .= 'resident last name does not exist in house';
                            $comma = true;
                        }
                    }
                }
            }

            $policyExpirationDateforCheck = $rfidArr[$i]['policy_expiration_date'];

            $policyExpirationDate = '';

            if (!empty($rfidArr[$i]['policy_expiration_date'])) {
                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $policyExpirationDateforCheck)) {
                    $policyExpirationDate = $rfidArr[$i]['policy_expiration_date'];
                }
            }

            //if license plate match
            if (!empty($rfidArr[$i]['license_plate'])) {
                $vehicles = Vehicle::with('user')->where('license_plate', $rfidArr[$i]['license_plate'])->whereNull('deleted_at')->get();
                if ($vehicles->count() > 0) {
                    foreach ($vehicles as $vehicle) {
                        if (!empty($vehicle->user)) {
                            if ($communityId == $vehicle->user->community_id) {
                                $rowError = true;
                                $comma ? $csvError .= ', license plate exist in community' : $csvError .= 'license plate exist in community';
                                $comma = true;
                            }
                        }
                    }
                }
            }

            $driverLicenseNumber = $rfidArr[$i]['driver_license_number'];
            $licenseNumber = $rfidArr[$i]['license_plate'];

            if (!$rowError) {
                $rfid = Rfid::where(['community_id' => $communityId, 'rfid_tag_num' => $rfidTagNum])->first();

                $licencePlate = removeSpaceFromString($rfidArr[$i]['license_plate']);

                $futureDate = date('Y-m-d', strtotime('+1 year'));

                $stateRegistered = '';
                $vehicleType = '';
                $make = '';
                $color = '';
                $year = '';
                $model = '';
                $insuranceCompanyName = '';
                $policyNumber = '';
                $additionalDrivers = '';

                if (!empty($rfidArr[$i]['state_registered'])) {
                    $stateRegistered = $rfidArr[$i]['state_registered'];
                }
                if (!empty($rfidArr[$i]['vehicle_type'])) {
                    $vehicleType = $rfidArr[$i]['vehicle_type'];
                }
                if (!empty($rfidArr[$i]['make'])) {
                    $make = $rfidArr[$i]['make'];
                }
                if (!empty($rfidArr[$i]['color'])) {
                    $color = $rfidArr[$i]['color'];
                }
                if (!empty($rfidArr[$i]['year'])) {
                    $year = $rfidArr[$i]['year'];
                }
                if (!empty($rfidArr[$i]['model'])) {
                    $model = $rfidArr[$i]['model'];
                }
                if (!empty($rfidArr[$i]['insurance_company_name'])) {
                    $insuranceCompanyName = $rfidArr[$i]['insurance_company_name'];
                }
                if (!empty($rfidArr[$i]['policy_number'])) {
                    $policyNumber = $rfidArr[$i]['policy_number'];
                }
                if (!empty($rfidArr[$i]['additional_drivers'])) {
                    $additionalDrivers = $rfidArr[$i]['additional_drivers'];
                }

                if (empty($rfid) && !empty($licencePlate) && !empty($house)) {
                    $house = House::where(['community_id' => $communityId, 'house_name' => $rfidArr[$i]['house_assigned']])->first();
                    $resident = User::where(['community_id' => $communityId, 'house_id' => $house->id, 'first_name' => $rfidArr[$i]['resident_first_name'], 'last_name' => $rfidArr[$i]['resident_last_name']])->first();

                    if (!empty($rfidArr[$i]['driver_license_number'])) {
                        User::findOrFail($resident->id)->update(['license_number' => $rfidArr[$i]['driver_license_number']]);
                    }

                    $vehicle = Vehicle::updateOrCreate([
                        'license_plate' => $licencePlate,
                        'user_id' => $resident->id,
                    ], [
                        'make' => $make,
                        'color' => $color,
                        'year' => $year,
                        'model' => $model,
                        'state' => $stateRegistered,
                        'activation_date' => date('Y-m-d'), //date
                        'expiration_date' => $futureDate, //date
                        'vehicle_type' => $vehicleType,
                        'windshield_parking_sticker' => $additionalDrivers,
                        'policy_number' => $policyNumber,
                        'policy_expiration_date' => $policyExpirationDate, //date
                        'insurance_company_name' => $insuranceCompanyName
                    ]);

                    if (!empty($vehicle)) {
                        $rfid = Rfid::create([
                            'rfid_tag_num' => $rfidTagNum,
                            'user_id' => $resident->id,
                            'house_id' => $house->id,
                            'community_id' => $communityId,
                            'vehicle_id' => $vehicle->id,
                            'license' => $licencePlate,
                            'make_model' => $make . ' ' . $model,
                            'created_by' => auth()->id()
                        ]);

                        $futureDateBySlash = date('m/d/Y', strtotime('+1 year'));
                        $startEnd = date('m/d/Y') . ' - ' . $futureDateBySlash;


                        if (!empty($rfid)) {

                            //get unmatched tickets behalf of the license plate
                            $unmatchedTickets = TrafficTicket::where('ticket_type', 'unmatched')->whereNotNull('license_plate')->where('license_plate', $licencePlate)->get();
                            if ($unmatchedTickets->isNotEmpty()) {
                                $ticketIds = $unmatchedTickets->pluck('id');
                                $result = TrafficTicket::whereIn('id', $ticketIds)->update([
                                    'ticket_type' => 'resident',
                                    'user_id' => $resident->id,
                                    'community_id' => $resident->community_id,
                                ]);
                            }
                        } else {
                        }

                        $skipRow = array(
                            '#' => '',
                            'rfid_tag_num' => $rfidArr[$i]['rfid_tag_num'],
                            'house_assigned' => $rfidArr[$i]['house_assigned'],
                            'resident_first_name' => $rfidArr[$i]['resident_first_name'],
                            'resident_last_name' => $rfidArr[$i]['resident_last_name'],
                            'mailing_address' => $rfidArr[$i]['mailing_address'],
                            'driver_license_number' => $rfidArr[$i]['driver_license_number'],
                            'license_plate' => $rfidArr[$i]['license_plate'],
                            'state_registered' => $rfidArr[$i]['state_registered'],
                            'vehicle_type' => $rfidArr[$i]['vehicle_type'],
                            'make' => $rfidArr[$i]['make'],
                            'color' => $rfidArr[$i]['color'],
                            'year' => $rfidArr[$i]['year'],
                            'model' => $rfidArr[$i]['model'],
                            'insurance_company_name' => $rfidArr[$i]['insurance_company_name'],
                            'policy_number' => $rfidArr[$i]['policy_number'],
                            'policy_expiration_date' => $policyExpirationDate,
                            'additional_drivers' => $rfidArr[$i]['additional_drivers'],
                            'status' => 'Success',
                            'message' => 'Record imported successfully',
                            'id' => $rfid->id);

                        array_push($csvSkippedRows, $skipRow);

                    }
                }
            } else {

                $skipCSVFileGeneration = true;

                $skipRow = array(
                    '#' => '',
                    'rfid_tag_num' => $rfidArr[$i]['rfid_tag_num'],
                    'house_assigned' => $rfidArr[$i]['house_assigned'],
                    'resident_first_name' => $rfidArr[$i]['resident_first_name'],
                    'resident_last_name' => $rfidArr[$i]['resident_last_name'],
                    'mailing_address' => $rfidArr[$i]['mailing_address'],
                    'driver_license_number' => $rfidArr[$i]['driver_license_number'],
                    'license_plate' => $rfidArr[$i]['license_plate'],
                    'state_registered' => $rfidArr[$i]['state_registered'],
                    'vehicle_type' => $rfidArr[$i]['vehicle_type'],
                    'make' => $rfidArr[$i]['make'],
                    'color' => $rfidArr[$i]['color'],
                    'year' => $rfidArr[$i]['year'],
                    'model' => $rfidArr[$i]['model'],
                    'insurance_company_name' => $rfidArr[$i]['insurance_company_name'],
                    'policy_number' => $rfidArr[$i]['policy_number'],
                    'policy_expiration_date' => $policyExpirationDate,
                    'additional_drivers' => $rfidArr[$i]['additional_drivers'],
                    'status' => 'Failure',
                    'message' => $csvError,
                    'id' => ''
                );

                array_push($csvSkippedRows, $skipRow);

            }
        }

        if (!Storage::exists('/storage/app/public/csvrfidfilesresult')) {
            Storage::makeDirectory('/storage/app/public/csvrfidfilesresult', 0775, true); //creates directory
        }

        $fileName = "rfid_success_skip" . time() . ".txt";
        $successSkippedFile = "/storage/app/public/csvrfidfilesresult/" . $fileName;

        $handle = fopen("storage/app/storage/app/public/csvrfidfilesresult/" . $fileName, 'w');

        foreach ($csvSkippedRows as $rfid) {
            fputcsv($handle, [
                $rfid['#'],
                $rfid['rfid_tag_num'],
                $rfid['house_assigned'],
                $rfid['resident_first_name'],
                $rfid['resident_last_name'],
                $rfid['mailing_address'],
                $rfid['driver_license_number'],
                $rfid['license_plate'],
                $rfid['state_registered'],
                $rfid['vehicle_type'],
                $rfid['make'],
                $rfid['color'],
                $rfid['year'],
                $rfid['model'],
                $rfid['insurance_company_name'],
                $rfid['policy_number'],
                $rfid['policy_expiration_date'],
                $rfid['additional_drivers'],
                $rfid['status'],
                $rfid['message'],
                $rfid['id']
            ]);
        }

        fclose($handle);

        //sending email
        $emailData['fileName'] = $fileName;
        $emailData['csvBelongsTo'] = 'rfids';
        $emailData['subject'] = 'RFID Import File Generation';

        CsvImport::create([
            'email' => $adminEmail,
            'type' => 'rfid-import',
            'file_name' => $fileName
        ]);

        Mail::to($adminEmail)->send(new SendResidentImportFileGeneration($emailData));

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data["communityId"] = $request->communityId ?? '';
        $data["userId"] = $request->userId ?? '';
        $data["houseId"] = $request->houseId ?? '';
        $data['communities'] = getCommunities();
        $data['rfidId'] = $request->rfidId ?? '';
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get houses list
     * @param Request $request
     * @return
     * @throws Exception
     */
    public function rfidData(Request $request)
    {
        $communityId = null;
        if ($request->has('communityId') && !empty($request->communityId)) {
            $communityId = $request->communityId;
        }
        if (!empty(getCommunityIdByUser())) {
            $communityId = getCommunityIdByUser();
        }
        $userId = null;
        if ($request->has('userId') && !empty($request->userId)) {
            $userId = $request->userId;
        }
        $houseId = null;
        if ($request->has('houseId') && !empty($request->houseId)) {
            $houseId = $request->houseId;
        }


        $rfidId = null;
        if ($request->has('rfidId') && !empty($request->rfidId)) {
            $rfidId = $request->rfidId;
        }
        return datatables()->of($this->rfidService->all($communityId, $userId, $houseId, $rfidId)['result'])
            ->addColumn('resident_name', function ($model) {
                return ($model->user) ? $model->user->fullName : "-";
            })
            ->addColumn('resident_phone', function ($model) {
                return ($model->user) ? $model->user->formattedPhone : "-";
            })
            ->addColumn('resident_street_address', function ($model) {
                return ($model->user && !empty($model->house->house_detail)) ? $model->house->house_detail : "-";
            })
            ->addColumn('vehicle_license_plate', function ($model) {
                return (!empty($model->vehicle)) ? strtoupper($model->vehicle->license_plate) : "-";
            })
            ->addColumn('formatted_resident_phone', function ($model) {
                return ($model->user) ? $model->user->formattedPhone : "-";
            })
            ->addColumn('resident_email', function ($model) {
                return ($model->user) ? $model->user->email : "-";
            })
            ->addColumn('resident_house_lot_number', function ($model) {
                return (!empty($model->user) && !empty($model->user->house) && !empty($model->house->lot_number)) ? $model->house->lot_number : "-";
            })
            ->addColumn('community_name', function ($model) {
                return ($model->community) ? $model->community->name : "-";
            })
            ->addColumn('formatted_status', function ($model) {
                return $model->suspend == '0' ? 'Active' : 'Suspended';
            })
            ->addColumn('additional_drivers', function ($model) {
                return (!empty($model->vehicle) && !empty($model->vehicle->windshield_parking_sticker)) ? $model->vehicle->windshield_parking_sticker : "-";
            })
            ->addColumn('vehicle_info', function ($model) {
                return ($model->vehicle_info) ? $model->vehicle_info : "-";
            })
            ->addColumn('links', function ($model) {
                $suspendTitle = 'Suspend';
                $suspendIcon = 'fa fa-ban';
                if ($model->suspend == '1') {
                    $suspendTitle = 'Unsuspend';
                    $suspendIcon = 'fa fa-check';
                }

                $dropdown = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="' . route('admin.rfid.rfids-schedules.index', ["rfid_id" => $model->id]) . '"><i class="fa fa-times" aria-hidden="true"></i> Time Limit </a>
                                <a class="dropdown-item" href="' . route('admin.rfid.rfids.edit', [$model->id]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>';

                if (empty(auth()->user()->kioskPermission)) {
                    $dropdown .= '<form action="' . route('admin.rfid.suspendOrUnsuspend') . '" method="POST" class="ajax-form" style="max-height:29px">
                                    ' . csrf_field() . '
                                    <input type="hidden" name="id" value="' . $model->id . '">
                                    <input type="hidden" name="suspendId" value="' . $model->suspend . '">
                                    <button class="dropdown-item"><i class="' . $suspendIcon . '" aria-hidden="true"></i> ' . $suspendTitle . '</button>-
                                </form>';

                    if (!empty($model->user)) {
                        $dropdown .= '<a class="dropdown-item" href="' . route('admin.residential.residents.index', ["userId" => $model->user->id]) . '"><i class="fa fa-user" aria-hidden="true"></i> Resident </a>';
                    }
                    if (!empty($model->vehicle)) {
                        $dropdown .= '<a class="dropdown-item" href="' . route('admin.vehicles.index', ["vehicleId" => $model->vehicle->id]) . '"><i class="fa fa-car" aria-hidden="true"></i> Vehicle </a>';
                    }

                }

                $dropdown .= '<form action="' . route('admin.rfid.rfids.destroy', [$model->id]) . '" method="POST" class="ajax-form">
                                    ' . method_field("DELETE") . '
                                    ' . csrf_field() . '
                                    <button class="dropdown-item"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>
                                </div>
                                </div>';

                return $dropdown;
            })
            ->filter(function ($instance) use ($request) {

                if (isset($request->rfid_tag_num)) {
                    $instance->where('rfid_tag_num', 'like', "%{$request->get('rfid_tag_num')}%");
                }
                if (isset($request->license)) {
                    $instance->where('license', 'like', "%{$request->get('license')}%");
                }
                if (isset($request->resident_name)) {
                    $instance->whereHas('user', function ($where) use ($request) {
                        $where->where(function ($where) use ($request) {
                            $where->where('first_name', 'like', "%{$request->get('resident_name')}%")
                                ->orWhere('last_name', 'like', "%{$request->get('resident_name')}%");
                        });
                    });
                }
                if (isset($request->phone)) {
                    $instance->whereHas('user', function ($where) use ($request) {
                        $phoneNumber = str_replace('(', '', $request->get('phone'));
                        $phoneNumber = str_replace(')', '', $phoneNumber);
                        $phoneNumber = str_replace(' ', '', $phoneNumber);
                        $phoneNumber = str_replace('-', '', $phoneNumber);
                        $where->where('phone_number', 'like', "%{$phoneNumber}%");
                    });
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
                if (isset($request->lot_number)) {
                    $instance->whereHas('user.house', function ($q) use ($request) {
                        $q->where('lot_number', 'like', "%{$request->get('lot_number')}%");
                    });
                }
                if (isset($request->address)) {
                    $instance->whereHas('house', function ($where) use ($request) {
                        $where->where('house_detail', 'like', "%{$request->get('address')}%");
                    });
                }
                if (isset($request->additional_drivers)) {
                    $instance->whereHas('vehicle', function ($where) use ($request) {
                        $where->where('windshield_parking_sticker', 'like', "%{$request->get('additional_drivers')}%");
                    });
                }
                if (isset($request->status)) {
                    $instance->where('suspend', $request->status);
                }
            })
            ->rawColumns(['links'])
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function store(Request $request)
    {
        $rfidData = $request->all();
        $validation = [
            'rfid_tag_num' => 'required',
            'community_id' => 'required',
            'house_id' => 'required',
            'resident_id' => 'required|exists:users,id',
            'license_plate' => 'required',
        ];

        $validator = Validator::make($rfidData, $validation);
        if ($validator->fails()) {
            $validator->validate();
        }

        $rfids_response = $this->rfidService->find_store_duplicate($request->rfid_tag_num, $request->community_id);

        $response = '';

        if ($rfids_response['bool'] == true) {
            // user activity logs
            callUserActivityLogs("Add RFID Tag", __FUNCTION__, __FILE__, __DIR__, false, "Duplicate Data!");

            $response = ['status' => false, 'message' => 'Duplicate Data!'];
            return response()->json($response);
        }
        $resident = $this->userService->find($request->resident_id);


        if ($resident['result']) {
            $licensePlate = removeSpaceFromString($rfidData['license_plate']);

            $licensePlate = str_replace('-', '', $licensePlate); // Removes special chars.
            $licensePlate = preg_replace('/[^A-Za-z0-9\-]/', '', $licensePlate); // Removes special chars.
            $licensePlate = strtoupper($licensePlate);

            $this->userService->update([
                'license_number' => $rfidData['driver_license_number']
            ], $resident['result']->id);

            $vehicle = $this->vehicleService->updateOrCreate([
                'license_plate' => $licensePlate,
                'user_id' => $resident['result']->id,
            ], [
                'make' => $rfidData['make'],
                'color' => $rfidData['color'],
                'year' => $rfidData['year'],
                'model' => $rfidData['model'],
                'state' => $rfidData['state'],
                'activation_date' => $rfidData['activation_date'],
                'expiration_date' => $rfidData['expiration_date'],
                'vehicle_type' => $rfidData['vehicle_type'],
                'policy_number' => $rfidData['policy_number'],
                'policy_expiration_date' => $rfidData['policy_expiration_date'],
                'windshield_parking_sticker' => $rfidData['windshield_parking_sticker'],
                'insurance_company_name' => $rfidData['insurance_company_name'],
            ]);
            if ($vehicle['bool']) {
                $rfid = $this->rfidService->create([
                    'rfid_tag_num' => $rfidData['rfid_tag_num'],
                    'user_id' => $resident['result']->id,
                    'house_id' => $rfidData['house_id'],
                    'community_id' => $rfidData['community_id'],
                    'vehicle_id' => $vehicle['result']->id,
                    'license' => $licensePlate,
                    'make_model' => $rfidData['make'] . ' ' . $rfidData['model'],
                    'vehicle_info' => strtoupper($licensePlate) . ", " . $rfidData['color'] . ", " . $rfidData['make'] . ", " . $rfidData['model'] . ", " . $rfidData['year'],
                    'created_by' => auth()->id()
                ]);
                if ($rfid['bool']) {
                    //get unmatched tickets behalf of the license plate
                    $unmatchedTickets = $this->trafficTicketService->getUnmatchedTicketByLicensePlate($licensePlate)['result'];

                    if ($unmatchedTickets->isNotEmpty()) {
                        $ticketIds = $unmatchedTickets->pluck('id');
                        $updateTickets = $this->trafficTicketService->attachTicketsToResident($ticketIds, $resident['result']->id, $resident['result']->community_id);
                    }

                    // user activity logs
                    callUserActivityLogs("Add RFID Tag", __FUNCTION__, __FILE__, __DIR__, true, "Added Successfully");

                    $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('admin.rfid.rfids.index')];
                } else {
                    // user activity logs
                    callUserActivityLogs("Add RFID Tag", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

                    $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
                }
            } else {
                // user activity logs
                callUserActivityLogs("Add RFID Tag", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");
                $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
            }
        } else {
            // user activity logs
            callUserActivityLogs("Add RFID Tag", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        $rfidData = $request->all();
        $validation = [
            'rfid_tag_num' => ['required', Rule::unique('rfids')->ignore($id)->where('rfid_tag_num', $rfidData['rfid_tag_num'])->where('community_id', $rfidData['community_id'])->whereNull('deleted_at')],
            'community_id' => 'required',
            'house_id' => 'required',
            'resident_id' => 'required|exists:users,id',
            'license_plate' => 'required',
        ];

        $validator = Validator::make($rfidData, $validation);
        if ($validator->fails()) {
            $validator->validate();
        }

        $resident = $this->userService->find($request->resident_id);
        if ($resident['bool']) {
            $licensePlate = removeSpaceFromString($rfidData['license_plate']);

            $licensePlate = str_replace('-', '', $licensePlate); // Removes special chars.
            $licensePlate = preg_replace('/[^A-Za-z0-9\-]/', '', $licensePlate); // Removes special chars.
            $licensePlate = strtoupper($licensePlate);

            $this->userService->update([
                'license_number' => $rfidData['driver_license_number']
            ], $resident['result']->id);

            $vehicle = $this->vehicleService->updateOrCreate([
                'license_plate' => $licensePlate,
                'user_id' => $resident['result']->id,
            ], [
                'make' => $rfidData['make'],
                'color' => $rfidData['color'],
                'year' => $rfidData['year'],
                'model' => $rfidData['model'],
                'state' => $rfidData['state'],
                'activation_date' => $rfidData['activation_date'],
                'expiration_date' => $rfidData['expiration_date'],
                'vehicle_type' => $rfidData['vehicle_type'],
                'policy_number' => $rfidData['policy_number'],
                'policy_expiration_date' => $rfidData['policy_expiration_date'],
                'windshield_parking_sticker' => $rfidData['windshield_parking_sticker'],
                'insurance_company_name' => $rfidData['insurance_company_name'],
            ]);
            if ($vehicle['bool']) {
                $rfid = $this->rfidService->update([
                    'rfid_tag_num' => $rfidData['rfid_tag_num'],
                    'user_id' => $resident['result']->id,
                    'house_id' => $rfidData['house_id'],
                    'community_id' => $rfidData['community_id'],
                    'vehicle_id' => $vehicle['result']->id,
                    'license' => $licensePlate,
                    'make_model' => $rfidData['make'] . ' ' . $rfidData['model'],
                    'vehicle_info' => strtoupper($licensePlate) . ", " . $rfidData['color'] . ", " . $rfidData['make'] . ", " . $rfidData['model'] . ", " . $rfidData['year'],
                ], $id);
                if ($rfid['bool']) {
                    //get unmatched tickets behalf of the license plate
                    $unmatchedTickets = $this->trafficTicketService->getUnmatchedTicketByLicensePlate($licensePlate)['result'];
                    if ($unmatchedTickets->isNotEmpty()) {
                        $ticketIds = $unmatchedTickets->pluck('id');
                        $updateTickets = $this->trafficTicketService->attachTicketsToResident($ticketIds, $resident['result']->id, $resident['result']->community_id);
                    }

                    // user activity logs
                    callUserActivityLogs("Edit RFID Tags", __FUNCTION__, __FILE__, __DIR__, true, "Update Successfully");

                    $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('admin.rfid.rfids.index')];
                } else {
                    // user activity logs
                    callUserActivityLogs("Edit RFID Tags", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

                    $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
                }
            } else {
                // user activity logs
                callUserActivityLogs("Edit RFID Tags", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

                $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
            }
        } else {
            // user activity logs
            callUserActivityLogs("Edit RFID Tags", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws Exception
     */
    public function create()
    {
        $data['communities'] = getCommunities();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $data['communities'] = getCommunities();
        $rfid = $this->rfidService->find($id);
        if (!$rfid['bool']) {
            abort(500);
        }
        if (!$rfid['result']) {
            abort(404);
        }
        $data['rfid'] = $rfid['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $rfid = $this->rfidService->delete($id);
        if ($rfid['bool']) {
            // user activity logs
            callUserActivityLogs("Delete RFID Tag", __FUNCTION__, __FILE__, __DIR__, true, "Delete Successfully");

            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('admin.rfid.rfids.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Delete RFID Tag", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong. Please try again.");

            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }
        return response()->json($response);
    }

    // For Csv upload

    /**
     * @param Request $request
     * @param $houseId
     * @return JsonResponse
     */
    public function getAllUsersByHouseId(Request $request)
    {
        $houseId = $request->houseId ?? null;
        $residents = $this->userService->getAllUsersByHouseId($houseId)['result'];
        if ($residents->isNotEmpty()) {
            $response = ['status' => true, 'message' => 'Resident List', 'result' => $residents];
        } else {
            $response = ['status' => false, 'message' => 'No Records Found!'];
        }
        return response()->json($response);

    }

    function csvToArray($handle = '')
    {
        $HEADER = '#,rfid_tag_num,house_assigned,resident_first_name,resident_last_name,mailing_address,driver_license_number,license_plate,state_registered,vehicle_type,make,color,year,model,insurance_company_name,policy_number,policy_expiration_date,additional_drivers';

        $fail_message = '';
        $header = null;
        $csv = array();
        $row = 1;
        $imp_error = 0;
        while (($data = fgetcsv($handle, 5000, ',')) !== false) {
            if ($row == 1) {
                $first_row = array_diff($data, explode(",", $HEADER));
                if (count($first_row) > 0) {
                    $imp_error = 1;
                    $fail_message .= "<strong>Error!</strong> Invalid column name somewhere in your CSV file.";
                    $valid_file = 0;
                    return $fail_message;
                }
            }

            if (!$header) {
                $header = $data;
            } else {
                $csv[] = array_combine($header, $data);
            }

            $row++;
        }
        fclose($handle);
        return $csv;
    }

    function generateSkipRFIDRecords($skippedRowsArray)
    {
        return Excel::download(new RFIDSkippedRecordsExports($skippedRowsArray), 'rfid_skipped_records.csv');
    }

    /**
     * Suspend or Unsuspend RFID
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws Exception
     */
    public function suspendOrUnsuspend(Request $request)
    {
        $id = $request->id;

        $suspend;
        $message;

        if ($request->suspendId == '0') {
            $suspend = '1';
            $message = 'RFID Suspended Successfully';
        } else if ($request->suspendId == '1') {
            $suspend = '0';
            $message = 'RFID Unsuspended Successfully';

        }

        $data = array(
            'suspend' => $suspend
        );

        $rfid = $this->rfidService->update($data, $id);

        if ($rfid['result']) {
            // user activity logs
            callUserActivityLogs("Suspend or Unsuspend RFID Tag", __FUNCTION__, __FILE__, __DIR__, true, $message);

            $response = ['status' => true, 'message' => $message, 'redirect' => route('admin.rfid.rfids.index')];
        } else {
            // user activity logs
            callUserActivityLogs("Suspend or Unsuspend RFID Tag", __FUNCTION__, __FILE__, __DIR__, false, "Something went wrong");

            $response = ['status' => false, 'message' => 'Something went wrong'];
        }

        return response()->json($response);
    }

    //Mark: Job function

    public function importRfidTag1(Request $request)
    {
        $rules['rfid_csv'] = 'required|file';

        if (auth()->user()->role_id != 2) {
            $rules['community_id'] = 'required';
        }

        $messages['rfid_csv.required'] = 'Select a file to import.';
        $messages['rfid_csv.file'] = 'Please select a csv file to import.';

        if (auth()->user()->role_id != 2) {
            $messages['community_id.required'] = 'Select community.';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $validator->validate();
        }

        // Validation if wrong headers in csv
        $csv = $request->file('rfid_csv');
        $filename = $csv->getRealPath();

        if (($handle = fopen($filename, 'r')) !== false) {

            $HEADER = '#,rfid_tag_num,house_assigned,resident_first_name,resident_last_name,mailing_address,driver_license_number,license_plate,state_registered,vehicle_type,make,color,year,model,insurance_company_name,policy_number,policy_expiration_date,additional_drivers';

            $a = 0;

            while (($data = fgetcsv($handle, 5000, ',')) !== false) {

                if ($a == 0) {
                    $headerSent = '';

                    foreach ($data as $key => $value) {
                        $headerSent .= $value . ",";
                    }

                    $headerSent = chop($headerSent, ",");

                    $headerSent = explode(",", $headerSent);
                    $HEADER = explode(",", $HEADER);

                    //if columns are less or more
                    if (count($headerSent) != 18) {
                        $fail_message = "<strong>Error!</strong> Wrong numbers of columns, extra or missing column in CSV file.";
                        $validator->after(function ($validator) use ($fail_message) {
                            $validator->errors()->add('rfid_csv', $fail_message);
                        });
                        $validator->validate();
                    }

                    $standardHeaders = ["rfid_tag_num", "house_assigned", "resident_first_name", "resident_last_name", "mailing_address", "driver_license_number", "license_plate", "state_registered", "vehicle_type", "make", "color", "year", "model", "insurance_company_name", "policy_number", "policy_expiration_date", "additional_drivers"];

                    //if any wrong column given
                    foreach ($standardHeaders as $standardHeader) {
                        if (!array_search($standardHeader, $headerSent)) {
                            $fail_message = "<strong>Error!</strong> Invalid column given in your CSV file";
                            $validator->after(function ($validator) use ($fail_message) {
                                $validator->errors()->add('rfid_csv', $fail_message);
                            });
                            $validator->validate();
                        }
                    }

                }

                $a++;

            }
        }

        //         make a directory  if not exist
        if (!Storage::exists('/storage/app/public/csvrfidfiles')) {
            Storage::makeDirectory('/storage/app/public/csvrfidfiles', 0775, true); //creates directory
        }

        $path = Storage::disk('local')->put("\storage\app\public\csvrfidfiles", $request->file('rfid_csv'));

        $path = str_replace('\\', '/', $path);

        $communityId;
        if (auth()->user()->role_id == '1') {
            $communityId = $request->community_id;
        } else if (auth()->user()->role_id == '2') {
            $communityId = auth()->user()->community_id;
        }

        $data = array($path, $communityId, auth()->user()->email);

        $handle = fopen($filename, 'r');

        $pathParts = explode("/", $path);

        $a = 0;
        $totalRecords = 0;
        while (($data1 = fgetcsv($handle, 5000, ',')) !== false) {
            if ($a == 0) {

            } else {
                RfidCsv::create([
                    'rfid_tag_num' => $data1[1],
                    'house_assigned' => $data1[2],
                    'resident_first_name' => $data1[3],
                    'resident_last_name' => $data1[4],
                    'mailing_address' => $data1[5],
                    'driver_license_number' => $data1[6],
                    'license_plate' => $data1[7],
                    'state_registered' => $data1[8],
                    'vehicle_type' => $data1[9],
                    'make' => $data1[10],
                    'color' => $data1[11],
                    'year' => $data1[12],
                    'model' => $data1[13],
                    'insurance_company_name' => $data1[14],
                    'policy_number' => $data1[15],
                    'policy_expiration_date' => $data1[16],
                    'additional_drivers' => $data1[17],
                    'community_id' => $communityId,
                    'file_name' => $pathParts[5]]);

                $totalRecords++;
            }

            $a++;
        }

        RfidCsvFile::create(
            [
                'file_name' => $pathParts[5],
                'total_records' => $totalRecords,
                'process_records' => 0,
                'community_id' => $communityId
            ]
        );


        return response()->json(['status' => true, 'message' => '', 'redirect' => route('admin.rfid.rfid-imported-file-saved-records', ['file_name' => $pathParts[5]])]);

    }

    /**
     * imported files
     *
     * @param Request $request
     * @return JsonResponse
     */
    function importedFiles(Request $request)
    {
        $data['fileName'] = $request->file_name ? $request->file_name : '';
        return view($this->folderLink . 'imported-files', $data);
    }

    /**
     * Reads file data and give result for table
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function importedFileData(Request $request)
    {

        $rfidCsvFiles = RfidCsvFile::with('community');
        auth()->user()->role_id == 2 ? $rfidCsvFiles = $rfidCsvFiles->where('community_id', auth()->user()->community_id) : "";
        $rfidCsvFiles = $rfidCsvFiles->get();

        return datatables()->of($rfidCsvFiles)
            ->addColumn('community', function ($model) {
                return $model->community->name;
            })
            ->editColumn('created_at', function ($model) {
                return $model->created_at;
            })
            ->addColumn('links', function ($model) {
                return '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">
                               <a class="dropdown-item" href="' . route('admin.rfid.rfid-imported-file-saved-records', ['file_name' => $model->file_name]) . '"><i class="nav-icon fas fa-tasks" aria-hidden="true"></i> Process Records </a>
                               <a class="dropdown-item" href="' . route('admin.rfid.rfid-import-file-information', ['file_name' => $model->file_name]) . '"><i class="nav-icon fas fa-eye" aria-hidden="true"></i> View Records </a>
                               <form action="' . route('admin.rfid.rfid-csv-files-delete', [$model->id]) . '" method="POST" class="ajax-form">
                               ' . csrf_field() . '
                               <input type="hidden" name="id" value="' . $model->id . '">
                               <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                               </form>
                            </div>
                        </div>';
            })
            ->rawColumns(['links'])
            ->make(true);

    }

    /**
     * function to delete imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteImportedFile(Request $request)
    {
        if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . $request->file_name)) {
            unlink(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . $request->file_name);
            return response()->json(['status' => true, 'message' => 'Success! File deleted successfully', 'redirect' => route('admin.rfid.rfid-imported-files')]);
        } else {
            return response()->json(['status' => false, 'message' => 'Failed! File not found', 'redirect' => route('admin.rfid.rfid-imported-files')]);
        }
    }

    /**
     * function to download imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadImportedFile(Request $request)
    {

        $fileName = $request->file_name;

        $filepath = storage_path("app\storage\app\public\csvrfidfilesresult\\" . $fileName);

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . 'file.csv')) {
            unlink(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . 'file.csv');
        }

        if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . $request->file_name)) {
            //creating file csv
            $fp = fopen(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . 'file.csv', 'w');

            fputcsv($fp, [
                '#',
                'rfid_tag_num',
                'house_assigned',
                'resident_first_name',
                'resident_last_name',
                'mailing_address',
                'driver_license_number',
                'license_plate',
                'state_registered',
                'vehicle_type',
                'make',
                'color',
                'year',
                'model',
                'insurance_company_name',
                'policy_number',
                'policy_expiration_date',
                'additional_drivers',
                'status',
                'message',
                'id'
            ]);

            foreach ($fileData as $dat) {
                fputcsv($fp, $dat, ",");
            }

            fclose($fp);

            $filepath = storage_path("app/storage/app/public/csvrfidfilesresult/" . 'file.csv');

            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                die();
            }
        } else {
            return back()->with('fileNotFound', 'YES');
        }
    }

    /**
     * function to download imported file
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadFailRecordsImportedFile(Request $request)
    {

        $fileName = $request->file_name;

        $filepath = storage_path("app\storage\app\public\csvrfidfilesresult\\" . $fileName);

        $fileData = [];

        if (!empty($request->file_name)) {
            if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . $request->file_name)) {
                if (($open = fopen(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . $request->file_name, "r")) !== FALSE) {

                    while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                        $fileData[] = $data;
                    }

                    fclose($open);
                }
            }
        }

        if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . 'failed_records_file.csv')) {
            unlink(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . 'failed_records_file.csv');
        }

        if (Storage::disk('local')->exists('storage\app\public\csvrfidfilesresult\\' . $request->file_name)) {
            //creating file csv
            $fp = fopen(storage_path("app/storage/app/public/csvrfidfilesresult") . "/" . 'failed_records_file.csv', 'w');

            fputcsv($fp, [
                '#',
                'rfid_tag_num',
                'house_assigned',
                'resident_first_name',
                'resident_last_name',
                'mailing_address',
                'driver_license_number',
                'license_plate',
                'state_registered',
                'vehicle_type',
                'make',
                'color',
                'year',
                'model',
                'insurance_company_name',
                'policy_number',
                'policy_expiration_date',
                'additional_drivers',
                'status',
                'message'
            ]);

            foreach ($fileData as $dat) {
                if ($dat[18] == "Failure") {
                    fputcsv($fp, $dat, ",");
                }
            }

            fclose($fp);

            $filepath = storage_path("app/storage/app/public/csvrfidfilesresult/" . 'failed_records_file.csv');

            if (file_exists($filepath)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filepath));
                flush(); // Flush system output buffer
                readfile($filepath);
                die();
            }
        } else {
            return back()->with('fileNotFound', 'YES');
        }
    }

    public function rfidImportedFileSavedRecords(Request $request)
    {
        $data['fileName'] = $request->file_name ? $request->file_name : '';
        return view($this->folderLink . 'imported-file-saved-records', $data);
    }

    public function rfidImportedFileSavedRecordsData(Request $request)
    {
        $rfidCsvs = RfidCsv::where('file_name', $request->file_name)->get();

        return datatables()->of($rfidCsvs)
            ->addColumn('rfid_tag_num', function ($model) {
                return $model->rfid_tag_num;
            })
            ->addColumn('house_assigned', function ($model) {
                return $model->house_assigned;
            })
            ->addColumn('resident_first_name', function ($model) {
                return $model->resident_first_name;
            })
            ->addColumn('resident_last_name', function ($model) {
                return $model->resident_last_name;
            })
            ->addColumn('mailing_address', function ($model) {
                return $model->mailing_address;
            })
            ->addColumn('driver_license_number', function ($model) {
                return $model->driver_license_number;
            })
            ->addColumn('license_plate', function ($model) {
                return $model->license_plate;
            })
            ->addColumn('state_registered', function ($model) {
                return $model->state_registered;
            })
            ->addColumn('vehicle_type', function ($model) {
                return $model->vehicle_type;
            })
            ->addColumn('make', function ($model) {
                return $model->make;
            })
            ->addColumn('color', function ($model) {
                return $model->color;
            })
            ->addColumn('year', function ($model) {
                return $model->year;
            })
            ->addColumn('model', function ($model) {
                return $model->model;
            })
            ->addColumn('insurance_company_name', function ($model) {
                return $model->insurance_company_name;
            })
            ->addColumn('policy_number', function ($model) {
                return $model->policy_number;
            })
            ->addColumn('policy_expiration_date', function ($model) {
                return $model->policy_expiration_date;
            })
            ->addColumn('additional_drivers', function ($model) {
                return $model->additional_drivers;
            })
            ->addColumn('status', function ($model) {
                return $model->status;
            })
            ->addColumn('message', function ($model) {
                return $model->message;
            })
            ->addColumn('links', function ($model) {
                $actionHTML = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if ($model->status == "failure") {
                    $actionHTML .= '<a class="dropdown-item" href="' . route('admin.rfid.rfid-csvs-table-record-edit', $model->id) . '"><i class="nav-icon fas fa-edit" aria-hidden="true"></i> Edit </a>';
                }

                $actionHTML .= '
                    <form action="' . route('admin.rfid.rfid-csvs-delete', [$model->id]) . '" method="POST" class="ajax-form">
                    ' . csrf_field() . '
                    <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                    </form>
                    </div>
                    </div>';

                return $actionHTML;
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('rfid_tag_number') && !empty($request->rfid_tag_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['rfid_tag_num'], $request->rfid_tag_number);
                    });
                }
                if ($request->has('house_assigned') && !empty($request->house_assigned)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['house_assigned'], $request->house_assigned);

                    });
                }
                if ($request->has('resident_first_name') && !empty($request->resident_first_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['resident_first_name'], $request->resident_first_name);
                    });
                }
                if ($request->has('resident_last_name') && !empty($request->resident_last_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['resident_last_name'], $request->resident_last_name);
                    });
                }
                if ($request->has('mailing_address') && !empty($request->mailing_address)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['mailing_address'], $request->mailing_address);
                    });
                }
                if ($request->has('driver_license_number') && !empty($request->driver_license_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['driver_license_number'], $request->driver_license_number);
                    });
                }
                if ($request->has('license_plate') && !empty($request->license_plate)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['license_plate'], $request->license_plate);
                    });
                }
                if ($request->has('state_registered') && !empty($request->state_registered)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['state_registered'], $request->state_registered);
                    });
                }
                if ($request->has('vehicle_type') && !empty($request->vehicle_type)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['vehicle_type'], $request->vehicle_type);
                    });
                }
                if ($request->has('make') && !empty($request->make)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['make'], $request->make);
                    });
                }
                if ($request->has('color') && !empty($request->color)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['color'], $request->color);
                    });
                }
                if ($request->has('year') && !empty($request->year)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['year'], $request->year);
                    });
                }
                if ($request->has('model') && !empty($request->model)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['model'], $request->model);
                    });
                }
                if ($request->has('insurance_company_name') && !empty($request->insurance_company_name)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['insurance_company_name'], $request->insurance_company_name);
                    });
                }
                if ($request->has('policy_number') && !empty($request->policy_number)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['policy_number'], $request->policy_number);
                    });
                }
                if ($request->has('policy_expiration_date') && !empty($request->policy_expiration_date)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['policy_expiration_date'], $request->policy_expiration_date);
                    });
                }
                if ($request->has('additional_drivers') && !empty($request->additional_drivers)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['additional_drivers'], $request->additional_drivers);
                    });
                }
                if ($request->has('status') && !empty($request->status)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['status'], $request->status);
                    });
                }
                if ($request->has('message') && !empty($request->message)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['message'], $request->message);
                    });
                }

            })
            ->rawColumns(['links'])
            ->make(true);

    }

    /**
     * Determine if a given string contains all array values.
     * Not case sensitive
     *
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public function containsInsensitive($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && mb_stripos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    public function saveRfidsFromRfidCsvsTable(Request $request)
    {
        $rfidCsvs = RfidCsv::where(['file_name' => $request->file_name, 'mark' => '0'])->limit(10)->get();

        $count = 0;

        if (!empty($rfidCsvs)) {
            foreach ($rfidCsvs as $rfidCsv) {
                $rfidTagNum = $rfidCsv->rfid_tag_num;

                $rowError = false;
                $csvError = '';
                $comma = false;

                $houseName = $rfidCsv->house_assigned;

                //if empty rfid tag number given
                if (empty($rfidCsv->rfid_tag_num)) {
                    $rowError = false;
                    $csvError = 'blank rfid tag number';
                    $comma = true;
                }

                //if empty house name given
                if (empty($houseName)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank house name' : $csvError .= 'blank house name';
                    $comma = true;
                }

                //if empty first name given
                if (empty($rfidCsv->resident_first_name)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank first name' : $csvError .= 'blank first name';
                    $comma = true;
                }

                //if last name given
                if (empty($rfidCsv->resident_last_name)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank last name' : $csvError .= 'blank last name';
                    $comma = true;
                }

                //if license plate is blank
                if (empty($rfidCsv->license_plate)) {
                    $rowError = true;
                    $comma ? $csvError .= ', blank license plate' : $csvError .= 'blank license plate';
                    $comma = true;
                }

                //if not correct rfid_tag_num
                if (!empty($rfidCsv->rfid_tag_num)) {
                    if (!preg_match('/^\d+$/', $rfidCsv->rfid_tag_num)) {
                        $rowError = true;
                        $comma ? $csvError .= ', Not correct rfid number, it should be a number' : $csvError .= 'Not correct rfid number, it should be a number';
                        $comma = true;
                    } else {
                        $rfid = Rfid::where(['community_id' => $rfidCsv->community_id, 'rfid_tag_num' => $rfidCsv->rfid_tag_num])->first();
                        if (!empty($rfid)) {
                            $rowError = true;
                            $comma ? $csvError .= ', rfid number exist' : $csvError .= 'rfid number exist';
                            $comma = true;
                        }
                    }
                }

                //if house name does not exist in community
                if (!empty($houseName)) {
                    $house = House::where(['community_id' => $rfidCsv->community_id, 'house_name' => $houseName])->first();
                    if (empty($house)) {
                        $rowError = true;
                        $comma ? $csvError .= ', house does not exist in community' : $csvError .= 'house does not exist in community';
                        $comma = true;
                    } else {
                        if (!empty($rfidCsv->resident_first_name) && !empty($rfidCsv->resident_last_name)) {
                            $user = User::where(['community_id' => $rfidCsv->community_id, 'house_id' => $house->id, 'first_name' => $rfidCsv->resident_first_name])->first();
                            if (empty($user)) {
                                $rowError = true;
                                $comma ? $csvError .= ', resident first name does not exist in house' : $csvError .= 'resident first name does not exist in house';
                                $comma = true;
                            }

                            $user = User::where(['community_id' => $rfidCsv->community_id, 'house_id' => $house->id, 'last_name' => $rfidCsv->resident_last_name])->first();
                            if (empty($user)) {
                                $rowError = true;
                                $comma ? $csvError .= ', resident last name does not exist in house' : $csvError .= 'resident last name does not exist in house';
                                $comma = true;
                            }
                        }
                    }

                    if (!isset($house->id)) {
                        $rowError = true;
                        $comma ? $csvError .= ', house does not exist in community' : $csvError .= 'house does not exist in community';
                        $comma = true;
                    } else {
                        $resident = User::where(['community_id' => $rfidCsv->community_id, 'house_id' => $house->id, 'first_name' => $rfidCsv->resident_first_name, 'last_name' => $rfidCsv->resident_last_name])->first();

                        //if license plate match
                        if (!isset($resident->id)) {
                            $rowError = true;
                            $comma ? $csvError .= ', resident does not exist in community' : $csvError .= 'resident does not exist in community';
                            $comma = true;

                        }

                    }
                }

                //if not correct policy expiration date
                $policyExpirationDateforCheck = $rfidCsv->policy_expiration_date;

                $policyExpirationDate = '';

                if (!empty($rfidCsv->policy_expiration_date)) {
                    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $policyExpirationDateforCheck)) {
                        $policyExpirationDate = $rfidCsv->policy_expiration_date;
                    }
                }

                //if license plate match
                if (!empty($rfidCsv->license_plate)) {
                    $vehicles = Vehicle::with('user')->where('license_plate', $rfidCsv->license_plate)->whereNull('deleted_at')->get();
                    if ($vehicles->count() > 0) {
                        foreach ($vehicles as $vehicle) {
                            if (!empty($vehicle->user)) {
                                if ($rfidCsv->community_id == $vehicle->user->community_id) {
                                    $rowError = true;
                                    $comma ? $csvError .= ', license plate exist in community' : $csvError .= 'license plate exist in community';
                                    $comma = true;
                                }
                            }
                        }
                    }
                }


                $driverLicenseNumber = $rfidCsv->driver_license_number;
                $licenseNumber = $rfidCsv->license_plate;

                if (!$rowError) {
                    $rfid = Rfid::where(['community_id' => $rfidCsv->community_id, 'rfid_tag_num' => $rfidTagNum])->first();

                    $licencePlate = removeSpaceFromString($rfidCsv->license_plate);

                    $futureDate = date('Y-m-d', strtotime('+1 year'));

                    $stateRegistered = '';
                    $vehicleType = '';
                    $make = '';
                    $color = '';
                    $year = '';
                    $model = '';
                    $insuranceCompanyName = '';
                    $policyNumber = '';
                    $additionalDrivers = '';

                    if (!empty($rfidCsv->state_registered)) {
                        $stateRegistered = $rfidCsv->state_registered;
                    }
                    if (!empty($rfidCsv->vehicle_type)) {
                        $vehicleType = $rfidCsv->vehicle_type;
                    }
                    if (!empty($rfidCsv->make)) {
                        $make = $rfidCsv->make;
                    }
                    if (!empty($rfidCsv->color)) {
                        $color = $rfidCsv->color;
                    }
                    if (!empty($rfidCsv->year)) {
                        $year = $rfidCsv->year;
                    }
                    if (!empty($rfidCsv->model)) {
                        $model = $rfidCsv->model;
                    }
                    if (!empty($rfidCsv->insurance_company_name)) {
                        $insuranceCompanyName = $rfidCsv->insurance_company_name;
                    }
                    if (!empty($rfidCsv->policy_number)) {
                        $policyNumber = $rfidCsv->policy_number;
                    }
                    if (!empty($rfidCsv->additional_drivers)) {
                        $additionalDrivers = $rfidCsv->additional_drivers;
                    }

                    if (empty($rfid) && !empty($licencePlate) && !empty($house)) {

                        $house = House::where(['community_id' => $rfidCsv->community_id, 'house_name' => $rfidCsv->house_assigned])->first();
                        $resident = User::where(['community_id' => $rfidCsv->community_id, 'house_id' => $house->id, 'first_name' => $rfidCsv->resident_first_name, 'last_name' => $rfidCsv->resident_last_name])->first();

                        if (!empty($rfidCsv->driver_license_number)) {
                            User::findOrFail($resident->id)->update(['license_number' => $rfidCsv->driver_license_number]);
                        }

                        $vehicle = Vehicle::updateOrCreate([
                            'license_plate' => $licencePlate,
                            'user_id' => $resident->id,
                        ], [
                            'make' => $make,
                            'color' => $color,
                            'year' => $year,
                            'model' => $model,
                            'state' => $stateRegistered,
                            'activation_date' => date('Y-m-d'), //date
                            'expiration_date' => $futureDate, //date
                            'vehicle_type' => $vehicleType,
                            'windshield_parking_sticker' => $additionalDrivers,
                            'policy_number' => $policyNumber,
                            'policy_expiration_date' => $policyExpirationDate, //date
                            'insurance_company_name' => $insuranceCompanyName
                        ]);

                        if (!empty($vehicle)) {
                            $rfid = Rfid::create([
                                'rfid_tag_num' => $rfidTagNum,
                                'user_id' => $resident->id,
                                'house_id' => $house->id,
                                'community_id' => $rfidCsv->community_id,
                                'vehicle_id' => $vehicle->id,
                                'license' => $licencePlate,
                                'make_model' => $make . ' ' . $model,
                                'created_by' => auth()->id(),
                                'vehicle_info' => "{$licencePlate}\n{$color}\n{$make}\n{$model}\n{$year}",
                            ]);

                            $futureDateBySlash = date('m/d/Y', strtotime('+1 year'));
                            $startEnd = date('m/d/Y') . ' - ' . $futureDateBySlash;

                            if (!empty($rfid)) {
                                //get unmatched tickets behalf of the license plate
                                $unmatchedTickets = TrafficTicket::where('ticket_type', 'unmatched')->whereNotNull('license_plate')->where('license_plate', $licencePlate)->get();
                                if ($unmatchedTickets->isNotEmpty()) {
                                    $ticketIds = $unmatchedTickets->pluck('id');
                                    $result = TrafficTicket::whereIn('id', $ticketIds)->update([
                                        'ticket_type' => 'resident',
                                        'user_id' => $resident->id,
                                        'community_id' => $resident->community_id,
                                    ]);
                                }
                            } else {
                            }

                            $rfidCsv = RfidCsv::find($rfidCsv->id);
                            $rfidCsv->mark = '1';
                            $rfidCsv->status = 'success';
                            $rfidCsv->message = 'Record imported successfully';
                            $rfidCsv->save();

                        }
                    }
                } else {
                    $rfidCsv = RfidCsv::find($rfidCsv->id);
                    $rfidCsv->mark = '1';
                    $rfidCsv->status = 'failure';
                    $rfidCsv->message = $csvError;
                    $rfidCsv->save();
                }

                $count++;
            }
        }

        $rfidCsvFile = RfidCsvFile::where('file_name', $request->file_name)->first();
        $currentProcessRecords = intval($rfidCsvFile->process_records);
        $rfidCsvFile->process_records = $currentProcessRecords + $count;
        $rfidCsvFile->save();

        return response()->json(['recordsExecuted' => $count]);
    }

    public function rfidImportFileInformation(Request $request)
    {
        $data['fileName'] = $request->file_name;
        return view($this->folderLink . 'imported-file-information', $data);
    }

    public function rfidImportFileInformationData(Request $request)
    {
        $rfidCsvs = RfidCsv::where('file_name', $request->file_name)->get();
        return datatables()->of($rfidCsvs)
            ->addColumn('links', function ($model) {

                $actionHTML = '<div class="dropdown">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                            <div class="dropdown-menu">';

                if ($model->status == "failure") {
                    $actionHTML .= '<a class="dropdown-item" href="' . route('admin.rfid.rfid-csvs-table-record-edit', $model->id) . '"><i class="nav-icon fas fa-edit" aria-hidden="true"></i> Edit </a>';
                }

                $actionHTML .= '
                    <form action="' . route('admin.rfid.rfid-csvs-delete', [$model->id]) . '" method="POST" class="ajax-form">
                    ' . csrf_field() . '
                    <button class="dropdown-item"><i class="fas fa-trash"></i> Delete</button>
                    </form>
                    </div>
                    </div>';

                return $actionHTML;
            })
            ->rawColumns(['links'])
            ->make(true);
    }

    public function rfidCsvsTableRecordEdit($id)
    {
        $data['rfidCsv'] = RfidCsv::find($id);
        $data['houses'] = House::where('community_id', $data['rfidCsv']->community_id)->get();
        return view($this->folderLink . 'rfid-csv-edit', $data);
    }

    public function rfidCsvsRecordUpdate(Request $request, $id)
    {

        $rfidData = $request->all();
        $validation = [
            'rfid_tag_num' => 'required',
            'house_id' => 'required',
            'resident_id' => 'required',
            'license_plate' => 'required',
        ];

        $message = [
            'house_id.required' => 'Please select house',
            'resident_id.required' => 'Please select resident',
        ];

        $validator = Validator::make($rfidData, $validation, $message);
        if ($validator->fails()) {
            $validator->validate();
        }

        $house = House::find($request->house_id);

        $resident = User::find($request->resident_id);

        $rfidCsv = RfidCsv::find($id);
        $rfidCsv->rfid_tag_num = $request->rfid_tag_num;
        $rfidCsv->house_assigned = $house->house_name;
        $rfidCsv->resident_first_name = $resident->first_name;
        $rfidCsv->resident_last_name = $resident->last_name;
        $rfidCsv->mailing_address = $request->mailing_address;
        $rfidCsv->driver_license_number = $request->driver_license_number;
        $rfidCsv->license_plate = $request->license_plate;
        $rfidCsv->state_registered = $request->state;
        $rfidCsv->vehicle_type = $request->vehicle_type;
        $rfidCsv->make = $request->make;
        $rfidCsv->color = $request->color;
        $rfidCsv->year = $request->year;
        $rfidCsv->model = $request->model;
        $rfidCsv->insurance_company_name = $request->insurance_company_name;
        $rfidCsv->policy_number = $request->policy_number;
        $rfidCsv->policy_expiration_date = $request->policy_expiration_date;
        $rfidCsv->additional_drivers = $request->additional_drivers;
        $rfidCsv->mark = '0';
        $rfidCsv->status = null;
        $rfidCsv->message = null;
        $rfidCsv->save();

        $rfidCsvFile = RfidCsvFile::where('file_name', $rfidCsv->file_name)->first();
        $currentProcessRecords = intval($rfidCsvFile->process_records);
        $currentProcessRecords--;
        $rfidCsvFile->process_records = $currentProcessRecords;
        $rfidCsvFile->save();

        return response()->json(['status' => true, 'message' => 'updated successfully', 'redirect' => route('admin.rfid.rfid-imported-file-saved-records', ['file_name' => $rfidCsv->file_name])]);

    }

    /*
     *  To delete rfid_csv_files table record and rfid_csvs table records related to file name
     *
     * */
    public function rfidCsvFilesDelete($id)
    {
        $rfidCsvFile = RfidCsvFile::find($id);
        $rfidCsvFile->delete();

        $rfidCsv = RfidCsv::where('file_name', $rfidCsvFile->file_name);
        $rfidCsv->delete();

        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.rfid.rfid-imported-files')]);
    }

    /*
     * To delete house_csvs table record
     *
    * */
    public function rfidCsvsDelete($id)
    {
        $rfidCsv = RfidCsv::find($id);
        $rfidCsv->delete();

        $rfidCsvFile = RfidCsvFile::where('file_name', $rfidCsv->file_name)->first();
        $totalRecords = intval($rfidCsvFile->total_records) - 1;
        $processedRecords = intval($rfidCsvFile->process_records) - 1;
        $rfidCsvFile->total_records = $totalRecords;
        $rfidCsvFile->process_records = $processedRecords;
        $rfidCsvFile->save();

        return response()->json(['status' => true, 'message' => 'Deleted successfully', 'redirect' => route('admin.rfid.rfid-import-file-information', ['file_name' => $rfidCsvFile->file_name])]);
    }

    /**
     * Save data in vehicle info column
     */
    public function saveVehicleInfoData()
    {
        $rfids = Rfid::with('vehicle')->get();
        foreach ($rfids as $rfid) {
            $licensePlate = strtoupper($rfid->vehicle->license_plate);
            $vehicleInfo = $licensePlate . ", " . $rfid->vehicle->color . ", " . $rfid->vehicle->make . ", " . $rfid->vehicle->model . ", " . $rfid->vehicle->year;
            $rfid->vehicle_info = $vehicleInfo;
            $rfid->save();
        }
    }
}
