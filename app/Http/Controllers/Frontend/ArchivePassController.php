<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Resources\ActivePassCollection;
use App\Http\Resources\ActivePassDateCollection;
use App\Services\PassService;
use App\Services\PassUserService;
use App\Services\UserService;
use Illuminate\Http\Request;

class ArchivePassController extends Controller
{
    protected $folderLink = 'frontend.passes.archived-passes.';

    public $passService;
    public $passUserService;
    public $userService;

    public function __construct(
        PassService $passService,
        PassUserService $passUserService,
        UserService $userService

    )
    {
        $this->userService = $userService;
        $this->passService = $passService;
        $this->passUserService = $passUserService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['userId'] = $request->userId ?? "";
        return view($this->folderLink . 'index',$data);
    }

    /**
     * get user scanned passes
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function scannedPassesData(Request $request)
    {
        $userId = auth()->id();
        if (!empty($request->userId)){
            $userId = $request->userId;
            $user = $this->userService->find($userId)['result'];
            if (!empty($user) &&   $user->house_id != auth()->user()->house_id){
                $userId = null;
            }
        }
        $search = $request->search ?? "";
        $filter_type = $request->filter_type ?? "";
        $filter_search = $request->filter_search ?? "";
        $data = $this->passUserService->getScannedPassUserByUser($userId,$search,$filter_type,$filter_search,10);
        return response()->json([
            'data' =>  ActivePassCollection::collection($data['result'])
        ]);
    }

    /**
     * get scanned passes Dates for filter records
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function scannedPassesDates(Request $request){
        $userId = auth()->id();
        if (!empty($request->userId)){
            $userId = $request->userId;
            $user = $this->userService->find($userId)['result'];
            if (!empty($user) &&   $user->house_id != auth()->user()->house_id){
                $userId = null;
            }
        }
        $data = $this->passUserService->getScannedPassUserDatesByUser($userId);
        return response()->json([
            'data' =>  ActivePassDateCollection::collection($data['result'])
        ]);
    }

}
