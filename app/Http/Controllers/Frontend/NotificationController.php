<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\NotificationService;

class NotificationController extends Controller
{
    public $folderLink = 'frontend.notifications.';
    public $notificationService;


    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data["type"] = $request->type ?? "";
        return view($this->folderLink.'index', $data);
    }

    public function notificationsData(Request $request){
        $type = 'zuul';
        if($request->has('type') && $request->type == "pc"){
            $type = 'parental_control';
        }
        $notifications = $this->notificationService->getNotifications(auth()->id(),$type,30);
        return response()->json([
            'data' =>  $notifications
        ]);

    }

    /**
     * mark as read notification
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function markReadNotification(Request $request){
        $notificationId = $request->notificationId;
        $type = ($request->has('notificationType') && $request->notificationType == "pc")?"parental_control":"zuul";
        $result = $this->notificationService->updateNotification(['is_read'=>'1'], $notificationId, $type);
        if ($result['bool']) {
            $response = ['status' => true, 'message' => 'Notification Cleared' ];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function markAllReadNotification(Request $request){
        $notificationIds = $request->notificationIds;
        $type = ($request->has('notificationType') && $request->notificationType == "pc") ?"parental_control":"zuul";
        if(!empty($notificationIds)){
            foreach($notificationIds as $id){
                $this->notificationService->updateNotification(['is_read'=>'1'], $id, $type);
            }
        }
        return response()->json(['status' => true, 'message' => 'Notification Cleared' ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
