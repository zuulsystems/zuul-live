<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventRequest;
use Illuminate\Http\Request;
use App\Services\EventService;

class EventController extends Controller
{
    protected $folderLink = 'frontend.events.';
    public $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view($this->folderLink.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view($this->folderLink.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        $eventData = $request->all();
        $eventData['created_by'] = auth()->id();
        $eventData['community_id'] = auth()->user()->community_id;

        $event = $this->eventService->create($eventData);
        if ($event['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('events.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event = $this->eventService->getEventWithRight(auth()->user()->id, $id);
        checkErrors($event);
        $data['event'] = $event['result'];
        return view($this->folderLink.'edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(EventRequest $request, $id)
    {
        $eventData = $request->all();
        $event = $this->eventService->update($eventData,$id);

        if ($event['bool']) {
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('events.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->eventService->delete($id);
        $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('events.index')];
        return response()->json($response);
    }

    public function eventData(Request $request){
        $result =  $this->eventService->getEvents(30,$request->search, auth()->id())['result'];
        return response()->json([
            'data' =>  $result
        ]);
    }
}
