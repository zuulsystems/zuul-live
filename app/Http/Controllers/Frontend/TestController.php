<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;

class TestController extends Controller
{
    public function index()
    {
        $currentLatLng = "24.920069922350827, 67.085827606491";
        $cameraList = [
            ['mac_address' => '3423.234.234', 'camera_name' => 'camera 1', 'latLang' => '24.92005046246244, 67.08595635252165'],
            ['mac_address' => '3423.234.254', 'camera_name' => 'camera 2', 'latLang' => '24.923981, 67.090838'],
            ['mac_address' => '3423.123.251', 'camera_name' => 'camera 3', 'latLang' => '24.925635, 67.093284'],
            ['mac_address' => '3423.123.251', 'camera_name' => 'camera 3', 'latLang' => '24.920966, 67.087165'],
            ['mac_address' => '3423.123.251', 'camera_name' => 'camera 3', 'latLang' => '24.920286, 67.086196'],
            ['mac_address' => '3423.123.251', 'camera_name' => 'camera 3', 'latLang' => '24.920098, 67.085942'],
            ['mac_address' => '3423.123.251', 'camera_name' => 'camera 3', 'latLang' => '24.920044, 67.085869'],
        ];
        $currentLatLng = "24.920069922350827, 67.085827606491";

        $lat = 24.920069922350827;
        $lon = 67.085827606491;
        // Only show places within 100km
        $distance = 0.01; // 10 meter
        $result = DB::select('SELECT * FROM (
        SELECT *,
            (
                (
                    (
                        acos(
                            sin(( ' . $lat . ' * pi() / 180))
                            *
                            sin(( `lat` * pi() / 180)) + cos((  ' . $lat . ' * pi() /180 ))
                            *
                            cos(( `lat` * pi() / 180)) * cos(((  ' . $lon . ' - `long`) * pi()/180)))
                    ) * 180/pi()
                ) * 60 * 1.1515 * 1.609344
            )
        as distance FROM `cameras`
    ) markers
    WHERE distance <= ' . $distance . '
    LIMIT 15;');
        dd($result);
        dd($cameraList);
        dd("Hello");
    }

    public function firebase()
    {
        $factory = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
        $realtimeDatabase = $factory->createDatabase();
        $realtimeDatabase->getReference('scanners/56:46245:5417')->set([
            'mac_address' => '156:46245:54312',
            'status' => 'active',
            'capture_progress' => false,
            'imageUrl' => 'sdfdsf',
        ]);
        $reference = $realtimeDatabase->getReference('scanners');

    }
}
