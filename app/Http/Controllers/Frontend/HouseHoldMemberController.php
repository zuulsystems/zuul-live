<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleRequest;
use App\Models\Country;
use App\Services\UserService;
use App\Services\VehicleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HouseHoldMemberController extends Controller
{
    protected $folderLink = 'frontend.household-members.';
    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view($this->folderLink.'index');
    }

    /**
     * get household members list
     * @param Request $request
     * @return mixed
     */
    public function houseHoldMembersData(Request $request){
       $search = $request->search;
       $result =  $this->userService->getHouseHoldMemberDetail(auth()->user()->house_id,50, $search);
       $permission['can_manage_family'] = auth()->user()->hasPermission('can_manage_family');
       $permission['can_send_passes'] = auth()->user()->hasPermission('can_send_passes');
       $permission['allow_parental_control'] = auth()->user()->hasPermission('allow_parental_control');
       $permission['is_license_locked'] = auth()->user()->hasPermission('is_license_locked');
       $permission['head_of_family'] = auth()->user()->hasRole('family_head');

        return response()->json([
           'data' =>  $result,
           'permission' => $permission
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function store(VehicleRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function edit($id)
    {
        $data['countries'] = Country::all();
        $user = $this->userService->find($id);
        checkErrors($user);
        $data['user'] = $user['result'];
        return view($this->folderLink.'edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $memberData = $request->all();
        $memberData['phone_number'] = trim(str_replace("-", "", $memberData['phone_number']));
        $validation = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required|unique:users,phone_number,'.$id,
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
        ];
        $validator = Validator::make($memberData, $validation, $messages);
        if ($validator->fails()) {
            $validator->validate();
        }
        $user = $this->userService->find($id)['result'];
        $result = $this->userService->update($memberData,$id)['result'];
        //assign permission can manage family
        if ($request->has('can_manage_family') && $request->can_manage_family == '1' ){
            $user->assignPermission('can_manage_family');
        } else{
            $user->revokePermission('can_manage_family');
        }
        //assign permission can send passes
        if ($request->has('can_send_passes') && $request->can_send_passes == '1' ){
            $user->assignPermission('can_send_passes');
        } else{
            $user->revokePermission('can_send_passes');
        }
        //assign permission allow parental control
        if ($request->has('allow_parental_control') && $request->allow_parental_control == '1' ){
            $user->assignPermission('allow_parental_control');
        } else{
            $user->revokePermission('allow_parental_control');
        }
        if ($result){
            $response = ['status' => true,'message' => 'Updated Successfully','redirect' =>route('household-members.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = $this->userService->delete($id);
        if($user['bool'])
        {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('household-members.index')];
        }
        else
        {
            $response = ['status' => false, 'message' => 'Something went wrong.', 'redirect' => route('household-members.index')];
        }
        return response()->json($response);
    }
}
