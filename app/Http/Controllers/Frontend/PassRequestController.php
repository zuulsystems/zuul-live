<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddPassRequest;
use App\Models\Community;
use App\Models\Pass;
use App\Models\PassUser;
use App\Models\User;
use App\Services\PassRequestService;
use App\Services\UserContactService;
use Illuminate\Http\Request;

class PassRequestController extends Controller
{
    protected $folderLink = 'frontend.pass-requests.';
    public $passRequestService;
    public $userContactService;

    public function __construct(
        PassRequestService $passRequestService,
        UserContactService $userContactService
    )
    {
        $this->passRequestService = $passRequestService;
        $this->userContactService = $userContactService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['userContacts'] = $this->userContactService->getAllByUserHasPermissionCanSendPassById(auth()->id())['result'];
        return view($this->folderLink . 'create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(AddPassRequest $request)
    {
        $passRequest = $this->passRequestService->sendPassRequest($request->all());
        if ($passRequest['bool']) {
            $response = ['status' => true, 'message' => 'Request Sent Successfully', 'redirect' => route('passes.pass-requests.create')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again'];
        }
        return response()->json($response);
    }

    /**
     * reject pass request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function rejectPassRequest(Request $request){
        $passRequest = $this->passRequestService->rejectPassRequest($request->all());
       if ($passRequest['bool']) {
           $response = ['status' => true, 'message' => 'Rejected Successfully', 'redirect' => route('notifications.index')];
       } else {
           $response = ['status' => false, 'message' => 'Something went wrong.Please try again'];
       }
        return response()->json($response);
    }

    /**
     * accept request a pass
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function acceptPassRequest(Request $request){
        $passRequest = $this->passRequestService->acceptPassRequest($request->all());
        if ($passRequest['bool']) {
            $response = ['status' => true, 'message' => 'Pass Accept Successfully', 'redirect' => route('notifications.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again'];
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function qr_Code($qr){
        $qr = base64_decode($qr);
        $pass_data = PassUser::where('qr_code',$qr)->get();
        return view($this->folderLink . 'qr_screen',compact('pass_data'));
    }

    public function qr_CodeGroup($qr){
        return view($this->folderLink . 'qr_screen_group',compact('qr'));
    }

    public function qrGetGroupData($phone){
        $user_data = User::whereNull('deleted_at')->where('phone_number',$phone)->first();

        if($user_data){
            $user = $user_data->id;
        }else{
            return 0;
        }

        $pass_data = PassUser::where('user_id',$user)->orderBy('id', 'DESC')->first();

        if($pass_data){

            $data['qr_code'] = $pass_data->qr_code;

            $res_pass_data  = Pass::where('id',$pass_data->pass_id)->first();
            $user_pass_data = User::where('id',$res_pass_data->created_by)->first();
            $community_pass_data = Community::where('id',$user_pass_data->community_id)->first();

            $data['community_name'] = $community_pass_data->name;
            $data['resident_name'] = $user_pass_data->first_name;
            $data['guest_name'] = $user_data->first_name;
            $data['expiry'] = date('Y-m-d h:i a', strtotime($res_pass_data->pass_date));

            return $data;
        }else{
            return 0;
        }
    }

}
