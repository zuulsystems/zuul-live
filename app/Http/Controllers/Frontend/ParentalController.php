<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ParentalControl;
use App\Services\ParentalControlService;

use Illuminate\Http\Request;

class ParentalController extends Controller
{
    public $folderLink = 'frontend.parental-control.';
    public $parentControlService;

    public function __construct(ParentalControlService $parentControlService)
    {
        $this->parentControlService = $parentControlService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data["member_id"] = $id;

        $where= ['parent_id'=>auth()->user()->id, 'member_id'=>$id];
        $data= ['parent_id'=>auth()->user()->id, 'member_id'=>$id];

        $data["parentalControlValues"] = $this->parentControlService->firstOrCreate($where, $data)['result'];

        $data['options'] =  ["notify_when_member_guest_arrive", "notify_when_member_send_pass_to_others",
            "notify_when_member_receive_a_pass",
            "notify_when_member_retract_a_pass",
            "notify_when_member_update_a_pass",
            "notify_when_pass_retracted_included_member",
            "notify_when_pass_updated_included_member",
            "notify_when_member_request_a_pass_with_offensive_words",
            "blacklist_member_to_receive_request_a_pass",
            "allow_parental_controls_to_member"];

        return view($this->folderLink.'index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        //
        $data["notify_when_member_guest_arrive"] = $request->notify_when_member_guest_arrive ? '1' : '0';
        $data["notify_when_member_send_pass_to_others"] = $request->notify_when_member_send_pass_to_others ? '1' : '0';
        $data["notify_when_member_receive_a_pass"] = $request->notify_when_member_receive_a_pass ? '1' : '0';
        $data["notify_when_member_retract_a_pass"] = $request->notify_when_member_retract_a_pass ? '1' : '0';
        $data["notify_when_member_update_a_pass"] = $request->notify_when_member_update_a_pass ? '1' : '0';
        $data["notify_when_pass_retracted_included_member"] = $request->notify_when_pass_retracted_included_member ? '1' : '0';
        $data["notify_when_pass_updated_included_member"] = $request->notify_when_pass_updated_included_member ? '1' : '0';
        $data["notify_when_member_request_a_pass_with_offensive_words"] = $request->notify_when_member_request_a_pass_with_offensive_words ? '1' : '0';
        $data["blacklist_member_to_receive_request_a_pass"] = $request->blacklist_member_to_receive_request_a_pass ? '1' : '0';
        $data["allow_parental_controls_to_member"] = $request->allow_parental_controls_to_member ? '1' : '0';

        $result = $this->parentControlService->update($data,$request->parental_control_id);

        if ($result['bool']){
            $response = ['status' => true,'message' => 'Updated Successfully','redirect' =>route('parental-control.edit',[$id])];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
