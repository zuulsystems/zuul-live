<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\PushNotificationTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\ActivePassCollection;
use App\Http\Resources\PassDetailCollection;
use App\Http\Resources\SentPassCollection;
use App\Jobs\Zuul2EmailJob;
use App\Services\NotificationService;
use App\Services\PassService;
use App\Services\PassUserService;
use DB;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;

class DashboardController extends Controller
{
    use PushNotificationTrait;

    public $userService;
    public $passService;
    public $passUserService;
    public $notificationService;
    protected $folderLink = 'frontend.dashboard.';

    public function __construct(
        UserService         $userService,
        PassService         $passService,
        PassUserService     $passUserService,
        NotificationService $notificationService
    )
    {
        $this->userService = $userService;
        $this->passService = $passService;
        $this->passUserService = $passUserService;
        $this->notificationService = $notificationService;
    }

    public function login()
    {
        return redirect()->route('login');
    }

    public function index()
    {
        return view($this->folderLink . 'index');
    }

    public function sendZuul2Email(Request $request)
    {
        $users = DB::table('users')->whereIn('role_id', [4, 5, 6, 7])->whereNotNull('email')->where('email', '!=', "")->get();
        if ($users->isNotEmpty()) {
            foreach ($users as $user) {
                $emailData['emailTo'] = $user->email;
                $emailData['user'] = $user;
                Zuul2EmailJob::dispatch($emailData);
            }
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getPassByType(Request $request)
    {
        $passType = $request->type ?? "";
        $data = [];
        if ($passType == 'active') {
            $activePass = $this->passUserService->getActivePassUserByUser(auth()->id(), false, null, null, null, 1);
            $data = ActivePassCollection::collection($activePass['result']);
        } else if ($passType == 'scanned') {
            $scannedPass = $this->passUserService->getScannedPassUserByUser(auth()->id(), null, null, null, 1);
            $data = ActivePassCollection::collection($scannedPass['result']);
        } else if ($passType == 'sent') {
            $sentPass = $this->passService->getSentPassesCreatedBy(auth()->id(), false, null, null, null, 1);
            $data = SentPassCollection::collection($sentPass['result']);
        }
        return response()->json(['data' => $data]);
    }

    public function updateUserNotificationSettings(Request $request)
    {
        $data = $request->all();
        if ($request->push_notification) {
            $data['push_notifications'] = $request->push_notification;
        } else if ($request->email_notification) {
            $data['email_notification'] = $request->email_notification;
        } else if ($request->vacation_mode) {
            $data['vacation_mode'] = $request->vacation_mode;
        }

        $user = $this->userService->update($data, auth()->user()->id);

        if ($user) {
            $response = ['status' => true, 'message' => 'Updated Successfully'];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
            return response()->json($response);
        }
    }

    /**
     * store browser web token for push notifications
     * @param Request $request
     * @return JsonResponse
     */
    public function storeBrowserToken(Request $request)
    {
        $user = auth()->user()->update(['web_token' => $request->web_token]);
        if ($user) {
            $response = ['status' => true, 'message' => 'Updated Successfully'];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * @param Request $request
     */
    public function sentTestMessage(Request $request)
    {
        $receiver = $this->userService->find(5)['result'];
        $data = array(
            'description' => "Invited by asd,  Pass for Invitation on Hello for asd ",
            'title' => 'New pass received in family',
            'nid' => $receiver->id,
            'nid_type' => 'parental',
            'link' => url('/nd/' . $receiver->id),
            'info' => 'asd'
        );
        $this->sendPushNotificationToUser($data, $receiver);
    }

    /**
     * @param Request $request
     * @param $notificationId
     * @return Factory|View
     * @throws Exception
     */
    public function notificationDetail(Request $request, $notificationId)
    {
        $notification = $this->notificationService->find($notificationId)['result'];
        $showNotificationDetail = false;
        if (empty($notification)) {
            abort(404, 'Invalid notification');
        }
        $data['showNotificationDetail'] = $showNotificationDetail;
        $data['notification'] = $notification;
        return view('auth.notification-detail', $data);

    }


    /**
     * show notification detail by phone number
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function notificationSubmit(Request $request)
    {
        $phoneNumber = $outputString = preg_replace('/[^0-9]/', '', $request->phone_number);
        $user = $this->userService->findByPhone($phoneNumber)['result'];
        $notification = $this->notificationService->find($request->notificationId)['result'];
        if ((!empty($user) && !empty($notification)) && $user->id == $notification->user_id) {
            $passUser = $this->passUserService->getSinglePassUser($notification->pass_id, $user->id)['result'];
            if (!empty($passUser)) {
                $passDetailCollection = new PassDetailCollection($passUser);
                $data['passUser'] = $passDetailCollection->jsonSerialize();
                $data['showNotificationDetail'] = true;
                return view('auth.notification-detail', $data);
            }
            return redirect()->route('notification-detail', [$notification->id ?? ""])->with('error', 'Pass Does Not Exist');

        } else {
            return redirect()->route('notification-detail', [$notification->id ?? ""])->with('error', 'Phone Number not exist');
        }
    }

    public function qrCode($passId)
    {
        $data['passId'] = $passId;
        return view('frontend.qrcode.index', $data);
    }
}
