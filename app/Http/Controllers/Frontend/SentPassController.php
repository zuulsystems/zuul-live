<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\SentPassCollection;
use App\Http\Resources\SentPassDateCollection;
use App\Services\PassService;
use App\Services\PassUserService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SentPassController extends Controller
{

    public $passService;
    public $passUserService;
    public $userService;
    protected $folderLink = 'frontend.passes.sent-passes.';

    public function __construct(
        PassService     $passService,
        PassUserService $passUserService,
        UserService     $userService

    )
    {
        $this->passService = $passService;
        $this->passUserService = $passUserService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data['userId'] = $request->userId ?? "";
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get sent user passes
     * @param Request $request
     * @param $createdBy
     * @return JsonResponse
     * @throws Exception
     */
    public function sentPassesData(Request $request)
    {
        $userId = auth()->id();
        if (!empty($request->userId)) {
            $userId = $request->userId;
            $user = $this->userService->find($userId)['result'];
            if (!empty($user) && $user->house_id != auth()->user()->house_id) {
                $userId = null;
            }
        }
        $search = $request->search ?? "";
        $filterType = $request->filter_type ?? "";
        $filterSearch = $request->filter_search ?? "";
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passService->getSentPassesCreatedBy($userId, $isExpired, $search, $filterType, $filterSearch, 10);
        return response()->json([
            'data' => SentPassCollection::collection($data['result'])
        ]);
    }

    /**
     * get sent passes all dates for filter records
     * @param Request $request
     * @return JsonResponse
     */
    public function sentPassesDates(Request $request)
    {
        $userId = auth()->id();
        if (!empty($request->userId)) {
            $userId = $request->userId;
            $user = $this->userService->find($userId)['result'];
            if (!empty($user) && $user->house_id != auth()->user()->house_id) {
                $userId = null;
            }
        }
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passService->getSentPassesDatesCreatedBy($userId, $isExpired);
        return response()->json([
            'data' => SentPassDateCollection::collection($data['result'])
        ]);
    }

    /**
     * retract sent pass
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function retractSentPass(Request $request)
    {
        $passId = $request->pass_id;
        $result = $this->passService->retractSentPass($passId);
        if ($result['bool']) {
            $response = ['status' => true, 'message' => 'Pass Retracted Successfully', 'redirect' => route('passes.sent-passes.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again'];
        }
        return response()->json($response);
    }
}
