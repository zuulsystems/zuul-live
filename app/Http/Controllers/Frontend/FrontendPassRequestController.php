<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Http\Requests\FrontendPassRequest;
use App\Services\PassRequestService;
use App\Services\UserService;
use Illuminate\Http\Request;

class FrontendPassRequestController extends Controller
{
    use UserCommon;
    protected $folderLink = 'frontend.anonymous-pass-request.';
    public $passRequestService;
    public $userService;

    public function __construct(
        PassRequestService $passRequestService,
        UserService $userService
    )
    {
        $this->userService = $userService;
        $this->passRequestService = $passRequestService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view($this->folderLink.'index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param FrontendPassRequest $request
     * @return void
     * @throws \Exception
     */
    public function store(FrontendPassRequest $request)
    {
        $fullName  = $request->full_name;
        $phoneNumber = $request->phone_number;
        $residentPhoneNumber = $request->resident_phone_number;
        $comment = $request->comment;
        $names = $this->splitName($fullName);
        //check if user already or not
        $user = $this->userService->findByPhone($phoneNumber)['result'];
        $resident = $this->userService->findByPhone($residentPhoneNumber)['result'];
        if (empty($user)){
            $createUser = $this->userService->create([
                'first_name' => $names['first_name'],
                'last_name' => $names['last_name'],
                'phone_number' => $phoneNumber,
                'dial_code' => "+1",
                'role_id' => 7, //Guest Outsider Daily
            ]);
            if ($createUser['bool'] == false){
                return redirect()->route('anonymous-request-a-pass')->with('error','Something Went Wrong. Please try again');
            }
            $user = $createUser['result'];
        }
        if (empty($user)){
            return redirect()->route('anonymous-request-a-pass')->with('error','Something Went Wrong. Please try again');
        }
        if (empty($resident)){
            return redirect()->route('anonymous-request-a-pass')->with('error','Resident doesn\'t exist in system');
        }
        $passRequestData = [
            'requested_user_id' => $resident->id,
            'send_user_id' => $user->id,
            'name' => $fullName,
            'email' => null,
            'phone' => $user->phone_number,
            'description' => $comment,
        ];
        $passRequest = $this->passRequestService->sendPassRequestAnonymous($passRequestData,$user->id);
        if ($passRequest['bool']) {
            return redirect()->route('anonymous-request-a-pass')->with('success','Request Sent Successfully');
        } else {
            return redirect()->route('anonymous-request-a-pass')->with('error',$passRequest['message']);
        }
    }

}
