<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\AuthLog;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


class PasswordController extends Controller
{
    public $userService;
    protected $folderLink = 'frontend.change-password.';

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view($this->folderLink . 'index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Password reset form
     *
     * @param
     * @return
     */
    public function passwordReset()
    {

        return view('auth.reset');
    }

    /**
     * Password reset form post
     *
     * @param Request $request
     * @return
     */
    public function passwordResetPost(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);

        $password = bcrypt($request->password);

        $userData = array(
            'password' => $password,
            'is_reset_password' => '0',
            'temp_password' => null
        );

        $this->userService->update($userData, auth()->user()->id);

        AuthLog::create([
            'ip_address' => $request->ip(),
            'user_agent' => $request->userAgent(),
            'user_id' => auth()->user()->id
        ]);

        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        $userData = $request->all();

        $userData['password'] = bcrypt($request->newPassword);
        $userData['temp_password'] = $request->newPassword;

        $user = $this->userService->update($userData, $id);
        if ($user) {
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('change-password.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }
}
