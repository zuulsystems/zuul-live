<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ActivePassCollection;
use App\Http\Resources\ActivePassDateCollection;
use App\Http\Resources\PassDetailCollection;
use App\Http\Resources\ReceivedPassCollection;
use App\Services\PassService;
use App\Services\PassUserService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActivePassController extends Controller
{
    protected $folderLink = 'frontend.passes.active-passes.';

    public $passService;
    public $passUserService;
    public $userService;

    public function __construct(
        PassService $passService,
        PassUserService $passUserService,
        UserService $userService

    )
    {
        $this->userService = $userService;
        $this->passService = $passService;
        $this->passUserService = $passUserService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['userId'] = $request->userId ?? "";
        return view($this->folderLink . 'index',$data);
    }

    /**
     * get user received passes
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activePassesData(Request $request)
    {
        $userId = auth()->id();
        if (!empty($request->userId)){
            $userId = $request->userId;
            $user = $this->userService->find($userId)['result'];
            if (!empty($user) &&   $user->house_id != auth()->user()->house_id){
                $userId = null;
            }
        }
        $search = $request->search ?? "";
        $filter_type = $request->filter_type ?? "";
        $filter_search = $request->filter_search ?? "";
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passUserService->getActivePassUserByUser($userId,$isExpired,$search,$filter_type,$filter_search,10);
        return response()->json([
            'data' =>  ActivePassCollection::collection($data['result'])
        ]);
    }

    /**
     * get active passes Dates for filter records
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activePassesDates(Request $request){
        $userId = auth()->id();
        if (!empty($request->userId)){
            $userId = $request->userId;
            $user = $this->userService->find($userId)['result'];
            if (!empty($user) &&   $user->house_id != auth()->user()->house_id){
                $userId = null;
            }
        }
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passUserService->getActivePassUserDatesByUser($userId,$isExpired);
        return response()->json([
            'data' =>  ActivePassDateCollection::collection($data)
        ]);
    }

    /**
     * set pass user vehicle id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function setUserPassVehicle(Request $request){
        // Create Validator
        $validator = Validator::make($request->all(), [
            'pass_user_id' => 'required|exists:pass_users,id',
            'vehicle_id' => 'required|exists:vehicles,id',
        ]);
        if ($validator->fails()) {
            $validator->validate();
        }
        // Update Vehicle ID of User
        $result = $this->passUserService->update(['vehicle_id' => $request->vehicle_id], $request->pass_user_id);

        if($result['bool']){
            $response = ['status' => true, 'message' => 'Save Successfully', 'redirect' => route('passes.active-passes.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }

        return response()->json($response);
    }

    /**
     * get pass detail
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPassDetail(Request $request){
        $userId = auth()->id();
        $passId = $request->passId;
        if (!empty($request->userId)){
            $userId = $request->userId;
        }
        $result = $this->passUserService->getSinglePassUser($passId,$userId);
        return response()->json([
            'data' =>  new PassDetailCollection($result['result'])
        ]);
    }

}
