<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleRequest;
use App\Services\VehicleService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class VehicleController extends Controller
{
    public $vehicleService;
    protected $folderLink = 'frontend.vehicles.';

    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view($this->folderLink . 'index');
    }

    /**
     * get vehicle list
     * @return mixed
     * @throws Exception
     */
    public function vehiclesData(Request $request)
    {
        $result = $this->vehicleService->getByUserId(auth()->id(), 50, $request->search)['result'];
        return response()->json([
            'data' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function store(VehicleRequest $request)
    {
        $vehicleData = $request->all();
        $vehicleData['community_id'] = auth()->user()->community_id;
        $vehicleData['user_id'] = auth()->id();
        $vehicle = $this->vehicleService->create($vehicleData);
        if ($vehicle['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('vehicles.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view($this->folderLink . 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $vehicle = $this->vehicleService->find($id);
        checkErrors($vehicle);
        $data['vehicle'] = $vehicle['result'];
        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     * @throws Exception
     */
    public function update(VehicleRequest $request, $id)
    {
        $vehicleData = $request->all();
        $vehicle = $this->vehicleService->update($vehicleData, $id);

        if ($vehicle['bool']) {
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('vehicles.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $vehicle = $this->vehicleService->delete($id);
        if ($vehicle['bool']) {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('vehicles.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong', 'redirect' => route('vehicles.index')];
        }
        return response()->json($response);
    }
}
