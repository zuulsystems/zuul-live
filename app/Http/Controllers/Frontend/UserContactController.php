<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserContactRequest;
use App\Http\Requests\EditUserContactRequest;
use App\Models\Contact;
use App\Models\ContactCollection;
use App\Models\ContactGroup;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use App\Models\UserContact;
use App\Services\ContactGroupService;
use App\Services\EventService;
use App\Services\PassRequestService;
use App\Services\UserContactService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserContactController extends Controller
{
    public $userContactService;
    public $contactGroupService;
    public $passRequestService;
    public $eventService;
    protected $folderLink = 'frontend.user-contacts.';

    public function __construct(
        UserContactService  $userContactService,
        ContactGroupService $contactGroupService,
        PassRequestService  $passRequestService,
        EventService        $eventService
    )
    {
        $this->userContactService = $userContactService;
        $this->contactGroupService = $contactGroupService;
        $this->passRequestService = $passRequestService;
        $this->eventService = $eventService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     * @throws Exception
     */
    public function index(Request $request)
    {
        $data['type'] = $request->type ?? '';
        $data['contactGroups'] = auth()->user()->groups;
        $data['events'] = $this->eventService->all()['result'];
        return view($this->folderLink . 'index', $data);
    }

    /**
     * get contact list
     * @param Request $request
     * @return mixed
     */
    public function userContactData(Request $request)
    {
        $is_favourite = ($request->has('type') && $request->type == 'favourites') ? true : false;
        $result = $this->userContactService->getContactList(auth()->id(), 50, $request->search, $is_favourite);
        return response()->json([
            'data' => $result
        ]);
    }

    /**
     * add contact in group
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addContactInGroup(Request $request)
    {

        $contactGroups = $this->contactGroupService->addContactInGroup($request->contact_id, $request->group_id);
        if ($contactGroups) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('contacts.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['countries'] = Country::all();
        return view($this->folderLink . 'create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(AddUserContactRequest $request)
    {
        $userContact = $this->userContactService->createOrUpdateUserContact($request->all());
        if ($userContact) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('contacts.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function edit($id)
    {
        $data['countries'] = Country::all();
        $userContact = $this->userContactService->find($id);
        checkErrors($userContact);
        $data['userContact'] = $userContact['result'];

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $data['userContact']->contact->dial_code)->first();

        $data['phoneFormat'] = '999-999-9999';

        if (!empty($countryPhoneFormat)) {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        return view($this->folderLink . 'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     */
    //public function update(EditUserContactRequest $request, $id)
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        $userContact = $this->userContactService->delete($id);
        if ($userContact['bool']) {
            $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('contacts.index')];
        } else {
            $response = ['status' => false, 'message' => 'Somethign went wrong.', 'redirect' => route('contacts.index')];
        }
        return response()->json($response);
    }

    public function addOrRemoveFavouriteContact(Request $request)
    {
        $data['is_favourite'] = $request->is_favourite == '0' ? '1' : '0';
        $contactGroups = $this->userContactService->update($data, $request->id);
        $route = route('contacts.index');
        if ($request->has('type') && $request->type == "favourites") {
            $route = route('contacts.index', ['type' => 'favourites']);
        }
        if ($contactGroups) {
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => $route];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    //to add or remove contact in favourites


    public function update(Request $request, $id)
    {
        $userContactData = $request->all();
        $userContactData['phone_number'] = trim(str_replace("-", "", $userContactData['phone_number']));

        $validation = [
            'contact_name' => 'required',
            'phone_number' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',
            'phone_number.required' => 'Please select phone number',
        ];

        $validator = Validator::make($userContactData, $validation, $messages);
        $validator->after(function ($validator) use ($userContactData, $id) {

            $dialCode = str_replace("+", "", $userContactData['dial_code']);

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

            $phoneNumberDigits = 10;

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            if (isset($userContactData['phone_number']) && preg_match_all("/[0-9]/", $userContactData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }

            if ($this->userContactService->getSingleUserContactForUpdate($id, auth()->id(), $userContactData['phone_number'])) {
                $validator->errors()->add('phone_number', 'Phone number already exist');
            }
        });

        if ($validator->fails()) {
            $validator->validate();
        }

        $userContact = $this->userContactService->updateUserContact($userContactData, $id);
        if ($userContact) {
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('contacts.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * send pass request from contacts
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function sendPassRequestFromContact(Request $request)
    {
        $userContactId = $request->user_contact_id;
        //Check user has permission to send a pass
        $userContact = $this->userContactService->getUserContactPermission($userContactId);
        if (empty($userContact['result'])) {
            return response()->json(['status' => false, 'message' => 'The person you are requesting a pass from, is not approved to issue you one']);
        }

        $passRequestData = [
            'requested_user_id' => $userContact['result']->contact->user->id ?? "",
            'description' => $request->description,
        ];
        $passRequest = $this->passRequestService->sendPassRequest($passRequestData);
        if ($passRequest['bool']) {
            $response = ['status' => true, 'message' => 'Request Sent Successfully', 'redirect' => route('contacts.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again'];
        }
        return response()->json($response);
    }

    public function getPhoneNumberFormatByDialCode(Request $request)
    {
        $dialCode = str_replace("+", "", $request->dial_code);
        $data['countryPhoneFormat'] = CountryPhoneFormat::select("format")->where('dial_code', $dialCode)->first();
        return response()->json($data);
    }
}
