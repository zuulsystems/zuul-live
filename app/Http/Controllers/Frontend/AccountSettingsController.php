<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CountryPhoneFormat;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\AccountSettingsRequest;
use App\Services\UserService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use App\Models\Country;
use Intervention\Image\Facades\Image;
use App\Services\HouseService;



class AccountSettingsController extends Controller
{
    protected $folderLink = 'frontend.accountsettings.';
    public $userService;
    public $houseService;

    public function __construct(UserService $userService, HouseService $houseService)
    {
        $this->userService = $userService;
        $this->houseService = $houseService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["countries"] = Country::all();
        $countryPhoneFormat = CountryPhoneFormat::where('dial_code',auth()->user()->dial_code)->first();
        $data['phoneFormat'] = "999-999-9999";
        if(!empty($countryPhoneFormat))
        {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        return view($this->folderLink.'index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Have to update images
    public function update(AccountSettingsRequest $request, $id)
    {
        //
        $userData = Arr::except($request->all(), ['profile_image']);
        //upload profile picture
        if ($request->hasFile('profile_image'))
        {

            $image = $request->file('profile_image');
            $userData['profile_image'] = 'profile_'.time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('images/profile_images');
            $img = Image::make($image->getRealPath());
            $img->save($destinationPath.'/'.$userData['profile_image'], 10);
        }

        //upload license image to s3 bucket
        if ($request->hasFile('license_image'))
        {
            $image = $request->file('license_image');
            $licenseImage = Image::make($image->getRealPath());
            $licenseImagea =  $licenseImage->stream(null,10);
            $licenceName = md5(uniqid()) . '.' . 'png';
            $filePath =  "dynamic/lic_images/".$licenceName;
            $path = Storage::disk('s3')->put($filePath, $licenseImagea);
            if (!$path){
                return  response()->json(['status' => false, 'message' => 'Something went wrong.Please try again.']);
            }
            $userData['license_image'] = $filePath;
        } else {
            $userData['license_image'] = $request->old_license_image;
        }
        if (!auth()->user()->hasPermission('is_license_locked')){
            auth()->user()->assignPermission('is_license_locked');
        }
        if (auth()->user()->hasPermission('is_license_locked')){
            unset( $userData['license_image']);
        }
        unset( $userData['phone_number']);

        $user = $this->userService->update($userData,$id);

        $houseId = $this->userService->find($id)['result']->house_id;

        $houseCompletedProfileStatus = "no";
        $houseResidents = User::where('house_id', $houseId)->whereIn('role_id', [4, 5])->get();
        foreach ($houseResidents as $houseResident)
        {
            if(!empty($houseResident->license_image))
            {
                $houseCompletedProfileStatus = "yes";
            }
        }

        $this->houseService->update(['completed_profile'=>$houseCompletedProfileStatus], $houseId);

        if ($user['bool']) {
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('account-setting.index')];
        }
        else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
