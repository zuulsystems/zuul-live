<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Country;
use App\Models\CountryPhoneFormat;
use Illuminate\Http\Request;
use App\Services\UserContactService;
use App\Services\UserService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PreApproveController extends Controller
{
    private $contactSrvc;
    private $userSrvc;


    public function __construct(UserContactService $userContactService, UserService $userService)
    {
        $this->contactSrvc = $userContactService;
        $this->userSrvc = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('frontend.pre-approved.index');
    }

    public function preApprovedData(Request $request)
    {

        $user_id = auth()->user()->id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $is_favourite = ($request->has('is_favourite') && $request->is_favourite == '1') ? true : false;
        $is_dnc = true;
        $data = $this->contactSrvc->getContactList($user_id, 500, $search, $is_favourite, $is_dnc);

        if (count($data) > 0) {
            $data = $data->toArray();
        }

        if ($is_dnc) {
            $household_members = $this->userSrvc->getHouseHoldMemberDetail(auth()->user()->house_id, null, $search);

            $familyMemberIds = $household_members->pluck('id');
            $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds);

            if (count($familyMemberSharedContacts) > 0) {
                $familyMemberSharedContacts = $familyMemberSharedContacts->toArray();
            }

            if (count($familyMemberSharedContacts) > 0) {
                $data = array_merge($familyMemberSharedContacts, $data);
            }

        }

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['countries'] = Country::all();
        return view('frontend.pre-approved.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $contactData = $request->all();


        $isFromPreApproved = true;


        $validation = [
            'contact_name' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',

        ];


        $validator = Validator::make($contactData, $validation, $messages);

        $validator->after(function ($validator) use ($contactData) {

            $dialCode = str_replace("+", "", $contactData['dial_code']);

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

            $phoneNumberDigits = 10;

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }

            if (!empty($contactData['phone_number']) && preg_match_all("/[0-9]/", $contactData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }

        if ($isFromPreApproved) {
            $contactData['is_dnc'] = 1;
        }
        $contactData['is_from_pre_approved'] = $isFromPreApproved;

        $contactData['phone_number'] = str_replace("(", "", $request->phone_number);
        $contactData['phone_number'] = str_replace(")", "", $contactData['phone_number']);
        $contactData['phone_number'] = str_replace("-", "", $contactData['phone_number']);
        $contactData['phone_number'] = str_replace("_", "", $contactData['phone_number']);

        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData);
        if ($userContact['bool']) {
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('pre-approved.index')];

        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $data['countries'] = Country::all();
        $data['userContact'] = $this->contactSrvc->find($id)['result'];

        $dialCode = str_replace("+", "", $data['userContact']->contact->dial_code);

        $data['phoneFormat'] = "999-999-9999";

        $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

        if (!empty($countryPhoneFormat)) {
            $data['phoneFormat'] = $countryPhoneFormat->format;
        }

        return view('frontend.pre-approved.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $contactData = $request->all();

        $validation = [
            'contact_name' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',
        ];


        $validator = Validator::make($contactData, $validation, $messages);

        $validator->after(function ($validator) use ($contactData) {

            $dialCode = str_replace("+", "", $contactData['dial_code']);

            $countryPhoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

            $phoneNumberDigits = 10;

            if (!empty($countryPhoneFormat)) {
                $phoneNumberDigits = $countryPhoneFormat->digit;
            }


            if (!empty($contactData['phone_number']) && preg_match_all("/[0-9]/", $contactData['phone_number']) < $phoneNumberDigits) {
                $validator->errors()->add('phone_number', 'Phone number must be at least ' . $phoneNumberDigits . ' digit long');
            }

        });

        if ($validator->fails()) {
            $validator->validate();
        }

        if (empty($request->phone_number)) {
            if ($request->dial_code != "504") {
                $contactData['phone_number'] = '1111111111';
            } else {
                $contactData['phone_number'] = '11111111';
            }
        }


        $contactData['phone_number'] = str_replace("(", "", $request->phone_number);
        $contactData['phone_number'] = str_replace(")", "", $contactData['phone_number']);
        $contactData['phone_number'] = str_replace("-", "", $contactData['phone_number']);
        $contactData['phone_number'] = str_replace("_", "", $contactData['phone_number']);

        $userContact = $this->contactSrvc->find($id)['result'];
        $userContact->update(['contact_name' => $request->contact_name, 'is_dnc_shared' => $request->is_dnc_shared]);

        $contact = Contact::find($userContact->contact_id);
        $contact = $contact->update(['phone_number' => $contactData['phone_number'], 'dial_code' => $request->dial_code]);

        $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('pre-approved.index')];
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $userContact = $this->contactSrvc->find($id)['result']->delete();
        return response()->json(['status' => true, 'message' => 'Deleted Successfully', 'redirect' => route('pre-approved.index')]);

    }
}
