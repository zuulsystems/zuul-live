<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Services\EventService;
use App\Services\PassService;
use App\Services\UserContactService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PassController extends Controller
{
    protected $folderLink = 'frontend.passes.';
    public $eventService;
    public $userContactService;
    public $passService;
    public function __construct(
        EventService $eventService,
        UserContactService $userContactService,
        PassService $passService
    )
    {
        $this->eventService = $eventService;
        $this->userContactService = $userContactService;
        $this->passService = $passService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view($this->folderLink.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function create()
    {
        $data['events'] = $this->eventService->all()['result'];
        $data['userContacts'] =  $this->userContactService->getAllByUserId(auth()->id());
        return  view($this->folderLink.'create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $validation = [
            'pass_date' => 'required',
            'lat' => 'required',
        ];
        $messages = [
            'pass_date.required' => 'Please select date',
            'lat.required' => 'Please select location',
        ];
        $validator = Validator::make($request->all(), $validation, $messages);
        $validator->after(function ($validator) use($request){
            if (empty($request->lng)) {
                $validator->errors()->add(
                    'lat', 'Please select location'
                );
            } if($request->pass_type != 'self' && empty($request->selected_contacts)){
                $validator->errors()->add(
                    'selected_contact_message', 'Please Select Contacts'
                );
            }
        });
        if ($validator->fails()) {
            $validator->validate();
        }
        $passData = $request->all();
        if($request->pass_type == 'self'){
            $passData['selected_contacts'][] = auth()->id();
        }
        $passData['pass_date']  = date('Y-m-d H:i:s',strtotime($passData['pass_date']));
        $pass = $this->passService->createPass($passData);
        if ($pass['bool']){
            return response()->json(['status' => true, 'message' => 'Pass sent Successfully','redirect' => $request->redirect ]);
        }
        return response()->json(['status' => false, 'message' => $pass['message']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function edit($id)
    {
        $data['pass'] = $this->passService->find($id)['result'];
        if (empty( $data['pass'])){
            abort(404);
        }
        $data['events'] = $this->eventService->all()['result'];
        $data['userContacts'] =  $this->userContactService->getAllByUserId(auth()->id());
        return  view($this->folderLink.'edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        $pass =  $this->passService->editPass($request->all(),$id);
        if ($pass['bool']){
            return response()->json(['status' => true, 'message' => 'Pass Updated Successfully','redirect' => route('passes.sent-passes.index') ]);
        }
        return response()->json(['status' => false, 'message' => $pass['message']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
