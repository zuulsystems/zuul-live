<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddContactGroup;
use App\Services\ContactGroupService;
use Illuminate\Http\Request;

class ContactGroupController extends Controller
{
    protected $folderLink = 'frontend.contact-groups.';
    public $contactGroupService;

    public function __construct(ContactGroupService $contactGroupService)
    {
        $this->contactGroupService = $contactGroupService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->folderLink.'index');
    }

    /**
     * get contact groups list
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactGroupData(Request $request){
        $result =  $this->contactGroupService->getContactGroupList(auth()->id(),5,$request->search);
        return response()->json([
            'data' =>  $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->folderLink.'create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddContactGroup $request)
    {
        $userContact =  $this->contactGroupService->createContactGroup($request->all());
        if ($userContact){
            $response = ['status' => true, 'message' => 'Added Successfully', 'redirect' => route('contact-groups.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $userContact = $this->contactGroupService->find($id);
        checkErrors($userContact);
        $data['userContact'] = $userContact['result'];
        return  view($this->folderLink.'edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddContactGroup $request, $id)
    {
        //
        $userContact =  $this->contactGroupService->update($request->all(),$id);
        if ($userContact['bool']){
            $response = ['status' => true, 'message' => 'Updated Successfully', 'redirect' => route('contact-groups.index')];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $contactGroup = $this->contactGroupService->find($id)['result'];
        $contactGroup->userContacts()->detach();
        $contactGroup->delete();
        $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('contact-groups.index')];
        return response()->json($response);
    }

    //to delete group contact
    public function groupContactDelete(Request $request)
    {
        $contactGroup = $this->contactGroupService->groupContactDelete($request->contactGroupId, $request->contactId);
        $response = ['status' => true, 'message' => 'Delete Successfully', 'redirect' => route('contact-groups.index')];
        return response()->json($response);
    }
}
