<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ActivePassCollection;
use App\Http\Resources\ActivePassDateCollection;
use App\Http\Resources\PassDetailCollection;
use App\Services\PassService;
use App\Services\PassUserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ActivePassController extends Controller
{
    public $passService;
    public $passUserService;

    public function __construct(PassService $passService, PassUserService $passUserService)
    {
        $this->passService = $passService;
        $this->passUserService = $passUserService;
    }

    /**
     * -------------------------------------------
     * Get Active Pass Users By User
     * -------------------------------------------
     *
     * This method retrieves the active pass users associated with a specific user based on the provided parameters.
     *
     * @param int $userId The ID of the user.
     * @param bool $isExpired Flag indicating whether to retrieve expired pass users or not.
     * @param string $search The search query to filter pass users by.
     * @param string $filterType The type of filter to apply to pass users.
     * @param string $filterSearch The filter search query to further narrow down pass users.
     * @return JsonResponse The JSON response with the result of the active pass users.
     * @throws Exception If an error occurs during the process of retrieving the active pass users.
     */
    public function activePassesData(Request $request, $userId): JsonResponse
    {
        $search = $request->search ?? "";
        $filterType = $request->filter_type ?? "";
        $filterSearch = $request->filter_search ?? "";
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; // 0: active, 1: expired
        $data = $this->passUserService->getActivePassUserByUser($userId, $isExpired, $search, $filterType, $filterSearch, 10);
        return APIResponse::success(['list' => ActivePassCollection::collection($data['result'])], null);
    }

    /**
     * -------------------------------------------
     * Get Active Passes Count
     * -------------------------------------------
     *
     * This method retrieves the total count of active passes associated with a specific user.
     * @param int $userId The ID of the user.
     * @return JsonResponse The JSON response with the result of the total count of active passes.
     * @throws Exception If an error occurs during the process of retrieving the active passes count.
     */
    public function activePassesCount($userId): JsonResponse
    {
        $isExpired = 0;
        $data = $this->passUserService->getActivePassUserByUserCount($userId, $isExpired);
        return APIResponse::success(['total_active_passes' => $data['result']], null);
    }

    /**
     * -------------------------------------------
     * Get Active Passes Dates by User
     * -------------------------------------------
     *
     * This method retrieves the dates of active passes associated with a specific user based on the provided parameters.
     * @param Request $request The request object containing additional parameters (e.g., expired flag).
     * @param int $userId The ID of the user.
     * @return JsonResponse The JSON response with the result of the active pass dates.
     * @throws Exception If an error occurs during the process of retrieving the active pass dates.
     */
    public function activePassesDates(Request $request, $userId): JsonResponse
    {
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; // 0: active, 1: expired
        $data = $this->passUserService->getActivePassUserDatesByUser($userId, $isExpired);
        return APIResponse::success(['list' => ActivePassDateCollection::collection($data)], null);
    }

    /**
     * -------------------------------------------
     * Get Pass Detail
     * -------------------------------------------
     *
     * This method retrieves the details of a specific pass associated with a user.
     * @param int $passUserId The ID of the pass user.
     * @return JsonResponse The JSON response with the result of the pass detail.
     * @throws Exception If an error occurs during the process of retrieving the pass detail.
     */
    public function getPassDetail($passUserId): JsonResponse
    {
        $result = $this->passUserService->getSinglePassUser($passUserId, auth()->id());
        if (empty($result['result'])) {
            return APIResponse::error('No Record Found');
        }
        return APIResponse::success(new PassDetailCollection($result['result']), null);
    }

    /**
     * -------------------------------------------
     * Get Pass Detail by Family Member
     * -------------------------------------------
     *
     * This method retrieves the details of a specific pass associated with a family member.
     * @param int $passUserId The ID of the pass user.
     * @param int $userId The ID of the family member user.
     * @return JsonResponse The JSON response with the result of the pass detail.
     * @throws Exception If an error occurs during the process of retrieving the pass detail.
     */
    public function getPassDetailByFamilyMember($passUserId, $userId): JsonResponse
    {
        $result = $this->passUserService->getFamilyPassUser($passUserId, $userId);

        if (empty($result['result'])) {
            return APIResponse::error('No Record Found', ['passUserId' => $passUserId, 'userId' => $userId]);
        }
        return APIResponse::success(new PassDetailCollection($result['result']), null);
    }
}
