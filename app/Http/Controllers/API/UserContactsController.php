<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Helpers\PushNotificationTrait;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditUserContactRequest;
use App\Http\Resources\UserCanSendPassCollection;
use App\Http\Resources\UserCanSendPassFormattedCollection;
use App\Http\Resources\UserContactCollection;
use App\Models\Community;
use App\Models\Contact;
use App\Models\ContactCollection;
use App\Models\CountryPhoneFormat;
use App\Models\User;
use App\Models\UserContact;
use App\Services\UserContactService;
use App\Services\UserService;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserContactsController extends Controller
{
    use UserCommon;
    use PushNotificationTrait;

    /**
     * @var UserContactService
     */
    private $contactSrvc;
    private $userSrvc;


    /**
     * UserContactsController constructor.
     *
     * @param UserContactService $userContactService
     * @param UserService $userService
     */
    public function __construct(UserContactService $userContactService, UserService $userService)
    {
        $this->contactSrvc = $userContactService;
        $this->userSrvc = $userService;
    }

    /**
     * Show User contacts
     * @return mixed
     */
    public function index(Request $request)
    {

        $user_id = Auth::user()->id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $is_favourite = ($request->has('is_favourite') && $request->is_favourite == '1') ? true : false;
        $is_dnc = ($request->has('is_dnc') && $request->is_dnc == '1') ? true : false;
        $data = $this->contactSrvc->getContactList($user_id, 30, $search, $is_favourite, $is_dnc)->toArray();

        if ($is_dnc) {
            // change limit from null to 30
            $household_members = $this->userSrvc->getHouseHoldMemberDetail(auth()->user()->house_id, 30, $search);

            $familyMemberIds = $household_members->pluck('id');
            $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds)->toArray();


            $data = array_merge($familyMemberSharedContacts, $data);
        }

        return APIResponse::success(['list' => $data], null);
    }

    /**
     * Show User contacts count
     * @return mixed
     */
    public function userContactsCount(Request $request)
    {
        $user_id = Auth::user()->id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $is_favourite = ($request->has('is_favourite') && $request->is_favourite == '1') ? true : false;
        $is_dnc = ($request->has('is_dnc') && $request->is_dnc == '1') ? true : false;

        $data = $this->contactSrvc->getContactListCount($user_id, null, $search, $is_favourite, $is_dnc);

        if ($is_dnc) {
            $household_members = $this->userSrvc->getHouseHoldMemberDetail(auth()->user()->house_id, null, $search);

            $familyMemberIds = $household_members->pluck('id');
            $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds)->toArray();

            $data = array_merge($familyMemberSharedContacts, $data);
        }

        return APIResponse::success(['user_contacts_count' => $data], null);
    }

    /**
     * Show User contacts Dnc count
     * @return mixed
     */
    public function userContactsDncCount(Request $request)
    {
        $user_id = Auth::user()->id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $is_favourite = ($request->has('is_favourite') && $request->is_favourite == '1') ? true : false;
        $is_dnc = ($request->has('is_dnc') && $request->is_dnc == '1') ? true : false;

        $data = $this->contactSrvc->getContactListDncCount($user_id, null, $search, $is_favourite, true);

        return APIResponse::success(['user_contacts_count' => $data], null);
    }


    /**
     * Show pre approved count
     * @return mixed
     */
    public function preApprovedCount(Request $request)
    {

        $user_id = Auth::user()->id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $is_favourite = ($request->has('is_favourite') && $request->is_favourite == '1') ? true : false;
        $is_dnc = ($request->has('is_dnc') && $request->is_dnc == '1') ? true : false;
        $data = $this->contactSrvc->getContactList($user_id, 30, $search, $is_favourite, $is_dnc)->toArray();

        if ($is_dnc) {
            $household_members = $this->userSrvc->getHouseHoldMemberDetail(auth()->user()->house_id, null, $search);

            $familyMemberIds = $household_members->pluck('id');
            $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds)->toArray();

            $data = array_merge($familyMemberSharedContacts, $data);
        }


        return APIResponse::success(['pre_approved_count' => count($data)], null);
    }

    /**
     * Show User Contacts
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function show($id)
    {
        $userContact = $this->contactSrvc->find($id);
        if ($userContact['result']) {
            return APIResponse::success(new UserContactCollection($userContact['result']), null);
        } else {
            return APIResponse::error('No Contact Found');
        }
    }

    /**
     * Store User Contacts
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $contactData = $request->all();
        $dial_code = Str::start($request->dial_code, '+');
        $check_dial_code = 0;

        if($dial_code == "+1"){
            $check_dial_code = 10;
        }elseif($dial_code == "+504"){
            $check_dial_code = 8;
        }


        $isFromPreApproved = isset($contactData['is_from_pre_approved']) ? $contactData['is_from_pre_approved'] : false;

        if (isset($request->phone_number)) {
            $contactData['phone_number'] = $request->phone_number;
        }

        $validation = [
            'contact_name' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',

        ];

        if (!$isFromPreApproved) {
            $validation['phone_number'] = 'required';
            $messages['phone_number.required'] = 'Please select phone number';
        }
        Log::emergency($contactData['phone_number']);
        $validator = Validator::make($contactData, $validation, $messages);
        $validator->after(function ($validator) use ($contactData, $isFromPreApproved, $check_dial_code) {
            // modified condition
            if ($check_dial_code != 0 && (strlen($contactData['phone_number']) !== $check_dial_code) && !$isFromPreApproved) {
                $validator->errors()->add('phone_number', 'Phone number should be ' . strval($check_dial_code) . ' digits long');
            } else if ($isFromPreApproved) {
                return false;
            } else if (auth()->user()->contacts()->where('phone_number', $contactData['phone_number'])->count() > 0) {
                $validator->errors()->add('phone_number', 'Phone number already exist');
            }
        });

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        if ($isFromPreApproved) {
            $contactData['is_dnc'] = 1;
        }

        $usr_exist = User::where('phone_number', $contactData['phone_number'])->whereIn('role_id', [4, 5])->first();
        $usr_id = null;

        if (isset($usr_exist)) {
            $usr_id = $usr_exist->id;
        }

        $contactData['is_from_pre_approved'] = $isFromPreApproved;

        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData);

        $userCont = new UserContactCollection($userContact['result']);
        $arr = collect($userCont)->toArray();

        $arr = $arr + ['user_id' => $usr_id];

        if ($userContact['bool']) {
            return APIResponse::success($arr, 'contact created successfully');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    public function addUserContactByGuard2(Request $request)
    {

        /**
         * ***** REQUEST BODY *****
         * string  contact_name
         * string  dail_code
         * boolean is_private_household
         * string  phone_number
         * int     resident_id
         */

        $contactData = $request->all();


        $contactData['is_dnc_shared'] = isset($contactData['is_private_household']) ? $contactData['is_private_household'] : false;
        $isFromPreApproved = true;

        if (isset($request->phone_number)) {
            $contactData['phone_number'] = $request->phone_number;
        }

        $validation = [
            'contact_name' => 'required',
        ];


        $messages = [
            'contact_name.required' => 'Please enter name',
        ];

        if (!$isFromPreApproved) {
            $validation['phone_number'] = 'required';
            $messages['phone_number.required'] = 'Please select phone number';
        }

        $validator = Validator::make($contactData, $validation, $messages);
        $validator->after(function ($validator) use ($contactData, $isFromPreApproved) {

            if (isset($userContactData['phone_number']) && preg_match_all("/[0-9]/", $contactData['phone_number']) < 10) {
                $validator->errors()->add('phone_number', 'Phone number must be at least 10 digit long');
            } else if ($isFromPreApproved) {
                return false;
            } else if (auth()->user()->contacts()->where('phone_number', $contactData['phone_number'])->count() > 0) {
                $validator->errors()->add('phone_number', 'Phone number already exist');
            }


        });

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $contactData['is_dnc'] = 1;
        $contactData['is_from_pre_approved'] = $isFromPreApproved;
        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData, $contactData['resident_id']);
        if ($userContact['bool']) {
            return APIResponse::success(new UserContactCollection($userContact['result']), 'contact created successfully');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }


    public function addUserContactByGuard(Request $request)
    {

        /**
         * ***** REQUEST BODY *****
         * string  contact_name
         * string  dail_code
         * boolean is_private_household
         * string  phone_number
         * int     resident_id
         */

        $contactData = $request->all();

        $contactData['is_dnc_shared'] = isset($contactData['is_private_household']) ? $contactData['is_private_household'] : false;
        $isFromPreApproved = true;

        if (isset($request->phone_number)) {
            $contactData['phone_number'] = $request->phone_number;
        }

        $validation = [
            'contact_name' => 'required',
        ];


        $messages = [
            'contact_name.required' => 'Please enter name',
        ];

        if (!$isFromPreApproved) {
            $validation['phone_number'] = 'required';
            $messages['phone_number.required'] = 'Please select phone number';
        }

        $validator = Validator::make($contactData, $validation, $messages);
        $validator->after(function ($validator) use ($contactData, $isFromPreApproved) {

            if (isset($userContactData['phone_number']) && preg_match_all("/[0-9]/", $contactData['phone_number']) < 10) {
                $validator->errors()->add('phone_number', 'Phone number must be at least 10 digit long');
            } else if ($isFromPreApproved) {
                return false;
            } else if (auth()->user()->contacts()->where('phone_number', $contactData['phone_number'])->count() > 0) {
                $validator->errors()->add('phone_number', 'Phone number already exist');
            }


        });

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $contactData['is_dnc'] = 1;
        $contactData['is_from_pre_approved'] = $isFromPreApproved;
        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData, $contactData['resident_id']);
        if ($userContact['bool']) {
            return APIResponse::success(new UserContactCollection($userContact['result']), 'contact created successfully');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    /**
     * import bulk user contact
     * @param Request $request
     * @return JsonResponse
     */
    public function bulkImportUserContact(Request $request)
    {
        // updated code

        $id = Auth::user()->id;
        $data = $request->input('data');
        $user = $this->userSrvc->find($id);
        if (empty($user['result'])) {
            return APIResponse::error('Error: User Does not exist');
        }

        $result = [];
        foreach ($data as $key => $datum) {
            $clean_number = preg_replace('/[^0-9]/', '', $datum['phone_number']);
            $dialCode = isset($datum['dial_code']) ? Str::start($datum['dial_code'], '+') : '+1';

            if ((strlen($clean_number) === 10 && $dialCode === '+1') || (strlen($clean_number) === 8 && $dialCode === '+504')) {
                // Code to execute when either condition is true
                $contact = [];
                $phone_number = $clean_number;
                $contact_name = $datum['display_name'];
                $contact['contact_name'] = $datum['display_name'];
                $contact['email'] = isset($datum['email']) ? $datum['email'] : null;
                $contact['phone_number'] = $clean_number;
                $contact['dial_code'] = $dialCode;

                //array_push($result, $contact);
                if (!empty($phone_number) && !empty($contact_name)) {
                    $userContact = $this->contactSrvc->createOrUpdateUserContact($contact);
                    if ($userContact['bool']) {
                        $contact = new UserContactCollection($userContact['result']);
                        array_push($result, $contact);
                    }
                }

            }


        }
        return APIResponse::success(['list' => $result], 'Selected contact(s) imported successfully!');
    }

    /**
     * Delete Bulk Contacts
     *
     * @param $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        $this->removeContactFromGroupByContact($id);
        $userContact = $this->contactSrvc->delete($id);
        if ($userContact['bool']) {
            return APIResponse::success([], 'Contact deleted');
        } else {
            return APIResponse::error('Something went wrong. Please try again.');
        }
    }

    public function removeContactFromGroupByContact($userContactId)
    {
        ContactCollection::where('user_contact_id', $userContactId)->delete(); #todo need to review for SoftDelete
    }

    /**
     * Delete Bulk Contacts
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroyMany(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->removeContactFromGroupByContact($contact);
                $this->contactSrvc->delete($contact);
            }
        }
        return APIResponse::success([], 'Contact(s) deleted');
    }

    public function deleteUserContactsByGuard(Request $request)
    {
        $contact_id = $request->contact_id ?? null;
        $resident_id = $request->resident_id ?? null;
        if (($contact_id != null) && ($resident_id != null)) {

            $this->removeContactFromGroupByContact($contact_id);
            $contact = $this->contactSrvc->deleteByResidentId($contact_id, $resident_id);

            return APIResponse::success([], 'Contact(s) deleted');

        }


    }

    /**
     * add contact in favorite Bulk
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function addContactInFavorite(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->contactSrvc->update(['is_favourite' => '1'], $contact);
            }
        }
        return APIResponse::success([], 'Contact(s) updated to favorites');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, $id)
    {

        $userContactData = $request->all();
        $userContactData['phone_number'] = $userContactData['phone_number'];
        $dial_code = Str::start($request->dial_code, '+');
        $check_dial_code = $dial_code == "+1" ? 10 : 8;

        $validation = [
            'contact_name' => 'required',
            'phone_number' => 'required',
        ];

        $messages = [
            'contact_name.required' => 'Please enter name',
            'phone_number.required' => 'Please select phone number',
        ];

        $validator = Validator::make($userContactData, $validation, $messages);
        $validator->after(function ($validator) use ($userContactData, $id, $check_dial_code) {
            // modified condition
            if ((strlen($userContactData['phone_number']) !== $check_dial_code)) {
                $validator->errors()->add('phone_number', 'Phone number should be ' . strval($check_dial_code) . ' digits long');

            } else if ($this->contactSrvc->getSingleUserContactForUpdate($id, auth()->id(), $userContactData['phone_number'])) {
                $validator->errors()->add('phone_number', 'Phone number already exist');
            }
        });

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $userContact = $this->contactSrvc->updateUserContact($userContactData, $id);
        if ($userContact['bool']) {
            return APIResponse::success([], 'contact updated successfully');
        } else {
            return APIResponse::error('Something went wrong. Please try again.');
        }
    }

    /**
     * add contact in favorite Bulk
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function addContactInDnc(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->contactSrvc->update(['is_dnc' => '1'], $contact);
            }
        }
        return APIResponse::success([], 'Contact(s) updated to Pre Approved');
    }

    public function addContactInSharedDnc(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->contactSrvc->update(['is_dnc_shared' => '1'], $contact);
            }
        }
        return APIResponse::success([], 'Contact(s) updated to Pre Approved');
    }


    /**
     * remove contact in favorite Bulk
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function removeContactInFavorite(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->contactSrvc->update(['is_favourite' => '0'], $contact);
            }
        }
        return APIResponse::success([], 'Contact(s) removed to favorites');

    }

    /**
     * remove contact in favorite Bulk
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function removeContactInDnc(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->contactSrvc->update(['is_dnc' => '0'], $contact);
            }
        }
        return APIResponse::success([], 'Contact(s) removed to favorites');
    }


    public function getContactInDncByResidentId2(Request $request, $id)
    {

        $data = $this->contactSrvc->getContactList($id, null, '', false, true);

        $household_members = $this->userSrvc->getHouseHoldMemberDetail(User::find($id)->house_id, null, null);
        $familyMemberIds = $household_members->pluck('id');

        $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds);

        $data = ($data->merge($familyMemberSharedContacts)); //array_unique
        $data = datatables()->of($data)
            ->addColumn('links', function ($model) {
                $links = '<div class="dropdown">
                         <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>
                         <div class="dropdown-menu">';

                if (auth()->user()->hasRole('guard_admin')) {

                    $links .= '<a class="dropdown-item guard-permission-model-list" data-id="' . $model["id"] . '"  href="javascript:void(0)"><i class="fa fa-lock" aria-hidden="true"></i> Pre-approved guest list</a>';
                }

                if (auth()->user()->hasRole('guard_admin') || auth()->user()->hasRole('sub_admin')) {

                    $links .= '<a class="dropdown-item" href="' . route('admin.residential.approve-list.edit', [$model["id"]]) . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>';

                }
                if (auth()->user()->hasRole('guard_admin')) {
                    $links .= '<form action="' . route('admin.residential.delete-resident', [$model["id"]]) . '" method="POST" class="ajax-form">
                                 ' . csrf_field() . '
                                 <button class="dropdown-item" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-trash" aria-hidden="true"></i> Purge</button>
                             </form>';
                }

                $links .= '</div>
                     </div>';

                return $links;
            })
            ->filter(function ($instance) use ($request) {
                if (isset($request->full_name)) {
                    $instance->where(DB::raw('CONCAT(first_name," ",last_name)'), 'LIKE', '%' . $request->get('full_name') . '%');
                }
                if (isset($request->formatted_phone)) {
                    $formatted_phone = onlyPhoneDigits($request->get('formatted_phone'));
                    $instance->where('phone_number', 'like', "%{$formatted_phone}%");
                }
                if (isset($request->email)) {
                    $instance->where('email', 'like', "%{$request->get('email')}%");
                }
                if (isset($request->community_name)) {
                    $instance->whereHas('community', function ($q) use ($request) {
                        $q->where('name', 'like', "%{$request->get('community_name')}%");
                    });
                }
                if (isset($request->house_name)) {
                    $instance->whereHas('house', function ($q) use ($request) {
                        $q->where('house_name', 'like', "%{$request->get('house_name')}%");
                    });
                }
                if (isset($request->full_address)) {
                    $instance->whereHas('house', function ($q) use ($request) {
                        $q->where('house_detail', 'like', "%{$request->get('full_address')}%");
                    });
                }
                if (isset($request->special_instruction)) {
                    $instance->where('special_instruction', 'like', "%{$request->get('special_instruction')}%");
                }
            })
            ->rawColumns(['links', 'is_completed_profile'])
            ->toJson();

        return $data;

    }

    public function getContactInDncByResidentId(Request $request, $id)
    {


        $user = User::where('id', $id)->first();
        if (!$user) {
            return APIResponse::error("No user found");
        }

        $UserCOntacts = $this->contactSrvc->getContactList($id, null, '', false, true);
        $house_id = $user->house_id;

        if (!$house_id) {
            return APIResponse::error("No house assigned to user");
        }

        $data = $this->userSrvc->getUsersByHouseIdApi($house_id, null, null);
        $familymemberUserIds = $data['result']->pluck('id')->toArray();

        $indexOfMyIdInArray = array_search($id, $familymemberUserIds);

        if ($indexOfMyIdInArray != -1) {
            unset($familymemberUserIds[$indexOfMyIdInArray]);
        }

        $sharedCOntactList = $this->contactSrvc->getSharedDncContactsByUserIds($familymemberUserIds);

        if ($UserCOntacts->isEmpty()) {

            $data = $sharedCOntactList;

        } else if ($sharedCOntactList->isEmpty()) {

            $data = $UserCOntacts;

        } else {

            $data = $UserCOntacts->merge($sharedCOntactList);
        }

        return APIResponse::success(['list' => $data], null);
    }

    public function getUsersCanSendPass($search = null)
    {
        $userContacts = $this->contactSrvc->getAllByUserHasPermissionCanSendPassById(auth()->id(), $search)['result'];

        return APIResponse::success(['list' => UserCanSendPassFormattedCollection::collection($userContacts)], null);
    }

    /**
     * add contact by pass created by id to add sender in contact
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addContactByCreatedById(Request $request)
    {
        $createdBy = $this->userSrvc->find($request->created_by_id)['result'];
        if (empty($createdBy)) {
            return APIResponse::error('Invalid Sender');
        }
        $userContact = $this->contactSrvc->find($request->contact_id)['result'];
        if (!empty($userContact)) {
            return APIResponse::error('Contact already added');
        }
        $contactData = array(
            'contact_name' => $createdBy->fullName,
            'phone_number' => $createdBy->phone_number,
            'dial_code' => $createdBy->dial_code,
        );
        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData);
        if ($userContact['bool']) {
            return APIResponse::success(new UserContactCollection($userContact['result']), 'Contact added successfully');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    /**
     * add contact by user Id for request a pass
     * @param Request $request
     * @return JsonResponse
     */
    public function addContactByUserId(Request $request)
    {
        $user = $this->userSrvc->find($request->user_id)['result'];
        if (empty($user)) {
            return APIResponse::error('Invalid User');
        }
        $contactData = array(
            'contact_name' => $user->fullName,
            'phone_number' => $user->phone_number,
            'dial_code' => $user->dial_code,
        );
        $userContact = $this->contactSrvc->createOrUpdateUserContact($contactData);
        if ($userContact['bool']) {
            return APIResponse::success(new UserContactCollection($userContact['result']), 'Contact added successfully');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    public function addContactSharedDnc(Request $request)
    {
        $contacts = $request->contactIds ?? null;
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $this->contactSrvc->update(['is_dnc_shared' => '1'], $contact);
            }
        }

        return APIResponse::success([], 'Contact(s) updated to favorites');
    }

}
