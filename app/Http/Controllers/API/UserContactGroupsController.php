<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContactGroupCollection;
use App\Http\Resources\UserContactCollection;
use App\Services\ContactGroupService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserContactGroupsController extends Controller
{
    /**
     * @var ContactGroupService
     */
    private $groupSrvc;

    /**
     * UserContactGroupsController constructor.
     *
     * @param ContactGroupService $contactGroupService
     */
    public function __construct(ContactGroupService $contactGroupService)
    {
        $this->groupSrvc = $contactGroupService;
    }

    /**
     * Show User Groups
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user_id = Auth::user()->id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $data = $this->groupSrvc->getContactGroupList($user_id, 30, $search);
        return APIResponse::success(['list' => $data], 'Contact Groups');
    }

    /**
     * Add Contacts In A Group
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function store(Request $request)
    {
        $contactGroup = $this->groupSrvc->createContactGroup($request->all());
        if ($contactGroup['bool']) {
            return APIResponse::success(new ContactGroupCollection($contactGroup['result']), 'group created successfully.');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    /**
     * add contact in group
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addContactInGroup(Request $request)
    {
        $contacts = $request->contact_list;
        $group_id = $request->group_id;
        $group = $this->groupSrvc->find($group_id);
        if (empty($group['result'])) {
            return APIResponse::error('Group not exist');
        }
        if (!empty($contacts)) {
            $result = array();
            foreach ($contacts as $contact) {
                $contactArray = [];
                $userContacts = $this->groupSrvc->addContactInGroup($contact['contact_id'], $group_id);
            }
            return APIResponse::success([], 'contact(s) added / updated to selected group');
        } else {
            return APIResponse::error('No contact(s) selected');
        }

    }

    /**
     * remove contact from group
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function bulkRemoveContactFromGroup(Request $request)
    {
        $contacts = $request->contactIds;
        $group_id = $request->group_id;
        $group = $this->groupSrvc->find($group_id);
        if (empty($group['result'])) {
            return APIResponse::error('Group not exist');
        }
        if (!empty($contacts)) {
            $group['result']->UserContacts()->detach($contacts);
            return APIResponse::success([], 'Contact(s) removed from group');
        } else {
            return APIResponse::error('No contact(s) selected');
        }
    }

    /**
     * update contact group
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        $contactGroup = $this->groupSrvc->update($request->all(), $id);
        if ($contactGroup['bool']) {
            return APIResponse::success([], 'group updated successfully.');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    /**
     * Remove Group
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        $this->groupSrvc->delete($id);
        return APIResponse::success([], 'Group deleted');
    }

    /**
     * Remove Group
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroyMany(Request $request)
    {
        $ids = $request->ids ?? null;
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->groupSrvc->delete($id);
            }
        }

        return APIResponse::success([], 'Group(s) deleted');
    }

    /**
     * get user contact by group
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */

    public function getUserContactByGroup(Request $request)
    {
        $contact_group_id = $request->contact_group_id;
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $group = $this->groupSrvc->find($contact_group_id);
        if (!empty($group['result'])) {
            $userContacts = $group['result']->userContacts();
            if (!empty($search)) {
                $userContacts = $userContacts->where(function ($query) use ($search) {
                    $query->where('contact_name', 'LIKE', "%$search%");
                });
            }
            $result = $userContacts->get();
            return APIResponse::success(['list' => UserContactCollection::collection($result)], null);
        }
        return APIResponse::error('Group not exist');
    }
}
