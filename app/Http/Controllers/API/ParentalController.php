<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ParentalControlService;
use App\Helpers\APIResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\ParentalControlCollection;
use Illuminate\Http\Request;

class ParentalController extends Controller
{

    //Will be Initialized by Constructor
    private $parentalControlService;

    /**
     * Constructor for Parental Controller
     *
     * @param ParentalControlService $parentalControlService
     */
    public function __construct(ParentalControlService $parentalControlService)
    {
        $this->parentalControlService = $parentalControlService;
    }

    public function index(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function store(Request $request)
    {
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $memberId
     * @return JsonResponse
     * @throws Exception
     */
    public function parentalControlOptions($memberId)
    {
        $parentalControls = $this->parentalControlService->getParentalControls(auth()->user()->id, $memberId);
        if ($parentalControls['result']) {
            return APIResponse::success([new ParentalControlCollection($parentalControls['result'])], 'Success');
        }
        return APIResponse::error('Sorry! Member does not belong to family');
    }

    /**
     * Set Parental Control Options.
     *
     * @param Request $request , int $memberId
     * @return JsonResponse
     */
    public function setParentalControlOptions(Request $request, $memberId)
    {
        $parentalControlData = [
            "notify_when_member_guest_arrive" => $request->notify_when_member_guest_arrive ? '1' : '0',
            "notify_when_member_send_pass_to_others" => $request->notify_when_member_send_pass_to_others ? '1' : '0',
            "notify_when_member_receive_a_pass" => $request->notify_when_member_receive_a_pass ? '1' : '0',
            "notify_when_member_retract_a_pass" => $request->notify_when_member_retract_a_pass ? '1' : '0',
            "notify_when_member_update_a_pass" => $request->notify_when_member_update_a_pass ? '1' : '0',
            "notify_when_pass_retracted_included_member" => $request->notify_when_pass_retracted_included_member ? '1' : '0',
            "notify_when_pass_updated_included_member" => $request->notify_when_pass_updated_included_member ? '1' : '0',
            "notify_when_member_request_a_pass_with_offensive_words" => $request->notify_when_member_request_a_pass_with_offensive_words ? '1' : '0',
            "blacklist_member_to_receive_request_a_pass" => $request->blacklist_member_to_receive_request_a_pass ? '1' : '0',
            "allow_parental_controls_to_member" => $request->allow_parental_controls_to_member ? '1' : '0'
        ];

        $result = $this->parentalControlService->firstOrCreate([
            'parent_id' => auth()->user()->id,
            'member_id' => $memberId
        ], $parentalControlData);
        if ($result['bool']) {
            return APIResponse::success([], 'Parental controls updated successfully');
        }

        return APIResponse::error('Something went wrong');
    }

}
