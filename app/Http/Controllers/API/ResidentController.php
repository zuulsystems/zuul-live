<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventCollection;
use App\Models\User;
use App\Models\CountryPhoneFormat;
use App\Models\UserAdditionalContact;
use App\Services\EventService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;
use App\Services\ResidentService;

class ResidentController extends Controller
{
    use UserCommon;

    protected $residentService;

    public function __construct(ResidentService $residentService)
    {
        $this->residentService = $residentService;
    }

    /**
     * -------------------------------------------
     * Get Resident Additional Phone List
     * -------------------------------------------
     *
     * This method retrieves the additional phone list for a resident based on the provided ID.
     *
     * @param int $id The ID of the resident.
     * @return JsonResponse The JSON response with the result of the resident additional phone list.
     *
     * @throws Exception If an error occurs during the process of retrieving the resident additional phone list.
     *
     * @deprecated This method will be removed in the next version. Please use the `ResidentService` class instead.
     *
     * @see ResidentService
     */
    public function index($id): JsonResponse
    {
        $result = $this->residentService->getResidentAdditionalPhoneList($id);

        return $result;
    }

    /**
     * -------------------------------------------
     * Add Resident Additional Phone Number
     * -------------------------------------------
     *
     * This method adds an additional phone number for a resident based on the request data.
     *
     * @param Request $request The request object containing the resident additional phone list.
     * @return JsonResponse The JSON response with the result of adding the additional phone number.
     *
     * @throws Exception If an error occurs during the process of adding the additional phone number.
     *
     * @deprecated This method will be removed in the next version. Please use the `ResidentService` class instead.
     *
     * @see ResidentService
     */
    public function store(Request $request)
    {
        $userData = $request->all();

        $result = $this->residentService->createResidentAdditionalPhoneNumber($userData, $request);

        return $result;
    }

    /**
     * -------------------------------------------
     * Edit Resident Additional Phone Number
     * -------------------------------------------
     *
     * This method retrieves the additional contact information for editing based on the provided ID.
     *
     * @param int $id The ID of the additional contact.
     * @return JsonResponse The JSON response with the result of the additional contact for editing.
     *
     * @throws Exception If an error occurs during the process of retrieving the additional contact.
     */
    public function edit($id)
    {
        // Call the editResidentAdditionalPhoneNumber method from the ResidentService
        $result = $this->residentService->editResidentAdditionalPhoneNumber($id);

        // Return the result
        return $result;
    }

    /**
     * -------------------------------------------
     * Update Resident Additional Phone Number
     * -------------------------------------------
     *
     * This method updates the additional phone number for a resident based on the provided ID and request data.
     *
     * @param Request $request The request object containing the updated additional phone number data.
     * @param int $id The ID of the additional contact.
     * @return JsonResponse The JSON response with the result of updating the additional phone number.
     *
     * @throws Exception If an error occurs during the process of updating the additional phone number.
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        // Call the updateResidentAdditionalPhoneNumber method from the ResidentService
        $result = $this->residentService->updateResidentAdditionalPhoneNumber($id, $data);

        // Return the result
        return $result;
    }

    /**
     * -------------------------------------------
     * Delete Resident Additional Phone Number
     * -------------------------------------------
     *
     * This method soft deletes the additional contact for a resident based on the provided ID.
     *
     * @param int $id The ID of the additional contact.
     * @return JsonResponse The JSON response with the result of deleting the additional contact.
     *
     * @throws Exception If an error occurs during the process of deleting the additional contact.
     */
    public function destroy($id)
    {
        // Call the deleteAdditionalContact method from the ResidentService
        $result = $this->residentService->deleteResidentAdditionalPhoneNumber($id);

        // Return the result
        return $result;
    }

    /**
     * -------------------------------------------
     * Get Phone Format by Country Code
     * -------------------------------------------
     *
     * This method retrieves the phone format based on the country code for a specific user.
     *
     * @param int $userId The ID of the user.
     * @return JsonResponse The JSON response with the result of the phone format for the country code.
     *
     * @throws Exception If an error occurs during the process of retrieving the phone format.
     */
    public function getPhoneFormatByCountryCode($userId)
    {
        // Call the getPhoneFormatByCountryCode method from the ResidentService
        $result = $this->residentService->getPhoneFormatByCountryCode($userId);

        // Return the result
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }
}
