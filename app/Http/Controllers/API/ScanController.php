<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Helpers\PushNotificationTrait;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Http\Resources\PrinterGuestParkTemplateCollection;
use App\Http\Resources\PrinterVisitorPassTemplateCollection;
use App\Http\Resources\QuickPassCollection;
use App\Jobs\SendNotificationsForValidateQrCode;
use App\Models\RemoteGuardImage;
use App\Models\ScanLog;
use App\Models\User;
use App\Services\NotificationService;
use App\Services\PassUserService;
use App\Services\ScanLogService;
use App\Services\StaticScanLogService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\BannedCommunityUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class ScanController extends Controller
{
    use UserCommon, PushNotificationTrait;

    private $passUserService;
    private $scanLogService;
    private $staticScanLogService;
    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * PassController constructor.
     *
     * @param PassUserService $passUserService
     * @param ScanLogService $scanLogService
     */
    public function __construct(
        passUserService      $passUserService,
        ScanLogService       $scanLogService,
        StaticScanLogService $staticScanLogService,
        NotificationService  $notificationService

    )
    {
        $this->passUserService = $passUserService;
        $this->scanLogService = $scanLogService;
        $this->staticScanLogService = $staticScanLogService;
        $this->notificationService = $notificationService;
    }

    public function getCameraSnap(Request $request, $camera_id)
    {

        $scanQrCode = $this->passUserService->getCameraById($camera_id);

    }

    /**
     * Scan QrCode by type
     * @param Request $request
     * @param $type
     * @param $id =>  userId Or scannerId
     * @param $qrCode
     * @return JsonResponse
     * @throws Exception
     */
    public function scannedQrCode(Request $request, $type, $id, $qrCode)
    {
        $isRemoteGuard = $request->guard_type == "remote_guard";
        $guardId = $request->guard_id;

        if ($type != 'scanner' && $type != 'guard') {
            return APIResponse::error('type mismatch error');
        }

        $passUser = $this->passUserService->getByQrCode($qrCode)['result'];


        if (empty($passUser)) {
            return [
                'bool' => false,
                'message' => 'User has no pass'
            ];
        }


        $scanQrCode = "";

        if ($type == "guard") {
            if (!isset($isRemoteGuard)) {
                Log::emergency(auth()->user());
                $check_banned_user = BannedCommunityUser::where('phone_number', $passUser->user->phone_number)->where('community_id', auth()->user()->community_id)->first();

                if ($check_banned_user != null) {
                    return APIResponse::error('THIS GUEST IS BANNED FROM THE COMMUNITY. DENY ENTRY!');
                }
                $band = BannedCommunityUser::where(['community_id' => $passUser->pass->community->id, 'user_id' => $passUser->user->id]);

                if (!$band->get()->isEmpty()) {
                    return [
                        'bool' => false,
                        'message' => 'THIS GUEST IS BANNED FROM THE COMMUNITY.  DENY ENTRY'
                    ];
                }
            }
        }

        $scanQrCode = $this->passUserService->scanQrCode($type, $id, $qrCode, 1, $passUser->user->full_name, $isRemoteGuard, $guardId);

        if ($scanQrCode['bool']) {

            $this->notificationService->notifyAfterPassScan($passUser);


            $scanLog = ScanLog::with(
                'passUser.pass.community',
                'passUser.pass.createdBy:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code',
                'passUser.pass.createdBy.house',
                'passUser.user.house',
                'passUser.user:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code',
                'passUser.pass.event',
                'passUser.vehicle',
                'scanner',
                'scanBy'
            )->where('id', $scanQrCode['scanLog']->id)->first();


            if ($type == "scanner") {


                $data = array(
                    'start' => Carbon::now(),
                    'expires' => Carbon::now(),
                    'scan_date' => Carbon::now(),
                    'scan_log_id' => $scanQrCode['scanLog']->id,
                );

                $staticScanLog = $this->staticScanLogService->create($data);
                $passSentByEmail = $scanLog->passUser->pass->createdBy->email;
                sendGuestArrivalEmailToResident($passSentByEmail, $staticScanLog, true);

                return response()->json([
                    'bool' => true,
                    'message' => $scanQrCode['message'],
                    "scanLog" => $scanQrCode['scanLog']->id
                ])->header('Content-Type', 'text/plain');
            } else {
                return APIResponse::success((new QuickPassCollection($passUser))->setScanLog($scanQrCode['scanLog']), $scanQrCode['message']);
            }

        }
        return APIResponse::error($scanQrCode['message']);
    }

    public function webhook(Request $request)
    {
        $request_body = file_get_contents('php://input');
        Log::emergency($request_body);

        return APIResponse::success([
            'status' => true,
            'requests' => $request_body,
        ], 'camera configuration');
    }

    public function webhookCall()
    {

        try {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://107.128.123.204:8192/hooks/response',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('status' => 'true', 'mac_address' => 'abc'),
            ));

            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Connection: keep-alive',
                'Accept-Encoding: gzip, deflate, br',
                'Accept: */*',
                'Content-Type: multipart/form-data;',
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            Log::emergency($response);
            return APIResponse::success([
                'status' => true,
                'requests' => $response,
            ], 'camera configuration');
        } catch (Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }


    }

    /**
     * Printer Visitor Pass Template
     *
     * @param $scanLogId
     * @return JsonResponse
     * @throws Exception
     */
    public function getPrinterVisitorPassTemplate($scanLogId)
    {
        $scanLog = $this->scanLogService->getPrinterTemplate($scanLogId);

        if (!empty($scanLog['result'])) {
            $scanLog = $scanLog['result'];
            if ($scanLog->type == 'guard') {
                if ($scanLog->passUser->pass->community->name == $scanLog->community->name) {

                    //Dynamic Printer Visitor Pass Template
                    $printerTemplate = getPrinterTemplate('Visitor Pass', $scanLog->community->id);

                    $template = $printerTemplate->template ?? "";

                    $templateData['community_name'] = $scanLog->passUser->pass->community->name;

                    $templateData['expiration_date'] = $scanLog->passUser->pass->pass_date;
                    $date = strtotime($templateData['expiration_date']);
                    $templateData['expiration_date'] = date('m/d/Y', $date);

                    $templateData['time'] = $scanLog->passUser->scan_at;
                    $date = strtotime($templateData['time']);
                    $templateData['time'] = date('h:i:s', $date);

                    $templateData['issued_date'] = $scanLog->passUser->scan_at;
                    $date = strtotime($templateData['issued_date']);
                    $templateData['issued_date'] = date('m/d/Y', $date);

                    $templateData['make'] = $scanLog->passUser->vehicle->make ?? '';
                    $templateData['guest_name'] = $scanLog->passUser->user->first_name . " " . $scanLog->passUser->user->last_name;
                    $templateData['license_plate'] = $scanLog->passUser->vehicle->license_plate ?? '';
                    $templateData['directions_to'] = $scanLog->passUser->pass->createdBy->house->house_detail;

                    if (!empty($scanLog->passUser->pass)) {
                        if ($scanLog->passUser->pass->is_quick_pass == '1') {
                            $templateData['guest_name'] = $scanLog->guard_display_name;
                        }
                    }

                    $visitorPassTemplate['template'] = replacePrinterVisitorPassTemplateStrings($template, $templateData);

                    return APIResponse::success((new PrinterVisitorPassTemplateCollection($visitorPassTemplate)));

                } else {

                    return APIResponse::success('You are not in the Correect Community');

                }

            } else {
                return APIResponse::success('You have not the corrent scanner log id');
            }
        }

        return APIResponse::error('Something went wrong');

    }

    /**
     * Printer Template
     *
     * @param $scanLogId
     * @return JsonResponse
     * @throws Exception
     */
    public function getPrinterTemplate($scanLogId)
    {


        $scanLog = $this->scanLogService->getPrinterTemplate($scanLogId);


        if ($scanLog['bool']) {

            $temp = $scanLog;
            $scanLog = $scanLog['result'];

            if (!$scanLog) {
                return APIResponse::error('No Scan Log Object', $temp);
            }

            if ($scanLog->community_id == null || !$scanLog->community->id) {
                return APIResponse::error('Something went wrong');
            }

            //Printer GUEST PARKING TEMPLATE
            $printerTemplate = getPrinterTemplate('GUEST PARKING', $scanLog->community->id);
            $printerTemplate = $printerTemplate->template ?? "";

            $printerTemplateData['date'] = $scanLog->passUser->scan_at;
            $date = strtotime($printerTemplateData['date']);
            $printerTemplateData['issued_date'] = date('m/d/Y', $date);

            $printerTemplateData['time_in'] = $scanLog->passUser->scan_at;
            $date = strtotime($printerTemplateData['time_in']);
            $printerTemplateData['time'] = date('h:i:s', $date);


            // S/O name missing
            $printerTemplateData['father_name'] = '';
            $printerTemplateData['guest_name'] = $scanLog->passUser->user->first_name . " " . $scanLog->passUser->user->last_name;
            $printerTemplateData['building_or_unit_name'] = $scanLog->passUser->pass->createdBy->house->house_detail;
            $printerTemplateData['phone'] = $scanLog->passUser->user->phone_number;

            $printerTemplateData['tag'] = $scanLog->passUser->vehicle->license_plate ?? '';

            if (!empty($scanLog->passUser->vehicle->license_plate)) {
                $printerTemplateData['tag'] = strtoupper($scanLog->passUser->vehicle->license_plate);
            }

            $printerTemplateData['make'] = $scanLog->passUser->vehicle->make ?? '';
            $printerTemplateData['model'] = $scanLog->passUser->vehicle->model ?? '';
            $printerTemplateData['color'] = $scanLog->passUser->vehicle->color ?? '';

            $printerTemplateData['vehicle_detail'] = "Not found";

            if (!empty($scanLog->passUser->vehicle)) {
                $printerTemplateData['vehicle_detail'] = $scanLog->passUser->vehicle->make . ", " . $scanLog->passUser->vehicle->model . ", " . $scanLog->passUser->vehicle->color;
            }

            if (!empty($scanLog->passUser->pass)) {
                if ($scanLog->passUser->pass->is_quick_pass == '1') {
                    $printerTemplateData['guest_name'] = $scanLog->guard_display_name;
                    $printerTemplateData['building_or_unit_name'] = $scanLog->passUser->pass->createdBy->house->house_detail;
                }
            }

            $printerTemplateData['community_name'] = $scanLog->passUser->pass->community->name;


            $printerTemplateData['expiration_date'] = $scanLog->passUser->pass->pass_date;
            $date = strtotime($printerTemplateData['expiration_date']);
            $printerTemplateData['expiration_date'] = date('m/d/Y', $date);

            $printerTemplateData['license_plate'] = $scanLog->passUser->vehicle->license_plate ?? '';

            if (!empty($printerTemplateData['license_plate'])) {
                $printerTemplateData['license_plate'] = strtoupper($scanLog->passUser->vehicle->license_plate);
            }

            $printerTemplateData['directions_to'] = $scanLog->passUser->pass->createdBy->house->house_detail;

            $printerTemplateData['printerGuestParkingTemplate'] = replacePrinterTemplateStrings($printerTemplate, $printerTemplateData);

            return APIResponse::success((new PrinterGuestParkTemplateCollection($printerTemplateData)));

        }

        return APIResponse::error('Something went wrong');
    }

    /**
     * Printer Guest Parking Template
     *
     * @param $scanLogId
     * @return JsonResponse
     * @throws Exception
     */
    public function getGuestParkingPassTemplate($scanLogId)
    {
        $scanLog = $this->scanLogService->getPrinterTemplate($scanLogId);

        if ($scanLog['bool']) {

            $scanLog = $scanLog['result'];

            //Printer GUEST PARKING TEMPLATE
            $printerGUESTPARKINGTemplate = getPrinterTemplate('GUEST PARKING', $scanLog->community->id);
            $printerGUESTPARKINGTemplate = $printerGUESTPARKINGTemplate->template ?? "";

            $printerGuestParkingTemplateData['date'] = $scanLog->passUser->scan_at;
            $date = strtotime($printerGuestParkingTemplateData['date']);
            $printerGuestParkingTemplateData['issued_date'] = date('m/d/Y', $date);

            $printerGuestParkingTemplateData['time_in'] = $scanLog->passUser->scan_at;
            $date = strtotime($printerGuestParkingTemplateData['time_in']);
            $printerGuestParkingTemplateData['time'] = date('h:i:s', $date);

            // S/O name missing
            $printerGuestParkingTemplateData['father_name'] = '';
            $printerGuestParkingTemplateData['guest_name'] = $scanLog->passUser->user->first_name . " " . $scanLog->passUser->user->last_name;
            $printerGuestParkingTemplateData['building_or_unit_name'] = $scanLog->passUser->pass->createdBy->house->house_detail;
            $printerGuestParkingTemplateData['phone'] = $scanLog->passUser->user->phone_number;

            $printerGuestParkingTemplateData['tag'] = $scanLog->passUser->vehicle->tag ?? '';
            $printerGuestParkingTemplateData['make'] = $scanLog->passUser->vehicle->make ?? '';
            $printerGuestParkingTemplateData['model'] = $scanLog->passUser->vehicle->model ?? '';
            $printerGuestParkingTemplateData['color'] = $scanLog->passUser->vehicle->color ?? '';

            $printerGuestParkingTemplateData['vehicle_detail'] = "Not found";

            if (!empty($scanLog->passUser->vehicle)) {
                $printerGuestParkingTemplateData['vehicle_detail'] = $scanLog->passUser->vehicle->make . ", " . $scanLog->passUser->vehicle->model . ", " . $scanLog->passUser->vehicle->color;
            }

            if (!empty($scanLog->passUser->pass)) {
                if ($scanLog->passUser->pass->is_quick_pass == '1') {
                    $printerGuestParkingTemplateData['guest_name'] = $scanLog->guard_display_name;
                    $printerGuestParkingTemplateData['building_or_unit_name'] = $scanLog->passUser->user->house->house_detail;
                }
            }

            $printerGuestParkingTemplateData['printerGuestParkingTemplate'] = replacePrinterGuestParkingTemplateStrings($printerGUESTPARKINGTemplate, $printerGuestParkingTemplateData);

            return APIResponse::success((new PrinterGuestParkTemplateCollection($printerGuestParkingTemplateData)));

        }

        return APIResponse::error('Something went wrong');
    }

    /**
     * get license plate image url from scanLog
     *
     * @param $scanLogId
     * @return JsonResponse
     * @throws Exception
     */
    public function getLicensePlateOfScanLog($scanLogId)
    {
        $scanLog = $this->scanLogService->find($scanLogId)['result'];
        if (empty($scanLog)) {
            return APIResponse::error('No Record Found.');
        }
        return APIResponse::success([
            'license_plate_image_url' => $scanLog->LicensePlateImageUrl
        ], '');
    }

    public function getUserLicenseImageViaKiosk($type, $id)
    {
        $colValue = $type == 0 ? 'Driver License' : 'License Plate';
        $remoteGuardData = RemoteGuardImage::where('scan_log_id', $id)
            ->where('image_type', $colValue)
            ->latest('id')
            ->first();

        return APIResponse::success($remoteGuardData ?? null, 'Remote Guard Record');
    }

}
