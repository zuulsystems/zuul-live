<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Helpers\APIResponse;
use App\Models\BannedCommunityUser;
use App\Models\Camera;
use App\Models\ConnectedPie;
use App\Models\RemoteGuardWebTracking;
use App\Models\RemoteWebRelayTracking;
use App\Models\User;
use App\Models\Notification;
use App\Models\RemoteGuard;
use App\Http\Resources\GuardAppointLocationCollection;
use App\Http\Resources\GuardCollection;
use App\Http\Resources\QuickPassCollection;
use App\Jobs\SendNotificationsForValidateQrCode;
use App\Models\WebRelay;
use App\Models\WebRelayTracking;
use App\Models\RemoteGuardQrcodeRequest;
use App\Rules\ValidatePhoneNumber;
use App\Services\GuardAppointLocationService;
use App\Services\PassService;
use App\Services\PassUserService;
use App\Services\UserService;
use App\Services\StaticScanLogService;
use Carbon\Carbon;
use Exception;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\ScanLogService;
use App\Models\ScanLog;
use App\Models\RemoteGuardSocket;
use Illuminate\Support\Facades\Log;

use App\Helpers\PushNotificationTrait;
use App\Http\Resources\GuardApiCollection;
use PhpOffice\PhpSpreadsheet\Calculation\Web;

use Illuminate\Support\Facades\Http;

class GuardController extends Controller
{

    use PushNotificationTrait;

    //Will be Initialized by Constructor
    private $userService;
    private $passService;
    private $passUserService;
    private $guardAppointLocationService;
    private $staticScanLogService;

    private $scanLogService;


    /**
     * Constructor for GuardController
     *
     * @param UserService $userService
     * @param PassService $passService
     * @param PassUserService $passUserService
     */
    public function __construct(
        UserService                 $userService,
        PassService                 $passService,
        PassUserService             $passUserService,
        GuardAppointLocationService $guardAppointLocationService,
        StaticScanLogService        $staticScanLogService,
        ScanLogService              $scanLogService
    )
    {
        $this->userService = $userService;
        $this->passService = $passService;
        $this->passUserService = $passUserService;
        $this->guardAppointLocationService = $guardAppointLocationService;
        $this->staticScanLogService = $staticScanLogService;
        $this->scanLogService = $scanLogService;
    }

    /**
     * Get guard residents
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function getGuardResidents(Request $request)
    {
        if (!auth()->user()->hasRole('guards') && !auth()->user()->hasRole('guard_admin')) {
            return APIResponse::error('Invalid Guard User');
        }
        $search = $request->search ?? null;
        $page = $request->page;
        $result = $this->userService->getGuardResidents(auth()->user()->community_id, null, $search, $page);
        if ($result['result']) {
            return APIResponse::success(['list' => GuardApiCollection::collection($result['result'])], 'Guard Resident list');
        }
        return APIResponse::success(['list' => []], 'Guard Resident list');
    }

    public function getGuardResidentsKiosk(Request $request)
    {

        $remoteGuard = RemoteGuard::where(["id" => $request->remote_guard])->first();

        if (!$remoteGuard) {
            return APIResponse::error('Remote guard not found');
        }

        $residents = User::where('community_id', $remoteGuard->community_id)->whereIn('role_id', [4, 5])->with(['additionalPhone', 'rfids'])->get();
        return APIResponse::success(['list' => GuardApiCollection::collection($residents)], 'Guard Resident list');


    }

    public function createQuickPass2(Request $request)
    {

        $passData = $request->all();
        $passData['phone_number'] = $request->formatted_phone_number;
        $passData['display_name'] = $passData['contact_name'];
        $validator = Validator::make($passData, [
            'created_by' => 'required',
            'contact_name' => 'required',
            'formatted_phone_number' => ['required', new ValidatePhoneNumber],
        ], [
            'created_by.required' => 'Please select resident',
            'contact_name.required' => 'Please enter display name',
            'formatted_phone_number.required' => 'Please enter phone number'
        ]);
        if (!empty($request->user_id)) {
            $userId = $request->user_id;
        } else {
            $userId = auth()->id();
        }

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $createdBy = $passData['created_by'];
        $passData['visitor_type'] = "friends_family";
        $pass = $this->passService->createQuickPass($passData, $createdBy);


        if ($pass['bool']) {
            //+++++++++++++ Scan Qrcode ++++++++++++++++
            $qrCode = $pass['qrcodes'][0];

            $scanQrCode = $this->passUserService->scanQrCode('guard', $userId, $qrCode, 1, $passData['display_name']);

            if (isset($scanQrCode['bool']) && $scanQrCode['bool'] == false) {
                return APIResponse::error($scanQrCode['message']);
            }
            $scanLog = ScanLog::with(
                'passUser.pass.community',
                'passUser.pass.createdBy:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code',
                'passUser.pass.createdBy.house',
                'passUser.user.house',
                'passUser.user:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code',
                'passUser.pass.event',
                'passUser.vehicle',
                'scanner',
                'scanBy'
            )->where('id', $scanQrCode['scanLog']->id)->first();


            if (!empty($scanLog->guard_display_name)) {
                $guestName = $scanLog->guard_display_name;
            } else {
                $guestName = $passData['display_name'];
            }


            $senderFullName = $scanLog->passUser->pass->createdBy->fullName;
            $senderFormattedPhone = $scanLog->passUser->pass->createdBy->FormattedPhone;
            $senderFormattedAddress = $scanLog->passUser->pass->createdBy->formattedAddress;

            if (!empty($scanLog->guard_display_name)) {
                $receiverFullName = $scanLog->guard_display_name;
            } else if (!empty($scanLog->passUser->user->fullName)) {
                $receiverFullName = $scanLog->passUser->user->fullName;
            }

            $receiverFormattedPhone = $scanLog->passUser->user->FormattedPhone;
            $receiverFormattedAddress = $scanLog->passUser->user->formattedAddress;


            $community = $scanLog->passUser->pass->community->name;
            $event = $scanLog->passUser->pass->event->name;
            $start = $scanLog->passUser->pass->pass_start_date;
            $expire = $scanLog->passUser->pass->pass_date;
            $scanAt = $scanLog->created_at;
            $scannedBy = $scanLog->ScanByName;


            $licensePlate = '';
            $color = '';
            $make = '';
            $model = '';
            $year = '';

            if (!empty($scanLog->passUser->vehicle)) {
                $licensePlate = $scanLog->passUser->vehicle->license_plate;
                $color = $scanLog->passUser->vehicle->color;
                $make = $scanLog->passUser->vehicle->make;
                $model = $scanLog->passUser->vehicle->model;
                $year = $scanLog->passUser->vehicle->year;
            }

            $data = array(
                'sender_full_name' => $senderFullName,
                'sender_formatted_phone' => $senderFormattedPhone,
                'sender_formatted_address' => $senderFormattedAddress,
                'recipient_full_name' => $receiverFullName,
                'recipient_formatted_phone' => $receiverFormattedPhone,
                'recipient_formatted_address' => $receiverFormattedAddress,
                'community' => $community,
                'event' => $event,
                'start' => $start,
                'expires' => $expire,
                'scan_date' => $scanAt,
                'scan_by' => $scannedBy,
                'license_plate' => $licensePlate,
                'color' => $color,
                'make' => $make,
                'model' => $model,
                'year' => $year,
                'scan_log_id' => $scanQrCode['scanLog']->id,
            );

            $this->staticScanLogService->create($data);

            if ($scanQrCode['bool'] || true) {
                //Send scan pass notifications
                $passUser = $this->passUserService->getByQrCode($qrCode)['result'];


                SendNotificationsForValidateQrCode::dispatch($passUser)->delay(Carbon::now()->addSeconds(2));
                return APIResponse::success((new QuickPassCollection($passUser))->setScanLog($scanQrCode['scanLog']), $scanQrCode['message']);

            } else {
                return APIResponse::error($scanQrCode['message']);
            }
            //+++++++++++ End Qrcode ++++++++++++++++++

            app('app\Http\Controllers\API\CameraController')->store($request);


        }


        return APIResponse::error($pass['message']);
    }

    /**
     * create quick for behalf of the resident
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function createQuickPass(Request $request)
    {
        $passData = $request->all();

        // validating the required keys
        $validator = Validator::make($passData, [
            'resident_id' => 'required',
            'display_name' => 'required',
            'phone_number' => 'required',
            'dial_code' => 'required'
        ], [
            'resident_id.required' => 'Please select resident',
            'display_name.required' => 'Please enter display name',
            'phone_number.required' => 'Please enter phone number',
            'dial_code.required' => 'Please select the dial code'
        ]);


        if (!empty($request->user_id)) {
            $userId = $request->user_id;
        } else {
            $userId = auth()->id();
        }
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $passData['phone_number'] = $request->phone_number;

        // Getting dial code
        $dial_code = $passData['dial_code'];

        if (auth()->user() != null) {
            // phone number format
            $phone_format = numberFormat($passData['phone_number'], $dial_code);

            $check_banned_user = BannedCommunityUser::where('user_name', $passData['display_name'])->where('community_id', auth()->user()->community_id)->first();

            if ($check_banned_user != null) {
                return APIResponse::error('THIS GUEST ' . $passData['display_name'] . ' IS BANNED FROM THE COMMUNITY. DENY ENTRY!');
            }

            $check_banned_user = BannedCommunityUser::where('phone_number', $passData['phone_number'])->where('community_id', auth()->user()->community_id)->first();

            if ($check_banned_user != null) {
                return APIResponse::error('THIS GUEST ' . $passData['phone_number'] . ' IS BANNED FROM THE COMMUNITY. DENY ENTRY!');
            }
        }


        $createdBy = $passData['resident_id'];
        $pass = $this->passService->createQuickPass($passData, $createdBy);

        if ($pass['bool']) {
            //+++++++++++++ Scan Qrcode ++++++++++++++++
            $qrCode = $pass['qrcodes'][0];

            $scanQrCode = $this->passUserService->scanQrCode('guard', $userId, $qrCode, 1, $passData['display_name']);


            if (isset($scanQrCode['bool']) && $scanQrCode['bool'] == false) {
                return APIResponse::error($scanQrCode['message']);
            }
            $scanLog = ScanLog::with(
                'passUser.pass.community',
                'passUser.pass.createdBy:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code,email',
                'passUser.pass.createdBy.house',
                'passUser.user.house',
                'passUser.user:id,first_name,last_name,phone_number,community_id,house_id,street_address,apartment,city,state,zip_code',
                'passUser.pass.event',
                'passUser.vehicle',
                'scanner',
                'scanBy'
            )->where('id', $scanQrCode['scanLog']->id)->first();


            if (!empty($scanLog->guard_display_name)) {
                $guestName = $scanLog->guard_display_name;
            } else {
                $guestName = $passData['display_name'];
            }

            $senderFullName = $scanLog->passUser->pass->createdBy->fullName;
            $senderFormattedPhone = $scanLog->passUser->pass->createdBy->FormattedPhone;
            $senderFormattedAddress = $scanLog->passUser->pass->createdBy->formattedAddress;

            if (!empty($scanLog->guard_display_name)) {
                $receiverFullName = $scanLog->guard_display_name;
            } else if (!empty($scanLog->passUser->user->fullName)) {
                $receiverFullName = $scanLog->passUser->user->fullName;
            }

            $receiverFormattedPhone = $scanLog->passUser->user->FormattedPhone;
            $receiverFormattedAddress = $scanLog->passUser->user->formattedAddress;


            $community = $scanLog->passUser->pass->community->name;
            $event = $scanLog->passUser->pass->event->name;
            $start = $scanLog->passUser->pass->pass_start_date;
            $expire = $scanLog->passUser->pass->pass_date;
            $scanAt = $scanLog->created_at;
            $scannedBy = $scanLog->ScanByName;


            $licensePlate = '';
            $color = '';
            $make = '';
            $model = '';
            $year = '';

            if (!empty($scanLog->passUser->vehicle)) {
                $licensePlate = $scanLog->passUser->vehicle->license_plate;
                $color = $scanLog->passUser->vehicle->color;
                $make = $scanLog->passUser->vehicle->make;
                $model = $scanLog->passUser->vehicle->model;
                $year = $scanLog->passUser->vehicle->year;
            }

            $sender = $senderFullName . ' ' . $senderFormattedPhone . ' ' . $senderFormattedAddress;
            $recipient = $receiverFullName . ' ' . $scanLog->passUser->user->fullName . ' ' . $receiverFormattedPhone . ' ' . $receiverFormattedAddress;

            $data = array(
                'sender_full_name' => $senderFullName,
                'sender_formatted_phone' => $senderFormattedPhone,
                'sender_formatted_address' => $senderFormattedAddress,
                'sender' => $sender,
                'recipient_full_name' => $receiverFullName,
                'recipient_formatted_phone' => $receiverFormattedPhone,
                'recipient_formatted_address' => $receiverFormattedAddress,
                'recipient' => $recipient,
                'community_name' => $community,
                'event_name' => $event,
                'start' => $start,
                'expires' => $expire,
                'scan_date' => $scanAt,
                'scan_by' => $scannedBy,
                'license_plate' => $licensePlate,
                'color' => $color,
                'make' => $make,
                'model' => $model,
                'year' => $year,
                'scan_log_id' => $scanQrCode['scanLog']->id,
            );


            $staticScanLog = $this->staticScanLogService->create($data);

            if ($scanQrCode['bool'] || true) {
                //Send scan pass notifications
                $passUser = $this->passUserService->getByQrCode($qrCode)['result'];

                return APIResponse::success((new QuickPassCollection($passUser))->setScanLog($scanQrCode['scanLog']), $scanQrCode['message']);

            } else {
                return APIResponse::error($scanQrCode['message']);
            }
            //+++++++++++ End Qrcode ++++++++++++++++++
        }
        return APIResponse::error($pass['message']);
    }

    /**
     * get guard appoint locations
     * @param Request $request
     * @return JsonResponse
     */

    public function getScanQrByKiosk(Request $request)
    {

        $data = $request->all();
        $user = auth()->user();


        $remoteGuard = RemoteGuard::where(['community_id' => $user->community_id])->first();

        if (!$remoteGuard) {
            return APIResponse::error('Remote guard not found');
        }

        $remoteGuardSocket = RemoteGuardSocket::where(['remote_guard_id' => $remoteGuard->id])->first();

        if (!$remoteGuardSocket) {
            return APIResponse::error('Remote guard sockets not found');
        }

        RemoteGuardQrcodeRequest::updateOrCreate([
            'user_id' => $user->id
        ], [
            'qrcode' => null,
            'expire_time' => Carbon::now()->addMinutes(5),
            'socket_id' => $remoteGuardSocket->socket_id
        ]);

        $url = "https://devsockets.zuulsystems-security.com/api/v1/guard/get-guard-scan-by-qr";
        $apiData = [
            'socket_id' => $remoteGuardSocket->socket_id,
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Socket-Header-Key' => 'sHHydot6qx8H6GZo',
            'Socket-Secret-Key' => 'e4d1Y8ANRFAjNQ7L',
        ];

        $response = Http::withHeaders($headers)->post($url, ['data' => $apiData]);


        return APIResponse::success($apiData, 'Scan Request Initiate successfully');
    }

    public function sendGuardScanByQrByKiosk(Request $request)
    {

        $data = $request->all();
        $user = auth()->user();

        if (!$data['socket_id']) {
            return APIResponse::error("Socket id is required");
        }

        if (!$data['qrcode']) {
            return APIResponse::error("Qrcode is required");
        }

        $requestObj = RemoteGuardQrcodeRequest::where([
            'socket_id' => $data['socket_id']
        ])->first();

        if (!$requestObj) {
            return APIResponse::error("Socket is failed to identify");
        }

        $requestObj->qrcode = $data['qrcode'];
        $requestObj->save();

        return APIResponse::success('Qrcode Request Initiate successfully');


    }

    public function receiveScanQrByKiosk(Request $request, $socket_id)
    {

        $data = $request->all();
        $user = auth()->user();

        if (!$socket_id) {
            return APIResponse::error("Socket id is required");
        }


        $requestObj = RemoteGuardQrcodeRequest::where([
            'socket_id' => $socket_id
        ])->first();

        if (!$requestObj) {
            return APIResponse::error("Socket is failed to identify ");
        }

        if (!$requestObj->qrcode) {
            return APIResponse::success(['qrcode' => null], "Qrcode not initialized ");
        }

        return APIResponse::success(['qrcode' => $requestObj->qrcode], 'Qrcode Request Initiate successfully');


    }


    public function getGuardAppointLocations(Request $request)
    {

        $appointLocations = $this->guardAppointLocationService->GetAllByUserId(auth()->id())['result'];
        if ($appointLocations->isNotEmpty()) {
            return APIResponse::success([
                'list' => GuardAppointLocationCollection::collection($appointLocations),
                'is_guard_geo_fence' => (isset(auth()->user()->community) && !empty(auth()->user()->community)) ? auth()->user()->community->guard_geo_fence : null,
            ], 'Guard Appoint Locations');
        }
        return APIResponse::success([
            'list' => [],
            'is_guard_geo_fence' => (isset(auth()->user()->community) && !empty(auth()->user()->community)) ? auth()->user()->community->guard_geo_fence : null,
        ], 'Guard Appoint Locations');
    }

    /**
     * set appoint locations
     * @param Request $request
     * @return JsonResponse
     */
    public function setGuardAppointLocations(Request $request)
    {

        $latLongData = $request->all();
        $validation = Validator::make($latLongData, [
            'lat' => 'required',
            'long' => 'required'
        ]);

        if ($validation->fails()) {
            return APIResponse::error($validation->errors()->first());
        }

        $latLongData['user_id'] = auth()->user()->id;

        $result = $this->guardAppointLocationService->create($latLongData);

        if ($result['bool']) {
            return APIResponse::success('Latitude and Longitude added successfully');
        } else {
            return APIResponse::error('Something went wrong! Latitude and Longitude not added');
        }

    }

    /**
     * Web Relays List
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getWebRelays(Request $request)
    {

        $webRelays = WebRelay::where('community_id', auth()->user()->community_id)->get();

        return APIResponse::success($webRelays, 'Success');
    }

    /**
     * Set web relay of guard in Users table
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setGuardWebRelay($webRelayId)
    {
        $webRelay = WebRelay::find($webRelayId);
        if (empty($webRelay)) {
            return APIResponse::error('Web relay does not exist');
        }

        $user = User::find(auth()->user()->id);
        $user->selected_web_relay = $webRelay->id;
        $user->save();

        return APIResponse::success([], 'Success');
    }

    /**
     * web tracking save
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function openWebRelay(Request $request)
    {
        $data = $request->all();

        $validation = [
            'web_relay_id' => 'required',
            'scan_log_id' => 'required'
        ];

        if (isset($data['ignore_scan_log_id'])) {
            unset($validation['scan_log_id']);
        }

        $validator = Validator::make($request->all(), $validation);
//
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $webRelay = WebRelay::find($request->web_relay_id);

        if (empty($webRelay)) {
            return APIResponse::error('Web Relay ID not found');
        }


        $flipFlag = isset($data['ignore_scan_log_id']);

        if ($request->scan_log_id && !$flipFlag) {

            $scanLog = ScanLog::find($request->scan_log_id);

            if (empty($scanLog)) {
                return APIResponse::error('Scan Log ID not found');
            }
        }

        $webRelayTracking = [
            'guard_id' => auth()->user()->id,
            'web_relay_id' => $request->web_relay_id,
            'scan_log_id' => -1
        ];

        if (!isset($data['ignore_scan_log_id'])) {
            $webRelayTracking = WebRelayTracking::create(
                [
                    'guard_id' => auth()->user()->id,
                    'web_relay_id' => $request->web_relay_id,
                    'scan_log_id' => $request->scan_log_id
                ]
            );
        }

        $apiData = [
            "mac_address" => $webRelay->mac_address,
            "status" => "true",
            "key" => $webRelay->secret_key
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'

        ];

        $response = Http::withHeaders($headers)->post($webRelay->url, $apiData);

        $apiDataJSON = json_encode($apiData);
        $headers = json_encode($headers);

        Log::error($apiDataJSON . " " . $headers . " " . $webRelay->url);

        if ($webRelayTracking) {
            return APIResponse::success([], 'web relay tracking saved successfully');
        }

    }


    /**
     * web tracking save
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function openRemoteWebRelay(Request $request)
    {
        $data = $request->all();

        $validation = [
            'web_relay_id' => 'required',
            'scan_log_id' => 'required'
        ];

        if (isset($data['ignore_scan_log_id'])) {
            unset($validation['scan_log_id']);
        }

        $validator = Validator::make($request->all(), $validation);
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $webRelay = WebRelay::find($request->web_relay_id);

        if (empty($webRelay)) {
            //If guard's web relay is not active find Community web relay
            $webRelay = WebRelay::where('community_id', auth()->user()->community_id)->first();

            if (empty($webRelayCheck)) {
                //if not found web not found
                return APIResponse::error('Web Relay ID not found');
            }
        }


        $flipFlag = isset($data['ignore_scan_log_id']);

        if ($request->scan_log_id && !$flipFlag) {

            $scanLog = ScanLog::find($request->scan_log_id);

            if (empty($scanLog)) {
                return APIResponse::error('Scan Log ID not found');
            }
        }

        $webRelayTracking = [
            'guard_id' => $request->remote_guard_id,
            'web_relay_id' => $request->web_relay_id,
            'scan_log_id' => -1
        ];

        if (!isset($data['ignore_scan_log_id'])) {
            $webRelayTracking = RemoteWebRelayTracking::create(
                [
                    'guard_id' => $request->remote_guard_id,
                    'web_relay_id' => $request->web_relay_id,
                    'scan_log_id' => $request->scan_log_id
                ]
            );
        }

        $apiData = [
            "mac_address" => $webRelay->mac_address,
            "status" => "true",
            "key" => $webRelay->secret_key
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'

        ];

        $response = Http::withHeaders($headers)->post($webRelay->url, $apiData);

        $apiDataJSON = json_encode($apiData);
        $headers = json_encode($headers);

        Log::error($apiDataJSON . " " . $headers . " " . $webRelay->url);

        if ($webRelayTracking) {
            return APIResponse::success([], 'web relay tracking saved successfully');
        }

    }

    public function getWebRelayStatus(Request $request)
    {

        $community = auth()->user()->community;
        $webRelayStatus = 0;
        if ($community) {
            $webRelayStatus = auth()->user()->community->web_relay_available;
        }

        return APIResponse::success(['web_relay_status' => $webRelayStatus], 'web relay status checked successfully');

    }

    public function getUserLicenceImageViaKiosk(Request $request)
    {

        $data = $request->all();

        $scanLog = ScanLog::find($request->scan_log_id);
        if (!$scanLog) {
            return APIResponse::error('Scan Log ID not found');
        }
        $remoteGuard = RemoteGuard::where(['community_id' => $scanLog->community_id])->first();

        if (!$remoteGuard) {
            return APIResponse::error('Remote guard not found');
        }

        $remoteGuardSocket = RemoteGuardSocket::where(['remote_guard_id' => $remoteGuard->id])->first();

        $url = "https://devsockets.zuulsystems-security.com/api/v1/guard/guard-camera";
        $apiData = [
            'socket_id' => $remoteGuardSocket->socket_id,
            'scan_log_id' => $scanLog->id,
            'image_type' => $data["image_type"],
            'guard_id'=> $data['guard_id'] ?? null
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Socket-Header-Key' => 'sHHydot6qx8H6GZo',
            'Socket-Secret-Key' => 'e4d1Y8ANRFAjNQ7L',
        ];

        $response = Http::withHeaders($headers)->post($url, ['data' => $apiData]);

        return APIResponse::success(['image' => $response], 'image fetch successflly');
    }

    public function openGateViaKiosk(Request $request)
    {
        $data = $request->all();

        $communityId = auth()->user()->community_id;
        $remoteGuard = RemoteGuard::where(['community_id' => $communityId])->first();

        if (!$remoteGuard) {
            return APIResponse::error('Remote guard not found');
        }

        $remoteGuardSocket = RemoteGuardSocket::where(['remote_guard_id' => $remoteGuard->id])->first();
        $url = "https://devsockets.zuulsystems-security.com/api/v1/guard/open-gate";
        $apiData = [
            'socket_id' => $remoteGuardSocket->socket_id,
            'scan_log_id' => $data["scan_log_id"]
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Socket-Header-Key' => 'sHHydot6qx8H6GZo',
            'Socket-Secret-Key' => 'e4d1Y8ANRFAjNQ7L',
            'X-Upstream-SOCKET-HEADER-KEY' => 'sHHydot6qx8H6GZo',
            'X-Upstream-SOCKET-SECRET-KEY' => 'e4d1Y8ANRFAjNQ7L'
        ];
        $response = Http::withHeaders($headers)->post($url, $apiData);
        return APIResponse::success(["openGate" => $response], "Gate open successfully");
    }
}
