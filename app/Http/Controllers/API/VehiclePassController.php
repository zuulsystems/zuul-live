<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Vehicle;
use App\Models\ScanLog;
use App\Models\StaticScanLog;
use App\Services\PassUserService;
use App\Helpers\APIResponse;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\PassUserCollection;
use Illuminate\Http\Request;
use App\Services\ScanLogService;
use App\Services\VehicleService;
use App\Services\StaticScanLogService;

class VehiclePassController extends Controller
{

    //Will be Initialized by Constructor
    private $passUserService;

    /**
     * Constructor for VehicleController
     *
     * @return Void
     */
    public function __construct(

        PassUserService      $passUserService,
        ScanLogService       $scanLogService,
        VehicleService       $vehicleService,
        StaticScanLogService $staticscanlogservice
    )
    {
        $this->passUserService = $passUserService;
        $this->scanLogService = $scanLogService;
        $this->vehicleService = $vehicleService;
        $this->staticscanlogservice = $staticscanlogservice;

    }

    public function fetchPassVehicle(Request $request)
    {

        // Create Validator
        $validator = Validator::make($request->all(), [
            'pass_user_id' => 'required',
        ]);

        // Show First Error
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        //Pass User
        $result = $this->passUserService->with('vehicle', $request->pass_user_id);

        if ($result['result']) {
            //Return the Appropiriate API response
            return $this->response($result, true);
        } else {
            return APIResponse::error("Something Went Wrong!");
        }

    }

    /**
     * Creates JSON Response for The Resource
     *
     * @param App\Services\PassUserCollection $passUser
     * @param bool $data
     * @return App\Helpers\APIResponse
     */
    private function response($result, $data = true)
    {
        // If Save or Edit is Success
        if ($result['bool']) {
            return $data ? APIResponse::success(new PassUserCollection($result['result']), "Success")
                : APIResponse::success([], "Success");
        }

        //Else Show Error
        APIResponse::error("Something Went Wrong!");
    }

    public function setPassVehicle(Request $request)
    {

        // Create Validator
        $validator = Validator::make($request->all(), [
            'pass_user_id' => 'required|exists:pass_users,id',
            'vehicle_id' => 'required|exists:vehicles,id',
        ]);

        // Show First Error
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        // Update Vehicle ID of User
        $result = $this->passUserService->update(['vehicle_id' => $request->vehicle_id], $request->pass_user_id);

        $scanLog = $this->scanLogService->set_Pass_Vehicle($request->pass_user_id)['result'];

        $scanLog = $scanLog[0];
        if (!empty($scanLog)) {
            $scanLogId = $scanLog->id;

            $vehicle = $this->vehicleService->find($request->vehicle_id)['result'];

            $staticScanLogData = array(
                'license_plate' => $vehicle->license_plate,
                'color' => $vehicle->color,
                'make' => $vehicle->make,
                'model' => $vehicle->model,
                'year' => $vehicle->year,
                'license' => $vehicle->license_plate . " " . $vehicle->color . " " . $vehicle->make . " " . $vehicle->model . " " . $vehicle->year
            );

            $staticScanLog = $this->staticscanlogservice->updateByScanLogId($staticScanLogData, $scanLogId);
        }

        if ($result['bool']) {
            //Add the Vehicle tp the Result
            $result = $this->passUserService->with('vehicle', $request->pass_user_id);
            //Return the Appropiriate API response
            return $this->response($result, true);
        } else {
            return APIResponse::error("Something Went Wrong!");
        }

    }
}
