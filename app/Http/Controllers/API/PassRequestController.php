<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Services\PassRequestService;
use App\Services\NotificationService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class PassRequestController extends Controller
{
    private $passRequestService;
    private $userService;
    private $notificationService;

    /**
     * PassRequestController constructor.
     *
     * @param PassRequestService $passRequestService , $userService, $notificationsService
     * @param UserService $userService
     * @param NotificationService $notificationService
     */
    public function __construct(PassRequestService  $passRequestService,
                                UserService         $userService,
                                NotificationService $notificationService)
    {
        $this->passRequestService = $passRequestService;
        $this->userService = $userService;
        $this->notificationService = $notificationService;

    }

    /**
     * Accept Request a pass request.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function acceptPassRequest(Request $request)
    {
        $passRequest = $this->passRequestService->acceptPassRequest($request->all());
        if ($passRequest['bool']) {
            return APIResponse::success([], 'Request Updated');
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * Reject request a pass request
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function rejectPassRequest(Request $request)
    {
        $passRequest = $this->passRequestService->rejectPassRequest($request->all());
        if ($passRequest['bool']) {
            return APIResponse::success([], 'Request Updated');
        }
        return APIResponse::error('Something went wrong.Please try again');
    }
}
