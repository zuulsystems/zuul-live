<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\IonoHeartbeatLogService;

class IonoHeartbeatLogController extends Controller
{
    private $ionoHeartbeatLogService;

    public function __construct(IonoHeartbeatLogService $ionoHeartbeatLogService)
    {
        $this->ionoHeartbeatLogService = $ionoHeartbeatLogService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $ionoHeartbeatLogData = $request->all();

        $rules = [
            'connected_pi_mac_address' => 'required',
            'status' => 'required'
        ];

        $validator = Validator::make($ionoHeartbeatLogData, $rules);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $this->ionoHeartbeatLogService->create($ionoHeartbeatLogData);

        return APIResponse::success('', 'Iono heart beat log saved successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
