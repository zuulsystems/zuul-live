<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\CameraService;
use App\Models\Camera;
use App\Models\Community;
use App\Models\PassUser;
use App\Models\ConnectedPie;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Helpers\APIResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Exception\DatabaseException;
use Kreait\Firebase\Factory;
use App\Models\ScanLog;
use App\Services\VehicleService;
use App\Services\ScanLogService;
use App\Services\UserService;
use App\Models\StaticScanLog;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Jobs\QrcodeValidate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class CameraController extends Controller
{
    public $cameraService;
    private $vehicleService;

    public function __construct(
        VehicleService $vehicleService,
        CameraService  $cameraService,
        UserService    $userService,
        ScanLogService $scanLogService
    )
    {
        $this->cameraService = $cameraService;
        $this->vehicleService = $vehicleService;
        $this->userService = $userService;
        $this->scanLogService = $scanLogService;
    }

    public function checkCurl()
    {
        header("Content-Type: image/jpeg");
        $url = 'http://admin:myzuulsystem1@107.128.123.204/media/cam0/still.jpg?res=max';

        $image = checkCurlHelper($url);

        $imagename = "Violation-" . strtotime('now') . ".jpg";
        if (Storage::disk('local')->put('images/tickets/' . $imagename, $image)) {
        }

    }

    public function guardFrontCameraSnapshot(Request $request)
    {


        $user_id = auth()->user()->id;
        $scanlog_id = $request->scanlog_id;

        $cam = $this->cameraService->find($request->camera_id)['result'];
        header("Content-Type: image/jpeg");
        $url = $request->static_stream_url;
        $image = callCurl($url);


        $imagename = "Violation-" . strtotime('now') . ".jpg";
        if (Storage::disk('s3')->put('images/tickets/' . $imagename, $image)) {
            $data = [
                'license_image' => $imagename,
            ];


            if ($this->userService->find($this->scanLogService->find($scanlog_id)['result']->pass_user_id)['result']->update($data)) {
                $data = [
                    'static_url' => $imagename,
                ];
                if ($this->cameraService->all()['result']->where('id', $request->camera_id)->update($data)) {
                    $data = [
                        'license_image' => $imagename,
                    ];
                    if ($this->scanLogService->find($scanlog_id)['result']->update($data)) {
                        return APIResponse::success(['url' => getS3Image('images/tickets/' . $imagename)], 'Capture Screenshot');
                    }
                }
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function fetchStream(Request $request)
    {
        $validation = [
            'guard_id' => 'required',
            'camera_id' => 'required',
        ];
        $messages = [
            'guard_id.required' => 'Please enter Guard ID',
            'camera_id.required' => 'Please enter Camera ID',
        ];

        $validator = Validator::make($request->all(), $validation, $messages);

        // Show First Error
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }


        $cam = $this->cameraService->find($request->camera_id)['result'];
        $user = $this->userService->find($request->guard_id)['result'];


        if ($cam && $user) {
            if ($cam->community_id != $user->community_id) {
                return APIResponse::error(['you are not in the correct community']);
            }
        } else {
            return APIResponse::error(['Camera or Guard not found']);
        }

        $result = [
            'url' => $cam->fetch_url
        ];
        return APIResponse::success($result, 'Fetch Stream URL');
    }

    public function snapImage(Request $request)
    {
        $data = ['https://timetracker.reavertech.com/images/web-logo.png', 'https://cdn.pixabay.com/photo/2015/03/04/22/35/head-659652_1280.png',
            'https://www.w3schools.com/images/htmlvideoad_footer.png'];
        $result = [
            'url' => $data[rand(0, 2)]
        ];
        return APIResponse::success($result, 'Fetch Stream URL');
    }

    public function updateCamImg(Request $request)
    {
        $uid = $request->uid;
        $mac_address = $request->cam_address;

        $factory_new = (new Factory)->withServiceAccount(public_path(env('FIREBASE_PATH')))->withDatabaseUri(env('FIREBASE_DATABASE'));
        $realtimeDatabase_new = $factory_new->createDatabase();

        $reference = $realtimeDatabase_new->getReference($uid); //.'/'.$mac_address
        $new_ref = $reference->getValue();
        if (!isset($new_ref[$mac_address])) {
            return APIResponse::error('Mac Address Not Match');
        }
        $check_condition = $new_ref[$mac_address];

        if (!array_key_exists("ImageURL", $check_condition)) {
        } else {
            $reference = $realtimeDatabase_new->getReference($uid);
            if (isset($new_ref['capture_request']['scanlog_id'])) {
                ScanLog::where('id', $new_ref['capture_request']['scanlog_id'])->update(
                    [
                        'license_plate_image' => $check_condition['ImageURL'],
                        'log_text' => 'QRCode Verified',
                        'is_success' => '1'
                    ]);


                StaticScanLog::where('scan_log_id', $new_ref['capture_request']['scanlog_id'])->update(
                    [
                        'license_plate' => $check_condition['plateNum']
                    ]);
            }
        }
        return APIResponse::success('Successfully Update');
    }

    public function activeCameraList(Request $request)
    {

        $result = [
            'doorbird' => true,
            'lpr' => true,
            'non-lpr' => true,
        ];
        return APIResponse::success($result, 'Camera List');
    }

    public function getCameraCount(Request $request)
    {
        $communityId = auth()->user()->community_id;
        $device_type = 'device_type';
        $values = ['doorbird', 'lpr', 'non_lpr'];
        $result = [
            'doorbird' => $this->cameraService->findByCameraCount($device_type, $values[0], $communityId)['result'],
            'lpr' => $this->cameraService->findByCameraCount($device_type, $values[1], $communityId)['result'],
            'non-lpr' => $this->cameraService->findByCameraCount($device_type, $values[2], $communityId)['result'],
        ];
        return APIResponse::success($result, 'Camera List');
    }

    public function getDoorbirdCameraInfo(Request $request)
    {

        $factory_new = (new Factory)->withServiceAccount(public_path(env('FIREBASE_PATH')))->withDatabaseUri(env('FIREBASE_DATABASE'));
        $realtimeDatabase_new = $factory_new->createDatabase();

        $aa = ConnectedPie::where('id', $request->connected_pi_id)->first()->mac_address;

        $reference = $realtimeDatabase_new->getReference("{$aa}/{$request->mac_address}");

        if (isset($reference->getValue()['ImageURL'])) {
            $this->scanLogService->find('id', $request->scan_log_id)->update(
                [
                    'license_image' => $reference->getValue()['ImageURL'],
                    'is_success' => '1'
                ]);
        }

        return APIResponse::success($reference->getValue(), "Doorbird cam object");
    }

    public function setDoorbirdCameraInitiate()
    {
        $doorbird_cam_id = auth()->user()->doorbird_cam_id;
        $camera = $this->cameraService->find($doorbird_cam_id)['result'];
        try {
            $factory = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
            $realtimeDatabase = $factory->createDatabase();


            $realtimeDatabase->getReference("100000002af993d7")->set([
                $camera->mac_address => [
                    'description' => $camera->name,
                ],
                'capture_request' => [
                    'mac_address' => $camera->mac_address,
                    'status' => 'true',
                ],
            ]);

            return ['bool' => true, 'message' => 'Success', 'result' => ['mac_address' => $camera->mac_address]];

        } catch (Exception $exception) {
            return ['bool' => false, 'message' => 'Something Went Wrong, Please try again with Firebase connection'];
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $communityId = auth()->user()->community_id;
        $lat = $request->lat;
        $lon = $request->lng;
        $type = $request->type;

        if ($type == 'doorbird') {
            $query_merge = "and (device_type = 'doorbird')";
        } else {
            $query_merge = "and (device_type = 'lpr' or device_type = 'non_lpr')";
        }
        $distance = 10; // 10 meter
        if ($communityId != null) {

            $result = DB::select("SELECT  `id`,`lat`,`long`,`mac_address`,`name` FROM `cameras`  where deleted_at is null and community_id = $communityId $query_merge");
        } else {

            $result = [[
                'id' => '-1',
                'lat' => $lat,
                'long' => $lon,
                'mac_address' => '00:00:00:00:00:00',
                'name' => 'Default Camera',
            ]];
            return APIResponse::success($result, "Your community does'nt have any camera list");
        }
        if ($result != null) {
            return APIResponse::success($result, 'List of cameras');
        } else {
            if ($type == 'doorbird') {
                return APIResponse::success([], "Your community does'nt have any camera list");
            } else {

                $result = [[
                    'id' => '-1',
                    'lat' => $lat,
                    'long' => $lon,
                    'mac_address' => '00:00:00:00:00:00',
                    'name' => 'Default Camera',
                ]];
                return APIResponse::success($result, "Your community does'nt have any camera list");
            }
        }
    }

    public function captureComInitialize(Request $request)
    {
        $img = "data:image/gif;base64,R0lGODlh+gDIAPcAAGVYSsSohnh0ZiM2OZmEab2khBEQEcR6XE13hEVCOVlSSG6Ki7p1V56bh6qTdUs4LGZ3eJiIdRowNiMkI6qciv+uTdXPyo1RNUhqdlNMQ2VbUqSJaYVqVItaRrOaeyc7Qri0nIpyWMyUdHFLLvimNpd9YtakdPSRTXZZSlh0evOtbTRMVHNtYUxdYTc7OZR3XGtrYmJhW0xiZ0NMSzZRWUtSUahkSP/MXWNkYIqGdsqFVdSYdlNiY+i0jFtkYnRjSqWNcrqyqLR7YVRsdExJQLhrVFpqanRpW417alNbW8W8stPPtkxRSlthXTtBO2tkWipIVDMiF2xrXXpsXDpLTIdhTMvHrrSqlmtiUnV6d6qll1xdXMWylSpCSIR0Y83FvryEZa6HVNfTukkrGaVbRGJsa82MazpCQtung1tbUktNSScYEUxZXOjj34qZk3NkVJedk8iOcDFAQmNpY8SBYteJZMWLa5RnUrNlTKh0WlJUUWpfTFRIOe2YY+7p4zJESpFjOGdxb5lhRkRRUktwfcuTadaXZ//INUtVV1FlaTMqI5qjl3tqU0JFQYx5Y3JlWtOcgM2EZKl3SbiefUBNUzVUYKlrUIV8ckJUWYKUj4iLg+Lc2OrNrURJQ3ddT7hzTaihj7WUaLyifVJpbDJJTbCWdF2Ah1NZUsyFamNhVOB0KLOadURaXmk5JsWGaHRpVGpsaaFTPMuvi9i4iox0YjlFSnqCfnxhUj4xJ4R5arGWeWI0GlxpZJiOg355aIg+KMWKY5lxRKpkNHpxYMuMY+C2mUxFP9qnkVxBKnplWFxlZ19tcdOPcZZIHcGtkmJPQ/TlvB8rLRkaG4VsXunJnKyPbOm7pGllXjtJRT9ic1xVTp6QelVQPmVpXVJVWZdtYExZVXZCIpGCboJ1a7OKbz8dDJ2BYCMuLR4+UJ2poFVpZWRlZZF+cLycdDxRUGJcXm1pWEFFSTRFRV5vcHRQPn9cOHBxbUlFRt+ffkFZWVxVVryUb8GTd4p1aaw/FIpvUCH5BAAAAAAALAAAAAD6AMgAAAj/AMUIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePEZeIHEmypMmTKFOqXMmypcuXMGPKnEkzJsWRILTk2Mmzp8+fQIMKHUq0qNGjSJMqXcq0KVEtIKxYGflQJIgMBrJKM7C1K9eta75KG0t2LNezXtOeRctWrdu2aMvKnUu3rt27dCdI0zuhr1++ZfXeFYy3sOGya8YmXiyNsWOyWtVckSpy4UgiWSco2swZlyLPY3CJHk269GfO5xSdWx2ttevXEqJJmE27tu3ZAyTkHsB7wIcPXbqQ+kOq+PDhf5In77L8T/Dn0KEnh0KcuPHiK7K7274dUz5WrGS0/xgPvnyl8+hpVKLBPrt69PChoJtPnz7w58nl6Zcnx8kZJ04kEGCACRRo4AN8IKhgAsg80OADEEYY4WiKhNWIEpSJscRBIimhiAF+qcbZaZ6VZmKJJW6W2jkTsMjaOa/JJmNsNN5WW2666cYbOr4xZ9wKVAS5wnXHKfcHf13IgR9zzCVH5I9UABmlOytQ4g4mV37XAivjjScDePmsx957Y9LgHnrZoAeFfPWh8xs60SnH35xn/AeggAAS+IAxe+6JDIMK8vmghBKSJo0ik021oUFLWKHIXopoFumIJFJo4mcpqqhaatGsBmOMM8pm46gSfCABOqeWeh92VLgzpZBUkP8Sa6xGNsekk1BmF+WuVHJHCZZWYuIdeF2WB955Y4opZnbrncdKfGzW94Gbbt7XpH5y8Oefnf7dSSCgCQrKp4OEEoqLCw+cK9peIGC4KEEiMQFiX6ppllpn+KKI4mn8iujpi7C1RqOopNLGm466veljdpTQsN2vVrpDCawr1EKKPModZ9yuQK5AZcMSc9cdDcJiKWx5W5K3ZT6YiMkessu+B9950c43LbVvWhucHDz3XCe3dybwQAJ8jgvu0BCOWy6EuDCNizQJtFsZvCDMy+KklOJLYr7+brppi/8GPBvBBeOWG6pnD+BmcFCQsoLDNAyCSdwmd+eqlLLGOmSUHsP/3fCvV3KHya8kl2z4dygn/myyM6v38syVrNmmzdNW/htwSvKcZM/9dXsn0gUmLXqEQiMtoQvnpou6aAY0ENXUGi6hAIiTataipFlTWuK9Xfv7r6euxUZ22bShfapvarPttjs0AI6J3IYbPkjzvbYKN8lXmixs4dofLizLiBsrfpiOy9z44+ihU7PN7FvexQdKwv8b5z3naSCfQucPof77R4hu6upS3dOMcQV3DaRRY+kLi+ilKdQ48By4WJFn7rWi3/1ObMQzWI5ShSpqwalt2THZr6KHCTCRsGT5SGEKvfO9E7YQfCX8TvjEV54wze1xjlOW+SS3vvZVCzjwe58c/+YHP/rV6VsGOpDQ0LXEB6ALXU6M4hOj6BlpgOJ1B6ya7TZjO3v1jncr0lQYx2jBTwlseKTCUY5QNRtUTYtt7pnb9qKHCBqu8Dts4JIejZVCVrBQhTMETwxpSEhkvax86VFT5CTXpptVrlrxA+IQ5Uc/OdjJQATKZIHQ5YQHdFJonXSiC544RdS1TmoDscIVrHa7q9ErNV4MY9cquJpN0RJgM8ogjs5WvLOtbU1vY4/hsuedOuaRDVsqTx67JJ4upQw8eYQmK5CpMkJWk48xC2YOt0mzHrbvckJ8Hzif07PNWRJoebLfgBLgAnZ2spMuCFA7A+TJoaHLAAKAyqIa1f+AedUuUiuy1xi9RsZaGrSM5xhYjYi3Gw12UGHAFCbJHGY4RORjmc50pgx4sNGOcjSjKgNpl6JprGcmk3yVcE+yZCYzKFRCfY20j5smKc73CTGIzOEP537GU542Yp3tZKdQRykgPIUuAbjAJ1SsIBB+zusvC2wRQW1JUE/VC6EXVOhCC8bGDrbxVNMC5tuYpz1iYoIN1GyBDMTD1hZwNBEySEQieEDXuro1ozxQ60bVWs1iNTOZz3IZFFiqLGh5M2fhvNxvkrSzLsjDsTzT6X7OII9aVBYbjcDsGX46oHZ6dp1INJABWHBFpmrICv0E2+0A6iKpWvWgt3wRQmM0Nl3/Fq82aHsjKaDgnoYJy24lQ2uX6OrRuMp1FHKVKw/mqo66Opeuc63rX8nzV7Yey5BmOmQOm0UzmE5ubYsNL04zJ4f8JId++8FGLbDB3vZu1lt5iid8k1ig0brOtE71Cyxb6yLf1bJTtrwgVs9Y262OipccPN584JidKjGPcNBDBFqRKd3lJlcd6jCCOhLR3A77AMMcVsaGlZEIEkcXrnxdq16bmY3risml2C1TNxkprR+G06bkbNJ5j1TeHh/psqRoLzbioVnOptNb9E0AH+oLj/s2VQmpjUZfOtUiKv/Od6rpVKdki1UzLhSNX8URGzmYtsW2zW1Vah7ITCY3CbMh/wlJqGuJM2yEOts5w+oQMYZ5kWc641kZRhAxiaEb17XC1botblx6eBvMZq3p0QumXLVqGhzgOKdWGKtFj/dTWXkIGRtnALUTsOGfRhhZnUmuLwucfNoouwZsrVkN2BQBYOAJeMuzVajAyma8VDWUR2rTLQinRImISex5bo4zXfVc53kYYRnzWIYRnF3nPNv52tjGdnK3Xei1BvY8K4APDQbrUpey59HySXcX4LS298Gp0uSUE8aOVAvz6qcW6w1Se4kc6sz+tBM/DXgCGpGAThScCEpmMqud2poJgOpTwAswl2E0cQnYOlS0AfOYcWMwte2IN4sVjtv4JjGSFXsQg/9ABDjQmoQm8MAHygD0s+dB82iXweZ2LsO0a/5sO9dc2nbm9qGzkWiapXTRa2rPudGtbjhVDt7S0XF+LFtvjGUa3+ydwacb0W9sdMIJpga7qQtuIIQTLQH2jUpTUQuihscIeLi24JZj3SmLW/xfdi+wbY3HI7R5/APBDs6PrET4YlNiEDVAhOLfrGwRl6EM0A4EzSU/j5tXfhmWr/zPa675ZUh7FKDntrfhE24YP7rBpV8Butn2nHfjmLFGsjqnqV7ZP1i2vVpnbycwa+rMih3sYh94gYiAcD4g3BgGaLLaT9sLA7gdVFruspbPmFBP2X01wrONxr9qNh193DeAD3n/cfhGhcKfXPEqfzMPXA5zI9xc8vCfRyAeT/P3c17+k+e85+s8CnUgV65rRXSKZHrjpnpiVYBMh25/4HqMFRyxJw/DgQ0QqF6eVlm3J2SdoHW752+91wgAJ3yNQASdQHwIV3xot2rL51TS8HwPF3cWJDwCU313h30ymHcFo0a8BHi9AXLI026sQglVUn6Gl3IqhwhJ8GYuF3PuN3kQEAhNGAiUB4VQ2IRP+IT4h3nOJm2JAHpvtVZfIm6RY4AJyFuP1gUK+AdQwBxpeGkPWAtUsF7YICtxGIeeBoe4l4Ez0Al6qIe9B3AeaHAjmADEZ3BEYHx8gHzKh19sJ2XQF30U//c7Fld3dZdQs0GJ55AblphBObIbO8iD4Qd+PaJjIxeEQohyirdycPZyMhcIyxB/TiiFUggBVEiFr0h51GZnFnZo4kF03xY5RkeGY5iGaWiG1FGMtVIdF7Ne6xVkQdKM2EAF6gWN2JCHM1CNGbiH2DiC2iiCJEiCSlaIfJB2ithPjYhrjviC2Dc8CYUjlKhBanQjqYIwvfGJoHg54OeAx7ErQnh4iQcOEpaKPsAL7gd/r/iE8EeF9gABWSCLDBl/NGdnypCLXvgl32Ymvvhoi8R0aGiGZkgcUpcxpGAxbthe0KhvJfmMb/iM1ViNatAJLfmSI9iNMTmIJGiI4BiOUv+wcFbQfA73GhTngp4yANUng+1od5i4S+sIj7exS97nicjzG+CnWA7oJEHihieHeIqXijyghO/HigdpkFOYkAu5kA1Jc0AXaMuVlm1VdAO4Hqonhqu3JsS4W8hoHbOSN/qmdVQwA84ojey1lzPABGrABIGpBoapBtyYmN1YiOAIjtzADeGIguPofK7RZTO4GkKZmbSRlA2lG+3YUDgYj5zYicgTlYp1mtJhe/hGCTNwlYmnbKq4hF4pf2AJi1Q4lmBJbYPmXG0lSOmhHuH2lhnJdBy5JtdhHRuDl0FSjSq5l83YjHzJkoIpmNygBtxABNWZncR3ndz5mN7JB975mMn/p5PkWJldhpmXmJ6aiYOX6JkbBI9d5VBrxIP1aJqnGV5TiW/xAJhYmYpN0H6Qh3+xCIuwmJBimZBWWG10NR559VfH0jIxph6M1jYJSIxndh2xMnLN6CqAuZIe6pwdygSdwAQkSqLcwAQnyg0ZEJ4s2qIsKo5rV55ASYPWp57tyZShiaObiFtfxUa8VJqfCJWTdJ/gpCTJgW9uGA8zMAjgAGdx1gTKwAv0VwYESaAEqpBkaYXS1lx5xQZ15EyCtFLZpU0NRpyP9iTY0TFB4iruMANt6qFw2qHVWKIpaqIueqcKkKcKwA17+gyzk4hr13yxkY5bdpk2yhuXCJqb2BuL/8pxa8Sj87mDUAl4cmCfRDo/SfIH2aKfe5lyTRpnPgBzUkp/81elVuqEEJCQUMhzC9oC4IBy0MMyfnRDiGQmDVaAxaGRxvkjsiIlbOqmg+ChhEmYcMqSgVmiCoCij7mnfLqnAKAAANCs3PCs0FqtzwoAAACjreZ8NBqUh6qeiHowjJqZpLmonelVCfZxPBJ+l1OpQ+QCl2pTPnYG+FaNWPmp/+kDc/B4/Pp4pnqqTfhzaNmlrDAIhnewg6AP74AIZFomDbZbqrdbEjuxaIaX3LGkgzCsgRmYGbuxwkqnyRqye2qt11qy2HqyKHuyBpCTKch2Q4meQnmo4ZqoiTqupP8prpxoKuIqj9/niZTarr8Br+K1JOVFr1nHmlh5CnAmqvtKpfwqf6e6qpJneXUWXS0wYc9zsIaHAt/wDe+QXWPyNqj3lhRKsWrKK4PgDhm7tkzgDiVaoh1rmMNaonrADafAp9NKsti6B3uLrWnAt3vAt3sbuNrKcOn5O+QqszXLszfbuDtCZk0Jcvb4s/AzAO4atCE3RJuTHEY7jfZaAzXwqT6AA7wwB03rr6fKr553ixqWZ3l1tZiACM9jsMUmMYOAAkVQB0JwC5gwVtthqw8bsbyVpvroKyjHBDXwtsq7vNOJrNxQtwqQBtg6rQAAuIF7vdV7vakQuNubCliABSv/q5PN97KZiZ41G66O27g6SJ+gmRumAmymabmgeLk8Iz+UxFjzWgtERmT2qgY1oAdK+5+luw7+SqWpy68CCWjWNlfkIbvCgnKHl7aDow9FcAJ1QAfTUAMeYyUOM7bZQaFDgmYe0ypqy7bgwATgALrIS6J6wASncAoAzAR1i6IoegrOOq3Ua731sAc/cL0+fL3fuwdY8AbwULiLaHHlm7joe744m77s+3GA53fzOKmm+a5FZMXwmjmMNVlYp6R4OAP+C8At95/rcLoH/HgCGWhcSmI+0KVs8MCIAKsol7V6cAcncAJ9EAnTYGwcTAW2imZnpiskvB3Hm7wnHMMw3MI2//zCCmDDesoEesqn0TrJg9vDPLzDPfy9mrzJ8IAFr/AK4duy/WS+SlzKTiypT/nEpemU7EpE8iO0WewCQzTLWtwzFrheRJZ1YDwD/xvAoboOpYu6BIrGvBClEdnGzjUeeaR4KVcDBivHlFADt1AHfYDHdZAMI5S2xga8Q6IrQELIa3vCh/zCabDI5lzOaVDOjRy9OJwG1IuyfLvDmPwD9DzEQ/wG+DzE8LDPoCyZMep8SEyz5DrQi+vEQVqf64vKUVnFlMQ58CPLlcQz9BpqXey5LAm63jDGoRqljwcLUOjRCBylyExXSXBXyuylzBzHKGevKFcDTyAE1VwBfeAKev+QcnIjN73CPBzTKiTc0ioMDi98CumMzuUs1NGbBuyM1JMMre88uJjMw/Qc1T+gyfj8CK8AD1f9CiwQyorYfEmMmTOLvgOdvjoIr5KbyqnMrvVYubTMMxAN0RH9B3Win/pbC52gpCv5v96gtFuQrwHZ0WUAC/wKzD7gci4HZy1Q0iU9YejnzDYdrJ0AujWgDzB9AhUg0yFQhCr9PA4jMQ5mJWmLeD8d1I081OmsDaYNAEO9B+l81JScw9m7Bygg1VKNz2/wCrdt1VZ9BEdgxOQIszELrqfciZOqg4DnAvWZ1k/pyj1zxW7NORDdUz9TC2cQD/vroaCreKeQ0WP8n6b/+3hNuw6hetg8cIQT9mZodYqK58yOrQaIB8ZMoA/f0AcxrQJ08MJJoNk4/TBXMseincIvDMBHLb2BS+B/mwrS687Syw18S71NXb3yDNW0/QO2/Qq7/QiPcARTsNUsO5mHG7OeKa4C7bjGPQBmDa/IfdBQmdbt6htw3TNwLcvRzVPyQFnTrb9DNmR5jQh6AA577aRN4N1lXAamW9hN4KT57Y8StnJssHLrjQiSPQj+649grAZ1XAeXfdmG8AT4/cLg8KrQ8zz5MAj5kHihG9RCPdQA0L3c68N/+7drztqBq9qUXL2xPeG1/cm7/QpHoNVT4NsAXX2JG+I3G7lljTwp/47oH4DcoLjc4Oeu9SsHL/7cEi3R/EFZ5/Qz+6vjFy3Z4CDGSbAFfT26+qoM4h3kTsoG3qDk6FeEq87j/yvZMuzjJ2yYAMAAll0BN9AHyXAKW5AGSVDOr36K/pgP/vjlQb3a2yvEQowFUx3E1/u3BR7bsX299DzbUY3tUv3Jb5AMfK7hUzAMw8DVgWoANKqoNyqPTVnWQWvi7o7cZu3ulLrQQBvL9cMzTtAf+l7p0u1TQxYPX3yYoKsHiPDjcBbkTYAD443qcMbqUE6Eja0HsS7ZALzX260HeqAAanALqmDZh6AC35DmIh/UTP7lJk/O6ZwKy44F8HDbP/DJL/8Dnf+svT9c8/Qs4T2c87T9yRb+ybwd7uIO6LJxo+NqrpFbn/D+7u++6K88707f0BFd6T5TJ5neU/FQ3daNhy5pmJI98Pgd6n2N8EG+BQ1fhGYO5RSf9hKP8T6utImsABpwBwfQ8SRQAXTgCen863kP7EmgtE6qtOncBGnQvZ7M84xw+Ij/Coww1T+MBTz8+Hgu1dpOzy7P833+80BP7sxnAJgY4gjTnt6nI+vLrvA+SqW/6Ek/y1nM9Kwf0fluSdlyBucU+z1ValjfCAAP8Lvsv1z/v3pg8XAW9n1N9g3P6pIN612/9gOP8du9196QBnHPAHVwAqqgCnXfB3mgAems/b//juCmnQaCnwpboPLeO8SfjPiHzwHozwgw7+yaPNWRT9uQv+343PO6reFB78/bisQ2CxDnBkgYOJAgwQEJB3wY4KKhC4gRJUqU4+KDRTkXP2TEKMfjx4/y5Mk5I6/kGZQpzzRy0mjlmXiN4nXqNKOTGpw5a6ipoaeGt1PekmxpssVoEqTewHlDVGPQ0xpRpfaMqseq1aCngmpIlsdVnxMnVKmKdILEiUjTNKTRkCpNqrZp5KZpkipV3VRYsMB71XcKo2mA/3EIwYgDo3+MFL/6geXHj1d7Hr95/JhR5cp7UEiu/OYNFs+eHz06MmXKMAMsGoCwIkbMEisNDJyTgFDh/+2DuW83ZOhw4m8XFS0GH76xI0jkI5WrVOkSZSOXMeNhqzlDjXWcO3n21AMUaZKi35OcAtcU0SCn6af6vHrVW5qg75IJMaSiQoWwJ+qoOoG/zje3ArRrQALtggc0vo5gZIp/hgmhwRAejDCxwRZjpK/JMLsMM8z22MMTyUL7bMRXRittitRWa+212Aw4KCHbCsqttt0Yakg44CISbkeIdrwIOTmc8Ei5kZhjziXo4lESm3hmoGIG7LbbriqsTkFqi6HGSwKR8ppCD72pevKmu/a08uadacDow777zhKLv/xOyAMLu9rK604C9YKHr1dOPC3CCSX8R9DEFFMMMg4fu/+FQw89/CHExj4DbTTSTjRAChVdg60XA2TczdMXabxNjodccAKiB1xIAKJTTWW1R1iDA3JIIk0yEqV4VoquESZnwAZKKNHTaUru9DhFn2O1VIrLqL6UigkqT9EDWT20kVYPLISw74YKDiHhWzcPiLOOPGLI8wks0MXikb326qvP06bgwEEHAbWXg3/yzfewQy9MlNFHPdRrxIHfGK3EI0pLkTVNW4zxUxolQEdiCWzsbaJVH3AiAY01PtUJkEN29dSKZjVJDlttZQ4bXZM8gzrqgJ3hqUGYyG47aIvtLiigumyqWSZq3g6rnY+1SoNvDKngBm8PqeDb/FQRN6w6dFD/y9xUntD6DXi4fqNEsI94xbRhhvEihLPtBXTQB/U19LINGfv3jUYbHRg00Uir9LRLM2VRtk9rpNigTy+OKAEnHkhgccYRdzzkM0wNEiQhhzSylpVhkgk6mXr9VeaZZ3CKp5yI9QkRPbhk1kunBslOTGON1adaDTgIZelun9bdzToigbMOcrF4x9wY0n2Er0f6VF7sE4cxzQuz0VYbUMIGNextRH+gDGCB98ACRO/3eOMJgyttfuEVYZMNod4WgnhidBJiyHBTOW5c8cbzD9mJM0gWsnKSNOdIKckV5zb3uerIjBKDiJIaXOe6MNUAERM0T7OcYjOpFE0rp9DGLcCg/wKmtYkEu8vPAVRRBzcx4BZpeEcqWhiDJ8CDNPA60dgSZprTRC9t04sQIwrztsswZnscckz4sGC3uukFYTgsW98Y9holAC4hDunN/BISv8LJTyIby1/+jMG4RiwOcikJoMo2x6tdpQRJSZLJTJTkK9CFzoFzhODrpgIOCVoQPTkrlrSOdQtzgPAQThthf0Z4nziJqwJ90AEH5JIK4sFQCgkT2xSYZ0lLsoBsZTtbJ3now7cxpi+UoYxjShmwI9ZNld7DArw2ib6GSZGKFtGIFa/ovvkNx1RcZNwXv8gHYwQzmIvjHP9upcaYbC4mukqmTDTXiTZ2okm/+pzMrmMdMP8Rq1k/Sw+Y1qMzfbzAEEwbJNT6cx9EgqUOJuxDBepwhzTEQAMxoCcMryEFFhxBkzfUZD77mUMvBDSg0pPeMBhhUNO8qy960UtjvCepVHpIA3uYKEWTuK6xnWYaTkxfi6jovuLgUqRW/IBxPtZLxvEhAcFUqTATEEaWOEemzaRJI2oqHSQ106ZulKY04wi66+zEWerxph4jeJUaeAIMNyDnCAt5SHSq4AQ6+MQB6rDIb2yBnpCk5z3vyQKwhlWsYy2b2cwKvRDQqzDOY15fDsRQuDaGoRbdQxoAcNc9AECVSiSNaWD5t05dhCIYWYgtS8oQjpSMl4vzJUuFCcyVvtT/gJPlHE0se9meSlOZMqmJZX8KLJyITnTO+tLPztM6C65HD0zwRCHIOUjdnROdFVBBHRjwjSLU4QR9+MZbugrDJ9wTBvgca3HHKgDkcrKsZVvQFJTHl7ceUbqsnK6H8AqANORVA3q9aCv7xFFNRdEAvtEIcWiJy/kddgCUC1njHMsHIgDTGEQgQjAbYQzoWLaymKXJPTrb05kA2LIzsclnr3Ng0XUzjxOU4AShMlQq1QAL4xxkhduETm7RtpEa8MQdIiEERwI3Btcg8TUeMVwYpNi4KwarAMoWL+c5z5IJg+4r3rqHVAgMrnVLBXbrele9YhcAFaUuFv6qvk4BxyO1/zzsYY8TpPYy1rHzhe98g0mEBNC3E0TohDEwy2Uuq+EmYiZzgT2bwAT+dI44Qc9ot9lg89DMKeZpylX0QYzXllN3s6XtATyhD7Z8TwPDg6E9STxJGLAg0YsGK6PFCgsW2EPSkRbrFDT5CinY+Ah7goe6BoTjHR/RLXnNMQB6nFe7AjnHdQMvYMvLKpAFp5a0pOVGPCKk/rWkl4+lL3zrS19gA3vLwwYzTYighmOH9ppQqsk9ruPfz2IzSkIFE3ooOMHyDOI82gYHONhAnqWkTggVtrDT+HwfP2uDLWt5B1sKfY3gXgMG8k6xoxUd1kSzANKwgAGk7aFvFYd1klKQAv88Cs7pvRCoyHppVKnrimogC/nh2QXAkVv0kIrsb8mk8oisSjYkkPUPplcGpq99HWyU0xfZyNbyynEy5msqO0rW+elTroNBmzmwm9e+9lMmiBQeiCcJ4JiGCipMznPfp7a30IY2NADotswTkvHW2omPkG98F/ffkE4xv/XtdUjjk+BjPzinn4AnuO44x3bNLsXzCuQfAzkNrUZyRPYnssn1CEi4RomuhUlfKhsjAyf3dQboa/gMrPzYi3d5TmiOYMjHkYEOFB1PmMDHnTM4Hw6eIBuA3gQeNOE7WwCG0ml7H3PzuQ930Aag1b0W2GdNa8EljT/1ueJ/g5Xf/e761u//rejhSuGe3djTNfbUjW7QKS9pxzGP8/rwIL9dyKm2uGzsvjGYavzu//tf/0T+0l8DMwNUNvzhiZAB9Kcf/SxnPE6SDSXIH9jxMmOg6xq4EyboQZvaxoTqsO35JAg9AQSPJHiEdjo9FWATPktAPzuFNNAHDYjACHwL2aOUR7A9sLI0rdO9euu6ftM3Sls0KegG4TM7eEg+5buTdqEThlu15vOQU0s1veKGPeCGisOUJ0KyjAMZYmKJloAckJGHIDQm/mkJ6LAyYIOv9OMD9WtC9OMGNUi80kEw+LOmYKEj0Hkg0sG//IMWzKMzLmEDcPiOJvABZfABNETDLTCHPmhD/zc8wLBYk/vogzzQAA5ii3WDC72oOvOhpNK4Pd0bq3rbvXqTgtwTgEaDARKEB+OLIa2Bh1SAxLSTrrnKCxhcJet6u7yqvvHapZf6ROiQrGJiCe8rRSfABpu6siQcPFZ0QvRjggyAxZxIttCqwmh7imAJHQbaiajAGfZYLarIo24TCqRogjLkBV5QBmXghTRsAiGogz4AHmlsQ/1opwRUASHQB3Vbt0Gzi9kzET/UJ0pKBj80Lq4rRKyLNLEjOOPbk0dAlwOJrkmEK3iQDMeorky8roqDB79ZAvHyRMnCr/vCslAspufou5iyqWArPFd8QodUgFiURSqMtlysP1y0SP/M60KrUID8QypEyAorMcYmwAFlmAOTTMYzNMNvYAAhOAAhoANUQAV1gsY1SUAwsMO1kIuoY6gnMBG9OQLSIMdk6MOEOS6BQzFBZAGxkyFO45p2eSu+cJcD+Zp7fAVTGqLHUKVH4cRdwj788rJiIwKbeqkfJMWEbISUOz/1g8j044b0g0WOjMX5o8iZqb+6pBmawTyf6EgFaI8yEQqiGEkfMMkyKMw5MIJkZMZrSIZp+IY8EAJmeEOavEZUuAWoy0O4mL11oZRkMJhkEMqEAceiPA1EHAaxIi7jKkobS56uaU3o4hq3srFX+Jp3yZ5/4YzHMAB+zEGP4sGXGib6yjL/YDMgI4wpXSM2YFO/Z3BFtoxFjry8uYyjZZuBBVogXZSzqqASvywTY9GKofCBGOCFOViHMpgD8VxGZDRDH2gC+fgGIUAFfEjAN7xGZpgGB9RJPYQLDUAX8nkEz/jMrxlKoBzQvcEhFjBN01wxS0vQGaMkheoTsZHNB32FCwGiH9iQf/kBrtwYLlLF+Qq2BOiE4jzLI9wy83NIt2xIWLy852QC0HmSz6o/d6CE66wZ7dxOBeCgDdIK+NgCusABHDBJk1wHZEzMZfQBHkhSH1iHb/gGZkhA2rrGG1AB3kKWNPDRLYALecpM8vnPz/xM/9Qb/xRKJlouBF2uDCwbTYoX/xxSnrFZnneJ0L4AorfhF0aogjvNU93sx97kQV/6tZSjLAN6KRNVy4ZU0YiETpmB0Rlwh2hzh0FYILyUiqa4Fh01FmuZC7lwQK2KARxYByE1SR8wUl5QB2VI0iRNhHcYhzyIT/sAIaZSAXz4Bg1IgiuViyy1E9nzzC9NhlfozDD91WTAoecx02FwseUiG2KVsYRyLoVy1r8YmwoFIg44jGq9Vmy9U650HClLOW7QsrG8L3EN0Q890QxIUfQz1LeMxUR1UQYCliehAkfNQgZyVEeFCkolGj9qukzV1LnQKiANVWZUT3XwAXUo2ETgASRN0i2ABW1hKoiNVSF4h6HAUv8ByYuqE9Ze7cw3+EwFIdZp8IKQDSgBgJ6AQlNlZVYcWoxobVlp/YsFWRBrxReaHYzEqIJ/qAI83VPetD7fvK+/S0stwy+xRM5gS7z0i8LzQ7b1e0WbuTwXBRZ3oAIqoARHpdHQkZl5xc48ugqtsBZr0cY02EZ/zVIgJck5SEMkRVJ1UFgeaFtUTVJleAQ7gFWmooYbmFUsaYJb9S0sAK6++tIj2NjPXNaRDahx8IJx8AWSFagXKys2/YuVlVzFoFyZNSgf4oBpiJBqtVl8yRfFyNltdS9VTM7yAzYvy7JCTU61VAO3VLwonEI1gM6ohRJKkFcnmdp5fRL700U3y07/DYIPf50L2LtSSDrbgUVDt1VSJU3YuOWBd3hYiMUHVPiGaxBJo4gnSJI9GfITTCJW05iGYThcgeoHLzBfL8gFgSpZszIb8J3cmFUMg8LcYfgHebGeECAMxLDZfRkMDsBZRuDZjtoGA+BBLgJaQG1CoU3LdIVCw4Pd2S2dp13URqXaqW1UDKa/K7TR09HX+xxeDfDREOYqT8UBtVXbMlxbVF3Y5fWCWYBYZsiDIxDJ9dSqJqgnrbkn7x0G78Uh6DEbgTpfL3CEXDDfflDfID5Z8R1fYp2X/C2MwmiQBsGX+h2GedHftdGX/qUQDtjWxfqi+hI8JlzCBTZdxEtXnJBC/9llM2BhIKqV1zfW3TY+MNeZ3RrIGdTxo2MZ3nYbkEgaMSDthhMeZLZ12xNOgncwAaZCg2+ABWU0Txygp0j21BEjPnwqDWONsZAF4iB2hIByBFDuB08G5U/eIQeZgrSannqpXyj+By8YjAlhm0BpG8+91ioQ4FjqRN+MLCR0xfP7ZWD+5ShUvzRe4w2u1wt+4zfO3VysPC68Y/bQY+El3rYgnicYsWvAgW44WxwY2ORFQ9BLQ7gd1WZsAlFgKiHIAIhIAD0Qz7Pthmwusa8Cq7JyMRczWU4eYoEa5VwAZX/25xAgZYLaoR1am1WWZQipl7URFJqtVsTYVlUh3XJFv/8xVsKUa9pXzGicyLlarMhBcAeQdhJltuAnmVeag1qN7MgNAuG6oKd4KzFtjmleMGFCTsO3VVi15YV1UAYc2AemAoQ1MAChXgNjmANYMEnkS+p7Iq5jPVZkPVYvKFkvYNz0LeV/doQSQIISAOWt3uoXAJS0oQUeGmu1keWFHpSGdmi6swICvj4v+lAlPLnyQ7xfRtp1XdHSAR1I/WiQntqRJmncnZmcYNGO9NoPxlXjTQUcyOZuUGwgZWwgnWmaVtu4Hec05IUyaIJTYAIdYJpwWANpGGpFSAN7SLFuUERFJLiwQq7kSq6y8YWSpWohJmIkAGWt1uoSyG3c3mpHCGj/6XkB4A6BrxZusp5lezHr6slWrlQVVeGlPzU/Bq5rdZXFy0s8jl7UvgbpOCaFv75gm5tdqN3IMilb31Lsa0C+bE6Fxt7mbQbSmn5e5sVsH9AGY1AVHXCaXXgANVgD/iZqKQiE0+5AKcg31nbqYfCFA2dcBU/fIyZi287tEiCA3JZwCYfwF3CECwduDAfu4P7q4S7uhS5rzi2Utba+6zvgKytXoT3jpXVF6OxokbZg3aWCFehueX1XB0JpJuhLBehLHf1Xubhhelpv9lbvQNZm8PSByV5PFn7egtUDbsCFKFCEB7hvEtiFCVCE0F6DKHgABSjtDsQ61rZnF0NwM09f//XNhX7u59uGcAovAXMgAHOA89zm8BcoAeDGczvHcOLucxBXm+Su1uVmbg4FxdJVcXMdZidcUZs5NngFbDiecZI2acGuGY3s8Wr58X/N0iyNAfaGZ/Zm7DkwYRxoAkFGw28WQB6QgbfdAmNQhKBeA0VABhLwll0Q6qGOgijABXgQgBTLPXsY89Y2c184c184YjV3cEfQagJo9giPczmPc3OYcwivdmu3czsXbg8n6w9/EGsNYBzsKBPfIsSBKaAVJkRv8SZkyyhkAiisxZr460iHY9yldAZ62tXSAx6vlqb7YB/dXk+FZ21O6sWOaVIvQ/fu5plO3rf1AQV4gCiQdf8XUIQJGIFDuAFV2IWgNgAuD+oo4INgN8RIc+phP3CoPvNc8AUizgUk6AckYHYKh/YNIIANkPZon3acn3Y6t3Bs5/AQEOtt33YestZBP5VV4cEwIlR0R3SMbst1db/r6ARskHd5nfELxtrHcx3CxnQFaLqxnYsY8K16WuzTRm/2DtJuTvjI7uZuGPVtFs80eHX+doFuiAFcGAMrD4dWeIDQLoeN1/UMQK5gH3PYLvYEV/ADV1+V94U1X3atFocIb/YNmHzKn3xzqHnLn/aa1/k5p3ZsJ+5sD3pA+fBpSIy1buvDaW6WkKzfHEhAVTm7bkjFqw4qmPo3JgXupveQBhb/F31a8GZRfe9xr38k4xXyIT9v0z5vpBZVQV775Z+D04aFbsgAXOBvoVaExVEEYRikC/iFX4h4Xc91WVcAYUeuYj//Yp/q9FdwNWf5ly8ByG92mq98+q/8MMD8y8/5nef5ngeIFwJDvCBIsGDBECH+cTDAogEIK2LELLHSwIALF040JnDipOPHRglE3jNmjMhJIkQyZFjJ8iVMNSzVEFHTqRMVbLWokOrZcwUVd0FnMCHKRA1SpEyYKFCQoak2BdqmAtCgIU2qGKmyxuja7Vo3GGK7zSE7B1ZZXnNw8OqGY45YsbAEpEqwZo0iRQb2GlgTjsQhVWOa/bo7JsreNVEW/4/R5kuAAF+PI0v25cUyZsxecjlC4hmJuBIlCBDYYNp0tQ2pU4dR7Zpc6w3mZtMuYU707RIvdOsWOPA3QoMKQxiQAlEiRYsGPLo446JjgjMgRVI3mVJlBj4wtyvglkFmzU4zOukkJY+nT1IrSLmbQYmomqPylypg2lSqAgDa9F+9mpVrVziEBQMsBMaFFlpzKKhgXGLZw4IeuBggTQLcTNDXBGuMocohJDRTTjMfRvHAA2sYsNhdI0a2oi/DSJaLL7nIeBmNuSBho2fi6Egaj6ettkEYqblWDZFCnmaObLTNZtuSuYnmG5QvOBKcQv84dNxEFV2UUSMfSXeGlyCNZP/ddSthtx2aRDCh0k1U7HSePOmRQsVQMyi1FBMZ2AeVVFPtp0FVaVgFYFcxfDUgDPYQCEuBZcBSBgyQFmigALAQgdgaDzih1wNYpILLBR0KM8YavzQzhjRjjEDqGtLcZQAukrFYGYycYSZjZ58hwQ5oJYhD2mkbAFHkkEUSy9qRSSq5bJO7RfnbcAoVh2VyWzoh3XMaebRtAmOilBIfJrnkEpox3UReeaT8ISed7dl5J1N74kfVflVZJWgqGmxV6FvXEMiCPQEnymikkZ4FixQBd8PHq4og1pcxAAAwgg6B/VJOObvsMsZeURzWF2LScPMYyZXFWKuMMurKqzigicP/Do+lCWsskaoRWUoo1eTsmmmtMXubkrY92duzvg13ZURZKsfRGU1vBKZHIyVQ0kniqjRuuWna1Ek82JRXS09g00nFDPDFt9RT9/Wpn8QSA/r2oFvsm4qA/ib64IOwsFBgo3HZQ9cDUayBi4l3GR4FMqqQoIo/GI9YIl9jcOyxYgBcYvKLKXO2OY67uhzarz0CMbOxDhDpQCk4l2Iszz8LDbRovMVe9JQITZt0tQY4F7VGGSWg7ZjiWkcEH1dnXS5NN8WTEzZuzkln2TMMYhR9e0bFdtuAVmVvf6m8s1VWOIAFA8ABA1wgXGc5KEAahCumiIl9rVGOhiMs3vhdDyCz/2pfD4zwgOQcFgU+DMMRJzuZ5m6kQM8hgQA68lXogDW6YTnAdBZEXepSlzNiBas2ToodCEMoQqK9AGnI0RJGrpWR3m0EOtJphDE6QaarGaMlx0sTEc6Vkx2OLXp3qs99/NS2Z7Rte3CzyqD25RV/wQJvezNQgwRgjzToBS+FwwX96Neqv6hiF/NDRhWQIUaP1eMCIxgBpqKAiz3cyBEJxFHncrSjmPUoAsISlgWrYcEManB1NTvSbAjgpN3IboSG7E0JbndC5ezOOSvMFtSkBkPhfSsDNSzXMzKQSZiw6SbN26EPlaKnIOaHiEU8pRGROCh9ZeUaMbiGFFgglr3J8v9A5UsDYgRHOAMoQgqsil8zGFeOKIwgGJIIBjJMFIV6AAIQyJCfxwDgmZS5MY452tUcI1AaO3LTNMMCQgXDicFV8BFn5DCSaQRpDgLEzhElcKezBDLCD4LQhEq7CHMe+Tsw/a5bIenW8ExiyXI55SmbfAlSiFC2T0bvXWi7j35MicpTHhGJ+dJKDJ5wDVjOUm/kk0ui2HehwV0RACzYJV+E0cVy7E8SgKhHPTAVjnokU0PIINEPbrQ5N+bKM+xgWejsODqhbkCoQABnBUuhC10oNXW6WAVTNfjHDQiSNOt8pyOy+s53ElKEH/ygIu+pOxVuhIXa8kiXxpQAMlnHhtv/eUYmFQDXg7bEJg0tW1IeKteJSjR729uDBgBrUQ08IaMatdtHG0Q+e6RCmfNbDC6uAQ/I8aUcjNvFCJ6xhpcaQIwnGqNi6qGDf+zBCzzd3GdYxg4d7aiowjrqBGF7OtQtdRW2hept+8hBqsrGV1jNqjuBOyXd0PN19XwI7lC4rbIyt4VR65ZIgjc8t7IkrnOFqyYRqoa7vkuUUBniRP2qSsAKVgMxMO9hNwqDWCqWBQBLxUhd8FgFwAAAD+OLAUCE2WDUYwwP+MFhkOFfeggYFw+oxwZatlNdfY61PLLja2ErW6TSdhUesLBtL5xbP1ZjdKfh0W/diYStutOrr2vS/23CmrtGNHKFG+HWcqEL0BmSq7qavC52N5kUvMJLDVAhoilNSY+JagAFVfGEKleZiiek9xENusYRxCIAKehlAsaIX6bsi1++YBYQqmjGCOqxBmT8ABn0AEQVaCpGAOTqtAxmbeh+xU0JU9gBFExqbT2gZw3f1rapI5KH08nOliGBpyUYcXANGch11saeK2Yxc3t31udIR2pSo3GNcQwAuG76GQX9Tl6Xwo099RUARkalkYuMRPIGNolPwAKTN6rR9cLAyTAQAAuMsZcJJIAvhoufr/d3AVV0cRcxPVE9JFGPXfChHgD4wa8+E8efrtaBpNFmBIy6DXGGsxSoo+1T9f88CT1j2LZPTZ3pjvphB3qGp505NAhDg5vRLLqqtlExCuMBaec40pHNcU5HukSdkUzSeNmVK6efQQ8UDBnITflOfewTFYhKbMhGRsHFxVsVFFilvPnCgr6YrNHDSgHK5GPBMLSxZV/Tb8vPcCkyQlQOA/wAjXtBhjORYY4foOBXhF5Zg6/dzQgjdalL9cBTj77npZe7zxm0c4ftSJqWubHd7xaHI+QNwnrbm52ORmEjwt603bk4TJWGDsGNkQAZnunGRFz4wjEud4erjU9UKeLFOX7qvat6DwBAMqufQFgmY+Ea6YWHv9zLgkfoRRrSwK/h8DsGcxSiHuX4X1/GYMz/mtMDmXsQBSOQ4YgIQLBl1Hbwg1171G0cNZy6WPokxr3n2MOe3H32s7fTDQRtEoBXDEb0PJlUVUazkwD4Vs49wq585pS1Oc/h1sCNAUMZnqS6c920qeWufYk9Y+IK0EMQ25Zxue8h40UELMcFC3JVYsGwh33ENZx8BPcqYC+4GOkDVh6Ff4QiGBxbA02NAS7UQzAUAhjw1wjwXzA8AC4wAu9NHZzFzJwRneu9nriJgijEHu2NGwd6AO1pGJ89nboRgDa9jO99xtX51rypUyDRmyB9nXIo35ckQDyMnfOpUMD9EwxRDbk8AzfAVdzJXRUMIQp4AsZtmvcJEQDsQflp/1+RcZz4mVrfARaSIRGsDR6sMRn8GZ6TDcM1SAgv7VqvFU4U0EMohMEIFA49pKGrIEMwvBRM/YM5JFNePIACMMLUQaADZRudSVgFJZ0Hxl4BDGIGamAHciDtZVifMZWd7R4QOBBrwVtojJgKhtBtWBVuEIAj/MDxXUQnKJ++6RuLNY0LNMLzfcRHSJLVEI/1cVoQesIQxqIRntp+1IvEoMAeeEIu6t0Rmt+pqZouagCSgRzICZ7IPcIx1hrKpYJerJwyPcMLmEMa4gIuRMEPiFli7IIzkVEVBM6ICJg0PABc7cHokSAJ9qEfos7sTUIBiMIgEmIBGKIHBqIhKqKG5f/eUBWVz0EivKkgO41G7MDOVf2ACzweci2SJ4qiKJ6LKJ5Bl6CijIlEDKWEjc0Vw2FcB8RiFdwBEfbiKeViLs4ixpWf34kfoKRfkpWXMWahFj6C/B2BAGiAIjwe5JERPQBQNSrCCCBDPTwA/HTWGR0GHyADZO1POFRjq/SFIgBACURAbIFTnSXVOr4jVbZjPBaiBmJgIa7CJFhYV/qZnXETN8WMzwHkP7KTk1xiCbxBAkyANFxIN1ALCn3iJzbCuYiHnXTCPSTfC3XLPYwED7qEdmgaxsHiRg4hR1bBLRQhYxbZ33nCLXiCSHqkqRmR2wjWMGrA+hGWZj4BMrokDBz/gWgOwzDYxeEogn/hQjggAy5IjsJ9IzL9jyLsQiv4Dz0gJc4BQiuIkcNkSP5MAWzZWbe5nrixIztWJVUWYgZqZTtioCHGnp9FnSM6ZbbR0WgMGkBm52xMgTG45QS4JQx6YiPcwyfGQyds150ghfJFX2CyYis+w0WiQCxy5B3Up0ZKptxJpn7ip9zdomUmGYAikeANHpOJpmgq3jhMQQLkheS05vucURRIg06i5k1lSuD41xjQwzPgAvwgQxhcgOAoQuAohuBIaAhwmwMslVJZoCEip4u+aDvGHlaCJToelTnGjGiQhnZqogIownf+KFzKpXLcBHmKx03ciR4chRqM/2fYpd0qZkcmzdWQxd182md9JuYQ8idkbil/OqbbfKmqpSQSvUNnduYjGKjiDYMAIAEt8MFi9AVrjoEicKgyLYbDKNMZ7c+IPgCBEVMajgjhuMpiCM4zfNvRvZ4F0qM7vmMAMGoANCqMJqdzQqcuVNDq2RnrOeWN5mHxVdUL7IELAOmPGqSQXoRdcs2Rniee6AGrHkUnEMEOWo12vASOaWh8IuaV5mpiRuZ+4id/YpyqcZz2BCj7YQEWvEMWgub8zR/KjYONDIM3asiICk6dClCmvBSB+aRioIAk0IPgDFA9pCE0kUgUJIADQJXSwZ4gMmoBQOqjtiu8Omq8vuOkcv8lVOWeOKnbAzpQjrLTFACAMdyfqAKpAcRlcilHDunlkd6DUnwfqyZp8lzNmQxmrQJAEGbkRupqrg7hLVSBEe5nfhYhkglrmHJPkp0XgWohmrLAFAzDOHjBZlxCMjgMLrSCf4nRgXnroCrCG5QAPcjpnSrGMLzALpzIPyAT/ByOxzwAH5TC0hVni8rrozbq1Fbtuz7qFRSAM1ClBmIY0jGipeorp5KGFwBAW3on2n7nWzpeeBpADuUQw9IEkmoD+OnBdxhcdrwnEFqs9lWpxtpnLN7CYi5mfhphYAkryRKrVVxhS57p/E0BC0zDMMDsZmxGLmiWAYzB/nTAP2RWhY7/WQhUgTPBjxo9DOI4TBW8wKp8a4pEAWrygQPM3jxqIFVebbtS7dQ6QwBsLe8WwBXsrtZybdfuGb4CwTZkG1XxCBK8gTH4qKi+Jdo6HnjCQ6m6rdwanEwwhR5MxcNlTZRel60uXAegAMZm7N/eAQfc58eKJD1cwMK1muIKaGd6puMeAeROQcu+bD9sRj/IyBSUiOT4j4ChJongAk+GAyDQw11IA+KUSKvgxQgIQhoqhoENajU+wzzSY1aya9W2q+7ebu7y7u4Cb7tmbYx2oJ5VaiNOEB8SABYkwDkMbNqqrXeS6sFeRE3k8HfssI+Bn598GiaBb/bJXQfAYgfQ52Ei/yYH7KrHgiz5kgEekIH7CiMSmewqsST9nql74e8UjMPk5sL+psyN7MGgooig6s8DGBMylMAexA/ioICbUnBQyvEz6E+B/QAiPmftxivV6u6j6q4zBHLVbu0guuu7YiW5VarxGm84AYEXEIHzfmcMD+zaAunjPUH1yi1S8DBLSBz3AvFTZJemwR1Gkm/5JnEspm/6KqZiEm4HCEIRxHIUXwD3pNLiciZhvYFLiiYXk6bkOutmFJrmeMGVrYFvGhgAMIzmPkAVrAIjuMqJGNgzLChqMobkqBEghME/3EJtOkI8PqeM7jHVgnDujjAgz+u8Qioig22KOkAEeMEeZEAMR/9DJMuwJddw287ETMiEPs+LVISyU9wHXCHc3l5sFZyyRiZ0KwsuxtGDINhAETBALBcBGfyCFFbUHhirsRIWrO1yL08u5QYzNemUF2jAM/ABPTzCNGQdEgDATdHDBkzCG2CZ6xoYAFlra6JuO9yCGDXtN+txVeKu1Y4wUVelVmagi37gPLrzK2SAC9SzPVPyqIKnwSKkAbxEniiATPBz2nCv99WdNnha910ffBZm+d5BRp6y4LIyQ7cvFDOARONBEdhALPxC4iIRkr2BMMJaRzsu/g7DNIQ0/9oIYf+eT4lDtlFnacTuBrjPBCyG/1Qj0C7GLpSIIiDDM/gXBwQAOG//cCHzsdb2cQfDI1Z69juGswcQwCsQAS5MclS/diWvbdvmSZJy8g5DhVfXXRCNdViLn8imdQdgbBEXcRF6rOB6Qvt2gA3ItRDENR7UNT0EaF57gsjpcv3+dWDrrxf0b9UVtq7AGWI7ZZ15mwO/j+a25k3J6TfaqYg+g3EitXFaJQfDq1AjZ2lvoIwuKgg7wBRkAC5Ew2tL9T3HNlVXb22rgd3myQ6zRBD5ySfj9t2ZdKoVpnzeQgdYOAos5vqS7yxeABlYgkTLMh48d12jAMhVIZLp4hs8gSe8gYs/QjLY70dvNxgvkHenFmislpyJtzh5gCMw4AM/NoncVE6KETVy/yjiAEEAHLU75ndzFvI4Q+rtGrUGqms4ywIXFAAQYIELuHaA0/CoSi/0xrYNW3V81IeecLJMUFwt6oeDZ08V36J+cvj4TuYsdngsCAJcO3cs9PkvjECKBxbgvUGLu3gywHgycDH+esE09EM/FNrvXYJnXAKvANVqJXbrud4kWCyqkKiZ/UMYcEDgkNkzXDYfVCMfAIEsuGOTTwJzYiAHS7mU0+ukVvkGP+okeIE2PPUE+CiAR3Ulu6WY17CwC/tbFmz1ZoAenE2a8zM/4zbbvPlUAIo+WKbefax8GuFwa19DK7clkIENxDUU+7ldCyNIaqYnYAGhH/obJHoyMMIUSP8u5eJ4pQOdjoP3NmU6BomCJ3ijAfABCpSAA6BAehuYkSODm+rPDxTDkrN6jB51o4rCkj+qfkuqc963IAaAB4jDHjSC8wL4r0O1DEuv2jre2oq59O7FQYqVfHzHUdi2jzWFmzt4hFcUL0rmcHsCcRemEdJDPTz0JxwAiY94n9f1L9CDZDKhMOpii3vCI7S7osv7dj96joO3jpyez8kZCYo3hRmdKDjCoK6Bz/9A6NmpnAownV42PXDAJMhCuzo8rGPgxEs83ds3IRpnlbfDKhBAPD81yPf6d/q6PRP4sBc+yj8e9d6wAeRJUmx1msNEg8+8m7sNEgnrkeGnx1ZBtm//+yvbwCfA9QEIwXP/QtH/wtG7+H5iQaG3ezIkOv4GdmB7QaFV27XdKHhjG7YRgCOy3rYR54WZpk277k2pEQClZl4oRmUP0AasutXK/atT/KwndQbelgcUQC9sgST7qCJMspePPA2TubG7SrGz7crnTk0klNwi1EuozVR0X7RbsVUY2fqu9UFbOCyiwAV4/gGAARgAhBBXdA7g+fUr1kEUnhjeuuXpza03npK9SZZsypRp04ZN8+IICRJ2BAhE2GAyAsqSBMRFKAnk5MkIQIBsA+IApy4PunSKcoRrDa4xUdZEefAA1wNkD8YIHTOGKS5cRFHImlUgQABRorRy9Zo1/yvXAljHlt06qewVLkq+fAli79yEc4om1LV71660unr5TpD21y9fwIP/GujWAIQVMWKWWGlg4B4RNZIlZ1BjOQNmBZu1KdDW+TMAbQAAaNCAwnRp1Aw9Paxyq4qnKh3qdbDBgI4QOgNdHTjAgMxBeihuXSx+3PjFixs7egRZYiQBmtNpzlyJknr2m9twluKpc5KHEnzGKFrTFGrU8oqOjhkBqDwfqShCyQJbwETXAlu1ZpX1374AwXImgEkoUMKCBBPc5At76oorGrrw0suvCvu6ULDABCvMABYQU4wxxyCL7J7JLjsxs81U5Ay00DQgzbTTXtwDhT1Yg62KHGe7gP8MS+gAg47dgvytCDwuoIeiZBxCLhkOlLtIo4288IIWJKAjSbsstZzOJpxy4smDMCcRpZpkAEjqAT6eoYcRDh4oKoooFEFGEECQWYooPvaxLwAT9vMvQPtEWaUac14IIYQSNqjGgVKAKAEFerRoYxMLNrl0EyX0qUtCvPKyEFTC/hqVVFINeOLDxRp7rJPIJsMsM1hX9MyzZ0IbTR/SSoNRxhtxnI0eQSyxRKAhffMNj1g6uIUDZpU0joMqmnxyI49ooQUkdswpaQOacGJUSwduEhen6bxsFMwwPZhkTA9WIcCREqoJUxRZpojCAKOYGuET+OKUE4U+w8qqmjxe+Of/jn9ypAeZVu5sJZwRauuggwtG+KWETSjVWOM2tEiAQk8pvDDUwCws1dQOUw3xsUbu6YSIyGJVQ4EUM5iVVm1sfSZXXGFEDQUsbGwNx1vouUBYSxgoArelD/iEDEFumYaDZq121smqk6lWymvjLYGkDbr11stxzz1Xl0bPdkCXUtQVk112sQoAKwCtWgUXA8rZhZ4fGh7hAUXmZOSBdmb5D412gukAEEAuePyCcCRv5ZeLL77glwtiicWGWAQJwo82RB/dAls8nbBkwjREGeVTV17VgE5ajRnFWG3H2bPPPuP5GQBy5VUDLHz15IIOkh7WEjyWLgLqDqqouoppnmeW/9lpmqSaay+m8dpKksIeuxrvdAqTJ/G/Kz9t79h2G252z9qKK7ACDeAZZKpopZ5pFIniqTFQAAIFyDDUP8xRjTscDQ+fUCAebNBAMpDBBoKQoAQbaANhMDAWHvBD6EYXOiWoIWSfItlgQsW61qksMaoSkTFmZyJYWWYzNOOMinRXw9CI5mcauJEnKGaDPHyCAXlgAAOSh4cHCsJi0ftG1ThAtaphr1oh2J7X4jWSsXVrJmxLG6PYtq6dfGcnqzhf28AUN7SIYixoJIt/AiCgUhRgH/R4xpuiMIJ/PKMKXEBBFKaCDHpU7AI2+IQO6kAMHejAkAzQAREZ8BsGCIEBCv8UxieEIQQLbHB0orOAJlAXwhFuyISsM0AqXiciNZzyRPfIjB5stpmb4UxnKrKVznSWqxgJ7yG1sUQehNDL3ECyCGSIheZGwLcOECda1WviRq7VzH44opkheAESXvANa97hDt94wTa/cQdBYPMF5nDAF8fXjrftRF3uU+NY6Ca/Nr7zP7OYxT+SUh56BEMSSChAFZ6xBgOsoXgBTaAODGECExgCocQgRiQYAAZgAMOhD41kJC2hBA5mchNBUIMBRGYy1YUSpAZAIYhgdw4XOEFmNbvMzV7J0prhblag2dVpGPKGP+4SDGDQDZAOAExhPo5y9BhBOII1G+fliAPf2Ij/UhWmIwlSzHhIm6Dmfoq5xwliA2D0IrvOOaZ1ggWsbbSb3QLAhyjUIxj14IO8OBABZBDFACOwhAQfOEhimEAFKkCDCg5qCGLQgRgiKIQdHFqIODgUDMEowAY5tjElXGIvFYoshka1BpCa0HUpZIwSHgOYCQzABfeI6Wb0oYDSvtS0o+UMz4CHBRQgTQg5zakrZuvIpVkiOJqzQQd+0aMIRlAQxuvAH40nCBvgYVgMbGARilQkIz7wgZvbHEI2kE4PmJOr4VHnGsMqILICyBNjoEcV6DGGZ4TiGWN46xoeUEFBkCGBBVVBD+jbA4OiwQwi2AEkILGPwRZCsMAghhB0/7EJP2DqUhb4Aiju4ckSTsCyly2VAUYlUg9pFnalkosLOJyABNwjpZzRx2dgmbPRzPQ0SCNWTu0QB364Asa5OQBBluZApQnLtkWwRAVto7TfHGvGxlqec52LByMLMxZ50O51zajd7c4NrN79DzwDMAsHvJWPSwHcU4pyAWE08IKD7IMKZtGDG9DXBD3Axw7QcAw0FEAEgo3zDgpRCDqQ4wsbq1SC3OKgkZVQwiGl8ChL2QsKn0zDE4jQXDjciMzYkMQy/V2M6BEsnNoB0/wQQRw4PZAfDWmIzAtSQ4XstEaawQyo+PEBIoGKINFB1QdgWiN9w1xbFykP5Gvyrr/qTv/vUjlQGwAAU+7UvzWsoRURhGARDlCHgtKXzHxNM5uPUW3+QsIEcYYEnXUaBEwpSMFK6EWDOxnoUFKYQxcmqYjMTaoHdViVLGJRaWIEgAsAAqdgsAM5OM1pfsTBDDAeiKpHjRtX2GEgAQc1q1GxA2ZEgtW+iUQkCIIKi0Nc4gdQNXPxYGswpHPXZlRjd4ENzynHcxIdGEr/doGM9zxQkAe4a5r5yleE4oO/1UZDACDRjp1nm87EMAMYnEEpcCdICVrw817aLeFBSyOz6zZ001mnaEV02AXGqFkGnvGM1NRDWLzUd4v7zWkRIBzhrrA4KjwNhoO7AhgAVzhBImEGZtz/vQ4TbzUzXK32icvayIEXfBHIoS7whHxM+2EnG0seqJPLIhQqj9Mu6hGGMACigZ9gNTPwuleD6sA3d4/DfvkrAv6iAQ0mQMMOiGGHsxfg6Ar6ghYi4II/g5LqUOeQhUvZWap7FGX3yMIlLjEOewwDBmuSoCRi6wpytPjsZj87jDEtd1SbocUItz4qKE6Hvd+dGXUIP/jp0MilITe439ylEDZADp2ka6u85k9Y3Qls77bjDuHYhXskYVBJCIMBgEG/8AENegD1TEAHKIkMPqHVUCEO9AsS+IEfICEAUA8NIAHAIAEY+OELFOzolCAIemEdQoZUIqzdnk73oI7QMMwK/6bO3DQEBkfGBbRA9r5ACdgBGVSsl8ju3zgN+uKAsGDM7IAwzkSAGQBOBK7PFSYuSFDBDCbO7sxg1IrABoblG/LgG7bFW8BojMYH5MxI8RaP/hxvrOxjAy6gHnZhFwDB8uqhHhjADtisvuzLBA4gIWKBDJgN1uLgCONsAtFAFixQBDTNDvgBQTpQ9kAQFMYB+FbH3NBN9waN91jwMSDx9y4kGnrh2zTGAjogHASB+Vjs3zat7OLMDn6kEPutCHdg05jBCFGN+xrQ1Yok1OYquLJJUYCgFLTobb5Di7yDfNrHfRSP/nytDAFEFLzJEooJGcJhDMoBEA4AGHaAvqzBAP9NwAbUECFioeM0DhUwDfoo8L5kYc72YR+CwAP5rC2CAA4uwfZyz1QKIxI55DBY0AXhcVQCwxssBcH8IAjCAWk+IQzGLs4esOweMAjJbhBX0QiT8PpgkRVRATiKB5vu4AXAplu+pTvEh5x4Yn3gL/5EAS2wIn7G8Bi5YBa4gAsib5IEIRzK4diS7RPiANvQwBoRcBf2hnLwkGmCDEjsAMDuywJZEQidQcE6sC1sUAsooBeMQVROUB5RMN16T6QqDB+lIRNHh2P8wAMexwYkgcUesAhJsd+0z8XGsgi3jRkeEvz2SyItkgDEBiaoozvICEzu8jtKARi7Sm56bQzFShb/VDIwAzMlyUFYJOETkEgNO0Bp2Ayv0CwSsrEcxoByegu+lGangNL1IAH1jmHbdgAYYC8p20IJkg4O2EEfrnIeU/DpDKPQ0M0S4fEe8iyT2sAPDPMC1k8hx5LT9sEHC3Eh/VDb9CvOrq8tdyAODqD9pkNs4rJbqoFRtIofdMGcOhIkwTAMi3FAxooLBrM7N8AG9M2hKGrHDqAQ8EH10AwfeqAIjk0nAdJzbgM3mnAAj6EYUg8SXIwt9rM0g0ALeuESsOApAw02z22kVKgSrTIezc0WOqaD/CAPbADfmK8Q+cGwNM3F/i3TzhItiXMswY8ZHM7h+C4P8sAchKAESiAP/1LUHEx0RdmPHNyPfXRhErzwnNZlJMsiR8lCDAGzO7uTMLlACGwgSDQzSHSMAVCBGdaTGlaPGfirHMqBclphFyhHtyxBgeAwRI/BAC+wv8ghCEqzP/2TAvphCjTAEXOvNVVw0FDFHgtUHqlOESalNkMnDyThK2MLDMou+8qyxTRtEDlU0zSNFV/R7kA0RFFBCA5AiJiHuWqMYr5JEK6wgGSUUdgn/sAQLeSmLOZGDAHkRwOTC5whDzquCIRAwAgrN1zh4fCBGgzQ4srvAvAgEiCBDiAHIR6oG5V0Pd1s584ODK4ABIOAWLUAFCJASTwBF3DP6VIwHifg6g6UZaoyNv8n7LJqQAkwSnQ24U6ZL7YKERyxL+DGFfpMb9OEExLMdQdSjfss7lBDdAccbu24j9UobmlqEblsoCKxyRJKoIBoNG4KYFPB8KugDJ6c4T8Ec1SHNJiq8BOAJEiqzRqOwRohQQiCKRY0Dh+skRiKgEcEYXOq8ADMgDOLwTMvUASIAQwoIAiuwD+XshemoR84wBMegOpiEwUV4Qz0AAaywACk4DUhEQWHFqTWgR/1zA++4E57qZdcDAhdQeiWkA6AIRLsIL/QMl3TVSzjgPuWcAmJAVHXci3rIBbrjhkEzlhkTZiiS3NUDAiyS7soIJ3WCcpkAWHFCkhVUiXJ4bikK2r/TtUVIKEYOIETigEfDqADmgG+DoAZKPYYRMAGIGiYqMo8TcA+vXTb7IABgEALOtdYe6EfpuQNnuEd4bEq68IFTgEWssAWskAApBV2qtVZJWwC2OELKgXB2uALhAAsYyu24i4OgCFIPC3GXG8ss/ZcjZBrrfYbUU3oQhQWy7Ze/84bCaLWBA++omtzCm/JDA9HDdZuQdU79VYlJwG3yACQhCVw7dMaJrYIMofZigAVKBYS+E4+o2aYYsESgIEzUdb07MwSaKEXmLIXCrgfkmEPPiMD0vSyns6kmGB1s8B17YGCf5Yq1xROZxdlcAEOEJEfL0UJekmnYquRXi1tpxbW/8hOLM11ax2QLY1z0wiO1WgsEsi24ui11oos8DjugWy0bShgnNZl8QgkYUWVfFXSGUZVF7xJWSQoD0Sg2orBGoqBAX6hGYogEu5OcK0BElChCOigi3HrvfIwEnZgzZJXUSMgBwq4gNlhHB5hCgRgjXMAZH7PCU6hZ1vXHgJhjynYHmBXRKoyTqNykEslHq7AgxeEQfLAoZjWek2YICJ5N6iPTw+uIPvNFTM5XoFQ1pwGyPbO4gAu1pjtxziuGyOhSAoPJ4A4J+R2JOUniY9YlpG4AMjhG4znK0VgFlCvGCCBDBT3AAwBH3p5fi0WFUzWiy0ogeigEHYg24AwDlS2F//SoRfYoZrHoeumoBeIFRQ0IQFk11onIHUDgXUlmIL5mI/72ILfVBIjUXZldx2CIJEXRAnMAQz2AZJ+JAF945FfradmDBjAcbac1vXEkhXjtfTAgAyoCr6MjJQh2XqHrJSZS+NkbQNYGScawEuEGCsIBGFH1Yhl2RlGOolLoQP09xOEIA7awQQggRgUiNV6FRXwINas4Qv44WnwQBgQk6A2OcbAoAEWYRE04QimwRM0QJvlOQhsIQEYGGXEeRDUoZwhgKrtAQL2OBCyOqthAZArsZ1hU6QEueo0IUyPrgNDOEYf6TeA6Fhora2BaKeAxO1abCB8cPRYkb9wDhKWsRX/WuFyNOekIcgGiiAPdMyBoAu+bo2ihQAnuMMBHHsbPIAC5IZuPHqkR3WWR3Wkr8AZlpgMdOOhCsGgTKAQDskQ0AAfvJifofgYUMESLgAxL0jm6kx4cyOo4WCoNeAZHmGbQdANFkAGwtqpxRkThgACTIGqk3uPIUCry6AMYOG5uRpo31QF5xFOMfgvcGGb55k0N0CnHimSmEfzjqX8fmyIwBuS5toOmq8J35X0RKAIoOqYnAebcgSbrlBFVZSbEMYSvmGXbC0PamIbBpw7ClwnKKBTC8AZFJykZxkEnOEKrqAAJPwF8lC8FYkYSvuQiMEQHo7VTMB9UUEELmAXJomS/z5BwIAhiAgAt+EADtzADTQhCzQBxhdgAUwBx2tBds/BCfJhCHAcx1MguZmbuZvbuZ+7DOagq8OaycO6urH7L+6htxP5rH33h87brQ9AB4wFDNS6p4ZormfL04IE1cTvSeOADpC0iHYpD5ZTLznynNJGXKiDwGtCwAdcoydbYCd8LCScszk7lrngwSHcz6+A9gSbgbBUIHUgDLS8EAxByw1BipmhCJihEIJhF0YAMRXokIQgDyJgEV4cxt0gE0g9E2z8xoE8BQYgwuTiDFrgx4M8BWZ91iFgHoh8HuYhq5fhyJ17HdZZ6pz8q6l1TeVRH+BAC9jCg8/6RPPAEh7Vrf8bCQy2nA4aPZKx/AAObk/jQODgzgkP1eFijMZszYgEoc2jswvJ6FwInN3bfZURXM9Boc8nfKQf/MFHNcIj3FhBoQFegBuVS9EVCRgOSfMKIfUMYTeoYRbCgMQBQYFSOgy2QahFvdQzweJPHdWBHMcJ4QxcwB1kAAFUHQFGntZpncgDQddzndePnBeUXN0QtMmJnU2tOwUVoAG0QJ5Hcx15ydmB6LzJW8aOBQwc6TcW9dVQjfowzThdke2WkOIiTtZ2rIICfC+/aBcbu923IQKyngLkdrLRQt4p4AomodAh3BlA4ArQ/gpAge1vWxP8e1gi1xIQkwEkgQHCgMML6qD/CoEa/AAaQgE+dmHTw6AB0kGoR/3iMd7GNX7kGz/kH//xTcHxaT3XUT7Xc925V54XyuDXpzvYrVvY3Znm9aEX4CBMR5MtgkCInP3nx9s3hkg3FrWnfmToY8uRJvnt3NUMiNIVHhDVoBDibq2CqtBEF4VtvITdUyIltH7rCdwBGqDru74AEFze5b3QI5zQDZ3f4aABXtziL2EajEcS5l4S8CAMHr3OCsoQdMAEoMH9Z0ESWqEcYLsaQCEdDH8REP/iUT3VAcIUAoEICho8eDAFIULzUjScNy8QxHllllUsU4YXLwMsGoCwIkbMEisNDJg0IA2lypMnU6pMKU1fr15a/4IouYkziCUhlvAw+Pnp5wE6OhgcEHJgKBg6YI4ypUMnaVI6rlyhiiMia9Y4VOPsYMYsEqpIB4rgMYvnbJEiZMjYECToTgkg2+pGuHu3V9662xo0oOABMCgKoAqAOnwF1JXFjBdrAdUADhw3mSovWGCqyqc4wA58+nRABzAdhkwY0kGaGjRonEIBKveAwJV0i2pPpmz5sqnduwci/I2QEIIUxB1OnGikzDxeRngpU2ZAikeQIq30MumSJcyX3GE+uxRBy5Ug5G8GuWLJktsiDD63FwIVKVNgYIC5og8GjB07S+kYPRCJGcSIAEmBXFliQxEHWGUGKqhItVaECpa1Vv9bF1x4BwER6KXXXZrs1YsDf1FAIgWTDHZYAYQV4Bhjjy0iWWW5YcaCDQcwkJ4Nn4WhQyGoEYNGDybMwgk1sgSzyxoCpMOkbW7gdhlmvBWUAnBWGrRQcRAcN88yRiT3pRHKbNTRRyGNVFJL3a3ZHRFTJMOOFuKRR54Wd5ARCxlpBWWUUUvZ5woY9wm6nyuF2gHVVJEMSCAkduRxCz0ddCCIJT9NmJSEEFJoVlt5soNXDhGIGoEm2/QyYokkongYYq7KqcUioMSoSSZRSmlKB3h8hqAln33SI4HH9IBGIbLMYoIk9cDmBm0wPjnjlMAJFxwCC1lrLSHFQWQcRF+W8SX/c89FN92Z1rGE3UrbrSmNNHxwME0/7FAAq3gUdHBhLAnyyYAQDDCVX6CH7hcHP3HEYUdVUNERiVUFxyEEO0j0Qwst03AgSAdtJWgWe1JBiNZaaVmyYQ69iGqyyb30pephhLHa6mFygiKrZLjZiqtA89yRxydvCXIBGWGYgMYxx0CCjyGShBFKGMEguQYMTdYGJa6+/SbctVpnm3WWxG05kZdfKiPmmDxwVG51aaKbbrrbvW0MB144Mo7JqkaAL7426OieUE0tld9+ghu8VaGo2EGMoQkf8A0SSLAjMeTsOBJCFVUI0hYelqzFgIJRCRHhUGuJM+qoon54aqoux8x6/6uy2jbZjFYPZEoW+F5oAwOGELvD0fhYMkIwkgQTRhjLPpBJOk9SFqW0WGO7UPTSc61tCkN022WYz23vA3TSmVnddWyr6dLbL+HCDQBvTMPOTO6zU8+kHeSZnqWf9PyTEGDon1/gDx+suDiYwQwi4EpU8uC4x0FOHOJgRwMlV4I8fMMSd7ABgiS0FqnkoVSlI9WG/FKiVjWAdbKaVc1gJJnYZUITuukNQQaSt9ztAB9JM8QO0CCEXeyiHoAABPGgtgxo5YY3L5yWcLJxREJkAwFIRCIhMKCt6x1HbGRTBg94YMWzlYk6aBpfdtT0EphIQwEsOMITHvEGT0zhEjOJwP8znkGPEdCDHrHA3N40d7/89UsI/MsPVVxBFTvEgRkFtAMqFJYHWjhOcgxkYAQ0dBcgbECS5siDBDeHQUh2EEQkGmGrCCOZWdEMDjVL4W1W2Dzn8eYRF4jFBWzgikLUwRCGYIYldjGGMeziAj1k2vEWgLOcXSlr1lqiEovJRARAEQPMxMAQnjmPIWjvOVj0gQ+uSC7wjUR842ubutI1gSw4pgGXeMIzAOAJFnjiHveYgRoykAEFwFFS83OLr/JgiTwwQJ989BdS4FOVQ/KjgAICBh0SiQRxKLCRpNMkEIDggG044KEbIIAQLLlBvJDOgxvqy4hEuIjImBKFplzeEKf/VERTQMATv8AdHYhRhzg4IgrlKEcudzGCegQjFJJoxRqykcoiXimZBVmiE5uoxGZiYBRDYOoovqSORCRCGd274jXRpk2SdNObYCyfPsahCS2YRwu96IcnMqAGStRgEIOogVvdKk84Ugoue0OQBW2gOc4JpV8Hw0pBdSDBXDjwEg1k6CPx8lAHKHai2wACXu5Cqo1uaCYe9YtIZwUHUZrSZstjYQt7gxCB7CYQj5jUTkrAwjTQ9KbhaEY9lrYsXCBgds+DXtaMmkxjMjMbSl3qKH5rhKkm4orEzWI2uXguL2rnm2E0wATeEQhb9EITmrCHPvTQVrautQaI6G4NvOGN/1NoAwAAQIF5PYFeT9zCcpa7wx2+AZd+uaKvxDDoZ77hiBxcQnIOdKAjI0CXxEq0sW0MVekOPJMGoMovcBBpCmFE0lNaxhYLsMVuUmCKKoWWN1nIwiVsYQucxWC1ugzHCHwYjHCUgwbOOwi1XlzU3DJxIbxVKm9H4dvfSnW4WOwxcbGK3JK0a8hbdRt3nNtd8LZgC+Dtrhqy290og8MbSaBylZOQhC1oORWpwMIZjzCFMMPrFncQRBHmGwdARqU9lnDEYCX2wEYitrEPJfBjH4uyUrnPfW6IjIM3uzwhfvbCViJilCiM6AVsYQw2LTEvezgGXGBgqNDD1hKzZdSk7v+2tzrWsVR9TNwtnO17QTbAB049gAFEIxoTiAaRi2ySM1QZvN5gA5W9+9YoI6LWVsZylrfQBGDHAAfXuMYjWMCCKSR7GNOYAgfuUASqkCNQBj1AekJA2P0qcIGkiySd6XJnyUK2dNTdcy/c0AtAT+Y2N6swEX2jYYNUCcMQqJ29IYBoU4CD0aztISCWhY3hDIda2cJWwWeMxGX2treJwIAMRrHjT3+auDxoAZDNVZIPuMAFZ3BBLc7wBzmIXOPnYPUEiOySAVD5FElgua+9AQ5EuFXmSd61r7OchGAHOwY8vwY8YHCEZCc7zMMYxhSqsJP5Gsw/DLDBpLxA2AQystv/kQTwnceNshxoIgdaLxV1NTGTJ537SeyW0UkznOGhfg0CbFdpFkxhi3pDIAmr1eGJe7iLKLBi4MTsmvR4+8QnLlypMsBAw2UQcYlTvOI8SMJxz6SEkrhADpOXwxnOUIt4xGMGmqdEPGoB+o5rPNV6aLk+sKxlLJ+iyTMHh62vfHNRA7sJPX/Cl5Ed9KGHeQqM+IYQXFFAV9yoCOrpQBWgHjn+OvDqdz4Z17nOjudr3WRfB7smAp0JIcpo0MPEVnFU2na2i5/tp6BpOXZhYh++xgUaVkjfo8dMwS8cx7+FuMMNn/gd82DijE+CxbeIcaY2eZeHefGADZs3A5QwAwmo/wZupQfeoAcPeHpWlno4V2Wn8F2I4HKxF2w80AQfuAXDFgM+9whBdwQnCGZghmzJxgE7cUhHcSk2QAYdwAG50ECX8Djb1kB3Jn1c92G28GE9uHVft0JFaHbbl0rt52LUUxwpMH5PCAFOmALgYH44xUuSMAJr4A7EIT3SM3jNVH8P13BS9XCjIIaIt2MysH88oIYtUHEtYHGkFoCVZ3lngA2Zt4B5mFYLWAMPSGVbkAY4p2Wpl3qBuHK+tgW/Bmxa9oG0R3vEVmwwcGwnOIlhtoLJxjNlISh81CmxUAU2yA44iIPcBjlZN30ghopAaAs5sIrPV33UdYSyYzXxhiW25f81WjIPUch2uZiLKTADq5V+wzMGipAICDAE2hJ4X1h4iGd4nSZVZiiGZKh/akhcbfiGF6c2pkaAmad5eUgJDVgDagCOD3gKhDiIjJgKWhYDWpYGiTiIjQiP8dgEODCCxfYI92hs92iCZVRGQDcNd7A5m7gWCDKDtLBfl3CQ/SV9QYiKHdZh+AZiDKkJtjCRnnUZsvhZtFiLXXg9DuEQW9ItxtEtnQCMOhUGgDAGLlA9TzQEYLhUhreMZghxiTCTOyaTaJgI1Lh4VwSH/heHabMEkWcAlGd5mYcNeTgDbfVWDUiOi6hzIchzIkiPqfCUjhiPOOAD7wCPUVlsXXkNtvf/BMaGgmMJdI/wj5ZwFKBzKXggCEiAkG95CfoVivrVkCD2dvUWdx2WiqzIQol2K7fybmm3kV24EEPQEB45EdgDEYY5DwmwBucXDsQTBiMQBVv4TEtFCDjWVPVHhkuVk2YYcTg5cTzGhhTnhnAIh9jYRQOIeUe5gGr1VrGpB6fQjlFpmziAmzjQDdfQDfR4m7kJnFtJe7Y5gk8QA2CJBY9gbNdwBEAniTAAA8aGdOzROVJhCf3gYXD5lqpoCw6pUuB3lwuQBak4kSA2aJiRMyhVEFrThVB0jB35EIopTWEyCvNgBE5QUzskCe0QCrswAaxgPU5VfwOafwUamjw2XMPl/4Y86ZMt4H+Pl40iV5R4mJQzUANM0IcROJu1OWy4eQ26uZvQCQNSMKK86aG7OQfAiZsdOpzEyXOp8A7HeZy2p5z6aGySCAvXkAxV8A2hcwB5MA7j4AvaiZDj6Z1QCH4qlYqoiJ6mkJ5EJBBKSJjLFHjQhJgTIU32aQTqMA9PZQTz4AMu8Ji7AAhhIAuSMAbnMAopMKBeuqU1aaCJJ5qL54ZJUHFswAZwyAaqqVUaJwcfl3kKmJQOqKG0KWzFRqIiCgMraA+XGJ1ScA3QeQ1zMKm56QP0+IgtGpUaEAOpoAFP4GVgGZbLqZzNuajJMA0TlB7YOQ4d1qpFapdZUG+yqv9SxIF237mkumFhUOpCCcF3hCl/zvRM0HQc8xkmx/otPoALNhWZQxIM5TAADzEP6nCs6qAOcGqTNBmnpMmTjeegSYCneAqh2zSUIvcHmAd6C9hWGrqhacBzxAadjMoCAmAPAmCv9CoAK7ioIkqpKpqb8+iiMaAB7/AOnPoEqWB7X6mwXnkEsOCwxzYN9ZoFvjCxHtZhlzCe+BZ+UdiEG1s77rarKEUQBFElyJgt7rcQUCR/hDCshumy3oKsx6oRgdAEisCsYTALs7AsLlADPJAczFGt22qgxMWtFed/4BqufJpxH2CufwB6VEAJbYUIEXgKhjpsIcoCjTqv99qq9lr/r/WatSwAC4qqm8AplSIYlVtAsDCKsDEQqgl7RpEYrzAAC/Zgt+NgD1nQqnrrYd3ZYd8ZhRnWhPQ2q/fGqxhWEEVEmO6nEFTKTE21VC+bpVoas2SjEWUQCEywWvUQCtSgs1GwBoqQAHqQERoBVYkQVal7oDmJeGqok8SFZWyAtOG6p3KYjacmoU5bC1BLCTLXlOqom4u6tfYapFkgAF2Lr2DbqHQLnZSaov4aAwDLc1ugAe04sJyKBTMalts7qiLqsFlrtwKQBXnrkEZ6bxw7b4Mrfklab2inYUIFrIUpvy3ZkszEVE01rEZgrDIbLmUwB7wAA4GQAPkJCLMADbOQ/4XlELo+kBH/C1XWCsHSKAMTnAhuKANwyHiN5620G66IoLQCyLRd8AdOSwVUwFaDgAjgwHLAu5vIxrXjcK/JG7aNag8OC53rAAPrsA7Pm5sduqLTmwbvkAYG+w7ZC7deGZ04vKh1G74Su7cPKXf1hozEMRyD64Ry575ChSXA2pIsW5ju6Uy/hb/SxFTU+qUYgRHMUbq84L/20A24MKZhAA3UgCRJEgWnYA9zAAtpXK3Wmoase8Go2ZNY5qB5ysGIULtA2aen1gXmSgq7G7UorIEtl2XDFqksMAz1GsN2G7awILZiO7ZjOwc5rMM67K9mO73UGwPvYJwyOqNeGalKDP8DNGy3eWu3Drm+TSg9VszLaRdvGha/7+lMLCvMLUl/ArqZwJUc/ovG/ssLczDKs6wAClwOgGACoRAM/xAOu4AMMbC8MKDH/hsuWxpVinfBRqvBR+t/tKtrH4y7IifCf/DIlEDPbJVkSZAGjwidntzEWvvN+zy2OUzKpWzKp7wOPwyIMVqwtvkEONDQo9qVdOuwTFzLtjx+xUE9x6gQ1/Oeg1vFAgfMwezFw2o9wyqgbTqgZszM/gvN/zvKoywA3TABaxAOkmAOwhMM9fAD3KC1S9y8/6sRvKAOPDDUQ01cDlqnhBy7HewNUTauQukCuMu0I/zItUDPkYzC4IB687j/z0vsyZ7MvDos0Dl8DQRdygaNAzqcojzXjlAZsPVYbDisxA5b0bUcCOJHxccoPe/J1yPtNR+9nl6osiPdsvFp0obXjNpqk9S60nPA0tAsogKQCmuwBv8gCSgGCKFQD3yQr5eYteDc0hoxJmZzTXYKu43na3haa4jcXYPgzqcWwnIwwqBn1fUsybKbcz6c1rHMvJNa1sTGw6usogWd1mldyh+6jjHQjjDqosZZbB66DpEK0OBrtxBw1+gLf81Uv83EsvE32PHL3SzZslbKJdHkaZ9JwRRsrRrR0tBcBiKKbL4w2WuAU08TDOYQBv+ADEcwDPM6r/2dqC8NzdZkTR6Y/3NYdkVK7aDdxQa65sG2S66wLeGyrbu1Xc8MjnrCBpXAedCojKnnmJXFjZsHTeJlbcrruGUxSpwO/aHXMGw6XNahXLcUDQHLwLHIqHBfuHDRg7IpC398PQTLYJgpsAxcUuTL4IyAPMFouN7s/cyjzAIwkK++0A2UTabYfGLtYAIcwN9F5+X9ragw0A3QjAO8gJVN4IEGLmqxW2UNrmtr9cEDcGpRnbtOSwqkwLuUgAm3fXO/pnMfiAO0t4goPogZvgU48A6B7q8ETY8hqOIBG6O+OYIELdETbQ+BgOnL0ITH2Fu8VWOdTqV6zemBR8z4uwxCXt5ho781ueStjnhXlP8IPhDU0Ry29ioFYjoG9TACuVQPhdAOjlB0XvDlRYds0NkN4IyiZw6PHtgEvlbIrO1dD67ISyvnUi3bVb0CK0DPmNC7mMDgeAqu7ghsiljohT5rht4EiZ7uHC7imBqV9NjD7/ruxFbKE23psIDpRM7Rl8lMhYfYGODp2SDw8Te/Xlzq+Vvk5b0MElERXwJxrdvqLXDBE3xF6iDrGnHsUsACGq/x8MANCcAHD5Cf9RACwyDsJu8FJ0/sUa6ou9mb1tQEBN6Ifd7gbo4Irg2A2VjtH1DtchDPtbACeb7t3u7tNQ+uNE+7N0dlS/+H4q62IT7i7Q7vU4/Kv7nb6wALOkz/13W7DLBw6kI+rLvl6QAv8GX/6SyrENZT6qX+LV8/EcpxEeASJmS45HA48RJfmhbfDbyQ8SPq9/Y6r1LADWOADD8wBV4u7CpfdJ3t9yQ6yrg5j5eq7M2e2kmga64N4VYACqbG8xI+1bO9AkB/1YOg55jg7Yhca7iNpw6e9LxGa7D3a+uI6FJf0B2OAzwM7++am2at9XQdCF5/6vp7mWOYDTIQ8GZ/9s9EzOL9TEawDGSDxhZx6s2MrDnJhoGMmndP1LIu66MsBSQK/iO68Zi8+EZn/sE+7P2NbOHfDb2Jm9aElWeO2rIru1FGCQYgAIoMAgZwDgDxQeCAD3IKdvmT/5AUFSqUHFLCFHEQJkRsLF5EhGiQxowZ2VRk4y2kN5JJvCVBuWVLky3vmuCAiWPdTJk1r62LWTNnzZk918H6CUtomTLLRg05OgpDomyJMGSDGjXbUwwYhlzFmvWqESPKvK4zQrSo2DJdjfDyykOtjBZtW7Bi5VYtD3U8ePHqBkOvFBh8pbBg8ffIsCmEpxQuPEyxF8WKAcNg0Zdvt24wfViG2aQJyhYok3xEVMOAL1AgrIgRs8QKCGnnCBIUKKeL7IR/atVaUeshRIqYMGbcOHGQ8I4gP4pEdFJ5SpU43uWkKbPnTeg7pfv0CRRWGa/KRiUCn+i7jGzkpUKtWlWrVf+sXLt290qUl3zuynjxUMYjEY8W/N2ygcut/tTywYe78tKrLwWlkOKII1g4DELEGqNwGBYsBCwwBinDDIc5cPChCR6S6OyzzxA5w4AcSjstNSuCIEIagWY0SA7aEqrFtttWaIg30IrDhJJBdnMIEYoqAqm4kEJi7iWYcKIJu+hwuiYmnHjCcibtZlJmiPDAw4A8886rSgYMjkJKq2WW4UqZAn1w8yv64JtLLQHfYgVANu5US50m8KIswb0Y/AtCCQ87rELHMNSQUBiu+XCOOSAFUcQkRkTJomgU0YRF1MSwQokGDHBBDhdKLcjG2mwjJcfbcHMoIiM7qmE4IomUVcn/45hMqQkfnsvyOp2CtQ46n+pTJjwZlmV2WajEbKqqUaYdYplq1xwiLK/eLFCZdXiZqT60fKjTvz1bsChPdFsARy4C8ZojLwS7ITTDKTIEbLAJF2XhwQcdnbSbawT+EMQQ7UQJEQOk6EULJZb4dAklrnBBGhsv/kNVOWpzlVXdHqLI1iEfmiEeh1bYLUhEICoOpJNQYumdXy97UrphocQyJ5ec25lLr8CTgYdmmS0PWqcS8TLNao1gUxnu6nvTYG+9Xce+tNS6tC2L0NUTQLfa5Y9AA7vBC156+eo3XxZeSRswfhf91y8YBBa0m3UqM9hSHiaYwJcGrrCixSVUAwGU/wlkNPVixVXF8dWFdlt5ZEp0o/zVV29luSORPGtC5uc6LxYH0HcCtvN3nnPpnRhmhjM//diCfehmwzwK6VGsHUVb+Lb9dSWZpfOWW6z74/qidNv9mngefH2TF5gCJTT6Iwp98MILKbQ+Qwb1QlCvgQvOm4dzpGFhRdMgRk21iTWRRppSE8fYxjNsdPVyzCmJJ55aqLA8N8uJXBlHPMIclbDEdKZLXQJf8o6WnK5zMUtdS3ywBfzMRUCwa0GzMpgIGYBnWkbIFtN2N7WZbSFEJgQRDpRhMB6Q61IkItGe9hSgOylPLb5iXgxwQJlrJMgvflGbhRojgMa47TF8gQEsBP81sLtVpkBNOFwqLvE3JVgBfakhnBZ6oQgDuO8D74Of4uZ3Bh3VQn8OMZn+LLfGNebmIUOaSA0ysrkR8WALSVBJHvWoRwbukYEP3CMF7bSWDLYFg0MLz3duxzQRVi1OTVCGHh+4s8vgcHlJ0BsM17Un5LULHPy51PJw6CsQcYgylJHCNaSXISEqhohCxFdkBKWXOcztbrxIwzkMMAEp+KJh5/tU+gIHAi3ag4sGQGYylbnMZEqji890Zvug+UxkRrOa0GxfNLO5TW5205vfBGc4xdnNw43TnOdEJzjL2b5yHs6d0nhnPOHJTnrOU5rInIA2WHCJXrDIisH8VKiI2Yv7HAggFdq4xz0S0AiGNtShDV1oRBsRUYlKlKEWrWgCNDpRjXbUox8FaUhF6tGJXrSkFT3pRhf6UIhSdKQvhSlJYwrSiRojATbtKE5nutOcJkABp3iCAHyRgwZoAQRVvGIwBzfMK4CiAZrIwSUu4QuqVtWqV8VqVrW6Va521atfBWtYxTpWspbVrFuVag400QBQGDVwgwNoxFQj0CtowakNwGte9bpXvvbVr38FbGAFO1jCFtawh0VsYhUr2LZe4ahvTWpcsRg4JYDAspd1bGZBoFnObtaznQXtZ0UbWtKO1rSlRe1pVZta1q7Wta2F7Wtl29nLHrWKVoxsQAAAOw==";
        return APIResponse::success(ScanLog::where('id', $request->scan_id)->update(['license_image' => $img]), 'Record Updated');
    }

    public function getIntercomStream(Request $request)
    {
        return APIResponse::success(ScanLog::where('id', $request->scan_id)->first(), 'Record Updated');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @throws DatabaseException
     */
    public function store(Request $request)
    {
        $user_id = auth()->user()->id;
        $cam_id = User::find($user_id)->default_camera;
        $camera = $this->cameraService->find($cam_id)['result'];
        $community_id = auth()->user()->community_id;


        $cam_id = User::find($user_id)->default_camera;

        $camera = $this->cameraService->find($cam_id)['result'];


        if (empty($camera)) {

            $camera = (Camera::where('community_id', $community_id)->where('connected_pi_id', '!=', null)->first());

            if (empty($camera)) {

                return APIResponse::error('Camera not found in community');

            }

        }

        try {

            if ($camera->connected_pi_id == null || $camera->connected_pi_id == "") {
                return APIResponse::error('Camera Pi not connected');
            }

            $connectedPi = ConnectedPie::where('id', $camera->connected_pi_id)->first();

            if (!$connectedPi) {
                return APIResponse::error('Camera Pi not connected');
            }


            $url = $connectedPi->dynamic_url;

            if ($url) {

                $factory = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
                $realtimeDatabase = $factory->createDatabase();

                $reference = $realtimeDatabase->getReference("{$connectedPi->uid}");

                $reference->set([
                    $camera->mac_address => [
                        'timestamp' => Carbon::now()
                    ],
                ]);

                try {

                    $apiData = [
                        "mac_address" => $camera->mac_address,
                        "status" => "true"
                    ];

                    $headers = [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json'
                    ];

                    $response = Http::withHeaders($headers)->post($url, $apiData);


                } catch (Exception $exception) {
                    Log::debug($exception);
                    return APIResponse::success([], 'updated successfully');
                }

            } else {


                $factory = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
                $realtimeDatabase = $factory->createDatabase();

                $pref = $realtimeDatabase->getReference("$connectedPi->uid/capture_request");

                if (isset($pref)) {
                    $status = $pref->getValue()["status"];
                    if (isset($status) && $status == "true") {
                        return APIResponse::error('Camera is busy');
                    }
                }

                $reference = $realtimeDatabase->getReference("$connectedPi->uid");

                if (isset($reference)) {

                    $reference->set([
                        'capture_request' => [
                            'mac_address' => $camera->mac_address,
                            'status' => 'true',
                            'timestamp' => Carbon::now()
                        ],
                    ]);

                }

            }

            return APIResponse::success($camera, 'updated successfully');
        } catch (Exception $exception) {
            return APIResponse::error('Something Went Wrong, Please try again');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     * @throws DatabaseException
     */
    public function show($id)
    {
        $cameraId = $id ?? 0;
        $camera = $this->cameraService->find($cameraId)['result'];
        if (empty($camera)) {
            return APIResponse::error('No Camera Exist');
        }
        $aa = ConnectedPie::where('id', $camera->connected_pi_id)->first();

        $realtimeDatabase = $this->initializeFireBase();


        if ($aa->uid) {
            $reference = $realtimeDatabase->getReference("{$aa->uid}/{$camera->mac_address}")->getValue(); ///{$aa}
        } else {
            $reference = $realtimeDatabase->getReference("cameras/{$camera->mac_address}")->getValue(); ///{$aa}
        }


        return APIResponse::success($reference, 'Camera Info');

    }

    public function initializeFireBase()
    {
        $factory = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
        $realtimeDatabase = $factory->createDatabase();
        return $realtimeDatabase;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * update camera lat long behalf of mac address
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function updateCameraLatLong(Request $request)
    {
        $macAddress = $request->mac_address ?? null;
        $camera = $this->cameraService->findByColumn('mac_address', $macAddress)['result'];
        if (empty($camera)) {
            return APIResponse::error('No Camera Exist');
        }
        $updateCamera = $this->cameraService->update([
            'lat' => $request->latitude,
            'long' => $request->longitude
        ], $camera->id);
        if ($updateCamera['bool']) {
            return APIResponse::success([], 'Updated successfully');
        }
        return APIResponse::error('Something Went Wrong, Please try again');
    }
}
