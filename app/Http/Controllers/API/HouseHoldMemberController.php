<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\ContactService;
use App\Services\UserContactService;
use Hash;
use Illuminate\Http\Request;
use App\Helpers\APIResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\HouseHoldMemberCollection;
use DB, Exception, Auth;

class HouseHoldMemberController extends Controller
{

    private $userService;
    private $userContactService;

    public function __construct(UserService $userService, UserContactService $userContactService)
    {
        $this->userService = $userService;
        $this->userContactService = $userContactService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $result = $this->userService->getHouseHoldMemberDetail(auth()->user()->house_id, 50, $search);

        return APIResponse::success($result);
    }


    /**
     * Display family members count
     *
     * @return Response
     */
    public function familyMembersCount(Request $request)
    {
        $search = $request->search;
        $result = $this->userService->getHouseHoldMemberCount(auth()->user()->house_id, 50, $search);

        return APIResponse::success(['total_family_members' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Fetch All Requests
        $residentData = $request->all();

        // Fetch Phone Number
        $residentData['phone_number'] = trim(str_replace("-", "", $residentData['phone_number']));

        //Validation and Messages
        $validation = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'phone_number' => 'required|unique:users|digits:10',
        ];
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'phone_number.required' => 'Please select phone number',
            'phone_number.digits' => 'Phone number must be at least 10 digit long.',
            'phone_number.unque' => 'Phone number already exists.',
            'email.unque' => 'Email already exists.',
        ];


        $validator = Validator::make($residentData, $validation, $messages);

        // Show First Error
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        // IF user has permissions
        if (!(Auth::user()->hasRole('family_head') || Auth::user()->hasPermission('can_manage_family'))) {
            return APIResponse::error('You dont have permission to create family members.');
        }

        // Add a Random Password
        $randomPassword = generateRandomStringWithSpecialCharacter();

        // Generate Random Password's Hash
        $residentData['password'] = Hash::make($randomPassword);
        $residentData['temp_password'] = $randomPassword;

        // Fetch and Set details from current user.
        $residentData['community_id'] = Auth::user()->community_id;
        $residentData['house_id'] = Auth::user()->house_id;
        $residentData['created_by'] = auth()->user()->id;

        // Set as Household Memeber
        $residentData['role_id'] = 5;

        // Success - To Track if All Transactions are Successful - Used for Rollback
        $success = true;

        try {

            // Begin Transaction for Multi Table Relational Query
            DB::beginTransaction();


            // Save the Family Member
            $resident = $this->userService->create($residentData);


            // Check for Errors - Used for Rollback
            $success ? $resident['bool'] : $success;

            //Assign Permission to Family Head
            if ($request->allow_parental_control) {
                $resident['result']->assignPermission('can_send_passes');
                $resident['result']->assignPermission('can_manage_family');
                $resident['result']->assignPermission('allow_parental_control');
            } else {
                ($request->can_manage_family) ? $resident['result']->assignPermission('can_manage_family') : '';
                ($request->can_send_passes) ? $resident['result']->assignPermission('can_send_passes') : '';

            }


            // Data to be saved inside Contact
            $userContactData = $request->only(['is_temporary', 'temporary_duration', 'phone_number', 'dial_code', 'email']);

            // Add Contact name to User Contact data
            $userContactData['contact_name'] = $request->first_name . " " . $request->lastname;

            //Create Contact for Resident
            $userContact = $this->userContactService->createOrUpdateUserContact($userContactData);

            //Record Any Errors
            $success ? $userContact['bool'] : $success;


            if ($success) {

                //Commit The
                DB::commit();

                //Return the Response
                return APIResponse::success(new HouseHoldMemberCollection($resident['result']), 'Success');
            } else {

                // Rollback and Show Error
                DB::rollback();
                return APIResponse::error("Something Went Wrong! Please try again later.");
            }

        } catch (Exception $e) {
            //Roll Back and Show Error
            DB::rollback();
            return APIResponse::error("Something Went Wrong! Please try again later.");
        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {

        // Delete adn Return the Response
        return APIResponse::success($this->userService->delete($id)['result'], "Success");
    }
}
