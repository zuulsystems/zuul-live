<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Helpers\APIResponse;
use App\Http\Resources\ParentalNotificationCollection;
use App\Http\Resources\ZuulNotificationCollection;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\NotificationService;


class NotificationController extends Controller
{

    //Will be Initialized by Constructor
    private $notificationService;

    /**
     * Constructor for NotificationController
     *
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Mark read parental control notifications
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function markReadParentalControlNotification(Request $request)
    {

        $validation = [
            'notification_ids' => 'required'
        ];

        $messages = [
            'notification_ids.required' => 'Please select ids'
        ];

        $data['notification_ids'] = $request->notification_ids;

        $validator = Validator::make($data, $validation, $messages);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        if ($request->notification_ids) {
            foreach ($request->notification_ids as $id) {
                $type = "parental_control";
                $result = $this->notificationService->updateNotification(['is_read' => '1'], $id, $type);
            }
        }

        if ($result['bool']) {
            return APIResponse::success([], 'Notification Cleared');
        }

        return APIResponse::error('Something went wrong');
    }

    /**
     * Mark read push notifications
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function markReadPushNotification(Request $request)
    {

        $validation = [
            'notification_ids' => 'required'
        ];

        $messages = [
            'notification_ids.required' => 'Please select ids'
        ];

        $data['notification_ids'] = $request->notification_ids;

        $validator = Validator::make($data, $validation, $messages);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        if ($request->notification_ids) {
            foreach ($request->notification_ids as $id) {
                $type = "zuul";
                $result = $this->notificationService->updateNotification(['is_read' => '1'], $id, $type);
            }
        }

        if ($result['bool']) {
            return APIResponse::success([], 'Notification Cleared');
        }

        return APIResponse::error('Something went wrong');
    }

    /**
     * Get parental control notifications
     * @param Request $request
     * @return JsonResponse
     */
    public function getParentalControlNotifications(Request $request)
    {
        $notifications = $this->notificationService->getNotifications(auth()->id(), 'parental_control', 30);
        return APIResponse::success(['list' => $notifications]);
    }

    /**
     * Get parental control notifications
     * @param Request $request
     * @return JsonResponse
     */
    public function getZuulControlNotifications(Request $request)
    {
        $notifications = $this->notificationService->getNotifications(auth()->id(), 'zuul', 30);
        return APIResponse::success(['list' => $notifications]);
    }

    /**
     * get Notification Counts
     * @param Request $request
     * @return JsonResponse
     */
    public function getNotificationCount(Request $request)
    {
        $zuulNotificationCount = $this->notificationService->getTotalNotifications(auth()->id());
        $parentalControlNotificationCount = $this->notificationService->getTotalNotifications(auth()->id(), 'parental_control');
        return APIResponse::success([
            'parental_control_unread' => $parentalControlNotificationCount,
            'zuul_unread' => $zuulNotificationCount,
        ]);
    }


    /**
     * @param $id
     * @return JsonResponse
     * @throws Exception
     */
    public function removeNotification($id)
    {
        $this->notificationService->delete($id);
        return APIResponse::success([], 'Notification Cleared');
    }
}
