<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventCollection;
use App\Services\EventService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class EventController extends Controller
{
    use UserCommon;

    public $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * -------------------------------------------
     * Index of Events
     * -------------------------------------------
     *
     * This method retrieves a list of events based on the provided search parameter and the authenticated user.
     *
     * @param Request $request The request object containing the search parameter.
     * @return JsonResponse The JSON response with the result of the events.
     *
     * @throws Exception If an error occurs during the process of retrieving the events.
     */
    public function index(Request $request): JsonResponse
    {
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $result = $this->eventService->getEvents(30, $search, auth()->id());
        return APIResponse::success(['list' => EventCollection::collection($result['result'])], null);
    }

    /**
     * -------------------------------------------
     * Store Event
     * -------------------------------------------
     *
     * This method creates a new event based on the provided data from the request.
     *
     * @param Request $request The request object containing the event data.
     * @return JsonResponse The JSON response with the result of the event creation.
     *
     * @throws Exception If an error occurs during the process of creating the event.
     */
    public function store(Request $request): JsonResponse
    {
        $eventData = $request->all();
        $eventData['created_by'] = auth()->id();
        $eventData['community_id'] = auth()->user()->community_id;

        $event = $this->eventService->create($eventData);
        if ($event['bool']) {
            return APIResponse::success(new EventCollection($event['result']), 'event created successfully');
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * -------------------------------------------
     * Update Event
     * -------------------------------------------
     *
     * This method updates an existing event based on the provided data and event ID.
     *
     * @param Request $request The request object containing the updated event data.
     * @param int $id The ID of the event to be updated.
     * @return JsonResponse The JSON response indicating the result of the event update.
     *
     * @throws Exception If an error occurs during the process of updating the event.
     */
    public function update(Request $request, $id): JsonResponse
    {
        $event = $this->eventService->getEventWithRight(auth()->id(), $id)['result']; //user can only update there created events
        if ($event) {
            $event->name = $request->name;
            $event->save();
            return APIResponse::success([], 'event updated successfully');
        } else {
            return APIResponse::error('Error: Event Not Found');
        }
    }

    /**
     * -------------------------------------------
     * Destroy Event
     * -------------------------------------------
     *
     * This method deletes an existing event based on the provided event ID and user's right to delete.
     *
     * @param int $id The ID of the event to be deleted.
     * @return JsonResponse The JSON response indicating the result of the event deletion.
     *
     * @throws Exception If an error occurs during the process of deleting the event.
     */
    public function destroy($id): JsonResponse
    {
        $event = $this->eventService->getEventWithRight(auth()->id(), $id)['result']; //user can only delete there created events
        if ($event) {
            $event->delete();
            return APIResponse::success([], 'event deleted');
        } else {
            return APIResponse::error('Error: Event Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }
}
