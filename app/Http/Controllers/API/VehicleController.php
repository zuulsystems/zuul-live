<?php

namespace App\Http\Controllers\API;


use App\Services\NotificationService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Vehicle;
use App\Models\BannedCommunityUser;
use App\Models\PassUser;
use App\Models\ScanLog;
use App\Models\StaticScanLog;
use App\Models\User;
use App\Services\PassUserService;
use App\Services\UserService;
use App\Services\VehicleService;
use App\Helpers\APIResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\VehicleCollection;
use Illuminate\Http\Request;
use App\Services\ScanLogService;
use App\Services\StaticScanLogService;
use App\Models\Vehicle as VehicleModel;

class VehicleController extends Controller
{

    //Will be Initialized by Constructor
    protected $scanLogService;
    private $vehicleService;
    private $notificationService;
    private $userService;
    private $passUserService;

    /**
     * Constructor for VehicleController
     *
     * @param VehicleService $vehicleService
     * @param UserService $userService
     * @param PassUserService $passUserService
     */
    public function __construct(
        NotificationService  $notificationService,
        VehicleService       $vehicleService,
        UserService          $userService,
        PassUserService      $passUserService,
        ScanLogService       $scanLogService,
        StaticScanLogService $staticscanlogservice
    )
    {
        $this->notificationService = $notificationService;
        $this->vehicleService = $vehicleService;
        $this->userService = $userService;
        $this->passUserService = $passUserService;
        $this->scanLogService = $scanLogService;
        $this->staticscanlogservice = $staticscanlogservice;
    }

    public function index(Request $request)
    {
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : "";
        $vehicle = $this->vehicleService->getByUserId(auth()->id(), 30, $search);
        if ($vehicle['result']) {
            return APIResponse::success(['list' => VehicleCollection::collection($vehicle['result'])], 'Vehicle list');
        }
        return APIResponse::error('Something went wrong');
    }

    public function houseResidentsVehicles()
    {

        $vehicles = [];

        $familyHeadVehicles = VehicleModel::where('user_id', auth()->user()->id)->where('deleted_at', NULL)->get();

        if (!empty($familyHeadVehicles)) {
            foreach ($familyHeadVehicles as $familyHeadVehicle) {
                if (!empty($familyHeadVehicle)) {
                    array_push($vehicles, $familyHeadVehicle);
                }
            }
        }

        $familyMembers = User::where(['role_id' => 5, 'house_id' => auth()->user()->house_id])->get();

        foreach ($familyMembers as $familyMember) {
            $houseMemberVehicles = VehicleModel::where('user_id', $familyMember->id)->get();

            if (!empty($houseMemberVehicles)) {
                foreach ($houseMemberVehicles as $houseMemberVehicle) {
                    if (!empty($houseMemberVehicle)) {
                        array_push($vehicles, $houseMemberVehicles);
                    }
                }
            }
        }

        return APIResponse::success($vehicles, 'familyVehicles');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function store(Request $request)
    {
        //Get the Validator for Vehicle
        $validator = $this->validator($request);

        // Show Validation Errors if Present
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        // Store the Vehicle Details
        $vehicleData = $request->all();
        $vehicleData['community_id'] = auth()->user()->community_id;
        $vehicleData['user_id'] = auth()->id();
        $vehicleData['license_plate'] = removeSpaceFromString($request->license_plate);
        $vehicle = $this->vehicleService->create($vehicleData);

        // Return the Appropiriate Response
        return $this->response($vehicle);

    }

    /**
     * Creates Validator for The Vehicle Resource
     *
     * @param Request $request
     * @param integer $id
     * @return Illuminate\Support\Facades\Validator
     */
    private function validator(Request $request)
    {

        //Return Validator For Vehicles
        return $validator = Validator::make($request->all(), [
            'license_plate' => 'required',
        ]);
    }

    /**
     * Creates Validator for The Vehicle Resource
     *
     * @param App\Services\VehicleService $vehicle
     * @param bool $insert
     * @return App\Helpers\APIResponse
     */
    private function response($vehicle, $insert = true)
    {
        // If Save or Edit is Success
        if ($vehicle['bool']) {
            return $insert ? APIResponse::success(new VehicleCollection($vehicle['result']), "Success")
                : APIResponse::success([], "Success");
        }

        //Else Show Error
        APIResponse::error("Something Went Wrong!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        // Perform Vehicle Deletion
        $vehicle = $this->vehicleService->delete($id);

        // Return the Response
        return $this->response($vehicle, false);
    }

    /**
     * add vehicle by guard
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addVehicleByGuard(Request $request)
    {

        $check_banned_user = BannedCommunityUser::where('license_plate', $request->license_plate)->where('community_id', auth()->user()->community_id)->first();

        if ($check_banned_user != null) {
            $scan_log_id = $request->scan_log_id;

            ScanLog::where('id', $scan_log_id)->update([
                'log_text' => 'THIS GUEST IS BANNED FROM THE COMMUNITY. DENY ENTRY!'
            ]);

            return APIResponse::error2('THIS GUEST IS BANNED FROM THE COMMUNITY. DENY ENTRY!');
        }

        // Create Validator
        $validator = Validator::make($request->all(), [
            'pass_user_id' => 'required|exists:pass_users,id',
            'license_plate' => 'required',
            'user_id' => 'required',
        ]);

        // Show First Error
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $user = $this->userService->find($request->user_id)['result'];
        if (empty($user)) {
            return APIResponse::error("No User Found.");
        }


        // Store the Vehicle Details
        $vehicleData = $request->all();
        $vehicleData['community_id'] = $user->community_id;
        $vehicleData['user_id'] = $user->id;
        $vehicleData['license_plate'] = removeSpaceFromString($vehicleData['license_plate']);

        $vehicle = $this->vehicleService->updateOrCreate([
            'license_plate' => removeSpaceFromString($vehicleData['license_plate']),
            'user_id' => $user->id,
        ], $vehicleData);


        if ($vehicle['bool']) {

            $result = $this->passUserService->update([
                'vehicle_id' => $vehicle['result']->id
            ], $request->pass_user_id);

            $scanLog = $this->scanLogService->set_Pass_Vehicle($request->pass_user_id)['result'];
            $scanLog = $this->scanLogService->find($request->scan_log_id)['result']; //$scanLog;
            $scanLogId = $request->scan_log_id;

            $staticScanLogData = array(
                'license_plate' => $vehicle['result']->license_plate,
                'color' => $vehicle['result']->color,
                'make' => $vehicle['result']->make,
                'model' => $vehicle['result']->model,
                'year' => $vehicle['result']->year,
                'license' => $vehicle['result']->license_plate . " " . $vehicle['result']->color . " " . $vehicle['result']->make . " " . $vehicle['result']->model . " " . $vehicle['result']->year
            );

            $staticScanLog = $this->staticscanlogservice->updateByScanLogId($staticScanLogData, $scanLogId);

            // get pass data and send as parameter
            $passUserById = PassUser::where('id', $request->pass_user_id)->first();

            if ($passUserById) {
                $npass = $this->passUserService->getPassByQrcode($passUserById->qr_code);
                $this->passUserService->sendPassScanNotificationToUser($npass, $scanLog);

                $passSentByEmail = $npass->createdBy->email;

                // New Logic
                $senderFullName = $scanLog->passUser->pass->createdBy->fullName;
                $senderFormattedPhone = $scanLog->passUser->pass->createdBy->FormattedPhone;
                $senderFormattedAddress = $scanLog->passUser->pass->createdBy->formattedAddress;

                if (!empty($scanLog->guard_display_name)) {
                    $receiverFullName = $scanLog->guard_display_name;
                } else if (!empty($scanLog->passUser->user->fullName)) {
                    $receiverFullName = $scanLog->passUser->user->fullName;
                }

                $receiverFormattedPhone = $scanLog->passUser->user->FormattedPhone;
                $receiverFormattedAddress = $scanLog->passUser->user->formattedAddress;


                $community = $scanLog->passUser->pass->community->name;
                $event = $scanLog->passUser->pass->event->name;
                $start = $scanLog->passUser->pass->pass_start_date;
                $expire = $scanLog->passUser->pass->pass_date;
                $scanAt = $scanLog->created_at;
                $scannedBy = $scanLog->ScanByName;


                $licensePlate = '';
                $color = '';
                $make = '';
                $model = '';
                $year = '';

                if (!empty($scanLog->passUser->vehicle)) {
                    $licensePlate = $scanLog->passUser->vehicle->license_plate;
                    $color = $scanLog->passUser->vehicle->color;
                    $make = $scanLog->passUser->vehicle->make;
                    $model = $scanLog->passUser->vehicle->model;
                    $year = $scanLog->passUser->vehicle->year;
                }

                $data = array(
                    'sender_full_name' => $senderFullName,
                    'sender_formatted_phone' => $senderFormattedPhone,
                    'sender_formatted_address' => $senderFormattedAddress,
                    'recipient_full_name' => $receiverFullName,
                    'recipient_formatted_phone' => $receiverFormattedPhone,
                    'recipient_formatted_address' => $receiverFormattedAddress,
                    'community' => $community,
                    'event' => $event,
                    'start' => $start,
                    'expires' => $expire,
                    'scan_date' => $scanAt,
                    'scan_by' => $scannedBy,
                    'license_plate' => $licensePlate,
                    'color' => $color,
                    'make' => $make,
                    'model' => $model,
                    'year' => $year,
                    'scan_log_id' => $scanLogId,
                );
                // New Logic End
                $staticScanLogData3 = StaticScanLog::create($data);
                $resultOfScanLog = $this->staticscanlogservice->find($staticScanLogData3->id);

                //Mark:- Log for email data at scan qr code
                Log::emergency("------------------------------------------------------");
                Log::emergency($scanLogId);
                Log::emergency($resultOfScanLog);
                Log::emergency(json_encode($passSentByEmail));
                Log::emergency(json_encode($resultOfScanLog));
                sendGuestArrivalEmailToResident($passSentByEmail, $resultOfScanLog, true);
            }

            if ($result['bool']) {

                return APIResponse::success(new VehicleCollection($vehicle['result']), $vehicle['result']->id);
            } else {
                return APIResponse::error("Something Went Wrong!");
            }
        } else {
            return APIResponse::error("Something Went Wrong!");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //Get the Validator
        $validator = $this->validator($request);

        //Show Errors if Validator Fails
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        // Perform Update
        if (!empty($request->license_plate)) {
            $vehicleData['license_plate'] = removeSpaceFromString($request->license_plate);
        }
        $vehicle = $this->vehicleService->update($request->all(), $id);

        //Return the Appropiriate API response
        return $this->response($vehicle, false);
    }
}
