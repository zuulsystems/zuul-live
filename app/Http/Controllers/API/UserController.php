<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserPhone;
use App\Http\Resources\UserCollection;
use App\Mail\DynamicEmail;
use App\Repositories\UserRepository;
use App\Services\RoleService;
use App\Services\ScanLogService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Services\HouseService;
use Illuminate\Support\Str;
use App\Services\UserService;

class UserController extends Controller
{
    use UserCommon;

    /**
     * @var UserRepository
     */
    protected $userRepo;
    protected $roleService;
    protected $scanLogService;
    protected $houseService;
    protected $userService;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     * @param RoleService $roleService
     * @param ScanLogService $scanLogService
     */
    public function __construct(UserRepository $userRepository, RoleService $roleService, ScanLogService $scanLogService, HouseService $houseService, UserService $userService)
    {
        $this->userRepo = $userRepository;
        $this->roleService = $roleService;
        $this->scanLogService = $scanLogService;
        $this->houseService = $houseService;
        $this->userService = $userService;
    }

    /**
     * -------------------------------------------
     * Check Pass Permission
     * -------------------------------------------
     *
     * This method checks the pass permission for a user based on their phone number.
     * It delegates the responsibility to the UserService to perform the actual permission check.
     * The UserService is responsible for retrieving the user by the provided phone number and checking if they are a head of the family or have the "can_send_passes" permission.
     * It returns a JSON response indicating the pass permission result.
     * @param string $phone The phone number of the user.
     * @return JsonResponse The JSON response indicating the pass permission result.
     * @throws Exception If an error occurs during the pass permission check.
     * @see UserService
     */
    public function checkPassPerm($phone): JsonResponse
    {
        return $this->userService->checkUserPassPermission($phone);
    }

    /**
     * -------------------------------------------
     * Check Pass Permission
     * -------------------------------------------
     *
     * This method saves the FCM token for the currently authenticated user.
     * @param Request $request The request object containing the FCM token.
     * @return JsonResponse The JSON response indicating the result of the token update.
     * @throws Exception If an error occurs during the process of saving the FCM token.
     * @see UserService
     */
    public function saveFcmToken(Request $request): JsonResponse
    {
        return $this->userService->saveFcmToken($request);
    }

    /**
     * -------------------------------------------
     * User Signup
     * -------------------------------------------
     *
     * This method handles the signup process for a new user.
     * @param Request $request The request object containing user signup data.
     * @return JsonResponse The JSON response with the signup result.
     * @throws Exception If an error occurs during the signup process.
     * @see UserService
     */
    public function store(Request $request): JsonResponse
    {
        return $this->userService->userSignup($request);
    }

    /**
     * -------------------------------------------
     * Get User Profile
     * -------------------------------------------
     *
     * This method retrieves the profile of the authenticated user.
     * @return JsonResponse The JSON response with the user profile data.
     * @throws Exception If an error occurs during the process of retrieving the user profile.
     * @see UserService
     */
    public function show(): JsonResponse
    {
        return $this->userService->getUserProfile();
    }

    /**
     * -------------------------------------------
     * Forgot Password
     * -------------------------------------------
     *
     * This method handles the process of sending a password reset link to the user's email based on their phone number.
     *
     * @param string $phone The phone number of the user.
     * @return JsonResponse The JSON response with the result of sending the password reset link.
     * @throws Exception If an error occurs during the process of sending the password reset link.
     * @see UserService
     */
    public function forgetPassword(Request $request): JsonResponse
    {
        return $this->userService->userForgotPassword($request);
    }

    /**
     * -------------------------------------------
     * Update Password
     * -------------------------------------------
     *
     * This method handles the process of updating the user's password based on the provided request data.
     *
     * @param Request $request The request object containing the password update data.
     * @return JsonResponse The JSON response with the result of updating the password.
     * @throws Exception If an error occurs during the process of updating the password.
     * @see UserService
     */
    public function updatePassword(Request $request): JsonResponse
    {
        return $this->userService->userUpdatePassword($request);
    }

    /**
     * -------------------------------------------
     * Get All Roles
     * -------------------------------------------
     *
     * This method retrieves all the roles from the RoleService.
     * @return JsonResponse The JSON response with the list of roles.
     * @throws Exception If an error occurs during the retrieval of roles.
     */
    public function roles()
    {
        $data = $this->roleService->all();
        return APIResponse::success(['list' => $data['result']], 'All Roles');
    }

    /**
     * -------------------------------------------
     * Check if Email is Already in Use
     * -------------------------------------------
     *
     * This method checks if the provided email is already in use by another user.
     * @param Request $request The request object containing the email to check.
     * @return JsonResponse The JSON response indicating if the email is already in use.
     * @throws Exception If an error occurs during the email check process.
     */
    public function checkEmailAlreadyInUse(Request $request): JsonResponse
    {
        return $this->userService->checkEmailAlreadyInUse($request);
    }

    /**
     * -------------------------------------------
     * Become a Resident
     * -------------------------------------------
     *
     * This method handles the process of becoming a resident by submitting the necessary information.
     * @param Request $request The request object containing the resident information.
     * @return JsonResponse The JSON response with the result of becoming a resident.
     * @throws Exception If an error occurs during the process of becoming a resident.
     */
    public function becomeResident(Request $request): JsonResponse
    {
        return $this->userService->userBecomeResident($request);
    }

    /**
     * -------------------------------------------
     * Get Active Country Dial Codes
     * -------------------------------------------
     *
     * This method retrieves the active country dial codes.
     * @return JsonResponse The JSON response with the active country dial codes.
     * @throws Exception If an error occurs during the retrieval of the active country dial codes.
     */
    public function getActiveCountryDialCodes(): JsonResponse
    {
        return $this->userService->getActiveCountryDialCodes();
    }

    /**
     * -------------------------------------------
     * Update Scan Log
     * -------------------------------------------
     *
     * This method updates the scan log with the provided ID based on the request data.
     * @param Request $request The request object containing the scan log data.
     * @param int $id The ID of the scan log to be updated.
     * @return JsonResponse The JSON response with the result of the scan log update.
     * @throws Exception If an error occurs during the scan log update process.
     */
    public function updateScanLog(Request $request, $id): JsonResponse
    {
        return $this->userService->updateScanLog($request, $id);
    }

    /**
     * -------------------------------------------
     * Update User Notification Settings
     * -------------------------------------------
     *
     * This method updates the notification settings of the user based on the provided request data.
     * @param Request $request The request object containing the notification settings data.
     * @return JsonResponse The JSON response with the result of the notification settings update.
     * @throws Exception If an error occurs during the notification settings update process.
     */
    public function updateUserNotificationSettings(Request $request): JsonResponse
    {
        return $this->userService->updateUserNotificationSettings($request);
    }

    /**
     * -------------------------------------------
     * User Profile
     * -------------------------------------------
     *
     * This method retrieves the profile information of the user based on the provided request data.
     * @param Request $request The request object containing the user profile data.
     * @return JsonResponse The JSON response with the user profile information.
     * @throws Exception If an error occurs during the process of retrieving the user profile.
     */
    public function profile(Request $request): JsonResponse
    {
        return $this->userService->userProfile($request);
    }

    /**
     * -------------------------------------------
     * Delete Temporary User by Self
     * -------------------------------------------
     *
     * This method allows the user to delete their own temporary user account.
     * @return JsonResponse The JSON response indicating the result of the deletion.
     * @throws Exception If an error occurs during the process of deleting the temporary user.
     */
    public function tempUserDeleteBySelf(): JsonResponse
    {
        return $this->userService->temporaryUserDeleteBySelf();
    }

    /**
     * -------------------------------------------
     * Update User Profile
     * -------------------------------------------
     *
     * This method updates the profile of a specific user identified by the provided ID.
     * @param Request $request The request object containing the updated user profile data.
     * @param int $id The ID of the user to update.
     * @return JsonResponse The JSON response indicating the result of the update.
     * @throws Exception If an error occurs during the process of updating the user profile.
     */
    public function updateUserProfile(Request $request, $id): JsonResponse
    {
        return $this->userService->updateUserProfile($request, $id);
    }

    /**
     * -------------------------------------------
     * Check Phone Existence
     * -------------------------------------------
     *
     * This method checks the existence of a user with the provided phone number.
     * @param UserPhone $userPhone The user phone number object.
     * @return JsonResponse The JSON response indicating the result of the phone existence check.
     * @throws Exception If an error occurs during the process of checking the phone existence.
     */
    public function checkPhoneExistence(UserPhone $userPhone): JsonResponse
    {
        return $this->userService->checkPhoneExistence($userPhone);
    }

    /**
     * -------------------------------------------
     * Update User Profile By File
     * -------------------------------------------
     *
     * This method updates the user profile based on the data provided in the file.
     * @param Request $request The request object containing the file data.
     * @param int $id The ID of the user.
     * @return JsonResponse The JSON response indicating the result of the user profile update.
     * @throws Exception If an error occurs during the process of updating the user profile.
     */
    public function updateUserProfileByfile(Request $request, $id): JsonResponse
    {
        return $this->userService->updateUserProfileByFile($request, $id);
    }

    /**
     * Verify Guard Login
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function verifyGuardLogin(Request $request)
    {
        $user = $this->userRepo->checkVerificationCode($request->code);
        if (!$user['result']) {
            return APIResponse::error('code does not exist');
        }
        $user = new  UserCollection($user['result']);

        $authAttempt = Auth::loginUsingId($user['id']);

        if ($authAttempt) {
            $data = array(
                'user_id' => $user['id'],
                'token' => $user->toArray(null)['token']
            );
            $userData = ['is_verified' => '1', 'verification_code' => null];
            $result = $this->userRepo->update($userData, $user['id']);
            $user = $this->userRepo->find($user['id']);
            if ($result['bool']) {
                return APIResponse::success(['user' => new  UserCollection($user['result'])], 'Code Matched');
            }
            return APIResponse::error('Something went wrong');
        } else {
            return APIResponse::error('Login attempt fail');
        }
    }

    /**
     * Verify Email Address
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function verifyEmailAddress(Request $request)
    {
        $user = Auth::user();
        if (empty($request->email)) {
            return APIResponse::error('Invalid / Empty Email');
        }
        $emailAddress = $request->email;
        //update email
        $result = $this->userRepo->update([
            'email' => $request->email
        ], $user->id);
        if ($result['bool']) {
            $ownerUser = $this->userRepo->getByAttribute([
                'email~!=' => null,
                'email' => $emailAddress,
                'email_owner_count' => 1,
            ]);
            if ($ownerUser->isEmpty()) {
                return APIResponse::error('Invalid / Unknown Email Owner');
            }
            $ownerUser = $ownerUser->first();
            $verificationCode = emailVerificationCode(); // generate verification code
            $emailOwnerCount = $this->userRepo->findAllUserByEmail($emailAddress)['result']->count();
            //logged in user
            $this->userRepo->update([
                'email_verification_code' => $verificationCode,
                'email_owner_count' => $emailOwnerCount,
                'is_suspended' => '1'
            ], $user->id);

            $email = $this->sendDuplicateVerificationEmail($ownerUser);
            if ($email['bool']) {
                return APIResponse::success([], 'Email Sent Successfully');
            } else {
                return APIResponse::error('Something went wrong');
            }
        } else {
            return APIResponse::error('Something went wrong');
        }
    }

    private function sendDuplicateVerificationEmail($ownerUser)
    {
        if (!empty($ownerUser)) {
            $authUser = Auth::user();
            $emailData = array();
            $templateData = array();
            $subject = "ZUUL Systems - Duplicate Verification Email";
            $emailTo = $ownerUser->email;
            //define template variables
            $templateData['receiver'] = $ownerUser->first_name;
            $templateData['full_name'] = $authUser->full_name;
            $templateData['phone_number'] = numberFormat($authUser->phone_number, Str::start($authUser->dial_code, '+'));
            $templateData['verification_code'] = $authUser->email_verification_code;
            //define email variables
            $emailTemplate = getEmailTemplate('duplicate_verification_email');
            $template = $emailTemplate->template ?? "";
            $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
            $emailData['subject'] = $subject;
            $emailData['emailTo'] = $emailTo;


            try {
                Mail::to($emailTo)->send(new DynamicEmail($emailData));
                return [
                    'bool' => true,
                    'message' => 'Email Sent Successfully'
                ];
            } catch (Exception $exception) {
                return [
                    'bool' => false,
                    'message' => $exception->getMessage()
                ];
            }
        }

        return [
            'bool' => false,
            'message' => 'No Record Found'
        ];
    }

    /**
     * get License Plate Status
     * @param Request $request
     * @return JsonResponse
     */
    public function getLicenseLockStatus(Request $request)
    {
        return APIResponse::success(['locked' => \Auth()->user()->hasPermission('is_license_locked')], 'Get Licence Lock Status Successfully');
    }


    /**
     * Update user fields
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function updateUserFields(Request $request)
    {

        $userData = $request->all();

        $user = $this->userRepo->update($userData, auth()->user()->id);

        if ($user['result']) {
            return response()->json([
                'bool' => true,
                'result' => "Guard fields updated successfully",
                'message' => "",
            ]);
        }

        return APIResponse::error('Something went wrong');
    }

}
