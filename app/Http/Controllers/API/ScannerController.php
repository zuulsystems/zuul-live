<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Models\Camera;
use App\Models\Community;
use App\Models\RemoteGuard;
use App\Models\StaticRfidLog;
use App\Repositories\UserRepository;
use App\S3ImageHelper;
use App\Services\CommunityService;
use App\Services\RfidDateLimitService;
use App\Services\RfidLogService;
use App\Services\RfidService;
use App\Services\RoleService;
use App\Services\ScanLogService;
use App\Services\ScannerService;
use Carbon\Carbon;
use Exception;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ScannerController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepo;
    protected $roleService;
    protected $scanLogService;
    protected $scannerService;
    protected $rfidService;
    protected $rfidDateLimitService;
    protected $rfidLogService;


    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     * @param RoleService $roleService
     * @param ScanLogService $scanLogService
     * @param ScannerService $scannerService
     * @param RfidService $rfidService
     * @param RfidDateLimitService $rfidDateLimitService
     * @param RfidLogService $rfidLogService
     */
    public function __construct(
        UserRepository       $userRepository,
        RoleService          $roleService,
        ScanLogService       $scanLogService,
        ScannerService       $scannerService,
        RfidService          $rfidService,
        RfidDateLimitService $rfidDateLimitService,
        RfidLogService       $rfidLogService

    )
    {
        $this->userRepo = $userRepository;
        $this->roleService = $roleService;
        $this->scanLogService = $scanLogService;
        $this->scannerService = $scannerService;
        $this->rfidService = $rfidService;
        $this->rfidDateLimitService = $rfidDateLimitService;
        $this->rfidLogService = $rfidLogService;
    }

    /**
     * @param Request $request
     */
    public function sendScanIdToCamera(Request $request)
    {
        echo "Hello";
    }

    /**
     * @param Request $request
     * @param $scanLogId
     * @return JsonResponse
     * @throws Exception
     */
    public function updateLicensePlateImageInScanLog(Request $request, $scanLogId)
    {


        $scannerId = ($request->has('scanner_id') && !empty($request->scanner_id)) ? $request->scanner_id : null;
        $scanLog = $this->scanLogService->find($scanLogId);

        if (empty($scanLog['result'])) {
            return APIResponse::error('Something went wrong.Please try againn.');
        }
        $scanner = $this->scannerService->find($scannerId);
        if (empty($scanner['result'])) {
            return APIResponse::error('Something went wrong.Please try againnnn.');
        }
        if (!$request->has('license_plate_image') && empty($request->license_plate_image)) {
            return APIResponse::error('Please select license plate imagee');
        }

        $license_plate_image = $request->license_plate_image;
        if (base64_decode($license_plate_image, true) == true) {

            $license_plate_image = $this->delete_all_between("data:image", "base64,", $license_plate_image);
            $ls_image = base64_decode($license_plate_image);
            $licenceName = md5(uniqid()) . '.' . 'png';
            $pathAndFileName = "dynamic/lic_images/" . $licenceName;
            $s3uploadResp = S3ImageHelper::storeImageInBucket($pathAndFileName, $ls_image);
            $s3uploadRespGoogleCloud = S3ImageHelper::storeImageInBucketGoogleCloudStorage($pathAndFileName, $ls_image);
            if ($s3uploadResp || $s3uploadRespGoogleCloud) {
                $scan = $this->scanLogService->update([
                    'scanner_id' => $scanner['result']->id,
                    'license_plate_image' => $pathAndFileName
                ], $scanLog['result']->id);
                if ($scan['bool']) {
                    return APIResponse::success([], 'Updated Successfully.');
                } else {
                    return APIResponse::error('Something went wrong.Please try again.');
                }
            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    private function delete_all_between(string $beginning, string $end, string $string)
    {
        return S3ImageHelper::delete_all_between($beginning, $end, $string);
    }

    /**
     * get all rfid codes
     * @param Request $request
     * @return JsonResponse
     */
    public function getRfidCodes(Request $request)
    {

        $authUser = $request->header('php-auth-user');
        $authPass = $request->header('php-auth-pw');
        $scanner = $this->scannerService->getByCredentials($authUser, $authPass)['result'];
        if (empty($scanner)) {
            return APIResponse::error('Something went wrong. Please try again.');
        }
        $rfids = array();
        $pkr = [];
        $communityRfids = $this->rfidService->getUnsuspendedRfids($scanner->community_id)['result']->get();

        if ($communityRfids->isNotEmpty()) {
            $rfids = $communityRfids->pluck('rfid_tag_num');
        }

        if (!empty($rfids)) {
            foreach ($rfids as $rfid) {
                if ($this->rfidDateLimitService->checkIfScanTagLieInTimeRange($rfid)) {
                    Log::info('non-numeric value', [$scanner->rfid_multiplier]);
                    Log::info('non-numeric value', [$scanner->community->rfid_site_code]);
                    Log::info('non-numeric value', [$rfid]);
                    if ($rfid) {
                        if ($scanner->community->id != 61) {
                            $rfidCode = $scanner->rfid_multiplier * $scanner->community->rfid_site_code + $rfid;
                            $pkr[] = $rfidCode;
                        } else {
                            $rfidCode = $rfid;
                            $pkr[] = $rfidCode;
                        }


                    }

                }
            }
        }
        $list = implode("\n", $pkr);
        return response($scanner->mac_address . "\n" . $list, 200)->header('Content-Type', 'text/plain');
    }

    /**
     * get all rfid codes by kiosk
     * @param Request $request
     * @return JsonResponse
     */
    public function getRfidCodesWithKiosk(Request $request, $id)
    {

        $remoteGuard = RemoteGuard::find($id);

        if (empty($remoteGuard)) {
            return APIResponse::error('Remote Guard not found');
        }


        $rfids = [];
        $pkr = [];
        $communityRfids = $this->rfidService->getUnsuspendedRfids($remoteGuard->community_id)['result']->get();

        foreach ($communityRfids as $rfid) {


            if ($remoteGuard->community_id != 61) {
                if (empty($remoteGuard->rfid_multiplier) || $remoteGuard->rfid_multiplier == null) {
                    $remoteGuard->rfid_multiplier = 0;
                }

                $rfidCode = intval($remoteGuard->rfid_multiplier) * intval($remoteGuard->community->rfid_site_code) + intval($rfid['rfid_tag_num']);

                $rfid['code'] = $rfidCode;
            } else {
                $rfidCode = $rfid;
                $rfid['code'] = $rfidCode;
            }

            $rfids[] = $rfid;


        }


        $list = $rfids;// implode("\n", $pkr);
        return APIResponse::success(["data" => $list], "RFID Scanned Successfully");
    }

    /**
     * @param Request $request
     * @param $rfidCode
     * @return JsonResponse
     * @throws Exception
     */
    public function scannedRfidCode(Request $request, $rfidCode)
    {

        $authUser = $request->header('php-auth-user');
        $authPass = $request->header('php-auth-pw');

        $scanner = $this->scannerService->getByCredentials($authUser, $authPass)['result'];
        Log::debug("headers", [$authUser]);
        Log::debug("headers", [$authPass]);
        Log::debug("headers", [$scanner]);

        if (!$scanner) {
            return APIResponse::error('Something went wrong.Please try again.');
        }


        $rfidTagNum = $rfidCode - $scanner->rfid_multiplier * $scanner->community->rfid_site_code;

        if ($scanner->community->id == 61) {
            $rfidTagNum = $rfidCode;
        }

        $community_id = $scanner->community->id;
        $rfid876 = $this->rfidService->findByTagNumUnsuspendedRfid($rfidTagNum, $community_id);

        $isSuccess = '1';
        $logText = "RFID Scanned Successfully";
        $errorGenerated = false;

        $rfid = $rfid876['result'];

        if ($rfid876['bool'] == false) {
            $isSuccess = '0';
            $logText = $rfid876['message'];
            $errorGenerated = true;
        } else if (empty($rfid)) {
            $isSuccess = '0';
            $logText = "RFID Tag Number not found";
            $errorGenerated = true;
        } else if (empty($scanner)) {
            $isSuccess = '0';
            $logText = "Invalid RFID Code";
            $errorGenerated = true;
        } else if ($rfid && $rfid->status == 0) {
            $isSuccess = '0';
            $logText = "Inactive RFID Code";
            $errorGenerated = true;
        } else if (empty($rfid->user)) {
            $isSuccess = '0';
            $logText = "Invalid User Attached";
            $errorGenerated = true;
        } else if ($rfid && $scanner->community_id != $rfid->community->id) {
            $isSuccess = '0';
            $logText = "RFID Mismatch with Reader Community";
            $errorGenerated = true;
        }

        if (!$errorGenerated && !$this->rfidDateLimitService->checkIfScanTagLieInTimeRange($rfidTagNum)) {
            $isSuccess = '0';
            $logText = "RFID Mismatch with given time range";
            $errorGenerated = true;
        }


        // At this point rfid verified successfully
        if ($rfid && !$errorGenerated) {
            $rfidLog = $this->rfidLogService->create([
                'type' => 'rfid',
                'scanner_id' => $scanner->id,
                'community_id' => $rfid ? $rfid->community_id : 0,
                'is_success' => $isSuccess,
                'rfid_id' => $rfid ? $rfid->id : 0,
                'rfids_tag_num_static' => $rfid ? $rfid->rfid_tag_num : 0,
                'log_text' => $logText,
                'scan_at' => Carbon::now(),
                'vehicle_info' => $rfid->vehicle_info

            ]);

            $residentName = '';

            if (!empty($rfid->user)) {
                $residentName = $rfid->user->first_name . " " . $rfid->user->last_name;
            }

            $community = Community::find($rfid->community_id);

            $scanAt = (!empty($rfidLog['result']->scan_at)) ? date('Y-m-d h:i a', strtotime($rfidLog['result']->scan_at)) : "";

            StaticRfidLog::create([
                'rfid' => $rfid->rfid_tag_num,
                'scanner' => $scanner->name,
                'resident_name' => $residentName,
                'community_name' => $community->name,
                'type' => 'rfid',
                'is_success' => $isSuccess,
                'log_text' => $logText,
                'scan_at' => $scanAt,
                'vehicle_info' => $rfid->vehicle_info,
                'rfid_log_id' => $rfidLog['result']->id,
            ]);


            if ($rfidLog['bool'] && $isSuccess == '1') {
                return response("$scanner->mac_address\nRFID Scanned Successfully", 200)->header('Content-Type', 'text/plain');
            } else if ($rfidLog['bool'] && $isSuccess == '0') {
                if ($scanner->community->id == 61) {
                    return response("$scanner->mac_address\nRFID Scanned Successfully", 200)->header('Content-Type', 'text/plain');
                }
                return response($rfidLog, 403)->header('Content-Type', 'text/plain');
            }
        } else {
            StaticRfidLog::create([
                'rfid' => $rfidTagNum,
                'scanner' => $scanner->name,
                'resident_name' => "",
                'community_name' => "",
                'type' => 'rfid',
                'is_success' => $isSuccess,
                'log_text' => $logText,
                'scan_at' => Carbon::now(),
                'vehicle_info' => "",
                'rfid_log_id' => "",
            ]);
        }


        if ($scanner->community->id == 61) {
            return response("$scanner->mac_address\nRFID Scanned Successfully", 200)->header('Content-Type', 'text/plain');
        }

        return response("Something went wrong.Please try again.", 403)->header('Content-Type', 'text/plain');
    }

    /**
     * @param Request $request
     * @param $rfidCode
     * @return JsonResponse
     * @throws Exception
     */
    public function scannedRfidCodeWithKiosk(Request $request, $id, $rfCode)
    {


        $remoteGuard = RemoteGuard::find($id);

        if (empty($remoteGuard)) {
            return APIResponse::error('Remote Guard not found');
        }


        $rfidTagNum = $rfCode;

        $rfidTagNum = $rfCode - $remoteGuard->rfid_multiplier * $remoteGuard->community->rfid_site_code;


        $community_id = $remoteGuard->community_id;
        $rfid876 = $this->rfidService->findByTagNumUnsuspendedRfid($rfidTagNum, $community_id);

        $isSuccess = '1';
        $logText = "RFID Scanned Successfully";
        $errorGenerated = false;

        $rfid = $rfid876['result'];

        if ($rfid876['bool'] == false) {
            $isSuccess = '0';
            $logText = $rfid876['message'];
            $errorGenerated = true;
        } else if (empty($rfid)) {
            $isSuccess = '0';
            $logText = "RFID Tag Number not found";
            $errorGenerated = true;
        } else if (empty($remoteGuard)) {
            $isSuccess = '0';
            $logText = "Invalid Remote Guard";
            $errorGenerated = true;
        } else if ($rfid && $rfid->status == 0) {
            $isSuccess = '0';
            $logText = "Inactive RFID Code";
            $errorGenerated = true;
        } else if (empty($rfid->user)) {
            $isSuccess = '0';
            $logText = "Invalid User Attached";
            $errorGenerated = true;
        } else if ($rfid && $remoteGuard->community_id != $rfid->community->id) {
            $isSuccess = '0';
            $logText = "RFID Mismatch with Reader Community";
            $errorGenerated = true;
        }
        if (!$errorGenerated && !$this->rfidDateLimitService->checkIfScanTagLieInTimeRange($rfidTagNum)) {
            $isSuccess = '0';
            $logText = "RFID Mismatch with given time range";
            $errorGenerated = true;
        }


        // At this point rfid verified successfully
        if ($rfid && !$errorGenerated) {
            $rfidLog = $this->rfidLogService->create([
                'type' => 'rfid',
                'scanner_id' => $remoteGuard->scanner->id,
                'community_id' => $rfid ? $rfid->community_id : 0,
                'is_success' => $isSuccess,
                'rfid_id' => $rfid ? $rfid->id : 0,
                'rfids_tag_num_static' => $rfid ? $rfid->rfid_tag_num : 0,
                'log_text' => $logText,
                'scan_at' => Carbon::now(),
                'vehicle_info' => $rfid->vehicle_info

            ]);

            $residentName = '';

            if (!empty($rfid->user)) {
                $residentName = $rfid->user->first_name . " " . $rfid->user->last_name;
            }

            $community = Community::find($rfid->community_id);

            $scanAt = (!empty($rfidLog['result']->scan_at)) ? date('Y-m-d h:i a', strtotime($rfidLog['result']->scan_at)) : "";

            StaticRfidLog::create([
                'rfid' => $rfid->rfid_tag_num,
                'scanner' => $remoteGuard->identifier,
                'resident_name' => $residentName,
                'community_name' => $community->name,
                'type' => 'rfid',
                'is_success' => $isSuccess,
                'log_text' => $logText,
                'scan_at' => $scanAt,
                'vehicle_info' => $rfid->vehicle_info,
                'rfid_log_id' => $rfidLog['result']->id,
            ]);


            Log::debug('codes', [$rfidLog]);
            Log::debug('codes', [$isSuccess]);

            if ($rfidLog['bool'] && $isSuccess == '1') {
                return APIResponse::success(["macAddress" => $remoteGuard->identifier], "RFID Scanned Successfully");
            } else if ($rfidLog['bool'] && $isSuccess == '0') {
                if ($remoteGuard->community_id == 61) {
                    return APIResponse::success(["macAddress" => $remoteGuard->identifier], "RFID Scanned Successfully");
                }
                return response($rfidLog, 403)->header('Content-Type', 'text/plain');
            }
        } else {
            StaticRfidLog::create([
                'rfid' => $rfidTagNum,
                'scanner' => $remoteGuard->identifier,
                'resident_name' => "",
                'community_name' => "",
                'type' => 'rfid',
                'is_success' => $isSuccess,
                'log_text' => $logText,
                'scan_at' => Carbon::now(),
                'vehicle_info' => "",
                'rfid_log_id' => "",
            ]);
        }


        if ($remoteGuard->community_id == 61) {
            return APIResponse::success(["macAddress" => $remoteGuard->identifier], "RFID Scanned Successfully");
        }

        return response("Something went wrong.Please try again.", 403)->header('Content-Type', 'text/plain');
    }


    public function testing()
    {

        $license_plate_image = 'R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==';
        if (base64_decode($license_plate_image, true) == true) {

            $license_plate_image = $this->delete_all_between("data:image", "base64,", $license_plate_image);
            $ls_image = base64_decode($license_plate_image);
            $licenceName = md5(uniqid()) . '.' . 'png';
            $pathAndFileName = "dynamic/lic_images/" . $licenceName;
            $s3uploadResp = S3ImageHelper::storeImageInBucketGoogleCloudStorage($pathAndFileName, $ls_image);
            if ($s3uploadResp) {
                return APIResponse::success([], 'Updated Successfully.');

            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }


    }

    /**
     * get all rfid codes
     * @param Request $request
     * @return JsonResponse
     */
    public function getRfidCodesKiosk(Request $request)
    {

        $authUser = $request->header('php-auth-user');
        $authPass = $request->header('php-auth-pw');
        $scanner = $this->scannerService->getByCredentials($authUser, $authPass)['result'];
        if (empty($scanner)) {
            return APIResponse::error('Something went wrong.Please try again.');
        }
        $rfids = array();
        $pkr = [];
        $communityRfids = $this->rfidService->getUnsuspendedRfids($scanner->community_id)['result']->get();

        if ($communityRfids->isNotEmpty()) {
            $rfids = $communityRfids->pluck('rfid_tag_num');
        }

        if (!empty($rfids)) {
            foreach ($rfids as $rfid) {
                if ($this->rfidDateLimitService->checkIfScanTagLieInTimeRange($rfid)) {
                    Log::info('non-numeric value', [$scanner->rfid_multiplier]);
                    Log::info('non-numeric value', [$scanner->community->rfid_site_code]);
                    Log::info('non-numeric value', [$rfid]);
                    if ($rfid) {
                        if ($scanner->community->id != 61) {
                            $rfidCode = $scanner->rfid_multiplier * $scanner->community->rfid_site_code + $rfid;
                            $pkr[] = $rfidCode;
                        } else {
                            $rfidCode = $rfid;
                            $pkr[] = $rfidCode;
                        }


                    }

                }
            }
        }
        $list = implode("\n", $pkr);
        return APIResponse::success(["mac_address" => $scanner->mac_address, "list" => $list], "Success");
    }

    /**
     * @param Request $request
     * @param $rfidCode
     * @return JsonResponse
     * @throws Exception
     */
    public function scannedRfidCodeKiosk(Request $request, $rfidCode)
    {

        $authUser = $request->header('php-auth-user');
        $authPass = $request->header('php-auth-pw');

        $scanner = $this->scannerService->getByCredentials($authUser, $authPass)['result'];
        Log::debug("headers", [$authUser]);
        Log::debug("headers", [$authPass]);
        Log::debug("headers", [$scanner]);

        if (!$scanner) {
            return APIResponse::error('Something went wrong.Please try again.');
        }


        $rfidTagNum = $rfidCode - $scanner->rfid_multiplier * $scanner->community->rfid_site_code;

        if ($scanner->community->id == 61) {
            $rfidTagNum = $rfidCode;
        }
        $community_id = $scanner->community->id;
        $rfid876 = $this->rfidService->findByTagNumUnsuspendedRfid($rfidTagNum, $community_id);

        $isSuccess = '1';
        $logText = "RFID Scanned Successfully";
        $errorGenerated = false;

        $rfid = $rfid876['result'];

        if ($rfid876['bool'] == false) {
            $isSuccess = '0';
            $logText = $rfid876['message'];
            $errorGenerated = true;
        } else if (empty($rfid)) {
            $isSuccess = '0';
            $logText = "RFID Tag Number not found";
            $errorGenerated = true;
        } else if (empty($scanner)) {
            $isSuccess = '0';
            $logText = "Invalid RFID Code";
            $errorGenerated = true;
        } else if ($rfid && $rfid->status == 0) {
            $isSuccess = '0';
            $logText = "Inactive RFID Code";
            $errorGenerated = true;
        } else if (empty($rfid->user)) {
            $isSuccess = '0';
            $logText = "Invalid User Attached";
            $errorGenerated = true;
        } else if ($rfid && $scanner->community_id != $rfid->community->id) {
            $isSuccess = '0';
            $logText = "RFID Mismatch with Reader Community";
            $errorGenerated = true;
        }
        if (!$errorGenerated && !$this->rfidDateLimitService->checkIfScanTagLieInTimeRange($rfidTagNum)) {
            $isSuccess = '0';
            $logText = "RFID Mismatch with given time range";
            $errorGenerated = true;
        }


        // At this point rfid verified successfully
        if ($rfid && !$errorGenerated) {
            $rfidLog = $this->rfidLogService->create([
                'type' => 'rfid',
                'scanner_id' => $scanner->id,
                'community_id' => $rfid ? $rfid->community_id : 0,
                'is_success' => $isSuccess,
                'rfid_id' => $rfid ? $rfid->id : 0,
                'rfids_tag_num_static' => $rfid ? $rfid->rfid_tag_num : 0,
                'log_text' => $logText,
                'scan_at' => Carbon::now(),
                'vehicle_info' => $rfid->vehicle_info

            ]);

            $residentName = '';

            if (!empty($rfid->user)) {
                $residentName = $rfid->user->first_name . " " . $rfid->user->last_name;
            }

            $community = Community::find($rfid->community_id);

            $scanAt = (!empty($rfidLog['result']->scan_at)) ? date('Y-m-d h:i a', strtotime($rfidLog['result']->scan_at)) : "";

            StaticRfidLog::create([
                'rfid' => $rfid->rfid_tag_num,
                'scanner' => $scanner->name,
                'resident_name' => $residentName,
                'community_name' => $community->name,
                'type' => 'rfid',
                'is_success' => $isSuccess,
                'log_text' => $logText,
                'scan_at' => $scanAt,
                'vehicle_info' => $rfid->vehicle_info,
                'rfid_log_id' => $rfidLog['result']->id,
            ]);


            Log::debug('codes', [$rfidLog]);
            Log::debug('codes', [$isSuccess]);

            if ($rfidLog['bool'] && $isSuccess == '1') {

                return APIResponse::success(["mac_address" => $scanner->mac_address], "RFID Scanned Successfully");
            } else if ($rfidLog['bool'] && $isSuccess == '0') {
                if ($scanner->community->id == 61) {
                    return APIResponse::success(["mac_address" => $scanner->mac_address], "RFID Scanned Successfully");
                }

                return APIResponse::success(["data" => $rfidLog, "status" => 403]);
            }
        } else {
            StaticRfidLog::create([
                'rfid' => $rfidTagNum,
                'scanner' => $scanner->name,
                'resident_name' => "",
                'community_name' => "",
                'type' => 'rfid',
                'is_success' => $isSuccess,
                'log_text' => $logText,
                'scan_at' => Carbon::now(),
                'vehicle_info' => "",
                'rfid_log_id' => "",
            ]);
        }


        if ($scanner->community->id == 61) {

            return APIResponse::success(["mac_address" => $scanner->mac_address], "RFID Scanned Successfully");
        }


        return APIResponse::success(["status" => 403], "Something went wrong.Please try again.");
    }

}

