<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ScannedPassCollection;
use App\Http\Resources\ScannedPassDateCollection;
use App\Services\PassService;
use App\Services\PassUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ScannedPassController extends Controller
{
    public $passService;
    public $passUserService;

    public function __construct(
        PassService     $passService,
        PassUserService $passUserService

    )
    {
        $this->passService = $passService;
        $this->passUserService = $passUserService;
    }

    /**
     * get user scanned passes
     * @param Request $request
     * @param $userId
     * @return JsonResponse
     */
    public function scannedPassesData(Request $request, $userId)
    {
        $search = $request->search ?? "";
        $filterType = $request->filter_type ?? "";
        $filterSearch = $request->filter_search ?? "";
        $data = $this->passUserService->getScannedPassUserByUser($userId, $search, $filterType, $filterSearch, 10);
        if ($data['bool']) {
            return APIResponse::success(['list' => ScannedPassCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * get scanned passes Dates for filter records
     * @param Request $request
     * @param $userId
     * @return JsonResponse
     */
    public function scannedPassesDates(Request $request, $userId)
    {
        $data = $this->passUserService->getScannedPassUserDatesByUser($userId);
        if ($data['bool']) {
            return APIResponse::success(['list' => ScannedPassDateCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }
}
