<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\AuthService;

class AuthController extends Controller
{
    /**
     * @var authService
     */
    protected $authService;

    /**
     * AuthController constructor.
     *
     * @param AuthService $authService
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * -------------------------------------------
     * User Login
     * -------------------------------------------
     *
     * @param Request $request The request object containing login data.
     * @return JsonResponse The JSON response with the authentication result.
     * @throws Exception If an error occurs during the login process.
     * @see AuthService
     */
    public function login(LoginRequest $request): JsonResponse
    {
        return $this->authService->loginUser($request);
    }

    /**
     * -------------------------------------------
     * Verify Guard Login
     * -------------------------------------------
     *
     * This method verifies the guard login by validating the verification code provided in the request.
     * @param Request $request The request object containing the verification code.
     * @return JsonResponse The JSON response indicating the result of the verification.
     * @throws Exception If an error occurs during the verification process.
     */
    public function verifyGuardLogin(Request $request): JsonResponse
    {
        return $this->authService->verifyGuardLogin($request);
    }
}
