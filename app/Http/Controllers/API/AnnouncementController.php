<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\AnnouncementCollection;
use App\Services\AnnouncementService;
use App\Services\NotificationService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

class AnnouncementController extends Controller
{
    public $announcementService;
    public $notificationService;

    public function __construct(AnnouncementService $announcementService, NotificationService $notificationService)
    {
        $this->announcementService = $announcementService;
        $this->notificationService = $notificationService;
    }

    /**
     * -------------------------------------------
     * Index of Unread Announcements
     * -------------------------------------------
     *
     * This method retrieves the unread announcements for the authenticated user.
     * @return JsonResponse The JSON response with the result of the unread announcements.
     * @throws Exception If an error occurs during the process of retrieving the unread announcements.
     */
    public function index(): JsonResponse
    {
        $userId = Auth::user()->id;
        $announcementNotifications = $this->notificationService->getUnreadAnnouncements($userId, 30);
        return APIResponse::success(['list' => AnnouncementCollection::collection($announcementNotifications['result'])], null);
    }
}
