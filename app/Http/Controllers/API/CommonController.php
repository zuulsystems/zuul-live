<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Helpers\PushNotificationTrait;
use App\Helpers\UserCommon;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Pass;
use App\Models\PassUser;
use App\Models\UserContact;
use App\Services\PassUserService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\NotificationService;
use App\Services\UserContactService;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class CommonController extends Controller
{
    use UserCommon;
    use PushNotificationTrait;

    private $notificationService;
    private $contactSrvc;
    private $userSrvc;

    public function __construct(NotificationService $notificationService, UserContactService $userContactService, UserService $userService, PassUserService $passUserService)
    {
        $this->notificationService = $notificationService;
        $this->contactSrvc = $userContactService;
        $this->userSrvc = $userService;
        $this->passUserService = $passUserService;
    }

    public function getSoundLocation()
    {
        $location = asset('sound/knock-two-times.mp3');
        return APIResponse::success(['sound' => $location], 'Sound received');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAddedHourDate(Request $request)
    {
        $data = $request->all();
        $pDate = Carbon::parse($data['date']);
        $validity = $data['validity'];
        $new_date = $pDate->addHours($validity)->format('m-d-Y h:i A');
        $new_date1 = $pDate->addHours($validity);
        $timestamp = (int)round($new_date1->format('Uu') / pow(10, 6 - 3));
        return APIResponse::success(['date' => $new_date, 'timestamp' => $timestamp], 'New Date');
    }

    public function getNotificationContactPreApprovedPassesCounts()
    {

        $zuulNotificationCount = $this->notificationService->getTotalNotifications(auth()->id());

        $parentalControlNotificationCount = $this->notificationService->getTotalNotifications(auth()->id(), 'parental_control');
        $contactCount = UserContact::where('created_by', auth()->id())->get()->count();

            $preApproved = UserContact::with(['contact'])->where('created_by', auth()->id())->where('is_dnc', '1')->pluck('id')->toArray();

            $household_members = $this->userSrvc->getHouseHoldMemberDetail(auth()->user()->house_id, 30, "");

            $familyMemberIds = $household_members->pluck('id');
            $familyMemberSharedContacts = $this->contactSrvc->getSharedDncContactsByUserIds($familyMemberIds)->toArray();

            $data = array_merge($familyMemberSharedContacts, $preApproved);


        $activePassCount = $this->passUserService->getActivePassUserByUserCount(auth()->id(), 0);

        $allPassCount = PassUser::whereHas('pass', function ($query) {
            return $query->where('created_by', auth()->id());
        })->get()->count();

        return APIResponse::success([
            "parental_control_unread" => $parentalControlNotificationCount,
            "zuul_unread" => $zuulNotificationCount,
            "user_contact_count" => $contactCount,
            "pre_approved_count" => count($data),
            "active_passes" => $activePassCount['result'],
            "all_passes" => $allPassCount,
        ]);
    }
}
