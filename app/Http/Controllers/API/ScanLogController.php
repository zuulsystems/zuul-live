<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ScanLogCollection;
use App\Services\ScanLogService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ScanLogController extends Controller
{

    private $scanLogService;

    /**
     * ScanController constructor.
     *
     * @param ScanLogService $scanLogService
     */
    public function __construct(ScanLogService $scanLogService)
    {
        $this->scanLogService = $scanLogService;
    }

    /**
     * -------------------------------------------
     * Index Scan Logs
     * -------------------------------------------
     * This method retrieves a list of scan logs.
     * @return JsonResponse The JSON response with the list of scan logs.
     * @throws Exception If an error occurs during the retrieval process.
     */
    public function index(): JsonResponse
    {
        return $this->scanLogService->scanLogs();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function store(Request $request)
    {
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }

}
