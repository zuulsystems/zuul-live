<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Helpers\PushNotificationTrait;
use App\Services\NotificationService;

class UserPermissionController extends Controller
{
    use PushNotificationTrait;

    public $userService;
    public $notificationService;

    public function __construct(
        UserService         $userService,
        NotificationService $notificationService
    )
    {
        $this->userService = $userService;
        $this->notificationService = $notificationService;
    }

    /**
     * set can manage family permission
     * @param Request $request
     * @return JsonResponse
     */
    public function setCanManageFamilyPermission(Request $request)
    {
        $permission = $this->checkUserHasPermission($request->member_id);
        if (!$permission['bool']) {
            return APIResponse::error($permission['message']);
        }
        $member = $permission['result'];
        if ($request->has('can_manage_family') && $request->can_manage_family == '1') {
            $member->assignPermission('can_manage_family');

            $notificationData = [
                'user_id' => $request->member_id,
                'type' => 'zuul',
                'heading' => 'Permission Changed',
                'text' => 'Permission Assigned',
                'category' => 'unread_announcement',
            ];

            $this->notificationService->create($notificationData);

            $push = array(
                'description' => "Permission changed ",
                'title' => 'Permission Assigned',
                'nid' => null,
                'nid_type' => 'regular',
            );

            $user = $this->userService->find($request->member_id)['result'];

            $this->sendPushNotificationToUser($push, $user);

        } else {
            $member->revokePermission('can_manage_family');

            $notificationData = [
                'user_id' => $request->member_id,
                'type' => 'zuul',
                'heading' => 'Permission Changed',
                'text' => 'Permission Revoked',
                'category' => 'unread_announcement',
            ];

            $this->notificationService->create($notificationData);

            $push = array(
                'description' => "Permission changed ",
                'title' => 'Permission taken',
                'nid' => null,
                'nid_type' => 'regular',
            );

            $user = $this->userService->find($request->member_id)['result'];

            $this->sendPushNotificationToUser($push, $user);
        }
        return APIResponse::success([], 'Permission changed');
    }

    /**
     * check user has permission to assign permission to members
     * @param $memberId
     * @return array
     */
    public function checkUserHasPermission($memberId)
    {
        $user = auth()->user();
        $member = $this->userService->find($memberId)['result'];
        if (empty($member)) {
            return [
                'bool' => false,
                'message' => 'No Record Found'
            ];
        }
        if (($user->house_id == $member->house_id) && ($user->hasPermission('can_manage_family') || $user->hasRole('family_head'))) {
            return [
                'bool' => true,
                'result' => $member,
                'message' => ''
            ];
        }
        return [
            'bool' => false,
            'message' => 'Cant change permission'
        ];
    }

    /**
     * set can send pass permission
     * @param Request $request
     * @return JsonResponse
     */
    public function setCanSendPassPermission(Request $request)
    {
        $permission = $this->checkUserHasPermission($request->member_id);
        if (!$permission['bool']) {
            return APIResponse::error($permission['message']);
        }
        $member = $permission['result'];
        if ($request->has('can_send_passes') && $request->can_send_passes == '1') {
            $member->assignPermission('can_send_passes');
        } else {
            $member->revokePermission('can_send_passes');
        }
        return APIResponse::success([], 'Permission changed');
    }

    /**
     * set allow parental control permission
     * @param Request $request
     * @return JsonResponse
     */
    public function setAllowParentalPermission(Request $request)
    {
        $permission = $this->checkUserHasPermission($request->member_id);
        if (!$permission['bool']) {
            return APIResponse::error($permission['message']);
        }
        $member = $permission['result'];
        if ($request->has('allow_parental_control') && $request->allow_parental_control == '1') {
            $member->assignPermission('allow_parental_control');
        } else {
            $member->revokePermission('allow_parental_control');
        }
        return APIResponse::success([], 'Permission changed');
    }

}
