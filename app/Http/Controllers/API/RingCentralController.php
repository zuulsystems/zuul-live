<?php

namespace App\Http\Controllers\API;

use App\Helpers\UserCommon;
use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use RingCentral\SDK\Http\ApiException;
use RingCentral\SDK\SDK;

class RingCentralController extends Controller
{
    use UserCommon;

    /**
     * get call logs
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        /**
         * required credentials
         * clientId ,clientSecret, Ring Central => phone number,password
         */
        $clientId = 'CNCPe6-sRQeRMsFoErgHnQ';
        $clientSecret = 'wwMagFl0Q-CAl27XrNBFjgnc6Uin76SDCe9RT7oUezDg';
        $rcsdk = new SDK($clientId, $clientSecret, 'https://platform.devtest.ringcentral.com');
        try {
            $rcsdk->platform()->login('+14706156205', '101', 'Noman1122!!@@');//ZUUL Systems App Credentials
            $token = $rcsdk->platform()->auth()->accessToken(); // Get access Token
            $apiResponse = $rcsdk->platform()->get('/account/~/extension/~/call-log?dateFrom=2021-11-06&type=Voice&' . $request->getQueryString()); // Call Call logs API
            return APIResponse::success([
                'token' => $token,
                'data' => $apiResponse->json(),
            ], 'Call Logs');
        } catch (ApiException $e) {
            return APIResponse::error($e->getMessage());
        } catch (Exception $e) {
            return APIResponse::error($e->getMessage());
        }
    }

    /**
     * get voice message list
     * @param Request $request
     * @return JsonResponse
     */
    public function voiceMessageList(Request $request)
    {
        /**
         * required credentials
         * clientId ,clientSecret, Ring Central => phone number,password
         */
        $clientId = 'CNCPe6-sRQeRMsFoErgHnQ';
        $clientSecret = 'wwMagFl0Q-CAl27XrNBFjgnc6Uin76SDCe9RT7oUezDg';
        $rcsdk = new SDK($clientId, $clientSecret, 'https://platform.devtest.ringcentral.com');
        try {
            $rcsdk->platform()->login('+14706156205', '101', 'Noman1122!!@@');//ZUUL Systems App Credentials
            $token = $rcsdk->platform()->auth()->accessToken(); // Get access Token
            $apiResponse = $rcsdk->platform()->get('/account/~/extension/~/message-store?dateFrom=2021-11-06&messageType=VoiceMail&' . $request->getQueryString()); // Call API
            return APIResponse::success([
                'token' => $token,
                'data' => $apiResponse->json(),
            ], 'Call Logs');
        } catch (ApiException $e) {
            return APIResponse::error($e->getMessage());
        } catch (Exception $e) {
            return APIResponse::error($e->getMessage());
        }
    }

    public function viewLoad()
    {
        $clientId = 'CNCPe6-sRQeRMsFoErgHnQ';
        $clientSecret = 'wwMagFl0Q-CAl27XrNBFjgnc6Uin76SDCe9RT7oUezDg';
        $rcsdk = new SDK($clientId, $clientSecret, 'https://platform.devtest.ringcentral.com');
        $result = null;
        $url = null;
        $token = null;
        try {
            $rcsdk->platform()->login('+14706156205', '', 'Noman1122!!@@');//ZUUL Systems App Credentials
            $token = $rcsdk->platform()->auth()->accessToken();
            $apiResponse = $rcsdk->platform()->get('/account/~/extension/~/call-log?dateFrom=2021-11-06&type=Voice');
            $result = $apiResponse->jsonArray();
        } catch (ApiException $e) {
            dd($e->getMessage());
        }
        $data['token'] = $token;
        $data['result'] = $result;
        $data['url'] = $url;
        return view('ring-central.index', $data);
    }

    public function ringCentralVoiceMessageView()
    {
        $clientId = 'CNCPe6-sRQeRMsFoErgHnQ';
        $clientSecret = 'wwMagFl0Q-CAl27XrNBFjgnc6Uin76SDCe9RT7oUezDg';
        $rcsdk = new SDK($clientId, $clientSecret, 'https://platform.devtest.ringcentral.com');
        $result = null;
        $url = null;
        $token = null;
        try {
            $rcsdk->platform()->login('+14706156205', '', 'Noman1122!!@@');//ZUUL Systems App Credentials
            $token = $rcsdk->platform()->auth()->accessToken();
            $apiResponse = $rcsdk->platform()->get('/account/~/extension/~/message-store?dateFrom=2021-11-06&messageType=VoiceMail');
            $result = $apiResponse->jsonArray();
        } catch (ApiException $e) {
            dd($e->getMessage());
        }
        $data['token'] = $token;
        $data['result'] = $result;
        $data['url'] = $url;
        return view('ring-central.voice-mesage-list', $data);
    }

    public function viewLoadOriginal()
    {
        $clientId = 'CNCPe6-sRQeRMsFoErgHnQ';
        $clientSecret = 'wwMagFl0Q-CAl27XrNBFjgnc6Uin76SDCe9RT7oUezDg';
        $rcsdk = new SDK($clientId, $clientSecret, 'https://platform.devtest.ringcentral.com');
        $result = null;
        $url = null;
        $token = null;
        try {
            $rcsdk->platform()->login('+14706156205', '', 'Noman1122!!@@');//ZUUL Systems App Credentials
            $token = $rcsdk->platform()->auth()->accessToken();
            $apiResponse = $rcsdk->platform()->get('/account/~/extension/~/call-log?dateFrom=2021-12-06&type=Voice');
            $result = $apiResponse->jsonArray();
        } catch (ApiException $e) {
        }
        $data['token'] = $token;
        $data['result'] = $result;
        $data['url'] = $url;
        return view('ring-central.index', $data);
    }
}
