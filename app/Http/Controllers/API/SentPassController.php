<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\PassRecipientCollection;
use App\Http\Resources\ScanPassRecipientCollection;
use App\Http\Resources\SentPassCollection;
use App\Http\Resources\SentPassDateCollection;
use App\Services\PassService;
use App\Services\PassUserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SentPassController extends Controller
{
    public $passService;
    public $passUserService;

    public function __construct(
        PassService     $passService,
        PassUserService $passUserService

    )
    {
        $this->passService = $passService;
        $this->passUserService = $passUserService;
    }

    /**
     * get sent user passes
     * @param Request $request
     * @param $createdBy
     * @return JsonResponse
     * @throws Exception
     */
    public function sentPassesData(Request $request, $createdBy)
    {
        $search = $request->search ?? "";
        $filterType = $request->filter_type ?? "";
        $filterSearch = $request->filter_search ?? "";
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passService->getSentPassesCreatedBy($createdBy, $isExpired, $search, $filterType, $filterSearch, 10);
        if ($data['bool']) {
            return APIResponse::success(['list' => SentPassCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * get sent passes Dates for filter records
     * @param Request $request
     * @param $userId
     * @return JsonResponse
     */
    public function sentPassesDates(Request $request, $userId)
    {
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passService->getSentPassesDatesCreatedBy($userId, $isExpired);
        if ($data['bool']) {
            return APIResponse::success(['list' => SentPassDateCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * get passes receipts
     * @param $passId
     * @return JsonResponse
     */
    public function passRecipients($passId)
    {
        $data = $this->passUserService->getPassRecipients($passId);
        if (empty($data['result'])) {
            return APIResponse::success(['list' => []], null);
        }
        if ($data['bool']) {
            return APIResponse::success(['list' => PassRecipientCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * get passes receipts
     * @param $passId
     * @return JsonResponse
     */
    public function scanPassRecipients($passId)
    {
        $data = $this->passUserService->getPassRecipients($passId, true);
        if (empty($data['result'])) {
            return APIResponse::success(['list' => []], null);
        }
        if ($data['bool']) {
            return APIResponse::success(['list' => ScanPassRecipientCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * @param $passUserId
     * @return JsonResponse
     * @throws Exception
     */
    public function removePassRecipient($passUserId)
    {
        $passUser = $this->passUserService->find($passUserId)['result'];
        if (!empty($passUser)) {
            //check if recipient has already scanned a pass, then it should not be delete
            if ($passUser->is_scan == '1') {
                return APIResponse::error('Recipient has scanned a pass.');
            }
        }
        $result = $this->passUserService->delete($passUserId);
        if ($result['bool']) {
            return APIResponse::success([], 'Deleted Successfully');
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * add recipient in pass
     * @param Request $request
     * @param $passId
     * @return JsonResponse
     * @throws Exception
     */
    public function addRecipientInPass(Request $request, $passId)
    {
        $pass = $this->passService->addPassRecipient($request->all(), $passId);
        if ($pass['bool']) {
            return APIResponse::success([], $pass['message']);
        }
        return APIResponse::error($pass['message']);
    }
}
