<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Mail\ResetPasswordMail;
use App\Models\User;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ResetPasswordController extends Controller
{
    private $userService;

    public function __construct(
        UserService $userService
    )
    {
        $this->userService = $userService;
    }

    /**
     * send reset password code
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function sendResetPasswordCode(Request $request)
    {
        $email = $request->email;
        $user = '';
        $message = '';

        if ($request->has('phone_number_alternate') and $request->phone_number_alternate != '') {
            $phone = $request->phone_number_alternate;
            $dial_code = $request->dial_code;
            $user = $this->userService->findByEmailAndPhone($email, $phone, $dial_code)['result'];
            $message = 'User not exist';
        } else {
            $user = $this->userService->findByEmail($email)['result'];
            $message = 'email not exist';
        }

        if (!empty($user)) {
            $emailData['user'] = $user;
            $verificationCode = resetPasswordVerificationCode();
            $user = $this->userService->update([
                'reset_verification_code' => $verificationCode,
                'is_reset_password' => '1'
            ], $user->id);
            if ($user['bool']) {
                $emailData['resetPasswordLink'] = route('reset-app', ['code' => $verificationCode]);

                // Remember to remove this patch once finalized - ZL67895
                $emailData['resetPasswordLink'] = str_replace("zuul2", "app", $emailData['resetPasswordLink']);
                Mail::to($email)->send(new ResetPasswordMail($emailData));
                return APIResponse::success($emailData, 'Password Reset link has been sent to email');

            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        }
        return APIResponse::error($message);
    }


    /**
     * check duplicate email code
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function checkDuplicateEmail(Request $request)
    {
        $email = $request->email;

        $emailUserCount = User::where('email', $email)->get();
        $emailUserCount = $emailUserCount->count();
        $response = '';

        if ($emailUserCount > 1) {

            $response = ['bool' => true, 'message' => 'Dual Email Exists', 'result' => $emailUserCount];
            return response()->json($response);
        }

        $response = ['bool' => true, 'message' => 'No duplicate emails', 'result' => 0];

        return response()->json($response);
    }

    /**
     * Verify Guard Login
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function verifyResetPasswordVerificationCode(Request $request)
    {
        $user = $this->userService->checkResetVerificationCode($request->code)['result'];
        if (!empty($user)) {
            $userData = ['reset_verification_code' => null];
            $result = $this->userService->update($userData, $user['id']);
            $user = $this->userService->find($user['id'])['result'];
            if ($result['bool']) {
                $user = new UserCollection($user);
                $success['token'] = $user->toArray(null)['token'];
                return APIResponse::success(['success' => $success, 'user' => $user], 'Code Matched');
            }
            return APIResponse::error('Something went wrong');
        } else {
            return APIResponse::error('Code does not exist');
        }
    }
}
