<?php

namespace App\Http\Controllers\API;

use App\Helpers\APIResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ReceivedPassCollection;
use App\Http\Resources\SentPassCollection;
use App\Http\Resources\UserScanPassesCollection;
use App\Models\Pass;
use App\Models\PassUser;
use App\Services\PassService;
use App\Services\PassRequestService;
use App\Services\UserContactService;
use App\Services\UserService;
use App\Services\ParentalControlService;
use App\Services\NotificationService;
use App\Models\Notification;
use App\Services\OffensiveWordService;
use App\Services\PassUserService;
use App\Helpers\UserCommon;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use App\Models\User;

class PassController extends Controller
{
    private $passService;
    private $passRequestService;
    private $userService;
    private $userContactService;
    private $parentalControlService;
    private $notificationService;
    private $offensiveWordService;
    private $passUserService;
    use UserCommon;

    /**
     * PassController constructor.
     *
     * @param PassRequestService $passRequestService
     * @param PassService $passService
     * @param UserService $userService
     * @param ParentalControlService $parentalControlService
     * @param NotificationService $notificationService
     * @param OffensiveWordService $offensiveWordService
     * @param PassUserService $passUserService
     * @param UserContactService $userContactService
     */
    public function __construct(PassRequestService     $passRequestService,
                                PassService            $passService,
                                UserService            $userService,
                                ParentalControlService $parentalControlService,
                                NotificationService    $notificationService,
                                OffensiveWordService   $offensiveWordService,
                                PassUserService        $passUserService,
                                UserContactService     $userContactService
    )
    {
        $this->passRequestService = $passRequestService;
        $this->passService = $passService;
        $this->passUserService = $passUserService;
        $this->userService = $userService;
        $this->parentalControlService = $parentalControlService;
        $this->notificationService = $notificationService;
        $this->offensiveWordService = $offensiveWordService;
        $this->userContactService = $userContactService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ], [
            'event_id.required' => 'Please select event',
            'lat.required' => 'Please enter latitude',
            'lng.required' => 'Please enter longitude'
        ]);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $passData = $request->all();
        if ($request->pass_type == 'self') {
            $passData['selected_contacts'][] = auth()->id();
        }

        $passData['pass_date'] = Carbon::parse($request->pass_date)->format('Y-m-d H:i:s');
        $pass = $this->passService->createPass($passData);
        if ($pass['bool']) {

            return APIResponse::success(['qrcodes' => $pass['qrcodes']], 'Pass sent and created');
        }
        return APIResponse::error($pass['message']);

    }

    /**
     * get pass information
     * @param $passId
     * @return JsonResponse
     * @throws Exception
     */
    public function passInformation($qr_code)
    {
        $pass_id = $qr_code;

        $pass = PassUser::where('qr_code', $pass_id)->where('is_scan', '0')->first();

        if (empty($pass)) {
            return APIResponse::error('Sorry! Pass with id not found');
        }

        $pass_data = PassUser::where('pass_id', $pass->pass_id)->get();
        $phone_array = "";
        $phone_count = 0;
        foreach ($pass_data as $data) {
            $User = User::where('id', $data->user_id)->first();
            $phone_count = $phone_count + 1;
            $phone_array .= numberFormat($User->phone_number, $User->dial_code, 'INTERNATIONAL') . ",";
        }
        $phone_array = rtrim($phone_array, ',');

        if ($phone_count > 1) {
            $string = 'sms://open?addresses=' . $phone_array . '/?&body=You have been issued a group pass. To retrieve your unique pass please open the link and type in your cell phone number: ' . urlencode(url("qr_code/00000/group"));
        } else {
            $string = 'sms://' . $phone_array . '/?&body=Guest Pass: ' . urlencode(url("qr_code/" . base64_encode($pass->qr_code)));
        }

        $result = [
            'url' => $string
        ];

        return APIResponse::success($result, 'Success');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ], [
            'event_id.required' => 'Please select event',
            'lat.required' => 'Please enter latitude',
            'lng.required' => 'Please enter longitude'
        ]);
        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $pass = $this->passService->editPass($request->all(), $id);
        if ($pass['bool']) {
            return APIResponse::success([], 'Pass Updated successfully');
        }
        return APIResponse::error('Something went wrong. Please try again');
    }

    /**
     * Request a pass.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function sendRequestForAPass(Request $request)
    {
        $result = $this->passRequestService->sendPassRequest($request->all());
        if ($result['bool']) {
            return APIResponse::success([], 'Pass requested successfully');
        }
        return APIResponse::error('Something went wrong. Please try again');
    }

    /**
     * retract sent pass
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function retractSentPass(Request $request)
    {
        $passId = $request->pass_id;
        $result = $this->passService->retractSentPass($passId);
        if ($result['bool']) {
            return APIResponse::success([], 'Pass Retracted Successfully');
        }
        return APIResponse::error('Something went wrong. Please try again');
    }

    /**
     * get user sent passes
     * @param Request $request
     * @param $createdBy
     * @return JsonResponse
     * @throws Exception
     */
    public function getUserSentPasses(Request $request, $createdBy)
    {
        $search = ($request->has('search') && !empty($request->search)) ? $request->search : ""; #todo need to implement
        $isExpired = ($request->has('expired') && $request->expired == '1') ? true : false; //0: active, 1: expired
        $data = $this->passService->getSentUserPasses($createdBy, $isExpired, 10, $search);
        if ($data['bool']) {
            return APIResponse::success(['list' => SentPassCollection::collection($data['result'])], null);
        }
        return APIResponse::error('Something went wrong.Please try again');
    }

    /**
     * get user received passes
     * @param Request $request
     * @param $user_id
     * @return JsonResponse
     */
    public function getUserReceivedPasses(Request $request, $user_id)
    {
        $data = $this->passUserService->getPassUsersByUser($user_id, 10);
        if ($data['bool']) {
            return APIResponse::success(['list' => ReceivedPassCollection::collection($data['result'])], null);
        }
    }

    /**
     * get user scanned passes
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function GetUserScannedPasses(Request $request)
    {
        if (!$request->has('id')) {
            return APIResponse::success('User id Invalid or null');
        }

        $passes = $this->passUserService->getUserScannedPasses($request->id, 5);

        if ($passes['bool']) {
            return APIResponse::success(['list' => UserScanPassesCollection::collection($passes['result'])], 'Success');
        }

        return APIResponse::error('Something went wrong');
    }

    /**
     * send pass request from contacts
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function sendPassRequestFromContact(Request $request)
    {
        $userContactId = $request->contact_id;
        //Check user has permission to send a pass
        $userContact = $this->userContactService->getUserContactPermission($userContactId);
        if (empty($userContact['result'])) {
            return APIResponse::error('The person you are requesting a pass from, is not approved to issue you one');
        }

        $passRequestData = [
            'requested_user_id' => $userContact['result']->contact->user->id ?? "",
            'description' => $request->description,
        ];
        $passRequest = $this->passRequestService->sendPassRequest($passRequestData);
        if ($passRequest['bool']) {
            return APIResponse::success([], 'Pass requested successfully');
        } else {
            return APIResponse::error('Something went wrong');
        }
    }

    public function createPassKiosk(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ], [
            'event_id.required' => 'Please select event',
            'lat.required' => 'Please enter latitude',
            'lng.required' => 'Please enter longitude'
        ]);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        $passData = $request->all();
        if ($request->pass_type == 'self') {
            $passData['selected_contacts'][] = auth()->id();
        }
        $passData['pass_date'] = Carbon::parse($request->pass_date)->format('Y-m-d H:i:s');
        $pass = $this->passService->createPass($passData);
        if ($pass['bool']) {


            return APIResponse::success(['qrcodes' => $pass['qrcodes']], 'Pass sent and created');
        }
        return APIResponse::error($pass['message']);

    }

}
