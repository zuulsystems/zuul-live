<?php

namespace App\Http\Resources;

use App\Helpers\UserCommon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Http;

class UserCollection extends JsonResource
{
    use UserCommon;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $profile_image_url_base64 = "";

        if($this->profile_image_url != null){

            $response = Http::get($this->profile_image_url);

            if ($response->successful()) {
                try {
                    $image = Image::make($response->body());
                    $profile_image_url_base64 = $image->encode('data-url')->encoded;
                } catch (\Exception $e) {
                    // Handle the exception, e.g., log it or set a default image.
                    $profile_image_url_base64 = null;
                }

            }

        }

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'email_verified_at' => $this->email_verified_at,
            'temp_password' => $this->temp_password,
            'profile_image' => $this->profile_image,
            'community_id' => $this->community_id,
            'house_id' => $this->house_id,
            'enable_remote_guard' => $this->enable_remote_guard,
            'fcm_token' => $this->fcm_token,
            'web_token' => $this->web_token,
            'created_by' => $this->created_by,
            'verification_code' => $this->verification_code,
            'is_verified' => $this->is_verified,
            'dial_code' => $this->dial_code,
            'push_notification' => $this->push_notification,
            'sms_notification' => $this->sms_notification,
            'email_notification' => $this->email_notification,
            'special_instruction' => $this->special_instruction,
            'is_two_factor' => $this->is_two_factor,
            'vacation_mode' => $this->vacation_mode,
            'license_type' => $this->license_type,
            'license_image' => $this->license_image,
            'mailing_address' => $this->mailing_address,
            'role_id' => $this->role_id,
            'is_two_factor_code' => $this->is_two_factor_code,
            'is_two_factor_on' => $this->is_two_factor_on,
            'api_token' => $this->api_token,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'full_name' => $this->full_name,
            'license_image_url' =>  (!empty($this->license_image_url))?$this->license_image_url:asset('images/license/license.png'),
            'profile_image_url' => $this->profile_image_url,
            'profile_image_url_base64' => $profile_image_url_base64,
            'fullNameInverted' => $this->fullNameInverted,
            'date_of_birth' => $this->date_of_birth,
            'street_address' => $this->street_address,
            'apartment' => $this->apartment,
            'city' => $this->city,
            'state' => $this->state,
            'zip_code' => $this->zip_code,
            'house' => $this->house,
            'role' => $this->role,
            'active' => 1,
            'is_reset_password' => $this->is_reset_password,
            'token' => $this->createToken('ZUUL Systems')->accessToken,
            'can_user_become_resident' => $this->role->id == 7 ? true : false,
            "can_show_settings" => ($this->role->id != 7),
            'can_manage_family' => $this->hasPermission('can_manage_family'),
            'can_send_passes' => $this->canSendPasses($this),
            'allow_parental_control' => $this->hasPermission('allow_parental_control'),
            'is_license_locked' => $this->hasPermission('is_license_locked'),
            'rover_mode' => $this->rover_mode,
            'rover_type' => $this->rover_type,
            'is_onboarded' => $this->is_onboarded,
            'extension' => $this->extension,
            'default_camera' => $this->default_camera,
            'air_print' => $this->air_print,
            'front_camera' => $this->camera_id,
            'doorbird_cam_id' => $this->doorbird_cam_id,
            'selected_web_relay' => $this->selected_web_relay ?? 0
        ];
    }
}
