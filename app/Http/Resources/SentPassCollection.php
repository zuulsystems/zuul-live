<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SentPassCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $AddedUser = (!empty($this->AddedUser))?$this->AddedUser:"";

        return [
            'id' => $this->id,
            'event_name' => $this->event->name ?? "",
            'event_id' => $this->event->id ?? "",
            'description' => $this->description,
            'community_name' => $this->community->name ?? "",
            'recipient_count' => $this->addedUsers->count() ?? "0",
            'recipient' => (!empty( $AddedUser))? $AddedUser->fullName.' - '.$this->formatted_phone.' '.$AddedUser->email:"",
            'pass_start_date' => Carbon::parse($this->pass_start_date)->format('M-d-y'),
            'pass_date' => Carbon::parse($this->pass_date)->format('M-d-y'),
            'pass_type' => $this->pass_type,
            'pass_start_date_with_time' => $this->pass_start_date,
            'pass_validity' => (!empty($this->pass_validity)) ? formatHoursToText($this->pass_validity) : '',
            'expired' => $request->expired ?? "0",
            'username' => (!empty( $AddedUser))? $AddedUser->fullName:"",
            'lat' => $this->lat,
            'lng' => $this->lng,
            'visitor_type' => $this->visitor_type
        ];
    }
}
