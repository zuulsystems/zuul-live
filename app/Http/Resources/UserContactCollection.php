<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserContactCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'contact_name' => $this->contact_name,
            'email' => $this->email,
            'formatted_phone_number' => (!empty($this->contact)) ? $this->contact->formattedPhone : "",
            'dial_code' => (!empty($this->contact)) ? $this->contact->dial_code : "",
            'is_favourite' => $this->is_favourite,
            'is_temporary' => $this->is_temporary,
            'temporary_duration' => $this->temporary_duration,
            'created_by' => $this->created_by
        ];
    }
}
