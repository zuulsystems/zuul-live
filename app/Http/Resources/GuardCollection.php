<?php

namespace App\Http\Resources;

use App\Helpers\UserCommon;
use Illuminate\Http\Resources\Json\JsonResource;

class GuardCollection extends JsonResource
{
    use UserCommon;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'role_id' => $this->role_id,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'formatted_phone' => $this->formatted_phone,
            'is_suspended' => $this->is_suspended,
            'house' => $this->house,
            'can_manage_family' => $this->hasPermission('can_manage_family'),
            'can_send_passes' => $this->canSendPasses($this),
            'allow_parental_control' => $this->hasPermission('allow_parental_control')
        ];
    }
}
