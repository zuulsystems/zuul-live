<?php

namespace App\Http\Resources;

use App\Helpers\UserCommon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class GuardApiCollection extends JsonResource
{
    use UserCommon;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $additional_phone_one = isset($this->additionalPhone[0]) ? $this->additionalPhone[0]['plain_number'] : "";
        $additional_phone_two = isset($this->additionalPhone[1]) ? $this->additionalPhone[1]['plain_number'] : "";
        $dial_code_one = isset($this->additionalPhone[0]) ? Str::start($this->additionalPhone[0]['dial_code'], '+') : "";
        $dial_code_two = isset($this->additionalPhone[1]) ? Str::start($this->additionalPhone[1]['dial_code'], '+') : "";

        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'formatted_phone' => $this->formatted_phone,
            'formatted_phone_by_dial_code' => $this->formatted_phone_by_dial_code,
            'email' => $this->email,
            'house' => $this->house,
            'role_id' => $this->role_id,
            'visible_in_the_directory' => $this->visible_in_the_directory,
            'rfids' => $this->rfids,
            "formatted_phone_two_by_dial_code11"=> isset($this->additionalPhone[0]) ? $this->additionalPhone[0]['dial_code'] : "",
            'formatted_phone_one_by_dial_code' => numberFormat($additional_phone_one, $dial_code_one, 'INTERNATIONAL'),
            'formatted_phone_two_by_dial_code' => numberFormat($additional_phone_two, $dial_code_two, 'INTERNATIONAL'),
            'formatted_phone_one' => numberFormat($additional_phone_one, $dial_code_one, 'INTERNATIONAL'),
            'formatted_phone_two' => numberFormat($additional_phone_two, $dial_code_two, 'INTERNATIONAL'),
        ];
    }
}
