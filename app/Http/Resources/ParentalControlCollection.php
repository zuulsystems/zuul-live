<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ParentalControlCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "parent_id" => $this->parent_id,
            "member_id" => $this->member_id,
            "notify_when_member_guest_arrive" => $this->notify_when_member_guest_arrive,
            "notify_when_member_send_pass_to_others" => $this->notify_when_member_send_pass_to_others,
            "notify_when_member_receive_a_pass" => $this->notify_when_member_receive_a_pass,
            "notify_when_member_retract_a_pass" => $this->notify_when_member_retract_a_pass,
            'notify_when_member_update_a_pass' => $this->notify_when_member_update_a_pass,
            'notify_when_pass_retracted_included_member' => $this->notify_when_pass_retracted_included_member,
            'notify_when_pass_updated_included_member' => $this->notify_when_pass_updated_included_member,
            'notify_when_member_request_a_pass_with_offensive_words' => $this->notify_when_member_request_a_pass_with_offensive_words,
            'blacklist_member_to_receive_request_a_pass' => $this->blacklist_member_to_receive_request_a_pass,
            'allow_parental_controls_to_member' => $this->allow_parental_controls_to_member,
        ];
    }
}
