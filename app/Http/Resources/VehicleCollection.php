<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'year' => $this->year,
            'license_plate' => strtoupper($this->license_plate),
            "make" => $this->make,
            "model" => $this->model,
            "color" => $this->color,
            'user_id' => $this->user_id,
        ];
    }
}
