<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ZuulNotificationCollection extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'user_id' => $this->user_id,
            'pass_id' => $this->pass_id,
            'event_id' => $this->event_id,
            'heading' => $this->heading,
            'subheading' => $this->subheading,
            'text' => $this->text,
            'link' => $this->link,
            'is_read' => $this->is_read,
        ];
    }
}
