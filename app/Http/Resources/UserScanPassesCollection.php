<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserScanPassesCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pass_id'=>$this->pass_id,
            'user_id'=>$this->user_id,
            'qr_code'=>$this->qr_code,
            'is_scan'=>$this->is_scan,
            'vehicle_id'=>$this->vehicle_id,
            'ticket_id'=>$this->ticket_id,
            'deleted_at'=>$this->deleted_at,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
            'pass' =>  $this->pass,
            'user' =>  $this->user,
            'scan_logs' =>  $this->scanLogs
        ];

    }
}
