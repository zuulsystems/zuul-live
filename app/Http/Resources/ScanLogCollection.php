<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;


class ScanLogCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $createdBy = (!empty($this->passUser) && !empty($this->passUser->pass) && !empty($this->passUser->pass->createdBy)) ? $this->passUser->pass->createdBy : null;
        $event = (!empty($this->passUser) && !empty($this->passUser->pass) && !empty($this->passUser->pass->event)) ? $this->passUser->pass->event : null;
        $vehicle = ( !empty($this->passUser) && !empty($this->passUser->vehicle))? $this->passUser->vehicle:null;

        return [
            'id' => $this->id,
            'is_success' => $this->is_success,
            'log_text' => $this->log_text,
            'type' => $this->type,
            'scan_date' => $this->ScanDate,
            'guard_display_name' => $this->guard_display_name,
            'added_user' => [
                'formattedPhone' => !empty($this->addedUser) ? $this->addedUser->FormattedPhone : "",
                'dial_code' => !empty($this->addedUser) ? $this->addedUser->dial_code : "",
                "formatted_phone_by_dial_code"=> !empty($this->addedUser) ? numberFormat($this->addedUser->phone_number, Str::start($this->addedUser->dial_code, '+'), 'INTERNATIONAL') : "",
                'fullName' => !empty($this->addedUser) ? $this->addedUser->fullName : "",
                'fullNameInverted' => !empty($this->addedUser) ? $this->addedUser->fullNameInverted : "",
            ],
            "vehicle" => !empty($vehicle) ? new VehicleCollection($vehicle):null,
            'pass' => [
                'created_by' => [
                    'fullName' => !empty($createdBy) ? $createdBy->fullName : "",
                ],
                "event" => [
                    'fullName' => !empty($event) ? $event->name : "",
                ],
            ]
        ];

    }
}
