<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PassDetailCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pass = ( $this->pass()->exists())? $this->pass:"";
        $user = ( $this->user()->exists())? $this->user:"";
        $vehicle = ( $this->vehicle()->exists())? $this->vehicle:"";
        $event = (!empty($pass) && $pass->event()->exists()) ? $pass->event:"";
        $createdBy = (!empty($pass) && $pass->createdBy()->exists()) ? $pass->createdBy:"";
        $createdByCommunity = (!empty($createdBy) && $createdBy->community()->exists()) ? $pass->community:"";

        $convertedPassStartDate = (!empty($pass) && !empty($pass->pass_start_date))  ? $pass->convertedPassStartDate():"";
        $convertedPassDate = (!empty($pass) && !empty($pass->pass_date))  ? $pass->convertedPassDate():"";
        $senderUserContact =   (!empty($user) &&  !empty($createdBy))?$user->userContacts()->whereHas('contact',function ($contact)use($createdBy){
            $contact->where('phone_number',$createdBy->phone_number);
        })->first():"";

        $formatted_phone = "";
        if($createdBy->countryCode->dial_code == 504){
            $formatted_phone = DialCodeFormattedPhone($createdBy->phone_number);
        }else if($createdBy->countryCode->dial_code == 1){
            $formatted_phone = (!empty($createdBy)) ? $createdBy->formattedPhone:"";
        }

        return [
            'id' => $this->id,
            'pass_id' => $this->pass_id,
            'dial_code' => Str::start($createdBy->countryCode->dial_code, '+'),
            'qr_code' => $this->qr_code,
            'is_scan' => $this->is_scan,
            'sender_id' => (!empty($createdBy))?$createdBy->id:"",
            'sender_name' => (!empty($createdBy))?$createdBy->fullName:"",
            "sender_formatted_phone_number" => $formatted_phone,
            "sender_community_name" => $createdByCommunity->name,
            'pass_date' => (!empty($pass))? Carbon::parse($pass->pass_date)->format('M-d-y') :"" ,
            'pass_validity' => (!empty($pass)) ? formatHoursToText($pass->pass_validity) : '',
            "lat" =>  (!empty($pass))? $pass->lat :"",
            "lng" =>  (!empty($pass))? $pass->lng :"",
            "description" =>  (!empty($pass))? $pass->description :"",
            'visitor_type' =>  (!empty($pass))? $pass->visitor_type :"",
            'to_date' => (!empty($pass) && !empty($pass->pass_start_date)) ? date('M j Y ',$convertedPassStartDate) : '',
            'to_date_time' => (!empty($pass) && !empty($pass->pass_start_date)) ? date('g:i A',$convertedPassStartDate) : '',
            'for_date' => (!empty($pass) && !empty($pass->pass_date)) ? date('M j Y ',$convertedPassDate) : '',
            'for_date_time' => (!empty($pass) && !empty($pass->pass_date)) ? date('g:i A ',$convertedPassDate):'',
            'license_image' =>  (!empty($user) && (!empty($user->licenseImageUrl)))?$user->licenseImageUrl:asset('images/license/license.png'),
            'event_name' => (!empty($event))? $event->name: '',
            'vehicle_id' => $this->vehicle_id,
            'make' => (!empty($vehicle))? $vehicle->make: "",
            'model' => (!empty($vehicle))? $vehicle->model: "",
            'year' => (!empty($vehicle))? $vehicle->year: "",
            'color' => (!empty($vehicle))? $vehicle->color: "",
            'license_plate' => (!empty($vehicle))? $vehicle->license_plate: "",
            'created_at' => $this->created_at,
            'contact_id' => (!empty($user) && !empty($createdBy) && $user->userContacts->isNotEmpty() && !empty($senderUserContact) )? $senderUserContact->id:null,
        ];

    }
}
