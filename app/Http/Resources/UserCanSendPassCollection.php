<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class UserCanSendPassCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->contact->user->id,
            'contact_name' => $this->contact_name,
            'formatted_phone_number' => numberFormat($this->contact->phone_number, $this->contact->dial_code, 'INTERNATIONAL')
        ];
    }
}
