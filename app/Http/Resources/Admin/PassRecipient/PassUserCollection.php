<?php

namespace App\Http\Resources\Admin\PassRecipient;

use Illuminate\Http\Resources\Json\JsonResource;

class PassUserCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $passUsers = $this;
        return [
            'id' => !empty($passUsers) ? $passUsers->id : "",
            'qr_code' => $this->qr_code,
            'pass_id' => $this->pass_id,
            'user_id' => $this->user_id,
            'ScannedDate' => !empty($passUsers) ? $passUsers->scannedDate : "",
            'user' => new AddedPassUserCollection($passUsers->user),
        ];

    }
}
