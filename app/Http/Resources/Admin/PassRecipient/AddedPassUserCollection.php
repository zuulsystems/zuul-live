<?php

namespace App\Http\Resources\Admin\PassRecipient;

use Illuminate\Http\Resources\Json\JsonResource;

class AddedPassUserCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $passAddedUser  = $this;
        return [
            'id' =>  !empty($passAddedUser)? $passAddedUser->id : "",
            'full_name' =>  !empty($passAddedUser)? $passAddedUser->fullName : "",
            'email' =>  !empty($passAddedUser)? $passAddedUser->email : "",
            'formatted_phone' =>  !empty($passAddedUser)? $passAddedUser->formattedPhone : "",
            'license_image' =>  !empty($passAddedUser)? $passAddedUser->license_image : "",
            'license_image_url' =>  (!empty($passAddedUser) && (!empty($passAddedUser->license_image_url)))?$passAddedUser->license_image_url:asset('images/license/license.png'),
        ];

    }
}
