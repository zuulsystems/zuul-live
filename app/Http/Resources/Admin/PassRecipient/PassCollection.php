<?php

namespace App\Http\Resources\Admin\PassRecipient;

use Illuminate\Http\Resources\Json\JsonResource;

class PassCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pass_users' => PassUserCollection::collection($this->PassUsers),
            'isExpired' => $this->isExpired
        ];

    }
}
