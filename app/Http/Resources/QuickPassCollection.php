<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuickPassCollection extends JsonResource
{
    protected $scanLog = null;
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $pass = ( $this->pass()->exists())? $this->pass:"";
        $vehicle = ( $this->vehicle()->exists())? $this->vehicle:"";
        $createdBy = (!empty($pass) && $pass->createdBy()->exists()) ? $pass->createdBy:"";
        $addedUser = (!empty($this->user))?$this->user:"";
        $alertMode = [
            'flag' => false,
            'message' => ''
        ];
        if (!empty($createdBy) && $createdBy->vacation_mode  == '1'){
            $alertMode = [
                'flag' => true,
                'message' => 'Pass scanned successfully, Please note that '.$createdBy->fullName .' is on vacation'
            ];
        }
        $collection = [
            'id' => $this->id,
            'qr_code' => $this->qr_code,
            'pass_id' => $this->pass_id,
            'user_id' => $this->user_id,
            'scan_log' => !empty($this->scanLog)? new SingleScanLogCollection($this->scanLog):null,
            'added_user' => [
                'formattedPhone' => !empty($addedUser) ? $addedUser->FormattedPhone : "",
                'fullName' => !empty($addedUser) ? $addedUser->fullName : "",
                'fullNameInverted' => !empty($addedUser) ? $addedUser->fullNameInverted : "",
                'license_image_url' =>  (!empty($addedUser) && (!empty($addedUser->license_image_url)))?$addedUser->license_image_url:asset('images/license/license.png'),
                'license_type' =>  !empty($addedUser) ?$addedUser->license_type:"",
                "vehicle" => !empty($vehicle) ? new VehicleCollection($vehicle):null,
            ],
            'alert_mode' => $alertMode
        ];

        if(!empty($addedUser))
        {
            if($addedUser->phone_number == "1111111111" || $addedUser->phone_number == "11111111")
            {
                $collection['added_user']['license_image_url'] = url('images/quick-pass-sample-img.png');
            }
        }

        return $collection;

    }

    /**
     * set scanLog object in collection
     * @param $scanLog
     * @return $this
     */
    public function setScanLog($scanLog){
        $this->scanLog  = $scanLog;
        return $this;
    }
}
