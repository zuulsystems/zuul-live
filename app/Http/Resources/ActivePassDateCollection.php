<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivePassDateCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pass_start_day' => (!empty($this->pass_start_date))? Carbon::parse($this->pass_start_date)->format('d') :"" ,
            'pass_start_month' => (!empty($this->pass_start_date))? Carbon::parse($this->pass_start_date)->format('M') :"" ,
            'pass_start_date' => (!empty($this->pass_start_date))? Carbon::parse($this->pass_start_date)->format('M-d') :"" ,
            'date' => (!empty($this->pass_start_date))? Carbon::parse($this->pass_start_date)->format('Y-m-d') :"" ,
        ];
    }
}
