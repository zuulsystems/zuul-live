<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GuardAppointLocationCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'community' => (isset($this->community) && !empty($this->community))?$this->community:null,
            'id' => $this->id,
            'lat' => $this->lat,
            'long' => $this->long,
        ];
    }
}
