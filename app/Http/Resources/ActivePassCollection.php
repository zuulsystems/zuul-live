<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivePassCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pass = ( $this->pass()->exists())? $this->pass:"";
        $vehicle = ( $this->vehicle()->exists())? $this->vehicle:"";
        $event = (!empty($pass) && $pass->event()->exists()) ? $pass->event:"";
        $createdBy = (!empty($pass) && $pass->createdBy()->exists()) ? $pass->createdBy:"";
        $createdByCommunity = (!empty($createdBy) && $createdBy->community()->exists()) ? $pass->community:"";
        return [
            'id' => $this->id,
            'pass_id' => $this->pass_id,
            'qr_code' => $this->qr_code,
            'is_scan' => $this->is_scan,
            'sender_name' => (!empty($createdBy))?$createdBy->fullName:"",
            'sender_profile_image' => (!empty($createdBy))? $createdBy->ProfileImageUrl:"",
            "sender_community_name" => !empty($createdByCommunity)?$createdByCommunity->name:"",
            'pass_date' => (!empty($pass))? Carbon::parse($pass->pass_date)->format('M-d-y') :"" ,
            'pass_validity' => (!empty($pass)) ? formatHoursToText($pass->pass_validity) : '',
            "lat" =>  (!empty($pass))? $pass->lat :"",
            "lng" =>  (!empty($pass))? $pass->lng :"",
            "description" =>  (!empty($pass))? $pass->description :"",
            'pass_start_date' => (!empty($pass) && !empty($pass->pass_start_date)) ? Carbon::parse($pass->pass_start_date)->format('M-d-y') : '',
            'pass_start_date_edit' => (!empty($pass) && !empty($pass->pass_start_date)) ? Carbon::parse($pass->pass_start_date)->format('m/d/Y h:i A') : '',
            'pass_end_date_edit' => (!empty($pass) && !empty($pass->pass_start_date)) ? Carbon::parse($pass->pass_date)->format('m/d/Y h:i A') : '',
            'event_name' => (!empty($event))? $event->name: '',
            'vehicle_id' => $this->vehicle_id,
            'make' => (!empty($vehicle))? $vehicle->make: "",
            'model' => (!empty($vehicle))? $vehicle->model: "",
            'year' => (!empty($vehicle))? $vehicle->year: "",
            'color' => (!empty($vehicle))? $vehicle->color: "",
            'license_plate' => (!empty($vehicle))? $vehicle->license_plate: "",
            'created_at' => $this->created_at,
            'created_by' => $pass->created_by
        ];

    }
}
