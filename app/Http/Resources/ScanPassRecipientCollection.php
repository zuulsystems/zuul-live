<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ScanPassRecipientCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $addedUser = (!empty($this->user))?$this->user:"";
        $successScanLog = (!empty($this->successScanLog))?$this->successScanLog:"";

        $formatted_phone = "";

        if($this->user->countryCode->dial_code == 504){
            $formatted_phone = DialCodeFormattedPhone($this->user->phone_number);
        }else if($this->user->countryCode->dial_code == 1){
            $formatted_phone = (!empty($this->user))?$this->user->FormattedPhone:"";
        }

        return [
            'id' => $this->id,
            'is_scan' => $this->is_scan,
            'fullNameInverted' => !empty($addedUser) ?$addedUser->fullNameInverted:"",
            'fullName' =>  !empty($addedUser) ?$addedUser->fullName:"",
            'formattedPhone' => $formatted_phone,
            'scanDate' => !empty($successScanLog)?$successScanLog->ScanDate:"",
            'scanByFullName' => (!empty($successScanLog) && !empty($successScanLog->scanBy))?$successScanLog->scanBy->fullName:"",
            'log_text' => (!empty($successScanLog) && !empty($successScanLog->scanBy))?$successScanLog->log_text:"",
        ];
    }
}
