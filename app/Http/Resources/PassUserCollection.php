<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PassUserCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->vehicle->id,
            "make" => $this->vehicle->make,
            "model" => $this->vehicle->model,
            'year' => $this->vehicle->year,
            "color" => $this->vehicle->color,
            'licence_plate' => $this->vehicle->licence_plate,
            'is_default' => $this->vehicle->is_default  
        ];
    }
}
