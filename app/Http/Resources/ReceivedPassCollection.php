<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ReceivedPassCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $pass = ( $this->pass()->exists())? $this->pass:"";
        $createdBy = (!empty($pass) && $pass->createdBy()->exists()) ? $pass->createdBy:"";
        $createdByCommunity = (!empty($createdBy) && $createdBy->community()->exists()) ? $pass->community:"";
        return [
            'id' => $this->id,
            'pass_id' => $this->pass_id,
            'qr_code' => $this->qr_code,
            'is_scan' => $this->is_scan,
            'sender_name' => (!empty($createdBy))?$createdBy->fullName:"",
            'sender_profile_image' => (!empty($createdBy))? $createdBy->ProfileImageUrl:"",
            "sender_community_name" => $createdByCommunity->name,
            'created_at' => $this->created_at,
            'pass_date' => (!empty($pass))? Carbon::parse($pass->pass_date)->format('M-d-y') :"" ,
            'pass_validity' => (!empty($pass)) ? formatHoursToText($pass->pass_validity) : '',
            "lat" =>  (!empty($pass))? $pass->lat :"",
            "lng" =>  (!empty($pass))? $pass->lng :"",
        ];
    }
}
