<?php

namespace App\Http\Middleware;

use App\Services\CameraService;
use App\Services\ScannerService;
use Closure;

class ZkeyBasicAuthScannerRfid
{
    public $scannerService;
    public $cameraService;
    public function __construct(
        ScannerService $scannerService,
        CameraService $cameraService
    )
    {
        $this->scannerService = $scannerService;
        $this->cameraService = $cameraService;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $AUTH_USER = $request->header('php-auth-user');
        $AUTH_PASS = $request->header('php-auth-pw');
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }
        // check scanner with mac_address and secret_key
        $scanner = $this->scannerService->getByCredentials($AUTH_USER,$AUTH_PASS);
        if(!$scanner['result']){
            return response('Unauthorized action.', 403)->header('Content-Type','text/plain');
        }
        return $next($request);
    }
}
