<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\APIResponse;

class CheckNode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $socketHeaderKey = $request->header('socket-header-key');
        $socketSecretKey = $request->header('socket-secret-key');

        if($socketHeaderKey == "sHHydot6qx8H6GZo" && $socketSecretKey == "e4d1Y8ANRFAjNQ7L")
        {
            return $next($request);
        }
        else
        {
           return APIResponse::error('Invalid credentials: Un authorized action');
        }

    }
}
