<?php

namespace App\Http\Middleware;

use Closure;

class CustomCheckForMaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('API_MAINTENANCE_MODE') == true){
            return response([
                'bool' => false,
                'message' => 'Upgrading System'
            ], 503);
        }
        return $next($request);
    }
}
