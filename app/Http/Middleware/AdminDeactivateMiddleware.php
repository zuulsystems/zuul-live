<?php

namespace App\Http\Middleware;

use Closure;

class AdminDeactivateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(
            auth()->user()->hasRole('super_admin') ||
            auth()->user()->hasRole('sub_admin') ||
            auth()->user()->hasRole('tl_admin') ||
            auth()->user()->hasRole('guard_admin') ||
            auth()->user()->hasRole('kiosk')
        ){
            if(auth()->user()->status == 'deactivate')
            {
                auth()->logout();
                return redirect()->route('login')->with('error','Your Account has been deactivated');
            }
            return $next($request);
        } else {
            return abort(403,'This action is unauthorized.');
        }
    }
}
