<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(
            auth()->user()->hasRole('super_admin') ||
            auth()->user()->hasRole('sub_admin') ||
            auth()->user()->hasRole('tl_admin') ||
            auth()->user()->hasRole('guard_admin')||
            auth()->user()->hasRole('kiosk')
        ){
            if(auth()->user()->is_two_factor_on == 1)
            {
                return redirect('two-factor-verification');
            }
            return $next($request);
        } else {
            return abort(403,'This action is unauthorized.');
        }
    }
}
