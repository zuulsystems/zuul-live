<?php

namespace App\Http\Middleware;

use Closure;

class CheckKiosk
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->hasRole('kiosk'))
        {
            return redirect()->route('admin.settings.scanners.index');
        }
        else
        {
            return $next($request);
        }
    }
}
