<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontendMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            auth()->user()->hasRole('family_head') ||
            auth()->user()->hasRole('family_member') ||
            auth()->user()->hasRole('guests_outsiders_daily')
        ) {

            if (auth()->user()->is_reset_password == '1') {
                return redirect('password-reset');
            }

            return $next($request);
        } else {
            return abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Checking guest account setting.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function checkAccountSettings()
    {
       $guest = auth()->user();
       if(empty($guest->name)||empty($guest->email)||empty($guest->phone_number)||empty($guest->password)||empty($guest->date_of_birth)||empty($guest->last_name)||empty($guest->street_address)||empty($guest->city)||empty($guest->state)||empty($guest->zip_code)||empty($guest->license_image))
       {
           return false;
       }
       else
       {
           return true;
       }
    }
}
