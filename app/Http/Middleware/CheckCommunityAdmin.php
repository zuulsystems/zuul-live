<?php

namespace App\Http\Middleware;

use Closure;

class CheckCommunityAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('sub_admin')) {
            if (auth()->user()->community->community_right == "both") {
                $slugs = explode("/", url()->current());
                if (isset($slugs[6])) {
                    if (url()->current() != url('admin/settings/email-templates') && url()->current() != url('admin/settings/email-templates/create') && url()->current() != url('admin/settings/email-templates/' . $slugs[6] . '/edit') && url()->current() != url('admin/settings/email-templates/' . $slugs[6])&&url()->current() != url('admin/residential/announcements') && url()->current() != url('admin/residential/announcements/create') && url()->current() != url('admin/residential/announcements/' . $slugs[6] . '/edit') && url()->current() != url('admin/residential/announcements/' . $slugs[6])) {
                        return redirect()->route('admin.dashboard');
                    }
                } else if (url()->current() != url('admin/settings/email-templates') && url()->current() != url('admin/settings/email-templates/create')&&url()->current() != url('admin/residential/announcements') && url()->current() != url('admin/residential/announcements/create')&& url()->current() != url('admin/residential/announcements/create')) {
                    return redirect()->route('admin.dashboard');
                }
            } else if (auth()->user()->community->community_right == "traffic_logics") {
                $slugs = explode("/", url()->current());
                if (isset($slugs[6])) {
                    if (url()->current() != url('admin/settings/email-templates') && url()->current() != url('admin/settings/email-templates/create') && url()->current() != url('admin/settings/email-templates/' . $slugs[6] . '/edit') && url()->current() != url('admin/settings/email-templates/' . $slugs[6])) {
                        return redirect()->route('admin.dashboard');
                    }
                } else if (url()->current() != url('admin/settings/email-templates') && url()->current() != url('admin/settings/email-templates/create')) {
                    return redirect()->route('admin.dashboard');
                }
            } else if (auth()->user()->community->community_right == "zuul") {
                $slugs = explode("/", url()->current());
                if (isset($slugs[6])) {
                    if (url()->current() != url('admin/residential/announcements') && url()->current() != url('admin/residential/announcements/create') && url()->current() != url('admin/residential/announcements/' . $slugs[6] . '/edit') && url()->current() != url('admin/residential/announcements/' . $slugs[6])) {
                        return redirect()->route('admin.dashboard');
                    }
                } else if (url()->current() != url('admin/residential/announcements') && url()->current() != url('admin/residential/announcements/create')&& url()->current() != url('admin/residential/announcements/create') && url()->current() != url('admin/user/guests')) {
                    return redirect()->route('admin.dashboard');
                }
            }
        }
        return $next($request);
    }
}
