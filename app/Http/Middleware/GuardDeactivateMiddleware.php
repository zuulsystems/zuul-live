<?php

namespace App\Http\Middleware;

use Closure;

class GuardDeactivateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ((
            auth()->user()->hasRole('guards') ||
            auth()->user()->hasRole('guard_admin'))
            && (auth()->user()->status == 'deactivate')
        ) {

            return response()->json([
                'bool' => false,
                'message' => 'Your Account has been deactivated'
            ]);

        }
        return $next($request);
    }
}
