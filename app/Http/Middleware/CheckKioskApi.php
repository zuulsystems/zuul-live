<?php

namespace App\Http\Middleware;

use Closure;

class CheckKioskApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->hasRole('kiosk'))
        {
            return $next($request);
        }
        else
        {
            return response()->json("Unauthorized action");
        }
    }
}
