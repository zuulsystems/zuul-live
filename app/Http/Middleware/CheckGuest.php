<?php

namespace App\Http\Middleware;

use Closure;

class CheckGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('guests_outsiders_daily')){
            return redirect('dashboard');
        }
        return $next($request);
    }
}
