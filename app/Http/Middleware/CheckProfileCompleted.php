<?php

namespace App\Http\Middleware;

use Closure;

class CheckProfileCompleted
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user  = auth()->user();
        if(empty($user->city) || empty($user->city) || empty($user->zip_code) || empty($user->license_image) ){
            return redirect()->route('account-setting.index')->with('success','Please Complete Your Profile');
        }
        return $next($request);
    }
}
