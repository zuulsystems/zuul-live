<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;

class CheckGuardAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->user()->hasRole('guard_admin')){

            $name = \Request::route()->getName();
            // dd($name);
            if($name == "admin.user.banned-guests"
                || $name == "admin.guest-logs.index"
                || $name == "admin.rfid.rfids.index"
                || $name == "admin.residential.residents.index"
                || $name == "admin.rfid.rfids-data"
                || $name == "admin.rfid.rfid-logs.index"
                || $name == "admin.rfid.rfid-logs-data") {

            }else{
                return redirect()->route('admin.residential.residents.index');
            }
        }
        return $next($request);
    }
}
