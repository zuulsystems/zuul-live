<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class RFIDSkippedRecordsExports implements FromCollection
{
    protected $data;

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function collection()
    {
        return collect($this->data);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings() :array
    {
        return [
            '#',
            'rfid_tag_num',
            'house_assigned',
            'resident_first_name',
            'resident_last_name',
            'mailing_address',
            'driver_license_number',
            'license_plate',
            'state_registered',
            'vehicle_type',
            'make',
            'color',
            'year',
            'model',
            'insurance_company_name',
            'policy_number',
            'policy_expiration_date',
            'additional_drivers',
            'error'
        ];
    }
}
