<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
//use Maatwebsite\Excel\Concerns\WithHeadings;
//use Maatwebsite\Excel\Concerns\WithEvents;
//use Maatwebsite\Excel\Events\AfterSheet;

class ResidentsSkippedRecordsExports implements FromCollection
//class ResidentsSkippedRecordsExports implements FromCollection, WithHeadings, WithEvents
{
    protected $data;

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function collection()
    {
        return collect($this->data);
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings() :array
    {
        return [
            'first_name',
            'last_name',
            'email',
            'phone_number',
            'house_name',
            'head_of_family',
            'special_instructions',
            'temp_password',
            'success_error',
            'link'
        ];
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
//    public function registerEvents(): array
//    {
//        return [
//            AfterSheet::class    => function(AfterSheet $event) {

//                $event->sheet->cells('A2:D4', function ($cells) {
//                    $cells->setBackground('#008686');
//                    $cells->setAlignment('center');
//                });

//                $sheet->cells('A1:D1', function ($cells) {
//                    $cells->setBackground('#008686');
//                    $cells->setAlignment('center');
//                });

//                $event->sheet->getDelegate()->getStyle('A2:C2')
//                    ->getFill()
//                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
//                    ->getStartColor()
//                    ->setARGB('DD4B39');

//            },
//        ];
//    }

}
