<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        /*Gate::before(function ($user, $ability) {
            return $user->role->slug == 'super_admin' ? true : null;
        });*/

        //super admin
        Gate::define('super-admin', function($user) {
            return (($user->role) && ($user->role->slug == 'super_admin'));
        });

        //community admin
        Gate::define('community-admin', function($user) {
            return (($user->role) && ($user->role->slug == 'sub_admin'));
        });

        //
        Gate::define('is-both-right', function($user) {
            return (($user->community) && ($user->community->community_right == 'both'));
        });

        //
        Gate::define('is-zuul-right1', function($user) {
            return (($user->community) && ($user->community->community_right == 'zuul'));
        });

        //tl admin
        Gate::define('tl-admin', function($user) {
            return (($user->role) && ($user->role->slug == 'tl_admin'));
        });

        //is traffic logics right
        Gate::define('is-tl-right1', function($user) {
            return (($user->community) && ($user->community->community_right == 'traffic_logics'));
        });

        Gate::define('only-tl-right', function($user) {
            return (($user->community) && ($user->community->community_right == 'traffic_logics'));
        });

        Gate::define('only-zuul-right', function($user) {
            return (($user->community) && ($user->community->community_right == 'zuul'));
        });

        //guard admin
        Gate::define('is-guard-admin', function($user) {
            return $user->role->slug == 'guard_admin';
        });

        //is zuul right
        Gate::define('is-zuul-right', function($user) {
            return (($user->community) && ($user->community->community_right == 'zuul' || $user->community->community_right == 'both'));
        });
        //is traffic logics right
        Gate::define('is-tl-right', function($user) {
            return (($user->community) && ($user->community->community_right == 'traffic_logics' || $user->community->community_right == 'both'));
        });

        //is guest right
        Gate::define('guest-outsiders-daily', function($user) {
            return $user->role->slug == 'guests_outsiders_daily';
        });

        Gate::define('kiosk', function($user) {
            return $user->role->slug == 'kiosk';
        });

        if (! app()->runningInConsole()) {
            foreach( $this->getPermissions() as $permission)
            {
                Gate::define($permission->slug, function($user) use ($permission){
                    return $user->hasPermissionTo($permission);
                });
            }
        }

    }

    protected function getPermissions()
    {
        try {
           // return Permission::with('roles')->get();
            return Permission::all();
        } catch (\Exception $e) {
            return [];
        }
    }
}
