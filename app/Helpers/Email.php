<?php

namespace App\Helpers;

use App\Services\PassService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;

trait Email
{
    /**
     * @var UserService
     */
    private $userSrvc;
    /**
     * @var PassService
     */
    private $passSrvc;

    /**
     * Email constructor.
     *
     * @param UserService $userService
     * @param PassService $passService
     */
    public function __construct(UserService $userService, PassService $passService)
    {
        $this->userSrvc = $userService;
        $this->passSrvc = $passService;
    }

    /**
     * Send QR Code In Email
     *
     * @param $qrCode
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function sendQRCodeEmail($qrCode)
    {
        if (!$qrCode) {
            return APIResponse::error("Invalid QR-Code");
        }

        $user = $this->userSrvc->find($qrCode->user_id);
        if (!$user['bool']) {
            return APIResponse::error("Invalid customer");
        }

        $user = $user['data'];
        if (!$user->email || $user->email == null || $user->email == "") {
            return APIResponse::error("Invalid email");
        }

        $pass = $this->passSrvc->find($qrCode->pass_id);

        $email = new \stdClass();
        $email->template = "qr_to_email";
        $email->event_name = $pass->event->name;
        $email->sender_name = "{$pass->createdBy->first_name} {$pass->createdBy->last_name}";
        $email->sender_community = $pass->createdBy->community->name;
        $email->description = $pass["description"];
        $email->sender_phone = $pass->createdBy->formattedPhone;
        $email->pass_start_date = $pass->convertedPassStartDate().' '.$pass->formattedStartTime;
        $email->pass_date = $pass->convertedPassDate().' '.$pass->convertedPassTime();
        $email->qr_code = qrCodeLink($qrCode->qrcode);
        $email->sender = 'ZUUL Team';
        $email->receiver = $user->name;
        $email->subject = 'ZUUL Systems - Pass received';

        if ($user->email_notification == '1') {
            $dyn_template = EmailTemplates::where('assign_to', 'qr_to_email')->first();
            $email->dynamic_template = EmailTemplatesController::replaceTemplateStringsAndSendmail($dyn_template,
                $email);

            if ($user->email) {
                if ($_SERVER['SERVER_NAME'] == "localhost") {
                    return response()->json(['bool' => true, 'message' => 'Email May Not Sent From localhost'], 200);
                }

                Mail::to($user->email)->send(new ZUULSystem($email));
                if (Mail::failures()) {
                    return response()->json(['bool' => true, 'message' => 'Email not sent'], 200);
                }

                return response()->json(['bool' => true, 'message' => 'Email Sent Successfully'], 200);
            } else {
                return response()->json(['bool' => true, 'message' => 'Email is disabled from user'], 200);
            }

        } else {
            return response()->json(['bool' => true, 'message' => 'Email is disabled from user'], 200);
        }
    }
}
