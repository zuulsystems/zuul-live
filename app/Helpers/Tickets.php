<?php

namespace App\Helpers;

use Exception;
use Illuminate\Http\Response;
use yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\TicketEmailSender;

trait Tickets
{

    public function generateDatatable($request, $data)
    {
        return Datatables::of($data)
            ->addColumn('excess_speed', function ($record) {
                return $record->violating_speed - $record->current_speed_limit;
            })->addColumn('community', function ($record) {
                if ($record->ticket_type == 'vendor')
                    return (!empty($record->community)) ? $record->community->name : "-";
                else
                    return (!empty($record->location) && !empty($record->location->community)) ? $record->location->community->name : "-";
            })->addColumn('violation_count', function ($record) {
                if (!empty($record->license_plate))
                    return '<a href="' . route('admin.traffic-tickets.tickets-by-license-plate', $record->license_plate) . '">' . $record->where('license_plate', $record->license_plate)->count() . '</a>';
                else
                    return $record->where('license_plate', $record->license_plate)->count();
            })
            ->addColumn('violation_count_raw', function ($record) {
                return $record->where('license_plate', $record->license_plate)->count();
            })
            ->addColumn('license_plate', function ($record) {
                return strtoupper($record->license_plate);
            })
            ->editColumn('vehicle', function ($record) {
                return is_null($record->vehicle) ? "-" : $record->vehicle->make . " " . $record->vehicle->model;
            })->addColumn('household', function ($record) {
                if (!empty($record->user_id) && !empty($record->user)) {
                    if (!empty($record->user->house))
                        return $record->user->house->house_name;
                }
                return "N/A";
            })->addColumn('action', function ($record) {
                $btn = '<div class="dropdown">';

                if (is_null($record->deleted_at))
                    $btn .= '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>';
                else
                    $btn .= '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">Action</button>';

                $btn .= '<div class="dropdown-menu">';


                if ($record->ticket_type == "unmatched")
                    $btn .= '<a class="dropdown-item" href="#" id="editLicBtn" data-id="' . $record->id . '" data-lic="' . $record->license_plate . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit License Plate</a>
                        <a class="dropdown-item" href="#" id="attachResidentBtn" data-id="' . $record->id . '"><i class="fa fa-edit" aria-hidden="true"></i> Attach to Resident </a>
                        <a class="dropdown-item" href="#" id="attachVendorBtn" data-id="' . $record->id . '"><i class="fa fa-edit" aria-hidden="true"></i> Attach to Vendor </a>';

                $btn .= '<a class="dropdown-item" href="#" id="ticketDetailBtn';
                if ($record->ticket_type == 'unmatched')
                    $btn .= 'Unmatched';
                $btn .= '" data-id="' . $record->id . '"><i class="fa fa-eye" aria-hidden="true"></i> Ticket Detail </a>';
                if ($record->ticket_type == 'vendor') {
                    if (!empty($record->vehicle))
                        if (!empty($record->vehicle->vendor[0]->community_id) && !empty($record->vehicle->vendor[0]->email))
                            $btn .= '<a class="dropdown-item sendMail" data-id="' . $record->id . '" data-communityId="' . $record->vehicle->vendor[0]->community_id . '" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a>';
                        else
                            $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';
                    else
                        $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';
                } else {
                    if (!is_null($record->location->community_id) && $record->ticket_type == "resident")
                        if (!is_null($record->user) && !empty($record->user->email))
                            $btn .= '<a class="dropdown-item sendMail" data-id="' . $record->id . '" data-communityId="' . $record->location->community_id . '" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a>';
                        else
                            $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';
                    else
                        $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';

                }

                $btn .= '                                    <a class="dropdown-item" href="' . route('admin.email-logs.show', $record->id) . '"><i class="fa fa-file-signature" aria-hidden="true"></i> Email Log </a>
                            <a class="dropdown-item" data-ticketID="' . $record->id . '" id="ticketImagesBtn" href="#"><i class="fa fa-edit" aria-hidden="true" id="ticketImages"></i> Ticket Images (' . $record->ticket_images_count . ') </a>';
                if (is_null($record->deleted_at))
                    $btn .= '<a class="dropdown-item" href="#" data-id="' . $record->id . '" id="deleteTicketBtn"><i class="fa fa-times-circle" aria-hidden="true" ></i> Dismiss Ticket </a>';
                else
                    $btn .= '<a class="dropdown-item" href="#" data-id="' . $record->id . '" id="undeleteTicketBtn"><i class="fa fa-times-circle" aria-hidden="true" ></i> Revert Ticket </a>';

                $btn .= '</div>
                        </div>';

                return $btn;
            })->editColumn('is_paid', function ($record) {
                $btn = '<div class="form-check">
                    <input class="form-check-input payment" type="checkbox" value="" id="payment" ';
                $btn .= ($record->is_paid == '0') ? '' : 'checked';
                $btn .= ' data-ticketID="' . $record->id . '">
                    <label class="form-check-label" for="payment">';
                $btn .= ($record->is_paid == '0') ? 'Pending' : 'Paid';
                return $btn .= '</label>
                  </div>';
            })->addColumn('formatted_time', function ($record) {
                return date('m-d-Y h:i:s A', strtotime($record->time));
            })->addColumn('mass_mail', function ($record) {
                if ($record->ticket_type == 'vendor') {
                    if (!empty($record->vehicle))
                        if (!empty($record->vehicle->vendor[0]->community_id) && !empty($record->vehicle->vendor[0]->email))
                            return '<input class="checkboxrow" type="checkbox" data-id=' . $record->id . '></input>';
                        else
                            return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';
                    else
                        return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';
                } else {
                    if (!is_null($record->location->community_id) && $record->ticket_type == "resident")
                        if (!is_null($record->user) && !empty($record->user->email))
                            return '<input class="checkboxrow" type="checkbox" data-id=' . $record->id . '></input>';
                        else
                            return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';
                    else
                        return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';

                }
            })->filter(function ($instance) use ($request) {


                if ($request->has('user_id') && !empty($request->user_id)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['user']['id'] == $request->user_id;
                    });
                }

                if ($request->has('ticket_id') && !empty($request->ticket_id)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['ticket_id'] == $request->ticket_id;
                    });
                }

                if ($request->has('license_plate') && !empty($request->license_plate)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $this::containsInsensitive($row['license_plate'], $request->license_plate);
                    });
                }

                if ($request->has('community') && !empty($request->community)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['community'], $request->community);
                    });
                }
                if ($request->has('vehicle') && !empty($request->vehicle)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['vehicle'], $request->vehicle);
                    });
                }

                if ($request->has('is_paid') && !empty($request->is_paid)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['is_paid'] == $request->is_paid;
                    });
                }

                if ($request->has('household') && !empty($request->household)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['household'], $request->household);
                    });
                }

                if ($request->has('from') && !empty($request->from)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return strtotime($row['time']) >= strtotime($request->from);
                    });
                }

                if ($request->has('to') && !empty($request->to)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return strtotime($row['time']) <= strtotime($request->to);
                    });
                }

                if ($request->has('excess_speed_start') && !empty($request->excess_speed_start)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['excess_speed'] >= $request->excess_speed_start;
                    });
                }

                if ($request->has('excess_speed_end') && !empty($request->excess_speed_end)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['excess_speed'] <= $request['excess_speed_end'];
                    });
                }

                if ($request->has('email_count_start') && !empty($request->email_count_start)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['email_log_count'] >= $request->email_count_start;
                    });
                }

                if ($request->has('email_count_end') && !empty($request->email_count_end)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['email_log_count'] <= $request->email_count_end;
                    });
                }

                if ($request->has('violation_count_start') && !empty($request->violation_count_start)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['violation_count_raw'] >= $request->violation_count_start;
                    });
                }

                if ($request->has('violation_count_end') && !empty($request->violation_count_end)) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return $row['violation_count_raw'] <= $request->violation_count_end;
                    });
                }


            })->rawColumns(['is_paid', 'action', 'violation_count', 'mass_mail'])->toJson();
    }

    /**
     * Determine if a given string contains all array values.
     * Not case sensitive
     *
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public function containsInsensitive($haystack, $needles)
    {
        foreach ((array)$needles as $needle) {
            if ($needle !== '' && mb_stripos($haystack, $needle) !== false) {
                return true;
            }
        }

        return false;
    }

    public function generateDatatableUnmatched($request, $data)
    {
        return Datatables::of($data)
            ->addColumn('mass_dismissed_ticket', function ($record) {
                if (is_null($record->deleted_at))
                    return "<input class='checkbox-mass-dismissed-ticket'  type='checkbox' data-id='" . $record->id . "'/>";
                else
                    return "<input class='' title='Already dismissed' disabled type='checkbox' data-id='" . $record->id . "'/>";

            })
            ->addColumn('excess_speed', function ($record) {
                // Calculate Excess Speed
                return $record->violating_speed - $record->current_speed_limit ?? "-";
            })
            ->addColumn('license_plate', function ($record) {
                //
                return strtoupper($record->license_plate) ?? "-";
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('license_plate') && !empty($request->license_plate)) {
                    $instance->where('license_plate', 'LIKE', "%{$request->license_plate}%");
                }
            })
            ->addColumn('community', function ($record) {
                return (!empty($record->location) && !empty($record->location->community)) ? $record->location->community->name : "-";
            })->addColumn('violation_count', function ($record) {
                // Get total Violation Count across all tickets using License Plate
                if (!empty($record->license_plate))
                    return '<a href="' . route('admin.traffic-tickets.tickets-by-license-plate', $record->license_plate) . '">' . $record->traffic_tickets_count . '</a>';
                else
                    return $record->traffic_tickets_count;
            })->addColumn('total_violation_count_sort', function ($record) {
                // Get total Violation Count across all tickets using License Plate
                if (!empty($record->license_plate))
                    return '<a href="' . route('admin.traffic-tickets.tickets-by-license-plate', $record->license_plate) . '">' . $record->total_violation_count . '</a>';
                else
                    return $record->total_violation_count;
            })->editColumn('vehicle', function ($record) {
                return is_null($record->vehicle) ? "N/A" : $record->vehicle->make . " " . $record->vehicle->model;
            })->addColumn('household', function ($record) {
                // Get the Household of the user from User's Account - Return NA if not found
                if (!empty($record->user_id) && !empty($record->user)) {
                    if (!empty($record->user->house))
                        return $record->user->house->house_name;
                }
                return "N/A";
            })->addColumn('action', function ($record) {
                $btn = '<div class="dropdown">';

                if (is_null($record->deleted_at))
                    $btn .= '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>';
                else
                    $btn .= '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">Action</button>';

                $btn .= '<div class="dropdown-menu">';


                //If Ticket Type is Unmatched
                if ($record->ticket_type == "unmatched")
                    $btn .= '<a class="dropdown-item" href="#" id="editLicBtn" data-id="' . $record->id . '" data-lic="' . $record->license_plate . '"><i class="fa fa-edit" aria-hidden="true"></i> Edit License Plate</a>
                <a class="dropdown-item" href="#" id="attachResidentBtn" data-id="' . $record->id . '"><i class="fa fa-edit" aria-hidden="true"></i> Attach to Resident </a>
                <a class="dropdown-item" href="#" id="attachVendorBtn" data-id="' . $record->id . '"><i class="fa fa-edit" aria-hidden="true"></i> Attach to Vendor </a>';

                $btn .= '<a class="dropdown-item" href="#" id="ticketDetailBtn';
                if ($record->ticket_type == 'unmatched')
                    $btn .= 'Unmatched';
                $btn .= '" data-id="' . $record->id . '"><i class="fa fa-eye" aria-hidden="true"></i> Ticket Detail </a>';
                //Show Email and Email Log only if User has Email, Or User is a part of community
                if ($record->ticket_type == 'vendor') {
                    if (!empty($record->vehicle))
                        if (!empty($record->vehicle->vendor[0]->community_id) && !empty($record->vehicle->vendor[0]->email))
                            $btn .= '<a class="dropdown-item sendMail" data-id="' . $record->id . '" data-communityId="' . $record->vehicle->vendor[0]->community_id . '" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a>';
                        else
                            $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';
                    else
                        $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';
                } else {
                    if (!is_null($record->location->community_id) && $record->ticket_type == "resident")
                        if (!is_null($record->user) && !empty($record->user->email))
                            $btn .= '<a class="dropdown-item sendMail" data-id="' . $record->id . '" data-communityId="' . $record->location->community_id . '" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a>';
                        else
                            $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';
                    else
                        $btn .= '<span data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work."><a class="dropdown-item disabled"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email </a></span>';

                }

                $btn .= '                                    <a class="dropdown-item" href="' . route('admin.email-logs.show', $record->id) . '"><i class="fa fa-file-signature" aria-hidden="true"></i> Email Log </a>
                    <a class="dropdown-item" data-ticketID="' . $record->id . '" id="ticketImagesBtn" href="#"><i class="fa fa-edit" aria-hidden="true" id="ticketImages"></i> Ticket Images (' . $record->ticket_images_count . ') </a>';
                if (is_null($record->deleted_at))
                    $btn .= '<a class="dropdown-item" href="#" data-id="' . $record->id . '" id="deleteTicketBtn"><i class="fa fa-times-circle" aria-hidden="true" ></i> Dismiss Ticket </a>';
                else
                    $btn .= '<a class="dropdown-item" href="#" data-id="' . $record->id . '" id="undeleteTicketBtn"><i class="fa fa-times-circle" aria-hidden="true" ></i> Revert Ticket </a>';

                $btn .= '</div>
                </div>';

                return $btn;
            })->editColumn('is_paid', function ($record) {
                $btn = '<div class="form-check">
            <input class="form-check-input payment" type="checkbox" value="" id="payment" ';
                $btn .= ($record->is_paid == '0') ? '' : 'checked';
                $btn .= ' data-ticketID="' . $record->id . '">
            <label class="form-check-label" for="payment">';
                $btn .= ($record->is_paid == '0') ? 'Pending' : 'Paid';
                return $btn .= '</label>
          </div>';
            })->addColumn('formatted_time', function ($record) {
                return date('m-d-Y h:i:s A', strtotime($record->time));
            })->addColumn('mass_mail', function ($record) {
                if ($record->ticket_type == 'vendor') {
                    if (!empty($record->vehicle))
                        if (!empty($record->vehicle->vendor[0]->community_id) && !empty($record->vehicle->vendor[0]->email))
                            return '<input class="checkboxrow" type="checkbox" data-id=' . $record->id . '></input>';
                        else
                            return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';
                    else
                        return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';
                } else {
                    if (!is_null($record->location->community_id) && $record->ticket_type == "resident")
                        if (!is_null($record->user) && !empty($record->user->email))
                            return '<input class="checkboxrow" type="checkbox" data-id=' . $record->id . '></input>';
                        else
                            return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';
                    else
                        return '<input data-toggle="tooltip" data-placement="left" title="User must have a valid email and community for this feature to work." type="checkbox" disabled data-type="' . $record->ticket_type . '" data-community="' . $record->community . '"></input>';

                }
            })
            ->rawColumns(['is_paid', 'action', 'violation_count', 'mass_dismissed_ticket', 'total_violation_count_sort'])->toJson();
    }

    public function ticketImages(Request $request)
    {
        $data = $this->trafficTicketImageService->getImagesByTicketID($request->ticket_id);

        if ($data['bool']) {
            $response = ['status' => true, 'result' => $data['result']];
        } else {
            $response = ['status' => false, 'message' => $data['message']];
        }

        return $response;
    }

    // To be moved to email logs controller
    public function getTicketTemplatesByCommunity(Request $request)
    {

        $community_id = null;

        if ($request->has('community_id'))
            $community_id = $request->community_id;

        $data = $this->communityService->getTicketTemplatesByCommunityAndType($community_id, 'trafficlogix');

        if ($data['bool']) {
            $response = ['status' => true, 'result' => $data['result']];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong. Please try again.'];
        }

        return $response;
    }

    // To be moved to email logs controller
    public function sendEmail(Request $request)
    {

        $response = ['status' => true, 'message' => 'Email Sent Successfully.'];

        try {
            $email = null;
            $ticket = $this->trafficTicketService->ticketDetails($request->ticketId);
            if ($ticket['result']->ticket_type == 'vendor') {
                $email = $ticket['vendor']->email;
            } else {
                $email = $ticket['result']->user->email;
            }

            ticketEmail($email, $request->template, $request->title);

            $this->emailLogService->create([
                'to' => $email,
                'subject' => $request->title,
                'body' => $request->template,
                'ticket_id' => $request->ticketId,
                'email_sent_by' => auth()->user()->id,
                'email_status' => 'Email sent successfully',
            ]);

        } catch (Exception $e) {
            $response = ['status' => false, 'message' => "Something went wrong. Please try again."];
        }

        return $response;
    }

    // To be moved to email logs controller
    public function ticketDetails(Request $request)
    {
        $data = $this->trafficTicketService->ticketDetails($request->ticket_id);
        if ($data['bool']) {
            $response = ['status' => true,
                'result' => $data['result'],
                'pass' => $data['pass'],
                'vendor' => $data['vendor']];
        } else {
            $response = ['status' => false, 'message' => $data['message']];
        }

        return $response;
    }

    public function payment(Request $request)
    {
        try {

            // Get The Payee ID of Ticket
            $payee_id = $this->trafficTicketService->find($request->ticketId)['result']->user_id;

            //Get the Status
            $status = ($request->checked == "true") ? "Paid" : "Pending";

            // Mark Ticket as Paid/Unpaid
            $this->trafficTicketService->update(
                ['is_paid' => ($request->checked == "true") ? "1" : "0"],
                $request->ticketId);

            //LoggedIn USer
            $user = $request->user()->id;
            // Save the Payment Log
            $success = $this->trafficPaymentLogService->create([
                'status' => 'Successfully mark as ' . $status,
                'ticket_id' => $request->ticketId,
                'created_by' => $user,
                'payee_user_id' => $payee_id,
                'date' => date('Y-m-d')
            ]);


            if ($success['bool']) {

                //Mark:- Track Activity
                callUserActivityLogs("Vendor Payment mark", __FUNCTION__, __FILE__, __DIR__, true, 'Vendor Payment mark Successfully');

                return [
                    'status' => true,
                    'message' => 'Ticket status changed',
                ];
            } else {
                throw new Exception($success['message']);
            }


        } catch (Exception $exception) {

            //Mark:- Track Activity
            callUserActivityLogs("Vendor Payment mark", __FUNCTION__, __FILE__, __DIR__, false, 'Vendor Payment mark Failed');

            return [
                'status' => false,
                'message' => "Something Went Wrong",
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = $this->trafficTicketService->delete($id);
        if ($data['bool']) {

            //Mark:- Track Activity
            callUserActivityLogs("Ticket Deleted", __FUNCTION__, __FILE__, __DIR__, true, 'Tickets Deleted Successfully');

            $response = ['status' => true, 'message' => 'Delete Successfully'];
        } else {
            //Mark:- Track Activity
            callUserActivityLogs("Ticket Deleted", __FUNCTION__, __FILE__, __DIR__, false, 'Tickets Delete Failed');

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    // Future Work - Need Optimization + Logic Merging of Print and Send Email
    public function restore($id)
    {
        $data = $this->trafficTicketService->restore($id);
        if ($data['bool']) {
            $response = ['status' => true, 'message' => 'Restored Successfully'];
        } else {
            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
        return response()->json($response);
    }

    // Urgent Work - Need Optimization + Logic Merging of Print and Send Email
    public function sendBulkEmail(Request $request)
    {
        try {
            foreach ($request->ids as $ticket_id) {
                $data = $this->trafficTicketService->ticketDetails($ticket_id);
                $ticket = $data['result'];
                $datetime = date("m/d/Y H:i:s A");
                $geocode = $ticket->location->geocode;
                $map_url = 'https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=300x300&maptype=roadmap&markers=color:red%7Clabel:Z%7C' . substr($geocode, 1, -1) . '.&key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA';
                $community_id = null;
                $email = null;

                if ($ticket->ticket_type == 'vendor') {
                    $email = $data['vendor']->email;
                    $community_id = $data['vendor']->community_id;
                } else {
                    $email = $data['result']->user->email;
                    $community_id = $data['result']->location->community_id;
                }

                if (!empty($email) && !empty($community_id)) {
                    // Get the Ticket Templates by Community ID
                    $template = $this->communityService->getTicketTemplatesByCommunitySlugAndType($community_id, 'trafficlogix', $request->template);

                    if ($template['bool'] && !empty($template['result'])) {
                        $template = $template['result']->toArray();

                        $title = null;
                        if (!empty($template['email_templates'])) {
                            $title = $template['email_templates'][0]['name'];
                            $template = $template['email_templates'][0]['template'];

                        }

                        $template = str_replace('{ticket_id}', $ticket->ticket_id, $template);
                        $template = str_replace('{plate_text}', $ticket->license_plate, $template);
                        $template = str_replace('{timestamp}', $datetime, $template);
                        $template = str_replace('{ticket_time}', $ticket->time, $template);
                        $template = str_replace('{current_speed_limit}', $ticket->current_speed_limit, $template);
                        $template = str_replace('{violating_speed}', $ticket->violating_speed, $template);
                        $template = str_replace('{location_address}', $ticket->location->name, $template);
                        $template = str_replace('{location_name}', $ticket->location->name, $template);
                        $image = "";
                        $images = $ticket->toArray()['ticket_images'];
                        if ($images) {
                            $image = '<img src="' . env('APP_URL') . '/images/tickets/' . $images[0]['image'] . '" style="width:400px" />';
                        } else {
                            $image = '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/832px-No-Image-Placeholder.svg.png" style="width:400px" />';
                        }
                        $template = str_replace("{image}", $image, $template);
                        $template = str_replace('{notice_number}', $ticket->email_log_count + 1, $template);
                        $template = str_replace('{geocode}', '<img src="' . $map_url . '" />', $template);
                        $user_id = auth()->user()->id;
                        dispatch(new TicketEmailSender(compact('title', 'template', 'email', 'ticket_id', 'user_id')));

                    }
                }
            }

            //Mark:- Track Activity
            callUserActivityLogs("Bulk Email Sent", __FUNCTION__, __FILE__, __DIR__, true, 'Bulk Email Sent Successfully');


            $response = ['status' => true, 'message' => 'Emails Sent'];
            return response()->json($response);

        } catch (Exception $e) {
            return $e;

            //Mark:- Track Activity
            callUserActivityLogs("Bulk Email Sent", __FUNCTION__, __FILE__, __DIR__, false, 'Bulk Email Sent Failed');

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
            return response()->json($response);

        }

    }

    public function printBulkEmail(Request $request)
    {
        try {

            $html = '';
            foreach ($request->ids as $ticket_id) {
                $data = $this->trafficTicketService->ticketDetails($ticket_id);
                $ticket = $data['result'];
                $datetime = date("m/d/Y H:i:s A");
                $geocode = $ticket->location->geocode;
                $map_url = 'https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=300x300&maptype=roadmap&markers=color:red%7Clabel:Z%7C' . substr($geocode, 1, -1) . '&key=AIzaSyBMJa73RYD3-HOwR9ndGWS3SxH9mp4qkJA';
                $community_id = null;
                $email = null;

                if ($ticket->ticket_type == 'vendor') {
                    $email = $data['vendor']->email;
                    $community_id = $data['vendor']->community_id;
                } else {
                    $email = $data['result']->user->email;
                    $community_id = $data['result']->location->community_id;
                }

                if (!empty($email) && !empty($community_id)) {
                    // Get the Ticket Templates by Community ID
                    $template = $this->communityService->getTicketTemplatesByCommunitySlugAndType($community_id, 'trafficlogix', $request->template);

                    if ($template['bool'] && !empty($template['result'])) {
                        $template = $template['result']->toArray();

                        $title = null;
                        if (!empty($template['email_templates'])) {
                            $title = $template['email_templates'][0]['name'];
                            $template = $template['email_templates'][0]['template'];

                        }

                        $template = str_replace('{ticket_id}', $ticket->ticket_id, $template);
                        $template = str_replace('{plate_text}', $ticket->license_plate, $template);
                        $template = str_replace('{timestamp}', $datetime, $template);
                        $template = str_replace('{ticket_time}', $ticket->time, $template);
                        $template = str_replace('{current_speed_limit}', $ticket->current_speed_limit, $template);
                        $template = str_replace('{violating_speed}', $ticket->violating_speed, $template);
                        $template = str_replace('{location_address}', $ticket->location->name, $template);
                        $template = str_replace('{location_name}', $ticket->location->name, $template);
                        $image = "";
                        $images = $ticket->toArray()['ticket_images'];
                        if ($images) {
                            $image = '<img src="' . env('APP_URL') . '/images/tickets/' . $images[0]['image'] . '" style="width:400px" />';
                        } else {
                            $image = '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/832px-No-Image-Placeholder.svg.png" style="width:400px" />';
                        }
                        $template = str_replace("{image}", $image, $template);
                        $template = str_replace('{notice_number}', $ticket->email_log_count + 1, $template);
                        $template = str_replace('{geocode}', '<img src="' . $map_url . '" />', $template);
                        $html .= $template;
                        $html .= '<div style="page-break-after: always;"></div>';

                    }
                }
            }

            //Mark:- Track Activity
            callUserActivityLogs("Ticket Printed", __FUNCTION__, __FILE__, __DIR__, true, 'Ticket Printed Successfully');


            $response = ['status' => true, 'data' => $html];
            return response()->json($response);

        } catch (Exception $e) {
            return $e;

            //Mark:- Track Activity
            callUserActivityLogs("Ticket Printed", __FUNCTION__, __FILE__, __DIR__, false, 'Ticket Printed Failed');

            $response = ['status' => false, 'message' => 'Something went wrong.Please try again.'];
            return response()->json($response);

        }

    }

}
