<?php
define('ZUUL_SYSTEMS_IOS_LINK', 'https://apps.apple.com/us/app/zuul-systems/id1438385504');
define('ZUUL_GUARD_IOS_LINK', 'https://apps.apple.com/in/app/zuul-guard/id1438388878');
define('ZUUL_SYSTEMS_ANDROID_LINK', 'https://play.google.com/store/apps/details?id=com.reavertechnologies.zuulmaster&hl=en&gl=US');
define('ZUUL_GUARD_ANDROID_LINK', 'https://play.google.com/store/apps/details?id=com.reavertechnologies.zuulguard&hl=en&gl=US');

use App\Jobs\DynamicEmailJob;
use App\Mail\DynamicEmail;
use App\Models\Community;
use App\Models\EmailTemplate;
use App\Models\Notification;
use App\Models\ParentalControl;
use App\Models\PrintersTemplate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use libphonenumber\PhoneNumberUtil;
use libphonenumber\PhoneNumberFormat;
use Illuminate\Support\Facades\Auth;
use App\Models\Log;
use Illuminate\Support\Facades\DB;
use App\Helpers\APIResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * @return Carbon
 * @throws Exception
 */
function currentConvertedDateTime()
{
    $now = Carbon::now();
    $systemTimeZone = config('app.timezone');
    return Carbon::parse($now, $systemTimeZone);
}

/**
 * @param $dateTime
 * @param $fromTimeZone
 * @param $toTimeZone
 *
 * @return false|int
 * @throws Exception
 */
function convertTimezone($dateTime, $fromTimeZone, $toTimeZone)
{
    $date = new DateTime($dateTime, new DateTimeZone($fromTimeZone));
    $date->setTimezone(new DateTimeZone($toTimeZone));

    return strtotime($date->format('Y-m-d H:i:s'));
}

function convertedPassDate($dateTime, $fromTimeZone, $toTimeZone)
{
    $date = new DateTime($dateTime, new DateTimeZone($fromTimeZone));
    $date->setTimezone(new DateTimeZone($toTimeZone));

    return strtotime($date->format('Y-m-d H:i:s'));
}

function convertedPassTime($dateTime, $fromTimeZone, $toTimeZone)
{
    $date = new DateTime($dateTime, new DateTimeZone($fromTimeZone));
    $date->setTimezone(new DateTimeZone($toTimeZone));

    return strtotime($date->format('Y-m-d H:i:s'));
}

/**
 * @param $imageUrl
 *
 * @return string
 */
function getS3Image($imageUrl)
{
    $s3 = Storage::disk('s3');
    $exists = true;
    if (!$exists) {
        return false;
    } else {
        try {
            //image is in server directory
            $client = $s3->getDriver()->getAdapter()->getClient();
            $expiry = "+10 minutes";
            $command = $client->getCommand('GetObject', [
                'Bucket' => 'zuul-private-files',
                'Key' => $imageUrl,
            ]);
            $request = $client->createPresignedRequest($command, $expiry);
            $aws_path = (string)$request->getUri();

            return $aws_path;
        } catch (Exception $exception) {
            return false;
        }
    }
}

/**
 * check alphanumeric validation
 *
 * @param $string
 *
 * @return bool
 */
function alphaNumaric($string)
{
    if (preg_match('/[!@#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $string)) {
        return false;
    }

    return true;
}

/**
 * remove all special character from string
 *
 * @param $string
 *
 * @return string
 */
function removeSpecialCharacters($string)
{
    return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars.
}

/**
 * get email template
 *
 * @param $assignTo
 *
 * @return mixed
 */
function getEmailTemplate($assignTo)
{
    return EmailTemplate::where('assign_to', $assignTo)->first();
}


/**
 * get printer template
 *
 * @param $assignTo
 *
 * @return mixed
 */
function getPrinterTemplate($templateName, $community_id)
{
    return PrintersTemplate::where('community_id', $community_id)->first();
}

/**
 * send dynamic email template wise
 *
 * @param      $data
 * @param      $receiverEmail
 * @param bool $shouldQueue
 */
function dynamicEmail($data, $receiverEmail, $shouldQueue = false)
{

    $emailData = array();
    $templateData = array();
    $sendResetEmail = false;
    $emailTo = $receiverEmail;
    $type = (isset($data['type']) && !empty($data['type'])) ? $data['type'] : "";

    if ($type == 'community_admin') {
        $subject = "ZUUL Systems - Become Community Admin";
        $templateData['admin_name'] = $data['user']->fullName ?? "";
        $templateData['role_description'] = $data['user']->role->name ?? "";
        $token = Password::getRepository()->create($data['user']);
        $resetLink = route('password.reset', ['token' => $token, 'email' => $emailTo]);
        $templateData['reset_password_link'] = "Please <a href='{$resetLink}'>Click Here</a> to create your password";

        $emailTemplate = getEmailTemplate('become_community_admin');
        $template = $emailTemplate->template ?? "";
        $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
        $emailData['subject'] = $subject;
        Mail::to($emailTo)->send(new DynamicEmail($emailData));
    } elseif ($type == 'kiosk') {
        $subject = "ZUUL Systems - Become Kiosk Admin";
        $templateData['admin_name'] = $data['user']->fullName ?? "";
        $templateData['role_description'] = $data['user']->role->name ?? "";
        $token = Password::getRepository()->create($data['user']);
        $resetLink = route('password.reset', ['token' => $token, 'email' => $emailTo]);
        $templateData['reset_password_link'] = "Please <a href='{$resetLink}'>Click Here</a> to create your password";

        $emailTemplate = getEmailTemplate('become_community_admin');
        $template = $emailTemplate->template ?? "";
        $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
        $emailData['subject'] = $subject;
        Mail::to($emailTo)->send(new DynamicEmail($emailData));
    } elseif ($type == 'guard') {
        //Welcome login email
        $subject = "ZUUL Systems - Welcome";
        $templateData['receiver'] = $data['user']->role->name ?? "";
        $templateData['phone_number'] = $data['user']->formattedPhone ?? "";
        $templateData['android_link'] = '<a href=' . ZUUL_GUARD_ANDROID_LINK . '>' . ZUUL_GUARD_ANDROID_LINK . '</a>';
        $templateData['ios_link'] = '<a href=' . ZUUL_GUARD_IOS_LINK . '>' . ZUUL_GUARD_IOS_LINK . '</a>';
        $templateData['verification_code'] = "Here is your verification code:<span style='background-color: yellow'>" . $data['hash'] . "</span>";
        $emailTemplate = getEmailTemplate('security_guard_email');
        $template = $emailTemplate->template ?? "";
        $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
        $emailData['subject'] = $subject;
        Mail::to($emailTo)->send(new DynamicEmail($emailData));
        $sendResetEmail = true;
    } elseif ($type == 'resident') {
        //welcome login email
        $subject = "ZUUL Systems - Welcome";
        $templateData['receiver'] = $data['user']->fullName ?? "";
        $templateData['user_type'] = $data['user']->role->name ?? "";
        $templateData['phone_number'] = numberFormat($data['user']->phone_number, $data['user']->dial_code) ?? "";
        $templateData['community_name'] = $data['user']->community->name ?? "";

        $templateData['android_link'] = '<a href=' . ZUUL_SYSTEMS_ANDROID_LINK . '>' . ZUUL_SYSTEMS_ANDROID_LINK . '</a>';
        $templateData['ios_link'] = '<a href=' . ZUUL_SYSTEMS_IOS_LINK . '>' . ZUUL_SYSTEMS_IOS_LINK . '</a>';
        $templateData['temporary_password'] = $data['user']->temp_password;
        $emailTemplate = getEmailTemplate('welcome_login');
        $template = $emailTemplate->template ?? "";
        $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
        $emailData['subject'] = $subject;
        $emailData['emailTo'] = $emailTo;
        if ($shouldQueue) { // send email from queues
            dispatch(new DynamicEmailJob($emailData));
        } else {
            Mail::to($emailTo)->send(new DynamicEmail($emailData));
        }
        $sendResetEmail = true;
    }
}


/**
 * send dynamic ticket template wise
 *
 * @param      $data
 * @param      $receiverEmail
 * @param bool $shouldQueue
 */

function ticketEmail($email, $template, $subject, $shouldQueue = false)
{
    $emailTo = $email;
    $emailData['template'] = $template;
    $emailData['subject'] = $subject;

    if ($shouldQueue) {
        DynamicEmailJob::dispatch($emailData);
    } else {
        Mail::to($emailTo)->send(new DynamicEmail($emailData));
    }
}

/**
 * send reset password email
 *
 * @param $email
 *
 * @return bool
 */
function sendResetPasswordLink($email)
{
    $credentials = ['email' => $email];
    $response = Password::sendResetLink($credentials);
    switch ($response) {
        case Password::RESET_LINK_SENT:
            return true;
        case Password::INVALID_USER:
            return false;
    }
}

/**
 * Replace variables with values
 *
 * @param $template
 * @param $variables
 *
 * @return mixed
 */
function replaceTemplateStringsAndSendmail($template, $variables)
{
    $search = array(
        '{receiver}',
        '{admin_name}',
        '{verification_code}',
        '{sender}',
        '{role_description}',
        '{event_name}',
        '{sender_name}',
        '{sender_community}',
        '{sender_phone}',
        '{pass_start_date}',
        '{pass_date}',
        '{description}',
        '{qr_code}',
        '{random_password}',
        '{user_type}',
        '{receiver_email}',
        '{android_link}',
        '{ios_link}',
        '{requested_to_name}',
        '{wordings}',
        '{full_name}',
        '{phone_number}',
        '{temporary_password}',
        '{plate_text}',
        '{community}',
        '{date}',
        '{location_name}',
        '{current_speed_limit}',
        '{violating_speed}',
        '{ticket_time}',
        '{timestamp}',
        '{notice_number}',
        '{geocode}',
        '{ticket_id}',
        '{location_address}',
        '{image}',
        '{reset_password_link}',
    );
    $replace = array(
        $variables['receiver'] ?? "",
        $variables['admin_name'] ?? "",
        $variables['verification_code'] ?? "",
        $variables['sender'] ?? "",
        $variables['role_description'] ?? "",
        $variables['event_name'] ?? "",
        $variables['sender_name'] ?? "",
        $variables['sender_community'] ?? "",
        $variables['sender_phone'] ?? "",
        $variables['pass_start_date'] ?? "",
        $variables['pass_date'] ?? "",
        $variables['description'] ?? "",
        $variables['qr_code'] ?? "",
        $variables['random_password'] ?? "",
        $variables['user_type'] ?? "",
        $variables['receiver_email'] ?? "",
        $variables['android_link'] ?? "",
        $variables['ios_link'] ?? "",
        $variables['requested_to_name'] ?? "",
        $variables['wordings'] ?? "",
        $variables['full_name'] ?? "",
        $variables['phone_number'] ?? "",
        $variables['temporary_password'] ?? "",
        $variables['plate_text'] ?? "",
        $variables['community_name'] ?? "",
        $variables['date'] ?? "",
        $variables['location_name'] ?? "",
        $variables['current_speed_limit'] ?? "",
        $variables['violating_speed'] ?? "",
        $variables['ticket_time'] ?? "",
        $variables['timestamp'] ?? "",
        $variables['notice_number'] ?? "",
        $variables['geocode'] ?? "",
        $variables['ticket_id'] ?? "",
        $variables['location_address'] ?? "",
        $variables['image'] ?? "",
        $variables['reset_password_link'] ?? "",
    );

    return str_replace($search, $replace, $template);
}

/**
 * Replace variables with values
 *
 * @param $template
 * @param $variables
 *
 * @return mixed
 */
function replacePrinterTemplateStrings($template, $variables)
{

    $search = array(
        '{date}',
        '{issued_date}',
        '{time}',
        '{time_in}',
        '{father_name}',
        '{guest_name}',
        '{building_or_unit_name}',
        '{phone}',
        '{tag}',
        '{make}',
        '{model}',
        '{color}',
        '{vehicle_detail}',

        '{community_name}',
        '{expiration_date}',
        '{license_plate}',
        '{directions_to}',
    );
    $replace = array(
        $variables['date'] ?? "",
        $variables['issued_date'] ?? "",
        $variables['time'] ?? "",
        $variables['time_in'] ?? "",
        $variables['father_name'] ?? "",
        $variables['guest_name'] ?? "",
        $variables['building_or_unit_name'] ?? "",
        $variables['phone'] ?? "",
        $variables['tag'] ?? "",
        $variables['make'] ?? "",
        $variables['model'] ?? "",
        $variables['color'] ?? "",
        $variables['vehicle_detail'] ?? "",

        $variables['community_name'] ?? "",
        $variables['expiration_date'] ?? "",
        $variables['license_plate'] ?? "",
        $variables['directions_to'] ?? "",

    );

    return str_replace($search, $replace, $template);
}

/**
 * Replace variables with values
 *
 * @param $template
 * @param $variables
 *
 * @return mixed
 */
function replacePrinterVisitorPassTemplateStrings($template, $variables)
{

    $search = array(
        '{community_name}',
        '{expiration_date}',
        '{time}',
        '{issued_date}',
        '{make}',
        '{guest_name}',
        '{license_plate}',
        '{directions_to}',
    );
    $replace = array(
        $variables['community_name'] ?? "",
        $variables['expiration_date'] ?? "",
        $variables['time'] ?? "",
        $variables['issued_date'] ?? "",
        $variables['make'] ?? "",
        $variables['guest_name'] ?? "",
        $variables['license_plate'] ?? "",
        $variables['directions_to'] ?? "",

    );

    return str_replace($search, $replace, $template);
}

/**
 * Replace variables with values
 *
 * @param $template
 * @param $variables
 *
 * @return mixed
 */
function replacePrinterGuestParkingTemplateStrings($template, $variables)
{

    $search = array(
        '{issued_date}',
        '{time}',
        '{father_name}',
        '{guest_name}',
        '{building_or_unit_name}',
        '{phone}',
        '{tag}',
        '{vehicle_detail}'
    );
    $replace = array(
        $variables['issued_date'] ?? "",
        $variables['time'] ?? "",
        $variables['father_name'] ?? "",
        $variables['guest_name'] ?? "",
        $variables['building_or_unit_name'] ?? "",
        $variables['phone'] ?? "",
        $variables['tag'] ?? "",
        $variables['vehicle_detail'] ?? "",
    );

    return str_replace($search, $replace, $template);
}


/**
 * generate random code
 *
 * @param int $length
 *
 * @return string
 */
function generateRandomCode($length = 25)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

/**
 * generate verification code for guard
 *
 * @return string
 */
function guardVerificationCode()
{
    $verificationCode = generateRandomCode(6);
    $result = User::select('verification_code')->where('verification_code', trim($verificationCode))->get();
    if ($result->count() > 0) {
        return guardVerificationCode();
    }

    return $verificationCode;
}

/**
 * generate verification code for reset password sequence
 *
 * @return string
 */
function resetPasswordVerificationCode()
{
    $verificationCode = mt_rand(100000, 999999); // better than rand()
    $result = User::select('reset_verification_code')->where('reset_verification_code', trim($verificationCode))->get();
    if ($result->count() > 0) {
        return resetPasswordVerificationCode();
    }

    return $verificationCode;
}

function generateRandomNumber()
{
    return random_int(10000000, 99999999); // better than rand()
}

/**
 * @param int $length
 * @return string
 */
function incrementalHash($length = 3)
{
    $letters = 'abcdefghijklmnopqrstuvwxyz';
    $string = '';
    for ($x = 0; $x < $length; ++$x) {
        $string .= $letters[rand(0, 25)] . rand(0, 9);
    }
    return $string;
}

/**
 * email verification code for guard
 *
 * @return string
 */
function emailVerificationCode()
{
    $verificationCode = incrementalHash();
    $result = User::select('email_verification_code')->where('email_verification_code', trim($verificationCode))->get();
    if ($result->count() > 0) {
        return emailVerificationCode();
    }

    return $verificationCode;
}

/**
 * @param int $length
 *
 * @return string
 */
function generateRandomStringWithSpecialCharacter($length = 3)
{
    $letters = 'abcdefghijklmnopqrstuvwxyz';
    $capletters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';
    for ($x = 0; $x < $length; ++$x) {
        $string .= $letters[rand(0, 25)] . rand(0, 9);
    }
    $string .= rand(0, 9);

    return $capletters[rand(0, 25)] . $string;
}

/**
 * get community Id of user
 *
 * @return |null
 */
function getCommunityIdByUser()
{
    $communityId = null;
    if (auth()->user()->role->CommunityAdmin() || auth()->user()->role->GuardAdmin()) {
        $communityId = auth()->user()->community_id;
    }

    return $communityId;
}

/**
 * generate url by user & role Id
 * @param $user
 * @return string|null
 */
function generateUrlByUserRoleId($user)
{
    $url = null;
    $userId = $user->id ?? null;
    $roleId = $user->role_id ?? null;
    //check if user is resident
    if ($roleId == 4 || $roleId == 5) {
        $url = route('admin.residential.residents.index', ['userId' => $userId]);
    }
    //check if user is guard
    if ($roleId == 8 || $roleId == 11) {
        $url = route('admin.user.guards.index', ['userId' => $userId]);
    }
    //check if user is guest
    if ($roleId == 6 || $roleId == 7) {
        $url = route('admin.user.guests.index', ['userId' => $userId]);
    }
    //check if user is community admin
    if ($roleId == 2) {
        $url = route('admin.user.community-admins.index', ['userId' => $userId]);
    }
    if ($roleId == 12) {
        $url = route('admin.user.kiosk-admins.index', ['userId' => $userId]);
    }
    return $url;
}

/**
 * Phone Number Formatting
 *
 * @param $pn
 *
 * @return false|string|null
 */
function formattedPhoneDigits($number)
{


    return $number;
}

// for number formatting

/**
 * -------------------------------------------------------
 * Format Phone
 * -------------------------------------------------------
 * @param string $number
 * @param string $dial_code - i.e +1, +504
 * @param $type - NATIONAL, INTERNATIONAL, E164
 */

function numberFormat($number, $dial_code, $type = 'NATIONAL')
{
    if (!$number) {
        return "";
    }

    //If the dial code is without + sign in any case
    if (!preg_match('/\+/', $dial_code)) {
        $dial_code = "+".$dial_code;
    }

    $jsonString = file_get_contents(resource_path('json/countries.json'));
    $jsonData = json_decode($jsonString, true);

    foreach ($jsonData as $object) {

        if ($object['dial_code'] === $dial_code) {
            $country_code = $object['code'];

            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            $phoneNumberObject = $phoneNumberUtil->parse($number, $country_code);
            $phoneNumberFormat = "";
            if ($type == 'NATIONAL') {
                $phoneNumberFormat = $phoneNumberUtil->format($phoneNumberObject, PhoneNumberFormat::NATIONAL);
            }
            if ($type == 'INTERNATIONAL') {
                // PhoneNumberFormat::INTERNATIONAL doenst meet formating requiremnts so i put dial code at start instead of using PhoneNumberFormat::INTERNATIONAL
                $phoneNumberFormat = $dial_code . " " . $phoneNumberUtil->format($phoneNumberObject, PhoneNumberFormat::NATIONAL);
            }
            if ($type == 'E164') {
                $phoneNumberFormat = $phoneNumberUtil->format($phoneNumberObject, PhoneNumberFormat::E164);
            }

            return $phoneNumberFormat;
        }
    }
}

function onlyPhoneDigits($pn)
{
    if (!is_null($pn)) {
        $pn = preg_replace('/\D/', '', $pn);
        $pn = substr($pn, -10);

        return $pn;
    }

    return null;
}

/**
 * get communities
 *
 * @return null
 */
function getCommunities()
{
    $communityId = getCommunityIdByUser();
    $communities = null;
    if ($communityId != null) {
        $communities = Community::where('id', $communityId)->get();
    } else if (!empty(auth()->user()->kioskPermission)) {
        $communityIds = explode(",", auth()->user()->kioskPermission->communities);
        $communities = Community::whereIn('id', $communityIds)->get();
    } else {
        $communities = Community::orderBy('name', 'ASC')->get();
    }

    return $communities;
}

/**
 * get active communities
 *
 * @return null
 */
function getActivateCommunities()
{
    $communityId = getCommunityIdByUser();
    $communities = null;
    if ($communityId != null) {
        $communities = Community::where('community_status', 'active')->where('id', $communityId)->get();
    } else if (!empty(auth()->user()->kioskPermission)) {
        $communityIds = explode(",", auth()->user()->kioskPermission->communities);
        $communities = Community::whereIn('id', $communityIds)->where('community_status', 'active')->orderBy('name', 'ASC')->get();
    } else {
        $communities = Community::where('community_status', 'active')->orderBy('name', 'ASC')->get();
    }

    return $communities;
}

/**
 * @param $phoneNumber
 *
 * @return false|string|string[]|null
 */
function removeSpecialCharactersFromPhoneNumber($phoneNumber)
{
    if (!is_null($phoneNumber)) {
        $phoneNumber = preg_replace('/\D/', '', $phoneNumber);
        $phoneNumber = substr($phoneNumber, -10);

        if (strlen($phoneNumber) < 10) {
            $phoneNumber = str_pad($phoneNumber, 10, "0", STR_PAD_LEFT);
        }

        return $phoneNumber;
    }

    return null;
}

/**
 * Convert Community Time To DB Time
 *
 * @param $date
 * @param $community_id
 *
 * @return mixed
 */
function communityTzToDBTz($date, $community_id)
{
    $c = config('app.timezone');
    $community = Community::whereId($community_id)->first();
    if (!$community) {
        return $date;
    }
    $community_timezone = $community->timezone->abbreviation;
    if (!$community_timezone) {
        return $date;
    }

    $d1 = Carbon::parse($date, $community_timezone)->setTimezone($c);

    return $d1;
}

/**
 * To show total number of zuul and parental control notifications
 *
 * @param $notificationType
 * @param $userId
 *
 * @return mixed
 */
function notificationsCount($notificationType, $userId)
{
    $queryBuilder = Notification::query();
    $queryBuilder = $queryBuilder->where('type', $notificationType)->where('is_read', '0');
    if ($notificationType == 'parental_control') {
        $queryBuilder->where('parent_id', $userId);
    } else {
        $queryBuilder->where('user_id', $userId);
    }
    return $queryBuilder->count();
}

function dBTzToCommunityTz($date, $community_id)
{
    $tz = config('app.timezone');
    $community = Community::whereId($community_id)->first();
    if (!$community) {
        return $date;
    }
    $community_timezone = $community->timezone->abbreviation;
    if (!$community_timezone) {
        return $date;
    }

    return Carbon::parse($date, $tz)->setTimezone($community_timezone);
}

function qrCodeLink($qrCode)
{
    $link = env("GOOGLE_API") . $qrcode;

    return '<p><img src="' . $link . '"></p><p><a href="' . $link . '" target="_blank">Click Here</a> to view the QR code for scanning</p>';
}

/**
 * To show error page while countering errors
 *
 */
function checkErrors($result)
{
    if (!$result['bool']) {
        abort(500);
    }
    if (!$result['result']) {
        abort(404);
    }
}

/**
 * To show error page while countering error
 *
 */
function checkError($result)
{
    if (!$result['bool']) {
        abort(500);
    }
}

function formatHoursToText($hour)
{
    $lbl = "";
    switch ($hour) {
        case "1":
            $lbl = "1 Hour";
            break;
        case "3":
            $lbl = "3 Hours";
            break;
        case "6":
            $lbl = "6 Hours";
            break;
        case "12":
            $lbl = "12 Hours";
            break;
        case "24":
            $lbl = "24 Hours";
            break;
        case "48":
            $lbl = "48 Hours";
            break;
        case "168":
            $lbl = "1 Week";
            break;
        case "336":
            $lbl = "2 Weeks";
            break;
        case "720":
            $lbl = "1 Month";
            break;
        case "876000":
            $lbl = "No Limit";
            break;
        case "xxx":
            $lbl = "Custom";
            break;
        default:
            $lbl = $hour . " Hours";
    }
    return $lbl;
}


function checkParentalControlByMember($column, $memberId)
{
    $result = ParentalControl::where("{$column}", '1')->where('member_id', $memberId)->get();
    if ($result->isNotEmpty()) {
        return $result;
    }
    return false;
}

function getParentalControlMembers($column, $createdBy)
{

    $result = User::where('house_id',$createdBy->house_id)->where('role_id',5)->pluck('id')->toArray();
    $result = ParentalControl::whereIn('member_id',$result)->where("{$column}", '1')->get();

    //This will send all the household except the one who is created by

    if ($result->isNotEmpty()) {
        return $result;
    }
    return false;
}

function formattedPhone($phone_number)
{
    $phone = $phone_number;
    if (empty($phone)) {
        return '';
    }
    $ac = substr($phone, 0, 3);
    $prefix = substr($phone, 3, 3);
    $suffix = substr($phone, 6);
    return "({$ac}) {$prefix}-{$suffix}";
}

function DialCodeFormattedPhone($phone_number)
{
    $phone = $phone_number;
    if (empty($phone)) {
        return '';
    }
    $prefix = substr($phone, 0, 4);
    $suffix = substr($phone, 4, 4);
    return "{$prefix}-{$suffix}";
}


function qrCodeToHtml($qrcode)
{
    $link = env("GOOGLE_API") . $qrcode;
    return '
                <p><img src="' . $link . '"></p>
                <p><a href="' . $link . '" target="_blank">Click Here</a> to view the QR code for scanning</p>
            ';
}

/**
 * Send Qrcode to Pass users
 * @param $passUser
 * @param $shouldQueue
 * @return array
 */
function sendQRCodeToEmail($passUser, $shouldQueue)
{
    $emailData = array();
    $templateData = array();
    $pass = $passUser->pass;
    $subject = "ZUUL Systems - Pass received";
    $emailTo = $passUser->user->email;
    //define template variables
    $templateData['receiver'] = $passUser->user->first_name;
    $templateData['event_name'] = $pass->event->name;
    $templateData['sender_name'] = $pass->createdBy->fullName;
    $templateData['sender_community'] = $pass->createdBy->community->name;
    $templateData['description'] = $pass["description"];
    $link = qrCodeToHtml($passUser->qr_code);
    $templateData['qr_code'] = $link;
    $templateData['sender_phone'] = $pass->createdBy->formattedPhone;
    $templateData['pass_start_date'] = $pass->formattedStartDate . ' ' . $pass->formattedStartTime;
    $templateData['pass_date'] = $pass->formattedDate . ' ' . $pass->formattedTime;
    //define email variables
    $emailTemplate = getEmailTemplate('qr_to_email');
    $template = $emailTemplate->template ?? "";
    $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
    $emailData['subject'] = $subject;
    $emailData['emailTo'] = $emailTo;

    try {
        if (!empty($emailData['emailTo']) && $pass->is_quick_pass != 1) {
            if ($shouldQueue) { // send email from queues
                DynamicEmailJob::dispatch($emailData);
            } else {
                Mail::to($emailTo)->send(new DynamicEmail($emailData));
            }
        }
        return [
            'bool' => true,
            'message' => 'Email Sent Successfully'
        ];
    } catch (Exception $exception) {
        return [
            'bool' => false,
            'message' => $exception->getMessage()
        ];
    }
}

/**
 * Guest Arrival Email Template
 * @param $passUser
 * @param $shouldQueue
 * @return array
 */
function sendGuestArrivalEmailToResident($passSentByEmail = null, $scanLogData  , $shouldQueue = false)
{


    if (!isset($scanLogData)) {

        return [
            'bool' => false,
            'message' => "No Scan Log Created"
        ];
    }

    $templateData['receiver'] = $scanLogData['result']->recipient_full_name;

    if (auth()->user()->community_id == 64) {
        $scanDate = new DateTime($scanLogData['result']->scan_date);
        $scanDate->modify('-2 hours');
        $modifiedScanDate = $scanDate->format('Y-m-d H:i:s');

        $templateData['timestamp'] = $modifiedScanDate;
    } else {
        $templateData['timestamp'] = $scanLogData['result']->scan_date;
    }

    //define email variables
    $emailTemplate = getEmailTemplate('guest_arrival');
    $template = $emailTemplate->template ?? "";
    $emailData['template'] = replaceTemplateStringsAndSendmail($template, $templateData);
    $emailData['subject'] = 'Guest Arrived';
    $emailData['emailTo'] = $passSentByEmail;

    try {
        if (!empty($emailData['emailTo'])) {
            if ($shouldQueue) { // send email from queues
                DynamicEmailJob::dispatch($emailData);
            } else {
                Mail::to($passSentByEmail)->send(new DynamicEmail($emailData));

            }
        }
        return [
            'bool' => true,
            'message' => 'Email Sent Successfully'
        ];
    } catch (Exception $exception) {
        return [
            'bool' => false,
            'message' => $exception->getMessage()
        ];
    }
}

/**
 * remove all spaces from string
 * @param $string
 * @return string
 */
function removeSpaceFromString($string)
{
    return trim(str_replace(' ', '', $string));
}

/**
 * Send Qrcode to Pass users
 * @param $data
 * @param $receiverEmail
 * @param bool $shouldQueue
 * @return void
 */
function guestToResidentEmail($data, $receiverEmail, $shouldQueue = false)
{


}

function getBaseUrl($request)
{
    if ($request->get('version') == "localhost") {
        $domain = "https://127.0.0.1:8000/api";
    } else if ($request->get('version') == "6.5.620") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "6.6") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "6.7.640") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "6.8.660") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "6.8.680") {
        $domain = "https://zuul2.zuulsystems.com/api";
    } else if ($request->get('version') == "7.2.688") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "8.7.800") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "8.2.790") {
        $domain = "https://zuul2.zuulsystems.com/api";
    } else if ($request->get('version') == "8.8.820") {
        $domain = "https://app.zuulsystems.com/api";
    } else if ($request->get('version') == "8.8.822") {
        $domain = "https://zuul2.zuulsystems.com/api";
    }else if($request->get('version') == "8.8.840") {
        $domain = "https://app.zuulsystems.com/api";
    } else if($request->get('version') == "9.6.960") {
        $domain = "https://app.zuulsystems.com/api";
    } else if($request->get('version') == "9.8.980") {
        $domain = "https://app.zuulsystems.com/api";
    } else {
        $domain = "https://app.zuulsystems.com/api";
    }

    return response()->json([
        'bool' => true,
        'message' => 'API url',
        'result' => $domain,

    ]);
}


function getSocketBaseUrl($request)
{
    if ($request->get('version') == "localhost") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "6.5.620") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "6.6") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "6.7.640") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "6.8.660") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "6.8.680") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "7.2.688") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "8.7.800") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "8.2.790") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "8.8.820") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if ($request->get('version') == "8.8.822") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if($request->get('version') == "8.8.840") {
        $domain = "https://devsockets.zuulsystems-security.com";
    } else if($request->get('version') == "9.6.960") {
        $domain = "https://guardsockets.zuulsystems-security.com";
    } else if($request->get('version') == "9.7.970") {
        $domain = "https://guardsockets.zuulsystems-security.com";
    } else {
        $domain = "https://devsockets.zuulsystems-security.com";
    }

    //Put this live url to make changes
    return response()->json([
        'bool' => true,
        'message' => 'Socket API url',
        'result' => $domain,

    ]);
}

function resetPassword(Request $request)
{

    $validation = [
        'code' => 'required',
        'password' => 'required'
    ];

    $validator = Validator::make($request->all(), $validation);

    if ($validator->fails()) {
        return APIResponse::error($validator->errors()->first());
    }

    $code = $request->code;
    $password = $request->password;

    $user = User::where('reset_verification_code', $code)->first();

    if ($user) {
        // Update the user's data
        $user->update([
            'password' => Hash::make($password), // Example: Updating the password column
            'reset_verification_code' => NULL,
            'is_reset_password' => 0
        ]);

        return APIResponse::success([], "Password reset successfully. Please login from your mobile app again!");

    } else {
        return APIResponse::error("Invalid code / Link expired. Try again!");
    }
}

function callCurl($url)
{


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Chrome/23.0.1271.1 Safari/537.11');
    $image = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $image;
}

function checkCurlHelper($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $headers = array(
        "X-Custom-Header: value",
        "Content-Type: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    );

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36');
    $image = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $image;
}

/**
 * Determine if a given string contains all array values.
 * Not case sensitive
 *
 * @param string $haystack
 * @param string[] $needles
 * @return bool
 */
function containsInsensitive($haystack, $needles)
{
    foreach ((array)$needles as $needle) {
        if ($needle !== '' && mb_stripos($haystack, $needle) !== false) {
            return true;
        }
    }

    return false;
}


function getFormattedPhoneAttribute($phone)
{
    if (empty($phone)) {
        return '';
    }
    $ac = substr($phone, 0, 3);
    $prefix = substr($phone, 3, 3);
    $suffix = substr($phone, 6);

    return "({$ac}) {$prefix}-{$suffix}";
}

function activeCountries()
{
    $activeCountries = App\Models\Country::where('active', 1);


    $activeCountries = $activeCountries->get();

    return $activeCountries;
}

// activity logs for history purpose
function userActivityLogs($action, $function, $function_route, $status, $message)
{
    $data = [
        'action' => $action,
        'file_path' => $function_route,
        'function' => $function,
        'destination' => url()->current(),
        'status' => $status,
        'message' => $message
    ];

    $log = new Log();
    $log->user_id = Auth::user()->id;
    $log->description = json_encode($data);
    $log->module = "N/A";
    $log->ip = request()->ip();
    $log->device = request()->userAgent();
    return $log->save();
}

function callUserActivityLogs($action, $function, $file, $dir, $status = null, $message = null)
{
    $currentFilePath = $file;
    $projectFolderPath = realpath($dir . '/../../../../');

    $relativeFilePath = str_replace($projectFolderPath, '', $currentFilePath);

    userActivityLogs($action, $function, $relativeFilePath, $status, $message);
}


function dumpData()
{
    // Fetch data from the source database tables
    $tableNames = ['cameras', 'communities', 'connected_pies', 'houses', 'kiosk_permissions', 'remote_guards', 'scanners', 'users'];
    $sourceData = [];

    foreach ($tableNames as $tableName) {
        $sourceData[$tableName] = DB::connection('mysql')->table($tableName)->first();

    }

    // Dump data into the destination database tables
    $destinationTables = ['cameras', 'communities', 'connected_pies', 'houses', 'kiosk_permissions', 'remote_guards', 'scanners', 'users'];

    foreach ($destinationTables as $index => $destinationTableName) {
        foreach ($sourceData[$tableNames[$index]] as $data) {
            DB::connection('mysql_2')->table($destinationTableName)->updateOrInsert(
                ['id' => $data->id],
                (array)$data
            );
        }
    }

    return 'Data dumped successfully!';

}
