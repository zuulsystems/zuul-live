<?php

namespace App\Helpers;

trait UserCommon
{
    /**
     * Base64 Image Sanitizer
     * @param string $beginning
     * @param string $end
     * @param string $string
     *
     * @return string|string[]
     */
    public static function deleteAllBetween(string $beginning, string $end, string $string)
    {
        $beginningPos = strpos($string, $beginning);
        $endPos = strpos($string, $end);
        if ($beginningPos === false || $endPos === false) {
            return $string;
        }
        $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
        return str_replace($textToDelete, '', $string);
    }

    /**
     * Check if user can send passes
     *
     * @param $user
     *
     * @return bool
     */
    public function canSendPasses($user): bool
    {
        if (!$user) {
            return false;
        }

        if ($user->role->slug != 'family_head') {
            if ($user->hasPermission('can_send_passes')) {
                return true;
            }
            return false;
        }

        if ($user->role->slug == 'family_head') {
            return true;
        }

        return false;
    }

    /**
     * Split Full Name Into F, M and L name
     * @param $name
     *
     * @return array|bool|string
     */
    function splitName($name)
    {
        $name = $this->clean($name);
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $name));
        $names = array();
        $names['first_name'] = ucfirst($first_name);
        $names['middle_name'] = null;
        $names['last_name'] = $last_name;
        return $names;

    }

    /**
     * @param $string
     * @return string|string[]|null
     */
    public function clean($string)
    {
        return preg_replace('/[^A-Za-z0-9 ]/', '', $string); // Removes special chars.
    }
}
