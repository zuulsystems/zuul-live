<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;

class APIResponse
{
    /**
     * @param array  $data
     * @param string $msg
     *
     * @return JsonResponse
     */
    static public function success($data = [], $msg = ''): JsonResponse
    {
        return response()->json([
            'bool' => true,
            'result' => $data,
            'message' => $msg,
        ]);
    }

    /**
     * @param       $msg
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    static public function error($msg, $data = []): JsonResponse
    {
        return response()->json([
            'bool' => false,
            'message' => $msg,
            'result' => $data
        ], 422);
    }

    /**
     * @param       $msg
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    static public function error2($msg, $data = []): JsonResponse
    {
        return response()->json([
            'bool' => false,
            'is_licece_banned' => 1,
            'message' => $msg,
            'result' => $data
        ], 422);
    }
}
