<?php

namespace App\Helpers;


use App\Models\Notification;
use App\Models\SmsLog;
use App\Models\User;
use Illuminate\Support\Facades\Http;

trait PushNotificationTrait
{
    public function sendPushNotificationToUser($data, $userToReceivePush, $sendSms = false)
    {
        if (empty($userToReceivePush)) {
            return ['bool' => false, 'message' => 'No User Found'];
        } else if ($userToReceivePush->push_notification != '1'){
            return ['bool' => false, 'message' => 'User has disabled Push notification'];
        }
        $webToken = $userToReceivePush->web_token;
        $fcmToken = $userToReceivePush->fcm_token;

        $url = 'https://fcm.googleapis.com/fcm/send';
        $headerOptions = [
            'Authorization' => env('FIREBASE_AUTH_KEY'),
            'Content-Type' => 'application/json',
        ];
        $format = array(
            'to' => $webToken,
            'notification' =>
                array(
                    'body' => $data['description'],
                    'title' => $data['title'],
                    'key_id' => $data['nid'],
                    "sound" => "knock-two-times.mp3",
                    "click_action" => "MAIN_ACTIVITY"
                ),
            'data' =>
                array(
                    'body' => $data['description'],
                    'title' => $data['title'],
                    'key_id' => $data['nid'],
                    "sound" => "knock-two-times.mp3",
                    "info" => isset($data['info']) ? $data['info'] : null
                )
        );

        if (isset($data['showalert'])) {
            $format["notification"]["showalert"] = $data['showalert'];
            $format["data"]["showalert"] = $data['showalert'];
        }
        # Setup request to send json via POST.
        try {
            //Send Notification to Web
            if (!empty($webToken)) {
                $response = Http::withHeaders($headerOptions)->post($url, $format);
            }
            // Sent Notification to Mobile
            if (!empty($fcmToken)) {
                $format['to'] = $fcmToken;
                $response = Http::withHeaders($headerOptions)->post($url, $format);
            }
            //Send Sms
            if ($sendSms){
                $this->sendSms($data,$userToReceivePush);
            }
            return ['bool' => true, 'message' => 'Notification Sent Successfully'];

        } catch (\Exception $exception) {
            return ['bool' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * Sending sms.
     *
     * @param $data
     * @param $userToReceivePush
     * @return array|mixed
     * @throws \Exception
     */
    function sendSms($data,$userToReceivePush)
    {
        $phoneNumber = "";
        $smsContent = "";
        $dialCode = "";
        $notification = "";
        $smsCount  = 0;
        if (!empty($userToReceivePush) && !empty($userToReceivePush->phone_number)){
            $dialCode = $userToReceivePush->dial_code;
            $phoneNumber = $userToReceivePush->dial_code.$userToReceivePush->phone_number;
            $smsCount =  $userToReceivePush->sms_count + 1;
            $enterCode = ' ';
            //check notification type
            if( $data['title'] == 'New Pass Received' ){
                $notification = Notification::find($data['nid']);
                if (!empty($notification)){
                    $pass = $notification->pass ?? "";
                    //if sending sms third time then update message content
                    if ($smsCount == 1){
                        $smsContent = "This is your last pass to be received by text message.  To continue receiving passes you must download the ZUUL Systems app or receive by email {$pass->createdBy->fullName} has invited you on {$pass->event->name} ".route('notification-detail',[$notification->id])." Download ZUUL APP  Android: ".ZUUL_SYSTEMS_ANDROID_LINK." iOS: ".ZUUL_SYSTEMS_IOS_LINK."";
                    } else {
                        $smsContent = "{$pass->createdBy->fullName} has invited you on  {$pass->event->name}. ".route('notification-detail',[$notification->id]);

                    }

                }
            }
        }
        //check if user sending limit has reached and not registered in system
        if (empty($userToReceivePush ->password) &&$userToReceivePush->sms_count == 3){
            return [
                'bool' => false,
                'message' => "Sms sending limit has reached"
            ];
        }
        if (
            ($userToReceivePush->hasRole('guests_outsiders_daily') || $userToReceivePush->hasRole('guests_outsiders_one_time')) &&
            (empty($userToReceivePush->password))
        ) {
            //FOR US users we require two way settings
            $smsAPIkey = env('CLICK_A_TELL_2_WAY_API_KEY');
            $data['from'] = env('CLICK_A_TELL_LONG_NUMBER');
            $fromNumber = env('CLICK_A_TELL_LONG_NUMBER');
            $url = env("CLICKATELL");

            try {
                $apiData = [
                    'apiKey' => $smsAPIkey,
                    'to' => $phoneNumber,
                    'content' => $smsContent,
                    'from' => $fromNumber
                ];
                if ($dialCode != '+1') {
                    $apiData['apiKey'] = env('CLICK_A_TELL_1_WAY_API_KEY');
                    unset($apiData['from']);
                }
                $response = Http::get($url, $apiData);
                if ($response->successful()) {
                    $response = $response->throw()->json();
                    $log = array(
                        'phone_number' => $response['messages'][0]['to'],
                        'user_id' => $userToReceivePush->id,
                        'notification_id' => $notification->id ?? "",
                        'sms_content' => $smsContent,
                        'api_response' => json_encode($response['messages'][0])
                    );
                    $smsLog = SmsLog::create($log);
                    if ($smsLog) {
                        //update sms count for user
                        User::where('id', $userToReceivePush->id)->update([
                            'sms_count' => $smsCount
                        ]);
                    }
                }
                return [
                    'bool' => true,
                    'message' => 'Sms Send Successfully'
                ];

            } catch (\Exception $exception) {
                return [
                    'bool' => false,
                    'message' => $exception->getMessage()
                ];
            }
        } else {
            return [
                'bool' => false,
                'message' => 'Invalid User'
            ];
        }
    }

}
