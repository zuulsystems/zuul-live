<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportSmsLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                $notificationId = null;
                $userId = null;
                if (!empty($datum['receiver_id'])){
                    $user = \DB::table('users')->where('id',$datum['receiver_id'])->first();
                    if ($user){
                        $userId = $user->id;
                    }
                }
                if (!empty($datum['nid'])){
                    $notification = \DB::table('notifications')->where('id',$datum['nid'])->first();
                    if ($notification){
                        $notificationId = $notification->id;
                    }
                }
                DB::table('sms_logs')->insert([
                    'phone_number' => $datum['phone_number'],
                    'user_id' => $userId,
                    'notification_id' => $notificationId,
                    'api_response' => $datum['api_response'],
                    'sms_content' => $datum['sms_content'],
                    'sms_n_viewed' => $datum['sms_n_viewed'],
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                    'deleted_at' => null
                ]);
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
