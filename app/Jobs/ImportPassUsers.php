<?php

namespace App\Jobs;

use App\Models\Community;
use App\Models\Event;
use App\Models\House;
use App\Models\Pass;
use App\Models\PassRequest;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportPassUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['pass_id']) && !empty($datum['user_id'])){
                    $scanned_date = null;
                    $deleted_at = null;
                    $passRequestId = null;

                    $pass = \DB::table('passes')->where('id',$datum['pass_id'])->first();

                    $user = \DB::table('users')->where('id',$datum['user_id'])->first();

                    $passRequest = \DB::table('pass_requests')->where('id',$datum['request_id'])->first();

                    //pass request
                    if ($passRequest){
                        $passRequestId = $passRequest->id;
                    }
                    //scanned date
                    if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                        $scanned_date = $datum['deleted_at'];
                        $deleted_at = $datum['deleted_at'];
                    }

                    if(!empty($pass) && !empty($user)){
                        $data = [
                            'id' => $datum['id'],
                            'pass_id' => $pass->id,
                            'user_id' => $user->id,
                            'qr_code' => $datum['qrcode'],
                            'is_scan' => $datum['is_scanned'],
                            'scan_at' => $scanned_date,
                            'pass_request_id' => $passRequestId,
                            //'deleted_at' => $deleted_at, #todo need to review soft delete
                            'deleted_at' => ($datum['is_scanned'] == '0')?$deleted_at:null,
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ];
                        \DB::table('pass_users')->insert($data);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
