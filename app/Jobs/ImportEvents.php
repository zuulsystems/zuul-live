<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportEvents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                $user = null;
                $created_by = null;
                if(!empty($datum['created_by'])){
                    $user = \DB::table('users')->where('id',$datum['created_by'])->first();
                    if(!empty($user)){
                        $created_by = $user->id;
                    }
                }

                //deleted at
                $deleted_at = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }
                if ($datum['deleted'] == '1'){
                    $deleted_at = $datum['updated_at'];
                }

                if($datum['created_by'] == '0' || !empty($user)){
                    DB::table('events')->insert([
                        'id' => $datum['id'],
                        'name' => $datum['event_name'],
                        'description' => $datum['event_description'],
                        'created_by' => $created_by,
                        'community_id' => null,
                        'deleted_at' => $deleted_at,
                        'created_at' => $datum['created_at'],
                        'updated_at' => ($datum['updated_at'] != 'NULL' && !empty($datum['updated_at']))?$datum['updated_at']:null,
                    ]);
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
