<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TrafficTicket;
use App\Models\PassUser;

class ImportTrafficTicketEmailLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    function nullCheck($value){
        if ($value != 'NULL' && !empty($value))
            return $value;
        else
            return NULL;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            // Data Migration
            foreach ($this->data as $datum) {

                // Get the Traffic Location Real ID
                $trafficTicket = TrafficTicket::where('ticket_id', $datum['ticket_id'])->first();



                // If Ticket Exists, Add The Ticket Details
                if($trafficTicket){
                    $pass_id = PassUser::where('updated_at', "<=", $trafficTicket->time)
                        ->where('is_scan', '1')
                        ->whereHas('vehicle', function($vehicle) use ($trafficTicket) {
                            return $vehicle->where('license_plate', $trafficTicket->license_plate);
                        })->orderBy('updated_at', 'desc')->first();
                        
                    if(!empty($pass_id)){
                        $pass_id = $pass_id->pass_id;
                    }

                    $data = [
                        'to' => $datum['to'],
                        'subject' => $datum['subject'],
                        'body' => $datum['body'],
                        'ticket_id' => $trafficTicket->id,
                        'email_sent_by' => $this->nullCheck($datum['email_sent_by']),
                        'email_status' => $datum['description'],
                        'deleted_at' => null,
                        'pass_id' => $pass_id,
                        'created_at' => $datum['created_at'],
                        'updated_at' => $datum['updated_at'],
                    ];


                    // Make User Null if It Doesn't exists.
                    if (!is_null($data['email_sent_by'])){
                        $user = \DB::table('users')->where('id',$datum['email_sent_by'])->first();
                        if(!$user){
                            $data['email_sent_by'] = NULL;
                        }
                    }
                    
                    // Inset Into Table
                    \DB::table('email_logs')->insert($data);
                }
            }

            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
