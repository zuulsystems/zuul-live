<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportTrafficLocations implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {

                //deleted at
                $deleted_at = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }

                $data = [
                    'id' => $datum['id'],
                    'location_id' => $datum['location_id'],
                    'name' => $datum['name'],
                    'address' => $datum['address'],
                    'city' => $datum['city'],
                    'state' => $datum['state'],
                    'country' => $datum['country'],
                    'zip' => $datum['zip'],
                    'geocode' => $datum['geocode'],
                    'group_id' => $datum['group_id'],
                    'direction' => $datum['direction'],
                    'timezone_id' => $datum['timezone_id'],
                    'tz' => $datum['tz'],
                    'polygon' => $datum['polygon'],
                    'community_id' => $datum['community_id'],
                    'deleted_at' => $deleted_at,
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                ];

                // Only Import Community ID if Community Exists
                $community = \DB::table('communities')->where('id',$datum['community_id'])->first();
                // Else add camera to Unattached Cameras
                
                
                //create houses
                if (!$community){
                    $data['community_id'] = NULL;
                }
                \DB::table('locations')->insert($data);

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
