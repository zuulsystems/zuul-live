<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Vendor;
use App\Models\Vehicle;

class ImportVendorVehicles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    function nullCheck($value){
        if ($value != 'NULL' && !empty($value))
            return $value;
        else
            return NULL;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            // Data Migration
            foreach ($this->data as $datum) { 

                $vehicle = Vehicle::find($datum['vehicle_id']);
                $vendor = Vendor::find($datum['vendor_id']);

                if(!empty($vehicle) && !empty($vendor)){
                    $data = [
                        'id' => $datum['vehicle_id'],
                        'vehicle_id' => $this->nullCheck($datum['vehicle_id']),
                        'vendor_id' => $this->nullCheck($datum['vendor_id']),
                        'ticket_id' => $this->nullCheck($datum['ticket_id']),
                        'deleted_at' => $this->nullCheck($datum['deleted_at']),
                        'created_at' => $this->nullCheck($datum['created_at']),
                        'updated_at' => $this->nullCheck($datum['updated_at']),
                    ];

                    // Inset Into Table
                    \DB::table('vehicle_vendors')->insert($data);
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Started']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
