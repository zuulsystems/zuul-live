<?php

namespace App\Jobs;

use App\Models\Pass;
use App\Models\PassRequest;
use App\Models\PassUser;
use App\Models\Scanner;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportScanlogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                $type  = $datum['type'];
                if(!empty($datum['qrid']) ){
                    $scanned_date = null;
                    $passRequestId = null;
                    $passUser = \DB::table('pass_users')->where('id',$datum['qrid'])->first();

                    if(!empty($passUser) ){
                        $scanById = null;
                        $scannerId = null;
                        //guard
                        if (!empty($datum['scan_by'])){
                            $scanBy = \DB::table('users')->where('id',$datum['scan_by'])->first();
                            $scanById = (!empty($scanBy))?$scanBy->id:null;
                        }

                        //scanner
                        if (!empty($datum['scanner_id'])){
                            $scanner = \DB::table('scanners')->where('id',$datum['scanner_id'])->first();
                            $scannerId = (!empty($scanner))?$scanner->id:null;
                        }

                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        $data = [
                            'id' => $datum['id'],
                            'pass_user_id' => $passUser->id,
                            'scan_by' => ($type == 'guard')?$scanById:null,
                            'scanner_id' =>  ($type == 'scanner')?$scannerId:null,
                            'type' => $datum['type'],
                            'is_success' => $datum['is_success'],
                            'license_image' => $datum['licence_image'],
                            'log_text' => $datum['log_text'],
                            'deleted_at' => $deleted_at,
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ];
                        \DB::table('scan_logs')->insert($data);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
