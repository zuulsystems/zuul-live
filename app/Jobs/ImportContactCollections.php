<?php

namespace App\Jobs;

use App\Models\ContactGroup;
use App\Models\UserContact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportContactCollections implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['group_id'])&&!empty($datum['contact_id'])){

                    $contactGroup = \DB::table('contact_groups')->where('id',$datum['group_id'])->first();
                    $contactId = \DB::table('user_contacts')->where('id',$datum['contact_id'])->first();

                    //deleted at
                    $deleted_at = null;
                    if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                        $deleted_at = $datum['deleted_at'];
                    }
                    if(!empty($contactGroup)&&!empty($contactId)) {
                        DB::table('contact_collections')->insert([
                            'contact_group_id' => $datum['group_id'],
                            'user_contact_id' => $datum['contact_id'],
                            'deleted_at' => $deleted_at,
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ]);
                    }
                }

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
