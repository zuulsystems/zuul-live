<?php

namespace App\Jobs;

use App\Models\Event;
use App\Models\Pass;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportParentalControlNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if (!empty($this->data)) {
            foreach ($this->data as $datum) {
                $type = $datum['type'];
                $event_id = null;
                $pass_id = null;
                if (!empty($datum['parent_id']) && !empty($datum['member_id'])) {
                    $parent = \DB::table('users')->where('id',$datum['parent_id'])->first();
                    $member = \DB::table('users')->where('id',$datum['member_id'])->first();

                    if (!empty($datum['event_id'])) {
                        $event = \DB::table('events')->where('id',$datum['event_id'])->first();
                        $event_id = ($event) ? $event->id : null;
                    }

                    if (!empty($datum['pass_id'])) {
                        $pass = \DB::table('passes')->where('id',$datum['pass_id'])->first();
                        $pass_id = ($pass) ? $pass->id : null;
                    }

                    if (!empty($parent) && !empty($member)){
                        // notifications type
                        $category = null;
                        if ($type == '11') {
                            $category = 'pass_scan_to_user';
                        } else if ($type == '12') {
                            $category = 'pass_scan_to_guard';
                        } else if ($type == '14') {
                            $category = 'pass_scan_to_pass_owner';
                        } else if ($type == '15') {
                            $category = 'pass_scan_to_scanner';
                        } else if ($type == '13') {
                            $category = 'pass_scan_to_parent';
                        } else if ($type == '16') {
                            $category = 'new_pass_sent_from_user_to_guests';
                        } else if ($type == '17') {
                            $category = 'new_pass_sent_from_user_to_parents';
                        } else if ($type == '18') {
                            $category = 'new_pass_received_by_member_notify_parents';
                        } else if ($type == '19') {
                            $category = 'pass_updated_from_user_to_guests';
                        } else if ($type == '20') {
                            $category = 'pass_updated_from_user_to_guests_to_parents';
                        } else if ($type == '21') {
                            $category = 'pass_updated_received_by_member_notify_parents';
                        } else if ($type == '22') {
                            $category = 'pass_retracted_from_user_to_guests';
                        } else if ($type == '23') {
                            $category = 'pass_retracted_from_user_to_guests_to_parents';
                        } else if ($type == '24') {
                            $category = 'pass_retracted_received_by_member_notify_parents';
                        } else if ($type == '25') {
                            $category = 'pass_request_from_user_to_resident';
                        } else if ($type == '26') {
                            $category = 'pass_request_from_user_to_resident_to_parents';
                        } else if ($type == '27') {
                            $category = 'pass_request_received_by_member_notify_parents';
                        }

                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        $data = [
                            'parent_id' => $parent->id,
                            'user_id' => $member->id,
                            'event_id' => $event_id,
                            'pass_id' => $pass_id,
                            'heading' => $datum['heading'],
                            'subheading' => $datum['subheading'],
                            'text' => $datum['text'],
                            'link' => $datum['link'],
                            'type' => 'parental_control',
                            'is_read' => $datum['status'],
                            'category' => $category,
                            'deleted_at' => $deleted_at, #todo need to review soft delete functionality
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ];
                        \DB::table('notifications')->insert($data);
                    }
                }


            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
