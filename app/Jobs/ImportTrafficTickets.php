<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\PassUser;
use App\Models\Location;
use App\Models\VehicleVendor;



class ImportTrafficTickets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    // To check if vehicle belongs to vendor
    public function vendorCheck($license_plate){
        return !empty(VehicleVendor::join('vehicles', 'vehicles.id', 'vehicle_vendors.vehicle_id')
        ->join('vendors', 'vendors.id', 'vehicle_vendors.vendor_id')
        ->where('vehicles.license_plate', $license_plate)
        ->first());
    }

    // To check if vehicle belongs to vendor
    public function getVendorCommunity($license_plate){
        return VehicleVendor::join('vehicles', 'vehicles.id', 'vehicle_vendors.vehicle_id')
        ->join('vendors', 'vendors.id', 'vehicle_vendors.vendor_id')
        ->where('vehicles.license_plate', $license_plate)
        ->first()->community_id;
    }


    // For Null Check in CSV
    function nullCheck($value){
        if (!empty($value) && $value != 'NULL')
            return $value;
        else
            return NULL;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        if (!empty($this->data)){
            foreach ($this->data as $ticket) {

                // Get the User ID, User Role and Vehicle from Last Scanned QR Code for new ticket
                $vehicleData = PassUser::where('updated_at', "<=", $ticket['time'])
                ->where('is_scan', '1')
                ->whereHas('vehicle', function($vehicle) use ($ticket) {
                    return $vehicle->where('license_plate', $ticket['plate_text']);
                })->with('user.role')
                ->with('pass')
                ->orderBy('updated_at', 'desc')->first();

                // Set Default Type = Unmatched
                $type = "unmatched";

                // Get the Location ID
                $location = Location::where('location_id', $ticket['location_id'])->first();


                // Check If User is a Guest
                if($ticket['is_guest'] == 1){
                    $type = 'guest';
                }
                // Check if User is a Vendor
                else if($this->vendorCheck($ticket['plate_text'])){
                    $type = "vendor";
                }
                // Set the Ticket Type from User
                else if(!is_null($vehicleData) && !is_null($vehicleData->user)){
                    switch($vehicleData->user->role->slug){
                        case "family_member" :
                        case "family_head":
                            $type = 'resident';
                            break;
                        case "guests_outsiders_one_time":
                        case "guests_outsiders_daily":
                            $type = 'guest';
                            break;
                        case "vendors":
                            $type = "vendor";
                    }
                }


                if($location)
                {
                    $location = $location->id;
                } else{
                    $location = NULL;
                }

                $community = null;
                if($type == "vendor"){
                    $community =  $this->getVendorCommunity($ticket['plate_text']);
                }
                else{
                    $community =  empty($vehicleData) || empty($vehicleData->pass) ? null : $vehicleData->pass->community_id;
                }

                $user = null;
                if(!empty($ticket['resident_id'])){
                    $user = $ticket['resident_id'];
                }
                else if(!empty($vehicleData)){
                    $user = $vehicleData->user_id;
                }
                $data = [
                    'id' => $ticket['id'],
                    'ticket_id' => $ticket['ticket_id'],
                    'location_id' => $location,
                    'ticket_type' => $type,
                    'community_id' => $community,
                    'time' => $this->nullCheck($ticket['time']),
                    'data' => $this->nullCheck($ticket['data']),
                    'current_speed_limit' => $this->nullCheck($ticket['current_speed_limit']),
                    'violating_speed' => $this->nullCheck($ticket['violating_speed']),
                    'plate_image_filename' => $this->nullCheck($ticket['plate_image_filename']),
                    'license_plate' => $this->nullCheck($ticket['plate_text']),
                    'vehicle_id' => empty($vehicleData) ? null : $vehicleData->vehicle_id,
                    'locked' => $this->nullCheck($ticket['locked']),
                    'ocr_status' => $this->nullCheck($ticket['ocr_status']),
                    'edited' => $this->nullCheck($ticket['edited']),
                    'user_id' => $user,
                    'vision' => $this->nullCheck($ticket['vision']),
                    'karmen' => $this->nullCheck($ticket['karmen']),
                    'carmen_old' => $this->nullCheck($ticket['carmen_old']),
                    'carmen_general_engine' => $this->nullCheck($ticket['carmen_general_engine']),
                    'openalpr' => $this->nullCheck($ticket['openalpr']),
                    'google_v_1' => $this->nullCheck($ticket['google_v_1']),
                    'openalpr_response_plate' => $this->nullCheck($ticket['openalpr_response_plate']),
                    'openalpr_response_car' => $this->nullCheck($ticket['openalpr_response_car']),
                    'custom_info' => $this->nullCheck($ticket['custom_info']),
                    'speed_unit' => $this->nullCheck($ticket['speed_unit']),
                    'validation_status_name' => $this->nullCheck($ticket['validation_status_name']),
                    'validation_name_color' => $this->nullCheck($ticket['validation_name_color']),
                    'camera_name' => $this->nullCheck($ticket['camera_name']),
                    'hash' => $this->nullCheck($ticket['hash']),
                    'password' => $this->nullCheck($ticket['password']),
                    'static_url' => $this->nullCheck($ticket['static_url']),
                    'location_name' => $this->nullCheck($ticket['location_name']),
                    'images' => $this->nullCheck($ticket['images']),
                    'serial' => $this->nullCheck($ticket['serial']),
                    'is_paid' => $ticket['is_completed'],
                    'deleted_at' => $this->nullCheck($ticket['deleted_at']),
                    'created_at' => $ticket['created_at'],
                    'updated_at' => $this->nullCheck($ticket['updated_at']),
                ];

                \DB::table('traffic_tickets')->insert($data);
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
