<?php

namespace App\Jobs;

use App\Models\Community;
use App\Models\House;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ImportCheckGuardAdmin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)) {
            foreach ($this->data as $datum) {
                $guardId = null;
                if (!empty($datum['id'])) {
                    //get all guard from database
                    $guard = \DB::table('users')->where('role_id', 8)->where('id', $datum['id'])->first();
                    if ($guard) {
                        $guardId = $guard->id;
                    }
                }
                $data = [
                    'role_id' => 11, //guard admin
                ];
                //check if user has flag is guard admin
                if (!empty($guardId) && $datum['is_guard_admin'] == '1') {
                    \DB::table('users')->where('id', $guardId)->update($data);
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }

}
