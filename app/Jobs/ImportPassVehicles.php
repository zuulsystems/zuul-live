<?php

namespace App\Jobs;


use App\Models\PassUser;
use App\Models\Vehicle;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportPassVehicles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['qrcode_id'])){

                    $passUser = \DB::table('pass_users')->where('id',$datum['qrcode_id'])->first();
                    $vehicle = \DB::table('vehicles')->where('id',$datum['vehicle_id'])->first();

                    if(!empty($passUser) && !empty($vehicle)){
                        $data = [
                            'vehicle_id' => $vehicle->id,
                        ];
                        \DB::table('pass_users')->where('id',$passUser->id)->update($data);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
