<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Community;
use App\Models\User;

class ImportVendors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    function nullCheck($value){
        if ($value != 'NULL' && !empty($value))
            return $value;
        else
            return NULL;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            // Data Migration
            foreach ($this->data as $datum) {

               
                    $data = [
                        'id' => $datum['id'],
                        'vendor_name' => $this->nullCheck($datum['vendor_name']),
                        'email' => $this->nullCheck($datum['email']),
                        'alt_email' => $this->nullCheck($datum['alt_email']),
                        'phone' => $this->nullCheck($datum['phone']),
                        'alt_phone' => $this->nullCheck($datum['alt_phone']),
                        'dial_code' => $this->nullCheck($datum['dial_code']),
                        'place_name' => $this->nullCheck($datum['place_name']),
                        'address' => $this->nullCheck($datum['address']),
                        'city' => $this->nullCheck($datum['city']),
                        'zip' => $this->nullCheck($datum['zip']),
                        'state' => $this->nullCheck($datum['state']),
                        'country' => $this->nullCheck($datum['country']),
                        'description' => $this->nullCheck($datum['description']),
                        'licence_number' => $this->nullCheck($datum['licence_number']),
                        'user_id' => $this->nullCheck($datum['user_id']),
                        'community_id' => $this->nullCheck($datum['community_id']),
                        'created_by'=> $this->nullCheck($datum['created_by']),
                        'is_active' => $datum['is_active'],
                        'deleted_at' => $this->nullCheck($datum['deleted_at']),
                        'created_at' => $this->nullCheck($datum['created_at']),
                        'updated_at' => $this->nullCheck($datum['updated_at']),
                    ];

                    //Check if Community and User Exists
                    if(empty(Community::find($data['community_id']))){
                        $data['community_id'] = NULL;
                    }

                    if(empty(User::find($data['user_id']))){
                        $data['user_id'] = NULL;
                    }

                    if(empty(User::find($data['created_by']))){
                        $data['created_by'] = NULL;
                    }

                    // Inset Into Table
                    \DB::table('vendors')->insert($data);

            }

            return response()->json(['status' => true, 'message' => 'Job Started']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
