<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportUserProfile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                //update  users profile
                if (!empty($datum['user_id'])){
                    //$user = User::find($datum['user_id']);
                    $user = \DB::table('users')->where('id',$datum['user_id'])->first();
                    if ($user){
                        $license_type = null;
                        $license_format = $datum['licence_format'];
                        if ($license_format == 'dl'){
                            $license_type =  'driving_license';
                        } else if ($license_format == 'si'){
                            $license_type =  'student_id';
                        } else if ($license_format == 'ps'){
                            $license_type =  'passport';
                        } else if ($license_format == 'mi'){
                            $license_type =  'military_id';
                        }
                        $data = [
                            'middle_name' => $datum['middle_name'],
                            'last_name' => $datum['last_name'],
                            'date_of_birth' => $datum['date_of_birth'],
                            'street_address' => $datum['street_address'],
                            'apartment' => $datum['apartment'],
                            'city' => $datum['city'],
                            'state' => $datum['state'],
                            'zip_code' => $datum['zip_code'],
                            'license_type' => $license_type,
                            'license_image' => $datum['licence_image'],
                            'mailing_address' => $datum['mailing_address'],
                        ];
                        \DB::table('users')->where('id',$user->id)->update($data);
                    }
                }

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
