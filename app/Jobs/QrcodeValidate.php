<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ScanLog;
use Kreait\Firebase\Factory;
use Illuminate\Support\Facades\Log;

class QrcodeValidate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
                $factory_new = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
                $realtimeDatabase_new = $factory_new->createDatabase();

                $reference = $realtimeDatabase_new->getReference($this->data['pi_address'].'/'.$this->data['mac_address']);
                if (!array_key_exists("ImageURL", $reference->getValue()))
                {
                }else{
                    ScanLog::where('id', $this->data['id'])->update(
                    [
                        'license_plate_image' => $reference->getValue()['ImageURL'],
                        'log_text' => 'QRCode Verified',
                        'is_success' => '1'
                    ]);
                }

        }

        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
