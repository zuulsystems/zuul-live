<?php

namespace App\Jobs;

use App\Models\Pass;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportZuulNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if (!empty($this->data)) {
            foreach ($this->data as $datum) {
                $type = $datum['type'];
                $user_id = null;
                $pass_id = null;
                if (!empty($datum['user_id'])) {
                    $user = \DB::table('users')->where('id',$datum['user_id'])->first();
                    $user_id = ($user) ? $user->id : null;
                }
                // status
                $category = null;
                $announcement_id = null;
                $pass_request_id = null;
                $pass_id = null;
                $traffic_ticket_id = null;
                $table = null;
                if ($type == '3') {
                    $category = 'unread_announcement';
                    if (!empty($datum['pr_id'])) {
                        $announcement = \DB::table('announcements')->where('id',$datum['pr_id'])->first();
                        $announcement_id = ($announcement) ? $announcement->id : null;
                    }
                } else if ($type == '1') {
                    $category = 'send_pass';
                    if (!empty($datum['pr_id'])) {
                        $pass = \DB::table('passes')->where('id',$datum['pr_id'])->first();
                        $pass_id = ($pass) ? $pass->id : null;
                    }
                } else if ($type == '0') {
                    $category = 'send_request_for_pass';
                    if (!empty($datum['pr_id'])) {
                        $pass_request = \DB::table('pass_requests')->where('id',$datum['pr_id'])->first();
                        $pass_request_id = ($pass_request) ? $pass_request->id : null;
                    }
                } else if ($type == '5') {
                    $category = 'send_email_to_head_of_family';
                    if (!empty($datum['pr_id'])) {
                        $pass = \DB::table('passes')->where('id',$datum['pr_id'])->first();
                        $pass_id = ($pass) ? $pass->id : null;
                    }
                } else if ($type == '8') {
                    $category = 'ticket';
                    if (!empty($datum['pr_id'])) {
                        $traffic_ticket = \DB::table('traffic_tickets')->where('id',$datum['pr_id'])->first();
                        $traffic_ticket_id = ($traffic_ticket) ? $traffic_ticket->id : null;
                    }
                }


                //deleted at
                $deleted_at = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }
                if (!empty($user_id)){
                    $data = [
                        'user_id' => $user_id,
                        'pass_id' => $pass_id,
                        'pass_request_id' => $pass_request_id,
                        'announcement_id' => $announcement_id,
                        'traffic_ticket_id' => $traffic_ticket_id,
                        'heading' => $datum['heading'],
                        'subheading' => $datum['subheading'],
                        'text' => $datum['text'],
                        'link' => $datum['link'],
                        'type' => 'zuul',
                        'is_read' => $datum['status'],
                        'category' => $category,
                        'deleted_at' => $deleted_at, #todo need to review soft delete functionality
                        'created_at' => $datum['created_at'],
                        'updated_at' => $datum['updated_at'],
                    ];
                    \DB::table('notifications')->insert($data);
                }

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
