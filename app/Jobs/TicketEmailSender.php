<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\EmailLog;

class TicketEmailSender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        extract($this->data);
        ticketEmail($email, $template, $title);

        EmailLog::create([
            'to' => $email,
            'subject' => $title,
            'body' => $template,
            'ticket_id' => $ticket_id,
            'email_sent_by' => $user_id,
            'email_status' => 'Email sent successfully',
        ]);
        
    }


}
