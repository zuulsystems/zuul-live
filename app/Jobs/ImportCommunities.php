<?php

namespace App\Jobs;

use App\Models\Community;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportCommunities implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                $community_right = $datum['community_right'];
                if ( $datum['community_right'] == 'tl'){
                    $community_right =  "traffic_logics";
                }

                //deleted at
                $deleted_at = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }
                //create communities
                \DB::table('communities')->insert([
                    'id' => (int)$datum['id'],
                    'name' => $datum['name'],
                    'address' => $datum['address'],
                    'lat' => $datum['latitude'],
                    'lng' => $datum['longitude'],
                    'emergency_contact_number' => $datum['emergency_contact_information'],
                    'contact_number' => $datum['contact_information'],
                    'community_logo' => $datum['community_picture'],
                    'timezone_id' => $datum['timezone_id'],
                    'community_right' => $community_right,
                    'company_id' => $datum['company_id'],
                    'rfid_site_code' => $datum['rfid_site_code'],
                    'key' => $datum['key'],
                    'deleted_at' => $deleted_at,
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                ]);

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
