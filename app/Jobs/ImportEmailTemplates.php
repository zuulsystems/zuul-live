<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportEmailTemplates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['created_by'])){
                    $user = \DB::table('users')->where('id',$datum['created_by'])->first();
                    if(empty($user)){
                        $datum['created_by'] = null;
                    }
                }
                DB::table('email_templates')->insert([
                    'name' => $datum['name'],
                    'template' => $datum['template'],
                    'template_code' => $datum['template_code'],
                    'assign_to' => $datum['assign_to'],
                    'created_by' => $datum['created_by'],
                    'email_type' => 'zuul',
                    'deleted_at' => null,
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                ]);
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
