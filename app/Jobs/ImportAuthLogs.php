<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportAuthLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['authenticatable_id'])){
                    $user = User::find($datum['authenticatable_id']);
                    if(!empty($user)){
                        DB::table('auth_logs')->insert([
                            'ip_address' => $datum['ip_address'],
                            'user_agent' => $datum['user_agent'],
                            'user_id' => $datum['authenticatable_id'],
                            'login_at' => $datum['login_at'],
                            'logout_at' => $datum['logout_at'],
                            'deleted_at' => null
                        ]);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
