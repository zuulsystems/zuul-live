<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class ImportContactGroups implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {

                if(!empty($datum['user_id']))
                {
                    $user = \DB::table('users')->where('id',$datum['user_id'])->first();

                    if(!empty($user))
                    {
                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        DB::table('contact_groups')->insert([
                            'id' => $datum['id'],
                            'created_by' => $datum['user_id'], // Todo user table column id,
                            'group_name' => $datum['group_name'],
                            'description' => $datum['group_description'],
                            'deleted_at' => $deleted_at,
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ]);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
