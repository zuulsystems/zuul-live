<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportParentalControls implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {

                if(!empty($datum['parent_id'])&&!empty($datum['member_id'])){
                    $parentId = \DB::table('users')->where('id',$datum['parent_id'])->first();
                    $memberId = \DB::table('users')->where('id',$datum['member_id'])->first();

                    if(!empty($parentId)&&!empty($memberId)){
                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        DB::table('parental_controls')->insert([
                            'parent_id' => $datum['parent_id'],
                            'member_id' => $datum['member_id'],
                            'notify_when_member_guest_arrive' => $datum['notify_when_member_guest_arrive'],
                            'notify_when_member_send_pass_to_others' => $datum['notify_when_member_send_pass_to_others'],
                            'notify_when_member_receive_a_pass' => $datum['notify_when_member_receive_a_pass'],
                            'notify_when_member_retract_a_pass' => $datum['notify_when_member_retract_a_pass'],
                            'notify_when_member_update_a_pass' => $datum['notify_when_member_update_a_pass'],
                            'notify_when_pass_retracted_included_member' => $datum['notify_when_pass_retracted_included_member'],
                            'notify_when_pass_updated_included_member' => $datum['notify_when_pass_updated_included_member'],
                            'notify_when_member_request_a_pass_with_offensive_words' => $datum['notify_when_member_request_a_pass_with_offensive_words'],
                            'blacklist_member_to_receive_request_a_pass' => $datum['blacklist_member_to_receive_request_a_pass'],
                            'allow_parental_controls_to_member' => $datum['allow_parental_controls_to_member'],
                            'notify_when_member_request_a_pass' => $datum['notify_when_member_request_a_pass'],
                            'deleted_at' => $deleted_at
                        ]);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
