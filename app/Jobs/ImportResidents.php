<?php

namespace App\Jobs;

use App\Models\Community;
use App\Models\House;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ImportResidents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {

                $communityId = null;
                $houseId = null;
                $createdById = null;
                if (!empty($datum['community'])){
                    $community = \DB::table('communities')->where('id',$datum['community'])->first();

                    if ($community){
                        $communityId = $community->id;
                    }
                }
                if (!empty($datum['house'])){
                    $house = \DB::table('houses')->where('id',$datum['house'])->first();

                    if ($house){
                        $houseId = $house->id;
                    }
                }
                if (!empty($datum['created_by'])){
                    $createdBy = \DB::table('users')->where('id',$datum['created_by'])->first();
                    if ($createdBy){
                        $createdById = $createdBy->id;
                    }
                }

                //deleted at
                $deleted_at = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }

                //deleted at
                $phone_number = null;
                if ($datum['phone_number'] != 'NULL' && !empty($datum['phone_number'])){
                    $phone_number = $datum['phone_number'];
                }

                $data = [
                    'id' => $datum['id'],
                    'first_name' => $datum['name'],
                    'email' => $datum['email'],
                    'phone_number' => $phone_number,
                    'password' => $datum['password'],
                    'temp_password' => $datum['temporary_password'],
                    'profile_image' => str_replace("profile/","",$datum['profile_image']),
                    'community_id' => $communityId,
                    'house_id' => $houseId,
                    'fcm_token' => $datum['fcm_token'],
                    'web_token' => $datum['web_token'],
                    'created_by' => $createdById,
                    'verification_code' => $datum['is_reset_password'] == '1'? $datum['guard_verification_code']: $datum['verification_code'],
                    'email_verification_code' => $datum['email_verification_code'],
                    'email_owner_count' => $datum['email_owner_count'],
                    'is_email_verified' => $datum['is_email_verified'],
                    'dial_code' => $datum['dial_code'],
                    'push_notification' => $datum['push_notification'],
                    'sms_notification' => $datum['sms_notification'],
                    'email_notification' => $datum['email_notification'],
                    'special_instruction' => $datum['special_instructions'],
                    'is_two_factor' => $datum['is_two_factor'],
                    'vacation_mode' => $datum['vacation_mode'],
                    'is_suspended' => $datum['suspand'],
                    'is_reset_password' => $datum['is_reset_password'],
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                    'deleted_at' => $deleted_at, #todo need to review soft delete

                ];
                //create users
                if ($datum['id'] != '1'){
                    \DB::table('users')->insert($data);
                    $user =  \DB::table('users')->where('id',$datum['id'])->first();
                    if ($user){
                            //assign Permissions
                            if ($datum['can_send_passes']){
                                $this->assignPermission('can_send_passes',$datum['id']);
                            }
                            if ($datum['can_manage_family']){
                                $this->assignPermission('can_manage_family',$datum['id']);
                            }
                            if ($datum['allow_parental_control']){
                                $this->assignPermission('allow_parental_control',$datum['id']);

                            }
                            if ($datum['can_retract_sent_passes']){
                                $this->assignPermission('can_retract_sent_passes',$datum['id']);
                            }
                            if ($datum['is_licence_locked']){
                                $this->assignPermission('is_license_locked',$datum['id']);
                            }
                            if ($datum['can_check_license_plate']){
                                $this->assignPermission('can_check_license_plate',$datum['id']);
                            }

                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }

    private function assignPermission($permission,$userId){
        $permission =  \DB::table('permissions')->where('slug',$permission)->first();
        if ($permission){
            $hasPermissions  = \DB::table('user_permissions')->where('user_id',$userId)->where('permission_id',$permission->id)->first();
            if (empty($hasPermissions)){
                $assignPermission =  \DB::table('user_permissions')->insert([
                    'user_id' => $userId,
                    'permission_id' => $permission->id
                ]);
            }
        }

    }
}
