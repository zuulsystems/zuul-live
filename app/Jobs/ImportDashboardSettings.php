<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportDashboardSettings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['user_id'])){
                    $user = \DB::table('users')->where('id',$datum['user_id'])->first();
                    if(!empty($user)){
                        //deleted at
                        $deleted_at = null;
                        $icon = null;
                        $url = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        if ($datum['widget_name'] == 'guards'){
                            $icon = 'fa fa-user';
                            $url = route('admin.user.guards.index');
                        } else if (
                            $datum['widget_name'] =='household_resident' ||
                            $datum['widget_name'] =='guest' ||
                            $datum['widget_name'] =='community_admin' ||
                            $datum['widget_name'] =='communities'
                        ){
                            //set url according to module
                            if ($datum['widget_name']  == 'household_resident'){
                                $url = route('admin.residential.residents.index');
                            }
                            if ($datum['widget_name']  == 'guest'){
                                $url = route('admin.user.guests.index');
                            }
                            if ($datum['widget_name']  == 'community_admin'){
                                $url = route('admin.user.community-admins.index');
                            }
                            if ($datum['widget_name']  == 'communities'){
                                $url = route('admin.residential.communities.index');
                            }
                            $icon = 'fa fa-users';
                        } else if ($datum['widget_name'] == 'houses'){
                            $icon = 'fa fa-home';
                            $url = route('admin.residential.houses.index');
                        } else if ($datum['widget_name'] == 'announcements'){
                            $icon = 'fa fa-bullhorn';
                            $url = route('admin.residential.announcements.index');
                        } else if ($datum['widget_name'] == 'rfid_tags'){
                            $icon = 'fa fa-tag';
                            $url = route('admin.rfid.rfids.index');
                        } else if ($datum['widget_name'] == 'vendors'){
                            $icon = 'fa fa-truck';
                            $url = route('admin.vendors.index');
                        } else if ($datum['widget_name'] == 'vehicles'){
                            $icon = 'fa fa-car';
                            $url = route('admin.vehicles.index');
                        } else if ($datum['widget_name'] == 'passes'){
                            $icon = 'fa fa-ticket-alt';
                            $url = route('admin.passes.index');
                        } else if ($datum['widget_name'] == 'guest_log'){
                            $icon = 'fa fa-history';
                            $url = route('admin.guest-logs.index');
                        } else if ($datum['widget_name'] == 'camera_locations'){
                            $icon = 'fa fa-camera';
                            $url = route('admin.locations.index');
                        } else if ($datum['widget_name'] == 'rfid_tracking'){
                            $icon = 'fa fa-map-marker';
                            $url = route('admin.rfid.rfid-logs.index');
                        }

                        DB::table('dashboard_settings')->insert([
                            'widget_name' => $datum['widget_name'],
                            'is_allowed' => $datum['is_allowed'],
                            'user_id' => $datum['user_id'],
                            'color' => $datum['color'],
                            'url' => $url,
                            'icon' => $icon,
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                            'deleted_at' => $deleted_at,
                        ]);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
