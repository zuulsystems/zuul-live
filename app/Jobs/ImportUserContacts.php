<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;

class ImportUserContacts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {


                $contact = \DB::table('contacts')->where('dial_code',$datum['dial_code'])->where('phone_number',$datum['phone_number'])->first();

                if(!empty($datum['created_by']))
                {
                    $user = \DB::table('users')->where('id',$datum['created_by'])->first();

                    if(!empty($contact) && !empty($user))
                    {

                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        DB::table('user_contacts')->insert([
                            'id' => $datum['id'],
                            'created_by' => $datum['created_by'],
                            'contact_id' => $contact->id,
                            'contact_name' => $datum['display_name'],
                            'email' => $datum['email'],
                            'is_favourite' => $datum['is_favourite'],
                            'relationship' => $datum['relationship'],
                            'is_temporary' => $datum['is_temporary'],
                            'temporary_duration' => $datum['temporary_duration'],
                            'expire_date' => $datum['expiry_date'],
                            'deleted_at' => $deleted_at,
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ]);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
