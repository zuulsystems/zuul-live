<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportVehicles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
            $deleted_at = null;
            $user_id = null;

            // Deleted At
            if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                $deleted_at = $datum['deleted_at'];
            }

            // User ID
            if ($datum['user_id'] != 'NULL' && !empty($datum['user_id'])){
                $user_id = $datum['user_id'];
                $user_id = empty(User::find($datum['user_id'])) ? null : $user_id;
            }

            DB::table('vehicles')->insert([
                'id' => $datum['id'],
                'make' => $datum['make'],
                'model' => $datum['model'],
                'year' => $datum['year'],
                'color' => $datum['color'],
                'license_plate' => $datum['licence_plate'],
                'image' => $datum['image'],
                'user_id' => $user_id,
                'community_id' => null,
                'deleted_at' => $deleted_at,
                'created_at' => $datum['created_at'],
                'updated_at' => $datum['updated_at'],
            ]);


            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
