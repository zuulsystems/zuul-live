<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TrafficTicket;

class ImportTrafficTicketPaymentLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {

                // Get the data
                $data = [
                    'status' => $datum['status'],
                    'date' => $datum['date'],
                    'ticket_id' => $datum['ticket_id'],
                    'created_by' => $datum['user_id'],
                    'payee_user_id' => $datum['payee_user_id'],
                    'deleted_at' => null,
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                ];



                // Only Import Template if Payee, User and Ticket Exists
                 $user = \DB::table('users')->where('id',$datum['user_id'])->first();
                 $payee = \DB::table('users')->where('id',$datum['payee_user_id'])->first();
                 $ticket = TrafficTicket::find($datum['ticket_id']);

                 if (($payee && $user) && $ticket){
                     \DB::table('traffic_payment_logs')->insert($data);
                 }

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
