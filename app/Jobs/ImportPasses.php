<?php

namespace App\Jobs;

use App\Models\Community;
use App\Models\Event;
use App\Models\House;
use App\Models\Pass;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportPasses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['created_by']) && !empty($datum['community_id']) && !empty($datum['house_id'])){

                    $createdBy = \DB::table('users')->where('id',$datum['created_by'])->first();
                    $community = \DB::table('communities')->where('id',$datum['community_id'])->first();
                    $house = \DB::table('houses')->where('id',$datum['house_id'])->first();

                    if(!empty($createdBy) && !empty($community) && !empty($house)){
                        $pass_type_import =  $datum['pass_type'];
                        $visitor_type_import =  $datum['visitor_type'];
                        $event_id = null;
                        $pass_type = null;
                        $visitor_type = null;
                        //pass type
                        if ($pass_type_import == '1'){
                            $pass_type = 'one';
                        } else if ($pass_type_import == '2'){
                            $pass_type = 'recurring';
                        } else if ($pass_type_import == '3'){
                            $pass_type = 'self';
                        }
                        //visitor type
                        if ($visitor_type_import == '1'){
                            $visitor_type = 'friends_family';
                        } else if ($visitor_type_import == '2'){
                            $visitor_type = 'vendor_delivery';
                        }
                        //pass event
                        if (!empty($datum['event_id'])){
                            $event = \DB::table('events')->where('id',$datum['event_id'])->first();
                            $event_id =  ($event)?$event->id:null;
                        }

                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        $data = [
                            'id' => $datum['id'],
                            'pass_validity' => $datum['pass_validity'],
                            'pass_type' => $pass_type,
                            'visitor_type' => $visitor_type,
                            'description' => $datum['description'],
                            'event_id' => $event_id,
                            'created_by' => $createdBy->id,
                            'community_id' => $community->id,
                            'house_id' => $house->id,
                            'is_quick_pass' => $datum['quickpass'],
                            'pass_start_date' => $datum['pass_start_date'],
                            'pass_date' => $datum['pass_date'],
                            'lat' => $datum['latitude'],
                            'lng' => $datum['longitude'],
                            'deleted_at' => $deleted_at, #todo need to review soft delete
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ];

                        \DB::table('passes')->insert($data);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
