<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportTrafficEmailTemplates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {

                // Get the data
                $data = [
                    'name' => $datum['name'],
                    'template' => $datum['template'],
                    'template_code' => $datum['template_code'],
                    'assign_to' => $datum['assign_to'],
                    'created_by' => $datum['created_by'],
                    'community_id' => $datum['community_id'],
                    'email_type' => 'trafficlogix',
                    'deleted_at' => null,
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                ];                    
                 
                // Only Import Template if Community and User Exists
                 $user = \DB::table('users')->where('id',$datum['created_by'])->first();
                 $community = \DB::table('communities')->where('id',$datum['community_id'])->first();
                 if ($community && $user){
                     \DB::table('email_templates')->insert($data);
                 }        

            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);
    }
}
