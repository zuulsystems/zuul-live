<?php

namespace App\Jobs;

use App\Models\Announcement;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportAnnouncements implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                //deleted at
                $deleted_at = null;
                $community_id = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }

                if (!empty($datum['community'])) {
                    $community = \DB::table('communities')->where('id',$datum['community'])->first();
                    $community_id = ($community) ? $community->id : null;
                }
                DB::table('announcements')->insert([
                    'id' => $datum['id'],
                    'title' => $datum['title'],
                    'description' => $datum['description'],
                    'created_by' => 1,
                    'community_id' => $community_id,
                    'is_must_read' => $datum['mustread'],
                    'deleted_at' => $deleted_at,
                    'created_at' => $datum['created_at'],
                    'updated_at' => $datum['updated_at'],
                ]);
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
