<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportPassRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['requested_user_id']) && !empty($datum['sent_user_id'])){
                    $requestedUser = \DB::table('users')->where('id',$datum['requested_user_id'])->first();
                    $sentUser = \DB::table('users')->where('id',$datum['sent_user_id'])->first();

                    $expire_at = null;
                    if(!empty($requestedUser) && !empty($sentUser)){
                        // status
                        $status = null;
                        if($datum['is_rejected'] == '-1'){
                            $status = 'rejected';
                        } else if($datum['is_rejected'] == '0'){
                            $status = 'pending';
                        } else if($datum['is_rejected'] == '1'){
                            $status = 'approved';
                        }

                        //expire date
                        if ($datum['expire_at'] != 'NULL' && !empty($datum['expire_at'])){
                            $expire_at = $datum['expire_at'];
                        }

                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        $data = [
                            'id' => $datum['id'],
                            'request_user_id' => $requestedUser->id,
                            'send_user_id' => $sentUser->id,
                            'name' => $datum['display_name'],
                            'email' => $datum['display_email'],
                            'description' => $datum['description'],
                            'phone' => $datum['phone_number'],
                            'anonymous' => $datum['anonymous'],
                            'status' => $status,
                            'expire_at' => $expire_at,
                            'deleted_at' => $deleted_at, #todo need to review soft delete functionality
                            'created_at' => $datum['created_at'],
                            'updated_at' => $datum['updated_at'],
                        ];
                        \DB::table('pass_requests')->insert($data);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
