<?php

namespace App\Jobs;

use App\Models\DeviceType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportScanners implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            foreach ($this->data as $datum) {
                if(!empty($datum['device_type'])){
                    $deviceType = \DB::table('device_types')->where('id',$datum['device_type'])->first();
                    $community = \DB::table('communities')->where('id',$datum['community'])->first();
                    if(!empty($deviceType) && !empty($community)){

                        //deleted at
                        $deleted_at = null;
                        if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                            $deleted_at = $datum['deleted_at'];
                        }
                        DB::table('scanners')->insert([
                            'id' => $datum['id'],
                            'name' => $datum['name'],
                            'mac_address' => $datum['mac_address'],
                            'community_id' => $datum['community'],
                            'enable' => $datum['enable'],
                            'device_type_id' => $datum['device_type'],
                            'secret_key' => $datum['secret_key'],
                            'rfid_multiplier' => $datum['rfid_multiplier'],
                            'access_token' => $datum['access_token'],
                            'last_event' => $datum['last_event'],
                            'last_event_at' => $datum['last_event_at'],
                            'online_status' => $datum['online_status'],
                            'online_at' => $datum['online_at'],
                            'deleted_at' => $deleted_at,
                        ]);
                    }
                }
            }
            return response()->json(['status' => true, 'message' => 'Job Completed']);
        }
        return response()->json(['status' => false, 'message' => 'No Records']);

    }
}
