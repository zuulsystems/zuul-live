<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TrafficTicket;

class ImportTrafficTicketImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->data)){
            // Data Migration
            foreach ($this->data as $datum) {

                //deleted at check
                $deleted_at = null;
                if ($datum['deleted_at'] != 'NULL' && !empty($datum['deleted_at'])){
                    $deleted_at = $datum['deleted_at'];
                }

                // Get the Traffic Location Real ID
                $trafficTicket = TrafficTicket::where('ticket_id', $datum['ticket_id'])->first();

                // If Ticket Exists, Add The Ticket Images
                if($trafficTicket){
                    $data = [
                        'ticket_id' => $trafficTicket->id,
                        'image' => str_replace('zip/', '', $datum['image']),
                        'deleted_at' => $deleted_at,
                        'created_at' => $datum['created_at'],
                        'updated_at' => $datum['updated_at'],
                    ];
                }

                \DB::table('traffic_ticket_images')->insert($data);

            }

        }
    }
}
