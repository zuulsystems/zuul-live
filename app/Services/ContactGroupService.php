<?php

namespace App\Services;

use App\Models\Contact;
use App\Repositories\ContactGroupRepository;

class ContactGroupService extends ContactGroupRepository
{

    /**
     * get contacts details
     *
     * @param $userId
     * @param $limit
     * @param $search
     *
     * @return mixed
     */
    public function getContactGroupList($userId, $limit = null, $search = null)
    {

        $result = parent::getByUserId($userId, $limit, $search)['result'];

        return $result->map(function ($row, $key) {
            return [
                'id' => $row->id,
                'group_name' => $row->group_name,
                'description' => $row->description,
                'user_contact' => $row->userContacts->map(function ($userContact, $key) {
                    return [
                        'id' => $userContact->id,
                        'contact_name' => $userContact->contact_name,
                        'formatted_phone_number' => ($userContact->contact()->exists()) ? $userContact->contact->formattedPhone : "",
                    ];
                }),
                'total_contact'=>$row->userContacts->count()
            ];
        });
    }

    /**
     * create user contact
     *
     * @param array $data
     *
     * @return mixed|void
     */
    public function createContactGroup($data)
    {
        try {
            //create contact group
            $group = self::create([
                'created_by' => auth()->id(),
                'group_name' => $data['group_name'],
                'description' => $data['description'],
            ]);

            return $group;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * add contact in group
     *
     * @param $contactId
     * @param $groupId
     *
     * @return bool
     * @throws \Exception
     */
    public function addContactInGroup($contactId, $groupId)
    {
        try {
            $group = self::find($groupId)['result'];
            if ($group) {
                 $group->userContacts()->sync([$contactId], false);
            }
            $group =  self::find($groupId);
            return $group;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @param $contactGroupId
     * @param $contactId
     *
     * @return bool
     */
    public function groupContactDelete($contactGroupId, $contactId)
    {
        try {
            $contactGroup = $this->model->find($contactGroupId);
            $contactGroup->userContacts()->detach($contactId);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
