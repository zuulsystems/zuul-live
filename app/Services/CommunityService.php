<?php

namespace App\Services;

use App\Models\Community;
use App\Models\UserPermission;
use App\Repositories\CommunityRepository;
use App\Repositories\DeactivateResidentRepository;
use App\Repositories\UserRepository;

class CommunityService extends CommunityRepository
{


    private $userRepo;
    private $deactivateResidentRepo;

    /**
     * PassService constructor.
     *
     * @param Community $community
     * @param UserRepository $userRepo
     * @param DeactivateResidentRepository $deactivateResidentRepo
     */
    public function __construct(
        Community $community,
        UserRepository $userRepo,
        DeactivateResidentRepository $deactivateResidentRepo
    )
    {
        parent::__construct($community);
        $this->userRepo = $userRepo;
        $this->deactivateResidentRepo = $deactivateResidentRepo;
    }

    /**
     * deactivate community
     * @param $communityId
     * @return array
     * @throws \Exception
     */
    public function deactivateCommunity($communityId)
    {
        $community = self::find($communityId)['result'];
        if (!empty($community)) {
            if ($community->houses->isNotEmpty()) {
                $houseIds = $community->houses->pluck('id');
                $residents = $this->userRepo->getAllResidentsByHouseIds($community->id, $houseIds)['result'];

                \DB::beginTransaction();
                try {
                    if ($residents->isNotEmpty()) {
                        $residentIds = $residents->pluck('id');
                        $deactivateResidentData = [];
                        foreach ($residents as $resident) {
                            $permissionIds = null;
                            if ($resident->permissions->isNotEmpty()) {
                                $permissionIds = implode(',', $resident->permissions->pluck('id')->toArray());
                            }
                            $deactivateResidentData[] = array(
                                'user_id' => $resident->id,
                                'role_id' => $resident->role_id,
                                'house_id' => $resident->house_id,
                                'community_id' => $resident->community_id,
                                'permission_ids' => $permissionIds,
                            );
                        }
                        //update residents move to guests
                        if (!empty($deactivateResidentData)) {
                            $this->userRepo->updateResidentToGuestWithOutTryCatch($residentIds);
                            $this->deactivateResidentRepo->directInsertWithOutTryCatch($deactivateResidentData);
                        }

                    }
                    //deactivate community admin and guard
                    $this->userRepo->deactivateCommunityAdminAndGuardWithOutTryCatch($communityId);

                    self::updateWithOutTryCatch([
                        'community_status' => 'inactive'
                    ], $community->id);

                    \DB::commit();
                    return ['status' => true, 'message' => 'Deactivated Successfully.'];

                } catch (\Exception $exception) {
                    \DB::rollback();
                    // return ['status' => false,'message'=> $exception->getMessage()." ".$exception->getLine()." ".$exception->getFile()];
                    return ['status' => false, 'message' => 'Something went wrong.Please try again'];
                }

            } else {
                return ['status' => false, 'message' => 'Community has No Houses'];
            }
        } else {
            return ['status' => false, 'message' => 'Something went wrong.Please try again.'];
        }
    }
    /**
     * activate community
     * @param $communityId
     * @return array
     * @throws \Exception
     */
    public function activateCommunity($communityId)
    {
        $community = self::find($communityId)['result'];
        if (!empty($community)) {
            \DB::beginTransaction();
            if ($community->houses->isNotEmpty()) {
                try {
                    //deactivate residents
                    $deactivateResidents = $this->deactivateResidentRepo->getAllByCommunity($community->id)['result'];
                    if ($deactivateResidents->isNotEmpty()) {
                        $guestIds = $deactivateResidents->pluck('id');
                        $permissions = [];
                        foreach ($deactivateResidents as $deactivateResident) {
                            $deactivateResidentPermissions = null;
                            if (!empty($deactivateResident->permission_ids)) {
                                //get residents permissions before deactivated
                                foreach (explode(',', $deactivateResident->permission_ids) as $permission_id) {
                                    $permissions[] = array(
                                        'user_id' => $deactivateResident->user_id,
                                        'permission_id' => $permission_id,
                                    );
                                }
                            }
                            //move guest to residents which was deactivated
                            $this->userRepo->updateWithOutTryCatch([
                                'role_id' => $deactivateResident->role_id,
                                'house_id' => $deactivateResident->house_id,
                                'community_id' => $deactivateResident->community_id,
                            ], $deactivateResident->user_id);

                        }
                        //assign permissions to residents before deactivated
                        UserPermission::insert($permissions);

                        //delete deactivate resident records
                        $this->deactivateResidentRepo->bulkDeleteWithOutTryCatch($guestIds);

                    }

                    //activate community admin and guard
                    $this->userRepo->activateCommunityAdminAndGuardWithOutTryCatch($communityId);
                    //update community status to activate
                    self::updateWithOutTryCatch([
                        'community_status' => 'active'
                    ], $communityId);
                    \DB::commit();
                    return ['status' => true, 'message' => 'Community has been Activated'];

                } catch (\Exception $exception) {
                    \DB::rollback();
                    return ['status' => false, 'message' => 'Something went wrong.Please try again'];
                }

            } else {
                return ['status' => false, 'message' =>  'Community has No Houses'];
            }
        } else {
            return ['status' => false, 'message' =>  'Something went wrong.Please try again.'];
        }
    }
}
