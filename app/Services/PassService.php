<?php

namespace App\Services;

use App\Helpers\APIResponse;
use App\Helpers\Email;
use App\Helpers\Notification;
use App\Helpers\UserCommon;
use App\Models\Pass;
use App\Repositories\ContactGroupRepository;
use App\Repositories\EventRepository;
use App\Repositories\PassRepository;
use App\Repositories\PassUserRepository;
use App\Repositories\UserContactRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Models\BannedCommunityUser;

class PassService extends PassRepository
{
    use UserCommon, Email;

    /**
     * @var EventService
     */
    private $eventRepo;
    /**
     * @var ContactGroupService
     */
    private $contactGroupRepo;
    /**
     * @var
     */
    private $userContactRepo;
    /**
     * @var
     */
    private $passUserRepo;
    /**
     * @var NotificationService
     */
    private $notySrcv;
    /**
     * @var UserService
     */
    private $userRepo;

    /**
     * PassService constructor.
     *
     * @param Pass $pass
     * @param EventRepository $eventRepo
     * @param ContactGroupRepository $contactGroupRepo
     * @param UserContactRepository $userContactRepo
     * @param PassUserRepository $passUserRepo
     * @param NotificationService $notificationService
     * @param UserRepository $userRepo
     */
    public function __construct(
        Pass $pass,
        EventRepository $eventRepo,
        ContactGroupRepository $contactGroupRepo,
        UserContactRepository $userContactRepo,
        PassUserRepository $passUserRepo,
        NotificationService $notificationService,
        UserRepository $userRepo
    ) {
        parent::__construct($pass);
        $this->eventRepo = $eventRepo;
        $this->contactGroupRepo = $contactGroupRepo;
        $this->passUserRepo = $passUserRepo;
        $this->notySrcv = $notificationService;
        $this->userRepo = $userRepo;
        $this->userContactRepo = $userContactRepo;
    }

    /**
     * Create Pass
     *
     * @param $data
     *
     * @return array
     * @throws Exception
     */
    public function createPass($data)
    {
        //Check if pass is recurring Or Not
        $shouldConvertTimezone = (isset($data['created_by']))? false:true;
        $id = (isset($data['created_by']))?$data['created_by'] : auth()->id();

        $data['created_by'] = $id;
        $user = $this->userRepo->find($id);
        if (!$user['result']) {
            return [
                'bool' => false,
                'message' => 'Error: Invalid user'
            ];
        }

        if (!$this->canSendPasses($user['result'])) {
            return [
                'bool' => false,
                'message' => 'Not Allowed to send passes with this user'
            ];
        }

        foreach($user['result']->bandCommunities as $bandcommunities){
            if($user['result']->community->id == $bandcommunities->community->id){
                return [
                    'bool' => false,
                    'message' => 'The user is not allowed to recieve pass in this community'
                ];
            }
        }

        // Add community id and house id in pass
        $data['community_id'] = $user['result']->community_id;


        if (($data['community_id'] == "66" || $data['community_id'] == "65") && $data['pass_type'] == 'recurring') {
            return [
                'bool' => false,
                'message' => 'Recurring Passes has been disabled for your community. For additional help, please contact your property manager'
            ];
        }
        Log::emergency($data['community_id'],$data);

        $data['house_id'] = $user['result']->house_id;
        $data['event_id'] = $data['event_id'] ? $data['event_id'] : 14;
        $passEndDate = null;

        // Convert date to timezone if flag is set
        if ($shouldConvertTimezone) {
            $data['pass_date'] = communityTzToDBTz($data['pass_date'], $data['community_id']);
            if ($data['pass_validity'] == 'xxx') {
                $data['pass_end_date'] =  Carbon::parse($data['pass_end_date'])->format('Y-m-d H:i:s');
                $passEndDate = communityTzToDBTz($data['pass_end_date'], $data['community_id']);

            }
        }

        // Render date time and hour here
        if (!isset($data['is_recurring'])) {
            $validity = $data['pass_validity'];
            $pass_date = $data['pass_date'];
            $data['pass_start_date'] = $data['pass_date'];
            if ($validity == 'xxx'){
                $new_date = $passEndDate;
            } else if ($validity == 5) {
                $new_date = Carbon::parse($pass_date)->addMinutes($validity);
            } else {
                $new_date = Carbon::parse($pass_date)->addHours($validity);
            }
            $data['pass_date'] = $new_date;
            $data['created_at'] = $new_date;
        }

        $data['is_sent_by_sms'] = isset( $data['isSMS']) &&  $data['isSMS'] == true ? 1 : 0;
        //create pass
        $pass = $this->create($data);

        if (!$pass['bool']) {
            return [
                'bool' => false,
                'message' => "Something went wrong. Please try again."
            ];
        }

        #=================== Send Pass to Group =================
        if (isset($data['group_id'])) {
            $ContactGroup = $this->contactGroupRepo->find($data['group_id'])['result'];
            if(empty($ContactGroup)){
                return [
                    'bool' => false,
                    'message' => 'Group No Found'
                ];
            }
            if($ContactGroup->userContacts->isEmpty()){
                return [
                    'bool' => false,
                    'message' => 'No Contact Found In this group'
                ];
            }
            $userIdsForContacts = $ContactGroup->userContacts->pluck('id');
            $data['selected_contacts'] = $userIdsForContacts;
        }

        $userList = [];

        # ================== Get User Contact From Contacts Ids =================
        if(isset($data['selected_contacts']) && !empty($data['selected_contacts'])){
            //for recurring pass
            if(isset($data['is_recurring']) && $data['is_recurring'] == 1){
                $userList[]  = $this->userRepo->find($data['selected_contacts'][0])['result']; // Set previous added user in Pass Users

            } else if($data['pass_type'] == 'self'){  //self pass
                $userList[]  = auth()->user(); // send pass to myself

            } else {
                $userContacts = $this->userContactRepo->getByIds($data['selected_contacts']);
                //Get or create guest
                if($userContacts->isNotEmpty()){
                    foreach ($userContacts as $userContact){
                        $phoneNumber = $userContact->contact->phone_number;
                        $dialCode = $userContact->contact->dial_code;
                        $names = $this->splitName($userContact->contact_name);
                        $userList[] = $this->userRepo->getOrCreateUserByPhoneNumbers($phoneNumber,[
                            'first_name' =>  $names['first_name'],
                            'last_name' =>  $names['last_name'],
                            'email' =>  $userContact->email,
                            'phone_number' => $phoneNumber,
                            'dial_code' => $dialCode,
                            'role_id' => 7, //Guest Outsider Daily
                        ]);
                    }
                }
            }
        }
        $contacts = isset($fetch_group_contacts) ? $fetch_group_contacts : $data['selected_contacts'];
        $temporary = array();
        $qrCodes = array();
        # ===================== Add Pass Users ===================

        //Add pass users
        if(!empty($userList)){
            foreach ($userList as $user) {

                $passUserData = [
                    'pass_id' => $pass['result']->id,
                    'user_id' => $user->id,
                    'qr_code' => '000000',
                ];

                //Passes users
                $userPass = $this->passUserRepo->create($passUserData);
                if (!$userPass['bool']) {
                    return [
                        'bool' => false,
                        'message' => "Something went wrong.Please try again."
                    ];
                }


                //Update qrcode in pass user
                if ($userPass['result'] != null) {
                    $userPass = $userPass['result'];
                    $aPass['qr_code'] = "{$userPass->id}{$userPass['pass_id']}{$userPass['user_id']}";// generate qrcode
                    $userPass->qr_code = $aPass['qr_code'];
                    $userPass->save();
                    $qrCodes[] = $userPass->qr_code;
                    //Send Qrcode to email
                    if($userPass->user->email_notification == '1'){
                        $qrCodeEmail = sendQRCodeToEmail($userPass,true);
                        if(!$qrCodeEmail['bool']){
                            Log::error($qrCodeEmail['message']);
                        }
                    }
                }
            }
        }

        # ======================== Sent Notification to guest for new pass sent ===================
        $createdBy = auth()->user();
        $this->notySrcv->notifyGuestsAfterNewPassSent($pass['result'],$createdBy); //
        Log::emergency("createPass");
        Log::emergency(json_encode($pass['result']));
        Log::emergency(json_encode($createdBy));


        return [
            'bool' => true,
            'qrcodes' => $qrCodes,
            'message' => 'Pass sent and created'
        ];
    }

    /**
     * Edit Pass
     *
     * @param $data
     * @param $passId
     * @return array
     * @throws Exception
     */
    public function editPass($data,$passId)
    {
        $pass = self::find($passId)['result'];
        if(empty($pass)){
            return [
                'bool' => false,
                'message' => 'No Record Found'
            ];
        }
        $shouldConvertTimezone = isset($data['created_by']);
        $id = $shouldConvertTimezone ? $data['created_by'] : auth()->user()->id;

        $data['created_by'] = $id;
        $user = $this->userRepo->find($id);
        if (!$user['result']) {
            return [
                'bool' => false,
                'message' => 'Error: Invalid user'
            ];
        }

        //$user = $user;
        if (!$this->canSendPasses($user['result'])) {
            return [
                'bool' => false,
                'message' => 'Not Allowed to send passes with this user'
            ];
        }
        // add community id and house id in pass
        $data['community_id'] = $user['result']->community_id;
        $data['house_id'] = $user['result']->house_id;
        $data['event_id'] = $data['event_id'] ? $data['event_id'] : 14;

        // convert date to timezone if flag is set
        if ($shouldConvertTimezone) {
            $data['pass_date'] = communityTzToDBTz($data['pass_date'], $data['community_id']);
        }

        // render date time and hour here
        if (!isset($data['is_recurring'])) {
            $validity = $data['pass_validity'];
            $pass_date = $data['pass_date'];
            $data['pass_start_date'] = $data['pass_date'];

            if ($validity == 5) {
                $new_date = Carbon::parse($pass_date)->addMinutes($validity);
            } else {
                $new_date = Carbon::parse($pass_date)->addHours($validity);
            }

            $data['pass_date'] = $new_date;
        }

        //update pass
        $pass = self::update($data,$passId);

        if ($pass['bool']) {
            $createdBy = auth()->user();
            $pass = self::find($passId)['result'];
            $this->notySrcv->notifyGuestsAfterNewPassUpdated($pass,$createdBy); //
            return [
                'bool' => true,
                'message' => 'Pass Updated'
            ];
        }
        return [
            'bool' => false,
            'message' => $pass['message']
        ];
    }

    /**
     * add pass recipient
     * @param $data
     * @param $passId
     * @return array
     * @throws Exception
     */
    public function addPassRecipient($data,$passId){
        $pass = self::find($passId)['result'];
        if(empty($pass)){
            return [
                'bool' => false,
                'message' => 'No Record Found'
            ];
        }
        $contactIds = (isset($data['contactIds']) && !empty($data['contactIds']))?$data['contactIds']:null;
        if (empty($contactIds)){
            return [
                'bool' => false,
                'message' => 'No Contact selected'
            ];
        }
        $userContacts = $this->userContactRepo->getByIds($contactIds);
        if($userContacts->isEmpty()){
            return [
                'bool' => false,
                'message' => 'No Contact Found'
            ];
        }
        //Get or create guest
        foreach ($userContacts as $userContact){
            $phoneNumber = $userContact->contact->phone_number;
            $userList[] = $this->userRepo->getOrCreateUserByPhoneNumbers($phoneNumber,[
                'first_name' =>  $userContact->contact_name,
                'email' =>  $userContact->email,
                'phone_number' => $phoneNumber,
                'role_id' => 7, //Guest Outsider Daily
            ]);
        }

        $qrCodes = array();
        # ===================== Add Pass Users ===================
        //Add pass users
        if(!empty($userList)){
            foreach ($userList as $user) {
                $passUserData = [
                    'pass_id' => $pass->id,
                    'user_id' => $user->id,
                    'qr_code' => '000000',
                ];
                //Passes users
                $userPass = $this->passUserRepo->create($passUserData);
                if (!$userPass['bool']) {
                    return [
                        'bool' => false,
                        'message' => "Something went wrong.Please try again."
                    ];
                }

                //Update qrcode in pass user
                if ($userPass['result'] != null) {
                    $userPass = $userPass['result'];
                    $aPass['qr_code'] = "{$userPass->id}{$userPass['pass_id']}{$userPass['user_id']}";// generate qrcode
                    $userPass->qr_code = $aPass['qr_code'];
                    $userPass->save();
                    $qrCodes[] = $userPass->qr_code;
                    //Send Qrcode to email
                    if($userPass->user->email_notification == '1'){
                        $qrCodeEmail = sendQRCodeToEmail($userPass,true);
                        if(!$qrCodeEmail['bool']){
                            Log::error($qrCodeEmail['message']);
                        }
                    }
                }
            }
        }
        # ======================== Sent Notification to guest for new pass sent ===================
        $createdBy = auth()->user();
        $this->notySrcv->notifyGuestsAfterNewPassSent($pass['result'],$createdBy); //
        Log::emergency("addPassRecipient");
        Log::emergency(json_encode($pass['result']));
        Log::emergency(json_encode($createdBy));

        return [
            'bool' => true,
            'qrcodes' => $qrCodes,
            'message' => 'Pass recipient(s) updated successfully'
        ];
    }

    //Created By Sabih
    public function store_original($data)
    {
        $shouldConvertTimezone = isset($data['created_by']);
        $id = $shouldConvertTimezone ? $data['created_by'] : auth()->user()->id;

        $data['created_by'] = $id;
        $user = $this->find($id);

        if (!$user['bool']) {
            return APIResponse::error('Error: Invalid user');
        }

        $user = $user['result'];
        if (!$this->canSendPasses($user)) {
            return APIResponse::error('Not Allowed to send passes with this user');
        }

        // add community id and house id in pass
        $data['community_id'] = $user->community;
        $data['house_id'] = $user->house;
        $data['event_id'] = $data['event_id'] ? $data['event_id'] : 14;

        // convert date to timezone if flag is set
        if ($shouldConvertTimezone) {
            $data['pass_date'] = communityTzToDBTz($data['pass_date'], $data['community_id']);
        }

        if ($data['event_id'] == 0) {
            if (!is_null($data['event_name'])) {
                $event = $this->eventRepo->create([
                    'name' => $data['event_name'],
                    'description' => $data['event_name'],
                    'created_by' => $id,
                ]);

                if (!$event['bool']) {
                    return APIResponse::error($event['message']);
                }
                $event = $event['result'];
                $data['event_id'] = $event->id;
            } else {
                $data['event_id'] = 14;
            }
        }


        // render date time and hour here
        if (!isset($data['is_recurring'])) {
            $validity = $data['pass_validity'];
            $pass_date = $data['pass_date'];
            $data['pass_start_date'] = $data['pass_date'];

            if ($validity == 5) {
                $new_date = Carbon::parse($pass_date)->addMinutes($validity);
            } else {
                $new_date = Carbon::parse($pass_date)->addHours($validity);
            }

            $data['pass_date'] = $new_date;
        }

        $data["visitor_type"] = (int)$data["visitor_type"];

        //TODO: move to edit function
        if ($data['edit'] == true) {
            $_pass = Pass::find($data['id']);
            if (!$_pass) {
                return APIResponse::error('No Pass Found');
            } else {
                $_pass->update($data);

                // parental notifications
                $pc = new PCNotificationController();
                $contacts_array = $_pass->getQrs->pluck('user_id');
                $pc->notifyGuestsAfterPassUpdated($data, $_pass->id, $user->id, $contacts_array);

                return self::success('Pass Updated !');
            }
        }

        $pass = $this->create($data);

        if (!$pass['bool']) {
            return APIResponse::error($pass['message']);
        }

        if (isset($data['group_id'])) {
            $userIdsForContacts = $this->contactSrvc->find($data['group_id'])->pluck('userContacts.created_by');
            $data['selected_contacts'] = $userIdsForContacts;
        }

        $contacts = isset($fetch_group_contacts) ? $fetch_group_contacts : $data['selected_contacts'];
        $temporary = array();
        foreach ($contacts as $contact) {
            $aPass = [
                'pass_id' => $pass->id,
                'user_id' => $contact,
                'qr_code' => '000000',
            ];

            $new_pass = $this->userPassSrcv->storePass($aPass);
            if (!$new_pass['bool']) {
                return APIResponse::error($new_pass['message']);
            }

            if ($new_pass != null) {
                $new_pass = $new_pass['result'];
                $aPass['qr_code'] = "{$new_pass->id}{$new_pass['pass_id']}{$new_pass['user_id']}";
                $new_pass->qr_code = $aPass['qr_code'];
                $new_pass->save();
            }

            //create notification entry
            $df = Carbon::parse(date('Y-m-d'))->format('m-d-Y g:i a');
            $noty = $this->notySrcv->create([
                'user_id' => $aPass['user_id'],
                'heading' => 'New Pass Received',
                'subheading' => "Created By $user->FullName",
                'text' => "Pass received from $user->FullName at $df",
                'category' => 'passes',
                'type' => 'zuul',
            ]);
            $noty->link = url('/nd/'.$noty->id);
            $noty->save();

            array_push($temporary, [
                'description' => $data['description'],
                'title' => 'New Pass Received',
                'nid' => $noty->id,
                'link' => url('/nd/'.$noty->id),
                'info' => APIResponse::success(['qr_code' => $new_pass->qrcode], $data['description']),
            ]);

            $this->sendQRCodeEmail($new_pass);
        }

        $parental_push = [
            "title" => "New pass was sent by {$user->name}",
            "desc" => $data['description'],
        ];

        $this->sendEmailToHead($user, "notify_when_member_send_pass_to_others",
            $pass->id, $parental_push['title'], $parental_push['desc']);

        $returnObj = [
            'pushes' => $temporary,
        ];
        if (isset($data['event_id']) && (int)$data['event_id'] == 16) {
            $returnObj['qrcode'] = $new_pass->qrcode;
        }

        //parental notifications
        $noty = new PCNotificationController();
        $noty->notifyGuestsAfterNewPassSent($data, $pass->id, $user->id, $contacts);

        Log::emergency("store_original");
        Log::emergency(json_encode($data));
        Log::emergency(json_encode($pass->id));
        Log::emergency(json_encode($user->id));
        Log::emergency(json_encode($contacts));

        return APIResponse::success($returnObj, 'Pass sent and created');
    }

    function sendEmailToHead($user, $activity, $pass_id = null, $title = "Parental Control", $notification = null)
    {
        $user = User::where('id', $user->id)->with(['member_parental_control'])->first();
        if (!$user) {
            return;
        }

        if ($user->head_of_family != 1) {
            if ($user->member_parental_control && count($user->member_parental_control)) {
                if ((int)$user->member_parental_control->{$activity}) {
                    $head = User::where('house', $user->house)->where('head_of_family', 1)->first();

                    $allowed_parents = $head ? ParentalControl::where('allow_parental_controls_to_member', 1)
                        ->where('parent_id', $head->id)->get() : [];

                    $push_notifications = $head ? [
                        [
                            'device_id' => null,
                            'user_id' => $head->id,
                            'pr_id' => $pass_id,
                            'status' => 0,
                            'type' => 5,
                            'heading' => $title,
                            'text' => $notification,
                        ],
                    ] : [];
                    $push_users = $head ? [$head] : [];
                    foreach ($allowed_parents as $allowed_parent) {
                        $push_notifications[] = [
                            'device_id' => null,
                            'user_id' => $allowed_parent->member_id,
                            'pr_id' => $pass_id,
                            'status' => 0,
                            'type' => 5,
                            'heading' => $title,
                            'text' => $notification,
                        ];
                        $push_users[] = $allowed_parent;
                    }

                    //dd($push_notifications);
                    foreach ($push_notifications as $key => $push_notification) {
                        $noti = PushNotification::create($push_notification);

                        $push = [
                            'title' => $title,
                            'description' => $notification,
                            'nid' => $noti->id,
                        ];

                        $this->sendPushNotification($push, false, $push_users[$key]);
                    }
                }
            }
        }
    }


    /**
     * create a quick pass from guard behalf of the resident
     * @param $data
     * @param $createdBy
     * @return array
     * @throws Exception
     */
    public function createQuickPass($data, $createdBy) {

        $createdBy = $this->userRepo->find($createdBy);

        if (!$createdBy['result']) {
            return [
                'bool' => false,
                'message' => 'Error: Invalid user'
            ];
        }
        // Check User have Permission to send a Pass
        if (!$this->canSendPasses($createdBy['result'])) {
            return [
                'bool' => false,
                'message' => 'Not Allowed to send passes with this user'
            ];
        }

        # ================== Get Or Create user =================
        $phoneNumber = $data['phone_number'];
        $displayName = $this->splitName($data['display_name']);

        if(auth()->user() != null) {
            $check_banned_user = BannedCommunityUser::where('phone_number',$phoneNumber)->where('community_id',auth()->user()->community_id)->first();

            if($check_banned_user != null){
                return APIResponse::error('THIS GUEST IS BANNED FROM THE COMMUNITY. DENY ENTRY!');
            }

            $check_banned_user = BannedCommunityUser::where('user_name',$displayName)->where('community_id',auth()->user()->community_id)->first();

            if($check_banned_user != null){
                return APIResponse::error('THIS GUEST IS BANNED FROM THE COMMUNITY. DENY ENTRY!');
            }
        }


        $user = $this->userRepo->getOrCreateUserByPhoneNumbers($phoneNumber,[
            'first_name' =>  $displayName['first_name'],
            'middle_name' =>  $displayName['middle_name'],
            'last_name' =>  $displayName['last_name'],
            'email' =>  "",
            'phone_number' => $phoneNumber,
            'dial_code' => $data['dial_code'] ?? "+1",
            'role_id' => 7, //Guest Outsider Daily
        ]);

        if($phoneNumber != '1111111111'){
            $band = BannedCommunityUser::where(['community_id'=>$createdBy['result']->community_id, 'phone_number' => $phoneNumber ])->count();

            if($band > 0 ){
                return [
                    'bool' => false,
                    'message' => 'THIS GUEST IS BANNED FROM THE COMMUNITY.  DENY ENTRY'
                ];
            }

        }


        $passData['community_id'] = $createdBy['result']->community_id;
        $passData['house_id'] = $createdBy['result']->house_id;
        $passData['event_id'] = 16; // Event Quick Pass From Guard
        $passData['created_by'] =  $createdBy['result']->id;
        $passData['description'] = ""; //"A pass has been sent on behalf of ".$createdBy['result']->fullName." from ".$guardName.", T&C Applied";
        $passData['visitor_type'] = $data['visitor_type'];
        $passData['pass_type'] = 'one'; // one time pass
        // Convert date to timezone
        $selectedPassDate =  now();
        $pass_start_date = communityTzToDBTz($selectedPassDate, $passData['community_id']);
        // Render date time and hour here
        $validity = '5';
        $passData['pass_start_date'] = $pass_start_date;
        $pass_date = Carbon::parse($pass_start_date)->addMinutes($validity);
        $passData['pass_validity'] = $validity;
        $passData['pass_date'] = $pass_date;
        $passData['is_quick_pass'] = '1';
        //create pass
        $pass = $this->create($passData);

        if (!$pass['bool']) {
            return [
                'bool' => false,
                'message' => "Something went wrong. Please try again."
            ];
        }

        $qrCodes = array();
        # ===================== Add Pass Users ===================
        //Add pass users
        if(!empty($user)){
            //Passes users
            $userPass = $this->passUserRepo->create([
                'pass_id' => $pass['result']->id,
                'user_id' => $user->id,
                'qr_code' => '000000',
            ]);


            if (!$userPass['bool']) {
                return [
                    'bool' => false,
                    'message' => "Something went wrong. Please try again."
                ];
            }

            //Update qr code in pass user
            if ($userPass['result'] != null) {
                $userPass = $userPass['result'];
                $aPass['qr_code'] = "{$userPass->id}{$userPass['pass_id']}{$userPass['user_id']}";// generate qrcode
                $userPass->qr_code = $aPass['qr_code'];
                $userPass->save();
                $qrCodes[] = $userPass->qr_code;
                //Send Qrcode to email
                if($userPass->user->email_notification == '1'){
                    $qrCodeEmail = sendQRCodeToEmail($userPass,true); // Shariq 2nd parameter was false before
                    if(!$qrCodeEmail['bool']){
                        Log::error($qrCodeEmail['message']);
                    }
                }
            }

        }
        # ======================== Sent Notification to guest for new pass sent ===================
        //$createdBy = auth()->user();
        $this->notySrcv->notifyGuestsAfterNewPassSent($pass['result'],$createdBy['result']); //

        Log::emergency("createQuickPass");
        Log::emergency(json_encode($pass['result']));
        Log::emergency(json_encode($createdBy['result']));

        return [
            'bool' => true,
            'qrcodes' => $qrCodes,
            'message' => 'Pass sent and created'
        ];
    }

    /**
     * retract sent pass
     * @param $passId
     * @return array
     * @throws Exception
     */
    public function retractSentPass($passId){
        try{
            $pass = self::find($passId)['result'];
            if(empty($pass)){
                return ['bool' => false,'message' => 'Pass Not Found'];
            }
            $createdBy = auth()->user();
            $this->notySrcv->notifyGuestsAfterPassRetracted($pass,$createdBy);

            //remove pass
            $deletePass = self::delete($passId);
            if(!$deletePass['bool']){
                return ['bool' => false,'message' => $deletePass['message']];
            }
            //remove pass users
            $deletePassUsers = $this->passUserRepo->deleteByPass($passId);
            if(!$deletePassUsers['bool']){
                return ['bool' => false,'message' => $deletePassUsers['message']];
            }

            #todo need to implement head of family email
            //$this->sendEmailToHead($user , 'notify_when_member_retract_a_pass');

            //remove zuul notification against pass
            $deleteNotification = $this->notySrcv->deleteNotificationByPass($passId);
            if(!$deleteNotification['bool']){
                return ['bool' => false,'message' => $deleteNotification['message']];
            }

            return ['bool' => true, 'message' => 'Pass Retracted Successfully' ];
        } catch (\Exception $exception){
            return ['bool' => false, 'message' => $exception->getMessage()];
        }
    }

    public function testPassService($message){
        echo $message;
    }
}
