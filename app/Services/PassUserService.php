<?php

namespace App\Services;

use App\Helpers\PushNotificationTrait;
use App\Models\Notification;
use App\Mail\DynamicEmail;
use App\Models\Pass;
use App\Models\PassUser;
use App\Models\User;
use App\Models\ConnectedPie;
use App\Models\Camera;
use App\Models\ScanLog;
use App\Repositories\PassUserRepository;
use App\Repositories\ScanLogRepository;
use App\Repositories\ScannerRepository;
use App\Repositories\UserRepository;
use App\Services\CameraService;
use Carbon\Carbon;
use Kreait\Firebase\Factory;
use App\Jobs\QrcodeValidate;
use Illuminate\Support\Facades\Log;

class PassUserService extends PassUserRepository
{
    use PushNotificationTrait;
    private $userRepo;
    public $cameraService;
    private $passService;
    private $scannerRepo;
    private $scanLogRepo;
    public function __construct(
        PassUser $passUser,
        UserRepository $userRepo,
        ScannerRepository $scannerRepo,
        ScanLogRepository $scanLogRepo,
        CameraService $cameraService
        // PassService $passService

    ) {
        parent::__construct($passUser);

        $this->userRepo = $userRepo;
        $this->scannerRepo = $scannerRepo;
        $this->scanLogRepo = $scanLogRepo;
        $this->cameraService = $cameraService;
        //$this->passService = $passService;
    }

    public function getCameraById($camId){

    }

    /**
     * @param $scannerType
     * @param $scannerId
     * @param $scannedQrCode
     * @return array
     * @throws \Exception
     */

    public function getPassByQrcode($qrcode){

        $passuser = PassUser::where('qr_code', $qrcode)->first();
        if($passuser){

            $pass = Pass::where('id', $passuser->pass_id)->first();
            return $pass;
        }
        return null;
    }

    public function scanQrCode($scannerType, $scannerId, $scannedQrCode, $checkNoti = 0, $displayName = '', $isRemoteGuard=false, $guardId=null){

        $validateQrCode = $this->validateQrCode($scannerType, $scannerId, $scannedQrCode, $checkNoti, $displayName, $isRemoteGuard, $guardId);

        if ($validateQrCode['bool'] == false) {
            return $validateQrCode;
        }

        //Update Qr code
        $passUser =  self::updateOrGetByQrCode([
            'is_scan' => '1',//#todo need to set 1 on production
            'scan_at' => date('Y-m-d H:i:s') //new added in zuul2 #todo old system using deleted_at for ScannedDate
        ],$scannedQrCode);
        if (!$passUser['bool']){
            return ['bool' => false,'message' => $passUser['message']];
        }
        $passUser =  $passUser['result'];


        //Check If Pass Type =>  Recurring
        if ($passUser->pass->pass_type == 'recurring'){
            $createRecurringPass = $this->createRecurringPass($passUser);

            if(!$createRecurringPass['bool']){
               return ['bool' => false,'message' => $createRecurringPass['message']];
            }
        }

        return ['bool' => true,'message' => 'QR Code Verified', 'scanLog' => $validateQrCode['scanLog'] ?? ""];
    }

    /**
     * validate QR code
     * @param $scannerType
     * @param $scannerId
     * @param $scannedQrCode
     * @return array
     * @throws \Exception
     */
    private function validateQrCode($scannerType, $scannerId, $scannedQrCode, $checkNoti = 0, $displayName = '', $isRemoteGuard = null, $guardId = null)
    {
        // dd($isRemoteGuard, $guardId);
        $scanBy = null;
        $passReceiver = null;
        $passSender = null;
        $scanLogResponse =  ['bool' => true,'message' => 'QRCode Verified'];
        $scanLogTextLog = "";


        #todo $this->qrcodeObj =>  $passUser
        //Get Pass User Detail
        $passUser  = self::getByQrCode($scannedQrCode)['result'];// #todo need to implement ->withTrashed() functionality
        if (empty($passUser)){
            return ['bool' => false,'message' => 'Qr Code Not Found'];
        }
        if (empty($passUser->pass)){
            return ['bool' => false,'message' => 'Pass Not Found'];
        }

        $passReceiver  = $passUser->user ?? "";
        $passSender  = $passUser->pass->createdBy ?? "";
        $scanLog = [];
        //Check if Scan by Security Guard Or Scanner
        #todo $this->scanner =>  $scanBy

        if($isRemoteGuard && $guardId) {
            $scanBy = $this->userRepo->find($guardId)['result']; //Get Security Guard Data
            //Check if user has guard role
            if(!$scanBy->hasRole('guards') && !$scanBy->hasRole('guard_admin')){
                return ['bool' => false,'message' => 'Invalid Guard User'];
            }
            // return $scanBy;
        }else if($scannerType == 'guard'){
            // change static value
            $scanBy = $this->userRepo->find($scannerId)['result']; //Get Security Guard Data
            //Check if user has guard role
            if(!$scanBy->hasRole('guards') && !$scanBy->hasRole('guard_admin')){
                return ['bool' => false,'message' => 'Invalid Guard User'];
            }
        } else {
            $scanBy = $this->scannerRepo->find($scannerId)['result']; //Get Scanner Device Data

            if(empty($scanBy->camera_id)){
                return ['bool' => false,'message' => 'Camera is not attached'];
            }
            // That is the scanner code
            // $cam = Camera::where('scanner_id', $scannerId)->first();

            // $cameraId = $cam->id;
            $cameraId = $scanBy->camera_id;
            $camera = $this->cameraService->find($cameraId)['result'];
            if(empty(ConnectedPie::find($camera->connected_pi_id))){
                return ['bool' => false,'message' => 'Pie is not exist'];
            }

            $connected_pi_id = ConnectedPie::find($camera->connected_pi_id)->first()->mac_address;
            if (empty($camera)) {
                return APIResponse::error('No Camera Exist');
            }
            try {


                $data = [
                    'pass_user_id' => $passUser->id,
                    'scanner_id' =>  ($scannerType == 'scanner')?$scannerId:null,
                    'type' => $scannerType,
                    'license_image' => (!empty($passReceiver)) ? $passReceiver->license_image:null,
                    'license_plate_image' => '',
                    'log_text' => 'QRCode Verified',
                    'is_success' => '1'
                ];

                $id = ScanLog::create($data);

                $factory = (new Factory)->withServiceAccount(public_path('assets/zuul-master-firebase-adminsdk-us959-5c7a166c45.json'))->withDatabaseUri(env('FIREBASE_DATABASE'));
                $realtimeDatabase = $factory->createDatabase();

                $realtimeDatabase->getReference(ConnectedPie::find($camera->connected_pi_id)->first()->uid)->set([
                    $camera->mac_address => [
                        'description' => $camera->name,
                    ],
                    'capture_request' => [
                        'mac_address' => $camera->mac_address,
                        'status' => 'true',
                        'scanlog_id' => $id->id
                    ],
                ]);

                $datas = [
                    'id' => $id->id,
                    'pi_address' => ConnectedPie::find($camera->connected_pi_id)->first()->uid,
                    'mac_address' => $camera->mac_address,
                ];


                $scanLog = $id;

            } catch (\Exception $exception) {
                return ['bool' => false,'message' => 'Something Went Wrong, Please try again with Firebase connection'];
            }
        }


        $timezone =  $passUser->pass->community->timezone->abbreviation ?? config('app.timezone');
        //Convert datetime from community timezone
        $convertedPassStartDate = Carbon::parse($passUser->pass->pass_start_date)->setTimezone($timezone); //start date
        $convertedPassDate = Carbon::parse($passUser->pass->pass_date)->setTimezone($timezone); //end date
        $convertedCurrentDateTime = Carbon::now($timezone); //converted current date time



        //Check  pass end date => greater than current datetime
        $isExpired = Carbon::parse($convertedCurrentDateTime,$timezone)->greaterThan($convertedPassDate);
        if( $isExpired ){
            $scanLogResponse =  ['bool' => false,'message' => 'Date of qrcode has passed, please contact its community resident'];
            return $scanLogResponse;
        }

        //check if qrcode already scanned
        if ($passUser->is_scan == '1'){
            $scanLogResponse =  ['bool' => false,'message' => 'This QRCode has already been scanned, it is no longer valid'];
            return $scanLogResponse;
        }
        $isDateInRange = Carbon::parse($convertedCurrentDateTime,$timezone)->between($convertedPassStartDate, $convertedPassDate);
        if(!$isDateInRange) {
            $scanLogResponse =  ['bool' => false,'message' => 'Date of qrcode is not reached, please contact its community resident'];
            return $scanLogResponse;
        }

        //check if qrcode belongs to correct community
        if ($scanBy->community->id != $passUser->pass->community->id){
            $scanLogResponse =  ['bool' => false,'message' => "QRcode isn't matched with community"];
            return $scanLogResponse;
        }
        #todo need to discuss  $this->checkCondition => withTrashed()
        if($scannerType == 'guard'){
            $scanLog = $this->scanLogRepo->create([
                'scan_by' => ($scannerType == 'guard') ? $scanBy->id:null,
                'scanner_id' => ($scannerType == 'scanner') ? $scanBy->id:null,
                'pass_user_id' => $passUser->id ?? "",
                'community_id' => $scanBy->community_id ?? "", //new added in zuul2 #todo old system has not community_id
                'type' => $scannerType,
                'guard_display_name' => $displayName,
                'log_text' => $scanLogResponse['message'],
                'is_success' =>  ($scanLogResponse['bool'] == true) ? '1':'0',
                'license_image' => (!empty($passReceiver)) ? $passReceiver->license_image:null,
                /*'log_text' => 'QRCode Verified',
                 'is_success' => 1,*/
            ])['result'];

        }

        $npass = $this->getPassByQrcode($scannedQrCode);


        //Send push notification to receiver If Got any Error
        if (!$scanLogResponse['bool']){
            $push = array(
                'description' => 'Error scanning QR Code',
                'title' => 'ERROR: 404',
                'nid' => $passReceiver->id,
                'showalert' =>  $scanLogResponse['message'],
            );
            $this->sendPushNotificationToUser($push, $passReceiver);
        }

        if($scannerType == 'guard'){
            return ['bool' => $scanLogResponse['bool'], 'message' =>  $scanLogResponse['message'], 'scanLog' => $scanLog];
        }else{
            return ['bool' => $scanLogResponse['bool'], 'message' =>  $scanLogResponse['message'], 'scanLog' => $scanLog];
        }

        //return $scanLogResponse;
    }

    public function sendPassScanNotificationToUser($npass, $scanLog){



        $displayName = $scanLog["guard_display_name"];
        $passSender = User::where('id',$npass['created_by'])->first();
        $passHeadMember = User::where('house_id',$passSender->house_id)->where('role_id',4)->first();
        $date = Carbon::now()->format('Y-m-d H:i:s');

        if($passSender['community_id'] == 64){

            $date = Carbon::now();

            $date->subHours(2);

            $date = $date->format('Y-m-d H:i:s');

        }

        $zuulNotificationData = [
            'pass_id' => $npass['id'],
            'event_id' => $npass['event_id'],
            'user_id' => $npass['created_by'],
            'type' => "zuul",
            'category' => "send_pass",
            'heading' => 'Guest Arrived',
            'subheading' => $npass['description'],
            'text' => $scanLog->id.' '.$displayName.' has now entererd the community, arrival time was '.$date
        ];

        $zuulNotification = Notification::create($zuulNotificationData);
        $push = array(
            'description' => $scanLog->id.' '.$displayName.' has now entererd the community, arrival time was '.$date,
            'title' => 'Guest Arrived',
            'nid' => $scanLog->id,
            'showalert' =>  'success',
        );

        //For Resident
        return $this->sendPushNotificationToUser($push, $passSender);

    }

    /**
     * create a recurring pass
     * @param PassService $passService
     * @param $passUser
     * @return array
     * @throws \Exception
     */
    private function createRecurringPass($passUser){
        $pass = $passUser->pass;
        $userId = $passUser->user_id;
        // check if end date of pass not passed by 15 + current date
        $end_date = Carbon::parse($pass->pass_date);
        $start_date = Carbon::parse(Carbon::now()->addMinutes(15));

        $minutes = $start_date->diffInMinutes($end_date);

        if($minutes <= 15 ){
            // delete the pass without recurring
            //$qrcodeObj->delete();
            return [
                'bool' => true,
                'message' => 'Qrcode Deleted',
            ];
        }

        $hours = $start_date->diffInHours($end_date);
        $passData = [
            'pass_validity' => $pass->pass_validity,
            // 'pass_validity' => $hours, #todo need to discuss why is not coming from pass_validity column
            'pass_type' => 'recurring',
            'visitor_type' => $pass->visitor_type,
            'description' => $pass->description,
            'event_id' => $pass->event_id,
            'pass_date' => $pass->pass_date,
            'pass_start_date' => Carbon::now()->addMinutes(15),
            'created_by' => $pass->created_by, // pass created by
            'latitude' => $pass->latitude,
            'longitude' => $pass->longitude,
            'selected_contacts' => [ $userId ],
            'is_recurring' => 1,
        ];

        //Create another recurring pass
        $passService = app(PassService::class);
        $pass =  $passService->createPass($passData);
        //If only pass send via sms
        if($pass['bool']){

            $PassUser = PassUser::where("qrcodes",$pass['qrcodes'][0])->first();

            if($PassUser->pass->is_sent_by_sms == 1){
                $pass = $PassUser->pass;
                $zuulNotificationData = [
                    'pass_id' => $pass->id,
                    'event_id' => $pass->event_id,
                    'user_id' => $pass->created_by,
                    'type' => "zuul",
                    'category' => "send_pass",
                    'heading' => 'Confirmation for Recurring Pass Via SMS',
                    'subheading' => $pass->description,
                    'text' => '',
                    'tap_and_send_sms' => 1,
                    'tap_and_send_sms_qr_code' => $pass->qr_code,
                ];

                $zuulNotification = Notification::create($zuulNotificationData);

            }
        }
        return [
            'bool' => $pass['bool'],
            'message' => $pass['message'],
        ];
    }
}
