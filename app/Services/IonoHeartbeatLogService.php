<?php
namespace App\Services;
use App\Models\IonoHeartbeatLog;

class IonoHeartbeatLogService {
    public function create($data)
    {
        IonoHeartbeatLog::create($data);
    }

    public function delete($id)
    {
        IonoHeartbeatLog::find($id)->delete();
    }
}
