<?php
namespace App\Services;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Str;
use App\Helpers\APIResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserCollection;
use App\Helpers\UserCommon;
use App\Models\Community;
use App\Models\WebRelay;
use Illuminate\Support\Facades\Password;
use App\Models\CountryPhoneFormat;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Services\ScanLogService;
use App\S3ImageHelper;
use App\Services\HouseService;

class UserService extends UserRepository {

    use UserCommon;
    private $userContactService;
    protected $scanLogService;
    protected $houseService;
    private $notySrcv;

    public function __construct(User $user, UserContactService $userContactService, ScanLogService $scanLogService, HouseService $houseService,
                                NotificationService $notificationService)
    {
        parent::__construct($user);
        $this->userContactService = $userContactService;
        $this->scanLogService = $scanLogService;
        $this->houseService = $houseService;
        $this->notySrcv = $notificationService;
    }
    /**
     * get house hold members details
     * @param $userId
     * @param $limit
     * @param $search
     * @return mixed
     */
    public function getHouseHoldMemberDetail($userId,$limit,$search){
        // dd($userId,$limit,$search);
        // dd(parent::getUsersByHouseId($userId,$limit,$search));
        $result =  parent::getUsersByHouseId($userId,$limit,$search)['result'];

        return $result->except(auth()->id())->map(function ($row,$key){


            return [
                'id' => $row->id,
                'full_name' => $row->fullName,
                'formatted_phone_number' => numberFormat($row->phone_number, Str::start($row->dial_code, '+')),
                'profile_image_url' => $row->profileImageUrl,
                'can_manage_family' => $row->hasPermission('can_manage_family'),
                'can_send_passes' => $row->hasPermission('can_send_passes'),
                'allow_parental_control' => $row->hasPermission('allow_parental_control'),
                'is_license_locked' => $row->hasPermission('is_license_locked'),
                'sent_passes_count' => $row->passes()->where('pass_date', ">=", date('Y-m-d H:i:s'))->count(),
                'active_passes_count' => $row->passUsers()->whereHas('pass', function($pass){
                    $pass->where('pass_date', ">=", date('Y-m-d H:i:s'));
                })->where('is_scan','0')->count(),
                'archived_passes_count' => $row->passUsers()->whereHas('pass', function($pass){
                    $pass->where('pass_date', "<", date('Y-m-d H:i:s'));
                })->where('is_scan','1')->count(),
                'dial_code' => $row->dial_code
            ];
        });
    }

    /**
     * get house hold members details for Admin
     * @param $userId
     * @param $limit
     * @param $search
     * @return mixed
     */
    public function  getHouseHoldMemberDetailForAdmin($userId,$limit,$search,$communityId){
        $result =  parent::getUsersByHouseIdForAdmin($userId,$limit,$search,$communityId);
        if(!empty($result))
        {
            $result = $result['result'];
            return $result->except($userId)->map(function ($row,$key){
                return [
                    'id' => $row->id,
                    'full_name' => $row->fullName,
                    'formatted_phone_number' => $row->formattedPhone,
                    'profile_image_url' => $row->profileImageUrl,
                    'can_manage_family' => $row->hasPermission('can_manage_family'),
                    'can_send_passes' => $row->hasPermission('can_send_passes'),
                    'allow_parental_control' => $row->hasPermission('allow_parental_control'),
                    'is_license_locked' => $row->hasPermission('is_license_locked'),
                    'sent_passes_count' => $row->passes()->where('pass_date', ">=", date('Y-m-d H:i:s'))->count(),
                    'active_passes_count' => $row->passUsers()->whereHas('pass', function($pass){
                        $pass->where('pass_date', ">=", date('Y-m-d H:i:s'));
                    })->where('is_scan','0')->count(),
                    'archived_passes_count' => $row->passUsers()->whereHas('pass', function($pass){
                        $pass->where('pass_date', "<", date('Y-m-d H:i:s'));
                    })->where('is_scan','1')->count()
                ];
            });
        }
        else{
            return $result;
        }
    }

    /**
     * get house hold members count
     * @param $userId
     * @param $limit
     * @param $search
     * @return mixed
     */
    public function getHouseHoldMemberCount($userId,$limit,$search){
        $result =  parent::getUsersCountByHouseId($userId,$limit,$search)['result'];
        return $result;
    }

    public function getHouseHoldMemberDetailApi($house_id,$limit,$search){
        $result =  parent::getUsersByHouseIdApi($house_id,$limit,$search)['result'];
        return $result->except(auth()->id())->map(function ($row,$key){
            return [
                'id' => $row->id,
                'full_name' => $row->fullName,
                'formatted_phone_number' => $row->formattedPhone,
                'profile_image_url' => $row->profileImageUrl,
                'can_manage_family' => $row->hasPermission('can_manage_family'),
                'can_send_passes' => $row->hasPermission('can_send_passes'),
                'allow_parental_control' => $row->hasPermission('allow_parental_control'),
                'is_license_locked' => $row->hasPermission('is_license_locked'),
                'sent_passes_count' => $row->passes()->where('pass_date', ">=", date('Y-m-d H:i:s'))->count(),
                'active_passes_count' => $row->passUsers()->whereHas('pass', function($pass){
                    $pass->where('pass_date', ">=", date('Y-m-d H:i:s'));
                })->where('is_scan','0')->count(),
                'archived_passes_count' => $row->passUsers()->whereHas('pass', function($pass){
                    $pass->where('pass_date', "<", date('Y-m-d H:i:s'));
                })->where('is_scan','1')->count()
            ];
        });
    }


    /**
     * added user contact in all household members
     * @param $houseId
     */
    public function addUserContactForHouseHoldMembers($houseId){
        $users = self::getAllUsersByHouseId($houseId);
        if($users['result']->isNotEmpty()){
            foreach ($users['result'] as $user) {
                //add household members
                if($users['result']->except($user->id) ){
                    foreach ($users['result']->except($user->id) as $member){
                        //if($user1->id != $user->id ){echo $user1->id."<br/>";}
                        $userContact = $this->userContactService->getByIdAndPhoneNumber($user->id,$member->phone_number);
                        if(empty($userContact['result'])){
                            $this->userContactService->createOrUpdateUserContact([
                                'phone_number' => $member->phone_number,
                                'contact_name' => $member->fullName,
                                'email' => $member->email,
                            ],$user->id);
                        }
                    }
                }
            }
        }
    }

    // check user pass permission
    public function checkUserPassPermission($phone)
    {
        $phone = preg_replace('/\D/', '', $phone);
        // Find the user by phone number
        $user = parent::findByPhone($phone);

        // Check if the user exists
        if ($user['bool'] && $user['result'] != null) {
            $user = $user['result'];

            // Check if the user is a head of the family or has the "can_send_passes" permission
            if ($user->role->slug == 'head_of_family' || $user->hasPermission('can_send_passes')) {
                return APIResponse::success(['user' => $user]);
            } else {
                return APIResponse::error('User is not head of family and can not send passes');
            }
        } else {
            return APIResponse::error('Phone number does not exist in system');
        }
    }

    // save fcm token
    public function saveFcmToken($request)
    {
        $id = Auth::user()->id;
        if(!$request->has('token')){
            return APIResponse::error('Token Invalid or null');
        }
        $token = $request->token;
        $user = parent::find($id);
        if($user['result']){
            parent::update(['fcm_token' => $token], $id);
        }
        return APIResponse::success([],'Token updated');
    }

    // user signup
    public function userSignup($request)
    {
        $userData = $request->all();

        // validating the required fields
        $validation = Validator::make($userData, [
            'name' => 'required|string',
            'phone_number' => 'required',
            'password' => 'required|string',
            'dial_code' => 'required|string'
        ]);

        // if validation failed
        if ($validation->fails()) {
            return APIResponse::error($validation->errors()->first());
        }

        // get user
        $user = parent::findByPhone($userData['phone_number']);

        // if result is null
        if ($user['bool']) {
            if (!empty($user['result'])) {
                $user = $user['result'];
                if (
                    ($user->hasRole('guests_outsiders_daily') || $user->hasRole('guests_outsiders_one_time')) &&
                    (empty($user->password))
                ){
                    $names = $this->splitName($userData['name']);
                    $updateUser = parent::update([
                        'first_name' => $names["first_name"],
                        'middle_name' => $names["middle_name"],
                        'last_name' => $names["last_name"],
                        'password' => bcrypt($userData['password'])
                    ],$user->id);
                    if ($updateUser['bool']){
                        $user = parent::find($user->id)['result'];
                        return APIResponse::success([new UserCollection($user)], 'Signup Successful');
                    }
                    return APIResponse::error('Phone number already exist.');

                }
                return APIResponse::error('Phone number already exist.');
            } else {
                $names = $this->splitName($userData['name']);
                $data = [
                    'first_name' => $names["first_name"],
                    'middle_name' => $names["middle_name"],
                    'last_name' => $names["last_name"],
                    'email' => $request->email ?? '',
                    'phone_number' => $userData['phone_number'] ?? '',
                    'dial_code' => $userData['dial_code'],
                    'created_by' => Auth::id(),
                    'role_id' => 7,
                    'password' => $request->password ? bcrypt($userData['password']) : null,
                ];

                if ($request->image && !empty($request->image)) {
                    $vehicle_image = $request->image;
                    $vehicle_image = $this->deleteAllBetween("data:image", "base64,", $vehicle_image);
                    $vehicle_image = base64_decode($vehicle_image);
                    $imageName = md5(uniqid()).'.'.'png';
                    $path = public_path("images/vehicles/{$imageName}");
                    file_put_contents($path, $vehicle_image);
                    $data['image'] = "vehicles/".$imageName;
                }

                $user = parent::create($data);
                if (!$user) {
                    return APIResponse::error($user['message']);
                }

                return APIResponse::success([new UserCollection($user['result'])], 'Signup Successful');
            }
        }
    }

    // get user profile
    public function getUserProfile()
    {
        $id = Auth::user()->id;
        $user = parent::find($id);

        // formatting the number
        $format_number = numberFormat($user['result']->phone_number, Str::start($user['result']->dial_code, '+'));

        if($user['result']) {

            $community = Community::where('id', $user['result']->community_id)->where('web_relay_available',1)->first();

            if(!empty($community))
            {

                if(empty($user['result']->selected_web_relay))
                {
                    $webRelays = WebRelay::where('community_id',$user['result']->community_id)->first();
                    if(!empty($webRelays))
                    {
                        $userToUpdate = User::find($user['result']->id);
                        $userToUpdate->selected_web_relay = $webRelays->id;
                        $userToUpdate->save();

                        $user['result']->selected_web_relay = $webRelays->id;

                    }
                }

            }

            $usr = new UserCollection($user['result']);
            $userArray = collect($usr)->toArray();

            // adding formatted phone number key in response array
            $userArray = $userArray + ['formatted_phone' => $format_number];


            return APIResponse::success(['user' => $userArray] , 'Welcome to ZUUL Systems');
        }

        return APIResponse::error('Invalid User / credentials');
    }

    // forgot password
    public function userForgotPassword($request)
    {
        $number = $request->number;
        $email = $request->email;

        // checking the email in db
        $user = parent::findByEmail($email);

        // if the email not found
        if(!$user['result'])
        {
            return APIResponse::error('Email Does not exist in system');
        }

        // else
        $check_email_count = parent::findMultipleEmails($email);

        // if email is in more than one account
        if($check_email_count['result'] > 1)
        {

            $validate = parent::checkEmailAndPhone($email, $number);
            if($validate['bool'])
            {
                $credentials = ['email' => $user['result']->email];
                $response = Password::sendResetLink($credentials);
                return APIResponse::success('Password Reset link has been sent to email');
            } else {
                return APIResponse::error($validate['bool']);
            }

        } else {
            $credentials = ['email' => $user['result']->email];

            $response = Password::sendResetLink($credentials);

            return APIResponse::success('Password Reset link has been sent to email');
        }
    }

    // update password
    public function userUpdatePassword($request)
    {
        $validation = [
            'password' => 'required',
            'confirm_password' => 'required|min:6|required_with:password|same:password'
        ];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $password = bcrypt($request->password);

        $passwordUpdate = parent::update(['password' => $password, 'is_reset_password' => 0, 'temp_password' => null], auth()->user()->id);

        if($passwordUpdate['bool'])
        {
            return APIResponse::success('Password updated successfully');
        }

        return APIResponse::error('Something went wrong');
    }

    // check already use email
    public function checkEmailAlreadyInUse($request)
    {
        $data = $request->all();
        $email = isset($data["email"]) ? $data["email"] : null;

        if (!$email) {
            return APIResponse::error('Invalid / Empty Email');
        } else {
            $user = \Auth()->user();
            if ($user) {
                if ($email == $user->email) {
                    return APIResponse::success(['user_count' => 0],'User(s) Exist');
                }

                $user_count = parent::getByAttribute([
                    'password~!=' => null,
                    'email' => $email,
                    'id~!=' => $user->id,
                ]);
            } else {
                $user_count = parent::getByAttribute([
                    'password~!=' => null,
                    'email' => $email
                ]);
            }

            if ($user_count->count() > 0) {
                return APIResponse::success(['user_count' => $user_count->count()],'User(s) Exist');

            } else {
                return APIResponse::success(['user_count' => $user_count->count()],'No User(s) Exist');
            }

        }
    }

    // User become Resident
    public function userBecomeResident($request)
    {
        $user_id = $request->user_id;
        $user = parent::find($user_id);
        if ($user['result']){
            if ($user['result']->role_id == 1){
                return APIResponse::error('No User(s) Exist');
            }
            $user = parent::update([
                'community_id' =>  $request->community_id,
                'house_id' =>  $request->house_id,
                'role_id' => 5
            ],$user_id);
            if ($user['bool']){
                return APIResponse::success([],'Updated Successfully');
            }
            return APIResponse::error('Something went wrong');
        }
        return APIResponse::error('No User(s) Exist');
    }

    // get active country dial code
    public function getActiveCountryDialCodes()
    {
        $DialCodes = CountryPhoneFormat::all();

        $response = ['bool' => true, 'message' => 'Country Phone Format List','result'=>$DialCodes];
        return response()->json($response);
    }

    // update scan log
    public function updateScanLog($request, $id)
    {
        $scanLog = $this->scanLogService->find($id);
        if (empty($scanLog['result'])){
            return APIResponse::error('Something went wrong.Please try again.');
        }
        if (!$request->has('license_plate_image') && empty($request->license_plate_image)){
            return APIResponse::error('Please select license plate image');
        }

        $licensePlateImage = $request->license_plate_image;
        $scanLogData['license_plate_image'] = $licensePlateImage;
        $scanLogData['license_plate_image_type'] = 'firebase_url';
        $path = true;
        //check if url is base64
        if( base64_decode($licensePlateImage, true) == true ) {
            $license_plate_image = $this->delete_all_between("data:image", "base64,", $licensePlateImage);
            $compressLicensePlateImage = Image::make($license_plate_image)->stream(null,60);
            $licencePlateName = 'license_'.md5(uniqid()) . '.' . 'png';
            $filePath =  "dynamic/lic_images/".$licencePlateName;
            $path = Storage::disk('s3')->put($filePath, $compressLicensePlateImage);
            $scanLogData['license_plate_image'] = $filePath;
            $scanLogData['license_plate_image_type'] = 'base64';
        }
        if($path){
            if(Str::contains($scanLogData['license_plate_image'], "dynamic/lic_images") && !Str::contains($scanLogData['license_plate_image'], "https://firebasestorage.googleapis.com")) {
                $val1 = explode("?", $scanLogData['license_plate_image']);
                $cleanedString = str_replace('https://zuul-private-files.s3.us-east-2.amazonaws.com/', '', $val1[0]);
                $scanLogData['license_plate_image'] = $cleanedString;
            }

            $updateScanLog =  $this->scanLogService->update([
                'license_plate_image' => $scanLogData['license_plate_image'] ?? null,
                'license_plate_image_type' => $scanLogData['license_plate_image_type'] ?? null
            ],$scanLog['result']->id);
            if ($updateScanLog['bool']){

                $this->notySrcv->parentalNotificationSend($id);

                return APIResponse::success([],'Updated Successfully.');
            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        } else {
            return APIResponse::error('Something went wrong.Please try again.');
        }
    }

    // delete all between function (A private function which is used as a sub function in this file)
    private function delete_all_between(string $beginning, string $end, string $string)
    {
        return S3ImageHelper::delete_all_between($beginning, $end, $string);
    }

    // update user notification settings
    public function updateUserNotificationSettings($request)
    {
        $data = $request->all();

        $userData['push_notification'] = (isset($data['push_notification']) && $data['push_notification'] == '1') ? '1' : '0';
        $userData['email_notification'] = (isset($data['email_notification']) &&  $data['email_notification'] == '1') ? '1' : '0';
        $userData['vacation_mode'] = (isset($data['vacation_mode'])  &&  $data['vacation_mode'] == '1') ? '1' : '0';
        $result = parent::update($userData, auth()->user()->id);

        if ($result['bool']) {
            return APIResponse::success([], 'Notification settings updated');
        } else {
            return APIResponse::error('Something went wrong');
        }
    }

    // user profile
    public function userProfile($request)
    {
        // validating the required fields
        $validation = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required',
            'email' => 'required'
        ];


        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'phone_number.required' => 'Please select phone number',
            'email.required' => 'Please enter email address'
        ];

        $data['first_name'] = $request->first_name;
        $data['middle_name'] = $request->middle_name;
        $data['last_name'] = $request->last_name;
        $data['license_type'] = "driving_license";
        $data['phone_number'] = $request->phone_number;

        $data['street_address'] = $request->street_address;
        $data['apartment'] = $request->apartment;
        $data['city'] = $request->city;
        $data['state'] = $request->state;
        $data['zip_code'] = $request->zip_code;
        $data['dial_code'] = $request->dial_code;
        $data['date_of_birth'] = $request->date_of_birth;
        $data['email'] = $request->email;


        $validator = Validator::make($data, $validation, $messages);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }
        //upload profile image
        if ($request->has('profile_image') && !empty($request->profile_image) && !Str::contains($request->profile_image, "http://") && !Str::contains($request->profile_image, "https://")) {

            $profile_image = $request->profile_image;

            if (base64_decode($profile_image, true) == true) {
                // Check if folder exists
                if (!is_dir(public_path('images/profile_images'))) {
                    mkdir(public_path('images/profile_images'), 0777, true);
                }
                $profile_image = $this->delete_all_between("data:image", "base64,", $profile_image);
                $image = base64_decode($profile_image);
                $imageName = 'profile_' . md5(uniqid()) . '.' . 'png';
                $path = public_path("images/profile_images/{$imageName}");
                file_put_contents($path, $image);
                $data['profile_image'] = $imageName;
            }
        }

        //upload license image to s3 bucket
        if ($request->has('license_image') && !empty($request->license_image)) {

            $licence_image = $request->license_image;
            if (base64_decode($licence_image, true) == true) {

                $license_image = $this->delete_all_between("data:image", "base64,", $licence_image);


                $compressLicenseImage = Image::make($license_image)
                    ->orientate()
                    ->fit(600, 360, function ($constraint) {
                        $constraint->upsize();
                    })->stream(null, 60);

                $licenceName = 'license_' . md5(uniqid()) . '.' . 'png';
                $filePath =  "dynamic/lic_images/" . $licenceName;
                $path = Storage::disk('s3')->put($filePath, $compressLicenseImage);
                $data['license_image'] = $filePath;
            }

        }

        // unset fields
        unset($data['phone_number']);
        unset($data['dial_code']);

        $result = parent::update($data, auth()->user()->id);

        $houseCompletedProfileStatus = "no";
        $houseResidents = User::where('house_id', auth()->user()->house_id)->whereIn('role_id', [4, 5])->get();
        foreach ($houseResidents as $houseResident) {
            if (!empty($houseResident->license_image)) {
                $houseCompletedProfileStatus = "yes";
            }
        }

        $this->houseService->update(['completed_profile' => $houseCompletedProfileStatus], auth()->user()->house_id);

        if ($result['bool']) {
            $result = parent::find(auth()->user()->id)['result'];
            if ($result) {
                if($request->has('license_image') && !empty($request->license_image)){
                    $result->assignPermission('is_license_locked');
                }
            }
            return APIResponse::success([], 'User updated');
        }

        return APIResponse::error('Something went wrong');
    }

    // user temporary delete by self
    public function temporaryUserDeleteBySelf()
    {
        $user = parent::tempUserDeleteBySelf(auth()->user('api')->id);

        if ($user['bool']) {
            $response = ['bool' => true, 'message' => 'Delete Successfully'];
        } else {
            $response = ['bool' => false, 'message' => 'Something Went Wrong, Please try again'];
        }
        return response()->json($response);
    }

    // update user profile
    public function updateUserProfile($request, $id)
    {
        $user = parent::find($id);
        if (empty($user['result'])) {
            return APIResponse::error('No Record Found');
        }
        if ((!$request->has('scan_log_id')) || ($request->has('scan_log_id') && empty($request->scan_log_id))) {
            return APIResponse::error('ScanLog id required');
        }
        $scanLog = $this->scanLogService->find($request->scan_log_id);
        if (empty($scanLog['result'])) {
            return APIResponse::error('Something went wrong.Please try again.');
        }
        if (!$request->has('license_image') && empty($request->license_image)) {
            return APIResponse::error('Please select license image');
        }
        if (empty($request->license_type)) {
            return APIResponse::error('Please select license type');
        }
        $license_image = $request->license_image;
        if (base64_decode($license_image, true) == true) {

            $license_image = $this->delete_all_between("data:image", "base64,", $license_image);
            $compressLicenseImage = Image::make($license_image)->stream();
            $licenceName = 'license_' . md5(uniqid()) . '.' . 'png';
            $filePath =  "dynamic/lic_images/" . $licenceName;
            $path = Storage::disk('s3')->put($filePath, $compressLicenseImage);
            $data['license_image'] = $filePath;
            if ($path) {
                $userData['license_type'] = $request->license_type;
                $userData['license_image'] = $filePath;
                if (!empty($user->license_image)) {
                }
                $user = parent::update($userData, $user['result']->id);
                if ($user['bool']) {
                    $this->scanLogService->update([
                        'license_image' => $userData['license_image'],
                    ], $scanLog['result']->id);
                    return APIResponse::success([], 'Updated Successfully.');
                } else {
                    return APIResponse::error('Something went wrong.Please try again.');
                }
            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        } else {

            $license_image = $request->license_image;
            $compressLicenseImage = Image::make($license_image)->stream(null, 60);
            $licenceName = 'license_' . md5(uniqid()) . '.' . 'png';
            $filePath =  "dynamic/lic_images/" . $licenceName;
            $userData['license_type'] = $request->license_type;
            $userData['license_image'] = $filePath;
            $user = parent::update($userData, $user['result']->id);
            if ($user['bool']) {
                $this->scanLogService->update([
                    'license_image' => $userData['license_image'],
                ], $scanLog['result']->id);
                return APIResponse::success([], 'Updated Successfully.');
            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        }
    }

    // check phone existence
    public function checkPhoneExistence($userPhone)
    {
        $user = parent::findByPhone($userPhone->phone);
        if ($user['bool']) {
            $user = $user['data'];
            if (!is_null($user)) {
                if ($user->role->slug = 'head_of_family' || $user->hasPermission('can_send_passes')) {
                    return APIResponse::success(['user' => $user]);
                } else {
                    return APIResponse::error('User is not head
                    of family and can not send passes');
                }
            } else {
                return APIResponse::error('Phone number does not exist in system');
            }
        } else {
            return APIResponse::error($user['message']);
        }
    }

    // update user profile by file
    public function updateUserProfileByFile($request, $id)
    {
        $user = parent::find($id);
        if (empty($user['result'])) {
            return APIResponse::error('No Record Found');
        }
        if ((!$request->has('scan_log_id')) || ($request->has('scan_log_id') && empty($request->scan_log_id))) {
            return APIResponse::error('ScanLog id required');
        }
        $scanLog = $this->scanLogService->find($request->scan_log_id);
        if (empty($scanLog['result'])) {
            return APIResponse::error('Something went wrong.Please try again.');
        }
        if (!$request->has('license_image') && empty($request->license_image)) {
            return APIResponse::error('Please select license image');
        }
        if (empty($request->license_type)) {
            return APIResponse::error('Please select license type');
        }
        $license_image = $request->license_image;
        if (base64_decode($license_image, true) == true) {

            $license_image = $this->delete_all_between("data:image", "base64,", $license_image);
            $compressLicenseImage = Image::make($license_image)->stream();
            $licenceName = 'license_' . md5(uniqid()) . '.' . 'png';
            $filePath =  "dynamic/lic_images/" . $licenceName;
            $path = Storage::disk('s3')->put($filePath, $compressLicenseImage);
            $data['license_image'] = $filePath;
            if ($path) {
                $userData['license_type'] = $request->license_type;
                $userData['license_image'] = $filePath;
                if (!empty($user->license_image)) {
                }
                $user = parent::update($userData, $user['result']->id);
                if ($user['bool']) {
                    $this->scanLogService->update([
                        'license_image' => $userData['license_image'],
                    ], $scanLog['result']->id);
                    return APIResponse::success([], 'Updated Successfully.');
                } else {
                    return APIResponse::error('Something went wrong.Please try again.');
                }
            } else {
                return APIResponse::error('Something went wrong.Please try again.');
            }
        } else {

            $license_image = $request->license_image;
            $compressLicenseImage = Image::make($license_image)->stream(null, 60);
            $licenceName = 'license_' . md5(uniqid()) . '.' . 'png';
            $filePath =  "dynamic/lic_images/" . $licenceName;
            $userData['license_type'] = $request->license_type;
            $userData['license_image'] = $filePath;
            $user = parent::update($userData, $user['result']->id);
            if ($user['bool']) {
                $this->scanLogService->update([
                    'license_image' => $userData['license_image'],
                ], $scanLog['result']->id);
                return APIResponse::success([], 'Updated Successfully.');
            } else {
                return APIResponse::error('Something went wrong. Please try again.');
            }
        }
    }
}
