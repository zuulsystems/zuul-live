<?php

namespace App\Services;

use App\Models\Contact;
use App\Repositories\UserContactRepository;

class UserContactService extends UserContactRepository
{

    /**
     * get contacts details
     *
     * @param      $userId
     * @param      $limit
     * @param null $search
     *
     * @param      $isFavourite
     *
     * @return mixed
     */
    public function getContactList($userId, $limit, $search = null, $isFavourite = null, $isDnc = null)
    {
        if(parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['bool']){

            $result = parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['result'];

            return $result->map(function ($row, $key) {

                $formatted_phone = "";
                if($row->contact->dial_code == '+504'){
                    $formatted_phone = DialCodeFormattedPhone($row->contact->phone_number);
                }else if($row->contact->dial_code == '+1'){
                    $formatted_phone = ($row->contact()->exists()) ? $row->contact->formattedPhone : "";
                }

                return [
                    'id' => $row->id,
                    'contact_name' => $row->contact_name,
                    'email' => $row->email,
                    'formatted_phone_number' => $formatted_phone,
                    'dial_code' => $row->contact->dial_code,
                    'is_favourite' => $row->is_favourite,
                    'is_temporary' => $row->is_temporary,
                    'temporary_duration' => $row->temporary_duration,
                    'created_by' => $row->created_by,
                ];
            });
        }
        return [];
    }

    /**
     * get contacts details for admin
     *
     * @param      $userId
     * @param      $limit
     * @param null $search
     *
     * @param      $isFavourite
     *
     * @return mixed
     */
    public function getContactListForAdmin($userId, $limit, $search = null, $isFavourite = null, $isDnc = null, $communityId = null)
    {
        if(parent::getByUserIdAdmin($userId, $limit, $search, $isFavourite, $isDnc, $communityId)['bool']){
            $result = parent::getByUserIdAdmin($userId, $limit, $search, $isFavourite, $isDnc, $communityId)['result'];

            return $result->map(function ($row, $key) {
                return [
                    'id' => $row->id,
                    'contact_name' => $row->contact_name,
                    'email' => $row->email,
                    'formatted_phone_number' => ($row->contact()->exists()) ? $row->contact->formattedPhone : "",
                    'dial_code' => $row->contact->dial_code,
                    'is_favourite' => $row->is_favourite,
                    'is_temporary' => $row->is_temporary,
                    'temporary_duration' => $row->temporary_duration,
                    'created_by' => $row->created_by,
                ];
            });
        }
        return [];
    }
    /**
     * get contacts count
     *
     * @param      $userId
     * @param      $limit
     * @param null $search
     *
     * @param      $isFavourite
     *
     * @return mixed
     */
    public function getContactListCount($userId, $limit, $search = null, $isFavourite = null, $isDnc = null)
    {
        if(parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['bool']){
            $result = parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['result']->count();
            return $result;
        }
        return [];
    }

    /**
     * get contacts dnc count
     *
     * @param      $userId
     * @param      $limit
     * @param null $search
     *
     * @param      $isFavourite
     *
     * @return mixed
     */
    public function getContactListDncCount($userId, $limit, $search = null, $isFavourite = null, $isDnc = null)
    {
        if(parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['bool']){
            $result = parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['result']->count();
            return $result;
        }
        return [];
    }

    public function getContactListApi($userId, $limit, $search = null, $isFavourite = null, $isDnc = null)
    {
        if(parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['bool']){
            $result = parent::getByUserId($userId, $limit, $search, $isFavourite, $isDnc)['result'];

            return $result->map(function ($row, $key) {
                return [
                    'id' => $row->id,
                    'contact_name' => $row->contact_name,
                    'email' => $row->email,
                    'formatted_phone_number' => ($row->contact()->exists()) ? $row->contact->formattedPhone : "",
                    'dial_code' => $row->contact->dial_code,
                    'is_favourite' => $row->is_favourite,
                    'is_temporary' => $row->is_temporary,
                    'temporary_duration' => $row->temporary_duration,
                    'created_by' => $row->created_by,
                ];
            });
        }
        return [];
    }



    public function getSharedDncContactsByUserIds($userIds){

        $result = parent::getSharedDncContacts($userIds)['result'];

        return $result->map(function ($row, $key) {
            return [
                'id' => $row->id,
                'contact_name' => $row->contact_name,
                'email' => $row->email,
                'formatted_phone_number' => ($row->contact()->exists()) ? $row->contact->formattedPhone : "",
                'dial_code' => (!empty($this->contact)) ? $this->contact->dial_code : "",
                'is_favourite' => $row->is_favourite,
                'is_temporary' => $row->is_temporary,
                'temporary_duration' => $row->temporary_duration,
                'created_by' => $row->created_by,
            ];
        });
    }

    /**
     * create user contact
     *
     * @param array $data
     *
     * @param null $createdBy
     * @return mixed|void
     */
    public function createOrUpdateUserContact($data,$createdBy = null)
    {

        $createdBy  = (!empty($createdBy))? $createdBy: auth()->id();

        $isFromPreApproved = isset($data['is_from_pre_approved']) ? $data['is_from_pre_approved'] : false;
        //check if user contact is temporary
        if (isset($data['is_temporary']) && $data['is_temporary'] == '1') {
            $data['is_temporary'] = '1';
            $data['expire_date'] = now()->addHours($data['temporary_duration']);
        } else {
            $data['is_temporary'] = '0';
            $data['temporary_duration'] = null;
            $data['expire_date'] = null;
        }

        if(isset($data['id_dnc'])){

        }


        try {
            //check if contact exist or not
            if($isFromPreApproved){


                $phoneNumber;

                if($data['dial_code'] != 504)
                {
                    $phoneNumber = $data['phone_number'] ?? '1111111111';
                }
                else
                {
                    $phoneNumber = $data['phone_number'] ?? '11111111';
                }


                $contact = Contact::firstOrCreate([
                    'phone_number' => $phoneNumber,
                    'dial_code' => $data['dial_code']
                ], [
                    'dial_code' => $data['dial_code'],
                ]);

                $userContact = self::updateOrCreate([
                    'created_by' => $createdBy,
                    'contact_id' => $contact->id,
                    'contact_name' => $data['contact_name'],
                ], [
                    'email' => $data['email'] ?? "",
                    'is_temporary' => $data['is_temporary'],
                    'temporary_duration' => $data['temporary_duration'],
                    'expire_date' => $data['expire_date'],
                    'is_dnc' => isset($data['is_dnc']) ? $data['is_dnc'] : 0,
                    'is_dnc_shared' => isset($data['is_dnc_shared']) ? $data['is_dnc_shared'] : 0
                ]);

            }else{
                $dial_code="";

                if(strlen($data['phone_number']) == 10) {
                    $dial_code = "+1";
                } else if(strlen($data['phone_number']) == 8) {
                    $dial_code = "+504";
                }

                $contact = Contact::firstOrCreate([
                    'phone_number' => $data['phone_number'],
                ], [
                    'dial_code' => $dial_code,
                ]);

                $userContact = self::updateOrCreate([
                    'created_by' => $createdBy,
                    'contact_id' => $contact->id,
                ], [
                    'contact_name' => $data['contact_name'],
                    'email' => $data['email'] ?? "",
                    'is_temporary' => $data['is_temporary'],
                    'temporary_duration' => $data['temporary_duration'],
                    'expire_date' => $data['expire_date'],
                    'is_dnc' => isset($data['is_dnc']) ? $data['is_dnc'] : 0,
                    'is_dnc_shared' => isset($data['is_dnc_shared']) ? $data['is_dnc_shared'] : 0
                ]);

            }

            return $userContact;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * update user contact
     *
     * @param $data
     * @param $id
     *
     * @return bool
     */
    public function updateUserContact($data, $id)
    {
        //check if user contact is temporary
        if (isset($data['is_temporary']) && $data['is_temporary'] == '1') {
            $data['is_temporary'] = '1';
            $data['expire_date'] = now()->addHours($data['temporary_duration']);
        } else {
            $data['is_temporary'] = '0';
            $data['temporary_duration'] = null;
            $data['expire_date'] = null;
        }

        try {
            //check if contact exist or not
            $contact = Contact::firstOrCreate(
                [
                    'phone_number' => $data['phone_number'],
                ],
                [
                    'dial_code' => $data['dial_code'],
                ]
            );
            // update user_contact
            $userContact =  self::update([
                'contact_id' => $contact->id,
                'contact_name' => $data['contact_name'],
                'email' => $data['email'] ?? "",
                'is_temporary' => $data['is_temporary'],
                'temporary_duration' => $data['temporary_duration'],
                'expire_date' => $data['expire_date'],
            ], $id);

            return $userContact;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
