<?php

namespace App\Services;

use App\Helpers\PushNotificationTrait;
use App\Helpers\UserCommon;
use App\Models\PassRequest;
use App\Repositories\NotificationRepository;
use App\Repositories\ParentalControlRespository;
use App\Repositories\PassRequestRepository;
use App\Repositories\UserContactRepository;
use App\Repositories\UserRepository;

class PassRequestService extends PassRequestRepository
{
    use PushNotificationTrait, UserCommon;
    protected $notificationRepo;
    protected $userRepo;
    protected $userContactRepo;
    protected $parentalControlRepo;

    public function __construct(
        NotificationRepository $notificationRepo,
        UserRepository $userRepo,
        UserContactRepository $userContactRepo,
        ParentalControlRespository $parentalControlRepo
    )
    {
        parent::__construct(new PassRequest);
        $this->notificationRepo = $notificationRepo;
        $this->userRepo = $userRepo;
        $this->userContactRepo = $userContactRepo;
        $this->parentalControlRepo = $parentalControlRepo;
    }

    /**
     * send request a pass
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function sendPassRequest($data)
    {
        try {
            $requestedUser = $this->userRepo->find($data['requested_user_id'])['result'];
            $sender = auth()->user();

            //checking If requested user have permission to send pass
            if (!$this->canSendPasses($requestedUser)) {
                return ['bool' => false, 'message' => 'The person you are requesting a pass from, is not approved to issue you one'];
            }

            $isBlackList = $this->parentalControlRepo->checkMemberBlackList($data['requested_user_id']);

            if ($isBlackList['result']) {
                return ['bool' => false, 'message' => 'The person you are requesting a pass from, is not approved to issue you one'];
            }

            //create pass request
            $requestPassData = [
                'request_user_id' => $data['requested_user_id'],
                'send_user_id' => $sender->id,
                'name' => $sender->fullName,
                'email' => $sender->email,
                'phone' => $sender->phone_number,
                'description' => $data['description']
            ];
            $passRequest = self::create($requestPassData);
            if (!$passRequest['bool']) {
                return $passRequest;
            }

            //==================== Notifications =====================
            // create notification entry
            $notificationData = [
                'user_id' => $data['requested_user_id'],
                'type' => 'zuul',
                'heading' => "Pass Request",
                'subheading' => 'Sent by ' . $sender->fullName,
                'text' => $sender->fullName . " is requested a pass invitation: " . $data['description'],
                'category' => 'send_request_for_pass',
                'pass_request_id' => $passRequest['result']->id
            ];
            $notificationData = $this->notificationRepo->create($notificationData);
            /* Sent Push notification to requested User */
            $push = array(
                'description' => $data['description'],
                'title' => 'Pass Requested',
                'nid' => $notificationData['result']->id,
                'nid_type' => 'regular'
            );
            $push = $this->sendPushNotificationToUser($push, $requestedUser);

            /* End Notification */

            // parental control notifications
            $notificationData = [
                'parent_id' => $sender->id,
                'user_id' => $data['requested_user_id'],
                'type' => 'parental_control',
                'heading' => "Pass Request",
                'subheading' => 'Sent by ' . $sender->fullName,
                'text' => $sender->fullName . " is requested a pass invitation: " . $data['description'],
                'category' => 'pass_request_from_user_to_resident',
                'pass_request_id' => $passRequest['result']->id
            ];

            $this->notificationRepo->create($notificationData);

            // Add the notification if parent needed to be notified
            // Send push notification when Requested User received a Pass Request
            $parentalControl = checkParentalControlByMember('notify_when_member_request_a_pass', $data['requested_user_id']);
            if ($parentalControl) {
                foreach ($parentalControl as $row) {
                    $parentalControlNotification = [
                        'parent_id' => $row->parent_id,
                        'user_id' => $data['requested_user_id'],
                        'pass_request_id' => $passRequest['result']->id,
                        'type' => 'parental_control',
                        'category' => "pass_request_received_by_member_notify_parents",
                        'heading' => "Pass request received in your family",
                        'subheading' => 'Received by ' . $requestedUser->fullName,
                        'text' => $sender->fullName . " is requested a pass invitation: {$data["description"]}, to {$requestedUser->fullName}"

                    ];
                    $parentalControlNotificationData = $this->notificationRepo->create($parentalControlNotification);

                    /* Push Notification */
                    $push = array(
                        'description' => $sender->fullName . " is requested a pass invitation: {$data['description']}, to {$requestedUser->fullName}",
                        'title' => "Pass request received in your family",
                        'nid' => $parentalControlNotificationData['result']->id,
                        'nid_type' => 'parental',
                        'link' => url('/nd/' . $parentalControlNotificationData['result']->id),
                        'info' => 'Sent by ' . $sender->fullName
                        //'info' => self::success('Sent by ' . $sender->fullName, [$type27])->getData()
                    );
                    //$this->sendPushNotification($push, true, $parent);
                    $this->sendPushNotificationToUser($push, $row->parentUser);
                }
            }

            // Add the notification if parent needed to be notified
            // Send push notification when Sent User sent a Pass Request to Someone
            $parentalControl = checkParentalControlByMember('notify_when_member_request_a_pass', $sender->id);
            if ($parentalControl) {
                foreach ($parentalControl as $row) {
                    $parentalControlNotification = [
                        'parent_id' => $row->parent_id,
                        'user_id' => $sender->id,
                        'pass_request_id' => $passRequest['result']->id,
                        'type' => 'parental_control',
                        'category' => "pass_request_from_user_to_resident_to_parents",
                        'heading' => "Pass request sent from your family",
                        'subheading' => 'Sent by ' . $sender->fullName,
                        'text' => "$sender->fullName is requested a pass invitation: {$data['description']}"
                    ];
                    $parentalControlNotificationData = $this->notificationRepo->create($parentalControlNotification);

                    /* Push Notification */
                    $push = array(
                        'description' => "$sender->fullName is requested a pass invitation: {$data['description']}",
                        'title' => "Pass request sent from your family",
                        'nid' => $parentalControlNotificationData['result']->id,
                        'nid_type' => 'parental',
                        'link' => url('/nd/' . $parentalControlNotificationData['result']->id),
                        'info' => 'Sent by ' . $sender->fullName
                        //'info' => self::success('Sent by ' . $sender->fullName, [$type27])->getData()
                    );
                    $this->sendPushNotificationToUser($push, $row->parentUser);
                }
            }
            //=========================== End Notifications =========================
            return $passRequest;

        } catch (\Exception $exception) {
            return ['bool' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function rejectPassRequest($data)
    {
        try {
            $passRequest = self::find($data['pass_request_id'])['result'];
            if (empty($passRequest)) {
                return ['bool' => false, 'message' => 'No Record Found'];
            }
            $result = self::update(['status' => 'rejected'], $passRequest->id);
            if (!$result['bool']) {
                return ['bool' => false, 'message' => $result['message']];
            }
            /* Sent Push notification to Sent User */
            $push = array(
                'description' => "Pass Request for " . $passRequest->description . " has been rejected",
                'title' => 'Warning! Rejected',
                'nid' => null,
                'info' => ''
            );
            // Sent user id who sent request of pass
            $sentUser = $this->userRepo->find($passRequest->send_user_id);
            if($sentUser['result']){
                $this->sendPushNotificationToUser($push, $sentUser['result']);
            }
            /*End Notification*/

            $this->notificationRepo->delete($data['id']);

            return ['bool' => true, 'message' => 'Rejected Successfully.'];
        } catch (\Exception $exception) {
            return ['bool' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function acceptPassRequest($data)
    {
        try {
            $passRequest = self::find($data['pass_request_id'])['result'];
            if (empty($passRequest)) {
                return ['bool' => false, 'message' => 'Something went wrong.Please try again'];
            }
            $result = self::update(['status' => 'approved'], $passRequest->id);
            if (!$result['bool']) {
                return ['bool' => false, 'message' => $result['message']];
            }

            /* Sent Push notification to Sent User */
            $push = array(
                'description' => "",
                'title' => 'Pass Accepted',
                'nid' => null
            );
            // Sent user id who sent request of pass
            $sentUser = $this->userRepo->find($passRequest->send_user_id);
            if($sentUser['result']){
                $this->sendPushNotificationToUser($push, $sentUser['result']);
            }
            /*End Notification*/

            return ['bool' => true, 'message' => 'Pass Accepted Successfully.'];
        } catch (\Exception $exception) {
            return ['bool' => false, 'message' => $exception->getMessage()];
        }
    }

    /**
     * send request a pass
     * @param $data
     * @param $senderId
     * @return array
     */
    public function sendPassRequestAnonymous($data,$senderId)
    {
        try {
            $requestedUser = $this->userRepo->find($data['requested_user_id'])['result'];
            $sender = $this->userRepo->find($senderId)['result'];

            //checking If requested user have permission to send pass
            if (!$this->canSendPasses($requestedUser)) {
                return ['bool' => false, 'message' => 'The person you are requesting a pass from, is not approved to issue you one'];
            }

            $isBlackList = $this->parentalControlRepo->checkMemberBlackList($data['requested_user_id']);

            if ($isBlackList['result']) {
                return ['bool' => false, 'message' => 'The person you are requesting a pass from, is not approved to issue you one'];
            }

            //create pass request
            $requestPassData = [
                'request_user_id' => $data['requested_user_id'],
                'send_user_id' => $sender->id,
                'name' => $sender->fullName,
                'email' => $sender->email,
                'phone' => $sender->phone_number,
                'description' => $data['description'],
                'anonymous' => '1'

            ];
            $passRequest = self::create($requestPassData);
            if (!$passRequest['bool']) {
                return $passRequest;
            }

            //==================== Notifications =====================
            // create notification entry
            $notificationData = [
                'user_id' => $data['requested_user_id'],
                'type' => 'zuul',
                'heading' => "Pass Request",
                'subheading' => 'Sent by ' . $sender->fullName,
                'text' => $sender->fullName . " is requested a pass invitation: " . $data['description'],
                'category' => 'send_request_for_pass',
                'pass_request_id' => $passRequest['result']->id
            ];
            $notificationData = $this->notificationRepo->create($notificationData);
            /* Sent Push notification to requested User */
            $push = array(
                'description' => $data['description'],
                'title' => 'Pass Requested',
                'nid' => $notificationData['result']->id,
                'nid_type' => 'regular'
            );
            $push = $this->sendPushNotificationToUser($push, $requestedUser);

            /* End Notification */

            // parental control notifications
            $notificationData = [
                'parent_id' => $sender->id,
                'user_id' => $data['requested_user_id'],
                'type' => 'parental_control',
                'heading' => "Pass Request",
                'subheading' => 'Sent by ' . $sender->fullName,
                'text' => $sender->fullName . " is requested a pass invitation: " . $data['description'],
                'category' => 'pass_request_from_user_to_resident',
                'pass_request_id' => $passRequest['result']->id
            ];

            $this->notificationRepo->create($notificationData);

            // Add the notification if parent needed to be notified
            // Send push notification when Requested User received a Pass Request
            $parentalControl = checkParentalControlByMember('notify_when_member_request_a_pass', $data['requested_user_id']);
            if ($parentalControl) {
                foreach ($parentalControl as $row) {
                    $parentalControlNotification = [
                        'parent_id' => $row->parent_id,
                        'user_id' => $data['requested_user_id'],
                        'pass_request_id' => $passRequest['result']->id,
                        'type' => 'parental_control',
                        'category' => "pass_request_received_by_member_notify_parents",
                        'heading' => "Pass request received in your family",
                        'subheading' => 'Received by ' . $requestedUser->fullName,
                        'text' => $sender->fullName . " is requested a pass invitation: {$data["description"]}, to {$requestedUser->fullName}"

                    ];
                    $parentalControlNotificationData = $this->notificationRepo->create($parentalControlNotification);

                    /* Push Notification */
                    $push = array(
                        'description' => $sender->fullName . " is requested a pass invitation: {$data['description']}, to {$requestedUser->fullName}",
                        'title' => "Pass request received in your family",
                        'nid' => $parentalControlNotificationData['result']->id,
                        'nid_type' => 'parental',
                        'link' => url('/nd/' . $parentalControlNotificationData['result']->id),
                        'info' => 'Sent by ' . $sender->fullName
                    );
                    $this->sendPushNotificationToUser($push, $row->parentUser);
                }
            }

            // Add the notification if parent needed to be notified
            // Send push notification when Sent User sent a Pass Request to Someone
            $parentalControl = checkParentalControlByMember('notify_when_member_request_a_pass', $sender->id);
            if ($parentalControl) {
                foreach ($parentalControl as $row) {
                    $parentalControlNotification = [
                        'parent_id' => $row->parent_id,
                        'user_id' => $sender->id,
                        'pass_request_id' => $passRequest['result']->id,
                        'type' => 'parental_control',
                        'category' => "pass_request_from_user_to_resident_to_parents",
                        'heading' => "Pass request sent from your family",
                        'subheading' => 'Sent by ' . $sender->fullName,
                        'text' => "$sender->fullName is requested a pass invitation: {$data['description']}"
                    ];
                    $parentalControlNotificationData = $this->notificationRepo->create($parentalControlNotification);

                    /* Push Notification */
                    $push = array(
                        'description' => "$sender->fullName is requested a pass invitation: {$data['description']}",
                        'title' => "Pass request sent from your family",
                        'nid' => $parentalControlNotificationData['result']->id,
                        'nid_type' => 'parental',
                        'link' => url('/nd/' . $parentalControlNotificationData['result']->id),
                        'info' => 'Sent by ' . $sender->fullName
                        //'info' => self::success('Sent by ' . $sender->fullName, [$type27])->getData()
                    );
                    $this->sendPushNotificationToUser($push, $row->parentUser);
                }
            }
            //=========================== End Notifications =========================
            return $passRequest;

        } catch (\Exception $exception) {
            return ['bool' => false, 'message' => $exception->getMessage()];
        }
    }
}
