<?php

namespace App\Services;

use App\Helpers\PushNotificationTrait;
use App\Models\Notification;
use App\Models\PassUser;
use App\Models\Pass;
use App\Models\ScanLog;
use App\Models\User;
use App\Repositories\NotificationRepository;
use App\Repositories\PassUserRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class NotificationService extends NotificationRepository
{
    use PushNotificationTrait;
    private $userRepo;
    private $passUserRepo;

    public function __construct(
        Notification $notification,
        UserRepository $userRepo,
        PassUserRepository $passUserRepo
    )
    {
        parent::__construct($notification);
        $this->userRepo = $userRepo;
        $this->passUserRepo = $passUserRepo;
    }


    public function parentalNotificationSend($id){
        $scanLog = ScanLog::where('id',$id)->first();
        $PassUser = PassUser::where('id',$scanLog->pass_user_id)->first();
        $pass = Pass::where('id',$PassUser->pass_id)->first();
        $createdBy = User::where('id',$pass->created_by)->first();
        $guestCount = $pass->addedUsers->count();
        $eventName =  $pass->event->name ?? "";

        // New Logic Here
        $parentalControl = getParentalControlMembers('notify_when_member_receive_a_pass',$createdBy);
        if($parentalControl){
            foreach ($parentalControl as $row){

                $usser = isset($pass->addedUsers) ? $pass->addedUsers[0]->id : 0;
                $usser_name = isset($pass->addedUsers) ? $pass->addedUsers[0]->fullName : "";
                $guard_display_name = $scanLog->guard_display_name ?? "";

                if($pass->addedUsers[0]->community_id == 64){

                    $date = Carbon::now();

                    $date->subHours(2);

                    $date = $date->format('Y-m-d H:i:s');

                }else{
                    $date = Carbon::now()->format('h:i A M jS Y');
                }

                $parentalControlNotification = [
                    'parent_id' => $row->parent_id,
                    'user_id' => $row->member_id,
                    'pass_id' => $pass->id,
                    'event_id' => $pass->event_id,
                    'type' => 'parental_control',
                    'category' => "new_pass_sent_from_user_to_parents",
                    'heading' => "Guest Arrived",
                    'subheading' => 'Sent by ' . $createdBy->fullName,
                    'text' => $usser.' '.$guard_display_name.' has now entered the community, arrival time was '.$date
                ];


                $parentalControlNotification = self::create($parentalControlNotification);

                /* Push Notification */
                $push = array(
                    'description' => $usser.' '.$guard_display_name.' has now entered the community, arrival time was '.$date,
                    'title' => 'Guest Arrived',
                    'nid' => $parentalControlNotification['result']->id,
                    'nid_type' => 'parental',
                    'link' => url('/nd/'.$parentalControlNotification['result']->id),
                    'info' => 'Sent by ' . $createdBy->fullName
                    //'info' => self::success('Send by ' . $sender->fullName, [$type16])->getData()
                );
                $this->sendPushNotificationToUser($push,$row->parentUser);
                /* End Push Notification */
            }
        }
    }

    /**
     * Send notification for new pass sent
     *
     * @param $pass
     * @param $createdBy
     * @return array
     */
    public function notifyGuestsAfterNewPassSent($pass, $createdBy)
    {
        try {
            if(empty($pass)){
                return [
                    'bool' => false,
                    'message' => 'Pass Not Found'
                ];
            }
            //create notification for pass users
            if($pass->addedUsers->isNotEmpty()){
                $eventName =  $pass->event->name ?? "";
                $guestCount = $pass->addedUsers->count();
                $sendSms = true;
                //check if pass send by guard
                if ($pass->is_quick_pass == '1'){
                    $sendSms = true;
                }
                //dd($pass->addedUsers);
                foreach ($pass->addedUsers as $passUser){



                    // create a regular push notification entry
                    $zuulNotificationData = [
                        'pass_id' => $pass->id,
                        'event_id' => $pass->event_id,
                        'user_id' => $passUser->id,
                        'type' => "zuul",
                        'category' => "send_pass",
                        'heading' => 'New Pass Received',
                        'subheading' => 'Send by ' . $createdBy->fullName,
                        'text' => "$createdBy->fullName is invited you to $eventName on $pass->description"
                    ];

                    $zuulNotification = self::create($zuulNotificationData);

                    if($zuulNotification['bool']){
                        /* Push Notification */
                        $push = array(
                            'description' => "$createdBy->fullName is invited you to $eventName on $pass->description",
                            'title' => 'New Pass Received',
                            'nid' => $zuulNotification['result']->id,
                            'nid_type' => 'regular',
                            'link' => url('/nd/'.$zuulNotification['result']->id),
                            'info' => "Send by ' . $createdBy->fullName"
                        );

                        // dont' delete commit this line for now ...
                        $this->sendPushNotificationToUser($push,$passUser,$sendSms);
                    }


                    // add the notification if parent needed to be notified
                    $parentalControl = checkParentalControlByMember('notify_when_member_receive_a_pass',$passUser->id);
                    if($parentalControl){
                        foreach ($parentalControl as $row){

                            if($passUser->community_id == 64){

                                $date = Carbon::now();

                                $date->subHours(2);

                                $date = $date->format('Y-m-d H:i:s');

                            }else{
                                $date = Carbon::now()->format('h:i A M jS Y');
                            }

                            // type - 17: new_pass_sent_from_user_to_parents
                            $parentalControlNotification = [
                                'parent_id' => $row->parent_id,
                                'user_id' => $passUser->id,
                                'pass_id' => $pass->id,
                                'event_id' => $pass->event_id,
                                'type' => 'parental_control',
                                'category' => "new_pass_sent_from_user_to_parents",
                                'heading' => "Guest Arrived",
                                'subheading' => 'Received by ' . $passUser->fullName,
                                'text' => $passUser->user_id.' '.$passUser->fullName.' has now entered the community, arrival time was '.$date
                            ];
                            $parentalControlNotification =  self::create($parentalControlNotification);

                            /* Push Notification */
                            $push = array(
                                'description' => "Invited by $createdBy->fullName, Pass for Invitation on $eventName for $pass->description ",
                                'title' => 'New pass received in family',
                                'nid' => $parentalControlNotification['result']->id,
                                'nid_type' => 'parental',
                                'link' => url('/nd/'.$parentalControlNotification['result']->id),
                                'info' => "Received by ' . $passUser->fullName"
                            );
                            $this->sendPushNotificationToUser($push,$row->parentUser);
                            /* End Push Notification */
                        }
                    }
                }

                // add the notification if parent needed to be notified
                $parentalControl = checkParentalControlByMember('notify_when_member_send_pass_to_others',$createdBy->id);
                if($parentalControl){
                    foreach ($parentalControl as $row){
                        // type - 17: new_pass_sent_from_user_to_parents
                        $parentalControlNotification = [
                            'parent_id' => $row->parent_id,
                            'user_id' => $createdBy->id,
                            'pass_id' => $pass->id,
                            'event_id' => $pass->event_id,
                            'type' => 'parental_control',
                            'category' => "new_pass_sent_from_user_to_parents",
                            'heading' => "Guest Arrived",
                            'subheading' => 'Sent by ' . $createdBy->fullName,
                            'text' => $pass->passUser->user_id.' '.$pass->passUser->fullName.' has now entered the community, arrival time was '.Carbon::now()->format('h:i A M jS Y')
                        ];
                        $parentalControlNotification = self::create($parentalControlNotification);

                        /* Push Notification */
                        $push = array(
                            'description' => "$guestCount Guest(s) Invited: on $eventName for $pass->description",
                            'title' => 'New pass sent from your family',
                            'nid' => $parentalControlNotification['result']->id,
                            'nid_type' => 'parental',
                            'link' => url('/nd/'.$parentalControlNotification['result']->id),
                            'info' => 'Sent by ' . $createdBy->fullName
                            //'info' => self::success('Send by ' . $sender->fullName, [$type16])->getData()
                        );
                        $this->sendPushNotificationToUser($push,$row->parentUser);
                        /* End Push Notification */
                    }
                }
            }
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Send notification for update pass
     * @param $pass
     * @param $createdBy
     * @return array
     */
    public function notifyGuestsAfterNewPassUpdated($pass, $createdBy)
    {
        try {
            if(empty($pass)){
                return [
                    'bool' => false,
                    'message' => 'Pass Not Found'
                ];
            }
            //create notification for pass users
            if($pass->addedUsers->isNotEmpty()){
                $eventName =  $pass->event->name ?? "";
                $guestCount = $pass->addedUsers->count();
                foreach ($pass->addedUsers as $passUser){
                    // add a notification in pc for guest pass scan
                    $PcNotificationData = [
                        'parent_id' => $createdBy->id,
                        'user_id' => $passUser->id,
                        'pass_id' => $pass->id,
                        'event_id' => $pass->event_id,
                        'type' => "parental_control",
                        'category' => "pass_updated_from_user_to_guests",
                        'heading' => $pass->id.' Pass Updated',
                        'subheading' => 'Send by ' . $createdBy->fullName,
                        'text' => "$createdBy->fullName is updated the invitation to $eventName on $pass->description"
                    ];

                    $PcNotificationData =  self::create($PcNotificationData);

                    // create a regular push notification entry
                    $zuulNotificationData = [
                        'pass_id' => $pass->id,
                        'event_id' => $pass->event_id,
                        'user_id' => $passUser->id,
                        'type' => "zuul",
                        'category' => "send_pass",
                        'heading' => $pass->id.' Pass Updated',
                        'subheading' => 'Send by ' . $createdBy->fullName,
                        'text' => "$createdBy->fullName is updated invitation to $eventName on $pass->description"
                    ];

                    self::create($zuulNotificationData);

                    //Send Push Notifications
                    if($PcNotificationData['bool']){
                        $push = array(
                            'description' => "$createdBy->fullName is updated the invitation to $eventName on $pass->description",
                            'title' => "$pass->id Pass Updated",
                            'nid' => $PcNotificationData['result']->id,
                            'nid_type' => 'regular',
                            'link' => url('/nd/'.$PcNotificationData['result']->id),
                            'info' => "Send by ' . $createdBy->fullName"
                            //'info' => self::success('Send by ' . $sender->fullName, [$type16])->getData()
                        );
                        $this->sendPushNotificationToUser($push,$passUser);
                    }
                    //End Notifications

                    // add the notification if parent needed to be notified
                    $parentalControl = checkParentalControlByMember('notify_when_pass_updated_included_member',$passUser->id);
                    if($parentalControl){
                        foreach ($parentalControl as $row){
                            // type - 17: new_pass_sent_from_user_to_parents
                            $parentalControlNotification = [
                                'parent_id' => $row->parent_id,
                                'user_id' => $passUser->id,
                                'pass_id' => $pass->id,
                                'event_id' => $pass->event_id,
                                'type' => 'parental_control',
                                'category' => "pass_updated_from_user_to_guests_to_parents",
                                'heading' => "$pass->id Pass Updated received in family",
                                'subheading' => 'Received by ' . $passUser->fullName,
                                'text' => "Invited updated by $createdBy->fullName, Pass for Invitation on $eventName for $pass->description"
                            ];
                            $parentalControlNotification = self::create($parentalControlNotification);

                            /* Push Notification */
                            $push = array(
                                'description' => "Invitation updated by $createdBy->fullName, Pass for Invitation on $eventName for $pass->description ",
                                'title' => "$pass->id Pass Updated received in family",
                                'nid' => $parentalControlNotification['result']->id,
                                'nid_type' => 'parental',
                                'link' => url('/nd/'.$parentalControlNotification['result']->id),
                                'info' => "Received by ' . $passUser->fullName"
                            );
                            $this->sendPushNotificationToUser($push,$row->parentUser);
                            /* End Push Notification */
                        }
                    }
                }

                // add the notification if parent needed to be notified
                $parentalControl = checkParentalControlByMember('notify_when_member_update_a_pass',$createdBy->id);
                if($parentalControl){
                    foreach ($parentalControl as $row){
                        // type - 17: new_pass_sent_from_user_to_parents
                        $parentalControlNotification = [
                            'parent_id' => $row->parent_id,
                            'user_id' => $createdBy->id,
                            'pass_id' => $pass->id,
                            'event_id' => $pass->event_id,
                            'type' => 'parental_control',
                            'category' => "pass_updated_received_by_member_notify_parents",
                            'heading' => "Pass updated sent from you family",
                            'subheading' => 'Sent by ' . $createdBy->fullName,
                            'text' => "$guestCount Guest(s) Invited: on $eventName for $pass->description"

                        ];
                        $parentalControlNotification =  self::create($parentalControlNotification);

                        /* Push Notification */
                        $push = array(
                            'description' => "$guestCount Guest(s) Invited: on $eventName for $pass->description",
                            'title' => "Pass updated sent from you family",
                            'nid' => $parentalControlNotification['result']->id,
                            'nid_type' => 'parental',
                            'link' => url('/nd/'.$parentalControlNotification['result']->id),
                            'info' => 'Sent by ' . $createdBy->fullName
                        );
                        $this->sendPushNotificationToUser($push,$row->parentUser);
                        /* End Push Notification */
                    }
                }
            }
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }


    /**
     * get zuul notifications
     * @param $userId
     * @param $type
     * @param int $limit
     * @return mixed
     */
    public function getNotifications($userId,$type, $limit = 10)
    {
        $result =  parent::getNotificationsByType($userId,$type, $limit);
        return $result->map(function ($row, $key) {
            $anonymous = 0;
            $expireSeconds = 0;
            $passRequestSentUserId = null;
            //Get Pass Request Expiration
            if ($row->category == 'send_request_for_pass' &&  $row->passRequest()->exists()){
                $anonymous =  $row->passRequest->anonymous;
                $passRequestSentUserId =  $row->passRequest->send_user_id;
                if(!empty($row->expire_at) && now() < $row->expire_at){
                    $expireSeconds = now()->diffInSeconds($row->expire_at);
                }
            }
            return [
                'id' => $row->id,
                'parent_id' => $row->parent_id,
                'user_id' => $row->user_id,
                'pass_id' => $row->pass_id,
                'pass_request_id' => $row->pass_request_id,
                'event_id' => $row->event_id,
                'heading' => $row->heading,
                'subheading' => $row->subheading,
                'category' => $row->category,
                'text' => $row->text,
                'link' => $row->link,
                'is_read' => $row->is_read,
                'formatted_created_at' => (!empty($row->created_at))? Carbon::parse($row->created_at)->format('M j, Y g:i A'):"",
                'formatted_expire_at' => $expireSeconds,
                'anonymous' => $anonymous,
                "pass_request_sent_user_id" => $passRequestSentUserId
            ];
        });
    }

    /**
     * @param $pass
     * @param $createdBy
     * @return array
     */
    public function notifyGuestsAfterPassRetracted($pass, $createdBy){
        try {
            if(empty($pass)){
                return [
                    'bool' => false,
                    'message' => 'Pass Not Found'
                ];
            }
            //create notification for pass users
            if($pass->addedUsers->isNotEmpty()){
                $eventName =  $pass->event->name ?? "";
                $guestCount = $pass->addedUsers->count();
                //dd($pass->addedUsers);
                foreach ($pass->addedUsers as $passUser){
                    // add a notification in pc for guest pass scan
                    $pcNotificationData = [
                        'parent_id' => $createdBy->id,
                        'user_id' => $passUser->id,
                        'pass_id' => $pass->id,
                        'event_id' => $pass->event_id,
                        'type' => "parental_control",
                        'category' => "pass_retracted_from_user_to_guests", //22
                        'heading' => "$pass->id Pass Retracted",
                        'subheading' => 'Send by ' . $createdBy->fullName,
                        'text' => "$createdBy->fullName is retracted the invitation to $eventName on $pass->description"
                    ];
                    $pcNotificationData = self::create($pcNotificationData);

                    if($pcNotificationData['bool']){
                        /* Push Notification */
                        $push = array(
                            'description' => "$createdBy->fullName is retracted the invitation to $eventName on  $pass->description",
                            'title' =>  "$pass->id Pass Retracted",
                            'nid' => $pcNotificationData['result']->id,
                            'nid_type' => 'parental',
                            'link' => url('/nd/'.$pcNotificationData['result']->id),
                            'info' => "Send by ' . $createdBy->fullName"
                            //'info' => self::success('Sent by ' . $sender->fullName, [$type22])->getData()
                        );
                        $this->sendPushNotificationToUser($push,$passUser);
                        /* End Push Notification */
                    }

                    // add the notification if parent needed to be notified
                    $parentalControl = checkParentalControlByMember('notify_when_pass_retracted_included_member',$passUser->id);
                    if($parentalControl){
                        foreach ($parentalControl as $row){
                            // type - 24: pass_retracted_received_by_member_notify_parents
                            $parentalControlNotification = [
                                'parent_id' => $row->parent_id,
                                'user_id' => $passUser->id,
                                'pass_id' => $pass->id,
                                'event_id' => $pass->event_id,
                                'type' => 'parental_control',
                                'category' => "pass_retracted_received_by_member_notify_parents", //24
                                'heading' => "$pass->id Pass Retracted received in family",
                                'subheading' => 'Received by ' . $passUser->fullName,
                                'text' => "Invitation Retracted by $createdBy->fullName, Pass for Invitation on $eventName for pass->description "
                            ];
                            $parentalControlNotification =  self::create($parentalControlNotification);

                            /* Push Notification */
                            $push = array(
                                'description' => "Invitation Retracted by $createdBy->fullName, Pass for Invitation on $eventName for $pass->description ",
                                'title' => "$pass->id Pass Retracted received in family",
                                'nid' => $parentalControlNotification['result']->id,
                                'nid_type' => 'parental',
                                'link' => url('/nd/'.$parentalControlNotification['result']->id),
                                'info' => "Received by ' . $passUser->fullName"
                            );
                            $this->sendPushNotificationToUser($push,$row->parentUser);
                            /* End Push Notification */
                        }
                    }
                }

                // add the notification if parent needed to be notified
                $parentalControl = checkParentalControlByMember('notify_when_member_retract_a_pass',$createdBy->id);
                if($parentalControl){
                    foreach ($parentalControl as $row){
                        // type - 23: pass_retracted_from_user_to_guests_to_parents:
                        $parentalControlNotification = [
                            'parent_id' => $row->parent_id,
                            'user_id' => $createdBy->id,
                            'pass_id' => $pass->id,
                            'event_id' => $pass->event_id,
                            'type' => 'parental_control',
                            'category' => "pass_retracted_from_user_to_guests_to_parents",
                            'heading' => "Pass retracted sent from your family",
                            'subheading' => 'Sent by ' . $createdBy->fullName,
                            'text' => "$guestCount Guest(s) Invited: on $eventName for $pass->description"

                        ];
                        $parentalControlNotification = self::create($parentalControlNotification);

                        /* Push Notification */
                        $push = array(
                            'description' => "$guestCount Guest(s) Invited: on $eventName for $pass->description",
                            'title' => 'Pass retracted sent from your family',
                            'nid' => $parentalControlNotification['result']->id,
                            'nid_type' => 'parental',
                            'link' => url('/nd/'.$parentalControlNotification['result']->id),
                            'info' => 'Sent by ' . $createdBy->fullName
                        );
                        $this->sendPushNotificationToUser($push,$row->parentUser);
                        /* End Push Notification */
                    }
                }
            }
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    public function notifyAdminDashboardForPassScan($communityId){
        $this->userRepo->getCommunityAdminList($communityId);
    }

    /**
     * Create And Send Push notifications
     * @param $passUser
     * @return array
     * @throws \Exception
     */
    public function notifyAfterPassScan($passUser){
        try{
            if($passUser){
                //Get Success ScanLog
                $scanLog = $passUser->scanLogs()->where('is_success','1')->orderBy('id', 'desc')->first();
                $createdBy = $passUser->pass->createdBy;
                $receiver = $passUser->user;
                $pass = $passUser->pass;
                $eventName = $pass->event->name;
                $description = $pass->description;
                $scanByName = $scanLog->ScanByName;
                $scanBy = ($scanLog->type == 'guard') ? $scanLog->scanBy :  $scanLog->scanner;


                $push = array(
                    'description' => "Scanned by ".$scanLog->scanByName,
                    //'title' => $userWhoScanThisPass['fullName'] . ' Arrived',
                    //'nid' => $key_id->id,
                    'title' => $receiver->fullName.' Arrived',
                    'nid' => null,
                    'nid_type' => 'regular',
                );
                $this->sendPushNotificationToUser($push, $createdBy);
                // --------------------- Two Notification End --------------------------

                // ++++++++++ Create a Notification For Scan For Receiver  +++++++++++
                // add a notification in pc for guest pass scan
                $parentalControlNotification = [
                    'parent_id' => $receiver->id,
                    'user_id' => $receiver->id,
                    'pass_id' => $pass->id,
                    'event_id' => $pass->event_id,
                    'type' => 'zuul',
                    //'type' => 'parental_control',
                    'category' => "pass_scan_to_user",
                    'heading' => "Pass scanned successfully",
                    'subheading' => 'Scan by ' . $scanByName,
                    'text' => "Thank you $receiver->fullName, your pass for $eventName has been scanned for $description, invited by $createdBy->fullname"

                ];
                $parentalControlNotification = self::create($parentalControlNotification);

                // ++++++++++ Send Push Notification To Pass Receiver +++++++++++
                if ($parentalControlNotification['bool']){
                    $push = array(
                        'description' => "Thank you $receiver->fullName, your pass for $eventName has been scanned for $description, invited by $createdBy->fullname",
                        'title' => 'Pass scanned successfully',
                        'nid' => $parentalControlNotification['result']->id,
                        'nid_type' => 'parental',
                        'link' => url('/nd/'.$parentalControlNotification['result']->id),
                        // 'info' => self::success('Scan by ' . $scan_by_name, [$type11])->getData()
                        'info' => 'Scan by ' . $scanByName
                    );
                    //Mark:- Push Notifications for scanned pass successfully has been removed on adam's request
                    //$this->sendPushNotificationToUser($push, $receiver);
                }

                // +++++++++++ Notify  Receiver Parental Control ++++++++++++++
                // add the notification if parent needed to be notified
                $parentalControl = checkParentalControlByMember('notify_when_member_guest_arrive',$receiver->id);
                if($parentalControl){
                    foreach ($parentalControl as $row){
                        // type - 13: pass_scan_to_parent
                        $parentalControlNotification = [
                            'parent_id' => $row->parent_id,
                            'user_id' => $passUser->id,
                            'pass_id' => $pass->id,
                            'event_id' => $pass->event_id,
                            'type' => 'parental_control',
                            'category' => "pass_scan_to_parent",
                            'heading' => "Pass scanned successfully",
                            'subheading' => 'Scan by ' . $scanByName,
                            'text' => "Guest Arrived: $receiver->fullName invited by $createdBy->fullname for $description"
                        ];
                        $parentalControlNotification = self::create($parentalControlNotification);

                        /* Push Notification */
                        if ($parentalControlNotification['bool']){
                            $push = array(
                                'description' => "Guest Arrived: $receiver->fullName invited by $createdBy->fullname for $description",
                                'title' => "Pass scanned successfully",
                                'nid' => $parentalControlNotification['result']->id,
                                'nid_type' => 'parental',
                                'link' => url('/nd/'.$parentalControlNotification['result']->id),
                                'info' => "Scan by ' . $scanByName"
                                //'info' => self::success('Send by ' . $sender->fullName, [$type16])->getData()
                            );

                            //Mark:- Push Notifications for scanned pass successfully has been removed on adam's request
                            //$this->sendPushNotificationToUser($push,$row->parentUser);
                        }
                        /* End Push Notification */
                    }
                }

                // +++++++++++ Notify  Sender Parental Control ++++++++++++++
                // add the notification if parent needed to be notified
                $parentalControl = checkParentalControlByMember('notify_when_member_guest_arrive',$createdBy->id);
                if($parentalControl){
                    foreach ($parentalControl as $row){
                        // type - 13: pass_scan_to_parent
                        $parentalControlNotification = [
                            'parent_id' => $row->parent_id,
                            'user_id' => $createdBy->id,
                            'pass_id' => $pass->id,
                            'event_id' => $pass->event_id,
                            'type' => 'parental_control',
                            'category' => "pass_scan_to_parent",
                            'heading' => "Pass scanned successfully",
                            'subheading' => 'Scan by ' . $scanByName,
                            'text' => "Guest Arrived: $receiver->fullName invited by $createdBy->fullname for $description"
                        ];
                        $parentalControlNotification =  self::create($parentalControlNotification);

                        /* Push Notification */
                        if ($parentalControlNotification['bool']){
                            $push = array(
                                'description' => "Guest Arrived: $receiver->fullName invited by $createdBy->fullname for $description",
                                'title' => "Pass scanned successfully",
                                'nid' => $parentalControlNotification['result']->id,
                                'nid_type' => 'parental',
                                'link' => url('/nd/'.$parentalControlNotification['result']->id),
                                'info' => 'Scan by ' . $scanByName,
                                //'info' => self::success('Scan by ' . $scan_by_name, [$type13])->getData()
                            );

                            //Mark:- Push Notifications for scanned pass successfully has been removed on adam's request
                            //$this->sendPushNotificationToUser($push,$row->parentUser);
                        }
                        /* End Push Notification */
                    }
                }

                return [
                    'bool' => true,
                    'message' => 'Pass scan notifications sent'
                ];
            }
        } catch (\Exception $exception){
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }

    }
}
