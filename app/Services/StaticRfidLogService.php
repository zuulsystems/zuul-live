<?php
namespace App\Services;
use App\Models\StaticRfidLog;

class StaticRfidLogService{

    /**
     * Create static rfid log
     *
     */
    public function create(array $data){
        $staticRfidLog = StaticRfidLog::create($data);
        return $staticRfidLog;
    }

    /**
     * Static rfid log by rfid log id
     *
     */
    public function StaticRfidLogByRfidLogId($rfidLogId){
        $staticRfidLog = StaticRfidLog::where('rfid_log_id',$rfidLogId)->first();
        return $staticRfidLog;
    }
}
