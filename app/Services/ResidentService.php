<?php

namespace App\Services;
use Illuminate\Support\Str;
use App\Helpers\APIResponse;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\UserAdditionalContact;
use App\Models\User;
use App\Models\CountryPhoneFormat;

class ResidentService
{
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    // get resident additional phone list
    public function getResidentAdditionalPhoneList($id)
    {
        $resident_id = !empty($id) ? $id : "";
        $result = UserAdditionalContact::where('user_id',$resident_id)->whereNull('deleted_at')->get();

        $result->each(function ($item) {
            $formatted_phone = numberFormat($item->contact, Str::start($item->dial_code, '+'), 'INTERNATIONAL');
            $item->formatted_phone = $formatted_phone;
        });

        return APIResponse::success($result, 'OK');
    }

    // create resident additional phone number
    public function createResidentAdditionalPhoneNumber($userData, Request $request)
    {
        $lenght = 0;

        if($userData['add_dialcode'] == "+1"){
            $lenght = 10;
        }else if($userData['add_dialcode'] == "+504"){
            $lenght = 8;
        }

        if (strlen($userData['additional_contact']) != $lenght){
            return APIResponse::error('Additional Phone number must be at least ' . $lenght . ' digit long');
        }

        $additional_contact = UserAdditionalContact::create([
            "user_id"=> $userData['user_id'],
            "contact"=> $userData['additional_contact'],
            "type"=> $userData['additional_contact_type'],
            "dial_code"=> $request->has('add_dialcode') ? $userData['add_dialcode'] : null,

        ]);

        if ($additional_contact){
            return APIResponse::success( $additional_contact, 'Additional Contact Added!');
        } else {
            return APIResponse::error('Something went wrong. Please try again.');
        }
    }

    // edit resident additional phone number
    public function editResidentAdditionalPhoneNumber($id)
    {
        $additionalContact = UserAdditionalContact::where('id', $id)
        ->whereNull('deleted_at')
        ->get();

        if ($additionalContact->isNotEmpty()) {
            return APIResponse::success($additionalContact, 'Additional Contact For Edit!');
        } else {
            return APIResponse::error('Something went wrong. Please try again.');
        }
    }

    // update resident additional phone number
    public function updateResidentAdditionalPhoneNumber($id, $data)
    {

        $lenght = 0;

        if($data['add_dialcode'] == "+1"){
            $lenght = 10;
        }else if($data['add_dialcode'] == "+504"){
            $lenght = 8;
        }

        if (strlen($data['additional_contact']) != $lenght){
            return APIResponse::error('Additional Phone number must be at least ' . $lenght . ' digit long');
        }

        $additionalContact = UserAdditionalContact::where('id', $id)
            ->whereNull('deleted_at')
            ->update([
                "contact" => $data['additional_contact'],
                "type" => $data['additional_contact_type'],
                "dial_code" => $data['add_dialcode']
            ]);

        if ($additionalContact) {
            return APIResponse::success($additionalContact, 'Additional Contact Updated!');
        } else {
            return APIResponse::error('Something went wrong. Please try again.');
        }
    }

    // delete resident additional phone number
    public function deleteResidentAdditionalPhoneNumber($id)
    {
        $user = UserAdditionalContact::where('id', $id)->update([
            "deleted_at" => date('Y-m-d H:i:s')
        ]);

        if ($user) {
            return APIResponse::success($user, 'User Contact deleted!');
        } else {
            return APIResponse::error('Error: Event Not Found');
        }
    }

    // get phone format by country code
    public function getPhoneFormatByCountryCode($userId)
    {
        $result = User::with('community')
            ->where('id', $userId)
            ->whereNull('deleted_at')
            ->first();

        //Separating + Sign from phone numbers
        $prefix = '+';
        $dialCode = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $result->dial_code);

        $phoneFormat = CountryPhoneFormat::where('dial_code', $dialCode)->first();

        return APIResponse::success($phoneFormat, 'OK');
    }
}
