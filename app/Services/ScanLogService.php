<?php
namespace App\Services;
use App\Repositories\ScanLogRepository;
use App\Helpers\APIResponse;
use App\Http\Resources\ScanLogCollection;
class ScanLogService extends ScanLogRepository {


    public function scanLogs()
    {
        if(!auth()->user()->hasRole('guards') && !auth()->user()->hasRole('guard_admin')){
            return APIResponse::error('Invalid Guard User');
        }

        $scanLogs = parent::getScanLogs(auth()->id(), 10);

        if(!empty($scanLogs['result']))
        {
            return APIResponse::success(['list' => ScanLogCollection::collection($scanLogs['result'])], 'Success');
        }

        return APIResponse::success(['list' => []], 'Success');
    }

}
