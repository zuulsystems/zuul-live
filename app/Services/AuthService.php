<?php

namespace App\Services;
use Illuminate\Support\Str;
use App\Models\AuthLog;
use App\Helpers\APIResponse;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Validator;

class AuthService
{
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    // user login service
    public function loginUser($request)
    {
        $data = $request->all();

        $format_number = numberFormat($data['phone_number'], Str::start($data['dial_code'], '+'));

        $authAttempt = Auth::attempt([
            'phone_number' => $data['phone_number'],
            'password' => $data['password'],
        ]);

        if ($authAttempt) {
            $user = Auth::user();
            if ($user->is_delete == '1') {
                return APIResponse::error('Error: Authentication failed. Please check your phone number or password');
            }
            if ($user->role->slug == 'suerp_admin' || $user->role->slug == 'community_admin') {
                return APIResponse::error('Please use your residential user credentials.');
            }
            if ($user->role->slug == 'sub_admin' && $user->community->community_status == 'inactive') {
                return APIResponse::error('Community Features are disabled, please contact admin@zuulsystems.com');
            }

            AuthLog::create([
                'ip_address' => $request->ip(),
                'user_agent' => $request->userAgent(),
                'user_id' => $user->id
            ]);

            $user = new UserCollection($user);
            $userArray = collect($user)->toArray();

            $userArray = $userArray + ['formatted_phone' => $format_number];

            $success['token'] = $user->toArray(null)['token'];

            return APIResponse::success(['success' => $success, 'user' => $userArray], 'Authorized: Welcome to ZUUL');
        } else {
            return APIResponse::error('Error: Authentication failed. Please check your phone number or password');
        }
    }

    // verify guard login
    public function verifyGuardLogin($request)
    {
        $validator = Validator::make($request->all(), ['code' => 'required'], ['code.required' => 'Verification code is required']);

        if ($validator->fails()) {
            return APIResponse::error($validator->errors()->first());
        }

        $code = $request->input('code');

        $user = $this->userRepo->whereAttribute(['verification_code'], [null, $request->code], ['<>']);

        if (!$user['bool']) {
            return APIResponse::error('Verification code does not exist');
        }

        if ($user['bool']) {
            $user = $user['data'];
            $auth_attempt = Auth::loginUsingId($user->id);
            if ($auth_attempt) {
                $user = Auth::user();
                $token = $user->createToken('ZUUL Systems')->accessToken;

                $user->update([
                    'role_id' => 8,
                    'is_verified' => '1',
                    'verification_code' => null,
                ]);

                return APIResponse::success([
                    'user_id' => $user->id,
                    'token' => $token,
                ], 'Verification code matched');

            } else {
                return APIResponse::error('Login attempt fail');
            }
        } else {
            return APIResponse::error('Verification code does not exist');
        }
    }
}
