<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\FetchTickets::class,
        Commands\RemoveTemporaryZuulNotification::class,
        Commands\RemoveTemporaryUserContact::class,
//        Commands\HouseCsvCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('fetch:tickets')->daily();
        $schedule->command('remove:temporary-zuul-notification')->everyFiveMinutes();
        $schedule->command('remove:temporary-user-contact')->everyFiveMinutes();
        $schedule->command('queue:work')->cron('* * * * *');
//        $schedule->command('housecsv:cron')->everyMinute();
        // $schedule->command('backup:clean')->weekly()->saturdays()->at('13:00'); //clean up old backups
        // $schedule->command('backup:run')->weekly()->saturdays()->at('14:00'); // fresh backup
        //$schedule->command('queue:work --tries=3')->cron('* * * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
