<?php

namespace App\Console\Commands;

use App\Models\UserContact;
use Illuminate\Console\Command;

class RemoveTemporaryUserContact extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:temporary-user-contact';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UserContact::whereNotNull('expire_date')->where('is_temporary','1')->where('expire_date', '<', now())->delete();
        $this->line('<fg=green>Job Successful.</>');

    }
}
