<?php

namespace App\Console\Commands;

use App\Models\Notification;
use Illuminate\Console\Command;

class RemoveTemporaryZuulNotification extends Command
{
    protected $notificationService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:temporary-zuul-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Notification::zuul()->where('category','send_request_for_pass')->where('expire_at','>',now())->delete();
    }
}
