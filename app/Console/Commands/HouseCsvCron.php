<?php

namespace App\Console\Commands;

//use App\Models\House;
use App\Models\HouseCsv;
use Illuminate\Console\Command;

class HouseCsvCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'housecsv:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        \App\Models\House::create([
            'house_name'=>'abc',
            'house_detail'=>'def',
            'community_id'=>1,
            'phone'=>'556',
            'created_by'=>1,
            'lot_number'=>'234'
        ]);

//        $houseCsvs = HouseCsv::where('mark','0')->limit(250)->get();

//        if(!empty($houseCsvs))
//        {
//            foreach ($houseCsvs as $houseCsv)
//            {
//                $rowError = false;
//                $csvError = '';
//                $comma = false;
//
//                if(empty($houseCsv->house_name))
//                {
//                    $rowError = true;
//                    $csvError .= 'blank house name';
//                    $comma = true;
//                }
//
//                if(empty($houseCsv->address))
//                {
//                    $rowError = true;
//                    $comma ? $csvError .= ', blank address' : $csvError .= 'blank address';
//                    $comma = true;
//                }
//
//                $house = House::where(['house_name'=>$houseCsv->house_name, 'house_detail'=>$houseCsv->address, 'community_id'=>$houseCsv->community_id])->first();
//
//                if(!$rowError)
//                {
//                    if(!empty($house))
//                    {
//                        $rowError = true;
//                        $comma ? $csvError .= ', house exist' : $csvError .= 'house exist';
//                    }
//                }
//
//                $status = '';
//                $message = '';
//
//                if(!$rowError)
//                {
//                    House::create([
//                        'house_name'=>$houseCsv->house_name,
//                        'house_detail'=>$houseCsv->address,
//                        'community_id'=>$houseCsv->community_id,
//                        'phone'=>$houseCsv->primary_contact,
//                        'created_by'=>$houseCsv->created_by,
//                        'lot_number'=>$houseCsv->lot_number
//                    ]);
//
//                    $status = 'success';
//                    $message = 'Record imported successfully';
//                }
//                else
//                {
//                    $status = 'failure';
//                    $message = $csvError;
//                }
//
//                $houseCsv = $houseCsv->find($houseCsv->id);
//                $houseCsv->mark='1';
//                $houseCsv->status=$status;
//                $houseCsv->message=$message;
//                $houseCsv->save();
//            }
//        }

        $this->info('Demo:Cron Command Run successfully!');
    }
}
