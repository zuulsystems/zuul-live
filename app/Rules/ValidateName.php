<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        //return (preg_match("/^([a-zA-Z' ]+)$/",$value))?true:false;
        // return (preg_match("/^([A-Za-z*]+)$/",$value))?true:false;
        return (preg_match("/^[a-zA-Z_ -]*$/",$value))?true:false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Format letters only.';
    }
}
