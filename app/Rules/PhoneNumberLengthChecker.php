<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class PhoneNumberLengthChecker implements Rule
{
    protected $expectedLength, $dial_code;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($dc)
    {
        $this->expectedLength = 0;
        $this->dial_code = $dc;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $dc = Str::start($this->dial_code, '+');
        $check_dial_code = $dc == "+1" ? 10 : 8;
        $this->expectedLength = $check_dial_code;
        return strlen($value) === $check_dial_code;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Phone number should be ' . $this->expectedLength . ' digits long';
    }
}
