<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactGroup extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function userContacts()
    {
        return $this->belongsToMany(UserContact::class, 'contact_collections')->wherePivotNull('deleted_at');
    }

    public function contactCollections()
    {
        return $this->hasMany(ContactCollection::class, 'contact_group_id');
    }
}
