<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParentalControl extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'parent_id','member_id','notify_when_member_guest_arrive', 'notify_when_member_send_pass_to_others', 'notify_when_member_receive_a_pass', 'notify_when_member_retract_a_pass', 'notify_when_member_update_a_pass', 'notify_when_pass_retracted_included_member', 'notify_when_pass_updated_included_member','notify_when_member_request_a_pass_with_offensive_words','blacklist_member_to_receive_request_a_pass','allow_parental_controls_to_member'
    ];


    public function parentUser(){
        return $this->belongsTo(User::class,'parent_id');
    }
}
