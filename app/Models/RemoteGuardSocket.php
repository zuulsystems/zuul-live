<?php

namespace App\models;

use App\Models\Scanner;
use App\Models\RemoteGuard;
use Illuminate\Database\Eloquent\Model;

class RemoteGuardSocket extends Model
{
    //
    protected $fillable = ['mac_address', 'socket_id', 'scan_log_id','remote_guard_id'];

    public function remote_guards()
    {
        return $this->hasOne(RemoteGuard::class, 'id', 'remote_guard_id');
    }


}
