<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuardAppointLocation extends Model
{
    protected $guarded = ['id'];
}
