<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class House extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function getFullAddressAttribute()
    {
        return "{$this->street_address} {$this->apartment} {$this->city} {$this->state} {$this->zip_code}";
    }

    public function community(){
        return $this->belongsTo(Community::class);
    }
    public function residents(){
        return $this->hasMany(User::class,'house_id','id');
    }
}
