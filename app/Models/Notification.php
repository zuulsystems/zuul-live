<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id','user_id','pass_id','event_id', 'heading', 'subheading', 'text', 'link', 'type','category','pass_request_id','announcement_id','traffic_ticket_id','is_read','expire_at'
    ];

    public function pass()
    {
        return $this->belongsTo(Pass::class,'pass_id');
    }

    public function passRequest()
    {
        return $this->belongsTo('App\Models\PassRequest','pass_request_id');
    }

    public function scopeZuul($query)
    {
        return $query->where('type', 'zuul');
    }
    public function scopeParentalControl($query)
    {
        return $query->where('type', 'parental_control');
    }

    public function announcement(){
        return $this->belongsTo(Announcement::class,'announcement_id');
    }
    public function smsLogs(){
        return $this->hasMany(SmsLog::class,'notification_id');
    }
}
