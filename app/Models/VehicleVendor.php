<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleVendor extends Model
{
    //
    use SoftDeletes;

    protected $guarded = ['id'];

    public function Vehicles(){
        return $this->belongsTo(Vehicle::class, 'vehicle_id');
    }

    public function Vendor(){
        return $this->belongsTo(Vendor::class);
    }

    public function Tickets(){
        return $this->belongsTo(TrafficTicket::class, 'ticket_id');
    }
}
