<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = ['id'];

    /**
     * get formatted phone number
     * @return string
     */
    public function getFormattedPhoneAttribute()
    {
        $phone = $this->phone_number;
        if (empty($phone)){
            return '';
        }

        if($this->dial_code == "504")
        {
            $a1 = substr($phone, 0, 4);
            $a2 = substr($phone, 4, 4);
            return $a1."-".$a2;
        }
        else if($this->dial_code == "+504")
        {
            $a1 = substr($phone, 0, 4);
            $a2 = substr($phone, 4, 4);
            return $a1."-".$a2;
        }

        $ac = substr($phone, 0, 3);
        $prefix = substr($phone, 3, 3);
        $suffix = substr($phone, 6);
        return "({$ac}) {$prefix}-{$suffix}";
    }

    public function user()
    {
        return $this->belongsTo(User::class,'phone_number','phone_number');
    }
}
