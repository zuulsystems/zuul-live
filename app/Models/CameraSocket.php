<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CameraSocket extends Model
{
    //

    protected $fillable = ['mac_address', 'socket_id', 'scan_log_id','remote_guard_id'];
}
