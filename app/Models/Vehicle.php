<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function getVehicleInfoAttribute(){
        $licensePlate = strtoupper($this->license_plate);
        return "{$licensePlate}\n{$this->color}\n{$this->make}\n{$this->model}\n{$this->year}";
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function trafficTickets(){
        return $this->hasMany(TrafficTicket::class,'license_plate','license_plate');
    }

    public function vendor()
    {
        return $this->belongsToMany(Vendor::class, 'vehicle_vendors', 'vehicle_id', 'vendor_id')->take(1);
    }
}
