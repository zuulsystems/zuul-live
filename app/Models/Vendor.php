<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use SoftDeletes;


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['formattedAddress'];


    public function getFormattedAddressAttribute(){
        return ($this->street_address) . " " . ($this->apartment) . " " .  ($this->city) . " " . ($this->state) . " " . ($this->zip_code);
    }

    protected $guarded = ['id'];

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function vehicle_vendor(){
        return $this->hasMany(VehicleVendor::class);
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'vehicle_vendors');
    }
}
