<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RemoteGuard extends Model
{

    use SoftDeletes;
    protected $fillable = ['identifier','type','attach_to','meta_id','meta_name','community_id','zuul_key','zuul_secret'];

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function scanner()
    {
        return $this->hasOne(Scanner::class, 'community_id','community_id');
    }
}
