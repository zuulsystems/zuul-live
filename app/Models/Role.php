<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function scopeCommunityAdmin($query)
    {
        return $query->where('slug', 'sub_admin');
    }
    public function scopeKioskAdmin($query)
    {
        return $query->where('slug', 'kiosk');
    }

    public function scopeGuardAdmin($query)
    {
        return $query->where('slug', 'guard_admin');
    }

    public function scopeGuard($query)
    {
        return $query->where('slug', 'guards');
    }

    public function scopeGuest($query)
    {
        return $query->where('slug', 'guests_outsiders_daily');
    }

    public function scopeResident($query)
    {
        return $query->whereIn('slug', ['family_member', 'family_head']);
    }

    public function scopeFamilyMember($query)
    {
        return $query->where('slug', 'family_member');
    }

    public function scopeFamilyHead($query)
    {
        return $query->where('slug', 'family_head');
    }

    public function scopeVendor($query)
    {
        return $query->where('slug', 'vendors');
    }

    public function scopeOfType($query, $name)
    {
        return $query->where('slug', $name);
    }
}
