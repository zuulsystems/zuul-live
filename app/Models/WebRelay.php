<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebRelay extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['mac_address','ip_address','web_hook','community_id','connected_pi_id','url','secret_key'];

    /*
     * Community
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }
}
