<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;
use PhpParser\Node\Expr\StaticCall;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Authorizable;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name', 'formatted_phone','formatted_phone_by_dial_code', 'profile_image_url', 'fullNameInverted', 'formattedAddress'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function deleteResidentData(){
        $this->authLogs()->forceDelete();
        $scanLogs = $this->scanLogs();


        if(!empty($scanLogs))
        {
            foreach ($scanLogs as $scanLog)
            {
                StaticScanLog::where('scan_log_id',$scanLog->id)->delete();
            }
        }

        $scanLogs->forceDelete();
        $this->smsLogs()->forceDelete();
        $this->dashboardSettings()->forceDelete();

        if($this->rfids()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->rfids()->withTrashed()->get() as $rfid){
                if ($rfid->rfidDateLimits()->withTrashed()->get()->isNotEmpty()){
                    foreach ($rfid->rfidDateLimits()->withTrashed()->get() as $rfidDateLimit ){
                        $rfidDateLimit->rfidDateLimitWeeks()->withTrashed()->forceDelete();
                    }
                    $rfid->rfidDateLimits()->withTrashed()->forceDelete();
                }
                $rfid->rfidLogs()->withTrashed()->forceDelete();
            }
            $this->rfids()->withTrashed()->forceDelete();
        }

        if ($this->zuulNotifictions()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->zuulNotifictions()->withTrashed()->get() as $zuulNotifiction){
                if ($zuulNotifiction->smsLogs->isNotEmpty()){
                    $zuulNotifiction->smsLogs()->forceDelete();
                }
            }
        }

        $this->zuulNotifictions()->withTrashed()->forceDelete();
        if ($this->parentalControlNotifictions()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->parentalControlNotifictions()->withTrashed()->get() as $parentalControlNotifiction){
                if ($parentalControlNotifiction->smsLogs->isNotEmpty()){
                    $parentalControlNotifiction->smsLogs()->forceDelete();
                }
            }
        }

        $this->parentalControlNotifictions()->withTrashed()->forceDelete();


        if ($this->passUsers()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->passUsers()->withTrashed()->get() as $passUser){


                if ($passUser->scanLogs->isNotEmpty()){

                    $scanLogs = $passUser->scanLogs;
                    if(!empty($scanLogs))
                    {
                        foreach($scanLogs as $scanLog)
                        {
                            StaticScanLog::where('scan_log_id', $scanLog->id)->forceDelete();
                        }
                    }
                    $passUser->scanLogs()->forceDelete();

                }
            }
        }

        $this->passUsers()->withTrashed()->forceDelete();

        if ($this->passes()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->passes()->withTrashed()->get() as $pass){
                if ($pass->passUsers()->withTrashed()->get()->isNotEmpty()){
                     foreach ($pass->passUsers()->withTrashed()->get() as $passUser){
                        if ($passUser->scanLogs->isNotEmpty()){

                            //11-8-2022
                            $scanLogs = $passUser->scanLogs;
                            if(!empty($scanLogs))
                            {
                                foreach($scanLogs as $scanLog)
                                {
                                    StaticScanLog::where('scan_log_id', $scanLog->id)->forceDelete();
                                }
                            }
                            $passUser->scanLogs()->forceDelete();

                        }
                    }
                    $pass->passUsers()->withTrashed()->forceDelete();
                }
                if ($pass->notifications()->withTrashed()->get()->isNotEmpty()){
                     foreach ($pass->notifications()->withTrashed()->get() as $notification){
                        if ($notification->smsLogs->isNotEmpty()){
                            $notification->smsLogs()->forceDelete();
                        }
                    }
                    $pass->notifications()->withTrashed()->forceDelete();
                }
            }
        }

        $this->passes()->withTrashed()->forceDelete();
        //notifications
        if ($this->passRequestRequester()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->passRequestRequester()->withTrashed()->get() as $passRequestRequesterRow){
                if ($passRequestRequesterRow->notifications()->withTrashed()->get()->isNotEmpty()){
                    $passRequestRequesterRow->notifications()->withTrashed()->forceDelete();
                }
            }
        }

        $this->passRequestRequester()->withTrashed()->forceDelete();
        if ($this->passRequestSend()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->passRequestSend()->withTrashed()->get() as $passRequestSendRow){
                if ($passRequestSendRow->notifications()->withTrashed()->get()->isNotEmpty()){
                    $passRequestSendRow->notifications()->withTrashed()->forceDelete();
                }
            }
        }

        $this->passRequestSend()->withTrashed()->forceDelete();
        if ($this->groups()->withTrashed()->get()->isNotEmpty()){
            foreach ($this->groups()->withTrashed()->get() as $group){
                if ($group->contactCollections->isNotEmpty()){
                    $group->contactCollections()->forceDelete();
                }
            }
        }

        $this->groups()->withTrashed()->forceDelete();
        $this->userContacts()->withTrashed()->forceDelete();

        $this->permissions()->sync([]);
        $this->vehicles()->withTrashed()->forceDelete();
        $this->events()->withTrashed()->forceDelete();


    }
    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Search from first name & last name
     * @param $query
     * @param $name
     * @return mixed
     */
    function scopeWithFullName($query, $name)
    {
        // Split each Name by Spaces
        $names = explode(" ", $name);

        // Search each Name Field for any specified Name
        return $query->where(function($query) use ($names) {
            $query->whereIn('first_name', $names);
            $query->orWhere(function($query) use ($names) {
                $query->whereIn('last_name', $names);
            });
        });
    }

    /**
     * get formatted phone number
     *
     * @return string
     */
    public function getFormattedPhoneAttribute()
    {
        $phone = $this->phone_number;
        if (empty($phone)){
            return '';
        }
        $ac = substr($phone, 0, 3);
        $prefix = substr($phone, 3, 3);
        $suffix = substr($phone, 6);

        return "({$ac}) {$prefix}-{$suffix}";
    }


    /**
     * get formatted phone number
     *
     * @return string
     */
    public function getFormattedPhoneByDialCodeAttribute()
    {
        $phone = $this->phone_number;
        $dialcode = $this->dial_code;

        return $dialcode . " " . numberFormat($phone, Str::start($dialcode, '+'));

    }

    //get profile image url from s3 bucket
    public function getProfileImageUrlAttribute()
    {
        if( isset($this->attributes['profile_image']) && !empty($this->attributes['profile_image']) ){
            $filepath =  base_path('public/images/profile_images/' . $this->attributes["profile_image"]);
            if(file_exists($filepath)){
                return asset('images/profile_images/'.$this->attributes["profile_image"]);
            }else{
                return asset('assets/frontend/img/default-user.png');
            }
        }
        return asset('assets/frontend/img/default-user.png');
    }

    //get license image url from s3 bucket
    public function getLicenseImageUrlAttribute()
    {
        return strpos(url('/'), '127.0.0.1') !== false ? url('/') .'/'. $this->license_image : getS3Image($this->license_image);
    }
    //get license image url from s3 bucket with default license image
    public function getLicenseImageUrlWithDefaultImageAttribute()
    {
        $licenseImage = getS3Image($this->license_image);
        return (!empty($licenseImage))?$licenseImage:asset('images/license/license.png');
    }

    public function getFormattedAddressAttribute(){
        return ($this->street_address) . " " . ($this->apartment) . " " .  ($this->city) . " " . ($this->state) . " " . ($this->zip_code);
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'user_permissions');
    }

    public function additionalPhone()
    {
        return $this->hasMany(UserAdditionalContact::class, 'user_id');
    }


    public function getEnableRemoteGuardAttribute($value)
    {
        return $value == 1;
    }


    public function isGuardAdmin()
    {
        return $this->hasRole('guard_admin');
    }

    public function hasRole($role)
    {
        return (!empty($this->role) && $role == $this->role->slug) ? true : false;
    }

    public function hasPermissionTo($permission)
    {
        //Check has permission through role

        return $this->hasPermission($permission->slug ?? "");
    }

    public function hasPermission($slug)
    {
        return (boolean)$this->permissions->where('slug', $slug)->count();
    }

    public function assignPermission($permission)
    {
        $permission = Permission::where('slug', $permission)->first();
        if ($permission) {
            //check if user has not permission then assign permission
            if (!$this->hasPermission($permission->slug)) {
                $this->permissions()->attach($permission->id);
            }
        }
    }

    public function revokePermission($permission)
    {
        $permission = Permission::where('slug', $permission)->first();
        if ($permission) {
            // revoke permission
            $this->permissions()->detach($permission->id);
        }
    }

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function house()
    {
        return $this->belongsTo(House::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'user_id');
    }

    public function bannedUser()
    {
        return $this->hasOne(BannedCommunityUser::class, 'user_id');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'created_by');
    }
    public function remoteGuard()
    {
        return $this->hasOne(RemoteGuard::class, 'community_id');
    }

    public function trafficTickets()
    {
        return $this->hasManyThrough(TrafficTicket::class, Vehicle::class, 'user_id', 'vehicle_id', 'id', 'id');
    }

    public function getFullNameInvertedAttribute()
    {
        return trim($this->last_name).' '.trim($this->first_name);
    }

    public function contacts()
    {
        return $this->hasManyThrough(Contact::class, UserContact::class, 'created_by', 'id', 'id', 'contact_id');
    }
    public function userContacts()
    {
        return $this->hasMany(UserContact::class,'created_by');
    }

    public function groups()
    {
        return $this->hasMany(ContactGroup::class, 'created_by');
    }

    public function passes()
    {
        return $this->hasMany(Pass::class, 'created_by');
    }

    public function passUsers()
    {
        return $this->hasMany(PassUser::class, 'user_id');
    }

    public function zuulNotifictions()
    {
        return $this->hasMany(Notification::class, 'user_id');
    }

    public function parentalControlNotifictions()
    {
        return $this->hasMany(Notification::class, 'parent_id');
    }

    public function passRequestRequester()
    {
        return $this->hasMany(PassRequest::class, 'request_user_id');
    }
    public function passRequestSend()
    {
        return $this->hasMany(PassRequest::class, 'send_user_id');
    }
    public function scanLogs()
    {
        return $this->hasMany(ScanLog::class, 'scan_by');
    }
    public function smsLogs()
    {
        return $this->hasMany(SmsLog::class, 'user_id');
    }

    public function authLogs()
    {
        return $this->hasMany(AuthLog::class, 'user_id');
    }

    public function countryCode()
    {
        return $this->hasOne(CountryPhoneFormat::class, 'dial_code', 'dial_code');
    }

    public function dashboardSettings()
    {
        return $this->hasMany(DashboardSetting::class, 'user_id');
    }

    public function rfids()
    {
        return $this->hasMany(Rfid::class, 'user_id');
    }

    public function bandCommunities()
    {
        return $this->hasMany(BannedCommunityUser::class, 'user_id')->with('community');
    }

    public function kioskPermission()
    {
        return $this->hasOne(KioskPermission::class);
    }


}



