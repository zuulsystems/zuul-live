<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemoteGuardWebTracking extends Model
{
    //
    protected $table = 'remote_guard_web_trackings';
    protected $fillable = ['guard_id','web_relay_id','scan_log_id','type'];

    public function webRelay()
    {
        return $this->belongsTo(WebRelay::class);
    }

    public function guardUser()
    {
        return $this->belongsTo(User::class,'guard_id','id');
    }
}
