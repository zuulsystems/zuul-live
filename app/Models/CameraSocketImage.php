<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CameraSocketImage extends Model
{
    //
    protected $fillable = ['mac_address', 'ImageURL', 'scan_log_id','plateNum'];
    protected $appends = ['fullImageUrl'];

    //get license image url from s3 bucket
    public function getFullImageUrlAttribute()
    {
        return getS3Image($this->ImageURL);
    }





}
