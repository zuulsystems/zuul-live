<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailLog extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'to', 'subject', 'body', 'email_sent_by', 'email_status', 'pass_id', 'ticket_id',
    ];

    public function ticket(){
        return $this->belongsTo(TrafficTicket::class, 'ticket_id', 'id');
    }

    public function pass(){
        return $this->belongsTo(Pass::class, 'pass_id', 'id');
    }

}
