<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IonoHeartbeatLog extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['connected_pi_mac_address','status','type'];
}
