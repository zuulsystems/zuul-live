<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RfidCsv extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['rfid_tag_num','house_assigned','resident_first_name','resident_last_name','mailing_address','driver_license_number','license_plate','state_registered','vehicle_type','make','color','year','model','insurance_company_name','policy_number','policy_expiration_date','additional_drivers','community_id','status','mark','message','file_name'];
}
