<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebRelayTracking extends Model
{
    //
    protected $fillable = ['guard_id','web_relay_id','scan_log_id'];

    public function webRelay()
    {
        return $this->belongsTo(WebRelay::class);
    }

    public function guardUser()
    {
        return $this->belongsTo(User::class,'guard_id','id');
    }
}
