<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrafficPaymentLog extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'date', 'ticket_id', 'user_id', 'payee_user_id', 'created_by',
    ];

    public function payee(){
        return $this->belongsTo(User::class, 'payee_user_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function ticket(){
        return $this->belongsTo(TrafficTicket::class);
    }
}
