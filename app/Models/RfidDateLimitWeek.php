<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RfidDateLimitWeek extends Model
{
    use SoftDeletes;
    protected $table = 'rfid_date_limit_weeks';
    protected $fillable = ['rfid_date_limit_id','day', 'start_time', 'end_time', 'status'];
}
