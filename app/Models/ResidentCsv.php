<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResidentCsv extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['first_name','last_name','email','phone_number','house_name','head_of_family','special_instructions','community_id','file_name','additional_phone_number1_dialcode','additional_phone_number1', 'additional_phone_number1_type','additional_phone_number2_dialcode','additional_phone_number2','additional_phone_number2_type', 'dial_code'];

}
