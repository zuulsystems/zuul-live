<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class StaticRfidLog extends Model
{
    //
    protected $fillable = ['rfid','scanner','resident_name','community_name','type','is_success','log_text','scan_at','vehicle_info','rfid_log_id'];
}
