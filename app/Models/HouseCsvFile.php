<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HouseCsvFile extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['file_name','total_records','process_records','community_id'];

    /**
     * Get the community.
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }
}
