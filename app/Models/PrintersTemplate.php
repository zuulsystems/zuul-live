<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrintersTemplate extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name', 'template','community_id'
    ];

    public function community(){
        return $this->belongsTo(Community::class);
    }
}
