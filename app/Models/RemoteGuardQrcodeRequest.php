<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemoteGuardQrcodeRequest extends Model
{
    //
    protected $fillable = ['user_id', 'qrcode', 'expire_time', 'socket_id'];

}
