<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PassUser extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pass_id', 'user_id', 'qr_code', 'is_scan','scan_at', 'vehicle_id', 'ticket_id',
    ];

    protected $appends = ['ScannedDate'];
    /**
     * Pass
     */
    public function pass(){
        return $this->belongsTo(Pass::class,'pass_id');
    }

    /**
     * assigned pass to user
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * assigned pass to vehicle
     */
    public function vehicle(){
        return $this->belongsTo(Vehicle::class,'vehicle_id');
    }

    /**
     * scan logs
     */
    public function scanLogs()
    {
        return $this->hasMany(ScanLog::class, 'pass_user_id')->orderBy('created_at', 'DESC');
    }
    /**
     * scan logs
     */
    public function successScanLog()
    {
        return $this->hasOne(ScanLog::class, 'pass_user_id')->where('is_success','1');
    }

    public function getScannedDateAttribute(){
        if(empty($this->scan_at)){
            return '';
        }

        if(isset(auth()->user()->community_id) && auth()->user()->community_id != null){
            $tzt = dBTzToCommunityTz($this->scan_at, auth()->user()->community_id);
            $t = Carbon::parse($tzt)->format('m-d-Y h:i a');
            return $t;
        }
    }
}
