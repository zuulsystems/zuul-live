<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RfidLog extends Model
{
    use SoftDeletes;

    protected $table = 'rfid_logs';
    protected $fillable = ['rfid_id','scan_by','scanner_id','community_id' , 'rfids_tag_num_static', 'is_success', 'log_text', 'type', 'scan_at', 'vehicle_info'];

    public function scanBy()
    {
        return $this->belongsTo(User::class,'scan_by');
    }

    public function scanByScanner()
    {
        return $this->belongsTo(Scanner::class,'scanner_id');
    }

    public function rfid()
    {
        return $this->belongsTo(Rfid::class,'rfid_id');
    }
}
