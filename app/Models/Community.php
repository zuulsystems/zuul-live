<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Community extends Model
{
    use SoftDeletes;
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['community_image_url'];


    protected $guarded = ['id'];

    public function houses(){
        return $this->hasMany(House::class);
    }

    public function timezone(){
        return $this->belongsTo(Timezone::class);
    }

    //get community image url
    public function getCommunityImageUrlAttribute(){
        if( isset($this->attributes['community_logo']) && !empty($this->attributes['community_logo']) ){
            $filepath =  base_path('public/images/communities/' . $this->attributes["community_logo"]);
            if(file_exists($filepath)){
                return asset('images/communities/'.$this->attributes["community_logo"]);
            }else{
                return asset('images/communities/default-community.png');
            }
        }
        return asset('images/communities/default-community.png');
    }

    public function locations(){
        return $this->hasMany(Location::class);
    }

    public function emailTemplates(){
        return $this->hasMany(EmailTemplate::class);
    }

    public function scanner()
    {
        return $this->hasOne(Scanner::class, 'community_id');
    }

    public function webRelay()
    {
        return $this->hasOne(WebRelay::class, 'community_id');
    }

    public function remoteGuard()
    {
        return $this->hasOne(RemoteGuard::class, 'community_id');
    }

}
