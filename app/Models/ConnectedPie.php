<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConnectedPie extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'mac_address', 'dynamic_url', 'uid', 'camera_email', 'camera_password', 'community_id'];

    public function community()
    {
        return $this->belongsTo(Community::class);
    }


}
