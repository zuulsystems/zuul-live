<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HouseCsv extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['house_name','address','lot_number','primary_contact','file_name','community_id','created_by'];
}
