<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAdditionalContact extends Model
{
    //
    protected $table = 'user_additional_phone_numbers';

    protected $fillable = [
        'id', 'user_id', 'dial_code', 'contact', 'type', 'deleted_at'
    ];
    protected $appends = ['plain_number'];

    function getPlainNumberAttribute()
    {
        return onlyPhoneDigits($this->contact);
    }
}
