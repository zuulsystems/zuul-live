<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CsvImport extends Model
{
    //
    protected $fillable = ['email','type','file_name'];
}
