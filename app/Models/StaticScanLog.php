<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaticScanLog extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['sender_full_name','sender_formatted_phone','sender_formatted_address','sender','recipient_full_name','recipient_formatted_phone','recipient_formatted_address','recipient','community','event','start','expires','scan_date','scan_by','scan_log_id','license_plate','color','make','model','year','license', 'deleted_at'];

    public function getVehicleInfoAttribute(){
        return "{$this->license_plate}\n{$this->color}\n{$this->make}\n{$this->model}\n{$this->year}";
    }

}
