<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scanner extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mac_address', 'camera_id', 'community_id', 'enable', 'device_type_id', 'secret_key', 'rfid_multiplier', 'access_token', 'last_event', 'last_event_at', 'online_status', 'online_at'
    ];

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function camera()
    {
        return $this->belongsTo(Camera::class);
    }
}
