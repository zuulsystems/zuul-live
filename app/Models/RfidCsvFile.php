<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RfidCsvFile extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['file_name','total_records','process_records','community_id'];

    /**
     * Get the community.
     */
    public function community()
    {
        return $this->belongsTo(Community::class);
    }
}
