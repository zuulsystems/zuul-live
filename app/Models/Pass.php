<?php

namespace App\Models;

use App\Http\Traits\TimezoneTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pass extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $appends = ['isExpired'];

    public function event(){
        return $this->belongsTo(Event::class);
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }
    public function community(){
        return $this->belongsTo(Community::class);
    }
    public function house(){
        return $this->belongsTo(House::class);
    }

    /**
     * convert pass_start_date from community timezone
     *
     * @return false|int|string
     * @throws \Exception
     */
    public function convertedPassStartDate()
    {
        if (!empty($this->pass_start_date) && $this->community()->exists()) {
            return convertTimezone($this->pass_start_date, config('app.timezone'),
                $this->community->timezone->abbreviation);
        }

        return "";
    }

    public function convertedPassDate()
    {
        if (!empty($this->pass_date) && $this->community()->exists()) {
            return convertTimezone($this->pass_date, config('app.timezone'),
                $this->community->timezone->abbreviation);
        }

        return "";
    }

    public function convertedCurrentDateTime()
    {
        if (!empty($this->pass_date) && $this->community()->exists()) {
            return convertTimezone(date('Y-m-d H:i:s'), config('app.timezone'),
                $this->community->timezone->abbreviation);
        }

        return date('Y-m-d H:i:s');
    }

    /**
     * convert pass_date from community timezone
     *
     * @return false|int|string
     * @throws \Exception
     */
    public function convertedPassTime()
    {
        if (!empty($this->pass_date) && $this->community()->exists()) {
            return convertedPassTime($this->pass_date, config('app.timezone'), $this->community->timezone->abbreviation);
        }

        return "";
    }

    public function passUsers(){
        return $this->hasMany(PassUser::class);
    }
    public function addedUsers(){
        return $this->belongsToMany(User::class,'pass_users');
    }
    public function getAddedUserAttribute(){
        return $this->addedUsers->first();
    }

    public function getFormattedStartDateAttribute() {
        $tzt = dBTzToCommunityTz($this->pass_start_date, $this->community_id);
        $d = Carbon::parse($tzt)->format('M-d-Y');
        return $d;
    }

    public function getFormattedStartTimeAttribute()
    {
        $tzt = dBTzToCommunityTz($this->pass_start_date, $this->community_id);

        return Carbon::parse($tzt)->format('h:i A');
    }

    public function getFormattedDateAttribute() {
        $tzt = dBTzToCommunityTz($this->pass_date, $this->community_id);
        $d = Carbon::parse($tzt)->format('M-d-Y');
        return $d;
    }

    public function getFormattedTimeAttribute() {
        $tzt = dBTzToCommunityTz($this->pass_date, $this->community_id);
        $t = Carbon::parse($tzt)->format('h:i A');
        return $t;
    }

    public function getIsExpiredAttribute(){
        return Carbon::parse($this->pass_date)->isPast();
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'pass_id');
    }

}
