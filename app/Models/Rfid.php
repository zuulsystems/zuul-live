<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rfid extends Model
{
    use SoftDeletes;
    protected $table = "rfids";
    protected $fillable = ['id','user_id', 'rfid_tag_num', 'license', 'make_model', 'status', 'community_id', 'house_id','vehicle_id', 'created_by', 'updated_by', 'allow_date_limits', 'allow_date_limits_daily', 'suspend', 'vehicle_info'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function community() {
        return $this->belongsTo(Community::class, 'community_id');
    }

    public function house(){
        return $this->belongsTo(House::class, 'house_id');
    }

    public function vehicle(){
        return $this->belongsTo(Vehicle::class, 'vehicle_id');
    }

    public function rfidLogs(){
        return $this->hasMany(RfidLog::class, 'rfid_id');
    }
    public function rfidDateLimits(){
        return $this->hasMany(RfidDateLimit::class, 'rfid_id');
    }
}
