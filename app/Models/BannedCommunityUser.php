<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BannedCommunityUser extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'community_id', 'user_id','phone_number','license_plate','user_name'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function community() {
        return $this->belongsTo(Community::class, 'community_id');
    }

}
