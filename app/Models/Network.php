<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Network extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['registered_ip','community_id'];

    public function community()
    {
        return $this->belongsTo(Community::class);
    }
}
