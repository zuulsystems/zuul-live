<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Location extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_id', 'name', 'address', 'city', 'state', 'country', 'zip', 'geocode', 'group_id', 'direction', 'timezone_id', 'tz', 'polygon', 'community_id',
    ];

    public function community(){
        return $this->belongsTo(Community::class);
    }

    public function communityName(){
        return $this->belongsTo(Community::class, 'community_id')->select(['id', 'name']);
    }

    public function trafficTickets(){
        return $this->hasMany(TrafficTicket::class);
    }
}
