<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class RemoteGuardImage extends Model
{
    //
    protected $fillable = ['mac_address', 'ImageURL', 'scan_log_id','plateNum','image_type', 'url'];
    protected $appends = ['fullImageUrl'];

    //get license image url from s3 bucket
    public function getFullImageUrlAttribute()
    {
        return strpos($this->url, '127.0.0.1') !== false ? url('/') .'/'. $this->ImageURL : getS3Image($this->ImageURL);
    }
}
