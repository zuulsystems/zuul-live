<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OffensiveWord extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function community(){
        return $this->belongsTo(Community::class);
    }
}
