<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Camera extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function community(){
        return $this->belongsTo(Community::class,'community_id');
    }

    public function scanner(){
        return $this->belongsTo(Scanner::class,'scanner_id');
    }

    public function connectedpie(){
        return $this->belongsTo(ConnectedPie::class,'connected_pi_id');
    }
}
