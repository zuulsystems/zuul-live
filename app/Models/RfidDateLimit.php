<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RfidDateLimit extends Model
{
    use SoftDeletes;
    protected $table = 'rfid_date_limits';
    protected $fillable = ['rfid_id','name', 'start_end', 'start_date', 'end_date', 'status'];

    public function rfidDateLimitWeeks(){
        return $this->hasMany(RfidDateLimitWeek::class,'rfid_date_limit_id');
    }
    public function rfid(){
        return $this->belongsTo(Rfid::class,'rfid_id');
    }
}
