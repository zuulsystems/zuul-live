<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PassRequest extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request_user_id', 'send_user_id', 'name', 'email', 'description', 'phone', 'status','is_rejected','anonymous'
    ];

    public function requestedUser()
    {
        return $this->belongsTo(User::class,'request_user_id');
    }

    public function sendUser()
    {
        return $this->belongsTo(User::class,'send_user_id');
    }

    /**
     * get convert requested datetime from requested user => community => timezone
     * @return false|int|string
     * @throws \Exception
     */
    public function convertedRequestedAt(){
        if ($this->requestedUser()->exists() && $this->requestedUser->community()->exists() && $this->requestedUser->community->timezone()->exists()){
            return convertTimezone($this->created_at,  config('app.timezone'), $this->requestedUser->community->timezone->abbreviation);
        }
        return strtotime($this->created_at);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'pass_request_id');
    }
}
