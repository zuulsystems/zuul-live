<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScanLog extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'pass_user_id', 'is_success', 'type', 'scanner_id','log_text','scan_by','community_id','license_image','license_plate_image','license_plate_image_type','scan_at', 'guard_display_name'
    ];

    /**
     * Send Pass to User
     */
    public function passUser()
    {
        #Todo Have to see withTrashed() is being used in previous system
        return $this->belongsTo(PassUser::class, 'pass_user_id');
    }

    /**
     * Scan by Guard
     */
    public function scanBy()
    {
        return $this->belongsTo(User::class, 'scan_by');
    }

    /**
     * Scan By Scanner
     */
    public function scanner()
    {
        return $this->belongsTo(Scanner::class, 'scanner_id');
    }

    /**
     * get Scan By name @Guard Or @Scanner by type
     * @return string
     */
    public function getScanByNameAttribute()
    {
        $scanByName = "";
        if ($this->type == 'scanner' && $this->scanner()->exists()) {
            $scanByName = $this->scanner->mac_address;
        } else if ($this->type == 'guard' && $this->scanBy()->exists()) {
            $scanByName = $this->scanBy->fullName;
        }
        return $scanByName;
    }

    public function getScanDateAttribute()
    {
        $convertedScanData = dBTzToCommunityTz($this->created_at, $this->community_id);
        return Carbon::parse($convertedScanData)->format('m/d/Y h:i A');
    }

    public function addedUser(){
        return $this->hasOneThrough(User::class, PassUser::class, 'id', 'id','pass_user_id','user_id');
    }

    //get license image url from s3 bucket
    public function getLicenseImageUrlAttribute()
    {
        return getS3Image($this->license_image);
    }
    //get license plate image url from s3 bucket
    public function getLicensePlateImageUrlAttribute()
    {
        if ($this->license_plate_image_type != 'base64'){
            return getS3Image($this->license_plate_image);
        }

        return $this->license_plate_image;
    }
    public function community(){
        return $this->belongsTo(Community::class);
    }

    public function staticScanLog(){
        return $this->hasOne(StaticScanLog::class);
    }
}
