<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PDO;

class TrafficTicket extends Model
{
    use softDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ticket_id', 'location_id', 'community_id', 'time', 'data', 'current_speed_limit', 'violating_speed', 'plate_image_filename', 'license_plate', 'locked', 'ocr_status', 'edited', 'user_id', 'vision', 'karmen', 'carmen_old', 'carmen_general_engine', 'openalpr', 'google_v_1', 'openalpr_response_plate', 'openalpr_response_car', 'ticket_type', 'custom_info', 'speed_unit', 'validation_status_name', 'validation_name_color', 'camera_name', 'hash', 'password', 'static_url', 'location_name', 'images', 'serial', 'is_paid',
    ];

    public function community(){
        return $this->belongsTo(Community::class);
    }

    public function emailLog(){
        return $this->hasMany(EmailLog::class, 'ticket_id');
    }

    public function ticketImages(){
        return $this->hasMany(TrafficTicketImage::class, 'ticket_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function vehicles(){
        return $this->hasMany(Vehicle::class, 'license_plate', 'license_plate');
    }

    public function vehicle(){
        return $this->belongsTo(Vehicle::class, 'license_plate', 'license_plate');
    }

    public function trafficTickets(){
        return $this->hasMany(TrafficTicket::class, 'license_plate', 'license_plate');
    }


}
